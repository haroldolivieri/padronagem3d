﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2064;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13779_gshared (Enumerator_t2069 * __this, Dictionary_2_t2064 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m13779(__this, ___host, method) (( void (*) (Enumerator_t2069 *, Dictionary_2_t2064 *, const MethodInfo*))Enumerator__ctor_m13779_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13780_gshared (Enumerator_t2069 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13780(__this, method) (( Object_t * (*) (Enumerator_t2069 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13780_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m13781_gshared (Enumerator_t2069 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m13781(__this, method) (( void (*) (Enumerator_t2069 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13781_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13782_gshared (Enumerator_t2069 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13782(__this, method) (( void (*) (Enumerator_t2069 *, const MethodInfo*))Enumerator_Dispose_m13782_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13783_gshared (Enumerator_t2069 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13783(__this, method) (( bool (*) (Enumerator_t2069 *, const MethodInfo*))Enumerator_MoveNext_m13783_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13784_gshared (Enumerator_t2069 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13784(__this, method) (( Object_t * (*) (Enumerator_t2069 *, const MethodInfo*))Enumerator_get_Current_m13784_gshared)(__this, method)
