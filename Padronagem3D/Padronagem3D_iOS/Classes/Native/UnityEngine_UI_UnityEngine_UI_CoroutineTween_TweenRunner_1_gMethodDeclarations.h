﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t560;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t2;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m3625_gshared (TweenRunner_1_t560 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m3625(__this, method) (( void (*) (TweenRunner_1_t560 *, const MethodInfo*))TweenRunner_1__ctor_m3625_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m15690_gshared (Object_t * __this /* static, unused */, FloatTween_t535  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m15690(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, FloatTween_t535 , const MethodInfo*))TweenRunner_1_Start_m15690_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m3626_gshared (TweenRunner_1_t560 * __this, MonoBehaviour_t2 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m3626(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t560 *, MonoBehaviour_t2 *, const MethodInfo*))TweenRunner_1_Init_m3626_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m3648_gshared (TweenRunner_1_t560 * __this, FloatTween_t535  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m3648(__this, ___info, method) (( void (*) (TweenRunner_1_t560 *, FloatTween_t535 , const MethodInfo*))TweenRunner_1_StartTween_m3648_gshared)(__this, ___info, method)
