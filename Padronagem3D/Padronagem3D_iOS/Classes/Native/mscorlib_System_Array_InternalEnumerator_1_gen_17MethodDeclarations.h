﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12093_gshared (InternalEnumerator_1_t1937 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12093(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1937 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12093_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12094_gshared (InternalEnumerator_1_t1937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12094(__this, method) (( void (*) (InternalEnumerator_1_t1937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12094_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12095_gshared (InternalEnumerator_1_t1937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12095(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12095_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12096_gshared (InternalEnumerator_1_t1937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12096(__this, method) (( void (*) (InternalEnumerator_1_t1937 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12096_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12097_gshared (InternalEnumerator_1_t1937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12097(__this, method) (( bool (*) (InternalEnumerator_1_t1937 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12097_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t348  InternalEnumerator_1_get_Current_m12098_gshared (InternalEnumerator_1_t1937 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12098(__this, method) (( GcScoreData_t348  (*) (InternalEnumerator_1_t1937 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12098_gshared)(__this, method)
