﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t2350;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2340;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17628_gshared (ShimEnumerator_t2350 * __this, Dictionary_2_t2340 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17628(__this, ___host, method) (( void (*) (ShimEnumerator_t2350 *, Dictionary_2_t2340 *, const MethodInfo*))ShimEnumerator__ctor_m17628_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17629_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17629(__this, method) (( bool (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_MoveNext_m17629_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t943  ShimEnumerator_get_Entry_m17630_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17630(__this, method) (( DictionaryEntry_t943  (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_get_Entry_m17630_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17631_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17631(__this, method) (( Object_t * (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_get_Key_m17631_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17632_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17632(__this, method) (( Object_t * (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_get_Value_m17632_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17633_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17633(__this, method) (( Object_t * (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_get_Current_m17633_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C" void ShimEnumerator_Reset_m17634_gshared (ShimEnumerator_t2350 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m17634(__this, method) (( void (*) (ShimEnumerator_t2350 *, const MethodInfo*))ShimEnumerator_Reset_m17634_gshared)(__this, method)
