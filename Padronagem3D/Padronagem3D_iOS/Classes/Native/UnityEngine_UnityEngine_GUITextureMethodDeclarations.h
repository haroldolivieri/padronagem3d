﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUITexture
struct GUITexture_t141;
// UnityEngine.Texture
struct Texture_t14;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Rect.h"

// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C" Color_t11  GUITexture_get_color_m659 (GUITexture_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C" void GUITexture_set_color_m660 (GUITexture_t141 * __this, Color_t11  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_get_color_m854 (GUITexture_t141 * __this, Color_t11 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_set_color_m855 (GUITexture_t141 * __this, Color_t11 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C" void GUITexture_set_texture_m658 (GUITexture_t141 * __this, Texture_t14 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_pixelInset(UnityEngine.Rect)
extern "C" void GUITexture_set_pixelInset_m654 (GUITexture_t141 * __this, Rect_t18  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
extern "C" void GUITexture_INTERNAL_set_pixelInset_m856 (GUITexture_t141 * __this, Rect_t18 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
