﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2044;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13583_gshared (Enumerator_t2052 * __this, Dictionary_2_t2044 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m13583(__this, ___dictionary, method) (( void (*) (Enumerator_t2052 *, Dictionary_2_t2044 *, const MethodInfo*))Enumerator__ctor_m13583_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13584_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13584(__this, method) (( Object_t * (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13584_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m13585_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m13585(__this, method) (( void (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13585_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t943  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13586_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13586(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13586_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13587_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13587(__this, method) (( Object_t * (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13587_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13588_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13588(__this, method) (( Object_t * (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13588_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13589_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13589(__this, method) (( bool (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_MoveNext_m13589_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2047  Enumerator_get_Current_m13590_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13590(__this, method) (( KeyValuePair_2_t2047  (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_get_Current_m13590_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m13591_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m13591(__this, method) (( Object_t * (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_get_CurrentKey_m13591_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m13592_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m13592(__this, method) (( int32_t (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_get_CurrentValue_m13592_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m13593_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_Reset_m13593(__this, method) (( void (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_Reset_m13593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m13594_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13594(__this, method) (( void (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_VerifyState_m13594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m13595_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m13595(__this, method) (( void (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_VerifyCurrent_m13595_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m13596_gshared (Enumerator_t2052 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13596(__this, method) (( void (*) (Enumerator_t2052 *, const MethodInfo*))Enumerator_Dispose_m13596_gshared)(__this, method)
