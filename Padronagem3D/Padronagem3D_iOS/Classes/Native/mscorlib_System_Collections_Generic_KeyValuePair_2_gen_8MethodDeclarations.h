﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15948(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2229 *, Font_t283 *, List_1_t690 *, const MethodInfo*))KeyValuePair_2__ctor_m14045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m15949(__this, method) (( Font_t283 * (*) (KeyValuePair_2_t2229 *, const MethodInfo*))KeyValuePair_2_get_Key_m14046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15950(__this, ___value, method) (( void (*) (KeyValuePair_2_t2229 *, Font_t283 *, const MethodInfo*))KeyValuePair_2_set_Key_m14047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m15951(__this, method) (( List_1_t690 * (*) (KeyValuePair_2_t2229 *, const MethodInfo*))KeyValuePair_2_get_Value_m14048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15952(__this, ___value, method) (( void (*) (KeyValuePair_2_t2229 *, List_1_t690 *, const MethodInfo*))KeyValuePair_2_set_Value_m14049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m15953(__this, method) (( String_t* (*) (KeyValuePair_2_t2229 *, const MethodInfo*))KeyValuePair_2_ToString_m14050_gshared)(__this, method)
