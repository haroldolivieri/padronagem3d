﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUITexture
struct GUITexture_t141;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_1.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "AssemblyU2DUnityScript_Fade.h"

// splashscreen/$FadeGUITexture$22/$
struct  U24_t139  : public GenericGeneratorEnumerator_1_t140
{
	// System.Single splashscreen/$FadeGUITexture$22/$::$start$23
	float ___U24startU2423_2;
	// System.Single splashscreen/$FadeGUITexture$22/$::$end$24
	float ___U24endU2424_3;
	// System.Single splashscreen/$FadeGUITexture$22/$::$i$25
	float ___U24iU2425_4;
	// System.Single splashscreen/$FadeGUITexture$22/$::$step$26
	float ___U24stepU2426_5;
	// System.Single splashscreen/$FadeGUITexture$22/$::$$6$27
	float ___U24U246U2427_6;
	// UnityEngine.Color splashscreen/$FadeGUITexture$22/$::$$7$28
	Color_t11  ___U24U247U2428_7;
	// UnityEngine.GUITexture splashscreen/$FadeGUITexture$22/$::$guiObject$29
	GUITexture_t141 * ___U24guiObjectU2429_8;
	// System.Single splashscreen/$FadeGUITexture$22/$::$timer$30
	float ___U24timerU2430_9;
	// Fade splashscreen/$FadeGUITexture$22/$::$fadeType$31
	int32_t ___U24fadeTypeU2431_10;
};
