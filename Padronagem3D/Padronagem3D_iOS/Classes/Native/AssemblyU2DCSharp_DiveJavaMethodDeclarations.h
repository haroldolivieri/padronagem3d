﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiveJava
struct DiveJava_t60;

#include "codegen/il2cpp-codegen.h"

// System.Void DiveJava::.ctor()
extern "C" void DiveJava__ctor_m281 (DiveJava_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveJava::.cctor()
extern "C" void DiveJava__cctor_m282 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveJava::Start()
extern "C" void DiveJava_Start_m283 (DiveJava_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveJava::Update()
extern "C" void DiveJava_Update_m284 (DiveJava_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveJava::setFullscreen()
extern "C" void DiveJava_setFullscreen_m285 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveJava::init()
extern "C" void DiveJava_init_m286 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
