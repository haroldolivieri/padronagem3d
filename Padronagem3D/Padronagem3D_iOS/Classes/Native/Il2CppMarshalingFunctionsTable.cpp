﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void WaitForSeconds_t153_marshal ();
extern "C" void WaitForSeconds_t153_marshal_back ();
extern "C" void WaitForSeconds_t153_marshal_cleanup ();
extern "C" void Coroutine_t154_marshal ();
extern "C" void Coroutine_t154_marshal_back ();
extern "C" void Coroutine_t154_marshal_cleanup ();
extern "C" void ScriptableObject_t166_marshal ();
extern "C" void ScriptableObject_t166_marshal_back ();
extern "C" void ScriptableObject_t166_marshal_cleanup ();
extern "C" void Gradient_t199_marshal ();
extern "C" void Gradient_t199_marshal_back ();
extern "C" void Gradient_t199_marshal_cleanup ();
extern "C" void CacheIndex_t222_marshal ();
extern "C" void CacheIndex_t222_marshal_back ();
extern "C" void CacheIndex_t222_marshal_cleanup ();
extern "C" void AsyncOperation_t127_marshal ();
extern "C" void AsyncOperation_t127_marshal_back ();
extern "C" void AsyncOperation_t127_marshal_cleanup ();
extern "C" void Touch_t233_marshal ();
extern "C" void Touch_t233_marshal_back ();
extern "C" void Touch_t233_marshal_cleanup ();
extern "C" void Object_t82_marshal ();
extern "C" void Object_t82_marshal_back ();
extern "C" void Object_t82_marshal_cleanup ();
extern "C" void YieldInstruction_t164_marshal ();
extern "C" void YieldInstruction_t164_marshal_back ();
extern "C" void YieldInstruction_t164_marshal_cleanup ();
extern "C" void WebCamDevice_t263_marshal ();
extern "C" void WebCamDevice_t263_marshal_back ();
extern "C" void WebCamDevice_t263_marshal_cleanup ();
extern "C" void AnimationCurve_t270_marshal ();
extern "C" void AnimationCurve_t270_marshal_back ();
extern "C" void AnimationCurve_t270_marshal_cleanup ();
extern "C" void AnimatorTransitionInfo_t272_marshal ();
extern "C" void AnimatorTransitionInfo_t272_marshal_back ();
extern "C" void AnimatorTransitionInfo_t272_marshal_cleanup ();
extern "C" void SkeletonBone_t274_marshal ();
extern "C" void SkeletonBone_t274_marshal_back ();
extern "C" void SkeletonBone_t274_marshal_cleanup ();
extern "C" void HumanBone_t276_marshal ();
extern "C" void HumanBone_t276_marshal_back ();
extern "C" void HumanBone_t276_marshal_cleanup ();
extern "C" void CharacterInfo_t281_marshal ();
extern "C" void CharacterInfo_t281_marshal_back ();
extern "C" void CharacterInfo_t281_marshal_cleanup ();
extern "C" void Event_t84_marshal ();
extern "C" void Event_t84_marshal_back ();
extern "C" void Event_t84_marshal_cleanup ();
extern "C" void GcAchievementData_t347_marshal ();
extern "C" void GcAchievementData_t347_marshal_back ();
extern "C" void GcAchievementData_t347_marshal_cleanup ();
extern "C" void GcScoreData_t348_marshal ();
extern "C" void GcScoreData_t348_marshal_back ();
extern "C" void GcScoreData_t348_marshal_cleanup ();
extern "C" void TrackedReference_t271_marshal ();
extern "C" void TrackedReference_t271_marshal_back ();
extern "C" void TrackedReference_t271_marshal_cleanup ();
extern "C" void X509ChainStatus_t843_marshal ();
extern "C" void X509ChainStatus_t843_marshal_back ();
extern "C" void X509ChainStatus_t843_marshal_cleanup ();
extern "C" void IntStack_t894_marshal ();
extern "C" void IntStack_t894_marshal_back ();
extern "C" void IntStack_t894_marshal_cleanup ();
extern "C" void Interval_t900_marshal ();
extern "C" void Interval_t900_marshal_back ();
extern "C" void Interval_t900_marshal_cleanup ();
extern "C" void UriScheme_t930_marshal ();
extern "C" void UriScheme_t930_marshal_back ();
extern "C" void UriScheme_t930_marshal_cleanup ();
extern "C" void Context_t1173_marshal ();
extern "C" void Context_t1173_marshal_back ();
extern "C" void Context_t1173_marshal_cleanup ();
extern "C" void PreviousInfo_t1174_marshal ();
extern "C" void PreviousInfo_t1174_marshal_back ();
extern "C" void PreviousInfo_t1174_marshal_cleanup ();
extern "C" void Escape_t1175_marshal ();
extern "C" void Escape_t1175_marshal_back ();
extern "C" void Escape_t1175_marshal_cleanup ();
extern "C" void MonoIOStat_t1293_marshal ();
extern "C" void MonoIOStat_t1293_marshal_back ();
extern "C" void MonoIOStat_t1293_marshal_cleanup ();
extern "C" void ParameterModifier_t1392_marshal ();
extern "C" void ParameterModifier_t1392_marshal_back ();
extern "C" void ParameterModifier_t1392_marshal_cleanup ();
extern "C" void ResourceInfo_t1403_marshal ();
extern "C" void ResourceInfo_t1403_marshal_back ();
extern "C" void ResourceInfo_t1403_marshal_cleanup ();
extern "C" void DSAParameters_t967_marshal ();
extern "C" void DSAParameters_t967_marshal_back ();
extern "C" void DSAParameters_t967_marshal_cleanup ();
extern "C" void RSAParameters_t965_marshal ();
extern "C" void RSAParameters_t965_marshal_back ();
extern "C" void RSAParameters_t965_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[32] = 
{
	{ WaitForSeconds_t153_marshal, WaitForSeconds_t153_marshal_back, WaitForSeconds_t153_marshal_cleanup },
	{ Coroutine_t154_marshal, Coroutine_t154_marshal_back, Coroutine_t154_marshal_cleanup },
	{ ScriptableObject_t166_marshal, ScriptableObject_t166_marshal_back, ScriptableObject_t166_marshal_cleanup },
	{ Gradient_t199_marshal, Gradient_t199_marshal_back, Gradient_t199_marshal_cleanup },
	{ CacheIndex_t222_marshal, CacheIndex_t222_marshal_back, CacheIndex_t222_marshal_cleanup },
	{ AsyncOperation_t127_marshal, AsyncOperation_t127_marshal_back, AsyncOperation_t127_marshal_cleanup },
	{ Touch_t233_marshal, Touch_t233_marshal_back, Touch_t233_marshal_cleanup },
	{ Object_t82_marshal, Object_t82_marshal_back, Object_t82_marshal_cleanup },
	{ YieldInstruction_t164_marshal, YieldInstruction_t164_marshal_back, YieldInstruction_t164_marshal_cleanup },
	{ WebCamDevice_t263_marshal, WebCamDevice_t263_marshal_back, WebCamDevice_t263_marshal_cleanup },
	{ AnimationCurve_t270_marshal, AnimationCurve_t270_marshal_back, AnimationCurve_t270_marshal_cleanup },
	{ AnimatorTransitionInfo_t272_marshal, AnimatorTransitionInfo_t272_marshal_back, AnimatorTransitionInfo_t272_marshal_cleanup },
	{ SkeletonBone_t274_marshal, SkeletonBone_t274_marshal_back, SkeletonBone_t274_marshal_cleanup },
	{ HumanBone_t276_marshal, HumanBone_t276_marshal_back, HumanBone_t276_marshal_cleanup },
	{ CharacterInfo_t281_marshal, CharacterInfo_t281_marshal_back, CharacterInfo_t281_marshal_cleanup },
	{ Event_t84_marshal, Event_t84_marshal_back, Event_t84_marshal_cleanup },
	{ GcAchievementData_t347_marshal, GcAchievementData_t347_marshal_back, GcAchievementData_t347_marshal_cleanup },
	{ GcScoreData_t348_marshal, GcScoreData_t348_marshal_back, GcScoreData_t348_marshal_cleanup },
	{ TrackedReference_t271_marshal, TrackedReference_t271_marshal_back, TrackedReference_t271_marshal_cleanup },
	{ X509ChainStatus_t843_marshal, X509ChainStatus_t843_marshal_back, X509ChainStatus_t843_marshal_cleanup },
	{ IntStack_t894_marshal, IntStack_t894_marshal_back, IntStack_t894_marshal_cleanup },
	{ Interval_t900_marshal, Interval_t900_marshal_back, Interval_t900_marshal_cleanup },
	{ UriScheme_t930_marshal, UriScheme_t930_marshal_back, UriScheme_t930_marshal_cleanup },
	{ Context_t1173_marshal, Context_t1173_marshal_back, Context_t1173_marshal_cleanup },
	{ PreviousInfo_t1174_marshal, PreviousInfo_t1174_marshal_back, PreviousInfo_t1174_marshal_cleanup },
	{ Escape_t1175_marshal, Escape_t1175_marshal_back, Escape_t1175_marshal_cleanup },
	{ MonoIOStat_t1293_marshal, MonoIOStat_t1293_marshal_back, MonoIOStat_t1293_marshal_cleanup },
	{ ParameterModifier_t1392_marshal, ParameterModifier_t1392_marshal_back, ParameterModifier_t1392_marshal_cleanup },
	{ ResourceInfo_t1403_marshal, ResourceInfo_t1403_marshal_back, ResourceInfo_t1403_marshal_cleanup },
	{ DSAParameters_t967_marshal, DSAParameters_t967_marshal_back, DSAParameters_t967_marshal_cleanup },
	{ RSAParameters_t965_marshal, RSAParameters_t965_marshal_back, RSAParameters_t965_marshal_cleanup },
	NULL,
};
