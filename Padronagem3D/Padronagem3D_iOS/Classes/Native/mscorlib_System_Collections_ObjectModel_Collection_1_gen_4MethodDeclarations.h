﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>
struct Collection_1_t1976;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t422;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t2485;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t1975;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::.ctor()
extern "C" void Collection_1__ctor_m12647_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12647(__this, method) (( void (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1__ctor_m12647_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12648_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12648(__this, method) (( bool (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12648_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12649_gshared (Collection_1_t1976 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12649(__this, ___array, ___index, method) (( void (*) (Collection_1_t1976 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12649_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12650_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12650(__this, method) (( Object_t * (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12650_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12651_gshared (Collection_1_t1976 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12651(__this, ___value, method) (( int32_t (*) (Collection_1_t1976 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12651_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12652_gshared (Collection_1_t1976 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12652(__this, ___value, method) (( bool (*) (Collection_1_t1976 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12652_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12653_gshared (Collection_1_t1976 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12653(__this, ___value, method) (( int32_t (*) (Collection_1_t1976 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12653_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12654_gshared (Collection_1_t1976 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12654(__this, ___index, ___value, method) (( void (*) (Collection_1_t1976 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12654_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12655_gshared (Collection_1_t1976 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12655(__this, ___value, method) (( void (*) (Collection_1_t1976 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12655_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12656_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12656(__this, method) (( bool (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12656_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12657_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12657(__this, method) (( Object_t * (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12657_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12658_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12658(__this, method) (( bool (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12658_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12659_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12659(__this, method) (( bool (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12659_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12660_gshared (Collection_1_t1976 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12660(__this, ___index, method) (( Object_t * (*) (Collection_1_t1976 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12660_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12661_gshared (Collection_1_t1976 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12661(__this, ___index, ___value, method) (( void (*) (Collection_1_t1976 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12661_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Add(T)
extern "C" void Collection_1_Add_m12662_gshared (Collection_1_t1976 * __this, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_Add_m12662(__this, ___item, method) (( void (*) (Collection_1_t1976 *, Color32_t187 , const MethodInfo*))Collection_1_Add_m12662_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Clear()
extern "C" void Collection_1_Clear_m12663_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12663(__this, method) (( void (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_Clear_m12663_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ClearItems()
extern "C" void Collection_1_ClearItems_m12664_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12664(__this, method) (( void (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_ClearItems_m12664_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool Collection_1_Contains_m12665_gshared (Collection_1_t1976 * __this, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12665(__this, ___item, method) (( bool (*) (Collection_1_t1976 *, Color32_t187 , const MethodInfo*))Collection_1_Contains_m12665_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12666_gshared (Collection_1_t1976 * __this, Color32U5BU5D_t422* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12666(__this, ___array, ___index, method) (( void (*) (Collection_1_t1976 *, Color32U5BU5D_t422*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12666_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12667_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12667(__this, method) (( Object_t* (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_GetEnumerator_m12667_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12668_gshared (Collection_1_t1976 * __this, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12668(__this, ___item, method) (( int32_t (*) (Collection_1_t1976 *, Color32_t187 , const MethodInfo*))Collection_1_IndexOf_m12668_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12669_gshared (Collection_1_t1976 * __this, int32_t ___index, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12669(__this, ___index, ___item, method) (( void (*) (Collection_1_t1976 *, int32_t, Color32_t187 , const MethodInfo*))Collection_1_Insert_m12669_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12670_gshared (Collection_1_t1976 * __this, int32_t ___index, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12670(__this, ___index, ___item, method) (( void (*) (Collection_1_t1976 *, int32_t, Color32_t187 , const MethodInfo*))Collection_1_InsertItem_m12670_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Remove(T)
extern "C" bool Collection_1_Remove_m12671_gshared (Collection_1_t1976 * __this, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12671(__this, ___item, method) (( bool (*) (Collection_1_t1976 *, Color32_t187 , const MethodInfo*))Collection_1_Remove_m12671_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12672_gshared (Collection_1_t1976 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12672(__this, ___index, method) (( void (*) (Collection_1_t1976 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12672_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12673_gshared (Collection_1_t1976 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12673(__this, ___index, method) (( void (*) (Collection_1_t1976 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12673_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12674_gshared (Collection_1_t1976 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12674(__this, method) (( int32_t (*) (Collection_1_t1976 *, const MethodInfo*))Collection_1_get_Count_m12674_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t187  Collection_1_get_Item_m12675_gshared (Collection_1_t1976 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12675(__this, ___index, method) (( Color32_t187  (*) (Collection_1_t1976 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12675_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12676_gshared (Collection_1_t1976 * __this, int32_t ___index, Color32_t187  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12676(__this, ___index, ___value, method) (( void (*) (Collection_1_t1976 *, int32_t, Color32_t187 , const MethodInfo*))Collection_1_set_Item_m12676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12677_gshared (Collection_1_t1976 * __this, int32_t ___index, Color32_t187  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12677(__this, ___index, ___item, method) (( void (*) (Collection_1_t1976 *, int32_t, Color32_t187 , const MethodInfo*))Collection_1_SetItem_m12677_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12678_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12678(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12678_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ConvertItem(System.Object)
extern "C" Color32_t187  Collection_1_ConvertItem_m12679_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12679(__this /* static, unused */, ___item, method) (( Color32_t187  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12679_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12680_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12680(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12680_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12681_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12681(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12681_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12682_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12682(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12682_gshared)(__this /* static, unused */, ___list, method)
