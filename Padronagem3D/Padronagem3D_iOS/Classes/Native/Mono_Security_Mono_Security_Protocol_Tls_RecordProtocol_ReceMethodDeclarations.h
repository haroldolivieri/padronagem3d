﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1051;
// System.AsyncCallback
struct AsyncCallback_t9;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t75;
// System.IO.Stream
struct Stream_t1053;
// System.Exception
struct Exception_t108;
// System.Threading.WaitHandle
struct WaitHandle_t1099;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m5488 (ReceiveRecordAsyncResult_t1051 * __this, AsyncCallback_t9 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t75* ___initialBuffer, Stream_t1053 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1053 * ReceiveRecordAsyncResult_get_Record_m5489 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t75* ReceiveRecordAsyncResult_get_ResultingBuffer_m5490 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t75* ReceiveRecordAsyncResult_get_InitialBuffer_m5491 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m5492 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t108 * ReceiveRecordAsyncResult_get_AsyncException_m5493 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m5494 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1099 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m5495 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m5496 (ReceiveRecordAsyncResult_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5497 (ReceiveRecordAsyncResult_t1051 * __this, Exception_t108 * ___ex, ByteU5BU5D_t75* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5498 (ReceiveRecordAsyncResult_t1051 * __this, Exception_t108 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5499 (ReceiveRecordAsyncResult_t1051 * __this, ByteU5BU5D_t75* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
