﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t565;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t2;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m3652_gshared (TweenRunner_1_t565 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m3652(__this, method) (( void (*) (TweenRunner_1_t565 *, const MethodInfo*))TweenRunner_1__ctor_m3652_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m15982_gshared (Object_t * __this /* static, unused */, ColorTween_t532  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m15982(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t532 , const MethodInfo*))TweenRunner_1_Start_m15982_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m3653_gshared (TweenRunner_1_t565 * __this, MonoBehaviour_t2 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m3653(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t565 *, MonoBehaviour_t2 *, const MethodInfo*))TweenRunner_1_Init_m3653_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m3660_gshared (TweenRunner_1_t565 * __this, ColorTween_t532  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m3660(__this, ___info, method) (( void (*) (TweenRunner_1_t565 *, ColorTween_t532 , const MethodInfo*))TweenRunner_1_StartTween_m3660_gshared)(__this, ___info, method)
