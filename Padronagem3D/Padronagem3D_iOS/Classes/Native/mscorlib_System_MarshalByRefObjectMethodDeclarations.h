﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MarshalByRefObject
struct MarshalByRefObject_t817;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1154;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MarshalByRefObject::.ctor()
extern "C" void MarshalByRefObject__ctor_m4859 (MarshalByRefObject_t817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::get_ObjectIdentity()
extern "C" ServerIdentity_t1154 * MarshalByRefObject_get_ObjectIdentity_m6756 (MarshalByRefObject_t817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
