﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2608  : public Array_t { };
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t2609  : public Array_t { };
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t1898  : public Array_t { };
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t410  : public Array_t { };
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t1912  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t446  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t448  : public Array_t { };
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t365  : public Array_t { };
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t361  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t174  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t175  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t1930  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t412  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t447  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t413  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t449  : public Array_t { };
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t299  : public Array_t { };
// UnityEngine.Vector4[]
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t418  : public Array_t { };
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t420  : public Array_t { };
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct Color32U5BU5D_t422  : public Array_t { };
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t371  : public Array_t { };
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t230  : public Array_t { };
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t246  : public Array_t { };
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t427  : public Array_t { };
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t2001  : public Array_t { };
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t428  : public Array_t { };
// UnityEngine.ContactPoint2D[]
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t257  : public Array_t { };
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t429  : public Array_t { };
// UnityEngine.CharacterInfo[]
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t430  : public Array_t { };
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t431  : public Array_t { };
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t432  : public Array_t { };
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t433  : public Array_t { };
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t437  : public Array_t { };
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t2061  : public Array_t { };
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2077  : public Array_t { };
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t324  : public Array_t { };
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t334  : public Array_t { };
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t335  : public Array_t { };
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t336  : public Array_t { };
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t370  : public Array_t { };
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t2109  : public Array_t { };
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t2108  : public Array_t { };
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t2142  : public Array_t { };
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t2147  : public Array_t { };
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2176  : public Array_t { };
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t2209  : public Array_t { };
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t2219  : public Array_t { };
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t2272  : public Array_t { };
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t2298  : public Array_t { };
