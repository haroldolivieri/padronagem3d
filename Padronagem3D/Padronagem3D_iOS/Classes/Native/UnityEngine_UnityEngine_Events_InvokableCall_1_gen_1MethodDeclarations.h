﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t2137;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t2138;
// System.Object[]
struct ObjectU5BU5D_t115;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m14531_gshared (InvokableCall_1_t2137 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m14531(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2137 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m14531_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m14532_gshared (InvokableCall_1_t2137 * __this, UnityAction_1_t2138 * ___action, const MethodInfo* method);
#define InvokableCall_1__ctor_m14532(__this, ___action, method) (( void (*) (InvokableCall_1_t2137 *, UnityAction_1_t2138 *, const MethodInfo*))InvokableCall_1__ctor_m14532_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m14533_gshared (InvokableCall_1_t2137 * __this, ObjectU5BU5D_t115* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m14533(__this, ___args, method) (( void (*) (InvokableCall_1_t2137 *, ObjectU5BU5D_t115*, const MethodInfo*))InvokableCall_1_Invoke_m14533_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m14534_gshared (InvokableCall_1_t2137 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m14534(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2137 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m14534_gshared)(__this, ___targetObj, ___method, method)
