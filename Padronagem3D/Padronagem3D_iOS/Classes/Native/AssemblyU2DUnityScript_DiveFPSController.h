﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t47;
// UnityEngine.CharacterController
struct CharacterController_t131;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"

// DiveFPSController
struct  DiveFPSController_t128  : public MonoBehaviour_t2
{
	// UnityEngine.Vector3 DiveFPSController::inputMoveDirection
	Vector3_t3  ___inputMoveDirection_2;
	// System.Boolean DiveFPSController::inputJump
	bool ___inputJump_3;
	// System.Boolean DiveFPSController::grounded
	bool ___grounded_4;
	// System.Single DiveFPSController::downmovement
	float ___downmovement_5;
	// UnityEngine.Vector3 DiveFPSController::velocity
	Vector3_t3  ___velocity_6;
	// System.Single DiveFPSController::max_speed
	float ___max_speed_7;
	// System.Single DiveFPSController::max_speed_air
	float ___max_speed_air_8;
	// System.Single DiveFPSController::max_speed_ground
	float ___max_speed_ground_9;
	// System.Int32 DiveFPSController::acceleration
	int32_t ___acceleration_10;
	// System.Int32 DiveFPSController::acceleration_air
	int32_t ___acceleration_air_11;
	// System.Single DiveFPSController::gravity
	float ___gravity_12;
	// System.Single DiveFPSController::friction
	float ___friction_13;
	// UnityEngine.GameObject DiveFPSController::cameraObject
	GameObject_t47 * ___cameraObject_14;
	// UnityEngine.CharacterController DiveFPSController::controller
	CharacterController_t131 * ___controller_15;
	// UnityEngine.Vector3 DiveFPSController::groundNormal
	Vector3_t3  ___groundNormal_16;
	// System.Single DiveFPSController::jumpspeed
	float ___jumpspeed_17;
	// System.Boolean DiveFPSController::stopmovingup
	bool ___stopmovingup_18;
	// System.Single DiveFPSController::fallkillspeed
	float ___fallkillspeed_19;
	// UnityEngine.CollisionFlags DiveFPSController::collisionFlags
	int32_t ___collisionFlags_20;
	// UnityEngine.GameObject DiveFPSController::ground_gameobject
	GameObject_t47 * ___ground_gameobject_21;
	// UnityEngine.Vector3 DiveFPSController::last_ground_pos
	Vector3_t3  ___last_ground_pos_22;
	// System.Int32 DiveFPSController::jumpcommand
	int32_t ___jumpcommand_23;
	// System.Boolean DiveFPSController::floating
	bool ___floating_24;
	// System.Int32 DiveFPSController::autowalk
	int32_t ___autowalk_25;
	// System.Int32 DiveFPSController::inhibit_autowalk
	int32_t ___inhibit_autowalk_26;
	// System.Int32 DiveFPSController::reload_once
	int32_t ___reload_once_27;
	// System.Single DiveFPSController::fadeduration
	float ___fadeduration_28;
};
