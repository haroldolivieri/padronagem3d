﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t317;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t316;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
struct  Enumerator_t456 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::l
	List_1_t317 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::current
	GUILayoutEntry_t316 * ___current_3;
};
