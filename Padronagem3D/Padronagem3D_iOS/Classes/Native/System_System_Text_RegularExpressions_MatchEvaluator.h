﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t863;
// System.IAsyncResult
struct IAsyncResult_t8;
// System.AsyncCallback
struct AsyncCallback_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t938  : public MulticastDelegate_t7
{
};
