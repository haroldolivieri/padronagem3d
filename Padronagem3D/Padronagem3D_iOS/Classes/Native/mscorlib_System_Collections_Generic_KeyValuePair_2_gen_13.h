﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey
struct DispatcherKey_t722;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct Dispatcher_t718;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>
struct  KeyValuePair_2_t2330 
{
	// TKey System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::key
	DispatcherKey_t722 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::value
	Dispatcher_t718 * ___value_1;
};
