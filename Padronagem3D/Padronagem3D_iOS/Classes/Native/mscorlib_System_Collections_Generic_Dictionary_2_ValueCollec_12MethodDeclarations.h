﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2112;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_12.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14410_gshared (Enumerator_t2118 * __this, Dictionary_2_t2112 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m14410(__this, ___host, method) (( void (*) (Enumerator_t2118 *, Dictionary_2_t2112 *, const MethodInfo*))Enumerator__ctor_m14410_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14411_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14411(__this, method) (( Object_t * (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14411_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m14412_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m14412(__this, method) (( void (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14412_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m14413_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14413(__this, method) (( void (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_Dispose_m14413_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14414_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14414(__this, method) (( bool (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_MoveNext_m14414_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t Enumerator_get_Current_m14415_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14415(__this, method) (( int32_t (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_get_Current_m14415_gshared)(__this, method)
