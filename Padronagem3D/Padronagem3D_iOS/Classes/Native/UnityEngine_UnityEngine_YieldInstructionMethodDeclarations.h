﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.YieldInstruction
struct YieldInstruction_t164;
struct YieldInstruction_t164_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m1369 (YieldInstruction_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void YieldInstruction_t164_marshal(const YieldInstruction_t164& unmarshaled, YieldInstruction_t164_marshaled& marshaled);
extern "C" void YieldInstruction_t164_marshal_back(const YieldInstruction_t164_marshaled& marshaled, YieldInstruction_t164& unmarshaled);
extern "C" void YieldInstruction_t164_marshal_cleanup(YieldInstruction_t164_marshaled& marshaled);
