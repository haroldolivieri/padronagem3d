﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2122;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2112;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m14442_gshared (ShimEnumerator_t2122 * __this, Dictionary_2_t2112 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m14442(__this, ___host, method) (( void (*) (ShimEnumerator_t2122 *, Dictionary_2_t2112 *, const MethodInfo*))ShimEnumerator__ctor_m14442_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m14443_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m14443(__this, method) (( bool (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_MoveNext_m14443_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t943  ShimEnumerator_get_Entry_m14444_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m14444(__this, method) (( DictionaryEntry_t943  (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_get_Entry_m14444_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m14445_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m14445(__this, method) (( Object_t * (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_get_Key_m14445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m14446_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m14446(__this, method) (( Object_t * (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_get_Value_m14446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m14447_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m14447(__this, method) (( Object_t * (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_get_Current_m14447_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void ShimEnumerator_Reset_m14448_gshared (ShimEnumerator_t2122 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m14448(__this, method) (( void (*) (ShimEnumerator_t2122 *, const MethodInfo*))ShimEnumerator_Reset_m14448_gshared)(__this, method)
