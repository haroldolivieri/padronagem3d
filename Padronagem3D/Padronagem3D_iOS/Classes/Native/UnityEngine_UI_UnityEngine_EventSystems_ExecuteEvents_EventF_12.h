﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t667;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t92;
// System.IAsyncResult
struct IAsyncResult_t8;
// System.AsyncCallback
struct AsyncCallback_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct  EventFunction_1_t500  : public MulticastDelegate_t7
{
};
