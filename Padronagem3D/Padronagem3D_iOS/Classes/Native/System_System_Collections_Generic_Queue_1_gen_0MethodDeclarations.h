﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2336;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object[]
struct ObjectU5BU5D_t115;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m17453_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1__ctor_m17453(__this, method) (( void (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1__ctor_m17453_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m17454_gshared (Queue_1_t2336 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m17454(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2336 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m17454_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m17455_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m17455(__this, method) (( bool (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m17455_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m17456_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m17456(__this, method) (( Object_t * (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m17456_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17457_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17457(__this, method) (( Object_t* (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17457_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m17458_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m17458(__this, method) (( Object_t * (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m17458_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Clear()
extern "C" void Queue_1_Clear_m17459_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_Clear_m17459(__this, method) (( void (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_Clear_m17459_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m17460_gshared (Queue_1_t2336 * __this, ObjectU5BU5D_t115* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m17460(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2336 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))Queue_1_CopyTo_m17460_gshared)(__this, ___array, ___idx, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m17461_gshared (Queue_1_t2336 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m17461(__this, ___item, method) (( void (*) (Queue_1_t2336 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m17461_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m17462_gshared (Queue_1_t2336 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m17462(__this, ___new_size, method) (( void (*) (Queue_1_t2336 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m17462_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m17463_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m17463(__this, method) (( int32_t (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_get_Count_m17463_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2337  Queue_1_GetEnumerator_m17464_gshared (Queue_1_t2336 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m17464(__this, method) (( Enumerator_t2337  (*) (Queue_1_t2336 *, const MethodInfo*))Queue_1_GetEnumerator_m17464_gshared)(__this, method)
