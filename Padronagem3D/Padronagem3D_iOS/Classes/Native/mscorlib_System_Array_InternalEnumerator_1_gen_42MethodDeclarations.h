﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m14147(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2096 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11054_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14148(__this, method) (( void (*) (InternalEnumerator_1_t2096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11056_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14149(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11058_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m14150(__this, method) (( void (*) (InternalEnumerator_1_t2096 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11060_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14151(__this, method) (( bool (*) (InternalEnumerator_1_t2096 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11062_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m14152(__this, method) (( DisallowMultipleComponent_t337 * (*) (InternalEnumerator_1_t2096 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11064_gshared)(__this, method)
