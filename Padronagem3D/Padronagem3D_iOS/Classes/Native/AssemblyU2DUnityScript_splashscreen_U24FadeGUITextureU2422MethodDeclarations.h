﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// splashscreen/$FadeGUITexture$22
struct U24FadeGUITextureU2422_t142;
// UnityEngine.GUITexture
struct GUITexture_t141;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Fade.h"

// System.Void splashscreen/$FadeGUITexture$22::.ctor(UnityEngine.GUITexture,System.Single,Fade)
extern "C" void U24FadeGUITextureU2422__ctor_m643 (U24FadeGUITextureU2422_t142 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> splashscreen/$FadeGUITexture$22::GetEnumerator()
extern "C" Object_t* U24FadeGUITextureU2422_GetEnumerator_m644 (U24FadeGUITextureU2422_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
