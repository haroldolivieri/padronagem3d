﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardboardHead
struct CardboardHead_t5;

#include "AssemblyU2DCSharp_Teleport.h"

// TeleportLegacyUI
struct  TeleportLegacyUI_t4  : public Teleport_t1
{
	// CardboardHead TeleportLegacyUI::head
	CardboardHead_t5 * ___head_3;
};
