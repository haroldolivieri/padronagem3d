﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t1951;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m12267_gshared (DefaultComparer_t1951 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12267(__this, method) (( void (*) (DefaultComparer_t1951 *, const MethodInfo*))DefaultComparer__ctor_m12267_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m12268_gshared (DefaultComparer_t1951 * __this, Vector3_t3  ___x, Vector3_t3  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m12268(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1951 *, Vector3_t3 , Vector3_t3 , const MethodInfo*))DefaultComparer_Compare_m12268_gshared)(__this, ___x, ___y, method)
