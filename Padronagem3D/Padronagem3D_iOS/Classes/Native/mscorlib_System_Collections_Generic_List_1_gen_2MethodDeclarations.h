﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t417;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t714;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t2476;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t2477;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t1944;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t299;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t1949;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t1952;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m12129_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1__ctor_m12129(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1__ctor_m12129_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12130_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12130(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1__ctor_m12130_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12131_gshared (List_1_t417 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12131(__this, ___capacity, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1__ctor_m12131_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m12132_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12132(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12132_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12133_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12133(__this, method) (( Object_t* (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12133_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12134_gshared (List_1_t417 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12134(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t417 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12134_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12135_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12135(__this, method) (( Object_t * (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12135_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12136_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12136(__this, ___item, method) (( int32_t (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12136_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12137_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12137(__this, ___item, method) (( bool (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12137_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12138_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12138(__this, ___item, method) (( int32_t (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12138_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12139_gshared (List_1_t417 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12139(__this, ___index, ___item, method) (( void (*) (List_1_t417 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12139_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12140_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12140(__this, ___item, method) (( void (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12140_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12141_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12141(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12141_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12142_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12142(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12142_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12143_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12143(__this, method) (( Object_t * (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12143_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12144_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12144(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12145_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12145(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12145_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12146_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12146(__this, ___index, method) (( Object_t * (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12146_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12147_gshared (List_1_t417 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12147(__this, ___index, ___value, method) (( void (*) (List_1_t417 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12147_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m12148_gshared (List_1_t417 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define List_1_Add_m12148(__this, ___item, method) (( void (*) (List_1_t417 *, Vector3_t3 , const MethodInfo*))List_1_Add_m12148_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12149_gshared (List_1_t417 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12149(__this, ___newCount, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12149_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12150_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12150(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12150_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12151_gshared (List_1_t417 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12151(__this, ___enumerable, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12151_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3765_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3765(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddRange_m3765_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1944 * List_1_AsReadOnly_m12152_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12152(__this, method) (( ReadOnlyCollection_1_t1944 * (*) (List_1_t417 *, const MethodInfo*))List_1_AsReadOnly_m12152_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m12153_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Clear_m12153(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Clear_m12153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m12154_gshared (List_1_t417 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define List_1_Contains_m12154(__this, ___item, method) (( bool (*) (List_1_t417 *, Vector3_t3 , const MethodInfo*))List_1_Contains_m12154_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12155_gshared (List_1_t417 * __this, Vector3U5BU5D_t299* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12155(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t417 *, Vector3U5BU5D_t299*, int32_t, const MethodInfo*))List_1_CopyTo_m12155_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t3  List_1_Find_m12156_gshared (List_1_t417 * __this, Predicate_1_t1949 * ___match, const MethodInfo* method);
#define List_1_Find_m12156(__this, ___match, method) (( Vector3_t3  (*) (List_1_t417 *, Predicate_1_t1949 *, const MethodInfo*))List_1_Find_m12156_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12157_gshared (Object_t * __this /* static, unused */, Predicate_1_t1949 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12157(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1949 *, const MethodInfo*))List_1_CheckMatch_m12157_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12158_gshared (List_1_t417 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1949 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12158(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t417 *, int32_t, int32_t, Predicate_1_t1949 *, const MethodInfo*))List_1_GetIndex_m12158_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t1943  List_1_GetEnumerator_m12159_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12159(__this, method) (( Enumerator_t1943  (*) (List_1_t417 *, const MethodInfo*))List_1_GetEnumerator_m12159_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12160_gshared (List_1_t417 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12160(__this, ___item, method) (( int32_t (*) (List_1_t417 *, Vector3_t3 , const MethodInfo*))List_1_IndexOf_m12160_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12161_gshared (List_1_t417 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12161(__this, ___start, ___delta, method) (( void (*) (List_1_t417 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12161_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12162_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12162(__this, ___index, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12162_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12163_gshared (List_1_t417 * __this, int32_t ___index, Vector3_t3  ___item, const MethodInfo* method);
#define List_1_Insert_m12163(__this, ___index, ___item, method) (( void (*) (List_1_t417 *, int32_t, Vector3_t3 , const MethodInfo*))List_1_Insert_m12163_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12164_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12164(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12164_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m12165_gshared (List_1_t417 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define List_1_Remove_m12165(__this, ___item, method) (( bool (*) (List_1_t417 *, Vector3_t3 , const MethodInfo*))List_1_Remove_m12165_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12166_gshared (List_1_t417 * __this, Predicate_1_t1949 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12166(__this, ___match, method) (( int32_t (*) (List_1_t417 *, Predicate_1_t1949 *, const MethodInfo*))List_1_RemoveAll_m12166_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12167_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12167(__this, ___index, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12167_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m12168_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Reverse_m12168(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Reverse_m12168_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m12169_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Sort_m12169(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Sort_m12169_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12170_gshared (List_1_t417 * __this, Comparison_1_t1952 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12170(__this, ___comparison, method) (( void (*) (List_1_t417 *, Comparison_1_t1952 *, const MethodInfo*))List_1_Sort_m12170_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t299* List_1_ToArray_m12171_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_ToArray_m12171(__this, method) (( Vector3U5BU5D_t299* (*) (List_1_t417 *, const MethodInfo*))List_1_ToArray_m12171_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m12172_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12172(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_TrimExcess_m12172_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12173_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12173(__this, method) (( int32_t (*) (List_1_t417 *, const MethodInfo*))List_1_get_Capacity_m12173_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12174_gshared (List_1_t417 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12174(__this, ___value, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12174_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m12175_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_get_Count_m12175(__this, method) (( int32_t (*) (List_1_t417 *, const MethodInfo*))List_1_get_Count_m12175_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t3  List_1_get_Item_m12176_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12176(__this, ___index, method) (( Vector3_t3  (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_get_Item_m12176_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12177_gshared (List_1_t417 * __this, int32_t ___index, Vector3_t3  ___value, const MethodInfo* method);
#define List_1_set_Item_m12177(__this, ___index, ___value, method) (( void (*) (List_1_t417 *, int32_t, Vector3_t3 , const MethodInfo*))List_1_set_Item_m12177_gshared)(__this, ___index, ___value, method)
