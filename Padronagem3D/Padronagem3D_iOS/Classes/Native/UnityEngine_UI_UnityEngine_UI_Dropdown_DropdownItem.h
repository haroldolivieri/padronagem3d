﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t548;
// UnityEngine.UI.Image
struct Image_t549;
// UnityEngine.RectTransform
struct RectTransform_t211;
// UnityEngine.UI.Toggle
struct Toggle_t550;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityEngine.UI.Dropdown/DropdownItem
struct  DropdownItem_t547  : public MonoBehaviour_t2
{
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::m_Text
	Text_t548 * ___m_Text_2;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::m_Image
	Image_t549 * ___m_Image_3;
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::m_RectTransform
	RectTransform_t211 * ___m_RectTransform_4;
	// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::m_Toggle
	Toggle_t550 * ___m_Toggle_5;
};
