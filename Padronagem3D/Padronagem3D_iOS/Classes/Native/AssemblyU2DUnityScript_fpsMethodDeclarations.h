﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// fps
struct fps_t132;

#include "codegen/il2cpp-codegen.h"

// System.Void fps::.ctor()
extern "C" void fps__ctor_m633 (fps_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void fps::Start()
extern "C" void fps_Start_m634 (fps_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void fps::Update()
extern "C" void fps_Update_m635 (fps_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void fps::Main()
extern "C" void fps_Main_m636 (fps_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
