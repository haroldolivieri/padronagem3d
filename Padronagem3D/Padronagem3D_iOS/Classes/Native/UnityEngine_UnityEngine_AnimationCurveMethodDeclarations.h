﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationCurve
struct AnimationCurve_t270;
struct AnimationCurve_t270_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t429;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m1488 (AnimationCurve_t270 * __this, KeyframeU5BU5D_t429* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m1489 (AnimationCurve_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m1490 (AnimationCurve_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m1491 (AnimationCurve_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m1492 (AnimationCurve_t270 * __this, KeyframeU5BU5D_t429* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t270_marshal(const AnimationCurve_t270& unmarshaled, AnimationCurve_t270_marshaled& marshaled);
extern "C" void AnimationCurve_t270_marshal_back(const AnimationCurve_t270_marshaled& marshaled, AnimationCurve_t270& unmarshaled);
extern "C" void AnimationCurve_t270_marshal_cleanup(AnimationCurve_t270_marshaled& marshaled);
