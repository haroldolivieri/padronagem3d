﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t674;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"

// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_startValue()
extern "C" float FloatTween_get_startValue_m2498 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_startValue(System.Single)
extern "C" void FloatTween_set_startValue_m2499 (FloatTween_t535 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_targetValue()
extern "C" float FloatTween_get_targetValue_m2500 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_targetValue(System.Single)
extern "C" void FloatTween_set_targetValue_m2501 (FloatTween_t535 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
extern "C" float FloatTween_get_duration_m2502 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_duration(System.Single)
extern "C" void FloatTween_set_duration_m2503 (FloatTween_t535 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
extern "C" bool FloatTween_get_ignoreTimeScale_m2504 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_ignoreTimeScale(System.Boolean)
extern "C" void FloatTween_set_ignoreTimeScale_m2505 (FloatTween_t535 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
extern "C" void FloatTween_TweenValue_m2506 (FloatTween_t535 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<System.Single>)
extern "C" void FloatTween_AddOnChangedCallback_m2507 (FloatTween_t535 * __this, UnityAction_1_t674 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::GetIgnoreTimescale()
extern "C" bool FloatTween_GetIgnoreTimescale_m2508 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::GetDuration()
extern "C" float FloatTween_GetDuration_m2509 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
extern "C" bool FloatTween_ValidTarget_m2510 (FloatTween_t535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
