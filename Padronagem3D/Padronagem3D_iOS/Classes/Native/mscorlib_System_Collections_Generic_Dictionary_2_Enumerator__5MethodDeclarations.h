﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14127(__this, ___dictionary, method) (( void (*) (Enumerator_t2095 *, Dictionary_2_t325 *, const MethodInfo*))Enumerator__ctor_m14071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14128(__this, method) (( Object_t * (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14129(__this, method) (( void (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14130(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14131(__this, method) (( Object_t * (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14132(__this, method) (( Object_t * (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m14133(__this, method) (( bool (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_MoveNext_m14077_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m14134(__this, method) (( KeyValuePair_2_t2094  (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_get_Current_m14078_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14135(__this, method) (( String_t* (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_get_CurrentKey_m14079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14136(__this, method) (( GUIStyle_t315 * (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_get_CurrentValue_m14080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Reset()
#define Enumerator_Reset_m14137(__this, method) (( void (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_Reset_m14081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m14138(__this, method) (( void (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_VerifyState_m14082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14139(__this, method) (( void (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_VerifyCurrent_m14083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m14140(__this, method) (( void (*) (Enumerator_t2095 *, const MethodInfo*))Enumerator_Dispose_m14084_gshared)(__this, method)
