﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t1946;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t299;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t2476;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t1945;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m12215_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12215(__this, method) (( void (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1__ctor_m12215_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12216_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12216(__this, method) (( bool (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12216_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12217_gshared (Collection_1_t1946 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12217(__this, ___array, ___index, method) (( void (*) (Collection_1_t1946 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12217_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12218_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12218(__this, method) (( Object_t * (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12218_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12219_gshared (Collection_1_t1946 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12219(__this, ___value, method) (( int32_t (*) (Collection_1_t1946 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12219_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12220_gshared (Collection_1_t1946 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12220(__this, ___value, method) (( bool (*) (Collection_1_t1946 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12220_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12221_gshared (Collection_1_t1946 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12221(__this, ___value, method) (( int32_t (*) (Collection_1_t1946 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12221_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12222_gshared (Collection_1_t1946 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12222(__this, ___index, ___value, method) (( void (*) (Collection_1_t1946 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12222_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12223_gshared (Collection_1_t1946 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12223(__this, ___value, method) (( void (*) (Collection_1_t1946 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12223_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12224_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12224(__this, method) (( bool (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12224_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12225_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12225(__this, method) (( Object_t * (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12225_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12226_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12226(__this, method) (( bool (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12226_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12227_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12227(__this, method) (( bool (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12227_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12228_gshared (Collection_1_t1946 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12228(__this, ___index, method) (( Object_t * (*) (Collection_1_t1946 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12228_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12229_gshared (Collection_1_t1946 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12229(__this, ___index, ___value, method) (( void (*) (Collection_1_t1946 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12229_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m12230_gshared (Collection_1_t1946 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_Add_m12230(__this, ___item, method) (( void (*) (Collection_1_t1946 *, Vector3_t3 , const MethodInfo*))Collection_1_Add_m12230_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m12231_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12231(__this, method) (( void (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_Clear_m12231_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m12232_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12232(__this, method) (( void (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_ClearItems_m12232_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m12233_gshared (Collection_1_t1946 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12233(__this, ___item, method) (( bool (*) (Collection_1_t1946 *, Vector3_t3 , const MethodInfo*))Collection_1_Contains_m12233_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12234_gshared (Collection_1_t1946 * __this, Vector3U5BU5D_t299* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12234(__this, ___array, ___index, method) (( void (*) (Collection_1_t1946 *, Vector3U5BU5D_t299*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12234_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12235_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12235(__this, method) (( Object_t* (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_GetEnumerator_m12235_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12236_gshared (Collection_1_t1946 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12236(__this, ___item, method) (( int32_t (*) (Collection_1_t1946 *, Vector3_t3 , const MethodInfo*))Collection_1_IndexOf_m12236_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12237_gshared (Collection_1_t1946 * __this, int32_t ___index, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12237(__this, ___index, ___item, method) (( void (*) (Collection_1_t1946 *, int32_t, Vector3_t3 , const MethodInfo*))Collection_1_Insert_m12237_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12238_gshared (Collection_1_t1946 * __this, int32_t ___index, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12238(__this, ___index, ___item, method) (( void (*) (Collection_1_t1946 *, int32_t, Vector3_t3 , const MethodInfo*))Collection_1_InsertItem_m12238_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m12239_gshared (Collection_1_t1946 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12239(__this, ___item, method) (( bool (*) (Collection_1_t1946 *, Vector3_t3 , const MethodInfo*))Collection_1_Remove_m12239_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12240_gshared (Collection_1_t1946 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12240(__this, ___index, method) (( void (*) (Collection_1_t1946 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12240_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12241_gshared (Collection_1_t1946 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12241(__this, ___index, method) (( void (*) (Collection_1_t1946 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12241_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12242_gshared (Collection_1_t1946 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12242(__this, method) (( int32_t (*) (Collection_1_t1946 *, const MethodInfo*))Collection_1_get_Count_m12242_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t3  Collection_1_get_Item_m12243_gshared (Collection_1_t1946 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12243(__this, ___index, method) (( Vector3_t3  (*) (Collection_1_t1946 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12243_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12244_gshared (Collection_1_t1946 * __this, int32_t ___index, Vector3_t3  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12244(__this, ___index, ___value, method) (( void (*) (Collection_1_t1946 *, int32_t, Vector3_t3 , const MethodInfo*))Collection_1_set_Item_m12244_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12245_gshared (Collection_1_t1946 * __this, int32_t ___index, Vector3_t3  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12245(__this, ___index, ___item, method) (( void (*) (Collection_1_t1946 *, int32_t, Vector3_t3 , const MethodInfo*))Collection_1_SetItem_m12245_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12246_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12246(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12246_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t3  Collection_1_ConvertItem_m12247_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12247(__this /* static, unused */, ___item, method) (( Vector3_t3  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12247_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12248_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12248(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12248_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12249_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12249(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12249_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12250_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12250(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12250_gshared)(__this /* static, unused */, ___list, method)
