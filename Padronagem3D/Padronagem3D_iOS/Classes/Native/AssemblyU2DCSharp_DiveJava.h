﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DiveJava
struct DiveJava_t60;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// DiveJava
struct  DiveJava_t60  : public MonoBehaviour_t2
{
	// System.String DiveJava::cacheDir
	String_t* ___cacheDir_2;
	// System.String DiveJava::startURI
	String_t* ___startURI_3;
	// System.Single DiveJava::time_since_last_fullscreen
	float ___time_since_last_fullscreen_5;
};
struct DiveJava_t60_StaticFields{
	// System.Int32 DiveJava::start_once
	int32_t ___start_once_4;
	// DiveJava DiveJava::instance
	DiveJava_t60 * ___instance_6;
	// System.Boolean DiveJava::initiated
	bool ___initiated_7;
};
