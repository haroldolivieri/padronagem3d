﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::.cctor()
#define ListPool_1__cctor_m11713(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m11714_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Component>::Get()
#define ListPool_1_Get_m3656(__this /* static, unused */, method) (( List_1_t426 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m11715_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m3657(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t426 *, const MethodInfo*))ListPool_1_Release_m11716_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
#define ListPool_1_U3Cs_ListPoolU3Em__15_m11717(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t426 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m11718_gshared)(__this /* static, unused */, ___l, method)
