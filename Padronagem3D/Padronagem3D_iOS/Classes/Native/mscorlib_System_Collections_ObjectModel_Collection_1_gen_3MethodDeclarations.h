﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>
struct Collection_1_t1966;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t420;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t2482;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t1965;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::.ctor()
extern "C" void Collection_1__ctor_m12503_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12503(__this, method) (( void (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1__ctor_m12503_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12504_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12504(__this, method) (( bool (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12504_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12505_gshared (Collection_1_t1966 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12505(__this, ___array, ___index, method) (( void (*) (Collection_1_t1966 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12505_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12506_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12506(__this, method) (( Object_t * (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12506_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12507_gshared (Collection_1_t1966 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12507(__this, ___value, method) (( int32_t (*) (Collection_1_t1966 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12507_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12508_gshared (Collection_1_t1966 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12508(__this, ___value, method) (( bool (*) (Collection_1_t1966 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12508_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12509_gshared (Collection_1_t1966 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12509(__this, ___value, method) (( int32_t (*) (Collection_1_t1966 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12509_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12510_gshared (Collection_1_t1966 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12510(__this, ___index, ___value, method) (( void (*) (Collection_1_t1966 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12510_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12511_gshared (Collection_1_t1966 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12511(__this, ___value, method) (( void (*) (Collection_1_t1966 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12511_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12512_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12512(__this, method) (( bool (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12512_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12513_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12513(__this, method) (( Object_t * (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12514_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12514(__this, method) (( bool (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12514_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12515_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12515(__this, method) (( bool (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12515_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12516_gshared (Collection_1_t1966 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12516(__this, ___index, method) (( Object_t * (*) (Collection_1_t1966 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12516_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12517_gshared (Collection_1_t1966 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12517(__this, ___index, ___value, method) (( void (*) (Collection_1_t1966 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12517_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Add(T)
extern "C" void Collection_1_Add_m12518_gshared (Collection_1_t1966 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_Add_m12518(__this, ___item, method) (( void (*) (Collection_1_t1966 *, Vector2_t15 , const MethodInfo*))Collection_1_Add_m12518_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Clear()
extern "C" void Collection_1_Clear_m12519_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12519(__this, method) (( void (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_Clear_m12519_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ClearItems()
extern "C" void Collection_1_ClearItems_m12520_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12520(__this, method) (( void (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_ClearItems_m12520_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool Collection_1_Contains_m12521_gshared (Collection_1_t1966 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12521(__this, ___item, method) (( bool (*) (Collection_1_t1966 *, Vector2_t15 , const MethodInfo*))Collection_1_Contains_m12521_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12522_gshared (Collection_1_t1966 * __this, Vector2U5BU5D_t420* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12522(__this, ___array, ___index, method) (( void (*) (Collection_1_t1966 *, Vector2U5BU5D_t420*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12522_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12523_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12523(__this, method) (( Object_t* (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_GetEnumerator_m12523_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12524_gshared (Collection_1_t1966 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12524(__this, ___item, method) (( int32_t (*) (Collection_1_t1966 *, Vector2_t15 , const MethodInfo*))Collection_1_IndexOf_m12524_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12525_gshared (Collection_1_t1966 * __this, int32_t ___index, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12525(__this, ___index, ___item, method) (( void (*) (Collection_1_t1966 *, int32_t, Vector2_t15 , const MethodInfo*))Collection_1_Insert_m12525_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12526_gshared (Collection_1_t1966 * __this, int32_t ___index, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12526(__this, ___index, ___item, method) (( void (*) (Collection_1_t1966 *, int32_t, Vector2_t15 , const MethodInfo*))Collection_1_InsertItem_m12526_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool Collection_1_Remove_m12527_gshared (Collection_1_t1966 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12527(__this, ___item, method) (( bool (*) (Collection_1_t1966 *, Vector2_t15 , const MethodInfo*))Collection_1_Remove_m12527_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12528_gshared (Collection_1_t1966 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12528(__this, ___index, method) (( void (*) (Collection_1_t1966 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12528_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12529_gshared (Collection_1_t1966 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12529(__this, ___index, method) (( void (*) (Collection_1_t1966 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12529_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12530_gshared (Collection_1_t1966 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12530(__this, method) (( int32_t (*) (Collection_1_t1966 *, const MethodInfo*))Collection_1_get_Count_m12530_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t15  Collection_1_get_Item_m12531_gshared (Collection_1_t1966 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12531(__this, ___index, method) (( Vector2_t15  (*) (Collection_1_t1966 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12531_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12532_gshared (Collection_1_t1966 * __this, int32_t ___index, Vector2_t15  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12532(__this, ___index, ___value, method) (( void (*) (Collection_1_t1966 *, int32_t, Vector2_t15 , const MethodInfo*))Collection_1_set_Item_m12532_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12533_gshared (Collection_1_t1966 * __this, int32_t ___index, Vector2_t15  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12533(__this, ___index, ___item, method) (( void (*) (Collection_1_t1966 *, int32_t, Vector2_t15 , const MethodInfo*))Collection_1_SetItem_m12533_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12534_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12534(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12534_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ConvertItem(System.Object)
extern "C" Vector2_t15  Collection_1_ConvertItem_m12535_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12535(__this /* static, unused */, ___item, method) (( Vector2_t15  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12535_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12536_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12536(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12536_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12537_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12537(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12537_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12538_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12538(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12538_gshared)(__this /* static, unused */, ___list, method)
