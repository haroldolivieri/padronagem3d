﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_DiveMouseLook_RotationAxes.h"

// DiveMouseLook
struct  DiveMouseLook_t62  : public MonoBehaviour_t2
{
	// DiveMouseLook/RotationAxes DiveMouseLook::axes
	int32_t ___axes_2;
	// System.Single DiveMouseLook::sensitivityX
	float ___sensitivityX_3;
	// System.Single DiveMouseLook::sensitivityY
	float ___sensitivityY_4;
	// System.Single DiveMouseLook::minimumX
	float ___minimumX_5;
	// System.Single DiveMouseLook::maximumX
	float ___maximumX_6;
	// System.Single DiveMouseLook::minimumY
	float ___minimumY_7;
	// System.Single DiveMouseLook::maximumY
	float ___maximumY_8;
	// System.Single DiveMouseLook::rotationY
	float ___rotationY_9;
	// System.Boolean DiveMouseLook::mouse_on
	bool ___mouse_on_10;
};
