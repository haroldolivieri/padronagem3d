﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t2208;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m15691_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m15691(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m15691_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15692_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15692(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15692_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15693_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15693(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15693_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m15694_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m15694(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m15694_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m15695_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m15695(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m15695_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m15696_gshared (U3CStartU3Ec__Iterator0_t2208 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m15696(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2208 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m15696_gshared)(__this, method)
