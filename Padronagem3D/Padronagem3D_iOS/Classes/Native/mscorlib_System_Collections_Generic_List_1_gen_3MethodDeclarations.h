﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t419;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector4>
struct IEnumerable_1_t2478;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t2479;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector4>
struct ICollection_1_t2480;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>
struct ReadOnlyCollection_1_t1954;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t418;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t1959;
// System.Comparison`1<UnityEngine.Vector4>
struct Comparison_1_t1962;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
extern "C" void List_1__ctor_m12273_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1__ctor_m12273(__this, method) (( void (*) (List_1_t419 *, const MethodInfo*))List_1__ctor_m12273_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12274_gshared (List_1_t419 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12274(__this, ___collection, method) (( void (*) (List_1_t419 *, Object_t*, const MethodInfo*))List_1__ctor_m12274_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12275_gshared (List_1_t419 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12275(__this, ___capacity, method) (( void (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1__ctor_m12275_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.cctor()
extern "C" void List_1__cctor_m12276_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12276(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12276_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12277_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12277(__this, method) (( Object_t* (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12277_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12278_gshared (List_1_t419 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12278(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t419 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12278_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12279_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12279(__this, method) (( Object_t * (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12279_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12280_gshared (List_1_t419 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12280(__this, ___item, method) (( int32_t (*) (List_1_t419 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12280_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12281_gshared (List_1_t419 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12281(__this, ___item, method) (( bool (*) (List_1_t419 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12281_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12282_gshared (List_1_t419 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12282(__this, ___item, method) (( int32_t (*) (List_1_t419 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12282_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12283_gshared (List_1_t419 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12283(__this, ___index, ___item, method) (( void (*) (List_1_t419 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12283_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12284_gshared (List_1_t419 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12284(__this, ___item, method) (( void (*) (List_1_t419 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12284_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12285_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12285(__this, method) (( bool (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12286_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12286(__this, method) (( bool (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12286_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12287_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12287(__this, method) (( Object_t * (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12287_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12288_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12288(__this, method) (( bool (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12289_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12289(__this, method) (( bool (*) (List_1_t419 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12289_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12290_gshared (List_1_t419 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12290(__this, ___index, method) (( Object_t * (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12290_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12291_gshared (List_1_t419 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12291(__this, ___index, ___value, method) (( void (*) (List_1_t419 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12291_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(T)
extern "C" void List_1_Add_m12292_gshared (List_1_t419 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define List_1_Add_m12292(__this, ___item, method) (( void (*) (List_1_t419 *, Vector4_t90 , const MethodInfo*))List_1_Add_m12292_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12293_gshared (List_1_t419 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12293(__this, ___newCount, method) (( void (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12293_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12294_gshared (List_1_t419 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12294(__this, ___collection, method) (( void (*) (List_1_t419 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12294_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12295_gshared (List_1_t419 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12295(__this, ___enumerable, method) (( void (*) (List_1_t419 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12295_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3768_gshared (List_1_t419 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3768(__this, ___collection, method) (( void (*) (List_1_t419 *, Object_t*, const MethodInfo*))List_1_AddRange_m3768_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1954 * List_1_AsReadOnly_m12296_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12296(__this, method) (( ReadOnlyCollection_1_t1954 * (*) (List_1_t419 *, const MethodInfo*))List_1_AsReadOnly_m12296_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear()
extern "C" void List_1_Clear_m12297_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_Clear_m12297(__this, method) (( void (*) (List_1_t419 *, const MethodInfo*))List_1_Clear_m12297_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool List_1_Contains_m12298_gshared (List_1_t419 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define List_1_Contains_m12298(__this, ___item, method) (( bool (*) (List_1_t419 *, Vector4_t90 , const MethodInfo*))List_1_Contains_m12298_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12299_gshared (List_1_t419 * __this, Vector4U5BU5D_t418* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12299(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t419 *, Vector4U5BU5D_t418*, int32_t, const MethodInfo*))List_1_CopyTo_m12299_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::Find(System.Predicate`1<T>)
extern "C" Vector4_t90  List_1_Find_m12300_gshared (List_1_t419 * __this, Predicate_1_t1959 * ___match, const MethodInfo* method);
#define List_1_Find_m12300(__this, ___match, method) (( Vector4_t90  (*) (List_1_t419 *, Predicate_1_t1959 *, const MethodInfo*))List_1_Find_m12300_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12301_gshared (Object_t * __this /* static, unused */, Predicate_1_t1959 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12301(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1959 *, const MethodInfo*))List_1_CheckMatch_m12301_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12302_gshared (List_1_t419 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1959 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12302(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t419 *, int32_t, int32_t, Predicate_1_t1959 *, const MethodInfo*))List_1_GetIndex_m12302_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Enumerator_t1953  List_1_GetEnumerator_m12303_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12303(__this, method) (( Enumerator_t1953  (*) (List_1_t419 *, const MethodInfo*))List_1_GetEnumerator_m12303_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12304_gshared (List_1_t419 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12304(__this, ___item, method) (( int32_t (*) (List_1_t419 *, Vector4_t90 , const MethodInfo*))List_1_IndexOf_m12304_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12305_gshared (List_1_t419 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12305(__this, ___start, ___delta, method) (( void (*) (List_1_t419 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12305_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12306_gshared (List_1_t419 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12306(__this, ___index, method) (( void (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12306_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12307_gshared (List_1_t419 * __this, int32_t ___index, Vector4_t90  ___item, const MethodInfo* method);
#define List_1_Insert_m12307(__this, ___index, ___item, method) (( void (*) (List_1_t419 *, int32_t, Vector4_t90 , const MethodInfo*))List_1_Insert_m12307_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12308_gshared (List_1_t419 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12308(__this, ___collection, method) (( void (*) (List_1_t419 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12308_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool List_1_Remove_m12309_gshared (List_1_t419 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define List_1_Remove_m12309(__this, ___item, method) (( bool (*) (List_1_t419 *, Vector4_t90 , const MethodInfo*))List_1_Remove_m12309_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12310_gshared (List_1_t419 * __this, Predicate_1_t1959 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12310(__this, ___match, method) (( int32_t (*) (List_1_t419 *, Predicate_1_t1959 *, const MethodInfo*))List_1_RemoveAll_m12310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12311_gshared (List_1_t419 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12311(__this, ___index, method) (( void (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12311_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Reverse()
extern "C" void List_1_Reverse_m12312_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_Reverse_m12312(__this, method) (( void (*) (List_1_t419 *, const MethodInfo*))List_1_Reverse_m12312_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort()
extern "C" void List_1_Sort_m12313_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_Sort_m12313(__this, method) (( void (*) (List_1_t419 *, const MethodInfo*))List_1_Sort_m12313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12314_gshared (List_1_t419 * __this, Comparison_1_t1962 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12314(__this, ___comparison, method) (( void (*) (List_1_t419 *, Comparison_1_t1962 *, const MethodInfo*))List_1_Sort_m12314_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector4>::ToArray()
extern "C" Vector4U5BU5D_t418* List_1_ToArray_m12315_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_ToArray_m12315(__this, method) (( Vector4U5BU5D_t418* (*) (List_1_t419 *, const MethodInfo*))List_1_ToArray_m12315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::TrimExcess()
extern "C" void List_1_TrimExcess_m12316_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12316(__this, method) (( void (*) (List_1_t419 *, const MethodInfo*))List_1_TrimExcess_m12316_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12317_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12317(__this, method) (( int32_t (*) (List_1_t419 *, const MethodInfo*))List_1_get_Capacity_m12317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12318_gshared (List_1_t419 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12318(__this, ___value, method) (( void (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12318_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t List_1_get_Count_m12319_gshared (List_1_t419 * __this, const MethodInfo* method);
#define List_1_get_Count_m12319(__this, method) (( int32_t (*) (List_1_t419 *, const MethodInfo*))List_1_get_Count_m12319_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t90  List_1_get_Item_m12320_gshared (List_1_t419 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12320(__this, ___index, method) (( Vector4_t90  (*) (List_1_t419 *, int32_t, const MethodInfo*))List_1_get_Item_m12320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12321_gshared (List_1_t419 * __this, int32_t ___index, Vector4_t90  ___value, const MethodInfo* method);
#define List_1_set_Item_m12321(__this, ___index, ___value, method) (( void (*) (List_1_t419 *, int32_t, Vector4_t90 , const MethodInfo*))List_1_set_Item_m12321_gshared)(__this, ___index, ___value, method)
