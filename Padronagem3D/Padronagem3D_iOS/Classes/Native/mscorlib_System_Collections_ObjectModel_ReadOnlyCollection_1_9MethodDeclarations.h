﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t1944;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t1945;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t299;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t2476;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m12185_gshared (ReadOnlyCollection_1_t1944 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m12185(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1944 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m12185_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12186_gshared (ReadOnlyCollection_1_t1944 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12186(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1944 *, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12186_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12187_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12187(__this, method) (( void (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12187_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12188_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, Vector3_t3  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12188(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12188_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12189_gshared (ReadOnlyCollection_1_t1944 * __this, Vector3_t3  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12189(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12189_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12190_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12190(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12190_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Vector3_t3  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12191_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12191(__this, ___index, method) (( Vector3_t3  (*) (ReadOnlyCollection_1_t1944 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12191_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12192_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, Vector3_t3  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12192(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12192_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12193_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12193(__this, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12193_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12194_gshared (ReadOnlyCollection_1_t1944 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12194(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1944 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12194_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12195_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12195(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12195_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m12196_gshared (ReadOnlyCollection_1_t1944 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m12196(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1944 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m12196_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m12197_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m12197(__this, method) (( void (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m12197_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m12198_gshared (ReadOnlyCollection_1_t1944 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m12198(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m12198_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12199_gshared (ReadOnlyCollection_1_t1944 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12199(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1944 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12199_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m12200_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m12200(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m12200_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m12201_gshared (ReadOnlyCollection_1_t1944 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m12201(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1944 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m12201_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12202_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12202(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12202_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12203_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12203(__this, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12203_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12204_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12204(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12204_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12205_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12205(__this, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12205_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12206_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12206(__this, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12206_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m12207_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m12207(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1944 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m12207_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m12208_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m12208(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1944 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m12208_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m12209_gshared (ReadOnlyCollection_1_t1944 * __this, Vector3_t3  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m12209(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1944 *, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_Contains_m12209_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m12210_gshared (ReadOnlyCollection_1_t1944 * __this, Vector3U5BU5D_t299* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m12210(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1944 *, Vector3U5BU5D_t299*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m12210_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m12211_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m12211(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m12211_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m12212_gshared (ReadOnlyCollection_1_t1944 * __this, Vector3_t3  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m12212(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1944 *, Vector3_t3 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m12212_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m12213_gshared (ReadOnlyCollection_1_t1944 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m12213(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1944 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m12213_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t3  ReadOnlyCollection_1_get_Item_m12214_gshared (ReadOnlyCollection_1_t1944 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m12214(__this, ___index, method) (( Vector3_t3  (*) (ReadOnlyCollection_1_t1944 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m12214_gshared)(__this, ___index, method)
