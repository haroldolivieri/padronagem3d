﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
#define Dictionary_2__ctor_m14290(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2__ctor_m14291_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14292(__this, ___comparer, method) (( void (*) (Dictionary_2_t389 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14293_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m14294(__this, ___capacity, method) (( void (*) (Dictionary_2_t389 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14295_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m14296(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t389 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m14297_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14298(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14299_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14300(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14301_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m14302(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14303_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m14304(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14305_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m14306(__this, ___key, method) (( void (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14307_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14308(__this, method) (( bool (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14309_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14310(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14312(__this, method) (( bool (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14314(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2_t2125 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14315_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14316(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t2125 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14317_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14318(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2U5BU5D_t2507*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14319_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14320(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t2125 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14321_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14322(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14323_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14324(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14325_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14326(__this, method) (( Object_t* (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14327_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14328(__this, method) (( Object_t * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14329_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Count()
#define Dictionary_2_get_Count_m14330(__this, method) (( int32_t (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_get_Count_m14331_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
#define Dictionary_2_get_Item_m14332(__this, ___key, method) (( int32_t (*) (Dictionary_2_t389 *, Event_t84 *, const MethodInfo*))Dictionary_2_get_Item_m14333_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m14334(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Event_t84 *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m14335_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m14336(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t389 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14337_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m14338(__this, ___size, method) (( void (*) (Dictionary_2_t389 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14339_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m14340(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14341_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m14342(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2125  (*) (Object_t * /* static, unused */, Event_t84 *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m14343_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m14344(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Event_t84 *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m14345_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m14346(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t389 *, KeyValuePair_2U5BU5D_t2507*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14347_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Resize()
#define Dictionary_2_Resize_m14348(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_Resize_m14349_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
#define Dictionary_2_Add_m14350(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t389 *, Event_t84 *, int32_t, const MethodInfo*))Dictionary_2_Add_m14351_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Clear()
#define Dictionary_2_Clear_m14352(__this, method) (( void (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_Clear_m14353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m14354(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, Event_t84 *, const MethodInfo*))Dictionary_2_ContainsKey_m14355_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m14356(__this, ___value, method) (( bool (*) (Dictionary_2_t389 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m14357_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m14358(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t389 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m14359_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m14360(__this, ___sender, method) (( void (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14361_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
#define Dictionary_2_Remove_m14362(__this, ___key, method) (( bool (*) (Dictionary_2_t389 *, Event_t84 *, const MethodInfo*))Dictionary_2_Remove_m14363_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m14364(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t389 *, Event_t84 *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m14365_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Values()
#define Dictionary_2_get_Values_m14366(__this, method) (( ValueCollection_t2126 * (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_get_Values_m14367_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m14368(__this, ___key, method) (( Event_t84 * (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14369_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m14370(__this, ___value, method) (( int32_t (*) (Dictionary_2_t389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14371_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m14372(__this, ___pair, method) (( bool (*) (Dictionary_2_t389 *, KeyValuePair_2_t2125 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14373_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m14374(__this, method) (( Enumerator_t2127  (*) (Dictionary_2_t389 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14375_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m14376(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, Event_t84 *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14377_gshared)(__this /* static, unused */, ___key, ___value, method)
