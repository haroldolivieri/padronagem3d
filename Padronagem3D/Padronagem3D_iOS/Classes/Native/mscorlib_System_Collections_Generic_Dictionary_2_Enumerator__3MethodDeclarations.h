﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13842(__this, ___dictionary, method) (( void (*) (Enumerator_t2076 *, Dictionary_2_t314 *, const MethodInfo*))Enumerator__ctor_m13785_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13843(__this, method) (( Object_t * (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m13844(__this, method) (( void (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13787_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13845(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13788_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13846(__this, method) (( Object_t * (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13789_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13847(__this, method) (( Object_t * (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13790_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m13848(__this, method) (( bool (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_MoveNext_m13791_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m13849(__this, method) (( KeyValuePair_2_t2074  (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_get_Current_m13792_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13850(__this, method) (( int32_t (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_get_CurrentKey_m13793_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13851(__this, method) (( LayoutCache_t311 * (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_get_CurrentValue_m13794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Reset()
#define Enumerator_Reset_m13852(__this, method) (( void (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_Reset_m13795_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m13853(__this, method) (( void (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_VerifyState_m13796_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13854(__this, method) (( void (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_VerifyCurrent_m13797_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m13855(__this, method) (( void (*) (Enumerator_t2076 *, const MethodInfo*))Enumerator_Dispose_m13798_gshared)(__this, method)
