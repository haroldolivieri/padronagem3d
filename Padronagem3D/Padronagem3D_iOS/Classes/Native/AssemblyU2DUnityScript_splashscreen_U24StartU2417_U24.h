﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// splashscreen
struct splashscreen_t136;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_0.h"
#include "UnityEngine_UnityEngine_Color.h"

// splashscreen/$Start$17/$
struct  U24_t134  : public GenericGeneratorEnumerator_1_t135
{
	// System.Int32 splashscreen/$Start$17/$::$$4$18
	int32_t ___U24U244U2418_2;
	// UnityEngine.Color splashscreen/$Start$17/$::$$5$19
	Color_t11  ___U24U245U2419_3;
	// splashscreen splashscreen/$Start$17/$::$self_$20
	splashscreen_t136 * ___U24self_U2420_4;
};
