﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t780;

#include "mscorlib_System_Object.h"

// System.Threading.CompressedStack
struct  CompressedStack_t1628  : public Object_t
{
	// System.Collections.ArrayList System.Threading.CompressedStack::_list
	ArrayList_t780 * ____list_0;
};
