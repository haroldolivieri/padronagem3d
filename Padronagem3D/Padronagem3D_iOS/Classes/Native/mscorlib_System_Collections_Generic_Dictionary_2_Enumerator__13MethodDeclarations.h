﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17377(__this, ___dictionary, method) (( void (*) (Enumerator_t2332 *, Dictionary_2_t720 *, const MethodInfo*))Enumerator__ctor_m14071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17378(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17379(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17380(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17381(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17382(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::MoveNext()
#define Enumerator_MoveNext_m17383(__this, method) (( bool (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_MoveNext_m14077_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::get_Current()
#define Enumerator_get_Current_m17384(__this, method) (( KeyValuePair_2_t2330  (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_Current_m14078_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17385(__this, method) (( DispatcherKey_t722 * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_CurrentKey_m14079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17386(__this, method) (( Dispatcher_t718 * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_CurrentValue_m14080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::Reset()
#define Enumerator_Reset_m17387(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_Reset_m14081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::VerifyState()
#define Enumerator_VerifyState_m17388(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_VerifyState_m14082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17389(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_VerifyCurrent_m14083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::Dispose()
#define Enumerator_Dispose_m17390(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_Dispose_m14084_gshared)(__this, method)
