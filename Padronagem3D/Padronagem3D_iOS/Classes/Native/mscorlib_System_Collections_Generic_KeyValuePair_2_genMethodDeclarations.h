﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15223(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t685 *, int32_t, PointerEventData_t48 *, const MethodInfo*))KeyValuePair_2__ctor_m13759_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
#define KeyValuePair_2_get_Key_m3592(__this, method) (( int32_t (*) (KeyValuePair_2_t685 *, const MethodInfo*))KeyValuePair_2_get_Key_m13760_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15224(__this, ___value, method) (( void (*) (KeyValuePair_2_t685 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m13761_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
#define KeyValuePair_2_get_Value_m3591(__this, method) (( PointerEventData_t48 * (*) (KeyValuePair_2_t685 *, const MethodInfo*))KeyValuePair_2_get_Value_m13762_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15225(__this, ___value, method) (( void (*) (KeyValuePair_2_t685 *, PointerEventData_t48 *, const MethodInfo*))KeyValuePair_2_set_Value_m13763_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
#define KeyValuePair_2_ToString_m3604(__this, method) (( String_t* (*) (KeyValuePair_2_t685 *, const MethodInfo*))KeyValuePair_2_ToString_m13764_gshared)(__this, method)
