﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiveMouseLook
struct DiveMouseLook_t62;

#include "codegen/il2cpp-codegen.h"

// System.Void DiveMouseLook::.ctor()
extern "C" void DiveMouseLook__ctor_m287 (DiveMouseLook_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveMouseLook::Update()
extern "C" void DiveMouseLook_Update_m288 (DiveMouseLook_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveMouseLook::Start()
extern "C" void DiveMouseLook_Start_m289 (DiveMouseLook_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
