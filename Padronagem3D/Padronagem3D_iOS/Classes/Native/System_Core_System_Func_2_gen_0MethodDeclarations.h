﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"

// System.Void System.Func`2<CardboardEye,CardboardHead>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m554(__this, ___object, ___method, method) (( void (*) (Func_2_t55 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m11879_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<CardboardEye,CardboardHead>::Invoke(T)
#define Func_2_Invoke_m11880(__this, ___arg1, method) (( CardboardHead_t5 * (*) (Func_2_t55 *, CardboardEye_t29 *, const MethodInfo*))Func_2_Invoke_m11881_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<CardboardEye,CardboardHead>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m11882(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t55 *, CardboardEye_t29 *, AsyncCallback_t9 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m11883_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<CardboardEye,CardboardHead>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m11884(__this, ___result, method) (( CardboardHead_t5 * (*) (Func_2_t55 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m11885_gshared)(__this, ___result, method)
