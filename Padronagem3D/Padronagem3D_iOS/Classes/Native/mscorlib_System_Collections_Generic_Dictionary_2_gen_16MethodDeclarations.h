﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t874;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2063;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2539;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t2540;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t942;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2364;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m17708_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17708(__this, method) (( void (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2__ctor_m17708_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17709_gshared (Dictionary_2_t874 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17709(__this, ___comparer, method) (( void (*) (Dictionary_2_t874 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17709_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17710_gshared (Dictionary_2_t874 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17710(__this, ___capacity, method) (( void (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17710_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17711_gshared (Dictionary_2_t874 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17711(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t874 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m17711_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17712_gshared (Dictionary_2_t874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17712(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17712_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17713_gshared (Dictionary_2_t874 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17713(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t874 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17713_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17714_gshared (Dictionary_2_t874 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17714(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t874 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17714_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17715_gshared (Dictionary_2_t874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17715(__this, ___key, method) (( bool (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17715_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17716_gshared (Dictionary_2_t874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17716(__this, ___key, method) (( void (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17716_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17717_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17717(__this, method) (( bool (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17717_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17718_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17718(__this, method) (( Object_t * (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17718_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17719_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17719(__this, method) (( bool (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17719_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17720_gshared (Dictionary_2_t874 * __this, KeyValuePair_2_t2362  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17720(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t874 *, KeyValuePair_2_t2362 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17720_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17721_gshared (Dictionary_2_t874 * __this, KeyValuePair_2_t2362  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17721(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t874 *, KeyValuePair_2_t2362 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17721_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17722_gshared (Dictionary_2_t874 * __this, KeyValuePair_2U5BU5D_t2539* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17722(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t874 *, KeyValuePair_2U5BU5D_t2539*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17722_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17723_gshared (Dictionary_2_t874 * __this, KeyValuePair_2_t2362  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17723(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t874 *, KeyValuePair_2_t2362 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17723_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17724_gshared (Dictionary_2_t874 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17724(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t874 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17724_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17725_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17725(__this, method) (( Object_t * (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17725_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17726_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17726(__this, method) (( Object_t* (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17726_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17727_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17727(__this, method) (( Object_t * (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17727_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17728_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17728(__this, method) (( int32_t (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_get_Count_m17728_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m17729_gshared (Dictionary_2_t874 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17729(__this, ___key, method) (( int32_t (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m17729_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17730_gshared (Dictionary_2_t874 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17730(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t874 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m17730_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17731_gshared (Dictionary_2_t874 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17731(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t874 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17731_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17732_gshared (Dictionary_2_t874 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17732(__this, ___size, method) (( void (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17732_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17733_gshared (Dictionary_2_t874 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17733(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t874 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17733_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2362  Dictionary_2_make_pair_m17734_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17734(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2362  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m17734_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m17735_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17735(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m17735_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17736_gshared (Dictionary_2_t874 * __this, KeyValuePair_2U5BU5D_t2539* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17736(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t874 *, KeyValuePair_2U5BU5D_t2539*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17736_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m17737_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17737(__this, method) (( void (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_Resize_m17737_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17738_gshared (Dictionary_2_t874 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17738(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t874 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m17738_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m17739_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17739(__this, method) (( void (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_Clear_m17739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17740_gshared (Dictionary_2_t874 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17740(__this, ___key, method) (( bool (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m17740_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17741_gshared (Dictionary_2_t874 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17741(__this, ___value, method) (( bool (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m17741_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17742_gshared (Dictionary_2_t874 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17742(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t874 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m17742_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17743_gshared (Dictionary_2_t874 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17743(__this, ___sender, method) (( void (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17743_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17744_gshared (Dictionary_2_t874 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17744(__this, ___key, method) (( bool (*) (Dictionary_2_t874 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m17744_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17745_gshared (Dictionary_2_t874 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17745(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t874 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m17745_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t2364 * Dictionary_2_get_Values_m17746_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17746(__this, method) (( ValueCollection_t2364 * (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_get_Values_m17746_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m17747_gshared (Dictionary_2_t874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17747(__this, ___key, method) (( int32_t (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17747_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m17748_gshared (Dictionary_2_t874 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17748(__this, ___value, method) (( int32_t (*) (Dictionary_2_t874 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17748_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17749_gshared (Dictionary_2_t874 * __this, KeyValuePair_2_t2362  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17749(__this, ___pair, method) (( bool (*) (Dictionary_2_t874 *, KeyValuePair_2_t2362 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17749_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2366  Dictionary_2_GetEnumerator_m17750_gshared (Dictionary_2_t874 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17750(__this, method) (( Enumerator_t2366  (*) (Dictionary_2_t874 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17750_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t943  Dictionary_2_U3CCopyToU3Em__0_m17751_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17751(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17751_gshared)(__this /* static, unused */, ___key, ___value, method)
