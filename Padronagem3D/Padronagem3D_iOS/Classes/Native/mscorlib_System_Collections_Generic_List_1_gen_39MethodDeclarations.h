﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t2413;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t2542;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2541;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t1814;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1812;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1810;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t2417;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t2420;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_36.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m18124_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1__ctor_m18124(__this, method) (( void (*) (List_1_t2413 *, const MethodInfo*))List_1__ctor_m18124_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m18125_gshared (List_1_t2413 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m18125(__this, ___collection, method) (( void (*) (List_1_t2413 *, Object_t*, const MethodInfo*))List_1__ctor_m18125_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m18126_gshared (List_1_t2413 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m18126(__this, ___capacity, method) (( void (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1__ctor_m18126_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m18127_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m18127(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18127_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18128_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18128(__this, method) (( Object_t* (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18128_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18129_gshared (List_1_t2413 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m18129(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t2413 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18129_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m18130_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18130(__this, method) (( Object_t * (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18130_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m18131_gshared (List_1_t2413 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m18131(__this, ___item, method) (( int32_t (*) (List_1_t2413 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18131_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m18132_gshared (List_1_t2413 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m18132(__this, ___item, method) (( bool (*) (List_1_t2413 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m18132_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m18133_gshared (List_1_t2413 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m18133(__this, ___item, method) (( int32_t (*) (List_1_t2413 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m18133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m18134_gshared (List_1_t2413 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m18134(__this, ___index, ___item, method) (( void (*) (List_1_t2413 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m18134_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m18135_gshared (List_1_t2413 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m18135(__this, ___item, method) (( void (*) (List_1_t2413 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m18135_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18136_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18136(__this, method) (( bool (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18136_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m18137_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18137(__this, method) (( bool (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m18137_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m18138_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m18138(__this, method) (( Object_t * (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m18138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m18139_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m18139(__this, method) (( bool (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m18139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m18140_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m18140(__this, method) (( bool (*) (List_1_t2413 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m18140_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m18141_gshared (List_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m18141(__this, ___index, method) (( Object_t * (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m18141_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m18142_gshared (List_1_t2413 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m18142(__this, ___index, ___value, method) (( void (*) (List_1_t2413 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m18142_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m18143_gshared (List_1_t2413 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define List_1_Add_m18143(__this, ___item, method) (( void (*) (List_1_t2413 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_Add_m18143_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m18144_gshared (List_1_t2413 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m18144(__this, ___newCount, method) (( void (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m18144_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m18145_gshared (List_1_t2413 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m18145(__this, ___collection, method) (( void (*) (List_1_t2413 *, Object_t*, const MethodInfo*))List_1_AddCollection_m18145_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m18146_gshared (List_1_t2413 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m18146(__this, ___enumerable, method) (( void (*) (List_1_t2413 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m18146_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m18147_gshared (List_1_t2413 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m18147(__this, ___collection, method) (( void (*) (List_1_t2413 *, Object_t*, const MethodInfo*))List_1_AddRange_m18147_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1812 * List_1_AsReadOnly_m18148_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m18148(__this, method) (( ReadOnlyCollection_1_t1812 * (*) (List_1_t2413 *, const MethodInfo*))List_1_AsReadOnly_m18148_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m18149_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_Clear_m18149(__this, method) (( void (*) (List_1_t2413 *, const MethodInfo*))List_1_Clear_m18149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m18150_gshared (List_1_t2413 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define List_1_Contains_m18150(__this, ___item, method) (( bool (*) (List_1_t2413 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_Contains_m18150_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m18151_gshared (List_1_t2413 * __this, CustomAttributeTypedArgumentU5BU5D_t1810* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m18151(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t2413 *, CustomAttributeTypedArgumentU5BU5D_t1810*, int32_t, const MethodInfo*))List_1_CopyTo_m18151_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t1374  List_1_Find_m18152_gshared (List_1_t2413 * __this, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_Find_m18152(__this, ___match, method) (( CustomAttributeTypedArgument_t1374  (*) (List_1_t2413 *, Predicate_1_t2417 *, const MethodInfo*))List_1_Find_m18152_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m18153_gshared (Object_t * __this /* static, unused */, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m18153(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2417 *, const MethodInfo*))List_1_CheckMatch_m18153_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m18154_gshared (List_1_t2413 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m18154(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t2413 *, int32_t, int32_t, Predicate_1_t2417 *, const MethodInfo*))List_1_GetIndex_m18154_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t2414  List_1_GetEnumerator_m18155_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m18155(__this, method) (( Enumerator_t2414  (*) (List_1_t2413 *, const MethodInfo*))List_1_GetEnumerator_m18155_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m18156_gshared (List_1_t2413 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define List_1_IndexOf_m18156(__this, ___item, method) (( int32_t (*) (List_1_t2413 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_IndexOf_m18156_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m18157_gshared (List_1_t2413 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m18157(__this, ___start, ___delta, method) (( void (*) (List_1_t2413 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m18157_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m18158_gshared (List_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m18158(__this, ___index, method) (( void (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_CheckIndex_m18158_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m18159_gshared (List_1_t2413 * __this, int32_t ___index, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define List_1_Insert_m18159(__this, ___index, ___item, method) (( void (*) (List_1_t2413 *, int32_t, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_Insert_m18159_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m18160_gshared (List_1_t2413 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m18160(__this, ___collection, method) (( void (*) (List_1_t2413 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m18160_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m18161_gshared (List_1_t2413 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define List_1_Remove_m18161(__this, ___item, method) (( bool (*) (List_1_t2413 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_Remove_m18161_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m18162_gshared (List_1_t2413 * __this, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m18162(__this, ___match, method) (( int32_t (*) (List_1_t2413 *, Predicate_1_t2417 *, const MethodInfo*))List_1_RemoveAll_m18162_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m18163_gshared (List_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m18163(__this, ___index, method) (( void (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_RemoveAt_m18163_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C" void List_1_Reverse_m18164_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_Reverse_m18164(__this, method) (( void (*) (List_1_t2413 *, const MethodInfo*))List_1_Reverse_m18164_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C" void List_1_Sort_m18165_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_Sort_m18165(__this, method) (( void (*) (List_1_t2413 *, const MethodInfo*))List_1_Sort_m18165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m18166_gshared (List_1_t2413 * __this, Comparison_1_t2420 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m18166(__this, ___comparison, method) (( void (*) (List_1_t2413 *, Comparison_1_t2420 *, const MethodInfo*))List_1_Sort_m18166_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t1810* List_1_ToArray_m18167_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_ToArray_m18167(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t1810* (*) (List_1_t2413 *, const MethodInfo*))List_1_ToArray_m18167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m18168_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m18168(__this, method) (( void (*) (List_1_t2413 *, const MethodInfo*))List_1_TrimExcess_m18168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m18169_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m18169(__this, method) (( int32_t (*) (List_1_t2413 *, const MethodInfo*))List_1_get_Capacity_m18169_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m18170_gshared (List_1_t2413 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m18170(__this, ___value, method) (( void (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_set_Capacity_m18170_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m18171_gshared (List_1_t2413 * __this, const MethodInfo* method);
#define List_1_get_Count_m18171(__this, method) (( int32_t (*) (List_1_t2413 *, const MethodInfo*))List_1_get_Count_m18171_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1374  List_1_get_Item_m18172_gshared (List_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m18172(__this, ___index, method) (( CustomAttributeTypedArgument_t1374  (*) (List_1_t2413 *, int32_t, const MethodInfo*))List_1_get_Item_m18172_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m18173_gshared (List_1_t2413 * __this, int32_t ___index, CustomAttributeTypedArgument_t1374  ___value, const MethodInfo* method);
#define List_1_set_Item_m18173(__this, ___index, ___value, method) (( void (*) (List_1_t2413 *, int32_t, CustomAttributeTypedArgument_t1374 , const MethodInfo*))List_1_set_Item_m18173_gshared)(__this, ___index, ___value, method)
