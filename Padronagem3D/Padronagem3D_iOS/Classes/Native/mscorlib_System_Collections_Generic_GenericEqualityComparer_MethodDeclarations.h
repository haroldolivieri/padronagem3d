﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t1843;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m11041_gshared (GenericEqualityComparer_1_t1843 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m11041(__this, method) (( void (*) (GenericEqualityComparer_1_t1843 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m11041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m18567_gshared (GenericEqualityComparer_1_t1843 * __this, DateTime_t308  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m18567(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1843 *, DateTime_t308 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m18567_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m18568_gshared (GenericEqualityComparer_1_t1843 * __this, DateTime_t308  ___x, DateTime_t308  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m18568(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1843 *, DateTime_t308 , DateTime_t308 , const MethodInfo*))GenericEqualityComparer_1_Equals_m18568_gshared)(__this, ___x, ___y, method)
