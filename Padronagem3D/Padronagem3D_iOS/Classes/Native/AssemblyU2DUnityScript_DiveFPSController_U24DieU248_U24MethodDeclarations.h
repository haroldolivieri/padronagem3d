﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiveFPSController/$Die$8/$
struct U24_t124;
// DiveFPSController
struct DiveFPSController_t128;

#include "codegen/il2cpp-codegen.h"

// System.Void DiveFPSController/$Die$8/$::.ctor(DiveFPSController)
extern "C" void U24__ctor_m616 (U24_t124 * __this, DiveFPSController_t128 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DiveFPSController/$Die$8/$::MoveNext()
extern "C" bool U24_MoveNext_m617 (U24_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
