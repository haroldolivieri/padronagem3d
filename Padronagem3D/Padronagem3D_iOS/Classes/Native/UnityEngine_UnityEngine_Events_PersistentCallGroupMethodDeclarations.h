﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t397;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t399;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t401;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m2089 (PersistentCallGroup_t397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m2090 (PersistentCallGroup_t397 * __this, InvokableCallList_t399 * ___invokableList, UnityEventBase_t401 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
