﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// splashscreen/$FadeGUITexture$22/$
struct U24_t139;
// UnityEngine.GUITexture
struct GUITexture_t141;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Fade.h"

// System.Void splashscreen/$FadeGUITexture$22/$::.ctor(UnityEngine.GUITexture,System.Single,Fade)
extern "C" void U24__ctor_m641 (U24_t139 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean splashscreen/$FadeGUITexture$22/$::MoveNext()
extern "C" bool U24_MoveNext_m642 (U24_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
