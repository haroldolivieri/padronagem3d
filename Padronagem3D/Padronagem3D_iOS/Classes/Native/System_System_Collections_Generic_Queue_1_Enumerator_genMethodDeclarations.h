﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t58;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m11904_gshared (Enumerator_t1921 * __this, Queue_1_t58 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m11904(__this, ___q, method) (( void (*) (Enumerator_t1921 *, Queue_1_t58 *, const MethodInfo*))Enumerator__ctor_m11904_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m11905_gshared (Enumerator_t1921 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m11905(__this, method) (( void (*) (Enumerator_t1921 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11905_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11906_gshared (Enumerator_t1921 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11906(__this, method) (( Object_t * (*) (Enumerator_t1921 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11906_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m11907_gshared (Enumerator_t1921 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11907(__this, method) (( void (*) (Enumerator_t1921 *, const MethodInfo*))Enumerator_Dispose_m11907_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11908_gshared (Enumerator_t1921 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11908(__this, method) (( bool (*) (Enumerator_t1921 *, const MethodInfo*))Enumerator_MoveNext_m11908_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m11909_gshared (Enumerator_t1921 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11909(__this, method) (( int32_t (*) (Enumerator_t1921 *, const MethodInfo*))Enumerator_get_Current_m11909_gshared)(__this, method)
