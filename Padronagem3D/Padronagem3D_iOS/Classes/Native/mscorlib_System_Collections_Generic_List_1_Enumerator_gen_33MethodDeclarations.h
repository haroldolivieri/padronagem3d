﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m16785(__this, ___l, method) (( void (*) (Enumerator_t2282 *, List_1_t621 *, const MethodInfo*))Enumerator__ctor_m11163_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16786(__this, method) (( void (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11164_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16787(__this, method) (( Object_t * (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::Dispose()
#define Enumerator_Dispose_m16788(__this, method) (( void (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_Dispose_m11166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::VerifyState()
#define Enumerator_VerifyState_m16789(__this, method) (( void (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_VerifyState_m11167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::MoveNext()
#define Enumerator_MoveNext_m16790(__this, method) (( bool (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_MoveNext_m11168_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>::get_Current()
#define Enumerator_get_Current_m16791(__this, method) (( MatEntry_t619 * (*) (Enumerator_t2282 *, const MethodInfo*))Enumerator_get_Current_m11169_gshared)(__this, method)
