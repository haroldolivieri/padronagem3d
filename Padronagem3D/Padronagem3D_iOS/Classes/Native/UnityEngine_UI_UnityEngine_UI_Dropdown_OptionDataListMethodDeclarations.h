﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t552;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t553;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Dropdown/OptionDataList::.ctor()
extern "C" void OptionDataList__ctor_m2570 (OptionDataList_t552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown/OptionDataList::get_options()
extern "C" List_1_t553 * OptionDataList_get_options_m2571 (OptionDataList_t552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionDataList::set_options(System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>)
extern "C" void OptionDataList_set_options_m2572 (OptionDataList_t552 * __this, List_1_t553 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
