﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void CacheIndex_t222_marshal(const CacheIndex_t222& unmarshaled, CacheIndex_t222_marshaled& marshaled);
extern "C" void CacheIndex_t222_marshal_back(const CacheIndex_t222_marshaled& marshaled, CacheIndex_t222& unmarshaled);
extern "C" void CacheIndex_t222_marshal_cleanup(CacheIndex_t222_marshaled& marshaled);
