﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Single UnityEngine.Random::get_value()
extern "C" float Random_get_value_m331 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C" Vector3_t3  Random_get_onUnitSphere_m329 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
extern "C" void Random_INTERNAL_get_onUnitSphere_m1368 (Object_t * __this /* static, unused */, Vector3_t3 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
