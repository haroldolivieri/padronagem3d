﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14481(__this, ___dictionary, method) (( void (*) (Enumerator_t2127 *, Dictionary_2_t389 *, const MethodInfo*))Enumerator__ctor_m14416_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14482(__this, method) (( Object_t * (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14417_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14483(__this, method) (( void (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14418_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14484(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14419_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14485(__this, method) (( Object_t * (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14420_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14486(__this, method) (( Object_t * (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14421_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m14487(__this, method) (( bool (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_MoveNext_m14422_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m14488(__this, method) (( KeyValuePair_2_t2125  (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_get_Current_m14423_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14489(__this, method) (( Event_t84 * (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_get_CurrentKey_m14424_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14490(__this, method) (( int32_t (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_get_CurrentValue_m14425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m14491(__this, method) (( void (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_Reset_m14426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m14492(__this, method) (( void (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_VerifyState_m14427_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14493(__this, method) (( void (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_VerifyCurrent_m14428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m14494(__this, method) (( void (*) (Enumerator_t2127 *, const MethodInfo*))Enumerator_Dispose_m14429_gshared)(__this, method)
