﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIText
struct GUIText_t152;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C" void GUIText_set_text_m692 (GUIText_t152 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
