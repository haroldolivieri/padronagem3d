﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>
struct Collection_1_t1956;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t418;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t2479;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t1955;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor()
extern "C" void Collection_1__ctor_m12359_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12359(__this, method) (( void (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1__ctor_m12359_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12360_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12360(__this, method) (( bool (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12360_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12361_gshared (Collection_1_t1956 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12361(__this, ___array, ___index, method) (( void (*) (Collection_1_t1956 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12361_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12362_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12362(__this, method) (( Object_t * (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12362_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12363_gshared (Collection_1_t1956 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12363(__this, ___value, method) (( int32_t (*) (Collection_1_t1956 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12363_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12364_gshared (Collection_1_t1956 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12364(__this, ___value, method) (( bool (*) (Collection_1_t1956 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12364_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12365_gshared (Collection_1_t1956 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12365(__this, ___value, method) (( int32_t (*) (Collection_1_t1956 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12365_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12366_gshared (Collection_1_t1956 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12366(__this, ___index, ___value, method) (( void (*) (Collection_1_t1956 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12366_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12367_gshared (Collection_1_t1956 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12367(__this, ___value, method) (( void (*) (Collection_1_t1956 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12367_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12368_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12368(__this, method) (( bool (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12368_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12369_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12369(__this, method) (( Object_t * (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12370_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12370(__this, method) (( bool (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12370_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12371_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12371(__this, method) (( bool (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12371_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12372_gshared (Collection_1_t1956 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12372(__this, ___index, method) (( Object_t * (*) (Collection_1_t1956 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12372_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12373_gshared (Collection_1_t1956 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12373(__this, ___index, ___value, method) (( void (*) (Collection_1_t1956 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12373_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Add(T)
extern "C" void Collection_1_Add_m12374_gshared (Collection_1_t1956 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_Add_m12374(__this, ___item, method) (( void (*) (Collection_1_t1956 *, Vector4_t90 , const MethodInfo*))Collection_1_Add_m12374_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Clear()
extern "C" void Collection_1_Clear_m12375_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12375(__this, method) (( void (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_Clear_m12375_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ClearItems()
extern "C" void Collection_1_ClearItems_m12376_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12376(__this, method) (( void (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_ClearItems_m12376_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool Collection_1_Contains_m12377_gshared (Collection_1_t1956 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12377(__this, ___item, method) (( bool (*) (Collection_1_t1956 *, Vector4_t90 , const MethodInfo*))Collection_1_Contains_m12377_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12378_gshared (Collection_1_t1956 * __this, Vector4U5BU5D_t418* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12378(__this, ___array, ___index, method) (( void (*) (Collection_1_t1956 *, Vector4U5BU5D_t418*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12378_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12379_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12379(__this, method) (( Object_t* (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_GetEnumerator_m12379_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12380_gshared (Collection_1_t1956 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12380(__this, ___item, method) (( int32_t (*) (Collection_1_t1956 *, Vector4_t90 , const MethodInfo*))Collection_1_IndexOf_m12380_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12381_gshared (Collection_1_t1956 * __this, int32_t ___index, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12381(__this, ___index, ___item, method) (( void (*) (Collection_1_t1956 *, int32_t, Vector4_t90 , const MethodInfo*))Collection_1_Insert_m12381_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12382_gshared (Collection_1_t1956 * __this, int32_t ___index, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12382(__this, ___index, ___item, method) (( void (*) (Collection_1_t1956 *, int32_t, Vector4_t90 , const MethodInfo*))Collection_1_InsertItem_m12382_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool Collection_1_Remove_m12383_gshared (Collection_1_t1956 * __this, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12383(__this, ___item, method) (( bool (*) (Collection_1_t1956 *, Vector4_t90 , const MethodInfo*))Collection_1_Remove_m12383_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12384_gshared (Collection_1_t1956 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12384(__this, ___index, method) (( void (*) (Collection_1_t1956 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12384_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12385_gshared (Collection_1_t1956 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12385(__this, ___index, method) (( void (*) (Collection_1_t1956 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12385_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12386_gshared (Collection_1_t1956 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12386(__this, method) (( int32_t (*) (Collection_1_t1956 *, const MethodInfo*))Collection_1_get_Count_m12386_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t90  Collection_1_get_Item_m12387_gshared (Collection_1_t1956 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12387(__this, ___index, method) (( Vector4_t90  (*) (Collection_1_t1956 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12387_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12388_gshared (Collection_1_t1956 * __this, int32_t ___index, Vector4_t90  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12388(__this, ___index, ___value, method) (( void (*) (Collection_1_t1956 *, int32_t, Vector4_t90 , const MethodInfo*))Collection_1_set_Item_m12388_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12389_gshared (Collection_1_t1956 * __this, int32_t ___index, Vector4_t90  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12389(__this, ___index, ___item, method) (( void (*) (Collection_1_t1956 *, int32_t, Vector4_t90 , const MethodInfo*))Collection_1_SetItem_m12389_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12390_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12390(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12390_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ConvertItem(System.Object)
extern "C" Vector4_t90  Collection_1_ConvertItem_m12391_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12391(__this /* static, unused */, ___item, method) (( Vector4_t90  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12391_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12392_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12392(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12392_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12393_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12393(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12393_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12394_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12394(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12394_gshared)(__this /* static, unused */, ___list, method)
