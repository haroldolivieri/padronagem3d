﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ThreadStateException
struct ThreadStateException_t1676;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadStateException::.ctor()
extern "C" void ThreadStateException__ctor_m10129 (ThreadStateException_t1676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadStateException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadStateException__ctor_m10130 (ThreadStateException_t1676 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
