﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2084;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14065_gshared (Enumerator_t2089 * __this, Dictionary_2_t2084 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m14065(__this, ___host, method) (( void (*) (Enumerator_t2089 *, Dictionary_2_t2084 *, const MethodInfo*))Enumerator__ctor_m14065_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14066_gshared (Enumerator_t2089 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14066(__this, method) (( Object_t * (*) (Enumerator_t2089 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14066_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m14067_gshared (Enumerator_t2089 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m14067(__this, method) (( void (*) (Enumerator_t2089 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14068_gshared (Enumerator_t2089 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14068(__this, method) (( void (*) (Enumerator_t2089 *, const MethodInfo*))Enumerator_Dispose_m14068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14069_gshared (Enumerator_t2089 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14069(__this, method) (( bool (*) (Enumerator_t2089 *, const MethodInfo*))Enumerator_MoveNext_m14069_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m14070_gshared (Enumerator_t2089 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14070(__this, method) (( Object_t * (*) (Enumerator_t2089 *, const MethodInfo*))Enumerator_get_Current_m14070_gshared)(__this, method)
