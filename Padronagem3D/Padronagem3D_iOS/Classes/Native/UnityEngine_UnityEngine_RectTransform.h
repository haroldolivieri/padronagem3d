﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t210;

#include "UnityEngine_UnityEngine_Transform.h"

// UnityEngine.RectTransform
struct  RectTransform_t211  : public Transform_t33
{
};
struct RectTransform_t211_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t210 * ___reapplyDrivenProperties_2;
};
