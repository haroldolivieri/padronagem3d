﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15968(__this, ___dictionary, method) (( void (*) (Enumerator_t2231 *, Dictionary_2_t563 *, const MethodInfo*))Enumerator__ctor_m14071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15969(__this, method) (( Object_t * (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m15970(__this, method) (( void (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15971(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15972(__this, method) (( Object_t * (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15973(__this, method) (( Object_t * (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m15974(__this, method) (( bool (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_MoveNext_m14077_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define Enumerator_get_Current_m15975(__this, method) (( KeyValuePair_2_t2229  (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_get_Current_m14078_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15976(__this, method) (( Font_t283 * (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_get_CurrentKey_m14079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15977(__this, method) (( List_1_t690 * (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_get_CurrentValue_m14080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Reset()
#define Enumerator_Reset_m15978(__this, method) (( void (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_Reset_m14081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyState()
#define Enumerator_VerifyState_m15979(__this, method) (( void (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_VerifyState_m14082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15980(__this, method) (( void (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_VerifyCurrent_m14083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m15981(__this, method) (( void (*) (Enumerator_t2231 *, const MethodInfo*))Enumerator_Dispose_m14084_gshared)(__this, method)
