﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t290;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t2491;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t2492;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t2493;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t2023;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t432;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t2027;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2030;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m13164_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1__ctor_m13164(__this, method) (( void (*) (List_1_t290 *, const MethodInfo*))List_1__ctor_m13164_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m13165_gshared (List_1_t290 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m13165(__this, ___collection, method) (( void (*) (List_1_t290 *, Object_t*, const MethodInfo*))List_1__ctor_m13165_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m2171_gshared (List_1_t290 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m2171(__this, ___capacity, method) (( void (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1__ctor_m2171_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m13166_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13166(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13166_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13167_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13167(__this, method) (( Object_t* (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13168_gshared (List_1_t290 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13168(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t290 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13168_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13169_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13169(__this, method) (( Object_t * (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13169_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13170_gshared (List_1_t290 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13170(__this, ___item, method) (( int32_t (*) (List_1_t290 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13170_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13171_gshared (List_1_t290 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13171(__this, ___item, method) (( bool (*) (List_1_t290 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13171_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13172_gshared (List_1_t290 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13172(__this, ___item, method) (( int32_t (*) (List_1_t290 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13172_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13173_gshared (List_1_t290 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13173(__this, ___index, ___item, method) (( void (*) (List_1_t290 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13173_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13174_gshared (List_1_t290 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13174(__this, ___item, method) (( void (*) (List_1_t290 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13174_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13175_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13175(__this, method) (( bool (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13176_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13176(__this, method) (( bool (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13176_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13177_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13177(__this, method) (( Object_t * (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13178_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13178(__this, method) (( bool (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13179_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13179(__this, method) (( bool (*) (List_1_t290 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13179_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13180_gshared (List_1_t290 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13180(__this, ___index, method) (( Object_t * (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13180_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13181_gshared (List_1_t290 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13181(__this, ___index, ___value, method) (( void (*) (List_1_t290 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13181_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m13182_gshared (List_1_t290 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define List_1_Add_m13182(__this, ___item, method) (( void (*) (List_1_t290 *, UICharInfo_t285 , const MethodInfo*))List_1_Add_m13182_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13183_gshared (List_1_t290 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13183(__this, ___newCount, method) (( void (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13183_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13184_gshared (List_1_t290 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13184(__this, ___collection, method) (( void (*) (List_1_t290 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13184_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13185_gshared (List_1_t290 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13185(__this, ___enumerable, method) (( void (*) (List_1_t290 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13185_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13186_gshared (List_1_t290 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13186(__this, ___collection, method) (( void (*) (List_1_t290 *, Object_t*, const MethodInfo*))List_1_AddRange_m13186_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2023 * List_1_AsReadOnly_m13187_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13187(__this, method) (( ReadOnlyCollection_1_t2023 * (*) (List_1_t290 *, const MethodInfo*))List_1_AsReadOnly_m13187_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m13188_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_Clear_m13188(__this, method) (( void (*) (List_1_t290 *, const MethodInfo*))List_1_Clear_m13188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m13189_gshared (List_1_t290 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define List_1_Contains_m13189(__this, ___item, method) (( bool (*) (List_1_t290 *, UICharInfo_t285 , const MethodInfo*))List_1_Contains_m13189_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13190_gshared (List_1_t290 * __this, UICharInfoU5BU5D_t432* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13190(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t290 *, UICharInfoU5BU5D_t432*, int32_t, const MethodInfo*))List_1_CopyTo_m13190_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t285  List_1_Find_m13191_gshared (List_1_t290 * __this, Predicate_1_t2027 * ___match, const MethodInfo* method);
#define List_1_Find_m13191(__this, ___match, method) (( UICharInfo_t285  (*) (List_1_t290 *, Predicate_1_t2027 *, const MethodInfo*))List_1_Find_m13191_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13192_gshared (Object_t * __this /* static, unused */, Predicate_1_t2027 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13192(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2027 *, const MethodInfo*))List_1_CheckMatch_m13192_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13193_gshared (List_1_t290 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2027 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13193(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t290 *, int32_t, int32_t, Predicate_1_t2027 *, const MethodInfo*))List_1_GetIndex_m13193_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t2022  List_1_GetEnumerator_m13194_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13194(__this, method) (( Enumerator_t2022  (*) (List_1_t290 *, const MethodInfo*))List_1_GetEnumerator_m13194_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13195_gshared (List_1_t290 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define List_1_IndexOf_m13195(__this, ___item, method) (( int32_t (*) (List_1_t290 *, UICharInfo_t285 , const MethodInfo*))List_1_IndexOf_m13195_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13196_gshared (List_1_t290 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13196(__this, ___start, ___delta, method) (( void (*) (List_1_t290 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13196_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13197_gshared (List_1_t290 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13197(__this, ___index, method) (( void (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13197_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13198_gshared (List_1_t290 * __this, int32_t ___index, UICharInfo_t285  ___item, const MethodInfo* method);
#define List_1_Insert_m13198(__this, ___index, ___item, method) (( void (*) (List_1_t290 *, int32_t, UICharInfo_t285 , const MethodInfo*))List_1_Insert_m13198_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13199_gshared (List_1_t290 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13199(__this, ___collection, method) (( void (*) (List_1_t290 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13199_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m13200_gshared (List_1_t290 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define List_1_Remove_m13200(__this, ___item, method) (( bool (*) (List_1_t290 *, UICharInfo_t285 , const MethodInfo*))List_1_Remove_m13200_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13201_gshared (List_1_t290 * __this, Predicate_1_t2027 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13201(__this, ___match, method) (( int32_t (*) (List_1_t290 *, Predicate_1_t2027 *, const MethodInfo*))List_1_RemoveAll_m13201_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13202_gshared (List_1_t290 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13202(__this, ___index, method) (( void (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13202_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m13203_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_Reverse_m13203(__this, method) (( void (*) (List_1_t290 *, const MethodInfo*))List_1_Reverse_m13203_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m13204_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_Sort_m13204(__this, method) (( void (*) (List_1_t290 *, const MethodInfo*))List_1_Sort_m13204_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m13205_gshared (List_1_t290 * __this, Comparison_1_t2030 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m13205(__this, ___comparison, method) (( void (*) (List_1_t290 *, Comparison_1_t2030 *, const MethodInfo*))List_1_Sort_m13205_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t432* List_1_ToArray_m13206_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_ToArray_m13206(__this, method) (( UICharInfoU5BU5D_t432* (*) (List_1_t290 *, const MethodInfo*))List_1_ToArray_m13206_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m13207_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13207(__this, method) (( void (*) (List_1_t290 *, const MethodInfo*))List_1_TrimExcess_m13207_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m13208_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m13208(__this, method) (( int32_t (*) (List_1_t290 *, const MethodInfo*))List_1_get_Capacity_m13208_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m13209_gshared (List_1_t290 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m13209(__this, ___value, method) (( void (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13209_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m13210_gshared (List_1_t290 * __this, const MethodInfo* method);
#define List_1_get_Count_m13210(__this, method) (( int32_t (*) (List_1_t290 *, const MethodInfo*))List_1_get_Count_m13210_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t285  List_1_get_Item_m13211_gshared (List_1_t290 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13211(__this, ___index, method) (( UICharInfo_t285  (*) (List_1_t290 *, int32_t, const MethodInfo*))List_1_get_Item_m13211_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13212_gshared (List_1_t290 * __this, int32_t ___index, UICharInfo_t285  ___value, const MethodInfo* method);
#define List_1_set_Item_m13212(__this, ___index, ___value, method) (( void (*) (List_1_t290 *, int32_t, UICharInfo_t285 , const MethodInfo*))List_1_set_Item_m13212_gshared)(__this, ___index, ___value, method)
