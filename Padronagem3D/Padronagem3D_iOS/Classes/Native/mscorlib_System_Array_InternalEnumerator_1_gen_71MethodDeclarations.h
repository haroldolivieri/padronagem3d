﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_71.h"

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17871_gshared (InternalEnumerator_1_t2379 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17871(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2379 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17871_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17872_gshared (InternalEnumerator_1_t2379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17872(__this, method) (( void (*) (InternalEnumerator_1_t2379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17872_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17873_gshared (InternalEnumerator_1_t2379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17873(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17873_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17874_gshared (InternalEnumerator_1_t2379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17874(__this, method) (( void (*) (InternalEnumerator_1_t2379 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17874_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17875_gshared (InternalEnumerator_1_t2379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17875(__this, method) (( bool (*) (InternalEnumerator_1_t2379 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17875_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m17876_gshared (InternalEnumerator_1_t2379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17876(__this, method) (( int64_t (*) (InternalEnumerator_1_t2379 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17876_gshared)(__this, method)
