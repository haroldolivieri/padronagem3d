﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t1846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C" void GenericComparer_1__ctor_m11047_gshared (GenericComparer_1_t1846 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m11047(__this, method) (( void (*) (GenericComparer_1_t1846 *, const MethodInfo*))GenericComparer_1__ctor_m11047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m18598_gshared (GenericComparer_1_t1846 * __this, Guid_t1720  ___x, Guid_t1720  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m18598(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1846 *, Guid_t1720 , Guid_t1720 , const MethodInfo*))GenericComparer_1_Compare_m18598_gshared)(__this, ___x, ___y, method)
