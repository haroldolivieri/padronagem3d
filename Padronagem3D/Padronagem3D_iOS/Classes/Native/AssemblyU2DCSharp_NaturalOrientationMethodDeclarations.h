﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NaturalOrientation
struct NaturalOrientation_t63;

#include "codegen/il2cpp-codegen.h"

// System.Void NaturalOrientation::.ctor()
extern "C" void NaturalOrientation__ctor_m290 (NaturalOrientation_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NaturalOrientation::.cctor()
extern "C" void NaturalOrientation__cctor_m291 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
