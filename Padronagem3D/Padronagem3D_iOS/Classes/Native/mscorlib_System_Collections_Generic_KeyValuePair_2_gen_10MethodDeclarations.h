﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16226(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2245 *, Graphic_t564 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m13551_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m16227(__this, method) (( Graphic_t564 * (*) (KeyValuePair_2_t2245 *, const MethodInfo*))KeyValuePair_2_get_Key_m13552_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16228(__this, ___value, method) (( void (*) (KeyValuePair_2_t2245 *, Graphic_t564 *, const MethodInfo*))KeyValuePair_2_set_Key_m13553_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m16229(__this, method) (( int32_t (*) (KeyValuePair_2_t2245 *, const MethodInfo*))KeyValuePair_2_get_Value_m13554_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16230(__this, ___value, method) (( void (*) (KeyValuePair_2_t2245 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m13555_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m16231(__this, method) (( String_t* (*) (KeyValuePair_2_t2245 *, const MethodInfo*))KeyValuePair_2_ToString_m13556_gshared)(__this, method)
