﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t58;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2475;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Int32[]
struct Int32U5BU5D_t107;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Int32>::.ctor()
extern "C" void Queue_1__ctor_m563_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1__ctor_m563(__this, method) (( void (*) (Queue_1_t58 *, const MethodInfo*))Queue_1__ctor_m563_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m11896_gshared (Queue_1_t58 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m11896(__this, ___array, ___idx, method) (( void (*) (Queue_1_t58 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m11896_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m11897_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m11897(__this, method) (( bool (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m11897_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m11898_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m11898(__this, method) (( Object_t * (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m11898_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11899_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11899(__this, method) (( Object_t* (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11899_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m11900_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m11900(__this, method) (( Object_t * (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m11900_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::Clear()
extern "C" void Queue_1_Clear_m574_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_Clear_m574(__this, method) (( void (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_Clear_m574_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m573_gshared (Queue_1_t58 * __this, Int32U5BU5D_t107* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m573(__this, ___array, ___idx, method) (( void (*) (Queue_1_t58 *, Int32U5BU5D_t107*, int32_t, const MethodInfo*))Queue_1_CopyTo_m573_gshared)(__this, ___array, ___idx, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m576_gshared (Queue_1_t58 * __this, int32_t ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m576(__this, ___item, method) (( void (*) (Queue_1_t58 *, int32_t, const MethodInfo*))Queue_1_Enqueue_m576_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m11901_gshared (Queue_1_t58 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m11901(__this, ___new_size, method) (( void (*) (Queue_1_t58 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m11901_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Int32>::get_Count()
extern "C" int32_t Queue_1_get_Count_m11902_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m11902(__this, method) (( int32_t (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_get_Count_m11902_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t1921  Queue_1_GetEnumerator_m11903_gshared (Queue_1_t58 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m11903(__this, method) (( Enumerator_t1921  (*) (Queue_1_t58 *, const MethodInfo*))Queue_1_GetEnumerator_m11903_gshared)(__this, method)
