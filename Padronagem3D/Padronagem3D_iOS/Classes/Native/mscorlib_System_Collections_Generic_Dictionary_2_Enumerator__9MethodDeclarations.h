﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16208(__this, ___dictionary, method) (( void (*) (Enumerator_t2244 *, Dictionary_2_t571 *, const MethodInfo*))Enumerator__ctor_m14071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16209(__this, method) (( Object_t * (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16210(__this, method) (( void (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16211(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16212(__this, method) (( Object_t * (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16213(__this, method) (( Object_t * (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m16214(__this, method) (( bool (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_MoveNext_m14077_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m16215(__this, method) (( KeyValuePair_2_t2242  (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_get_Current_m14078_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16216(__this, method) (( Canvas_t294 * (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_get_CurrentKey_m14079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16217(__this, method) (( IndexedSet_1_t698 * (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_get_CurrentValue_m14080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Reset()
#define Enumerator_Reset_m16218(__this, method) (( void (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_Reset_m14081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m16219(__this, method) (( void (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_VerifyState_m14082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16220(__this, method) (( void (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_VerifyCurrent_m14083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m16221(__this, method) (( void (*) (Enumerator_t2244 *, const MethodInfo*))Enumerator_Dispose_m14084_gshared)(__this, method)
