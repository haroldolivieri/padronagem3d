﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_36.h"
#include "mscorlib_System_Collections_Generic_Link.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13557_gshared (InternalEnumerator_1_t2049 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13557(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2049 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13557_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m13558_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m13558(__this, method) (( void (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m13558_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13559_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13559(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13559_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13560_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13560(__this, method) (( void (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13560_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13561_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13561(__this, method) (( bool (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13561_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern "C" Link_t1229  InternalEnumerator_1_get_Current_m13562_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13562(__this, method) (( Link_t1229  (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13562_gshared)(__this, method)
