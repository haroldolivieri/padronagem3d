﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// NaturalOrientation
struct  NaturalOrientation_t63  : public MonoBehaviour_t2
{
};
struct NaturalOrientation_t63_StaticFields{
	// System.Int32 NaturalOrientation::ORIENTATION_UNDEFINED
	int32_t ___ORIENTATION_UNDEFINED_2;
	// System.Int32 NaturalOrientation::ORIENTATION_PORTRAIT
	int32_t ___ORIENTATION_PORTRAIT_3;
	// System.Int32 NaturalOrientation::ORIENTATION_LANDSCAPE
	int32_t ___ORIENTATION_LANDSCAPE_4;
	// System.Int32 NaturalOrientation::ROTATION_0
	int32_t ___ROTATION_0_5;
	// System.Int32 NaturalOrientation::ROTATION_180
	int32_t ___ROTATION_180_6;
	// System.Int32 NaturalOrientation::ROTATION_270
	int32_t ___ROTATION_270_7;
	// System.Int32 NaturalOrientation::ROTATION_90
	int32_t ___ROTATION_90_8;
	// System.Int32 NaturalOrientation::PORTRAIT
	int32_t ___PORTRAIT_9;
	// System.Int32 NaturalOrientation::PORTRAIT_UPSIDEDOWN
	int32_t ___PORTRAIT_UPSIDEDOWN_10;
	// System.Int32 NaturalOrientation::LANDSCAPE
	int32_t ___LANDSCAPE_11;
	// System.Int32 NaturalOrientation::LANDSCAPE_LEFT
	int32_t ___LANDSCAPE_LEFT_12;
};
