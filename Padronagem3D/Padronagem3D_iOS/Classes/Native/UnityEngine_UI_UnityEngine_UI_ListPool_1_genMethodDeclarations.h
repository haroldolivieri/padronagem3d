﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::.cctor()
#define ListPool_1__cctor_m15792(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m11714_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::Get()
#define ListPool_1_Get_m3632(__this /* static, unused */, method) (( List_1_t687 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m11715_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m3634(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t687 *, const MethodInfo*))ListPool_1_Release_m11716_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
#define ListPool_1_U3Cs_ListPoolU3Em__15_m15793(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t687 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m11718_gshared)(__this /* static, unused */, ___l, method)
