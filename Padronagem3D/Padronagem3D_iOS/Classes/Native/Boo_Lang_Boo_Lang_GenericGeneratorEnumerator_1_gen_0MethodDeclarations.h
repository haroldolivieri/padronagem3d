﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_1MethodDeclarations.h"

// System.Void Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::.ctor()
#define GenericGeneratorEnumerator_1__ctor_m693(__this, method) (( void (*) (GenericGeneratorEnumerator_1_t135 *, const MethodInfo*))GenericGeneratorEnumerator_1__ctor_m699_gshared)(__this, method)
// System.Object Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::System.Collections.IEnumerator.get_Current()
#define GenericGeneratorEnumerator_1_System_Collections_IEnumerator_get_Current_m11931(__this, method) (( Object_t * (*) (GenericGeneratorEnumerator_1_t135 *, const MethodInfo*))GenericGeneratorEnumerator_1_System_Collections_IEnumerator_get_Current_m11921_gshared)(__this, method)
// T Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::get_Current()
#define GenericGeneratorEnumerator_1_get_Current_m11932(__this, method) (( YieldInstruction_t164 * (*) (GenericGeneratorEnumerator_1_t135 *, const MethodInfo*))GenericGeneratorEnumerator_1_get_Current_m11923_gshared)(__this, method)
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::Dispose()
#define GenericGeneratorEnumerator_1_Dispose_m11933(__this, method) (( void (*) (GenericGeneratorEnumerator_1_t135 *, const MethodInfo*))GenericGeneratorEnumerator_1_Dispose_m11925_gshared)(__this, method)
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::Reset()
#define GenericGeneratorEnumerator_1_Reset_m11934(__this, method) (( void (*) (GenericGeneratorEnumerator_1_t135 *, const MethodInfo*))GenericGeneratorEnumerator_1_Reset_m11927_gshared)(__this, method)
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::Yield(System.Int32,T)
#define GenericGeneratorEnumerator_1_Yield_m695(__this, ___state, ___value, method) (( bool (*) (GenericGeneratorEnumerator_1_t135 *, int32_t, YieldInstruction_t164 *, const MethodInfo*))GenericGeneratorEnumerator_1_Yield_m11928_gshared)(__this, ___state, ___value, method)
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::YieldDefault(System.Int32)
#define GenericGeneratorEnumerator_1_YieldDefault_m697(__this, ___state, method) (( bool (*) (GenericGeneratorEnumerator_1_t135 *, int32_t, const MethodInfo*))GenericGeneratorEnumerator_1_YieldDefault_m700_gshared)(__this, ___state, method)
