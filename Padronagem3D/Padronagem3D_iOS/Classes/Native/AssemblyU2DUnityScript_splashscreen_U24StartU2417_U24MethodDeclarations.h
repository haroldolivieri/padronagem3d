﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// splashscreen/$Start$17/$
struct U24_t134;
// splashscreen
struct splashscreen_t136;

#include "codegen/il2cpp-codegen.h"

// System.Void splashscreen/$Start$17/$::.ctor(splashscreen)
extern "C" void U24__ctor_m637 (U24_t134 * __this, splashscreen_t136 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean splashscreen/$Start$17/$::MoveNext()
extern "C" bool U24_MoveNext_m638 (U24_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
