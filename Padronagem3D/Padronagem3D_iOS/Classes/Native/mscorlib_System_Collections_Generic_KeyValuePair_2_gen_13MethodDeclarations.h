﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17357(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2330 *, DispatcherKey_t722 *, Dispatcher_t718 *, const MethodInfo*))KeyValuePair_2__ctor_m14045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::get_Key()
#define KeyValuePair_2_get_Key_m17358(__this, method) (( DispatcherKey_t722 * (*) (KeyValuePair_2_t2330 *, const MethodInfo*))KeyValuePair_2_get_Key_m14046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17359(__this, ___value, method) (( void (*) (KeyValuePair_2_t2330 *, DispatcherKey_t722 *, const MethodInfo*))KeyValuePair_2_set_Key_m14047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::get_Value()
#define KeyValuePair_2_get_Value_m17360(__this, method) (( Dispatcher_t718 * (*) (KeyValuePair_2_t2330 *, const MethodInfo*))KeyValuePair_2_get_Value_m14048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17361(__this, ___value, method) (( void (*) (KeyValuePair_2_t2330 *, Dispatcher_t718 *, const MethodInfo*))KeyValuePair_2_set_Value_m14049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::ToString()
#define KeyValuePair_2_ToString_m17362(__this, method) (( String_t* (*) (KeyValuePair_2_t2330 *, const MethodInfo*))KeyValuePair_2_ToString_m14050_gshared)(__this, method)
