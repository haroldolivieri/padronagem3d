﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t874;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17784_gshared (Enumerator_t2366 * __this, Dictionary_2_t874 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17784(__this, ___dictionary, method) (( void (*) (Enumerator_t2366 *, Dictionary_2_t874 *, const MethodInfo*))Enumerator__ctor_m17784_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17785_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17785(__this, method) (( Object_t * (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17785_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17786_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17786(__this, method) (( void (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17786_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t943  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17787_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17787(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17787_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17788_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17788(__this, method) (( Object_t * (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17788_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17789_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17789(__this, method) (( Object_t * (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17789_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17790_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17790(__this, method) (( bool (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_MoveNext_m17790_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2362  Enumerator_get_Current_m17791_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17791(__this, method) (( KeyValuePair_2_t2362  (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_get_Current_m17791_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17792_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17792(__this, method) (( int32_t (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_get_CurrentKey_m17792_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m17793_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17793(__this, method) (( int32_t (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_get_CurrentValue_m17793_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m17794_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_Reset_m17794(__this, method) (( void (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_Reset_m17794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m17795_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17795(__this, method) (( void (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_VerifyState_m17795_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17796_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17796(__this, method) (( void (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_VerifyCurrent_m17796_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m17797_gshared (Enumerator_t2366 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17797(__this, method) (( void (*) (Enumerator_t2366 *, const MethodInfo*))Enumerator_Dispose_m17797_gshared)(__this, method)
