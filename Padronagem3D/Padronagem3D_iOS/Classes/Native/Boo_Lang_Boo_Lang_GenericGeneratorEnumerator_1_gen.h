﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t127;
struct AsyncOperation_t127_marshaled;

#include "mscorlib_System_Object.h"

// Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.AsyncOperation>
struct  GenericGeneratorEnumerator_1_t125  : public Object_t
{
	// T Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.AsyncOperation>::_current
	AsyncOperation_t127 * ____current_0;
	// System.Int32 Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.AsyncOperation>::_state
	int32_t ____state_1;
};
