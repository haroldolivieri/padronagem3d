﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Outline
struct Outline_t656;
// UnityEngine.Mesh
struct Mesh_t182;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m3523 (Outline_t656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyMesh(UnityEngine.Mesh)
extern "C" void Outline_ModifyMesh_m3524 (Outline_t656 * __this, Mesh_t182 * ___mesh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
