﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t32;
// OffsetCenter
struct OffsetCenter_t64;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// OffsetCenter
struct  OffsetCenter_t64  : public MonoBehaviour_t2
{
	// System.Single OffsetCenter::offset
	float ___offset_2;
	// System.Single OffsetCenter::max_offset
	float ___max_offset_3;
	// UnityEngine.Camera OffsetCenter::cameraleft
	Camera_t32 * ___cameraleft_4;
	// UnityEngine.Camera OffsetCenter::cameraright
	Camera_t32 * ___cameraright_5;
	// System.Single OffsetCenter::correctionfactor
	float ___correctionfactor_6;
	// System.Boolean OffsetCenter::autoCorrectOffset
	bool ___autoCorrectOffset_8;
	// System.Boolean OffsetCenter::debugMetrics
	bool ___debugMetrics_9;
	// System.Int32 OffsetCenter::vpixels
	int32_t ___vpixels_10;
	// System.Int32 OffsetCenter::hpixels
	int32_t ___hpixels_11;
	// System.Single OffsetCenter::ydpi
	float ___ydpi_12;
	// System.Single OffsetCenter::xdpi
	float ___xdpi_13;
	// System.Single OffsetCenter::xmm
	float ___xmm_14;
	// System.Single OffsetCenter::ymm
	float ___ymm_15;
	// System.Single OffsetCenter::mmdist
	float ___mmdist_16;
	// System.Single OffsetCenter::correction_factor
	float ___correction_factor_17;
};
struct OffsetCenter_t64_StaticFields{
	// OffsetCenter OffsetCenter::instance
	OffsetCenter_t64 * ___instance_7;
};
