﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t145;
// UnityEngine.GameObject
struct GameObject_t47;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.GameObject UnityEngine.ControllerColliderHit::get_gameObject()
extern "C" GameObject_t47 * ControllerColliderHit_get_gameObject_m671 (ControllerColliderHit_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C" Vector3_t3  ControllerColliderHit_get_normal_m669 (ControllerColliderHit_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C" Vector3_t3  ControllerColliderHit_get_moveDirection_m670 (ControllerColliderHit_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
