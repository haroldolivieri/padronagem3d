﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// splashscreen
struct splashscreen_t136;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen_0.h"

// splashscreen/$Start$17
struct  U24StartU2417_t137  : public GenericGenerator_1_t138
{
	// splashscreen splashscreen/$Start$17::$self_$21
	splashscreen_t136 * ___U24self_U2421_0;
};
