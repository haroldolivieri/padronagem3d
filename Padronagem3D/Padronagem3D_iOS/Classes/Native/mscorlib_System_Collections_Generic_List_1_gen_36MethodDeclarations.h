﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t703;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t118;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t2469;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t1851;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Predicate`1<System.Object>
struct Predicate_1_t1861;
// System.Comparison`1<System.Object>
struct Comparison_1_t1867;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m11065_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1__ctor_m11065(__this, method) (( void (*) (List_1_t703 *, const MethodInfo*))List_1__ctor_m11065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m11067_gshared (List_1_t703 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m11067(__this, ___collection, method) (( void (*) (List_1_t703 *, Object_t*, const MethodInfo*))List_1__ctor_m11067_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m11069_gshared (List_1_t703 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m11069(__this, ___capacity, method) (( void (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1__ctor_m11069_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m11071_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m11071(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11071_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11073_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11073(__this, method) (( Object_t* (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m11075_gshared (List_1_t703 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m11075(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t703 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m11075_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m11077_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11077(__this, method) (( Object_t * (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m11077_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m11079_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m11079(__this, ___item, method) (( int32_t (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m11079_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m11081_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m11081(__this, ___item, method) (( bool (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m11081_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m11083_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m11083(__this, ___item, method) (( int32_t (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m11083_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m11085_gshared (List_1_t703 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m11085(__this, ___index, ___item, method) (( void (*) (List_1_t703 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m11085_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m11087_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m11087(__this, ___item, method) (( void (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m11087_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11089_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11089(__this, method) (( bool (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11089_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m11091_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m11091(__this, method) (( bool (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m11091_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m11093_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m11093(__this, method) (( Object_t * (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m11093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m11095_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m11095(__this, method) (( bool (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m11095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m11097_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m11097(__this, method) (( bool (*) (List_1_t703 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m11097_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m11099_gshared (List_1_t703 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m11099(__this, ___index, method) (( Object_t * (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m11099_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m11101_gshared (List_1_t703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m11101(__this, ___index, ___value, method) (( void (*) (List_1_t703 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m11101_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m11103_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m11103(__this, ___item, method) (( void (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_Add_m11103_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m11105_gshared (List_1_t703 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m11105(__this, ___newCount, method) (( void (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11105_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m11107_gshared (List_1_t703 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m11107(__this, ___collection, method) (( void (*) (List_1_t703 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11107_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m11109_gshared (List_1_t703 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m11109(__this, ___enumerable, method) (( void (*) (List_1_t703 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11109_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m11111_gshared (List_1_t703 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m11111(__this, ___collection, method) (( void (*) (List_1_t703 *, Object_t*, const MethodInfo*))List_1_AddRange_m11111_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1851 * List_1_AsReadOnly_m11113_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m11113(__this, method) (( ReadOnlyCollection_1_t1851 * (*) (List_1_t703 *, const MethodInfo*))List_1_AsReadOnly_m11113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m11115_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_Clear_m11115(__this, method) (( void (*) (List_1_t703 *, const MethodInfo*))List_1_Clear_m11115_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m11117_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m11117(__this, ___item, method) (( bool (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_Contains_m11117_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m11119_gshared (List_1_t703 * __this, ObjectU5BU5D_t115* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m11119(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t703 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))List_1_CopyTo_m11119_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m11121_gshared (List_1_t703 * __this, Predicate_1_t1861 * ___match, const MethodInfo* method);
#define List_1_Find_m11121(__this, ___match, method) (( Object_t * (*) (List_1_t703 *, Predicate_1_t1861 *, const MethodInfo*))List_1_Find_m11121_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m11123_gshared (Object_t * __this /* static, unused */, Predicate_1_t1861 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m11123(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1861 *, const MethodInfo*))List_1_CheckMatch_m11123_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m11125_gshared (List_1_t703 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1861 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m11125(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t703 *, int32_t, int32_t, Predicate_1_t1861 *, const MethodInfo*))List_1_GetIndex_m11125_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1855  List_1_GetEnumerator_m11127_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m11127(__this, method) (( Enumerator_t1855  (*) (List_1_t703 *, const MethodInfo*))List_1_GetEnumerator_m11127_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m11129_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m11129(__this, ___item, method) (( int32_t (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_IndexOf_m11129_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m11131_gshared (List_1_t703 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m11131(__this, ___start, ___delta, method) (( void (*) (List_1_t703 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11131_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m11133_gshared (List_1_t703 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m11133(__this, ___index, method) (( void (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11133_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m11135_gshared (List_1_t703 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m11135(__this, ___index, ___item, method) (( void (*) (List_1_t703 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m11135_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m11137_gshared (List_1_t703 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m11137(__this, ___collection, method) (( void (*) (List_1_t703 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11137_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m11139_gshared (List_1_t703 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m11139(__this, ___item, method) (( bool (*) (List_1_t703 *, Object_t *, const MethodInfo*))List_1_Remove_m11139_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m11141_gshared (List_1_t703 * __this, Predicate_1_t1861 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m11141(__this, ___match, method) (( int32_t (*) (List_1_t703 *, Predicate_1_t1861 *, const MethodInfo*))List_1_RemoveAll_m11141_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m11143_gshared (List_1_t703 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m11143(__this, ___index, method) (( void (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_RemoveAt_m11143_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m11145_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_Reverse_m11145(__this, method) (( void (*) (List_1_t703 *, const MethodInfo*))List_1_Reverse_m11145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m11147_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_Sort_m11147(__this, method) (( void (*) (List_1_t703 *, const MethodInfo*))List_1_Sort_m11147_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m11149_gshared (List_1_t703 * __this, Comparison_1_t1867 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m11149(__this, ___comparison, method) (( void (*) (List_1_t703 *, Comparison_1_t1867 *, const MethodInfo*))List_1_Sort_m11149_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t115* List_1_ToArray_m11150_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_ToArray_m11150(__this, method) (( ObjectU5BU5D_t115* (*) (List_1_t703 *, const MethodInfo*))List_1_ToArray_m11150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m11152_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m11152(__this, method) (( void (*) (List_1_t703 *, const MethodInfo*))List_1_TrimExcess_m11152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m11154_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m11154(__this, method) (( int32_t (*) (List_1_t703 *, const MethodInfo*))List_1_get_Capacity_m11154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m11156_gshared (List_1_t703 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m11156(__this, ___value, method) (( void (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11156_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m11158_gshared (List_1_t703 * __this, const MethodInfo* method);
#define List_1_get_Count_m11158(__this, method) (( int32_t (*) (List_1_t703 *, const MethodInfo*))List_1_get_Count_m11158_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m11160_gshared (List_1_t703 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m11160(__this, ___index, method) (( Object_t * (*) (List_1_t703 *, int32_t, const MethodInfo*))List_1_get_Item_m11160_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m11162_gshared (List_1_t703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m11162(__this, ___index, ___value, method) (( void (*) (List_1_t703 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m11162_gshared)(__this, ___index, ___value, method)
