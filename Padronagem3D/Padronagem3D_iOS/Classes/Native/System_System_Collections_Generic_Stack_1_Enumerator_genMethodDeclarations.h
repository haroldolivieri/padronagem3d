﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t1894;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m11596_gshared (Enumerator_t1896 * __this, Stack_1_t1894 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m11596(__this, ___t, method) (( void (*) (Enumerator_t1896 *, Stack_1_t1894 *, const MethodInfo*))Enumerator__ctor_m11596_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m11597_gshared (Enumerator_t1896 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m11597(__this, method) (( void (*) (Enumerator_t1896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11597_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11598_gshared (Enumerator_t1896 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11598(__this, method) (( Object_t * (*) (Enumerator_t1896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11598_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m11599_gshared (Enumerator_t1896 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11599(__this, method) (( void (*) (Enumerator_t1896 *, const MethodInfo*))Enumerator_Dispose_m11599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11600_gshared (Enumerator_t1896 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11600(__this, method) (( bool (*) (Enumerator_t1896 *, const MethodInfo*))Enumerator_MoveNext_m11600_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m11601_gshared (Enumerator_t1896 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11601(__this, method) (( Object_t * (*) (Enumerator_t1896 *, const MethodInfo*))Enumerator_get_Current_m11601_gshared)(__this, method)
