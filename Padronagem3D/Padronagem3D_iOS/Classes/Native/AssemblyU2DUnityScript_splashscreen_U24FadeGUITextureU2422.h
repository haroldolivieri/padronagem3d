﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUITexture
struct GUITexture_t141;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen_1.h"
#include "AssemblyU2DUnityScript_Fade.h"

// splashscreen/$FadeGUITexture$22
struct  U24FadeGUITextureU2422_t142  : public GenericGenerator_1_t143
{
	// UnityEngine.GUITexture splashscreen/$FadeGUITexture$22::$guiObject$32
	GUITexture_t141 * ___U24guiObjectU2432_0;
	// System.Single splashscreen/$FadeGUITexture$22::$timer$33
	float ___U24timerU2433_1;
	// Fade splashscreen/$FadeGUITexture$22::$fadeType$34
	int32_t ___U24fadeTypeU2434_2;
};
