﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t2369;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t874;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17810_gshared (ShimEnumerator_t2369 * __this, Dictionary_2_t874 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17810(__this, ___host, method) (( void (*) (ShimEnumerator_t2369 *, Dictionary_2_t874 *, const MethodInfo*))ShimEnumerator__ctor_m17810_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17811_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17811(__this, method) (( bool (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_MoveNext_m17811_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t943  ShimEnumerator_get_Entry_m17812_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17812(__this, method) (( DictionaryEntry_t943  (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_get_Entry_m17812_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17813_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17813(__this, method) (( Object_t * (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_get_Key_m17813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17814_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17814(__this, method) (( Object_t * (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_get_Value_m17814_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17815_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17815(__this, method) (( Object_t * (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_get_Current_m17815_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::Reset()
extern "C" void ShimEnumerator_Reset_m17816_gshared (ShimEnumerator_t2369 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m17816(__this, method) (( void (*) (ShimEnumerator_t2369 *, const MethodInfo*))ShimEnumerator_Reset_m17816_gshared)(__this, method)
