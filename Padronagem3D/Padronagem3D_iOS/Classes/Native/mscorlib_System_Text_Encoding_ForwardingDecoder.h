﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t111;

#include "mscorlib_System_Text_Decoder.h"

// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t1654  : public Decoder_t1278
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t111 * ___encoding_2;
};
