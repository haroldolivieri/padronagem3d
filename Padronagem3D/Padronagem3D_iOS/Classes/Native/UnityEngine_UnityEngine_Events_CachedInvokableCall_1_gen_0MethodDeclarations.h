﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t476;
// UnityEngine.Object
struct Object_t82;
struct Object_t82_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t115;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C" void CachedInvokableCall_1__ctor_m2229_gshared (CachedInvokableCall_1_t476 * __this, Object_t82 * ___target, MethodInfo_t * ___theFunction, int32_t ___argument, const MethodInfo* method);
#define CachedInvokableCall_1__ctor_m2229(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t476 *, Object_t82 *, MethodInfo_t *, int32_t, const MethodInfo*))CachedInvokableCall_1__ctor_m2229_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C" void CachedInvokableCall_1_Invoke_m14530_gshared (CachedInvokableCall_1_t476 * __this, ObjectU5BU5D_t115* ___args, const MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m14530(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t476 *, ObjectU5BU5D_t115*, const MethodInfo*))CachedInvokableCall_1_Invoke_m14530_gshared)(__this, ___args, method)
