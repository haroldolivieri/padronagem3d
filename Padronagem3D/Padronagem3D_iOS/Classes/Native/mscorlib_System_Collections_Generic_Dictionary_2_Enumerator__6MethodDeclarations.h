﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2112;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14416_gshared (Enumerator_t2119 * __this, Dictionary_2_t2112 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m14416(__this, ___dictionary, method) (( void (*) (Enumerator_t2119 *, Dictionary_2_t2112 *, const MethodInfo*))Enumerator__ctor_m14416_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14417_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14417(__this, method) (( Object_t * (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14417_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m14418_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m14418(__this, method) (( void (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14418_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t943  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14419_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14419(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14419_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14420_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14420(__this, method) (( Object_t * (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14420_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14421_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14421(__this, method) (( Object_t * (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14421_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14422_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14422(__this, method) (( bool (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_MoveNext_m14422_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t2114  Enumerator_get_Current_m14423_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14423(__this, method) (( KeyValuePair_2_t2114  (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_get_Current_m14423_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m14424_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m14424(__this, method) (( Object_t * (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_get_CurrentKey_m14424_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m14425_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m14425(__this, method) (( int32_t (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_get_CurrentValue_m14425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void Enumerator_Reset_m14426_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_Reset_m14426(__this, method) (( void (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_Reset_m14426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m14427_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14427(__this, method) (( void (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_VerifyState_m14427_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m14428_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m14428(__this, method) (( void (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_VerifyCurrent_m14428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m14429_gshared (Enumerator_t2119 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14429(__this, method) (( void (*) (Enumerator_t2119 *, const MethodInfo*))Enumerator_Dispose_m14429_gshared)(__this, method)
