﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13646(__this, ___dictionary, method) (( void (*) (Enumerator_t2059 *, Dictionary_2_t300 *, const MethodInfo*))Enumerator__ctor_m13583_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13647(__this, method) (( Object_t * (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13584_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m13648(__this, method) (( void (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13585_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13649(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13586_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13650(__this, method) (( Object_t * (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13587_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13651(__this, method) (( Object_t * (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13588_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m13652(__this, method) (( bool (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_MoveNext_m13589_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m13653(__this, method) (( KeyValuePair_2_t2057  (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_get_Current_m13590_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13654(__this, method) (( String_t* (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_get_CurrentKey_m13591_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13655(__this, method) (( int32_t (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_get_CurrentValue_m13592_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Reset()
#define Enumerator_Reset_m13656(__this, method) (( void (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_Reset_m13593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m13657(__this, method) (( void (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_VerifyState_m13594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13658(__this, method) (( void (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_VerifyCurrent_m13595_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m13659(__this, method) (( void (*) (Enumerator_t2059 *, const MethodInfo*))Enumerator_Dispose_m13596_gshared)(__this, method)
