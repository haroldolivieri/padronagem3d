﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DUnityScript_Fade.h"

// Fade
struct  Fade_t133 
{
	// System.Int32 Fade::value__
	int32_t ___value___1;
};
