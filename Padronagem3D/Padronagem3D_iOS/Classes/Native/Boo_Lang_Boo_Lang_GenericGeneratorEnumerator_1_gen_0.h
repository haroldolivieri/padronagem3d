﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.YieldInstruction
struct YieldInstruction_t164;
struct YieldInstruction_t164_marshaled;

#include "mscorlib_System_Object.h"

// Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>
struct  GenericGeneratorEnumerator_1_t135  : public Object_t
{
	// T Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::_current
	YieldInstruction_t164 * ____current_0;
	// System.Int32 Boo.Lang.GenericGeneratorEnumerator`1<UnityEngine.YieldInstruction>::_state
	int32_t ____state_1;
};
