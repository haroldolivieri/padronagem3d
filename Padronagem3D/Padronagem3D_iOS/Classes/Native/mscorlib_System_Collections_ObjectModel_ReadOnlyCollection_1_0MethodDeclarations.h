﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t1813;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1372;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1811;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t2543;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m18225_gshared (ReadOnlyCollection_1_t1813 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m18225(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1813 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m18225_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18226_gshared (ReadOnlyCollection_1_t1813 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18226(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1813 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18226_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18227_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18227(__this, method) (( void (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18227_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18228_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18228(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18228_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18229_gshared (ReadOnlyCollection_1_t1813 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18229(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18229_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18230_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18230(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18230_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1373  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18231_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18231(__this, ___index, method) (( CustomAttributeNamedArgument_t1373  (*) (ReadOnlyCollection_1_t1813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18231_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18232_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, CustomAttributeNamedArgument_t1373  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18232(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18232_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18233_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18233(__this, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18233_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18234_gshared (ReadOnlyCollection_1_t1813 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18234(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1813 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18234_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18235_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18235(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18235_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m18236_gshared (ReadOnlyCollection_1_t1813 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m18236(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1813 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m18236_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m18237_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m18237(__this, method) (( void (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m18237_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m18238_gshared (ReadOnlyCollection_1_t1813 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m18238(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m18238_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18239_gshared (ReadOnlyCollection_1_t1813 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18239(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1813 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18239_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m18240_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m18240(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m18240_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m18241_gshared (ReadOnlyCollection_1_t1813 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m18241(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1813 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m18241_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18242_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18242(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18242_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18243_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18243(__this, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18243_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18244_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18244(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18244_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18245_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18245(__this, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18245_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18246_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18246(__this, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18246_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m18247_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m18247(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m18247_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m18248_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m18248(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1813 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m18248_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m18249_gshared (ReadOnlyCollection_1_t1813 * __this, CustomAttributeNamedArgument_t1373  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m18249(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1813 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_Contains_m18249_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m18250_gshared (ReadOnlyCollection_1_t1813 * __this, CustomAttributeNamedArgumentU5BU5D_t1811* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m18250(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1813 *, CustomAttributeNamedArgumentU5BU5D_t1811*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m18250_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m18251_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m18251(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m18251_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m18252_gshared (ReadOnlyCollection_1_t1813 * __this, CustomAttributeNamedArgument_t1373  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m18252(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1813 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m18252_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m18253_gshared (ReadOnlyCollection_1_t1813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m18253(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1813 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m18253_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1373  ReadOnlyCollection_1_get_Item_m18254_gshared (ReadOnlyCollection_1_t1813 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m18254(__this, ___index, method) (( CustomAttributeNamedArgument_t1373  (*) (ReadOnlyCollection_1_t1813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m18254_gshared)(__this, ___index, method)
