﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t814;

#include "mscorlib_System_IO_StreamReader.h"

// System.IO.UnexceptionalStreamReader
struct  UnexceptionalStreamReader_t1310  : public StreamReader_t1303
{
};
struct UnexceptionalStreamReader_t1310_StaticFields{
	// System.Boolean[] System.IO.UnexceptionalStreamReader::newline
	BooleanU5BU5D_t814* ___newline_14;
	// System.Char System.IO.UnexceptionalStreamReader::newlineChar
	uint16_t ___newlineChar_15;
};
