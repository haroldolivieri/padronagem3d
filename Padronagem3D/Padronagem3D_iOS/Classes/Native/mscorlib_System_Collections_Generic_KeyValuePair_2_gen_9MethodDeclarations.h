﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16188(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2242 *, Canvas_t294 *, IndexedSet_1_t698 *, const MethodInfo*))KeyValuePair_2__ctor_m14045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define KeyValuePair_2_get_Key_m16189(__this, method) (( Canvas_t294 * (*) (KeyValuePair_2_t2242 *, const MethodInfo*))KeyValuePair_2_get_Key_m14046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16190(__this, ___value, method) (( void (*) (KeyValuePair_2_t2242 *, Canvas_t294 *, const MethodInfo*))KeyValuePair_2_set_Key_m14047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define KeyValuePair_2_get_Value_m16191(__this, method) (( IndexedSet_1_t698 * (*) (KeyValuePair_2_t2242 *, const MethodInfo*))KeyValuePair_2_get_Value_m14048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16192(__this, ___value, method) (( void (*) (KeyValuePair_2_t2242 *, IndexedSet_1_t698 *, const MethodInfo*))KeyValuePair_2_set_Value_m14049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::ToString()
#define KeyValuePair_2_ToString_m16193(__this, method) (( String_t* (*) (KeyValuePair_2_t2242 *, const MethodInfo*))KeyValuePair_2_ToString_m14050_gshared)(__this, method)
