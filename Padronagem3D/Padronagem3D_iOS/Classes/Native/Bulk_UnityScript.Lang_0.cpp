﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Enum
struct Enum_t150;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityScript_Lang_U3CModuleU3E.h"
#include "UnityScript_Lang_U3CModuleU3EMethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions.h"
#include "UnityScript_Lang_UnityScript_Lang_ExtensionsMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Int32.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityScript.Lang.Extensions::op_Implicit(System.Enum)
extern TypeInfo* IConvertible_t737_il2cpp_TypeInfo_var;
extern "C" bool Extensions_op_Implicit_m678 (Object_t * __this /* static, unused */, Enum_t150 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConvertible_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(461);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t150 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(7 /* System.Int32 System.IConvertible::ToInt32(System.IFormatProvider) */, IConvertible_t737_il2cpp_TypeInfo_var, L_0, (Object_t *)NULL);
		return ((((int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
