﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DiveFPSController
struct DiveFPSController_t128;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen.h"

// DiveFPSController/$Die$8
struct  U24DieU248_t129  : public GenericGenerator_1_t130
{
	// DiveFPSController DiveFPSController/$Die$8::$self_$16
	DiveFPSController_t128 * ___U24self_U2416_0;
};
