﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"


extern Il2CppGenericClass* s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[];
extern Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
extern const EncodedMethodIndex  g_Il2CppMethodReferenceTable[];
extern const int32_t g_FieldOffsetTable[];
extern const Il2CppTypeDefinitionSizes g_Il2CppTypeDefinitionSizesTable[];
extern const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	1844,
	s_Il2CppGenericTypes,
	546,
	g_Il2CppGenericInstTable,
	4378,
	s_Il2CppGenericMethodFunctions,
	7253,
	g_Il2CppTypeTable,
	4677,
	g_Il2CppMethodSpecTable,
	4203,
	g_Il2CppMethodReferenceTable,
	6860,
	g_FieldOffsetTable,
	1694,
	g_Il2CppTypeDefinitionSizesTable,
};
