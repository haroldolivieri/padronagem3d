﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardboardUILayer
struct CardboardUILayer_t26;

#include "codegen/il2cpp-codegen.h"

// System.Void CardboardUILayer::.ctor()
extern "C" void CardboardUILayer__ctor_m143 (CardboardUILayer_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardUILayer::.cctor()
extern "C" void CardboardUILayer__cctor_m144 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardUILayer::ComputeMatrix()
extern "C" void CardboardUILayer_ComputeMatrix_m145 (CardboardUILayer_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardUILayer::Draw()
extern "C" void CardboardUILayer_Draw_m146 (CardboardUILayer_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
