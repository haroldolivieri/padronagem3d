﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2024;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t432;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t2492;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t435;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m13250_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13250(__this, method) (( void (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1__ctor_m13250_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13251_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13251(__this, method) (( bool (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13251_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13252_gshared (Collection_1_t2024 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13252(__this, ___array, ___index, method) (( void (*) (Collection_1_t2024 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13252_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13253_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13253(__this, method) (( Object_t * (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13253_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13254_gshared (Collection_1_t2024 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13254(__this, ___value, method) (( int32_t (*) (Collection_1_t2024 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13254_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13255_gshared (Collection_1_t2024 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13255(__this, ___value, method) (( bool (*) (Collection_1_t2024 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13255_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13256_gshared (Collection_1_t2024 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13256(__this, ___value, method) (( int32_t (*) (Collection_1_t2024 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13256_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13257_gshared (Collection_1_t2024 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13257(__this, ___index, ___value, method) (( void (*) (Collection_1_t2024 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13257_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13258_gshared (Collection_1_t2024 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13258(__this, ___value, method) (( void (*) (Collection_1_t2024 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13258_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13259_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13259(__this, method) (( bool (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13259_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13260_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13260(__this, method) (( Object_t * (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13260_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13261_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13261(__this, method) (( bool (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13261_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13262_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13262(__this, method) (( bool (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13262_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13263_gshared (Collection_1_t2024 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13263(__this, ___index, method) (( Object_t * (*) (Collection_1_t2024 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13263_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13264_gshared (Collection_1_t2024 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13264(__this, ___index, ___value, method) (( void (*) (Collection_1_t2024 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13264_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m13265_gshared (Collection_1_t2024 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_Add_m13265(__this, ___item, method) (( void (*) (Collection_1_t2024 *, UICharInfo_t285 , const MethodInfo*))Collection_1_Add_m13265_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m13266_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13266(__this, method) (( void (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_Clear_m13266_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m13267_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13267(__this, method) (( void (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_ClearItems_m13267_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m13268_gshared (Collection_1_t2024 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13268(__this, ___item, method) (( bool (*) (Collection_1_t2024 *, UICharInfo_t285 , const MethodInfo*))Collection_1_Contains_m13268_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13269_gshared (Collection_1_t2024 * __this, UICharInfoU5BU5D_t432* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13269(__this, ___array, ___index, method) (( void (*) (Collection_1_t2024 *, UICharInfoU5BU5D_t432*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13269_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13270_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13270(__this, method) (( Object_t* (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_GetEnumerator_m13270_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13271_gshared (Collection_1_t2024 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13271(__this, ___item, method) (( int32_t (*) (Collection_1_t2024 *, UICharInfo_t285 , const MethodInfo*))Collection_1_IndexOf_m13271_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13272_gshared (Collection_1_t2024 * __this, int32_t ___index, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13272(__this, ___index, ___item, method) (( void (*) (Collection_1_t2024 *, int32_t, UICharInfo_t285 , const MethodInfo*))Collection_1_Insert_m13272_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13273_gshared (Collection_1_t2024 * __this, int32_t ___index, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13273(__this, ___index, ___item, method) (( void (*) (Collection_1_t2024 *, int32_t, UICharInfo_t285 , const MethodInfo*))Collection_1_InsertItem_m13273_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m13274_gshared (Collection_1_t2024 * __this, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13274(__this, ___item, method) (( bool (*) (Collection_1_t2024 *, UICharInfo_t285 , const MethodInfo*))Collection_1_Remove_m13274_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13275_gshared (Collection_1_t2024 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13275(__this, ___index, method) (( void (*) (Collection_1_t2024 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13275_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13276_gshared (Collection_1_t2024 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13276(__this, ___index, method) (( void (*) (Collection_1_t2024 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13276_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13277_gshared (Collection_1_t2024 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13277(__this, method) (( int32_t (*) (Collection_1_t2024 *, const MethodInfo*))Collection_1_get_Count_m13277_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t285  Collection_1_get_Item_m13278_gshared (Collection_1_t2024 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13278(__this, ___index, method) (( UICharInfo_t285  (*) (Collection_1_t2024 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13278_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13279_gshared (Collection_1_t2024 * __this, int32_t ___index, UICharInfo_t285  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13279(__this, ___index, ___value, method) (( void (*) (Collection_1_t2024 *, int32_t, UICharInfo_t285 , const MethodInfo*))Collection_1_set_Item_m13279_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13280_gshared (Collection_1_t2024 * __this, int32_t ___index, UICharInfo_t285  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13280(__this, ___index, ___item, method) (( void (*) (Collection_1_t2024 *, int32_t, UICharInfo_t285 , const MethodInfo*))Collection_1_SetItem_m13280_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13281_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13281(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13281_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t285  Collection_1_ConvertItem_m13282_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13282(__this /* static, unused */, ___item, method) (( UICharInfo_t285  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13282_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13283_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13283(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13283_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13284_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13284(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13284_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13285_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13285(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13285_gshared)(__this /* static, unused */, ___list, method)
