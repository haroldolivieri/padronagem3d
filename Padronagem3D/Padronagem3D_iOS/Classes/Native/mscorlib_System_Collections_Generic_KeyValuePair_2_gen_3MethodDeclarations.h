﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m13822(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2074 *, int32_t, LayoutCache_t311 *, const MethodInfo*))KeyValuePair_2__ctor_m13759_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
#define KeyValuePair_2_get_Key_m13823(__this, method) (( int32_t (*) (KeyValuePair_2_t2074 *, const MethodInfo*))KeyValuePair_2_get_Key_m13760_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m13824(__this, ___value, method) (( void (*) (KeyValuePair_2_t2074 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m13761_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
#define KeyValuePair_2_get_Value_m13825(__this, method) (( LayoutCache_t311 * (*) (KeyValuePair_2_t2074 *, const MethodInfo*))KeyValuePair_2_get_Value_m13762_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m13826(__this, ___value, method) (( void (*) (KeyValuePair_2_t2074 *, LayoutCache_t311 *, const MethodInfo*))KeyValuePair_2_set_Value_m13763_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
#define KeyValuePair_2_ToString_m13827(__this, method) (( String_t* (*) (KeyValuePair_2_t2074 *, const MethodInfo*))KeyValuePair_2_ToString_m13764_gshared)(__this, method)
