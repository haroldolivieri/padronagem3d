﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Renderer_t76_0_0_0;
static const Il2CppType* GenInst_Renderer_t76_0_0_0_Types[] = { &Renderer_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t76_0_0_0 = { 1, GenInst_Renderer_t76_0_0_0_Types };
extern const Il2CppType StereoController_t31_0_0_0;
static const Il2CppType* GenInst_StereoController_t31_0_0_0_Types[] = { &StereoController_t31_0_0_0 };
extern const Il2CppGenericInst GenInst_StereoController_t31_0_0_0 = { 1, GenInst_StereoController_t31_0_0_0_Types };
extern const Il2CppType Collider_t79_0_0_0;
static const Il2CppType* GenInst_Collider_t79_0_0_0_Types[] = { &Collider_t79_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t79_0_0_0 = { 1, GenInst_Collider_t79_0_0_0_Types };
extern const Il2CppType CardboardOnGUIWindow_t16_0_0_0;
static const Il2CppType* GenInst_CardboardOnGUIWindow_t16_0_0_0_Types[] = { &CardboardOnGUIWindow_t16_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardOnGUIWindow_t16_0_0_0 = { 1, GenInst_CardboardOnGUIWindow_t16_0_0_0_Types };
extern const Il2CppType CardboardOnGUIMouse_t13_0_0_0;
static const Il2CppType* GenInst_CardboardOnGUIMouse_t13_0_0_0_Types[] = { &CardboardOnGUIMouse_t13_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardOnGUIMouse_t13_0_0_0 = { 1, GenInst_CardboardOnGUIMouse_t13_0_0_0_Types };
extern const Il2CppType MeshRenderer_t17_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t17_0_0_0_Types[] = { &MeshRenderer_t17_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t17_0_0_0 = { 1, GenInst_MeshRenderer_t17_0_0_0_Types };
extern const Il2CppType Cardboard_t24_0_0_0;
static const Il2CppType* GenInst_Cardboard_t24_0_0_0_Types[] = { &Cardboard_t24_0_0_0 };
extern const Il2CppGenericInst GenInst_Cardboard_t24_0_0_0 = { 1, GenInst_Cardboard_t24_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Camera_t32_0_0_0;
static const Il2CppType* GenInst_Camera_t32_0_0_0_Types[] = { &Camera_t32_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t32_0_0_0 = { 1, GenInst_Camera_t32_0_0_0_Types };
extern const Il2CppType CardboardHead_t5_0_0_0;
static const Il2CppType* GenInst_CardboardHead_t5_0_0_0_Types[] = { &CardboardHead_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardHead_t5_0_0_0 = { 1, GenInst_CardboardHead_t5_0_0_0_Types };
extern const Il2CppType RadialUndistortionEffect_t52_0_0_0;
static const Il2CppType* GenInst_RadialUndistortionEffect_t52_0_0_0_Types[] = { &RadialUndistortionEffect_t52_0_0_0 };
extern const Il2CppGenericInst GenInst_RadialUndistortionEffect_t52_0_0_0 = { 1, GenInst_RadialUndistortionEffect_t52_0_0_0_Types };
extern const Il2CppType ISelectHandler_t95_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t95_0_0_0_Types[] = { &ISelectHandler_t95_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t95_0_0_0 = { 1, GenInst_ISelectHandler_t95_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t98_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t98_0_0_0_Types[] = { &IUpdateSelectedHandler_t98_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t98_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t98_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t100_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t100_0_0_0_Types[] = { &IPointerUpHandler_t100_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t100_0_0_0 = { 1, GenInst_IPointerUpHandler_t100_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t102_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t102_0_0_0_Types[] = { &IPointerClickHandler_t102_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t102_0_0_0 = { 1, GenInst_IPointerClickHandler_t102_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t104_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t104_0_0_0_Types[] = { &IPointerDownHandler_t104_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t104_0_0_0 = { 1, GenInst_IPointerDownHandler_t104_0_0_0_Types };
extern const Il2CppType CardboardEye_t29_0_0_0;
static const Il2CppType* GenInst_CardboardEye_t29_0_0_0_Types[] = { &CardboardEye_t29_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t29_0_0_0 = { 1, GenInst_CardboardEye_t29_0_0_0_Types };
extern const Il2CppType Boolean_t455_0_0_0;
static const Il2CppType* GenInst_CardboardEye_t29_0_0_0_Boolean_t455_0_0_0_Types[] = { &CardboardEye_t29_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t29_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_CardboardEye_t29_0_0_0_Boolean_t455_0_0_0_Types };
static const Il2CppType* GenInst_CardboardEye_t29_0_0_0_CardboardHead_t5_0_0_0_Types[] = { &CardboardEye_t29_0_0_0, &CardboardHead_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t29_0_0_0_CardboardHead_t5_0_0_0 = { 2, GenInst_CardboardEye_t29_0_0_0_CardboardHead_t5_0_0_0_Types };
extern const Il2CppType Int32_t106_0_0_0;
static const Il2CppType* GenInst_Int32_t106_0_0_0_Types[] = { &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0 = { 1, GenInst_Int32_t106_0_0_0_Types };
extern const Il2CppType Rigidbody_t112_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t112_0_0_0_Types[] = { &Rigidbody_t112_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t112_0_0_0 = { 1, GenInst_Rigidbody_t112_0_0_0_Types };
extern const Il2CppType AsyncOperation_t127_0_0_0;
static const Il2CppType* GenInst_AsyncOperation_t127_0_0_0_Types[] = { &AsyncOperation_t127_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncOperation_t127_0_0_0 = { 1, GenInst_AsyncOperation_t127_0_0_0_Types };
extern const Il2CppType GUITexture_t141_0_0_0;
static const Il2CppType* GenInst_GUITexture_t141_0_0_0_Types[] = { &GUITexture_t141_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t141_0_0_0 = { 1, GenInst_GUITexture_t141_0_0_0_Types };
extern const Il2CppType GUIText_t152_0_0_0;
static const Il2CppType* GenInst_GUIText_t152_0_0_0_Types[] = { &GUIText_t152_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t152_0_0_0 = { 1, GenInst_GUIText_t152_0_0_0_Types };
extern const Il2CppType YieldInstruction_t164_0_0_0;
static const Il2CppType* GenInst_YieldInstruction_t164_0_0_0_Types[] = { &YieldInstruction_t164_0_0_0 };
extern const Il2CppGenericInst GenInst_YieldInstruction_t164_0_0_0 = { 1, GenInst_YieldInstruction_t164_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t178_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t178_0_0_0_Types[] = { &GcLeaderboard_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t178_0_0_0 = { 1, GenInst_GcLeaderboard_t178_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t446_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t446_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t446_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t446_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t446_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t455_0_0_0_Types[] = { &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t455_0_0_0 = { 1, GenInst_Boolean_t455_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t448_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t448_0_0_0_Types[] = { &IAchievementU5BU5D_t448_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t448_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t448_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t365_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t365_0_0_0_Types[] = { &IScoreU5BU5D_t365_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t365_0_0_0 = { 1, GenInst_IScoreU5BU5D_t365_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t361_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t361_0_0_0_Types[] = { &IUserProfileU5BU5D_t361_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t361_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t361_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t455_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t254_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t254_0_0_0_Types[] = { &Rigidbody2D_t254_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t254_0_0_0 = { 1, GenInst_Rigidbody2D_t254_0_0_0_Types };
extern const Il2CppType Font_t283_0_0_0;
static const Il2CppType* GenInst_Font_t283_0_0_0_Types[] = { &Font_t283_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t283_0_0_0 = { 1, GenInst_Font_t283_0_0_0_Types };
extern const Il2CppType UIVertex_t296_0_0_0;
static const Il2CppType* GenInst_UIVertex_t296_0_0_0_Types[] = { &UIVertex_t296_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t296_0_0_0 = { 1, GenInst_UIVertex_t296_0_0_0_Types };
extern const Il2CppType UICharInfo_t285_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t285_0_0_0_Types[] = { &UICharInfo_t285_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t285_0_0_0 = { 1, GenInst_UICharInfo_t285_0_0_0_Types };
extern const Il2CppType UILineInfo_t286_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t286_0_0_0_Types[] = { &UILineInfo_t286_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t286_0_0_0 = { 1, GenInst_UILineInfo_t286_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t106_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t106_0_0_0_Types };
extern const Il2CppType LayoutCache_t311_0_0_0;
static const Il2CppType* GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_Types[] = { &Int32_t106_0_0_0, &LayoutCache_t311_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0 = { 2, GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t316_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t316_0_0_0_Types[] = { &GUILayoutEntry_t316_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t316_0_0_0 = { 1, GenInst_GUILayoutEntry_t316_0_0_0_Types };
extern const Il2CppType GUIStyle_t315_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t192_0_0_0;
static const Il2CppType* GenInst_GUILayer_t192_0_0_0_Types[] = { &GUILayer_t192_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t192_0_0_0 = { 1, GenInst_GUILayer_t192_0_0_0_Types };
extern const Il2CppType Single_t113_0_0_0;
static const Il2CppType* GenInst_Single_t113_0_0_0_Types[] = { &Single_t113_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t113_0_0_0 = { 1, GenInst_Single_t113_0_0_0_Types };
extern const Il2CppType PersistentCall_t396_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t396_0_0_0_Types[] = { &PersistentCall_t396_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t396_0_0_0 = { 1, GenInst_PersistentCall_t396_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t392_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t392_0_0_0_Types[] = { &BaseInvokableCall_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t392_0_0_0 = { 1, GenInst_BaseInvokableCall_t392_0_0_0_Types };
extern const Il2CppType BaseInputModule_t46_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t46_0_0_0_Types[] = { &BaseInputModule_t46_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t46_0_0_0 = { 1, GenInst_BaseInputModule_t46_0_0_0_Types };
extern const Il2CppType RaycastResult_t94_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t94_0_0_0_Types[] = { &RaycastResult_t94_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t94_0_0_0 = { 1, GenInst_RaycastResult_t94_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t668_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t668_0_0_0_Types[] = { &IDeselectHandler_t668_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t668_0_0_0 = { 1, GenInst_IDeselectHandler_t668_0_0_0_Types };
extern const Il2CppType BaseEventData_t92_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t92_0_0_0_Types[] = { &BaseEventData_t92_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t92_0_0_0 = { 1, GenInst_BaseEventData_t92_0_0_0_Types };
extern const Il2CppType Entry_t489_0_0_0;
static const Il2CppType* GenInst_Entry_t489_0_0_0_Types[] = { &Entry_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t489_0_0_0 = { 1, GenInst_Entry_t489_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t660_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t660_0_0_0_Types[] = { &IPointerEnterHandler_t660_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t660_0_0_0 = { 1, GenInst_IPointerEnterHandler_t660_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t661_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t661_0_0_0_Types[] = { &IPointerExitHandler_t661_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t661_0_0_0 = { 1, GenInst_IPointerExitHandler_t661_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t662_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t662_0_0_0_Types[] = { &IInitializePotentialDragHandler_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t662_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t662_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t663_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t663_0_0_0_Types[] = { &IBeginDragHandler_t663_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t663_0_0_0 = { 1, GenInst_IBeginDragHandler_t663_0_0_0_Types };
extern const Il2CppType IDragHandler_t664_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t664_0_0_0_Types[] = { &IDragHandler_t664_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t664_0_0_0 = { 1, GenInst_IDragHandler_t664_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t665_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t665_0_0_0_Types[] = { &IEndDragHandler_t665_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t665_0_0_0 = { 1, GenInst_IEndDragHandler_t665_0_0_0_Types };
extern const Il2CppType IDropHandler_t666_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t666_0_0_0_Types[] = { &IDropHandler_t666_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t666_0_0_0 = { 1, GenInst_IDropHandler_t666_0_0_0_Types };
extern const Il2CppType IScrollHandler_t667_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t667_0_0_0_Types[] = { &IScrollHandler_t667_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t667_0_0_0 = { 1, GenInst_IScrollHandler_t667_0_0_0_Types };
extern const Il2CppType IMoveHandler_t669_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t669_0_0_0_Types[] = { &IMoveHandler_t669_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t669_0_0_0 = { 1, GenInst_IMoveHandler_t669_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t670_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t670_0_0_0_Types[] = { &ISubmitHandler_t670_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t670_0_0_0 = { 1, GenInst_ISubmitHandler_t670_0_0_0_Types };
extern const Il2CppType ICancelHandler_t671_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t671_0_0_0_Types[] = { &ICancelHandler_t671_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t671_0_0_0 = { 1, GenInst_ICancelHandler_t671_0_0_0_Types };
extern const Il2CppType List_1_t659_0_0_0;
static const Il2CppType* GenInst_List_1_t659_0_0_0_Types[] = { &List_1_t659_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t659_0_0_0 = { 1, GenInst_List_1_t659_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t1889_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t1889_0_0_0_Types[] = { &IEventSystemHandler_t1889_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t1889_0_0_0 = { 1, GenInst_IEventSystemHandler_t1889_0_0_0_Types };
extern const Il2CppType Transform_t33_0_0_0;
static const Il2CppType* GenInst_Transform_t33_0_0_0_Types[] = { &Transform_t33_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t33_0_0_0 = { 1, GenInst_Transform_t33_0_0_0_Types };
extern const Il2CppType PointerEventData_t48_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t48_0_0_0_Types[] = { &PointerEventData_t48_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t48_0_0_0 = { 1, GenInst_PointerEventData_t48_0_0_0_Types };
extern const Il2CppType AxisEventData_t513_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t513_0_0_0_Types[] = { &AxisEventData_t513_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t513_0_0_0 = { 1, GenInst_AxisEventData_t513_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t512_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t512_0_0_0_Types[] = { &BaseRaycaster_t512_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t512_0_0_0 = { 1, GenInst_BaseRaycaster_t512_0_0_0_Types };
extern const Il2CppType GameObject_t47_0_0_0;
static const Il2CppType* GenInst_GameObject_t47_0_0_0_Types[] = { &GameObject_t47_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t47_0_0_0 = { 1, GenInst_GameObject_t47_0_0_0_Types };
extern const Il2CppType EventSystem_t91_0_0_0;
static const Il2CppType* GenInst_EventSystem_t91_0_0_0_Types[] = { &EventSystem_t91_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t91_0_0_0 = { 1, GenInst_EventSystem_t91_0_0_0_Types };
extern const Il2CppType ButtonState_t517_0_0_0;
static const Il2CppType* GenInst_ButtonState_t517_0_0_0_Types[] = { &ButtonState_t517_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t517_0_0_0 = { 1, GenInst_ButtonState_t517_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_Types[] = { &Int32_t106_0_0_0, &PointerEventData_t48_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0 = { 2, GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t220_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t220_0_0_0_Types[] = { &SpriteRenderer_t220_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t220_0_0_0 = { 1, GenInst_SpriteRenderer_t220_0_0_0_Types };
extern const Il2CppType RaycastHit_t78_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t78_0_0_0_Types[] = { &RaycastHit_t78_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t78_0_0_0 = { 1, GenInst_RaycastHit_t78_0_0_0_Types };
extern const Il2CppType Color_t11_0_0_0;
static const Il2CppType* GenInst_Color_t11_0_0_0_Types[] = { &Color_t11_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t11_0_0_0 = { 1, GenInst_Color_t11_0_0_0_Types };
extern const Il2CppType ICanvasElement_t675_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t675_0_0_0_Types[] = { &ICanvasElement_t675_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t675_0_0_0 = { 1, GenInst_ICanvasElement_t675_0_0_0_Types };
extern const Il2CppType Dropdown_t557_0_0_0;
static const Il2CppType* GenInst_Dropdown_t557_0_0_0_Types[] = { &Dropdown_t557_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t557_0_0_0 = { 1, GenInst_Dropdown_t557_0_0_0_Types };
extern const Il2CppType OptionData_t551_0_0_0;
static const Il2CppType* GenInst_OptionData_t551_0_0_0_Types[] = { &OptionData_t551_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t551_0_0_0 = { 1, GenInst_OptionData_t551_0_0_0_Types };
extern const Il2CppType DropdownItem_t547_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t547_0_0_0_Types[] = { &DropdownItem_t547_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t547_0_0_0 = { 1, GenInst_DropdownItem_t547_0_0_0_Types };
extern const Il2CppType FloatTween_t535_0_0_0;
static const Il2CppType* GenInst_FloatTween_t535_0_0_0_Types[] = { &FloatTween_t535_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t535_0_0_0 = { 1, GenInst_FloatTween_t535_0_0_0_Types };
extern const Il2CppType Toggle_t550_0_0_0;
static const Il2CppType* GenInst_Toggle_t550_0_0_0_Types[] = { &Toggle_t550_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t550_0_0_0 = { 1, GenInst_Toggle_t550_0_0_0_Types };
extern const Il2CppType Canvas_t294_0_0_0;
static const Il2CppType* GenInst_Canvas_t294_0_0_0_Types[] = { &Canvas_t294_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t294_0_0_0 = { 1, GenInst_Canvas_t294_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t567_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t567_0_0_0_Types[] = { &GraphicRaycaster_t567_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t567_0_0_0 = { 1, GenInst_GraphicRaycaster_t567_0_0_0_Types };
extern const Il2CppType CanvasGroup_t295_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t295_0_0_0_Types[] = { &CanvasGroup_t295_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t295_0_0_0 = { 1, GenInst_CanvasGroup_t295_0_0_0_Types };
extern const Il2CppType RectTransform_t211_0_0_0;
static const Il2CppType* GenInst_RectTransform_t211_0_0_0_Types[] = { &RectTransform_t211_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t211_0_0_0 = { 1, GenInst_RectTransform_t211_0_0_0_Types };
extern const Il2CppType Image_t549_0_0_0;
static const Il2CppType* GenInst_Image_t549_0_0_0_Types[] = { &Image_t549_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t549_0_0_0 = { 1, GenInst_Image_t549_0_0_0_Types };
extern const Il2CppType Button_t539_0_0_0;
static const Il2CppType* GenInst_Button_t539_0_0_0_Types[] = { &Button_t539_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t539_0_0_0 = { 1, GenInst_Button_t539_0_0_0_Types };
extern const Il2CppType List_1_t690_0_0_0;
static const Il2CppType* GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_Types[] = { &Font_t283_0_0_0, &List_1_t690_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t283_0_0_0_List_1_t690_0_0_0 = { 2, GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_Types };
extern const Il2CppType Text_t548_0_0_0;
static const Il2CppType* GenInst_Text_t548_0_0_0_Types[] = { &Text_t548_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t548_0_0_0 = { 1, GenInst_Text_t548_0_0_0_Types };
extern const Il2CppType ColorTween_t532_0_0_0;
static const Il2CppType* GenInst_ColorTween_t532_0_0_0_Types[] = { &ColorTween_t532_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t532_0_0_0 = { 1, GenInst_ColorTween_t532_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t297_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t297_0_0_0_Types[] = { &CanvasRenderer_t297_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t297_0_0_0 = { 1, GenInst_CanvasRenderer_t297_0_0_0_Types };
extern const Il2CppType Component_t122_0_0_0;
static const Il2CppType* GenInst_Component_t122_0_0_0_Types[] = { &Component_t122_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t122_0_0_0 = { 1, GenInst_Component_t122_0_0_0_Types };
extern const Il2CppType Graphic_t564_0_0_0;
static const Il2CppType* GenInst_Graphic_t564_0_0_0_Types[] = { &Graphic_t564_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t564_0_0_0 = { 1, GenInst_Graphic_t564_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t698_0_0_0;
static const Il2CppType* GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_Types[] = { &Canvas_t294_0_0_0, &IndexedSet_1_t698_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0 = { 2, GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_Types };
extern const Il2CppType Sprite_t219_0_0_0;
static const Il2CppType* GenInst_Sprite_t219_0_0_0_Types[] = { &Sprite_t219_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t219_0_0_0 = { 1, GenInst_Sprite_t219_0_0_0_Types };
extern const Il2CppType Type_t572_0_0_0;
static const Il2CppType* GenInst_Type_t572_0_0_0_Types[] = { &Type_t572_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t572_0_0_0 = { 1, GenInst_Type_t572_0_0_0_Types };
extern const Il2CppType FillMethod_t573_0_0_0;
static const Il2CppType* GenInst_FillMethod_t573_0_0_0_Types[] = { &FillMethod_t573_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t573_0_0_0 = { 1, GenInst_FillMethod_t573_0_0_0_Types };
extern const Il2CppType SubmitEvent_t579_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t579_0_0_0_Types[] = { &SubmitEvent_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t579_0_0_0 = { 1, GenInst_SubmitEvent_t579_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t581_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t581_0_0_0_Types[] = { &OnChangeEvent_t581_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t581_0_0_0 = { 1, GenInst_OnChangeEvent_t581_0_0_0_Types };
extern const Il2CppType OnValidateInput_t583_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t583_0_0_0_Types[] = { &OnValidateInput_t583_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t583_0_0_0 = { 1, GenInst_OnValidateInput_t583_0_0_0_Types };
extern const Il2CppType ContentType_t575_0_0_0;
static const Il2CppType* GenInst_ContentType_t575_0_0_0_Types[] = { &ContentType_t575_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t575_0_0_0 = { 1, GenInst_ContentType_t575_0_0_0_Types };
extern const Il2CppType LineType_t578_0_0_0;
static const Il2CppType* GenInst_LineType_t578_0_0_0_Types[] = { &LineType_t578_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t578_0_0_0 = { 1, GenInst_LineType_t578_0_0_0_Types };
extern const Il2CppType InputType_t576_0_0_0;
static const Il2CppType* GenInst_InputType_t576_0_0_0_Types[] = { &InputType_t576_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t576_0_0_0 = { 1, GenInst_InputType_t576_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t201_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t201_0_0_0_Types[] = { &TouchScreenKeyboardType_t201_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t201_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t201_0_0_0_Types };
extern const Il2CppType CharacterValidation_t577_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t577_0_0_0_Types[] = { &CharacterValidation_t577_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t577_0_0_0 = { 1, GenInst_CharacterValidation_t577_0_0_0_Types };
extern const Il2CppType Char_t699_0_0_0;
static const Il2CppType* GenInst_Char_t699_0_0_0_Types[] = { &Char_t699_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t699_0_0_0 = { 1, GenInst_Char_t699_0_0_0_Types };
extern const Il2CppType LayoutElement_t646_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t646_0_0_0_Types[] = { &LayoutElement_t646_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t646_0_0_0 = { 1, GenInst_LayoutElement_t646_0_0_0_Types };
extern const Il2CppType Mask_t587_0_0_0;
static const Il2CppType* GenInst_Mask_t587_0_0_0_Types[] = { &Mask_t587_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t587_0_0_0 = { 1, GenInst_Mask_t587_0_0_0_Types };
extern const Il2CppType IClippable_t678_0_0_0;
static const Il2CppType* GenInst_IClippable_t678_0_0_0_Types[] = { &IClippable_t678_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t678_0_0_0 = { 1, GenInst_IClippable_t678_0_0_0_Types };
extern const Il2CppType RectMask2D_t590_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t590_0_0_0_Types[] = { &RectMask2D_t590_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t590_0_0_0 = { 1, GenInst_RectMask2D_t590_0_0_0_Types };
extern const Il2CppType Direction_t599_0_0_0;
static const Il2CppType* GenInst_Direction_t599_0_0_0_Types[] = { &Direction_t599_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t599_0_0_0 = { 1, GenInst_Direction_t599_0_0_0_Types };
extern const Il2CppType Vector2_t15_0_0_0;
static const Il2CppType* GenInst_Vector2_t15_0_0_0_Types[] = { &Vector2_t15_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t15_0_0_0 = { 1, GenInst_Vector2_t15_0_0_0_Types };
extern const Il2CppType Selectable_t540_0_0_0;
static const Il2CppType* GenInst_Selectable_t540_0_0_0_Types[] = { &Selectable_t540_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t540_0_0_0 = { 1, GenInst_Selectable_t540_0_0_0_Types };
extern const Il2CppType Navigation_t594_0_0_0;
static const Il2CppType* GenInst_Navigation_t594_0_0_0_Types[] = { &Navigation_t594_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t594_0_0_0 = { 1, GenInst_Navigation_t594_0_0_0_Types };
extern const Il2CppType Transition_t609_0_0_0;
static const Il2CppType* GenInst_Transition_t609_0_0_0_Types[] = { &Transition_t609_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t609_0_0_0 = { 1, GenInst_Transition_t609_0_0_0_Types };
extern const Il2CppType ColorBlock_t546_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t546_0_0_0_Types[] = { &ColorBlock_t546_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t546_0_0_0 = { 1, GenInst_ColorBlock_t546_0_0_0_Types };
extern const Il2CppType SpriteState_t611_0_0_0;
static const Il2CppType* GenInst_SpriteState_t611_0_0_0_Types[] = { &SpriteState_t611_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t611_0_0_0 = { 1, GenInst_SpriteState_t611_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t536_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t536_0_0_0_Types[] = { &AnimationTriggers_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t536_0_0_0 = { 1, GenInst_AnimationTriggers_t536_0_0_0_Types };
extern const Il2CppType Animator_t273_0_0_0;
static const Il2CppType* GenInst_Animator_t273_0_0_0_Types[] = { &Animator_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t273_0_0_0 = { 1, GenInst_Animator_t273_0_0_0_Types };
extern const Il2CppType Direction_t615_0_0_0;
static const Il2CppType* GenInst_Direction_t615_0_0_0_Types[] = { &Direction_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t615_0_0_0 = { 1, GenInst_Direction_t615_0_0_0_Types };
extern const Il2CppType MatEntry_t619_0_0_0;
static const Il2CppType* GenInst_MatEntry_t619_0_0_0_Types[] = { &MatEntry_t619_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t619_0_0_0 = { 1, GenInst_MatEntry_t619_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t550_0_0_0_Boolean_t455_0_0_0_Types[] = { &Toggle_t550_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t550_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Toggle_t550_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType IClipper_t680_0_0_0;
static const Il2CppType* GenInst_IClipper_t680_0_0_0_Types[] = { &IClipper_t680_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t680_0_0_0 = { 1, GenInst_IClipper_t680_0_0_0_Types };
extern const Il2CppType AspectMode_t631_0_0_0;
static const Il2CppType* GenInst_AspectMode_t631_0_0_0_Types[] = { &AspectMode_t631_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t631_0_0_0 = { 1, GenInst_AspectMode_t631_0_0_0_Types };
extern const Il2CppType FitMode_t637_0_0_0;
static const Il2CppType* GenInst_FitMode_t637_0_0_0_Types[] = { &FitMode_t637_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t637_0_0_0 = { 1, GenInst_FitMode_t637_0_0_0_Types };
extern const Il2CppType Corner_t639_0_0_0;
static const Il2CppType* GenInst_Corner_t639_0_0_0_Types[] = { &Corner_t639_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t639_0_0_0 = { 1, GenInst_Corner_t639_0_0_0_Types };
extern const Il2CppType Axis_t640_0_0_0;
static const Il2CppType* GenInst_Axis_t640_0_0_0_Types[] = { &Axis_t640_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t640_0_0_0 = { 1, GenInst_Axis_t640_0_0_0_Types };
extern const Il2CppType Constraint_t641_0_0_0;
static const Il2CppType* GenInst_Constraint_t641_0_0_0_Types[] = { &Constraint_t641_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t641_0_0_0 = { 1, GenInst_Constraint_t641_0_0_0_Types };
extern const Il2CppType RectOffset_t318_0_0_0;
static const Il2CppType* GenInst_RectOffset_t318_0_0_0_Types[] = { &RectOffset_t318_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t318_0_0_0 = { 1, GenInst_RectOffset_t318_0_0_0_Types };
extern const Il2CppType TextAnchor_t278_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t278_0_0_0_Types[] = { &TextAnchor_t278_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t278_0_0_0 = { 1, GenInst_TextAnchor_t278_0_0_0_Types };
extern const Il2CppType ILayoutElement_t681_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t681_0_0_0_Single_t113_0_0_0_Types[] = { &ILayoutElement_t681_0_0_0, &Single_t113_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t681_0_0_0_Single_t113_0_0_0 = { 2, GenInst_ILayoutElement_t681_0_0_0_Single_t113_0_0_0_Types };
extern const Il2CppType Vector3_t3_0_0_0;
static const Il2CppType* GenInst_Vector3_t3_0_0_0_Types[] = { &Vector3_t3_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3_0_0_0 = { 1, GenInst_Vector3_t3_0_0_0_Types };
extern const Il2CppType Color32_t187_0_0_0;
static const Il2CppType* GenInst_Color32_t187_0_0_0_Types[] = { &Color32_t187_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t187_0_0_0 = { 1, GenInst_Color32_t187_0_0_0_Types };
extern const Il2CppType Vector4_t90_0_0_0;
static const Il2CppType* GenInst_Vector4_t90_0_0_0_Types[] = { &Vector4_t90_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t90_0_0_0 = { 1, GenInst_Vector4_t90_0_0_0_Types };
extern const Il2CppType DispatcherKey_t722_0_0_0;
extern const Il2CppType Dispatcher_t718_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_Types[] = { &DispatcherKey_t722_0_0_0, &Dispatcher_t718_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0 = { 2, GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1374_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1374_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1374_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1374_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1374_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t1373_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1373_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1373_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1373_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1373_0_0_0_Types };
extern const Il2CppType StrongName_t1623_0_0_0;
static const Il2CppType* GenInst_StrongName_t1623_0_0_0_Types[] = { &StrongName_t1623_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1623_0_0_0 = { 1, GenInst_StrongName_t1623_0_0_0_Types };
extern const Il2CppType DateTime_t308_0_0_0;
static const Il2CppType* GenInst_DateTime_t308_0_0_0_Types[] = { &DateTime_t308_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t308_0_0_0 = { 1, GenInst_DateTime_t308_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1698_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1698_0_0_0_Types[] = { &DateTimeOffset_t1698_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1698_0_0_0 = { 1, GenInst_DateTimeOffset_t1698_0_0_0_Types };
extern const Il2CppType TimeSpan_t846_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t846_0_0_0_Types[] = { &TimeSpan_t846_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t846_0_0_0 = { 1, GenInst_TimeSpan_t846_0_0_0_Types };
extern const Il2CppType Guid_t1720_0_0_0;
static const Il2CppType* GenInst_Guid_t1720_0_0_0_Types[] = { &Guid_t1720_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t1720_0_0_0 = { 1, GenInst_Guid_t1720_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1370_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1370_0_0_0_Types[] = { &CustomAttributeData_t1370_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1370_0_0_0 = { 1, GenInst_CustomAttributeData_t1370_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t2_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t2_0_0_0_Types[] = { &MonoBehaviour_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t2_0_0_0 = { 1, GenInst_MonoBehaviour_t2_0_0_0_Types };
extern const Il2CppType Behaviour_t185_0_0_0;
static const Il2CppType* GenInst_Behaviour_t185_0_0_0_Types[] = { &Behaviour_t185_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t185_0_0_0 = { 1, GenInst_Behaviour_t185_0_0_0_Types };
extern const Il2CppType Object_t82_0_0_0;
static const Il2CppType* GenInst_Object_t82_0_0_0_Types[] = { &Object_t82_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t82_0_0_0 = { 1, GenInst_Object_t82_0_0_0_Types };
extern const Il2CppType IReflect_t2715_0_0_0;
static const Il2CppType* GenInst_IReflect_t2715_0_0_0_Types[] = { &IReflect_t2715_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2715_0_0_0 = { 1, GenInst_IReflect_t2715_0_0_0_Types };
extern const Il2CppType _Type_t2713_0_0_0;
static const Il2CppType* GenInst__Type_t2713_0_0_0_Types[] = { &_Type_t2713_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2713_0_0_0 = { 1, GenInst__Type_t2713_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1804_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1804_0_0_0_Types[] = { &ICustomAttributeProvider_t1804_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1804_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1804_0_0_0_Types };
extern const Il2CppType _MemberInfo_t2714_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t2714_0_0_0_Types[] = { &_MemberInfo_t2714_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t2714_0_0_0 = { 1, GenInst__MemberInfo_t2714_0_0_0_Types };
extern const Il2CppType IFormattable_t1806_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1806_0_0_0_Types[] = { &IFormattable_t1806_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1806_0_0_0 = { 1, GenInst_IFormattable_t1806_0_0_0_Types };
extern const Il2CppType IConvertible_t737_0_0_0;
static const Il2CppType* GenInst_IConvertible_t737_0_0_0_Types[] = { &IConvertible_t737_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t737_0_0_0 = { 1, GenInst_IConvertible_t737_0_0_0_Types };
extern const Il2CppType IComparable_t1808_0_0_0;
static const Il2CppType* GenInst_IComparable_t1808_0_0_0_Types[] = { &IComparable_t1808_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1808_0_0_0 = { 1, GenInst_IComparable_t1808_0_0_0_Types };
extern const Il2CppType IComparable_1_t2935_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2935_0_0_0_Types[] = { &IComparable_1_t2935_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2935_0_0_0 = { 1, GenInst_IComparable_1_t2935_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2940_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2940_0_0_0_Types[] = { &IEquatable_1_t2940_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2940_0_0_0 = { 1, GenInst_IEquatable_1_t2940_0_0_0_Types };
extern const Il2CppType ValueType_t1118_0_0_0;
static const Il2CppType* GenInst_ValueType_t1118_0_0_0_Types[] = { &ValueType_t1118_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1118_0_0_0 = { 1, GenInst_ValueType_t1118_0_0_0_Types };
extern const Il2CppType Double_t465_0_0_0;
static const Il2CppType* GenInst_Double_t465_0_0_0_Types[] = { &Double_t465_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t465_0_0_0 = { 1, GenInst_Double_t465_0_0_0_Types };
extern const Il2CppType IComparable_1_t2952_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2952_0_0_0_Types[] = { &IComparable_1_t2952_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2952_0_0_0 = { 1, GenInst_IComparable_1_t2952_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2957_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2957_0_0_0_Types[] = { &IEquatable_1_t2957_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2957_0_0_0 = { 1, GenInst_IEquatable_1_t2957_0_0_0_Types };
extern const Il2CppType IComparable_1_t2965_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2965_0_0_0_Types[] = { &IComparable_1_t2965_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2965_0_0_0 = { 1, GenInst_IComparable_1_t2965_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2970_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2970_0_0_0_Types[] = { &IEquatable_1_t2970_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2970_0_0_0 = { 1, GenInst_IEquatable_1_t2970_0_0_0_Types };
extern const Il2CppType IComparable_1_t2981_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2981_0_0_0_Types[] = { &IComparable_1_t2981_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2981_0_0_0 = { 1, GenInst_IComparable_1_t2981_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2986_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2986_0_0_0_Types[] = { &IEquatable_1_t2986_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2986_0_0_0 = { 1, GenInst_IEquatable_1_t2986_0_0_0_Types };
extern const Il2CppType List_1_t703_0_0_0;
static const Il2CppType* GenInst_List_1_t703_0_0_0_Types[] = { &List_1_t703_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t703_0_0_0 = { 1, GenInst_List_1_t703_0_0_0_Types };
extern const Il2CppType List_1_t426_0_0_0;
static const Il2CppType* GenInst_List_1_t426_0_0_0_Types[] = { &List_1_t426_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t426_0_0_0 = { 1, GenInst_List_1_t426_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType Byte_t454_0_0_0;
static const Il2CppType* GenInst_Byte_t454_0_0_0_Types[] = { &Byte_t454_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t454_0_0_0 = { 1, GenInst_Byte_t454_0_0_0_Types };
extern const Il2CppType IComparable_1_t3004_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3004_0_0_0_Types[] = { &IComparable_1_t3004_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3004_0_0_0 = { 1, GenInst_IComparable_1_t3004_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3009_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3009_0_0_0_Types[] = { &IEquatable_1_t3009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3009_0_0_0 = { 1, GenInst_IEquatable_1_t3009_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t2669_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t2669_0_0_0_Types[] = { &IAchievementDescription_t2669_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2669_0_0_0 = { 1, GenInst_IAchievementDescription_t2669_0_0_0_Types };
extern const Il2CppType IAchievement_t416_0_0_0;
static const Il2CppType* GenInst_IAchievement_t416_0_0_0_Types[] = { &IAchievement_t416_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t416_0_0_0 = { 1, GenInst_IAchievement_t416_0_0_0_Types };
extern const Il2CppType IScore_t367_0_0_0;
static const Il2CppType* GenInst_IScore_t367_0_0_0_Types[] = { &IScore_t367_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t367_0_0_0 = { 1, GenInst_IScore_t367_0_0_0_Types };
extern const Il2CppType IUserProfile_t2668_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t2668_0_0_0_Types[] = { &IUserProfile_t2668_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t2668_0_0_0 = { 1, GenInst_IUserProfile_t2668_0_0_0_Types };
extern const Il2CppType AchievementDescription_t363_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t363_0_0_0_Types[] = { &AchievementDescription_t363_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t363_0_0_0 = { 1, GenInst_AchievementDescription_t363_0_0_0_Types };
extern const Il2CppType UserProfile_t360_0_0_0;
static const Il2CppType* GenInst_UserProfile_t360_0_0_0_Types[] = { &UserProfile_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t360_0_0_0 = { 1, GenInst_UserProfile_t360_0_0_0_Types };
extern const Il2CppType GcAchievementData_t347_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t347_0_0_0_Types[] = { &GcAchievementData_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t347_0_0_0 = { 1, GenInst_GcAchievementData_t347_0_0_0_Types };
extern const Il2CppType Achievement_t362_0_0_0;
static const Il2CppType* GenInst_Achievement_t362_0_0_0_Types[] = { &Achievement_t362_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t362_0_0_0 = { 1, GenInst_Achievement_t362_0_0_0_Types };
extern const Il2CppType GcScoreData_t348_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t348_0_0_0_Types[] = { &GcScoreData_t348_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t348_0_0_0 = { 1, GenInst_GcScoreData_t348_0_0_0_Types };
extern const Il2CppType Score_t364_0_0_0;
static const Il2CppType* GenInst_Score_t364_0_0_0_Types[] = { &Score_t364_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t364_0_0_0 = { 1, GenInst_Score_t364_0_0_0_Types };
extern const Il2CppType Display_t229_0_0_0;
static const Il2CppType* GenInst_Display_t229_0_0_0_Types[] = { &Display_t229_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t229_0_0_0 = { 1, GenInst_Display_t229_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t1837_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1837_0_0_0_Types[] = { &ISerializable_t1837_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1837_0_0_0 = { 1, GenInst_ISerializable_t1837_0_0_0_Types };
extern const Il2CppType ContactPoint_t247_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t247_0_0_0_Types[] = { &ContactPoint_t247_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t247_0_0_0 = { 1, GenInst_ContactPoint_t247_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t252_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t252_0_0_0_Types[] = { &RaycastHit2D_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t252_0_0_0 = { 1, GenInst_RaycastHit2D_t252_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t255_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t255_0_0_0_Types[] = { &ContactPoint2D_t255_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t255_0_0_0 = { 1, GenInst_ContactPoint2D_t255_0_0_0_Types };
extern const Il2CppType Keyframe_t269_0_0_0;
static const Il2CppType* GenInst_Keyframe_t269_0_0_0_Types[] = { &Keyframe_t269_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t269_0_0_0 = { 1, GenInst_Keyframe_t269_0_0_0_Types };
extern const Il2CppType CharacterInfo_t281_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t281_0_0_0_Types[] = { &CharacterInfo_t281_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t281_0_0_0 = { 1, GenInst_CharacterInfo_t281_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2047_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2047_0_0_0_Types[] = { &KeyValuePair_2_t2047_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2047_0_0_0 = { 1, GenInst_KeyValuePair_2_t2047_0_0_0_Types };
extern const Il2CppType Link_t1229_0_0_0;
static const Il2CppType* GenInst_Link_t1229_0_0_0_Types[] = { &Link_t1229_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1229_0_0_0 = { 1, GenInst_Link_t1229_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t106_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t943_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t943_0_0_0_Types[] = { &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t943_0_0_0 = { 1, GenInst_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2047_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t106_0_0_0, &KeyValuePair_2_t2047_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2047_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2047_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2057_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2057_0_0_0_Types[] = { &KeyValuePair_2_t2057_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2057_0_0_0 = { 1, GenInst_KeyValuePair_2_t2057_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t321_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t321_0_0_0_Types[] = { &GUILayoutOption_t321_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t321_0_0_0 = { 1, GenInst_GUILayoutOption_t321_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t106_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2066_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2066_0_0_0_Types[] = { &KeyValuePair_2_t2066_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2066_0_0_0 = { 1, GenInst_KeyValuePair_2_t2066_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t106_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Int32_t106_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2066_0_0_0_Types[] = { &Int32_t106_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2066_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2066_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2066_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Int32_t106_0_0_0, &LayoutCache_t311_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2074_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2074_0_0_0_Types[] = { &KeyValuePair_2_t2074_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2074_0_0_0 = { 1, GenInst_KeyValuePair_2_t2074_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t315_0_0_0_Types[] = { &GUIStyle_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t315_0_0_0 = { 1, GenInst_GUIStyle_t315_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2086_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2086_0_0_0_Types[] = { &KeyValuePair_2_t2086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2086_0_0_0 = { 1, GenInst_KeyValuePair_2_t2086_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2086_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2086_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2086_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2086_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t315_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2094_0_0_0_Types[] = { &KeyValuePair_2_t2094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2094_0_0_0 = { 1, GenInst_KeyValuePair_2_t2094_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t337_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t337_0_0_0_Types[] = { &DisallowMultipleComponent_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t337_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t337_0_0_0_Types };
extern const Il2CppType Attribute_t215_0_0_0;
static const Il2CppType* GenInst_Attribute_t215_0_0_0_Types[] = { &Attribute_t215_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t215_0_0_0 = { 1, GenInst_Attribute_t215_0_0_0_Types };
extern const Il2CppType _Attribute_t2703_0_0_0;
static const Il2CppType* GenInst__Attribute_t2703_0_0_0_Types[] = { &_Attribute_t2703_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t2703_0_0_0 = { 1, GenInst__Attribute_t2703_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t340_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t340_0_0_0_Types[] = { &ExecuteInEditMode_t340_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t340_0_0_0 = { 1, GenInst_ExecuteInEditMode_t340_0_0_0_Types };
extern const Il2CppType RequireComponent_t338_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t338_0_0_0_Types[] = { &RequireComponent_t338_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t338_0_0_0 = { 1, GenInst_RequireComponent_t338_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1392_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1392_0_0_0_Types[] = { &ParameterModifier_t1392_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1392_0_0_0 = { 1, GenInst_ParameterModifier_t1392_0_0_0_Types };
extern const Il2CppType HitInfo_t368_0_0_0;
static const Il2CppType* GenInst_HitInfo_t368_0_0_0_Types[] = { &HitInfo_t368_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t368_0_0_0 = { 1, GenInst_HitInfo_t368_0_0_0_Types };
extern const Il2CppType ParameterInfo_t473_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t473_0_0_0_Types[] = { &ParameterInfo_t473_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t473_0_0_0 = { 1, GenInst_ParameterInfo_t473_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2754_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2754_0_0_0_Types[] = { &_ParameterInfo_t2754_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2754_0_0_0 = { 1, GenInst__ParameterInfo_t2754_0_0_0_Types };
extern const Il2CppType Event_t84_0_0_0;
extern const Il2CppType TextEditOp_t387_0_0_0;
static const Il2CppType* GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_Types[] = { &Event_t84_0_0_0, &TextEditOp_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0 = { 2, GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2114_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2114_0_0_0_Types[] = { &KeyValuePair_2_t2114_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2114_0_0_0 = { 1, GenInst_KeyValuePair_2_t2114_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t387_0_0_0_Types[] = { &TextEditOp_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t387_0_0_0 = { 1, GenInst_TextEditOp_t387_0_0_0_Types };
extern const Il2CppType Enum_t150_0_0_0;
static const Il2CppType* GenInst_Enum_t150_0_0_0_Types[] = { &Enum_t150_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t150_0_0_0 = { 1, GenInst_Enum_t150_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t387_0_0_0, &TextEditOp_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t387_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_KeyValuePair_2_t2114_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t387_0_0_0, &KeyValuePair_2_t2114_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_KeyValuePair_2_t2114_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_KeyValuePair_2_t2114_0_0_0_Types };
static const Il2CppType* GenInst_Event_t84_0_0_0_Types[] = { &Event_t84_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t84_0_0_0 = { 1, GenInst_Event_t84_0_0_0_Types };
static const Il2CppType* GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Event_t84_0_0_0, &TextEditOp_t387_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2125_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2125_0_0_0_Types[] = { &KeyValuePair_2_t2125_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2125_0_0_0 = { 1, GenInst_KeyValuePair_2_t2125_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Int32_t106_0_0_0, &PointerEventData_t48_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t685_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t685_0_0_0_Types[] = { &KeyValuePair_2_t685_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t685_0_0_0 = { 1, GenInst_KeyValuePair_2_t685_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_Types[] = { &ICanvasElement_t675_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &ICanvasElement_t675_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType List_1_t687_0_0_0;
static const Il2CppType* GenInst_List_1_t687_0_0_0_Types[] = { &List_1_t687_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t687_0_0_0 = { 1, GenInst_List_1_t687_0_0_0_Types };
static const Il2CppType* GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Font_t283_0_0_0, &List_1_t690_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2229_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2229_0_0_0_Types[] = { &KeyValuePair_2_t2229_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2229_0_0_0 = { 1, GenInst_KeyValuePair_2_t2229_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_Types[] = { &Graphic_t564_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Graphic_t564_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Canvas_t294_0_0_0, &IndexedSet_1_t698_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2242_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2242_0_0_0_Types[] = { &KeyValuePair_2_t2242_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2242_0_0_0 = { 1, GenInst_KeyValuePair_2_t2242_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2245_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2245_0_0_0_Types[] = { &KeyValuePair_2_t2245_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2245_0_0_0 = { 1, GenInst_KeyValuePair_2_t2245_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2248_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2248_0_0_0_Types[] = { &KeyValuePair_2_t2248_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2248_0_0_0 = { 1, GenInst_KeyValuePair_2_t2248_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_Types[] = { &IClipper_t680_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &IClipper_t680_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2295_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2295_0_0_0_Types[] = { &KeyValuePair_2_t2295_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2295_0_0_0 = { 1, GenInst_KeyValuePair_2_t2295_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t648_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t648_0_0_0_Types[] = { &LayoutRebuilder_t648_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t648_0_0_0 = { 1, GenInst_LayoutRebuilder_t648_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t113_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t113_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t113_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t113_0_0_0_Types };
extern const Il2CppType List_1_t417_0_0_0;
static const Il2CppType* GenInst_List_1_t417_0_0_0_Types[] = { &List_1_t417_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t417_0_0_0 = { 1, GenInst_List_1_t417_0_0_0_Types };
extern const Il2CppType List_1_t423_0_0_0;
static const Il2CppType* GenInst_List_1_t423_0_0_0_Types[] = { &List_1_t423_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t423_0_0_0 = { 1, GenInst_List_1_t423_0_0_0_Types };
extern const Il2CppType List_1_t421_0_0_0;
static const Il2CppType* GenInst_List_1_t421_0_0_0_Types[] = { &List_1_t421_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t421_0_0_0 = { 1, GenInst_List_1_t421_0_0_0_Types };
extern const Il2CppType List_1_t419_0_0_0;
static const Il2CppType* GenInst_List_1_t419_0_0_0_Types[] = { &List_1_t419_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t419_0_0_0 = { 1, GenInst_List_1_t419_0_0_0_Types };
extern const Il2CppType List_1_t424_0_0_0;
static const Il2CppType* GenInst_List_1_t424_0_0_0_Types[] = { &List_1_t424_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t424_0_0_0 = { 1, GenInst_List_1_t424_0_0_0_Types };
extern const Il2CppType List_1_t2325_0_0_0;
static const Il2CppType* GenInst_List_1_t2325_0_0_0_Types[] = { &List_1_t2325_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2325_0_0_0 = { 1, GenInst_List_1_t2325_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t722_0_0_0_Types[] = { &DispatcherKey_t722_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t722_0_0_0 = { 1, GenInst_DispatcherKey_t722_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &DispatcherKey_t722_0_0_0, &Dispatcher_t718_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2330_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2330_0_0_0_Types[] = { &KeyValuePair_2_t2330_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2330_0_0_0 = { 1, GenInst_KeyValuePair_2_t2330_0_0_0_Types };
extern const Il2CppType _MethodInfo_t2752_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t2752_0_0_0_Types[] = { &_MethodInfo_t2752_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t2752_0_0_0 = { 1, GenInst__MethodInfo_t2752_0_0_0_Types };
extern const Il2CppType MethodBase_t471_0_0_0;
static const Il2CppType* GenInst_MethodBase_t471_0_0_0_Types[] = { &MethodBase_t471_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t471_0_0_0 = { 1, GenInst_MethodBase_t471_0_0_0_Types };
extern const Il2CppType _MethodBase_t2751_0_0_0;
static const Il2CppType* GenInst__MethodBase_t2751_0_0_0_Types[] = { &_MethodBase_t2751_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t2751_0_0_0 = { 1, GenInst__MethodBase_t2751_0_0_0_Types };
extern const Il2CppType KeySizes_t766_0_0_0;
static const Il2CppType* GenInst_KeySizes_t766_0_0_0_Types[] = { &KeySizes_t766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t766_0_0_0 = { 1, GenInst_KeySizes_t766_0_0_0_Types };
extern const Il2CppType UInt32_t467_0_0_0;
static const Il2CppType* GenInst_UInt32_t467_0_0_0_Types[] = { &UInt32_t467_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t467_0_0_0 = { 1, GenInst_UInt32_t467_0_0_0_Types };
extern const Il2CppType IComparable_1_t3283_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3283_0_0_0_Types[] = { &IComparable_1_t3283_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3283_0_0_0 = { 1, GenInst_IComparable_1_t3283_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3288_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3288_0_0_0_Types[] = { &IEquatable_1_t3288_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3288_0_0_0 = { 1, GenInst_IEquatable_1_t3288_0_0_0_Types };
extern const Il2CppType UInt16_t960_0_0_0;
static const Il2CppType* GenInst_UInt16_t960_0_0_0_Types[] = { &UInt16_t960_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t960_0_0_0 = { 1, GenInst_UInt16_t960_0_0_0_Types };
extern const Il2CppType IComparable_1_t3296_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3296_0_0_0_Types[] = { &IComparable_1_t3296_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3296_0_0_0 = { 1, GenInst_IComparable_1_t3296_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3301_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3301_0_0_0_Types[] = { &IEquatable_1_t3301_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3301_0_0_0 = { 1, GenInst_IEquatable_1_t3301_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2342_0_0_0_Types[] = { &KeyValuePair_2_t2342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2342_0_0_0 = { 1, GenInst_KeyValuePair_2_t2342_0_0_0_Types };
extern const Il2CppType IComparable_1_t3313_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3313_0_0_0_Types[] = { &IComparable_1_t3313_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3313_0_0_0 = { 1, GenInst_IComparable_1_t3313_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3318_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3318_0_0_0_Types[] = { &IEquatable_1_t3318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3318_0_0_0 = { 1, GenInst_IEquatable_1_t3318_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Boolean_t455_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t455_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Boolean_t455_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Boolean_t455_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t455_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_KeyValuePair_2_t2342_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t455_0_0_0, &KeyValuePair_2_t2342_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_KeyValuePair_2_t2342_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_KeyValuePair_2_t2342_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t455_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2354_0_0_0_Types[] = { &KeyValuePair_2_t2354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2354_0_0_0 = { 1, GenInst_KeyValuePair_2_t2354_0_0_0_Types };
extern const Il2CppType X509Certificate_t830_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t830_0_0_0_Types[] = { &X509Certificate_t830_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t830_0_0_0 = { 1, GenInst_X509Certificate_t830_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1840_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1840_0_0_0_Types[] = { &IDeserializationCallback_t1840_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1840_0_0_0 = { 1, GenInst_IDeserializationCallback_t1840_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t843_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t843_0_0_0_Types[] = { &X509ChainStatus_t843_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t843_0_0_0 = { 1, GenInst_X509ChainStatus_t843_0_0_0_Types };
extern const Il2CppType Capture_t865_0_0_0;
static const Il2CppType* GenInst_Capture_t865_0_0_0_Types[] = { &Capture_t865_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t865_0_0_0 = { 1, GenInst_Capture_t865_0_0_0_Types };
extern const Il2CppType Group_t868_0_0_0;
static const Il2CppType* GenInst_Group_t868_0_0_0_Types[] = { &Group_t868_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t868_0_0_0 = { 1, GenInst_Group_t868_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Types[] = { &Int32_t106_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2362_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2362_0_0_0_Types[] = { &KeyValuePair_2_t2362_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2362_0_0_0 = { 1, GenInst_KeyValuePair_2_t2362_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0_Types[] = { &Int32_t106_0_0_0, &Int32_t106_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Int32_t106_0_0_0, &Int32_t106_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2362_0_0_0_Types[] = { &Int32_t106_0_0_0, &Int32_t106_0_0_0, &KeyValuePair_2_t2362_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2362_0_0_0 = { 3, GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2362_0_0_0_Types };
extern const Il2CppType Mark_t893_0_0_0;
static const Il2CppType* GenInst_Mark_t893_0_0_0_Types[] = { &Mark_t893_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t893_0_0_0 = { 1, GenInst_Mark_t893_0_0_0_Types };
extern const Il2CppType UriScheme_t930_0_0_0;
static const Il2CppType* GenInst_UriScheme_t930_0_0_0_Types[] = { &UriScheme_t930_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t930_0_0_0 = { 1, GenInst_UriScheme_t930_0_0_0_Types };
extern const Il2CppType BigInteger_t987_0_0_0;
static const Il2CppType* GenInst_BigInteger_t987_0_0_0_Types[] = { &BigInteger_t987_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t987_0_0_0 = { 1, GenInst_BigInteger_t987_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t75_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t75_0_0_0_Types[] = { &ByteU5BU5D_t75_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t75_0_0_0 = { 1, GenInst_ByteU5BU5D_t75_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t731_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t731_0_0_0_Types[] = { &IEnumerable_t731_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t731_0_0_0 = { 1, GenInst_IEnumerable_t731_0_0_0_Types };
extern const Il2CppType ICloneable_t1818_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1818_0_0_0_Types[] = { &ICloneable_t1818_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1818_0_0_0 = { 1, GenInst_ICloneable_t1818_0_0_0_Types };
extern const Il2CppType ICollection_t952_0_0_0;
static const Il2CppType* GenInst_ICollection_t952_0_0_0_Types[] = { &ICollection_t952_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t952_0_0_0 = { 1, GenInst_ICollection_t952_0_0_0_Types };
extern const Il2CppType IList_t902_0_0_0;
static const Il2CppType* GenInst_IList_t902_0_0_0_Types[] = { &IList_t902_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t902_0_0_0 = { 1, GenInst_IList_t902_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1074_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1074_0_0_0_Types[] = { &ClientCertificateType_t1074_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1074_0_0_0 = { 1, GenInst_ClientCertificateType_t1074_0_0_0_Types };
extern const Il2CppType Int64_t466_0_0_0;
static const Il2CppType* GenInst_Int64_t466_0_0_0_Types[] = { &Int64_t466_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t466_0_0_0 = { 1, GenInst_Int64_t466_0_0_0_Types };
extern const Il2CppType UInt64_t1123_0_0_0;
static const Il2CppType* GenInst_UInt64_t1123_0_0_0_Types[] = { &UInt64_t1123_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1123_0_0_0 = { 1, GenInst_UInt64_t1123_0_0_0_Types };
extern const Il2CppType SByte_t1124_0_0_0;
static const Il2CppType* GenInst_SByte_t1124_0_0_0_Types[] = { &SByte_t1124_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1124_0_0_0 = { 1, GenInst_SByte_t1124_0_0_0_Types };
extern const Il2CppType Int16_t1125_0_0_0;
static const Il2CppType* GenInst_Int16_t1125_0_0_0_Types[] = { &Int16_t1125_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1125_0_0_0 = { 1, GenInst_Int16_t1125_0_0_0_Types };
extern const Il2CppType Decimal_t738_0_0_0;
static const Il2CppType* GenInst_Decimal_t738_0_0_0_Types[] = { &Decimal_t738_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t738_0_0_0 = { 1, GenInst_Decimal_t738_0_0_0_Types };
extern const Il2CppType Delegate_t81_0_0_0;
static const Il2CppType* GenInst_Delegate_t81_0_0_0_Types[] = { &Delegate_t81_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t81_0_0_0 = { 1, GenInst_Delegate_t81_0_0_0_Types };
extern const Il2CppType IComparable_1_t3386_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3386_0_0_0_Types[] = { &IComparable_1_t3386_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3386_0_0_0 = { 1, GenInst_IComparable_1_t3386_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3387_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3387_0_0_0_Types[] = { &IEquatable_1_t3387_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3387_0_0_0 = { 1, GenInst_IEquatable_1_t3387_0_0_0_Types };
extern const Il2CppType IComparable_1_t3390_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3390_0_0_0_Types[] = { &IComparable_1_t3390_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3390_0_0_0 = { 1, GenInst_IComparable_1_t3390_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3391_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3391_0_0_0_Types[] = { &IEquatable_1_t3391_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3391_0_0_0 = { 1, GenInst_IEquatable_1_t3391_0_0_0_Types };
extern const Il2CppType IComparable_1_t3388_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3388_0_0_0_Types[] = { &IComparable_1_t3388_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3388_0_0_0 = { 1, GenInst_IComparable_1_t3388_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3389_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3389_0_0_0_Types[] = { &IEquatable_1_t3389_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3389_0_0_0 = { 1, GenInst_IEquatable_1_t3389_0_0_0_Types };
extern const Il2CppType IComparable_1_t3384_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3384_0_0_0_Types[] = { &IComparable_1_t3384_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3384_0_0_0 = { 1, GenInst_IComparable_1_t3384_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3385_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3385_0_0_0_Types[] = { &IEquatable_1_t3385_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3385_0_0_0 = { 1, GenInst_IEquatable_1_t3385_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2750_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2750_0_0_0_Types[] = { &_FieldInfo_t2750_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2750_0_0_0 = { 1, GenInst__FieldInfo_t2750_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t479_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t479_0_0_0_Types[] = { &ConstructorInfo_t479_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t479_0_0_0 = { 1, GenInst_ConstructorInfo_t479_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t2748_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t2748_0_0_0_Types[] = { &_ConstructorInfo_t2748_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2748_0_0_0 = { 1, GenInst__ConstructorInfo_t2748_0_0_0_Types };
extern const Il2CppType TableRange_t1162_0_0_0;
static const Il2CppType* GenInst_TableRange_t1162_0_0_0_Types[] = { &TableRange_t1162_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1162_0_0_0 = { 1, GenInst_TableRange_t1162_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1165_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1165_0_0_0_Types[] = { &TailoringInfo_t1165_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1165_0_0_0 = { 1, GenInst_TailoringInfo_t1165_0_0_0_Types };
extern const Il2CppType Contraction_t1166_0_0_0;
static const Il2CppType* GenInst_Contraction_t1166_0_0_0_Types[] = { &Contraction_t1166_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1166_0_0_0 = { 1, GenInst_Contraction_t1166_0_0_0_Types };
extern const Il2CppType Level2Map_t1168_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1168_0_0_0_Types[] = { &Level2Map_t1168_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1168_0_0_0 = { 1, GenInst_Level2Map_t1168_0_0_0_Types };
extern const Il2CppType BigInteger_t1189_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1189_0_0_0_Types[] = { &BigInteger_t1189_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1189_0_0_0 = { 1, GenInst_BigInteger_t1189_0_0_0_Types };
extern const Il2CppType Slot_t1239_0_0_0;
static const Il2CppType* GenInst_Slot_t1239_0_0_0_Types[] = { &Slot_t1239_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1239_0_0_0 = { 1, GenInst_Slot_t1239_0_0_0_Types };
extern const Il2CppType Slot_t1247_0_0_0;
static const Il2CppType* GenInst_Slot_t1247_0_0_0_Types[] = { &Slot_t1247_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1247_0_0_0 = { 1, GenInst_Slot_t1247_0_0_0_Types };
extern const Il2CppType StackFrame_t470_0_0_0;
static const Il2CppType* GenInst_StackFrame_t470_0_0_0_Types[] = { &StackFrame_t470_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t470_0_0_0 = { 1, GenInst_StackFrame_t470_0_0_0_Types };
extern const Il2CppType Calendar_t1260_0_0_0;
static const Il2CppType* GenInst_Calendar_t1260_0_0_0_Types[] = { &Calendar_t1260_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1260_0_0_0 = { 1, GenInst_Calendar_t1260_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1336_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1336_0_0_0_Types[] = { &ModuleBuilder_t1336_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1336_0_0_0 = { 1, GenInst_ModuleBuilder_t1336_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t2743_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t2743_0_0_0_Types[] = { &_ModuleBuilder_t2743_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2743_0_0_0 = { 1, GenInst__ModuleBuilder_t2743_0_0_0_Types };
extern const Il2CppType Module_t1332_0_0_0;
static const Il2CppType* GenInst_Module_t1332_0_0_0_Types[] = { &Module_t1332_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1332_0_0_0 = { 1, GenInst_Module_t1332_0_0_0_Types };
extern const Il2CppType _Module_t2753_0_0_0;
static const Il2CppType* GenInst__Module_t2753_0_0_0_Types[] = { &_Module_t2753_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2753_0_0_0 = { 1, GenInst__Module_t2753_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1342_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1342_0_0_0_Types[] = { &ParameterBuilder_t1342_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1342_0_0_0 = { 1, GenInst_ParameterBuilder_t1342_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2744_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2744_0_0_0_Types[] = { &_ParameterBuilder_t2744_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2744_0_0_0 = { 1, GenInst__ParameterBuilder_t2744_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t438_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t438_0_0_0_Types[] = { &TypeU5BU5D_t438_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t438_0_0_0 = { 1, GenInst_TypeU5BU5D_t438_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1326_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1326_0_0_0_Types[] = { &ILTokenInfo_t1326_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1326_0_0_0 = { 1, GenInst_ILTokenInfo_t1326_0_0_0_Types };
extern const Il2CppType LabelData_t1328_0_0_0;
static const Il2CppType* GenInst_LabelData_t1328_0_0_0_Types[] = { &LabelData_t1328_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1328_0_0_0 = { 1, GenInst_LabelData_t1328_0_0_0_Types };
extern const Il2CppType LabelFixup_t1327_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1327_0_0_0_Types[] = { &LabelFixup_t1327_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1327_0_0_0 = { 1, GenInst_LabelFixup_t1327_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1324_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1324_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1324_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1324_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1324_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1318_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1318_0_0_0_Types[] = { &TypeBuilder_t1318_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1318_0_0_0 = { 1, GenInst_TypeBuilder_t1318_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2745_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2745_0_0_0_Types[] = { &_TypeBuilder_t2745_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2745_0_0_0 = { 1, GenInst__TypeBuilder_t2745_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1325_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1325_0_0_0_Types[] = { &MethodBuilder_t1325_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1325_0_0_0 = { 1, GenInst_MethodBuilder_t1325_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t2742_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t2742_0_0_0_Types[] = { &_MethodBuilder_t2742_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t2742_0_0_0 = { 1, GenInst__MethodBuilder_t2742_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1316_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1316_0_0_0_Types[] = { &ConstructorBuilder_t1316_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1316_0_0_0 = { 1, GenInst_ConstructorBuilder_t1316_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2738_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2738_0_0_0_Types[] = { &_ConstructorBuilder_t2738_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2738_0_0_0 = { 1, GenInst__ConstructorBuilder_t2738_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1322_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1322_0_0_0_Types[] = { &FieldBuilder_t1322_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1322_0_0_0 = { 1, GenInst_FieldBuilder_t1322_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2740_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2740_0_0_0_Types[] = { &_FieldBuilder_t2740_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2740_0_0_0 = { 1, GenInst__FieldBuilder_t2740_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t2755_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t2755_0_0_0_Types[] = { &_PropertyInfo_t2755_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2755_0_0_0 = { 1, GenInst__PropertyInfo_t2755_0_0_0_Types };
extern const Il2CppType ResourceInfo_t1403_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t1403_0_0_0_Types[] = { &ResourceInfo_t1403_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1403_0_0_0 = { 1, GenInst_ResourceInfo_t1403_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t1404_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t1404_0_0_0_Types[] = { &ResourceCacheItem_t1404_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t1404_0_0_0 = { 1, GenInst_ResourceCacheItem_t1404_0_0_0_Types };
extern const Il2CppType IContextProperty_t1798_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1798_0_0_0_Types[] = { &IContextProperty_t1798_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1798_0_0_0 = { 1, GenInst_IContextProperty_t1798_0_0_0_Types };
extern const Il2CppType Header_t1486_0_0_0;
static const Il2CppType* GenInst_Header_t1486_0_0_0_Types[] = { &Header_t1486_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1486_0_0_0 = { 1, GenInst_Header_t1486_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1832_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1832_0_0_0_Types[] = { &ITrackingHandler_t1832_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1832_0_0_0 = { 1, GenInst_ITrackingHandler_t1832_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1819_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1819_0_0_0_Types[] = { &IContextAttribute_t1819_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1819_0_0_0 = { 1, GenInst_IContextAttribute_t1819_0_0_0_Types };
extern const Il2CppType IComparable_1_t3606_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3606_0_0_0_Types[] = { &IComparable_1_t3606_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3606_0_0_0 = { 1, GenInst_IComparable_1_t3606_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3611_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3611_0_0_0_Types[] = { &IEquatable_1_t3611_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3611_0_0_0 = { 1, GenInst_IEquatable_1_t3611_0_0_0_Types };
extern const Il2CppType IComparable_1_t3394_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3394_0_0_0_Types[] = { &IComparable_1_t3394_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3394_0_0_0 = { 1, GenInst_IComparable_1_t3394_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3395_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3395_0_0_0_Types[] = { &IEquatable_1_t3395_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3395_0_0_0 = { 1, GenInst_IEquatable_1_t3395_0_0_0_Types };
extern const Il2CppType IComparable_1_t3630_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3630_0_0_0_Types[] = { &IComparable_1_t3630_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3630_0_0_0 = { 1, GenInst_IComparable_1_t3630_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3635_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3635_0_0_0_Types[] = { &IEquatable_1_t3635_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3635_0_0_0 = { 1, GenInst_IEquatable_1_t3635_0_0_0_Types };
extern const Il2CppType TypeTag_t1542_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1542_0_0_0_Types[] = { &TypeTag_t1542_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1542_0_0_0 = { 1, GenInst_TypeTag_t1542_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t110_0_0_0;
static const Il2CppType* GenInst_Version_t110_0_0_0_Types[] = { &Version_t110_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t110_0_0_0 = { 1, GenInst_Version_t110_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t94_0_0_0_RaycastResult_t94_0_0_0_Types[] = { &RaycastResult_t94_0_0_0, &RaycastResult_t94_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t94_0_0_0_RaycastResult_t94_0_0_0 = { 2, GenInst_RaycastResult_t94_0_0_0_RaycastResult_t94_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t3_0_0_0_Vector3_t3_0_0_0_Types[] = { &Vector3_t3_0_0_0, &Vector3_t3_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3_0_0_0_Vector3_t3_0_0_0 = { 2, GenInst_Vector3_t3_0_0_0_Vector3_t3_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t90_0_0_0_Vector4_t90_0_0_0_Types[] = { &Vector4_t90_0_0_0, &Vector4_t90_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t90_0_0_0_Vector4_t90_0_0_0 = { 2, GenInst_Vector4_t90_0_0_0_Vector4_t90_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t15_0_0_0_Vector2_t15_0_0_0_Types[] = { &Vector2_t15_0_0_0, &Vector2_t15_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t15_0_0_0_Vector2_t15_0_0_0 = { 2, GenInst_Vector2_t15_0_0_0_Vector2_t15_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t187_0_0_0_Color32_t187_0_0_0_Types[] = { &Color32_t187_0_0_0, &Color32_t187_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t187_0_0_0_Color32_t187_0_0_0 = { 2, GenInst_Color32_t187_0_0_0_Color32_t187_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t296_0_0_0_UIVertex_t296_0_0_0_Types[] = { &UIVertex_t296_0_0_0, &UIVertex_t296_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t296_0_0_0_UIVertex_t296_0_0_0 = { 2, GenInst_UIVertex_t296_0_0_0_UIVertex_t296_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t285_0_0_0_UICharInfo_t285_0_0_0_Types[] = { &UICharInfo_t285_0_0_0, &UICharInfo_t285_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t285_0_0_0_UICharInfo_t285_0_0_0 = { 2, GenInst_UICharInfo_t285_0_0_0_UICharInfo_t285_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t286_0_0_0_UILineInfo_t286_0_0_0_Types[] = { &UILineInfo_t286_0_0_0, &UILineInfo_t286_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t286_0_0_0_UILineInfo_t286_0_0_0 = { 2, GenInst_UILineInfo_t286_0_0_0_UILineInfo_t286_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t943_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &DictionaryEntry_t943_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t943_0_0_0_DictionaryEntry_t943_0_0_0 = { 2, GenInst_DictionaryEntry_t943_0_0_0_DictionaryEntry_t943_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2047_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2047_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2047_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2047_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2047_0_0_0_KeyValuePair_2_t2047_0_0_0_Types[] = { &KeyValuePair_2_t2047_0_0_0, &KeyValuePair_2_t2047_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2047_0_0_0_KeyValuePair_2_t2047_0_0_0 = { 2, GenInst_KeyValuePair_2_t2047_0_0_0_KeyValuePair_2_t2047_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2066_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2066_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2066_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2066_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2066_0_0_0_KeyValuePair_2_t2066_0_0_0_Types[] = { &KeyValuePair_2_t2066_0_0_0, &KeyValuePair_2_t2066_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2066_0_0_0_KeyValuePair_2_t2066_0_0_0 = { 2, GenInst_KeyValuePair_2_t2066_0_0_0_KeyValuePair_2_t2066_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2086_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2086_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2086_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2086_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2086_0_0_0_KeyValuePair_2_t2086_0_0_0_Types[] = { &KeyValuePair_2_t2086_0_0_0, &KeyValuePair_2_t2086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2086_0_0_0_KeyValuePair_2_t2086_0_0_0 = { 2, GenInst_KeyValuePair_2_t2086_0_0_0_KeyValuePair_2_t2086_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t387_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t387_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t387_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t387_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0_Types[] = { &TextEditOp_t387_0_0_0, &TextEditOp_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0 = { 2, GenInst_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2114_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2114_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2114_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2114_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2114_0_0_0_KeyValuePair_2_t2114_0_0_0_Types[] = { &KeyValuePair_2_t2114_0_0_0, &KeyValuePair_2_t2114_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2114_0_0_0_KeyValuePair_2_t2114_0_0_0 = { 2, GenInst_KeyValuePair_2_t2114_0_0_0_KeyValuePair_2_t2114_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t455_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t455_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t455_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t455_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t455_0_0_0_Boolean_t455_0_0_0_Types[] = { &Boolean_t455_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t455_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Boolean_t455_0_0_0_Boolean_t455_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2342_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2342_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2342_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2342_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2342_0_0_0_KeyValuePair_2_t2342_0_0_0_Types[] = { &KeyValuePair_2_t2342_0_0_0, &KeyValuePair_2_t2342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2342_0_0_0_KeyValuePair_2_t2342_0_0_0 = { 2, GenInst_KeyValuePair_2_t2342_0_0_0_KeyValuePair_2_t2342_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2362_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2362_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2362_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2362_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2362_0_0_0_KeyValuePair_2_t2362_0_0_0_Types[] = { &KeyValuePair_2_t2362_0_0_0, &KeyValuePair_2_t2362_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2362_0_0_0_KeyValuePair_2_t2362_0_0_0 = { 2, GenInst_KeyValuePair_2_t2362_0_0_0_KeyValuePair_2_t2362_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1374_0_0_0_CustomAttributeTypedArgument_t1374_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1374_0_0_0, &CustomAttributeTypedArgument_t1374_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1374_0_0_0_CustomAttributeTypedArgument_t1374_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1374_0_0_0_CustomAttributeTypedArgument_t1374_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1373_0_0_0_CustomAttributeNamedArgument_t1373_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1373_0_0_0, &CustomAttributeNamedArgument_t1373_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1373_0_0_0_CustomAttributeNamedArgument_t1373_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t1373_0_0_0_CustomAttributeNamedArgument_t1373_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19587_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19587_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19587_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19587_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19587_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19588_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19588_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19588_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19588_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19588_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19589_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19589_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19589_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19589_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19590_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19590_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19590_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19590_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m19592_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m19592_gp_0_0_0_0_Types[] = { &Component_GetComponents_m19592_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m19592_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m19592_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m19595_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m19595_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m19595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m19595_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m19595_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m19598_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m19598_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m19598_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m19598_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m19598_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t2670_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t2670_gp_0_0_0_0_Types[] = { &InvokableCall_1_t2670_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t2670_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t2670_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2671_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2671_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2671_gp_0_0_0_0_InvokableCall_2_t2671_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2671_gp_0_0_0_0, &InvokableCall_2_t2671_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2671_gp_0_0_0_0_InvokableCall_2_t2671_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2671_gp_0_0_0_0_InvokableCall_2_t2671_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2671_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2671_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2671_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2671_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2671_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2671_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2671_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t2672_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t2672_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t2672_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t2672_gp_0_0_0_0_InvokableCall_3_t2672_gp_1_0_0_0_InvokableCall_3_t2672_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2672_gp_0_0_0_0, &InvokableCall_3_t2672_gp_1_0_0_0, &InvokableCall_3_t2672_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2672_gp_0_0_0_0_InvokableCall_3_t2672_gp_1_0_0_0_InvokableCall_3_t2672_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t2672_gp_0_0_0_0_InvokableCall_3_t2672_gp_1_0_0_0_InvokableCall_3_t2672_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2672_gp_0_0_0_0_Types[] = { &InvokableCall_3_t2672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2672_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t2672_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2672_gp_1_0_0_0_Types[] = { &InvokableCall_3_t2672_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2672_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t2672_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2672_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2672_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2672_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t2672_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t2673_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t2673_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t2673_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t2673_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t2673_gp_0_0_0_0_InvokableCall_4_t2673_gp_1_0_0_0_InvokableCall_4_t2673_gp_2_0_0_0_InvokableCall_4_t2673_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2673_gp_0_0_0_0, &InvokableCall_4_t2673_gp_1_0_0_0, &InvokableCall_4_t2673_gp_2_0_0_0, &InvokableCall_4_t2673_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2673_gp_0_0_0_0_InvokableCall_4_t2673_gp_1_0_0_0_InvokableCall_4_t2673_gp_2_0_0_0_InvokableCall_4_t2673_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t2673_gp_0_0_0_0_InvokableCall_4_t2673_gp_1_0_0_0_InvokableCall_4_t2673_gp_2_0_0_0_InvokableCall_4_t2673_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2673_gp_0_0_0_0_Types[] = { &InvokableCall_4_t2673_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2673_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t2673_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2673_gp_1_0_0_0_Types[] = { &InvokableCall_4_t2673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2673_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t2673_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2673_gp_2_0_0_0_Types[] = { &InvokableCall_4_t2673_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2673_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t2673_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2673_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2673_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2673_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t2673_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t480_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t480_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t480_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t480_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t2674_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t2674_gp_0_0_0_0_Types[] = { &UnityEvent_1_t2674_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t2674_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t2674_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t2675_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t2675_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t2675_gp_0_0_0_0_UnityEvent_2_t2675_gp_1_0_0_0_Types[] = { &UnityEvent_2_t2675_gp_0_0_0_0, &UnityEvent_2_t2675_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t2675_gp_0_0_0_0_UnityEvent_2_t2675_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t2675_gp_0_0_0_0_UnityEvent_2_t2675_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t2676_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t2676_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t2676_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t2676_gp_0_0_0_0_UnityEvent_3_t2676_gp_1_0_0_0_UnityEvent_3_t2676_gp_2_0_0_0_Types[] = { &UnityEvent_3_t2676_gp_0_0_0_0, &UnityEvent_3_t2676_gp_1_0_0_0, &UnityEvent_3_t2676_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t2676_gp_0_0_0_0_UnityEvent_3_t2676_gp_1_0_0_0_UnityEvent_3_t2676_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t2676_gp_0_0_0_0_UnityEvent_3_t2676_gp_1_0_0_0_UnityEvent_3_t2676_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t2677_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t2677_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t2677_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t2677_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t2677_gp_0_0_0_0_UnityEvent_4_t2677_gp_1_0_0_0_UnityEvent_4_t2677_gp_2_0_0_0_UnityEvent_4_t2677_gp_3_0_0_0_Types[] = { &UnityEvent_4_t2677_gp_0_0_0_0, &UnityEvent_4_t2677_gp_1_0_0_0, &UnityEvent_4_t2677_gp_2_0_0_0, &UnityEvent_4_t2677_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t2677_gp_0_0_0_0_UnityEvent_4_t2677_gp_1_0_0_0_UnityEvent_4_t2677_gp_2_0_0_0_UnityEvent_4_t2677_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t2677_gp_0_0_0_0_UnityEvent_4_t2677_gp_1_0_0_0_UnityEvent_4_t2677_gp_2_0_0_0_UnityEvent_4_t2677_gp_3_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m19682_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m19682_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m19682_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m19682_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m19682_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m19685_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m19685_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m19685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m19685_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m19685_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2685_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2685_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2685_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2685_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t2687_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Types[] = { &IndexedSet_1_t2687_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2687_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Int32_t106_0_0_0_Types[] = { &IndexedSet_1_t2687_gp_0_0_0_0, &Int32_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Int32_t106_0_0_0 = { 2, GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Int32_t106_0_0_0_Types };
extern const Il2CppType ListPool_1_t2688_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t2688_gp_0_0_0_0_Types[] = { &ListPool_1_t2688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t2688_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t2688_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3701_0_0_0;
static const Il2CppType* GenInst_List_1_t3701_0_0_0_Types[] = { &List_1_t3701_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3701_0_0_0 = { 1, GenInst_List_1_t3701_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t2689_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t2689_gp_0_0_0_0_Types[] = { &ObjectPool_1_t2689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t2689_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t2689_gp_0_0_0_0_Types };
extern const Il2CppType GenericGenerator_1_t2690_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGenerator_1_t2690_gp_0_0_0_0_Types[] = { &GenericGenerator_1_t2690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGenerator_1_t2690_gp_0_0_0_0 = { 1, GenInst_GenericGenerator_1_t2690_gp_0_0_0_0_Types };
extern const Il2CppType GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0_Types[] = { &GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0 = { 1, GenInst_GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2692_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2692_gp_0_0_0_0_Types[] = { &List_1_t2692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2692_gp_0_0_0_0 = { 1, GenInst_List_1_t2692_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3711_0_0_0;
static const Il2CppType* GenInst_List_1_t3711_0_0_0_Types[] = { &List_1_t3711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3711_0_0_0 = { 1, GenInst_List_1_t3711_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m19831_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m19831_gp_0_0_0_0_Types[] = { &Enumerable_First_m19831_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m19831_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m19831_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m19831_gp_0_0_0_0_Boolean_t455_0_0_0_Types[] = { &Enumerable_First_m19831_gp_0_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m19831_gp_0_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Enumerable_First_m19831_gp_0_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m19832_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m19832_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m19832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m19832_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m19832_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m19833_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Types[] = { &Enumerable_Select_m19833_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m19833_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m19833_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Enumerable_Select_m19833_gp_1_0_0_0_Types[] = { &Enumerable_Select_m19833_gp_0_0_0_0, &Enumerable_Select_m19833_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Enumerable_Select_m19833_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Enumerable_Select_m19833_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m19833_gp_1_0_0_0_Types[] = { &Enumerable_Select_m19833_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m19833_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m19833_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m19835_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m19835_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m19835_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m19835_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m19835_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m19836_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m19836_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m19836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m19836_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m19836_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m19837_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Types[] = { &Enumerable_Where_m19837_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m19837_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Boolean_t455_0_0_0_Types[] = { &Enumerable_Where_m19837_gp_0_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Boolean_t455_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType PredicateOf_1_t2694_gp_0_0_0_0;
static const Il2CppType* GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Boolean_t455_0_0_0_Types[] = { &PredicateOf_1_t2694_gp_0_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Boolean_t455_0_0_0_Types };
static const Il2CppType* GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Types[] = { &PredicateOf_1_t2694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PredicateOf_1_t2694_gp_0_0_0_0 = { 1, GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Boolean_t455_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0, &Boolean_t455_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Boolean_t455_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Boolean_t455_0_0_0_Types };
extern const Il2CppType Queue_1_t2698_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t2698_gp_0_0_0_0_Types[] = { &Queue_1_t2698_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t2698_gp_0_0_0_0 = { 1, GenInst_Queue_1_t2698_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2699_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2699_gp_0_0_0_0_Types[] = { &Enumerator_t2699_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2699_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2699_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t2700_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t2700_gp_0_0_0_0_Types[] = { &Stack_1_t2700_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t2700_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2700_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2701_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2701_gp_0_0_0_0_Types[] = { &Enumerator_t2701_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2701_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2701_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2707_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2707_gp_0_0_0_0_Types[] = { &IEnumerable_1_t2707_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2707_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2707_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20000_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20000_gp_0_0_0_0_Array_Sort_m20000_gp_0_0_0_0_Types[] = { &Array_Sort_m20000_gp_0_0_0_0, &Array_Sort_m20000_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20000_gp_0_0_0_0_Array_Sort_m20000_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m20000_gp_0_0_0_0_Array_Sort_m20000_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20001_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m20001_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20001_gp_0_0_0_0_Array_Sort_m20001_gp_1_0_0_0_Types[] = { &Array_Sort_m20001_gp_0_0_0_0, &Array_Sort_m20001_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20001_gp_0_0_0_0_Array_Sort_m20001_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m20001_gp_0_0_0_0_Array_Sort_m20001_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m20002_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20002_gp_0_0_0_0_Types[] = { &Array_Sort_m20002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20002_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20002_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m20002_gp_0_0_0_0_Array_Sort_m20002_gp_0_0_0_0_Types[] = { &Array_Sort_m20002_gp_0_0_0_0, &Array_Sort_m20002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20002_gp_0_0_0_0_Array_Sort_m20002_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m20002_gp_0_0_0_0_Array_Sort_m20002_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20003_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20003_gp_0_0_0_0_Types[] = { &Array_Sort_m20003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20003_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20003_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20003_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20003_gp_0_0_0_0_Array_Sort_m20003_gp_1_0_0_0_Types[] = { &Array_Sort_m20003_gp_0_0_0_0, &Array_Sort_m20003_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20003_gp_0_0_0_0_Array_Sort_m20003_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m20003_gp_0_0_0_0_Array_Sort_m20003_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m20004_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20004_gp_0_0_0_0_Array_Sort_m20004_gp_0_0_0_0_Types[] = { &Array_Sort_m20004_gp_0_0_0_0, &Array_Sort_m20004_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20004_gp_0_0_0_0_Array_Sort_m20004_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m20004_gp_0_0_0_0_Array_Sort_m20004_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20005_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m20005_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20005_gp_0_0_0_0_Array_Sort_m20005_gp_1_0_0_0_Types[] = { &Array_Sort_m20005_gp_0_0_0_0, &Array_Sort_m20005_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20005_gp_0_0_0_0_Array_Sort_m20005_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m20005_gp_0_0_0_0_Array_Sort_m20005_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m20006_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20006_gp_0_0_0_0_Types[] = { &Array_Sort_m20006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20006_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20006_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m20006_gp_0_0_0_0_Array_Sort_m20006_gp_0_0_0_0_Types[] = { &Array_Sort_m20006_gp_0_0_0_0, &Array_Sort_m20006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20006_gp_0_0_0_0_Array_Sort_m20006_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m20006_gp_0_0_0_0_Array_Sort_m20006_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20007_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20007_gp_0_0_0_0_Types[] = { &Array_Sort_m20007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20007_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20007_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20007_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20007_gp_1_0_0_0_Types[] = { &Array_Sort_m20007_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20007_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m20007_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m20007_gp_0_0_0_0_Array_Sort_m20007_gp_1_0_0_0_Types[] = { &Array_Sort_m20007_gp_0_0_0_0, &Array_Sort_m20007_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20007_gp_0_0_0_0_Array_Sort_m20007_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m20007_gp_0_0_0_0_Array_Sort_m20007_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m20008_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20008_gp_0_0_0_0_Types[] = { &Array_Sort_m20008_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20008_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20008_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m20009_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m20009_gp_0_0_0_0_Types[] = { &Array_Sort_m20009_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m20009_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m20009_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m20010_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m20010_gp_0_0_0_0_Types[] = { &Array_qsort_m20010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m20010_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m20010_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m20010_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m20010_gp_0_0_0_0_Array_qsort_m20010_gp_1_0_0_0_Types[] = { &Array_qsort_m20010_gp_0_0_0_0, &Array_qsort_m20010_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m20010_gp_0_0_0_0_Array_qsort_m20010_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m20010_gp_0_0_0_0_Array_qsort_m20010_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m20011_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m20011_gp_0_0_0_0_Types[] = { &Array_compare_m20011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m20011_gp_0_0_0_0 = { 1, GenInst_Array_compare_m20011_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m20012_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m20012_gp_0_0_0_0_Types[] = { &Array_qsort_m20012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m20012_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m20012_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m20015_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m20015_gp_0_0_0_0_Types[] = { &Array_Resize_m20015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m20015_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m20015_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m20017_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m20017_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m20017_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m20017_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m20017_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m20018_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m20018_gp_0_0_0_0_Types[] = { &Array_ForEach_m20018_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m20018_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m20018_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m20019_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m20019_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m20019_gp_0_0_0_0_Array_ConvertAll_m20019_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m20019_gp_0_0_0_0, &Array_ConvertAll_m20019_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m20019_gp_0_0_0_0_Array_ConvertAll_m20019_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m20019_gp_0_0_0_0_Array_ConvertAll_m20019_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m20020_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m20020_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m20020_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m20020_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m20020_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m20021_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m20021_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m20021_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m20021_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m20021_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m20022_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m20022_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m20022_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m20022_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m20022_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m20023_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m20023_gp_0_0_0_0_Types[] = { &Array_FindIndex_m20023_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m20023_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m20023_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m20024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m20024_gp_0_0_0_0_Types[] = { &Array_FindIndex_m20024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m20024_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m20024_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m20025_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m20025_gp_0_0_0_0_Types[] = { &Array_FindIndex_m20025_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m20025_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m20025_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m20026_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m20026_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m20026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m20026_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m20026_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m20027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m20027_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m20027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m20027_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m20027_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m20028_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m20028_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m20028_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m20028_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m20028_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m20029_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m20029_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m20029_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m20029_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m20029_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m20030_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m20030_gp_0_0_0_0_Types[] = { &Array_IndexOf_m20030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m20030_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m20030_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m20031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m20031_gp_0_0_0_0_Types[] = { &Array_IndexOf_m20031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m20031_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m20031_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m20032_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m20032_gp_0_0_0_0_Types[] = { &Array_IndexOf_m20032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m20032_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m20032_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m20033_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m20033_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m20033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m20033_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m20033_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m20034_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m20034_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m20034_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m20034_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m20034_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m20035_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m20035_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m20035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m20035_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m20035_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m20036_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m20036_gp_0_0_0_0_Types[] = { &Array_FindAll_m20036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m20036_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m20036_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m20037_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m20037_gp_0_0_0_0_Types[] = { &Array_Exists_m20037_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m20037_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m20037_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m20038_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m20038_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m20038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m20038_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m20038_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m20039_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m20039_gp_0_0_0_0_Types[] = { &Array_Find_m20039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m20039_gp_0_0_0_0 = { 1, GenInst_Array_Find_m20039_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m20040_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m20040_gp_0_0_0_0_Types[] = { &Array_FindLast_m20040_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m20040_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m20040_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2708_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2708_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2708_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2708_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2708_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2709_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2709_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2709_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2709_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2709_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2711_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2711_gp_0_0_0_0_Types[] = { &IList_1_t2711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2711_gp_0_0_0_0 = { 1, GenInst_IList_1_t2711_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t2712_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2712_gp_0_0_0_0_Types[] = { &ICollection_1_t2712_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2712_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t2712_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1809_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1809_gp_0_0_0_0_Types[] = { &Nullable_1_t1809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1809_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1809_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t2719_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t2719_gp_0_0_0_0_Types[] = { &Comparer_1_t2719_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t2719_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t2719_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2720_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2720_gp_0_0_0_0_Types[] = { &DefaultComparer_t2720_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2720_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2720_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2663_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2663_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2663_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2663_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2721_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2721_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0, &Dictionary_2_t2721_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3820_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3820_0_0_0_Types[] = { &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3820_0_0_0 = { 1, GenInst_KeyValuePair_2_t3820_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0, &Dictionary_2_t2721_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0, &Dictionary_2_t2721_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_DictionaryEntry_t943_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0, &Dictionary_2_t2721_gp_1_0_0_0, &DictionaryEntry_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_DictionaryEntry_t943_0_0_0 = { 3, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_DictionaryEntry_t943_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t2722_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2722_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t2722_gp_0_0_0_0_ShimEnumerator_t2722_gp_1_0_0_0_Types[] = { &ShimEnumerator_t2722_gp_0_0_0_0, &ShimEnumerator_t2722_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2722_gp_0_0_0_0_ShimEnumerator_t2722_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2722_gp_0_0_0_0_ShimEnumerator_t2722_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2723_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2723_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2723_gp_0_0_0_0_Enumerator_t2723_gp_1_0_0_0_Types[] = { &Enumerator_t2723_gp_0_0_0_0, &Enumerator_t2723_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2723_gp_0_0_0_0_Enumerator_t2723_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2723_gp_0_0_0_0_Enumerator_t2723_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3833_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3833_0_0_0_Types[] = { &KeyValuePair_2_t3833_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3833_0_0_0 = { 1, GenInst_KeyValuePair_2_t3833_0_0_0_Types };
extern const Il2CppType ValueCollection_t2724_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2724_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types[] = { &ValueCollection_t2724_gp_0_0_0_0, &ValueCollection_t2724_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2724_gp_1_0_0_0_Types[] = { &ValueCollection_t2724_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2724_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2724_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2725_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2725_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2725_gp_0_0_0_0_Enumerator_t2725_gp_1_0_0_0_Types[] = { &Enumerator_t2725_gp_0_0_0_0, &Enumerator_t2725_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2725_gp_0_0_0_0_Enumerator_t2725_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2725_gp_0_0_0_0_Enumerator_t2725_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2725_gp_1_0_0_0_Types[] = { &Enumerator_t2725_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2725_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2725_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types[] = { &ValueCollection_t2724_gp_0_0_0_0, &ValueCollection_t2724_gp_1_0_0_0, &ValueCollection_t2724_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types[] = { &ValueCollection_t2724_gp_1_0_0_0, &ValueCollection_t2724_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0_Types[] = { &Dictionary_2_t2721_gp_0_0_0_0, &Dictionary_2_t2721_gp_1_0_0_0, &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0 = { 3, GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0_Types[] = { &KeyValuePair_2_t3820_0_0_0, &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0 = { 2, GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2721_gp_1_0_0_0_Types[] = { &Dictionary_2_t2721_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2721_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2721_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2727_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2727_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2727_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2727_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2727_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2728_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2728_gp_0_0_0_0_Types[] = { &DefaultComparer_t2728_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2728_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2728_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2662_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2662_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2662_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2662_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2662_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3860_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3860_0_0_0_Types[] = { &KeyValuePair_2_t3860_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3860_0_0_0 = { 1, GenInst_KeyValuePair_2_t3860_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2730_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t2730_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2730_gp_0_0_0_0_IDictionary_2_t2730_gp_1_0_0_0_Types[] = { &IDictionary_2_t2730_gp_0_0_0_0, &IDictionary_2_t2730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2730_gp_0_0_0_0_IDictionary_2_t2730_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2730_gp_0_0_0_0_IDictionary_2_t2730_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2732_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2732_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2732_gp_0_0_0_0_KeyValuePair_2_t2732_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t2732_gp_0_0_0_0, &KeyValuePair_2_t2732_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2732_gp_0_0_0_0_KeyValuePair_2_t2732_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2732_gp_0_0_0_0_KeyValuePair_2_t2732_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t2733_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2733_gp_0_0_0_0_Types[] = { &List_1_t2733_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2733_gp_0_0_0_0 = { 1, GenInst_List_1_t2733_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2734_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2734_gp_0_0_0_0_Types[] = { &Enumerator_t2734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2734_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2734_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t2735_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t2735_gp_0_0_0_0_Types[] = { &Collection_1_t2735_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t2735_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2735_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2736_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2736_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2736_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2736_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[546] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Renderer_t76_0_0_0,
	&GenInst_StereoController_t31_0_0_0,
	&GenInst_Collider_t79_0_0_0,
	&GenInst_CardboardOnGUIWindow_t16_0_0_0,
	&GenInst_CardboardOnGUIMouse_t13_0_0_0,
	&GenInst_MeshRenderer_t17_0_0_0,
	&GenInst_Cardboard_t24_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_Camera_t32_0_0_0,
	&GenInst_CardboardHead_t5_0_0_0,
	&GenInst_RadialUndistortionEffect_t52_0_0_0,
	&GenInst_ISelectHandler_t95_0_0_0,
	&GenInst_IUpdateSelectedHandler_t98_0_0_0,
	&GenInst_IPointerUpHandler_t100_0_0_0,
	&GenInst_IPointerClickHandler_t102_0_0_0,
	&GenInst_IPointerDownHandler_t104_0_0_0,
	&GenInst_CardboardEye_t29_0_0_0,
	&GenInst_CardboardEye_t29_0_0_0_Boolean_t455_0_0_0,
	&GenInst_CardboardEye_t29_0_0_0_CardboardHead_t5_0_0_0,
	&GenInst_Int32_t106_0_0_0,
	&GenInst_Rigidbody_t112_0_0_0,
	&GenInst_AsyncOperation_t127_0_0_0,
	&GenInst_GUITexture_t141_0_0_0,
	&GenInst_GUIText_t152_0_0_0,
	&GenInst_YieldInstruction_t164_0_0_0,
	&GenInst_GcLeaderboard_t178_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t446_0_0_0,
	&GenInst_Boolean_t455_0_0_0,
	&GenInst_IAchievementU5BU5D_t448_0_0_0,
	&GenInst_IScoreU5BU5D_t365_0_0_0,
	&GenInst_IUserProfileU5BU5D_t361_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Rigidbody2D_t254_0_0_0,
	&GenInst_Font_t283_0_0_0,
	&GenInst_UIVertex_t296_0_0_0,
	&GenInst_UICharInfo_t285_0_0_0,
	&GenInst_UILineInfo_t286_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t106_0_0_0,
	&GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0,
	&GenInst_GUILayoutEntry_t316_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t192_0_0_0,
	&GenInst_Single_t113_0_0_0,
	&GenInst_PersistentCall_t396_0_0_0,
	&GenInst_BaseInvokableCall_t392_0_0_0,
	&GenInst_BaseInputModule_t46_0_0_0,
	&GenInst_RaycastResult_t94_0_0_0,
	&GenInst_IDeselectHandler_t668_0_0_0,
	&GenInst_BaseEventData_t92_0_0_0,
	&GenInst_Entry_t489_0_0_0,
	&GenInst_IPointerEnterHandler_t660_0_0_0,
	&GenInst_IPointerExitHandler_t661_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t662_0_0_0,
	&GenInst_IBeginDragHandler_t663_0_0_0,
	&GenInst_IDragHandler_t664_0_0_0,
	&GenInst_IEndDragHandler_t665_0_0_0,
	&GenInst_IDropHandler_t666_0_0_0,
	&GenInst_IScrollHandler_t667_0_0_0,
	&GenInst_IMoveHandler_t669_0_0_0,
	&GenInst_ISubmitHandler_t670_0_0_0,
	&GenInst_ICancelHandler_t671_0_0_0,
	&GenInst_List_1_t659_0_0_0,
	&GenInst_IEventSystemHandler_t1889_0_0_0,
	&GenInst_Transform_t33_0_0_0,
	&GenInst_PointerEventData_t48_0_0_0,
	&GenInst_AxisEventData_t513_0_0_0,
	&GenInst_BaseRaycaster_t512_0_0_0,
	&GenInst_GameObject_t47_0_0_0,
	&GenInst_EventSystem_t91_0_0_0,
	&GenInst_ButtonState_t517_0_0_0,
	&GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0,
	&GenInst_SpriteRenderer_t220_0_0_0,
	&GenInst_RaycastHit_t78_0_0_0,
	&GenInst_Color_t11_0_0_0,
	&GenInst_ICanvasElement_t675_0_0_0,
	&GenInst_Dropdown_t557_0_0_0,
	&GenInst_OptionData_t551_0_0_0,
	&GenInst_DropdownItem_t547_0_0_0,
	&GenInst_FloatTween_t535_0_0_0,
	&GenInst_Toggle_t550_0_0_0,
	&GenInst_Canvas_t294_0_0_0,
	&GenInst_GraphicRaycaster_t567_0_0_0,
	&GenInst_CanvasGroup_t295_0_0_0,
	&GenInst_RectTransform_t211_0_0_0,
	&GenInst_Image_t549_0_0_0,
	&GenInst_Button_t539_0_0_0,
	&GenInst_Font_t283_0_0_0_List_1_t690_0_0_0,
	&GenInst_Text_t548_0_0_0,
	&GenInst_ColorTween_t532_0_0_0,
	&GenInst_CanvasRenderer_t297_0_0_0,
	&GenInst_Component_t122_0_0_0,
	&GenInst_Graphic_t564_0_0_0,
	&GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0,
	&GenInst_Sprite_t219_0_0_0,
	&GenInst_Type_t572_0_0_0,
	&GenInst_FillMethod_t573_0_0_0,
	&GenInst_SubmitEvent_t579_0_0_0,
	&GenInst_OnChangeEvent_t581_0_0_0,
	&GenInst_OnValidateInput_t583_0_0_0,
	&GenInst_ContentType_t575_0_0_0,
	&GenInst_LineType_t578_0_0_0,
	&GenInst_InputType_t576_0_0_0,
	&GenInst_TouchScreenKeyboardType_t201_0_0_0,
	&GenInst_CharacterValidation_t577_0_0_0,
	&GenInst_Char_t699_0_0_0,
	&GenInst_LayoutElement_t646_0_0_0,
	&GenInst_Mask_t587_0_0_0,
	&GenInst_IClippable_t678_0_0_0,
	&GenInst_RectMask2D_t590_0_0_0,
	&GenInst_Direction_t599_0_0_0,
	&GenInst_Vector2_t15_0_0_0,
	&GenInst_Selectable_t540_0_0_0,
	&GenInst_Navigation_t594_0_0_0,
	&GenInst_Transition_t609_0_0_0,
	&GenInst_ColorBlock_t546_0_0_0,
	&GenInst_SpriteState_t611_0_0_0,
	&GenInst_AnimationTriggers_t536_0_0_0,
	&GenInst_Animator_t273_0_0_0,
	&GenInst_Direction_t615_0_0_0,
	&GenInst_MatEntry_t619_0_0_0,
	&GenInst_Toggle_t550_0_0_0_Boolean_t455_0_0_0,
	&GenInst_IClipper_t680_0_0_0,
	&GenInst_AspectMode_t631_0_0_0,
	&GenInst_FitMode_t637_0_0_0,
	&GenInst_Corner_t639_0_0_0,
	&GenInst_Axis_t640_0_0_0,
	&GenInst_Constraint_t641_0_0_0,
	&GenInst_RectOffset_t318_0_0_0,
	&GenInst_TextAnchor_t278_0_0_0,
	&GenInst_ILayoutElement_t681_0_0_0_Single_t113_0_0_0,
	&GenInst_Vector3_t3_0_0_0,
	&GenInst_Color32_t187_0_0_0,
	&GenInst_Vector4_t90_0_0_0,
	&GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1374_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1373_0_0_0,
	&GenInst_StrongName_t1623_0_0_0,
	&GenInst_DateTime_t308_0_0_0,
	&GenInst_DateTimeOffset_t1698_0_0_0,
	&GenInst_TimeSpan_t846_0_0_0,
	&GenInst_Guid_t1720_0_0_0,
	&GenInst_CustomAttributeData_t1370_0_0_0,
	&GenInst_MonoBehaviour_t2_0_0_0,
	&GenInst_Behaviour_t185_0_0_0,
	&GenInst_Object_t82_0_0_0,
	&GenInst_IReflect_t2715_0_0_0,
	&GenInst__Type_t2713_0_0_0,
	&GenInst_ICustomAttributeProvider_t1804_0_0_0,
	&GenInst__MemberInfo_t2714_0_0_0,
	&GenInst_IFormattable_t1806_0_0_0,
	&GenInst_IConvertible_t737_0_0_0,
	&GenInst_IComparable_t1808_0_0_0,
	&GenInst_IComparable_1_t2935_0_0_0,
	&GenInst_IEquatable_1_t2940_0_0_0,
	&GenInst_ValueType_t1118_0_0_0,
	&GenInst_Double_t465_0_0_0,
	&GenInst_IComparable_1_t2952_0_0_0,
	&GenInst_IEquatable_1_t2957_0_0_0,
	&GenInst_IComparable_1_t2965_0_0_0,
	&GenInst_IEquatable_1_t2970_0_0_0,
	&GenInst_IComparable_1_t2981_0_0_0,
	&GenInst_IEquatable_1_t2986_0_0_0,
	&GenInst_List_1_t703_0_0_0,
	&GenInst_List_1_t426_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Byte_t454_0_0_0,
	&GenInst_IComparable_1_t3004_0_0_0,
	&GenInst_IEquatable_1_t3009_0_0_0,
	&GenInst_IAchievementDescription_t2669_0_0_0,
	&GenInst_IAchievement_t416_0_0_0,
	&GenInst_IScore_t367_0_0_0,
	&GenInst_IUserProfile_t2668_0_0_0,
	&GenInst_AchievementDescription_t363_0_0_0,
	&GenInst_UserProfile_t360_0_0_0,
	&GenInst_GcAchievementData_t347_0_0_0,
	&GenInst_Achievement_t362_0_0_0,
	&GenInst_GcScoreData_t348_0_0_0,
	&GenInst_Score_t364_0_0_0,
	&GenInst_Display_t229_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t1837_0_0_0,
	&GenInst_ContactPoint_t247_0_0_0,
	&GenInst_RaycastHit2D_t252_0_0_0,
	&GenInst_ContactPoint2D_t255_0_0_0,
	&GenInst_Keyframe_t269_0_0_0,
	&GenInst_CharacterInfo_t281_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t106_0_0_0,
	&GenInst_KeyValuePair_2_t2047_0_0_0,
	&GenInst_Link_t1229_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_DictionaryEntry_t943_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2047_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2057_0_0_0,
	&GenInst_GUILayoutOption_t321_0_0_0,
	&GenInst_Int32_t106_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2066_0_0_0,
	&GenInst_Int32_t106_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t106_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Int32_t106_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2066_0_0_0,
	&GenInst_Int32_t106_0_0_0_LayoutCache_t311_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2074_0_0_0,
	&GenInst_GUIStyle_t315_0_0_0,
	&GenInst_KeyValuePair_2_t2086_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2086_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t315_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2094_0_0_0,
	&GenInst_DisallowMultipleComponent_t337_0_0_0,
	&GenInst_Attribute_t215_0_0_0,
	&GenInst__Attribute_t2703_0_0_0,
	&GenInst_ExecuteInEditMode_t340_0_0_0,
	&GenInst_RequireComponent_t338_0_0_0,
	&GenInst_ParameterModifier_t1392_0_0_0,
	&GenInst_HitInfo_t368_0_0_0,
	&GenInst_ParameterInfo_t473_0_0_0,
	&GenInst__ParameterInfo_t2754_0_0_0,
	&GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0,
	&GenInst_KeyValuePair_2_t2114_0_0_0,
	&GenInst_TextEditOp_t387_0_0_0,
	&GenInst_Enum_t150_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t387_0_0_0_KeyValuePair_2_t2114_0_0_0,
	&GenInst_Event_t84_0_0_0,
	&GenInst_Event_t84_0_0_0_TextEditOp_t387_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2125_0_0_0,
	&GenInst_Int32_t106_0_0_0_PointerEventData_t48_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t685_0_0_0,
	&GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0,
	&GenInst_ICanvasElement_t675_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_List_1_t687_0_0_0,
	&GenInst_Font_t283_0_0_0_List_1_t690_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2229_0_0_0,
	&GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0,
	&GenInst_Graphic_t564_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Canvas_t294_0_0_0_IndexedSet_1_t698_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2242_0_0_0,
	&GenInst_KeyValuePair_2_t2245_0_0_0,
	&GenInst_KeyValuePair_2_t2248_0_0_0,
	&GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0,
	&GenInst_IClipper_t680_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2295_0_0_0,
	&GenInst_LayoutRebuilder_t648_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t113_0_0_0,
	&GenInst_List_1_t417_0_0_0,
	&GenInst_List_1_t423_0_0_0,
	&GenInst_List_1_t421_0_0_0,
	&GenInst_List_1_t419_0_0_0,
	&GenInst_List_1_t424_0_0_0,
	&GenInst_List_1_t2325_0_0_0,
	&GenInst_DispatcherKey_t722_0_0_0,
	&GenInst_DispatcherKey_t722_0_0_0_Dispatcher_t718_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2330_0_0_0,
	&GenInst__MethodInfo_t2752_0_0_0,
	&GenInst_MethodBase_t471_0_0_0,
	&GenInst__MethodBase_t2751_0_0_0,
	&GenInst_KeySizes_t766_0_0_0,
	&GenInst_UInt32_t467_0_0_0,
	&GenInst_IComparable_1_t3283_0_0_0,
	&GenInst_IEquatable_1_t3288_0_0_0,
	&GenInst_UInt16_t960_0_0_0,
	&GenInst_IComparable_1_t3296_0_0_0,
	&GenInst_IEquatable_1_t3301_0_0_0,
	&GenInst_KeyValuePair_2_t2342_0_0_0,
	&GenInst_IComparable_1_t3313_0_0_0,
	&GenInst_IEquatable_1_t3318_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t455_0_0_0_KeyValuePair_2_t2342_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t455_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2354_0_0_0,
	&GenInst_X509Certificate_t830_0_0_0,
	&GenInst_IDeserializationCallback_t1840_0_0_0,
	&GenInst_X509ChainStatus_t843_0_0_0,
	&GenInst_Capture_t865_0_0_0,
	&GenInst_Group_t868_0_0_0,
	&GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0,
	&GenInst_KeyValuePair_2_t2362_0_0_0,
	&GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_Int32_t106_0_0_0,
	&GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_Int32_t106_0_0_0_Int32_t106_0_0_0_KeyValuePair_2_t2362_0_0_0,
	&GenInst_Mark_t893_0_0_0,
	&GenInst_UriScheme_t930_0_0_0,
	&GenInst_BigInteger_t987_0_0_0,
	&GenInst_ByteU5BU5D_t75_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_IEnumerable_t731_0_0_0,
	&GenInst_ICloneable_t1818_0_0_0,
	&GenInst_ICollection_t952_0_0_0,
	&GenInst_IList_t902_0_0_0,
	&GenInst_ClientCertificateType_t1074_0_0_0,
	&GenInst_Int64_t466_0_0_0,
	&GenInst_UInt64_t1123_0_0_0,
	&GenInst_SByte_t1124_0_0_0,
	&GenInst_Int16_t1125_0_0_0,
	&GenInst_Decimal_t738_0_0_0,
	&GenInst_Delegate_t81_0_0_0,
	&GenInst_IComparable_1_t3386_0_0_0,
	&GenInst_IEquatable_1_t3387_0_0_0,
	&GenInst_IComparable_1_t3390_0_0_0,
	&GenInst_IEquatable_1_t3391_0_0_0,
	&GenInst_IComparable_1_t3388_0_0_0,
	&GenInst_IEquatable_1_t3389_0_0_0,
	&GenInst_IComparable_1_t3384_0_0_0,
	&GenInst_IEquatable_1_t3385_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2750_0_0_0,
	&GenInst_ConstructorInfo_t479_0_0_0,
	&GenInst__ConstructorInfo_t2748_0_0_0,
	&GenInst_TableRange_t1162_0_0_0,
	&GenInst_TailoringInfo_t1165_0_0_0,
	&GenInst_Contraction_t1166_0_0_0,
	&GenInst_Level2Map_t1168_0_0_0,
	&GenInst_BigInteger_t1189_0_0_0,
	&GenInst_Slot_t1239_0_0_0,
	&GenInst_Slot_t1247_0_0_0,
	&GenInst_StackFrame_t470_0_0_0,
	&GenInst_Calendar_t1260_0_0_0,
	&GenInst_ModuleBuilder_t1336_0_0_0,
	&GenInst__ModuleBuilder_t2743_0_0_0,
	&GenInst_Module_t1332_0_0_0,
	&GenInst__Module_t2753_0_0_0,
	&GenInst_ParameterBuilder_t1342_0_0_0,
	&GenInst__ParameterBuilder_t2744_0_0_0,
	&GenInst_TypeU5BU5D_t438_0_0_0,
	&GenInst_ILTokenInfo_t1326_0_0_0,
	&GenInst_LabelData_t1328_0_0_0,
	&GenInst_LabelFixup_t1327_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1324_0_0_0,
	&GenInst_TypeBuilder_t1318_0_0_0,
	&GenInst__TypeBuilder_t2745_0_0_0,
	&GenInst_MethodBuilder_t1325_0_0_0,
	&GenInst__MethodBuilder_t2742_0_0_0,
	&GenInst_ConstructorBuilder_t1316_0_0_0,
	&GenInst__ConstructorBuilder_t2738_0_0_0,
	&GenInst_FieldBuilder_t1322_0_0_0,
	&GenInst__FieldBuilder_t2740_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t2755_0_0_0,
	&GenInst_ResourceInfo_t1403_0_0_0,
	&GenInst_ResourceCacheItem_t1404_0_0_0,
	&GenInst_IContextProperty_t1798_0_0_0,
	&GenInst_Header_t1486_0_0_0,
	&GenInst_ITrackingHandler_t1832_0_0_0,
	&GenInst_IContextAttribute_t1819_0_0_0,
	&GenInst_IComparable_1_t3606_0_0_0,
	&GenInst_IEquatable_1_t3611_0_0_0,
	&GenInst_IComparable_1_t3394_0_0_0,
	&GenInst_IEquatable_1_t3395_0_0_0,
	&GenInst_IComparable_1_t3630_0_0_0,
	&GenInst_IEquatable_1_t3635_0_0_0,
	&GenInst_TypeTag_t1542_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t110_0_0_0,
	&GenInst_RaycastResult_t94_0_0_0_RaycastResult_t94_0_0_0,
	&GenInst_Vector3_t3_0_0_0_Vector3_t3_0_0_0,
	&GenInst_Vector4_t90_0_0_0_Vector4_t90_0_0_0,
	&GenInst_Vector2_t15_0_0_0_Vector2_t15_0_0_0,
	&GenInst_Color32_t187_0_0_0_Color32_t187_0_0_0,
	&GenInst_UIVertex_t296_0_0_0_UIVertex_t296_0_0_0,
	&GenInst_UICharInfo_t285_0_0_0_UICharInfo_t285_0_0_0,
	&GenInst_UILineInfo_t286_0_0_0_UILineInfo_t286_0_0_0,
	&GenInst_DictionaryEntry_t943_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_KeyValuePair_2_t2047_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2047_0_0_0_KeyValuePair_2_t2047_0_0_0,
	&GenInst_KeyValuePair_2_t2066_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2066_0_0_0_KeyValuePair_2_t2066_0_0_0,
	&GenInst_KeyValuePair_2_t2086_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2086_0_0_0_KeyValuePair_2_t2086_0_0_0,
	&GenInst_TextEditOp_t387_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t387_0_0_0_TextEditOp_t387_0_0_0,
	&GenInst_KeyValuePair_2_t2114_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2114_0_0_0_KeyValuePair_2_t2114_0_0_0,
	&GenInst_Boolean_t455_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t455_0_0_0_Boolean_t455_0_0_0,
	&GenInst_KeyValuePair_2_t2342_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2342_0_0_0_KeyValuePair_2_t2342_0_0_0,
	&GenInst_KeyValuePair_2_t2362_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2362_0_0_0_KeyValuePair_2_t2362_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1374_0_0_0_CustomAttributeTypedArgument_t1374_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1373_0_0_0_CustomAttributeNamedArgument_t1373_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19587_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19588_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19589_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19590_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m19592_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m19595_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m19597_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m19598_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t2670_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2671_gp_0_0_0_0_InvokableCall_2_t2671_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2671_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2671_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2672_gp_0_0_0_0_InvokableCall_3_t2672_gp_1_0_0_0_InvokableCall_3_t2672_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t2672_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t2672_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2672_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2673_gp_0_0_0_0_InvokableCall_4_t2673_gp_1_0_0_0_InvokableCall_4_t2673_gp_2_0_0_0_InvokableCall_4_t2673_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t2673_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t2673_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t2673_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2673_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t480_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t2674_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t2675_gp_0_0_0_0_UnityEvent_2_t2675_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t2676_gp_0_0_0_0_UnityEvent_3_t2676_gp_1_0_0_0_UnityEvent_3_t2676_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t2677_gp_0_0_0_0_UnityEvent_4_t2677_gp_1_0_0_0_UnityEvent_4_t2677_gp_2_0_0_0_UnityEvent_4_t2677_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m19682_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m19683_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m19685_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m19686_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m19687_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2685_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m19712_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2687_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2687_gp_0_0_0_0_Int32_t106_0_0_0,
	&GenInst_ListPool_1_t2688_gp_0_0_0_0,
	&GenInst_List_1_t3701_0_0_0,
	&GenInst_ObjectPool_1_t2689_gp_0_0_0_0,
	&GenInst_GenericGenerator_1_t2690_gp_0_0_0_0,
	&GenInst_GenericGeneratorEnumerator_1_t2691_gp_0_0_0_0,
	&GenInst_List_1_t2692_gp_0_0_0_0,
	&GenInst_List_1_t3711_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t2693_gp_0_0_0_0,
	&GenInst_Enumerable_First_m19831_gp_0_0_0_0,
	&GenInst_Enumerable_First_m19831_gp_0_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m19832_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m19833_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m19833_gp_0_0_0_0_Enumerable_Select_m19833_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m19833_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m19834_gp_0_0_0_0_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m19834_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m19835_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m19836_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m19837_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m19837_gp_0_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m19838_gp_0_0_0_0_Boolean_t455_0_0_0,
	&GenInst_PredicateOf_1_t2694_gp_0_0_0_0_Boolean_t455_0_0_0,
	&GenInst_PredicateOf_1_t2694_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2695_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2696_gp_0_0_0_0_Boolean_t455_0_0_0,
	&GenInst_Queue_1_t2698_gp_0_0_0_0,
	&GenInst_Enumerator_t2699_gp_0_0_0_0,
	&GenInst_Stack_1_t2700_gp_0_0_0_0,
	&GenInst_Enumerator_t2701_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t2707_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19988_gp_0_0_0_0,
	&GenInst_Array_Sort_m20000_gp_0_0_0_0_Array_Sort_m20000_gp_0_0_0_0,
	&GenInst_Array_Sort_m20001_gp_0_0_0_0_Array_Sort_m20001_gp_1_0_0_0,
	&GenInst_Array_Sort_m20002_gp_0_0_0_0,
	&GenInst_Array_Sort_m20002_gp_0_0_0_0_Array_Sort_m20002_gp_0_0_0_0,
	&GenInst_Array_Sort_m20003_gp_0_0_0_0,
	&GenInst_Array_Sort_m20003_gp_0_0_0_0_Array_Sort_m20003_gp_1_0_0_0,
	&GenInst_Array_Sort_m20004_gp_0_0_0_0_Array_Sort_m20004_gp_0_0_0_0,
	&GenInst_Array_Sort_m20005_gp_0_0_0_0_Array_Sort_m20005_gp_1_0_0_0,
	&GenInst_Array_Sort_m20006_gp_0_0_0_0,
	&GenInst_Array_Sort_m20006_gp_0_0_0_0_Array_Sort_m20006_gp_0_0_0_0,
	&GenInst_Array_Sort_m20007_gp_0_0_0_0,
	&GenInst_Array_Sort_m20007_gp_1_0_0_0,
	&GenInst_Array_Sort_m20007_gp_0_0_0_0_Array_Sort_m20007_gp_1_0_0_0,
	&GenInst_Array_Sort_m20008_gp_0_0_0_0,
	&GenInst_Array_Sort_m20009_gp_0_0_0_0,
	&GenInst_Array_qsort_m20010_gp_0_0_0_0,
	&GenInst_Array_qsort_m20010_gp_0_0_0_0_Array_qsort_m20010_gp_1_0_0_0,
	&GenInst_Array_compare_m20011_gp_0_0_0_0,
	&GenInst_Array_qsort_m20012_gp_0_0_0_0,
	&GenInst_Array_Resize_m20015_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m20017_gp_0_0_0_0,
	&GenInst_Array_ForEach_m20018_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m20019_gp_0_0_0_0_Array_ConvertAll_m20019_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m20020_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m20021_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m20022_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m20023_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m20024_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m20025_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m20026_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m20027_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m20028_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m20029_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m20030_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m20031_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m20032_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m20033_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m20034_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m20035_gp_0_0_0_0,
	&GenInst_Array_FindAll_m20036_gp_0_0_0_0,
	&GenInst_Array_Exists_m20037_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m20038_gp_0_0_0_0,
	&GenInst_Array_Find_m20039_gp_0_0_0_0,
	&GenInst_Array_FindLast_m20040_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2708_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2709_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2710_gp_0_0_0_0,
	&GenInst_IList_1_t2711_gp_0_0_0_0,
	&GenInst_ICollection_1_t2712_gp_0_0_0_0,
	&GenInst_Nullable_1_t1809_gp_0_0_0_0,
	&GenInst_Comparer_1_t2719_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2720_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2663_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3820_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m20189_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m20193_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_DictionaryEntry_t943_0_0_0,
	&GenInst_ShimEnumerator_t2722_gp_0_0_0_0_ShimEnumerator_t2722_gp_1_0_0_0,
	&GenInst_Enumerator_t2723_gp_0_0_0_0_Enumerator_t2723_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3833_0_0_0,
	&GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0,
	&GenInst_ValueCollection_t2724_gp_1_0_0_0,
	&GenInst_Enumerator_t2725_gp_0_0_0_0_Enumerator_t2725_gp_1_0_0_0,
	&GenInst_Enumerator_t2725_gp_1_0_0_0,
	&GenInst_ValueCollection_t2724_gp_0_0_0_0_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0,
	&GenInst_ValueCollection_t2724_gp_1_0_0_0_ValueCollection_t2724_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_0_0_0_0_Dictionary_2_t2721_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0,
	&GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0,
	&GenInst_Dictionary_2_t2721_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2727_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2728_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2662_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t3860_0_0_0,
	&GenInst_IDictionary_2_t2730_gp_0_0_0_0_IDictionary_2_t2730_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2732_gp_0_0_0_0_KeyValuePair_2_t2732_gp_1_0_0_0,
	&GenInst_List_1_t2733_gp_0_0_0_0,
	&GenInst_Enumerator_t2734_gp_0_0_0_0,
	&GenInst_Collection_1_t2735_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2736_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m20450_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20450_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m20451_gp_0_0_0_0,
};
