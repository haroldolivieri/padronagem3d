﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
#define IndexedSet_1__ctor_m3614(__this, method) (( void (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1__ctor_m15366_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15367(__this, method) (( Object_t * (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m15369(__this, ___item, method) (( void (*) (IndexedSet_1_t543 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m15370_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m15371(__this, ___item, method) (( bool (*) (IndexedSet_1_t543 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m15372_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m15373(__this, method) (( Object_t* (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m15374_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m15375(__this, method) (( void (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1_Clear_m15376_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m15377(__this, ___item, method) (( bool (*) (IndexedSet_1_t543 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m15378_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m15379(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t543 *, ICanvasElementU5BU5D_t2193*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m15380_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m15381(__this, method) (( int32_t (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1_get_Count_m15382_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m15383(__this, method) (( bool (*) (IndexedSet_1_t543 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m15384_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m15385(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t543 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m15386_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m15387(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t543 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m15388_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m15389(__this, ___index, method) (( void (*) (IndexedSet_1_t543 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m15390_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m15391(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t543 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m15392_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m15393(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t543 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m15394_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m3617(__this, ___match, method) (( void (*) (IndexedSet_1_t543 *, Predicate_1_t545 *, const MethodInfo*))IndexedSet_1_RemoveAll_m15395_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m3618(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t543 *, Comparison_1_t544 *, const MethodInfo*))IndexedSet_1_Sort_m15396_gshared)(__this, ___sortLayoutFunction, method)
