﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2039;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m13452_gshared (DefaultComparer_t2039 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13452(__this, method) (( void (*) (DefaultComparer_t2039 *, const MethodInfo*))DefaultComparer__ctor_m13452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m13453_gshared (DefaultComparer_t2039 * __this, UILineInfo_t286  ___x, UILineInfo_t286  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m13453(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2039 *, UILineInfo_t286 , UILineInfo_t286 , const MethodInfo*))DefaultComparer_Compare_m13453_gshared)(__this, ___x, ___y, method)
