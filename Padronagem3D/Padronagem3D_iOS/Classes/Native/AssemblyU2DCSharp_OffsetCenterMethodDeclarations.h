﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OffsetCenter
struct OffsetCenter_t64;
// UnityEngine.Camera
struct Camera_t32;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void OffsetCenter::.ctor()
extern "C" void OffsetCenter__ctor_m292 (OffsetCenter_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OffsetCenter::Start()
extern "C" void OffsetCenter_Start_m293 (OffsetCenter_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OffsetCenter::Update()
extern "C" void OffsetCenter_Update_m294 (OffsetCenter_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OffsetCenter::SetVanishingPoint(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" void OffsetCenter_SetVanishingPoint_m295 (OffsetCenter_t64 * __this, Camera_t32 * ___cam, Vector2_t15  ___perspectiveOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 OffsetCenter::PerspectiveOffCenter(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t43  OffsetCenter_PerspectiveOffCenter_m296 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, float ___near, float ___far, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OffsetCenter::setCorrectionFactor(System.Single)
extern "C" void OffsetCenter_setCorrectionFactor_m297 (OffsetCenter_t64 * __this, float ___fac, const MethodInfo* method) IL2CPP_METHOD_ATTR;
