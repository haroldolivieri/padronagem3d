﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17013(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2295 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m13551_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m17014(__this, method) (( Object_t * (*) (KeyValuePair_2_t2295 *, const MethodInfo*))KeyValuePair_2_get_Key_m13552_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17015(__this, ___value, method) (( void (*) (KeyValuePair_2_t2295 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13553_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m17016(__this, method) (( int32_t (*) (KeyValuePair_2_t2295 *, const MethodInfo*))KeyValuePair_2_get_Value_m13554_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17017(__this, ___value, method) (( void (*) (KeyValuePair_2_t2295 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m13555_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m17018(__this, method) (( String_t* (*) (KeyValuePair_2_t2295 *, const MethodInfo*))KeyValuePair_2_ToString_m13556_gshared)(__this, method)
