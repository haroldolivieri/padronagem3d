﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t1849;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m11050_gshared (GenericComparer_1_t1849 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m11050(__this, method) (( void (*) (GenericComparer_1_t1849 *, const MethodInfo*))GenericComparer_1__ctor_m11050_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m18651_gshared (GenericComparer_1_t1849 * __this, TimeSpan_t846  ___x, TimeSpan_t846  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m18651(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1849 *, TimeSpan_t846 , TimeSpan_t846 , const MethodInfo*))GenericComparer_1_Compare_m18651_gshared)(__this, ___x, ___y, method)
