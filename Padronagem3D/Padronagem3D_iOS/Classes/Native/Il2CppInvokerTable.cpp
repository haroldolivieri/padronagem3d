﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// System.Single[]
struct SingleU5BU5D_t44;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t175;
// System.String
struct String_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t48;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t681;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t773;
// System.Net.IPAddress
struct IPAddress_t806;
// System.Net.IPv6Address
struct IPv6Address_t808;
// System.UriFormatException
struct UriFormatException_t932;
// System.Byte[]
struct ByteU5BU5D_t75;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Exception
struct Exception_t108;
// System.MulticastDelegate
struct MulticastDelegate_t7;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1179;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1180;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1163;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1166;
// System.Reflection.MethodBase
struct MethodBase_t471;
// System.Reflection.Module
struct Module_t1332;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1496;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1758;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Text.StringBuilder
struct StringBuilder_t468;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1650;
// System.Char[]
struct CharU5BU5D_t469;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1641;
// System.Int64[]
struct Int64U5BU5D_t1786;
// System.String[]
struct StringU5BU5D_t88;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1874;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t299;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t418;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t420;
// UnityEngine.Color32[]
struct Color32U5BU5D_t422;
// System.Int32[]
struct Int32U5BU5D_t107;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t431;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t432;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t433;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1810;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1811;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_CardboardProfile_Distortion.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_ColorSpace.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "mscorlib_System_Char.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarVisibility.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_UInt32.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "mscorlib_System_UInt16.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "mscorlib_System_Byte.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "mscorlib_System_UInt64.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_0.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_12.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_21.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_24.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_36.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"

void* RuntimeInvoker_Void_t1129 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Matrix4x4_t43_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t18_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t43_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t18_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Matrix4x4U26_t2772_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t43 * p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t43 *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Ray_t71 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t71  (*Func)(void* obj, const MethodInfo* method);
	Ray_t71  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_SingleU5BU5DU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SingleU5BU5D_t44** p1, const MethodInfo* method);
	((Func)method->method)(obj, (SingleU5BU5D_t44**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Distortion_t37_Single_t113_Single_t113_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Distortion_t37  (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	Distortion_t37  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Distortion_t37_Distortion_t37_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Distortion_t37  (*Func)(void* obj, Distortion_t37  p1, float p2, int32_t p3, const MethodInfo* method);
	Distortion_t37  ret = ((Func)method->method)(obj, *((Distortion_t37 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Quaternion_t50  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Quaternion_t50 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Matrix4x4_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t43  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t50  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_SingleU26_t2774_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], (float*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Matrix4x4U26_t2772_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t43 * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Matrix4x4_t43 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Matrix4x4_t43_Single_t113_Single_t113_Single_t113_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float* p1, float* p2, float* p3, float* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (float*)args[0], (float*)args[1], (float*)args[2], (float*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32U26_t2775_Int32U26_t2775_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t* p2, float* p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float* p1, float* p2, float* p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (float*)args[0], (float*)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_GcAchievementDescriptionData_t346_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t346  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t346 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_GcUserProfileData_t345_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t345  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t345 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Double_t465_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UserProfileU5BU5DU26_t2776_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t175** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t175**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UserProfileU5BU5DU26_t2776_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t175** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t175**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_GcScoreData_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t348  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t348 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ColorSpace_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_BoneWeight_t183_BoneWeight_t183 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t183  p1, BoneWeight_t183  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t183 *)args[0]), *((BoneWeight_t183 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Rect_t18_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, Rect_t18  p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((Rect_t18 *)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_InternalDrawTextureArgumentsU26_t2777 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, InternalDrawTextureArguments_t186 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (InternalDrawTextureArguments_t186 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t11  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t11 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t11  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t11 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_ColorU26_t2778_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t11 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t11 *)args[2], *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ColorU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t11 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_RectU26_t2779 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t18 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t3 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t3 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Color_t11  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Color_t11 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_ColorU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Color_t11 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Color_t11 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Color_t11_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RenderTextureFormat_t354 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_IntPtr_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_CullingGroupEvent_t194 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t194  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t194 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_CullingGroupEvent_t194_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t194  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t194 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2781_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t200 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t200 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_LayerMask_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t30  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t30 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t30_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t30  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t15_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15_Vector2_t15_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector2_t15  p1, float p2, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector3_t3_Vector3_t3_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, float p3, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector3_t3_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector3_t3  p1, float p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Single_t113_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, float p1, Vector3_t3  p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t11_Color_t11_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, Color_t11  p1, Color_t11  p2, float p3, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), *((Color_t11 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t11_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, Color_t11  p1, float p2, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Color_t11  p1, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t11_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t187_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t187  (*Func)(void* obj, Color_t11  p1, const MethodInfo* method);
	Color32_t187  ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t11_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, Color32_t187  p1, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Quaternion_t50_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t50  p1, Quaternion_t50  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), *((Quaternion_t50 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Single_t113_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, float p1, Vector3_t3  p2, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Single_t113_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, float p1, Vector3_t3 * p2, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Vector3U26_t2780_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Vector3_t3 * p1, Vector3_t3 * p2, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, (Vector3_t3 *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Quaternion_t50  p1, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_QuaternionU26_t2782 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Quaternion_t50 * p1, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, (Quaternion_t50 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Quaternion_t50  p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_QuaternionU26_t2782 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Quaternion_t50 * p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, (Quaternion_t50 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Vector3_t3 * p1, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, (Vector3_t3 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t50_Quaternion_t50_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t50  (*Func)(void* obj, Quaternion_t50  p1, Quaternion_t50  p2, const MethodInfo* method);
	Quaternion_t50  ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), *((Quaternion_t50 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Quaternion_t50_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Quaternion_t50  p1, Vector3_t3  p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Quaternion_t50_Quaternion_t50 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t50  p1, Quaternion_t50  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t50 *)args[0]), *((Quaternion_t50 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t18  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t18 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Rect_t18_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t18  p1, Rect_t18  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t18 *)args[0]), *((Rect_t18 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t43_Matrix4x4_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Matrix4x4_t43  p1, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t43_Matrix4x4U26_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Matrix4x4_t43 * p1, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, (Matrix4x4_t43 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Matrix4x4_t43_Matrix4x4U26_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t43  p1, Matrix4x4_t43 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), (Matrix4x4_t43 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Matrix4x4U26_t2772_Matrix4x4U26_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t43 * p1, Matrix4x4_t43 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t43 *)args[0], (Matrix4x4_t43 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t90  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t90 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t43_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Quaternion_t50_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Quaternion_t50  p2, Vector3_t3  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Quaternion_t50 *)args[1]), *((Vector3_t3 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t43_Vector3_t3_Quaternion_t50_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Vector3_t3  p1, Quaternion_t50  p2, Vector3_t3  p3, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Quaternion_t50 *)args[1]), *((Vector3_t3 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t43_Vector3U26_t2780_QuaternionU26_t2782_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Vector3_t3 * p1, Quaternion_t50 * p2, Vector3_t3 * p3, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, (Vector3_t3 *)args[0], (Quaternion_t50 *)args[1], (Vector3_t3 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t43_Single_t113_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t43_Matrix4x4_t43_Matrix4x4_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t43  (*Func)(void* obj, Matrix4x4_t43  p1, Matrix4x4_t43  p2, const MethodInfo* method);
	Matrix4x4_t43  ret = ((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), *((Matrix4x4_t43 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_Matrix4x4_t43_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Matrix4x4_t43  p1, Vector4_t90  p2, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), *((Vector4_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Matrix4x4_t43_Matrix4x4_t43 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t43  p1, Matrix4x4_t43  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t43 *)args[0]), *((Matrix4x4_t43 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Bounds_t203 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t203  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t203 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Bounds_t203 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t203  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t203 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Bounds_t203_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t203  p1, Vector3_t3  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t203 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_BoundsU26_t2783_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t203 * p1, Vector3_t3 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t203 *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Bounds_t203_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t203  p1, Vector3_t3  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t203 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_BoundsU26_t2783_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t203 * p1, Vector3_t3 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t203 *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_RayU26_t2784_BoundsU26_t2783_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71 * p1, Bounds_t203 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t71 *)args[0], (Bounds_t203 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Ray_t71 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Ray_t71_SingleU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_BoundsU26_t2783_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Bounds_t203 * p1, Vector3_t3 * p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, (Bounds_t203 *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Bounds_t203_Bounds_t203 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t203  p1, Bounds_t203  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t203 *)args[0]), *((Bounds_t203 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector4_t90_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_Vector4_t90_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_Vector4_t90_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Vector4_t90  p1, float p2, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector4_t90_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Single_t113_Single_t113_SingleU26_t2774_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t11  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t11 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t11  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t11 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ColorU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t11 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Color_t11 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector4_t90  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t90 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_ColorU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t11 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t11 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t15 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Vector2_t15 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SphericalHarmonicsL2U26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t218 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t218 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Color_t11_SphericalHarmonicsL2U26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, SphericalHarmonicsL2_t218 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), (SphericalHarmonicsL2_t218 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ColorU26_t2778_SphericalHarmonicsL2U26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11 * p1, SphericalHarmonicsL2_t218 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t11 *)args[0], (SphericalHarmonicsL2_t218 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Color_t11  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Color_t11 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Color_t11_SphericalHarmonicsL2U26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Color_t11  p2, SphericalHarmonicsL2_t218 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Color_t11 *)args[1]), (SphericalHarmonicsL2_t218 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3U26_t2780_ColorU26_t2778_SphericalHarmonicsL2U26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3 * p1, Color_t11 * p2, SphericalHarmonicsL2_t218 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t3 *)args[0], (Color_t11 *)args[1], (SphericalHarmonicsL2_t218 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t218  (*Func)(void* obj, SphericalHarmonicsL2_t218  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t218  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t218 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t218_Single_t113_SphericalHarmonicsL2_t218 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t218  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t218  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t218  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t218 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t218  (*Func)(void* obj, SphericalHarmonicsL2_t218  p1, SphericalHarmonicsL2_t218  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t218  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t218 *)args[0]), *((SphericalHarmonicsL2_t218 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t218  p1, SphericalHarmonicsL2_t218  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t218 *)args[0]), *((SphericalHarmonicsL2_t218 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector4U26_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t90 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t90 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t90_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t15 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t15 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_RuntimePlatform_t161 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Matrix4x4U26_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t43 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t43 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_CameraClearFlags_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Object_t_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Object_t * p1, Vector3_t3 * p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t71_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t71  (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	Ray_t71  ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t71_Object_t_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t71  (*Func)(void* obj, Object_t * p1, Vector3_t3 * p2, const MethodInfo* method);
	Ray_t71  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t71_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t71  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t2784_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t71 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t71 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t2784_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t71 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t71 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Vector3_t3_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, Color_t11  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), *((Color_t11 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3U26_t2780_Vector3U26_t2780_ColorU26_t2778_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3 * p1, Vector3_t3 * p2, Color_t11 * p3, float p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t3 *)args[0], (Vector3_t3 *)args[1], (Color_t11 *)args[2], *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_RenderBuffer_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t350  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t350  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_RenderBufferU26_t2788_RenderBufferU26_t2788 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t350 * p2, RenderBuffer_t350 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t350 *)args[1], (RenderBuffer_t350 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchPhase_t231 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t3 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Touch_t233_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t233  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t233  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_QuaternionU26_t2782 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t50 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t50 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector3_t3_Vector3_t3_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, RaycastHit_t78 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), (RaycastHit_t78 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71  p1, RaycastHit_t78 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), (RaycastHit_t78 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71  p1, RaycastHit_t78 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), (RaycastHit_t78 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t71_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t71  p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3_t3_Vector3_t3_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t2780_Vector3U26_t2780_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t3 * p1, Vector3_t3 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t3 *)args[0], (Vector3_t3 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Vector3U26_t2780_Vector3U26_t2780_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t3 * p1, Vector3_t3 * p2, RaycastHit_t78 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t3 *)args[0], (Vector3_t3 *)args[1], (RaycastHit_t78 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Ray_t71_RaycastHitU26_t2789_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t71  p2, RaycastHit_t78 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Ray_t71 *)args[1]), (RaycastHit_t78 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_RayU26_t2784_RaycastHitU26_t2789_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t71 * p2, RaycastHit_t78 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t71 *)args[1], (RaycastHit_t78 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t71  p1, RaycastHit_t78 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t71 *)args[0]), (RaycastHit_t78 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Vector2_t15_Vector3_t3_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, Object_t * p2, Vector2_t15  p3, Vector3_t3  p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], (Object_t *)args[1], *((Vector2_t15 *)args[2]), *((Vector3_t3 *)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Vector2U26_t2785_Vector3U26_t2780_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, Object_t * p2, Vector2_t15 * p3, Vector3_t3 * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], (Object_t *)args[1], (Vector2_t15 *)args[2], (Vector3_t3 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_CollisionFlags_t149_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CollisionFlags_t149_Object_t_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t3 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t3 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2_t15_Vector2_t15_Single_t113_Int32_t106_Single_t113_Single_t113_RaycastHit2DU26_t2790 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t252 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t252 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785_Vector2U26_t2785_Single_t113_Int32_t106_Single_t113_Single_t113_RaycastHit2DU26_t2790 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, Vector2_t15 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t252 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], (Vector2_t15 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t252 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t252_Vector2_t15_Vector2_t15_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t252  (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t252  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t252_Vector2_t15_Vector2_t15_Single_t113_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t252  (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t252  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t15_Vector2_t15_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2U26_t2785_Vector2U26_t2785_Single_t113_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t15 * p1, Vector2_t15 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t15 *)args[0], (Vector2_t15 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_SByte_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_SendMessageOptions_t159 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorStateInfo_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t267  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t267  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorClipInfo_t268 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t268  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t268  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t281 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t281 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t281 * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t281 *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t281 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t281 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Color_t11_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Vector2_t15_Vector2_t15_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t11  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t15  p16, Vector2_t15  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t11 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t15 *)args[15]), *((Vector2_t15 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Color_t11_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Single_t113_Single_t113_Single_t113_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t11  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t11 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_ColorU26_t2778_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Single_t113_Single_t113_Single_t113_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t11 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t11 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t288_TextGenerationSettings_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t288  (*Func)(void* obj, TextGenerationSettings_t288  p1, const MethodInfo* method);
	TextGenerationSettings_t288  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t288 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_TextGenerationSettings_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t288  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t288 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_TextGenerationSettings_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t288  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t288 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RenderMode_t292 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t15  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_ColorU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t11 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t11 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_RectU26_t2779 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t18 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t18 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Vector2U26_t2785_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t15 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t15 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15_Vector2_t15_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector2_t15  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2_t15_Object_t_Object_t_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15  p1, Object_t * p2, Object_t * p3, Vector2_t15 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t15 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t15 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Object_t_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, Object_t * p2, Object_t * p3, Vector2_t15 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t15 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t18_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t_Vector3U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, Object_t * p3, Vector3_t3 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), (Object_t *)args[2], (Vector3_t3 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t_Vector2U26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, Object_t * p3, Vector2_t15 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), (Object_t *)args[2], (Vector2_t15 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t71_Object_t_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t71  (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, const MethodInfo* method);
	Ray_t71  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Ray_t71 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t71  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t71 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_EventType_t301 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventType_t301_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventModifiers_t302 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_KeyCode_t151 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_SByte_t1124_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, int32_t p3, int8_t p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Rect_t18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t18  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Int32_t106_Single_t113_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Rect_t18_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_RectU26_t2779_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t18 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Rect_t18_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t18  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t18 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_RectU26_t2779_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t18 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t18 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t18  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t18 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_RectU26_t2779 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t18 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t18 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Double_t465_SByte_t1124_SByte_t1124_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t308  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t308 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_DateTime_t308_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t308  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t308 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t373 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t366 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t366  (*Func)(void* obj, const MethodInfo* method);
	Range_t366  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Range_t366 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t366  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t366 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t374 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t368  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t368 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_HitInfo_t368_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t368  p1, HitInfo_t368  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t368 *)args[0]), *((HitInfo_t368 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t368  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t368 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_StringU26_t2792_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t441  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t441 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Color_t11_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t11  p1, Color_t11  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), *((Color_t11 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_TextGenerationSettings_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t288  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t288 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PersistentListenerMode_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_RaycastResult_t94_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t94  p1, RaycastResult_t94  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), *((RaycastResult_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t509 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t94  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t94  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t94  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_InputButton_t514 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t94_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t94  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t94  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t509_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t509_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t113_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_PointerEventDataU26_t2793_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t48 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t48 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Touch_t233_BooleanU26_t2794_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t233  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t233 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_FramePressState_t515_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector2_t15_Vector2_t15_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_InputMode_t523 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t30  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t30  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_LayerMask_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t30  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t30 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_RaycastHit_t78_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t78  p1, RaycastHit_t78  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t78 *)args[0]), *((RaycastHit_t78 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorTweenMode_t529 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorBlock_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t546  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t546  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_FontStyle_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextAnchor_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HorizontalWrapMode_t279 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VerticalWrapMode_t280 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Color_t11_Single_t113_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Color_t11_Single_t113_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t11_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t11  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t11  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Single_t113_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_BlockingObjects_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t15  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t15 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Type_t572 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FillMethod_t573 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t90_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Color32_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Color32_t187  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color32_t187 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Vector2_t15_Vector2_t15_Color32_t187_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, Vector2_t15  p3, Color32_t187  p4, Vector2_t15  p5, Vector2_t15  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), *((Vector2_t15 *)args[2]), *((Color32_t187 *)args[3]), *((Vector2_t15 *)args[4]), *((Vector2_t15 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t90_Vector4_t90_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t90  (*Func)(void* obj, Vector4_t90  p1, Rect_t18  p2, const MethodInfo* method);
	Vector4_t90  ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Rect_t18 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Single_t113_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Single_t113_Single_t113_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t15_Vector2_t15_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, Vector2_t15  p1, Rect_t18  p2, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Rect_t18 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t575 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LineType_t578 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputType_t576 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchScreenKeyboardType_t201 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterValidation_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t15  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditState_t582_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Object_t_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int16_t1125_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Char_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Rect_t18_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t18  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t18 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Mode_t593 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Navigation_t594 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t594  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t594  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t599 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Axis_t601 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MovementType_t604 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScrollbarVisibility_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Bounds_t203 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t203  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t203  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Navigation_t594 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t594  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t594 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Transition_t609 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ColorBlock_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t546  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t546 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SpriteState_t611 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t611  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t611  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SpriteState_t611 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t611  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t611 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SelectionState_t610 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Object_t_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Color_t11_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t11  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t11 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ColorU26_t2778_Color_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t11 * p1, Color_t11  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t11 *)args[0], *((Color_t11 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t615 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t617 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return ret;
}

void* RuntimeInvoker_TextGenerationSettings_t288_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t288  (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	TextGenerationSettings_t288  ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t15_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t15  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t15  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t18_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t18_Rect_t18_Rect_t18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t18  (*Func)(void* obj, Rect_t18  p1, Rect_t18  p2, const MethodInfo* method);
	Rect_t18  ret = ((Func)method->method)(obj, *((Rect_t18 *)args[0]), *((Rect_t18 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AspectMode_t631 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Single_t113_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScaleMode_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenMatchMode_t634 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Unit_t635 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FitMode_t637 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Corner_t639 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t640 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Constraint_t641 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int32_t106_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_LayoutRebuilder_t648 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t648  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t648 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_Object_t_Single_t113_ILayoutElementU26_t2795 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UIVertexU26_t2796_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t296 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertex_t296 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Color32_t187_Vector2_t15_Vector2_t15_Vector3_t3_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Color32_t187  p2, Vector2_t15  p3, Vector2_t15  p4, Vector3_t3  p5, Vector4_t90  p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Color32_t187 *)args[1]), *((Vector2_t15 *)args[2]), *((Vector2_t15 *)args[3]), *((Vector3_t3 *)args[4]), *((Vector4_t90 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3_t3_Color32_t187_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t3  p1, Color32_t187  p2, Vector2_t15  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Color32_t187 *)args[1]), *((Vector2_t15 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t296  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Color32_t187_Int32_t106_Int32_t106_Single_t113_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t187  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t187 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t736_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t467_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t773 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t773 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditorBrowsableState_t785 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_IPAddressU26_t2798 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t806 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t806 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t790 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_IPv6AddressU26_t2799 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t808 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t808 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_SecurityProtocolType_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t860_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t847_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t847_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t847_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t847 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t854 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t855 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_X509KeyUsageFlags_t852 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t852_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t454_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Int16_t1125_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_RegexOptions_t878 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Category_t885_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UInt16_t960_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UInt16_t960_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Object_t_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UInt16_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t960_UInt16_t960_UInt16_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t880_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UInt16_t960_UInt16_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775_Int32U26_t2775_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UInt16_t960_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int32_t106_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t900 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t900  (*Func)(void* obj, const MethodInfo* method);
	Interval_t900  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Interval_t900 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t900  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t900 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Interval_t900 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t900  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t900 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t900_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t900  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t900  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t465_Interval_t900 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t900  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t900 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t900_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t900  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t900 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t465_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t2800 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_RegexOptionsU26_t2800_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Int32U26_t2775_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t885 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int16_t1125_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UInt16_t960_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_UInt16_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t934_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t699_Object_t_Int32U26_t2775_CharU26_t2801 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_UriFormatExceptionU26_t2802 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t932 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t932 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_UInt32_t467_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t985_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_ConfidenceFactor_t989 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_ByteU26_t2803_Int32U26_t2775_ByteU5BU5DU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t75** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t75**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t308_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t967 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t967  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t967 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t965_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t965  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t965  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_RSAParameters_t965 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t965  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t965 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DSAParameters_t967_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t967  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t967  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Byte_t454_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t1026 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t1027 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Int16_t1125_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t1029 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t1047 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t760 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_ByteU5BU5DU26_t2804_ByteU5BU5DU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t75** p2, ByteU5BU5D_t75** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t75**)args[1], (ByteU5BU5D_t75**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1125_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Int16_t1125_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t1061 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t1060 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t1075 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t1061_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int64_t466_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Byte_t454_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t965 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t965  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t965  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Byte_t454_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t1040 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_ObjectU5BU5DU26_t2805 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t115** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t115**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_ObjectU5BU5DU26_t2805 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t115** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t115**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SByte_t1124_Object_t_Int32_t106_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int32U26_t2775_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t108 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t108 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_SByte_t1124_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t108 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t108 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Object_t_Object_t_BooleanU26_t2794_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Object_t_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_Int32U26_t2775_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t108 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t108 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t108 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t108 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeCode_t736 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int64U26_t2807_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int64U26_t2807_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t108 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t108 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int64U26_t2807 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int64U26_t2807 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_UInt32U26_t2808_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_UInt32U26_t2808_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t108 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t108 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_UInt32U26_t2808 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_UInt32U26_t2808 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_UInt64U26_t2809_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t108 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t108 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_UInt64U26_t2809 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_ByteU26_t2803 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_ByteU26_t2803 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_SByteU26_t2810_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByteU26_t2810 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int16U26_t2811_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t108 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t108 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int16U26_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_UInt16U26_t2812 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_UInt16U26_t2812 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ByteU2AU26_t2813_ByteU2AU26_t2813_DoubleU2AU26_t2814_UInt16U2AU26_t2815_UInt16U2AU26_t2815_UInt16U2AU26_t2815_UInt16U2AU26_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1275_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Int16_t1125_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t699_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1125_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32U26_t2775_Int32U26_t2775_BooleanU26_t2794_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_DoubleU26_t2816_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t108 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t108 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_DoubleU26_t2816 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t738_Decimal_t738_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, Decimal_t738  p1, Decimal_t738  p2, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), *((Decimal_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Decimal_t738_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t738  p1, Decimal_t738  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), *((Decimal_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Decimal_t738_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738  p1, Decimal_t738  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), *((Decimal_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Int32U26_t2775_BooleanU26_t2794_BooleanU26_t2794_Int32U26_t2775_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t738_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_DecimalU26_t2817_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t738 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t738 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_UInt64U26_t2809 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_Int64U26_t2807 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_DecimalU26_t2817 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, Decimal_t738 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], (Decimal_t738 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_DecimalU26_t2817 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t738 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DecimalU26_t2817_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t738 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t738 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_DecimalU26_t2817_DecimalU26_t2817_DecimalU26_t2817 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t738 * p1, Decimal_t738 * p2, Decimal_t738 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t738 *)args[0], (Decimal_t738 *)args[1], (Decimal_t738 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t467 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t7 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t7 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t466_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t1399 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t1379 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1130 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1130  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1130  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t736_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1130 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1130  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1130 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1130_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1130  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1130  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_RuntimeFieldHandle_t1132 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1132  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1132 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_ContractionU5BU5DU26_t2819_Level2MapU5BU5DU26_t2820 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1179** p3, Level2MapU5BU5D_t1180** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1179**)args[2], (Level2MapU5BU5D_t1180**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_CodePointIndexerU26_t2821_ByteU2AU26_t2813_ByteU2AU26_t2813_CodePointIndexerU26_t2821_ByteU2AU26_t2813 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1163 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1163 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1163 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1163 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t454_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1176_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1173 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1173 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1173 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1173 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int16_t1125_Int32_t106_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1173 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1173 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1173 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1173 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1173 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1173 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Object_t_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1173 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1173 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32_t106_ContractionU26_t2823_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1166 ** p8, Context_t1173 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1166 **)args[7], (Context_t1173 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106_Object_t_SByte_t1124_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1173 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1173 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32_t106_ContractionU26_t2823_ContextU26_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1166 ** p9, Context_t1173 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1166 **)args[8], (Context_t1173 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_ByteU5BU5DU26_t2804_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t75** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t75**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1187_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t967_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t967  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t967  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DSAParameters_t967 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t967  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t967 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t1125_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1125_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Single_t113_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124_MethodBaseU26_t2824_Int32U26_t2775_Int32U26_t2775_StringU26_t2792_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t471 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t471 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1700_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32U26_t2775_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1700_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DateTime_t308_DateTime_t308_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t308  p1, DateTime_t308  p2, TimeSpan_t846  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((DateTime_t308 *)args[1]), *((TimeSpan_t846 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t738  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t738  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_SByte_t1124_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_IntPtr_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Object_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1285_Object_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t1294_IntPtr_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_MonoIOStatU26_t2826_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1293 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1293 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_IntPtr_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_IntPtr_t_Object_t_Int32_t106_Int32_t106_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_IntPtr_t_Int64_t466_Int32_t106_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_IntPtr_t_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_IntPtr_t_Int64_t466_MonoIOErrorU26_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1369 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1743  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1743  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1380 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t1335 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1335  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1335  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1377 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1132 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1132  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1132  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_OpCode_t1339 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1339  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1339 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_OpCode_t1339_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1339  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1339 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1343 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2775_ModuleU26_t2827 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1332 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1332 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t1363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_ObjectU5BU5DU26_t2805_Object_t_Object_t_Object_t_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t115** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t115**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t115** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t115**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_ObjectU5BU5DU26_t2805_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t115** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t115**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1132 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1132  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1132 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t441  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t441 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1743  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1743 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_MonoEventInfoU26_t2829 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1384 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1384 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t1384_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1384  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1384  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_MonoMethodInfoU26_t2830 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1387 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1387 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1387_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1387  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1387  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1380_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t1369_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t108 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t108 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_MonoPropertyInfoU26_t2831_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1388 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1388 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t1395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int64_t466_ResourceInfoU26_t2832 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t1403 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t1403 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t1432_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1432  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1432  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Object_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t454_IntPtr_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t846  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t441_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t441  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t441 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t441_ISurrogateSelectorU26_t2833 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t441  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t441 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t846_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_Object_t_StringU26_t2792_StringU26_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1538 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t441  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t441  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1554 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t_SByte_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t454_Object_t_SByte_t1124_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_ObjectU26_t2828_HeaderU5BU5DU26_t2834 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1758** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1758**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Byte_t454_Object_t_SByte_t1124_ObjectU26_t2828_HeaderU5BU5DU26_t2834 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1758** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1758**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Byte_t454_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Byte_t454_Object_t_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t440 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t440 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t440 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t440 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t440 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t440 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466_ObjectU26_t2828_SerializationInfoU26_t2835 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t440 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t440 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Object_t_Int64_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64U26_t2807_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64U26_t2807_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Int64_t466_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t466_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Int32_t106_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_Int64_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_SByte_t1124_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t441  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t441 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t441  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t441 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_StreamingContext_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t441  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t441 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t441_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t441  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t441 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t308  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t308 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1571 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1571  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1571  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1574 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1578 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t467_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UInt32U26_t2808_Int32_t106_UInt32U26_t2808_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t1123_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Int64_t466_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t764 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_StringBuilderU26_t2836_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t468 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t468 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_EncoderFallbackBufferU26_t2837_CharU5BU5DU26_t2838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1650 ** p6, CharU5BU5D_t469** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1650 **)args[5], (CharU5BU5D_t469**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_DecoderFallbackBufferU26_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1641 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1641 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int16_t1125_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t106_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124_Int32U26_t2775_BooleanU26_t2794_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_CharU26_t2801_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1641 ** p7, ByteU5BU5D_t75** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1641 **)args[6], (ByteU5BU5D_t75**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1641 ** p6, ByteU5BU5D_t75** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1641 **)args[5], (ByteU5BU5D_t75**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_Object_t_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1641 ** p2, ByteU5BU5D_t75** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1641 **)args[1], (ByteU5BU5D_t75**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_Object_t_Int64_t466_Int32_t106_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1641 ** p2, ByteU5BU5D_t75** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1641 **)args[1], (ByteU5BU5D_t75**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_UInt32U26_t2808_UInt32U26_t2808_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1641 ** p9, ByteU5BU5D_t75** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1641 **)args[8], (ByteU5BU5D_t75**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_UInt32U26_t2808_UInt32U26_t2808_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1641 ** p8, ByteU5BU5D_t75** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1641 **)args[7], (ByteU5BU5D_t75**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t1124_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t1124_SByte_t1124_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_TimeSpan_t846_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t846  p1, TimeSpan_t846  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int64_t466_Int64_t466_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_IntPtr_t_Int32_t106_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t466_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t454_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t454_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t699_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t465_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1125_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1124_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t113_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t960_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t467_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1123_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SByte_t1124_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t846  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t1700 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, TimeSpan_t846  p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DateTime_t308_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t308  p1, DateTime_t308  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((DateTime_t308 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_DateTime_t308_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, DateTime_t308  p1, int32_t p2, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_DateTimeU26_t2840_DateTimeOffsetU26_t2841_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t308 * p4, DateTimeOffset_t1698 * p5, int8_t p6, Exception_t108 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t308 *)args[3], (DateTimeOffset_t1698 *)args[4], *((int8_t*)args[5]), (Exception_t108 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t108 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t108 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Object_t_SByte_t1124_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_SByte_t1124_DateTimeU26_t2840_DateTimeOffsetU26_t2841_Object_t_Int32_t106_SByte_t1124_BooleanU26_t2794_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t308 * p5, DateTimeOffset_t1698 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t308 *)args[4], (DateTimeOffset_t1698 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_Object_t_Object_t_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Int32_t106_DateTimeU26_t2840_SByte_t1124_BooleanU26_t2794_SByte_t1124_ExceptionU26_t2806 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t308 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t108 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t308 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t108 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_DateTime_t308_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, DateTime_t308  p1, TimeSpan_t846  p2, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DateTime_t308_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t308  p1, DateTime_t308  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((DateTime_t308 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DateTime_t308_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t308  p1, TimeSpan_t846  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int64_t466_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t846  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_DateTimeOffset_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1698  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1698 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DateTimeOffset_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1698  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1698 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1125_Object_t_BooleanU26_t2794_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1125_Object_t_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t308_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t308  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t308_Nullable_1_t1802_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t308  p1, Nullable_1_t1802  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((Nullable_1_t1802 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_MonoEnumInfo_t1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1711  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1711 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_MonoEnumInfoU26_t2842 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1711 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1711 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Int16_t1125_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int64_t466_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int16_t1125_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Guid_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1720  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1720 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Guid_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1720  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1720 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t1720  (*Func)(void* obj, const MethodInfo* method);
	Guid_t1720  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1124_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t465_Double_t465_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t1399_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_UInt64U2AU26_t2843_Int32U2AU26_t2844_CharU2AU26_t2845_CharU2AU26_t2845_Int64U2AU26_t2846_Int32U2AU26_t2844 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Double_t465_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Object_t_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t738  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t738 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1125_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t466_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t465_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t738_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t738  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t738 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t113_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t465_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Object_t_BooleanU26_t2794_SByte_t1124_Int32U26_t2775_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t466_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, TimeSpan_t846  p1, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_TimeSpan_t846_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t846  p1, TimeSpan_t846  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t846  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t846  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_Double_t465_Int64_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_TimeSpan_t846_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, TimeSpan_t846  p1, TimeSpan_t846  p2, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((TimeSpan_t846 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DateTime_t308_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t308  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t308_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t308  (*Func)(void* obj, DateTime_t308  p1, const MethodInfo* method);
	DateTime_t308  ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t846_DateTime_t308_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, DateTime_t308  p1, TimeSpan_t846  p2, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((DateTime_t308 *)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int64U5BU5DU26_t2847_StringU5BU5DU26_t2848 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1786** p2, StringU5BU5D_t88** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1786**)args[1], (StringU5BU5D_t88**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t455_ObjectU26_t2828_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ObjectU26_t2828_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2337 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2337  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2337  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1896 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1896  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1896  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t115** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t115**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t115** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t115**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2086  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2086 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2086 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2086  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2086 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2086_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2086  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2086  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2090 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2090  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2090  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2086 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2086  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2086  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2089 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2089  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2089  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1855 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1855  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1855  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Object_t_Object_t_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t2069 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2069  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2069  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2070 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2070  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2070  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2066  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2066  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_FloatTween_t535 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FloatTween_t535  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FloatTween_t535 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ColorTween_t532 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t532  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t532 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_TypeU26_t2849_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_BooleanU26_t2794_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_FillMethodU26_t2850_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SingleU26_t2774_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_ContentTypeU26_t2851_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_LineTypeU26_t2852_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_InputTypeU26_t2853_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_TouchScreenKeyboardTypeU26_t2854_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_CharacterValidationU26_t2855_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_CharU26_t2801_Int16_t1125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DirectionU26_t2856_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_NavigationU26_t2857_Navigation_t594 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t594 * p1, Navigation_t594  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t594 *)args[0], *((Navigation_t594 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_TransitionU26_t2858_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_ColorBlockU26_t2859_ColorBlock_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t546 * p1, ColorBlock_t546  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t546 *)args[0], *((ColorBlock_t546 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SpriteStateU26_t2860_SpriteState_t611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t611 * p1, SpriteState_t611  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t611 *)args[0], *((SpriteState_t611 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DirectionU26_t2861_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_AspectModeU26_t2862_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_FitModeU26_t2863_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_CornerU26_t2864_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_AxisU26_t2865_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector2U26_t2785_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t15 * p1, Vector2_t15  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t15 *)args[0], *((Vector2_t15 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_ConstraintU26_t2866_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_SingleU26_t2774_Single_t113 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_BooleanU26_t2794_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_TextAnchorU26_t2867_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Double_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastResult_t94_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t94  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t94  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t94  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t94  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t94  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t94 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_RaycastResultU5BU5DU26_t2868_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1874** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1874**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_RaycastResultU5BU5DU26_t2868_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1874** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1874**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_RaycastResult_t94_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t94  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t94 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_RaycastResult_t94_RaycastResult_t94_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t94  p1, RaycastResult_t94  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), *((RaycastResult_t94 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t347_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t347  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t347  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_GcAchievementData_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t347  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t347 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_GcAchievementData_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t347  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_GcAchievementData_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t347  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_GcAchievementData_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t347  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t347 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t348_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t348  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t348  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_GcScoreData_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t348  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t348 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_GcScoreData_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t348  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t348 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_GcScoreData_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t348  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t348 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t3  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t3  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t3 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t90  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t15  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t15  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t15 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t187_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t187  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color32_t187  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32_t187  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color32_t187 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t187  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t187  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color32_t187  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color32_t187 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3U5BU5DU26_t2869_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t299** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t299**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector3U5BU5DU26_t2869_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t299** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t299**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Vector3_t3_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t3  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t3 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Vector3_t3_Vector3_t3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector4U5BU5DU26_t2870_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t418** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t418**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector4U5BU5DU26_t2870_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t418** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t418**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Vector4_t90_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector4_t90  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t90 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Vector4_t90_Vector4_t90_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Vector2U5BU5DU26_t2871_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t420** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t420**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Vector2U5BU5DU26_t2871_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t420** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t420**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Vector2_t15_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t15  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t15 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Vector2_t15_Vector2_t15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Color32U5BU5DU26_t2872_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t422** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t422**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Color32U5BU5DU26_t2872_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t422** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t422**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Color32_t187_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Color32_t187  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t187 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Color32_t187_Color32_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t187  p1, Color32_t187  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), *((Color32_t187 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32U5BU5DU26_t2873_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t107** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t107**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32U5BU5DU26_t2873_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t107** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t107**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint_t247_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t247  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t247  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ContactPoint_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t247  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t247 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ContactPoint_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t247  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t247 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ContactPoint_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t247  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t247 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ContactPoint_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t247  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t247 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit_t78_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t78  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t78  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t78  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t78 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t78  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t78 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t78  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t78 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t78  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t78 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t252_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t252  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t252  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_RaycastHit2D_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t252  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t252 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_RaycastHit2D_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t252  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t252 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_RaycastHit2D_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t252  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t252 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_RaycastHit2D_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t252  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t252 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint2D_t255_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t255  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint2D_t255  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ContactPoint2D_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint2D_t255  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint2D_t255 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ContactPoint2D_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint2D_t255  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint2D_t255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ContactPoint2D_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint2D_t255  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint2D_t255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ContactPoint2D_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint2D_t255  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint2D_t255 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t269_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t269  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t269  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Keyframe_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t269  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t269 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Keyframe_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t269  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t269 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Keyframe_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t269  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t269 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Keyframe_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t269  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t269 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CharacterInfo_t281_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t281  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CharacterInfo_t281  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_CharacterInfo_t281 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharacterInfo_t281  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CharacterInfo_t281 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_CharacterInfo_t281 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CharacterInfo_t281  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CharacterInfo_t281 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_CharacterInfo_t281 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CharacterInfo_t281  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CharacterInfo_t281 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_CharacterInfo_t281 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CharacterInfo_t281  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CharacterInfo_t281 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t296_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t296  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t296  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t296  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t296  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t296  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t296 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UIVertexU5BU5DU26_t2874_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t431** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t431**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UIVertexU5BU5DU26_t2874_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t431** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t431**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_UIVertex_t296_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t296  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t296 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UIVertex_t296_UIVertex_t296_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t296  p1, UIVertex_t296  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), *((UIVertex_t296 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t285_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t285  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t285  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t285  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t285  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t285  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t285  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t285 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UICharInfoU5BU5DU26_t2875_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t432** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t432**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UICharInfoU5BU5DU26_t2875_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t432** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t432**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_UICharInfo_t285_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t285  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t285 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UICharInfo_t285_UICharInfo_t285_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t285  p1, UICharInfo_t285  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), *((UICharInfo_t285 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t286_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t286  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t286  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t286  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t286  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t286  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t286  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t286 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UILineInfoU5BU5DU26_t2876_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t433** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t433**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_UILineInfoU5BU5DU26_t2876_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t433** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t433**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_UILineInfo_t286_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t286  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t286 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UILineInfo_t286_UILineInfo_t286_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t286  p1, UILineInfo_t286  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), *((UILineInfo_t286 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2047_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2047  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2047  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2047 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2047  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2047 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2047 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2047  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2047 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2047 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2047  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2047 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2047 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2047  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2047 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1229_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1229  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1229  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Link_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1229  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1229 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Link_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1229  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1229 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Link_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1229  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1229 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Link_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1229  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1229 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t943_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_DictionaryEntry_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t943  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t943 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_DictionaryEntry_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t943  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t943 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DictionaryEntry_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t943  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t943 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_DictionaryEntry_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t943  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t943 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2066_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2066  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2066  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2066  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2066 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2066  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2066 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2066  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2066 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2066  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2066 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2086_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2086  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2086  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2086 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2086  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2086 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2086  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2086 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterModifier_t1392_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1392  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1392  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ParameterModifier_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1392  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1392 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ParameterModifier_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1392  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1392 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ParameterModifier_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1392  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1392 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ParameterModifier_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1392  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1392 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t368_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t368  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t368  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t368  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t368 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t368  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t368 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2114_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2114  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2114  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2114  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2114 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2114 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2114  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2114 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2114  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2114  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2114 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t387_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t575_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2342_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2342  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2342  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2342 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2342  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2342 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2342 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2342  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2342 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2342 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2342  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2342 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2342 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2342  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2342 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t843_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t843  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t843  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_X509ChainStatus_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t843  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t843 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_X509ChainStatus_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t843  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t843 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_X509ChainStatus_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t843  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t843 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_X509ChainStatus_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t843  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t843 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2362_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2362  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2362  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_KeyValuePair_2_t2362 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2362  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2362 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2362 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2362  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2362 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_KeyValuePair_2_t2362 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2362  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2362 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2362 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2362  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2362 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t893_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t893  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t893  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Mark_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t893  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t893 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Mark_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t893  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t893 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Mark_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t893  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t893 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Mark_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t893  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t893 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t930_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t930  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t930  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_UriScheme_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t930  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t930 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_UriScheme_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t930  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t930 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_UriScheme_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t930  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t930 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_UriScheme_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t930  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t930 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ClientCertificateType_t1074_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1162_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1162  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1162  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_TableRange_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1162  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1162 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_TableRange_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1162  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1162 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_TableRange_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1162  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1162 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_TableRange_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1162  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1162 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1239_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1239  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1239  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Slot_t1239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1239  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1239 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Slot_t1239 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1239  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Slot_t1239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1239  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Slot_t1239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1239  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1239 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1247_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1247  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1247  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Slot_t1247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1247  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1247 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_Slot_t1247 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1247  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1247 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Slot_t1247 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1247  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1247 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Slot_t1247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1247  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1247 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t1326_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1326  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1326  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ILTokenInfo_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1326  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1326 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ILTokenInfo_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1326  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ILTokenInfo_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1326  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ILTokenInfo_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1326  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1326 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t1328_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1328  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1328  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_LabelData_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1328  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1328 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_LabelData_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1328  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1328 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_LabelData_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1328  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1328 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_LabelData_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1328  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1328 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t1327_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1327  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1327  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_LabelFixup_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1327  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1327 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_LabelFixup_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1327  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1327 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_LabelFixup_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1327  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1327 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_LabelFixup_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1327  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1327 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1374_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1374  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1374  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t1374  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t1374 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1373_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1373  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1373  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t1373  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t1373 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_CustomAttributeTypedArgumentU5BU5DU26_t2877_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1810** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1810**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_CustomAttributeTypedArgumentU5BU5DU26_t2877_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1810** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1810**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_CustomAttributeTypedArgument_t1374_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1374  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1374 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, CustomAttributeTypedArgument_t1374  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), *((CustomAttributeTypedArgument_t1374 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1374  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1374 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_CustomAttributeNamedArgumentU5BU5DU26_t2878_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1811** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1811**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_CustomAttributeNamedArgumentU5BU5DU26_t2878_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1811** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1811**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t106_Object_t_CustomAttributeNamedArgument_t1373_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1373  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1373 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, CustomAttributeNamedArgument_t1373  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), *((CustomAttributeNamedArgument_t1373 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Object_t_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1373  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1373 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1403_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1403  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t1403  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ResourceInfo_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t1403  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t1403 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ResourceInfo_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t1403  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t1403 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ResourceInfo_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t1403  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t1403 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ResourceInfo_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t1403  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t1403 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t1404_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1404  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t1404  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_ResourceCacheItem_t1404 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t1404  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t1404 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t455_ResourceCacheItem_t1404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t1404  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_ResourceCacheItem_t1404 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t1404  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_ResourceCacheItem_t1404 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t1404  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t1404 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_DateTime_t308 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t308  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t308 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t738  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t738 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Decimal_t738 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t738  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t738 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t846_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t846  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t846  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_TimeSpan_t846 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t846  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t846 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1542_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1129_Int32_t106_Byte_t454 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t1876 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1876  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1876  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_RaycastResult_t94_RaycastResult_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t94  p1, RaycastResult_t94  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), *((RaycastResult_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t94  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RaycastResult_t94_RaycastResult_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t94  p1, RaycastResult_t94  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t94 *)args[0]), *((RaycastResult_t94 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1921 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1921  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1921  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t347  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t347  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t348  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t348  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t187  (*Func)(void* obj, const MethodInfo* method);
	Color32_t187  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t3  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t3  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1943 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1943  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1943  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t3  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Vector3_t3_Vector3_t3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t3_Vector3_t3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t3  p1, Vector3_t3  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t3 *)args[0]), *((Vector3_t3 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1953 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1953  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1953  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t90_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t90  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Vector4_t90_Vector4_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t90_Vector4_t90_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t90  p1, Vector4_t90  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t90 *)args[0]), *((Vector4_t90 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1963 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1963  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1963  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t15_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t15  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Vector2_t15_Vector2_t15 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t15_Vector2_t15_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t15  p1, Vector2_t15  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t15 *)args[0]), *((Vector2_t15 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Color32_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t187  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color32_t187  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1973 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1973  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1973  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Color32_t187_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t187  p1, Color32_t187  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), *((Color32_t187 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t187_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t187  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_Color32_t187_Color32_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t187  p1, Color32_t187  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), *((Color32_t187 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t187_Color32_t187_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t187  p1, Color32_t187  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t187 *)args[0]), *((Color32_t187 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1983 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1983  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1983  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t247  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t247  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit_t78 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t78  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t78  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t252  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t252  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint2D_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t255  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint2D_t255  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t269  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t269  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterInfo_t281 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t281  (*Func)(void* obj, const MethodInfo* method);
	CharacterInfo_t281  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t296  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t296  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t296_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t296  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t296  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2012 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2012  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2012  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UIVertex_t296_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t296  p1, UIVertex_t296  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), *((UIVertex_t296 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t296_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t296  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_UIVertex_t296_UIVertex_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t296  p1, UIVertex_t296  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), *((UIVertex_t296 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t296_UIVertex_t296_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t296  p1, UIVertex_t296  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t296 *)args[0]), *((UIVertex_t296 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t285  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t285  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t285_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t285  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t285  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2022  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2022  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UICharInfo_t285_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t285  p1, UICharInfo_t285  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), *((UICharInfo_t285 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t285_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t285  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_UICharInfo_t285_UICharInfo_t285 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t285  p1, UICharInfo_t285  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), *((UICharInfo_t285 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t285_UICharInfo_t285_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t285  p1, UICharInfo_t285  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t285 *)args[0]), *((UICharInfo_t285 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t286  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t286  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t286_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t286  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t286  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2032 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2032  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2032  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_UILineInfo_t286_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t286  p1, UILineInfo_t286  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), *((UILineInfo_t286 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t286_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t286  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_UILineInfo_t286_UILineInfo_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t286  p1, UILineInfo_t286  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), *((UILineInfo_t286 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t286_UILineInfo_t286_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t286  p1, UILineInfo_t286  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t286 *)args[0]), *((UILineInfo_t286 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2047_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2047  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2047  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2052 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2052  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2052  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2047 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2047  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2047  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1229  (*Func)(void* obj, const MethodInfo* method);
	Link_t1229  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2051 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2051  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2051  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2047_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2047  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2047  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2066_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2066  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2066  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_ObjectU26_t2828 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Int32_t106_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2066_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2066  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2066  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2086_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2086  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2086  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1392  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1392  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t368 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t368  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t368  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t387_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2114_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2114  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2114  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t387_Object_t_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_TextEditOpU26_t2879 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2119  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2119  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2114 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2114  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2114  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2118  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2118  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2114_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2114  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2114  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t113_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RaycastHit_t78_RaycastHit_t78_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t78  p1, RaycastHit_t78  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t78 *)args[0]), *((RaycastHit_t78 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Color_t11_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t11  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t11 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_FloatTween_t535 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, FloatTween_t535  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((FloatTween_t535 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_ColorTween_t532 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t532  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t532 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2342_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2342  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2342  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Object_t_BooleanU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2347  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2347  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Object_t_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2342 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2342  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2342  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2346 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2346  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2346  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2342_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2342  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2342  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_SByte_t1124_SByte_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t843  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t843  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2362_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2362  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2362  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2366 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2366  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2366  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t943_Int32_t106_Int32_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t943  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t943  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2362 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2362  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2362  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2365  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2365  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2362_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2362  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2362  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t893  (*Func)(void* obj, const MethodInfo* method);
	Mark_t893  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t930  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t930  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t1074 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1162  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1162  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1239 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1239  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1239  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1247 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1247  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1247  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1326  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1326  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1328  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1328  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1327  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1327  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1374  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t1374  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1373  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t1373  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1374_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1374  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1374  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2414 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2414  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2414  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, CustomAttributeTypedArgument_t1374  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), *((CustomAttributeTypedArgument_t1374 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1374_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, CustomAttributeTypedArgument_t1374  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), *((CustomAttributeTypedArgument_t1374 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1374  p1, CustomAttributeTypedArgument_t1374  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1374 *)args[0]), *((CustomAttributeTypedArgument_t1374 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1373_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1373  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1373  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2425 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2425  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2425  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, CustomAttributeNamedArgument_t1373  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), *((CustomAttributeNamedArgument_t1373 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1373_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, CustomAttributeNamedArgument_t1373  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), *((CustomAttributeNamedArgument_t1373 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1373  p1, CustomAttributeNamedArgument_t1373  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1373 *)args[0]), *((CustomAttributeNamedArgument_t1373 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ResourceInfo_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1403  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t1403  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t1404 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1404  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t1404  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1542 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_DateTimeOffset_t1698_DateTimeOffset_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1698  p1, DateTimeOffset_t1698  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1698 *)args[0]), *((DateTimeOffset_t1698 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_DateTimeOffset_t1698_DateTimeOffset_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1698  p1, DateTimeOffset_t1698  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1698 *)args[0]), *((DateTimeOffset_t1698 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Nullable_1_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1802  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1802 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t106_Guid_t1720_Guid_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1720  p1, Guid_t1720  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1720 *)args[0]), *((Guid_t1720 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t455_Guid_t1720_Guid_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1720  p1, Guid_t1720  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1720 *)args[0]), *((Guid_t1720 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1577] = 
{
	RuntimeInvoker_Void_t1129,
	RuntimeInvoker_Void_t1129_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t,
	RuntimeInvoker_Boolean_t455,
	RuntimeInvoker_Void_t1129_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Single_t113,
	RuntimeInvoker_Void_t1129_Single_t113,
	RuntimeInvoker_Object_t_Int32_t106,
	RuntimeInvoker_Matrix4x4_t43_Int32_t106_Int32_t106,
	RuntimeInvoker_Rect_t18_Int32_t106_Int32_t106,
	RuntimeInvoker_Vector2_t15,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106,
	RuntimeInvoker_Matrix4x4_t43,
	RuntimeInvoker_Quaternion_t50,
	RuntimeInvoker_Vector3_t3,
	RuntimeInvoker_Matrix4x4_t43_Int32_t106,
	RuntimeInvoker_Vector3_t3_Int32_t106,
	RuntimeInvoker_Rect_t18_Int32_t106,
	RuntimeInvoker_Void_t1129_Matrix4x4U26_t2772_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_Single_t113_Single_t113,
	RuntimeInvoker_Ray_t71,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_SingleU5BU5DU26_t2773,
	RuntimeInvoker_Rect_t18_Object_t,
	RuntimeInvoker_Distortion_t37_Single_t113_Single_t113_Single_t113_Int32_t106,
	RuntimeInvoker_Distortion_t37_Distortion_t37_Single_t113_Int32_t106,
	RuntimeInvoker_Single_t113_Single_t113,
	RuntimeInvoker_Boolean_t455_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector3_t3_Quaternion_t50,
	RuntimeInvoker_Void_t1129_Matrix4x4_t43,
	RuntimeInvoker_Void_t1129_Vector3_t3,
	RuntimeInvoker_Void_t1129_Quaternion_t50,
	RuntimeInvoker_Void_t1129_Object_t_Object_t,
	RuntimeInvoker_Int32_t106,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_SingleU26_t2774_SingleU26_t2774,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Matrix4x4U26_t2772_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_Single_t113,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Matrix4x4_t43_Single_t113_Single_t113_Single_t113_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Vector2_t15,
	RuntimeInvoker_Int32_t106_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774,
	RuntimeInvoker_Int32_t106_Int32U26_t2775_Int32U26_t2775_SingleU26_t2774,
	RuntimeInvoker_Int32_t106_SingleU26_t2774_SingleU26_t2774_SingleU26_t2774,
	RuntimeInvoker_Object_t_Object_t_Single_t113_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Single_t113_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Double_t465,
	RuntimeInvoker_Void_t1129_Int64_t466_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_GcAchievementDescriptionData_t346_Int32_t106,
	RuntimeInvoker_Void_t1129_GcUserProfileData_t345_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Double_t465_Object_t,
	RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_UserProfileU5BU5DU26_t2776_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_UserProfileU5BU5DU26_t2776_Int32_t106,
	RuntimeInvoker_Void_t1129_GcScoreData_t348,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_ColorSpace_t352,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_BoneWeight_t183_BoneWeight_t183,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Rect_t18_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Void_t1129_InternalDrawTextureArgumentsU26_t2777,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Color_t11,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Color_t11_Single_t113,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_ColorU26_t2778_Single_t113,
	RuntimeInvoker_Color_t11,
	RuntimeInvoker_Void_t1129_Color_t11,
	RuntimeInvoker_Void_t1129_ColorU26_t2778,
	RuntimeInvoker_Void_t1129_Rect_t18,
	RuntimeInvoker_Void_t1129_RectU26_t2779,
	RuntimeInvoker_Object_t_Vector3_t3,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t2780,
	RuntimeInvoker_Int32_t106_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_IntPtr_t,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Color_t11,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_ColorU26_t2778,
	RuntimeInvoker_Color_t11_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124,
	RuntimeInvoker_RenderTextureFormat_t354,
	RuntimeInvoker_Void_t1129_Object_t_IntPtr_t_Int32_t106,
	RuntimeInvoker_Void_t1129_CullingGroupEvent_t194,
	RuntimeInvoker_Object_t_CullingGroupEvent_t194_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Color_t11_Single_t113,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t,
	RuntimeInvoker_Void_t1129_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2781_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t,
	RuntimeInvoker_Int32_t106_LayerMask_t30,
	RuntimeInvoker_LayerMask_t30_Int32_t106,
	RuntimeInvoker_Single_t113_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Single_t113,
	RuntimeInvoker_Vector2_t15_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Single_t113_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Single_t113_Vector2_t15,
	RuntimeInvoker_Vector2_t15_Vector2_t15_Single_t113,
	RuntimeInvoker_Boolean_t455_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Vector2_t15_Vector3_t3,
	RuntimeInvoker_Vector3_t3_Vector2_t15,
	RuntimeInvoker_Vector3_t3_Vector3_t3_Vector3_t3_Single_t113,
	RuntimeInvoker_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Single_t113_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Single_t113_Vector3_t3,
	RuntimeInvoker_Vector3_t3_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Vector3_t3_Vector3_t3_Single_t113,
	RuntimeInvoker_Vector3_t3_Single_t113_Vector3_t3,
	RuntimeInvoker_Boolean_t455_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Color_t11_Color_t11_Color_t11_Single_t113,
	RuntimeInvoker_Color_t11_Color_t11_Single_t113,
	RuntimeInvoker_Vector4_t90_Color_t11,
	RuntimeInvoker_Color_t11_Vector4_t90,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Color32_t187_Color_t11,
	RuntimeInvoker_Color_t11_Color32_t187,
	RuntimeInvoker_Single_t113_Quaternion_t50_Quaternion_t50,
	RuntimeInvoker_Quaternion_t50_Single_t113_Vector3_t3,
	RuntimeInvoker_Quaternion_t50_Single_t113_Vector3U26_t2780,
	RuntimeInvoker_Quaternion_t50_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Quaternion_t50_Vector3U26_t2780_Vector3U26_t2780,
	RuntimeInvoker_Quaternion_t50_Quaternion_t50,
	RuntimeInvoker_Quaternion_t50_QuaternionU26_t2782,
	RuntimeInvoker_Quaternion_t50_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Vector3_t3_Quaternion_t50,
	RuntimeInvoker_Vector3_t3_QuaternionU26_t2782,
	RuntimeInvoker_Quaternion_t50_Vector3_t3,
	RuntimeInvoker_Quaternion_t50_Vector3U26_t2780,
	RuntimeInvoker_Quaternion_t50_Quaternion_t50_Quaternion_t50,
	RuntimeInvoker_Vector3_t3_Quaternion_t50_Vector3_t3,
	RuntimeInvoker_Boolean_t455_Quaternion_t50_Quaternion_t50,
	RuntimeInvoker_Void_t1129_Vector2_t15,
	RuntimeInvoker_Boolean_t455_Vector3_t3,
	RuntimeInvoker_Boolean_t455_Rect_t18,
	RuntimeInvoker_Boolean_t455_Rect_t18_Rect_t18,
	RuntimeInvoker_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Single_t113,
	RuntimeInvoker_Matrix4x4_t43_Matrix4x4_t43,
	RuntimeInvoker_Matrix4x4_t43_Matrix4x4U26_t2772,
	RuntimeInvoker_Boolean_t455_Matrix4x4_t43_Matrix4x4U26_t2772,
	RuntimeInvoker_Boolean_t455_Matrix4x4U26_t2772_Matrix4x4U26_t2772,
	RuntimeInvoker_Vector4_t90_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Vector4_t90,
	RuntimeInvoker_Matrix4x4_t43_Vector3_t3,
	RuntimeInvoker_Void_t1129_Vector3_t3_Quaternion_t50_Vector3_t3,
	RuntimeInvoker_Matrix4x4_t43_Vector3_t3_Quaternion_t50_Vector3_t3,
	RuntimeInvoker_Matrix4x4_t43_Vector3U26_t2780_QuaternionU26_t2782_Vector3U26_t2780,
	RuntimeInvoker_Matrix4x4_t43_Single_t113_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Matrix4x4_t43_Matrix4x4_t43_Matrix4x4_t43,
	RuntimeInvoker_Vector4_t90_Matrix4x4_t43_Vector4_t90,
	RuntimeInvoker_Boolean_t455_Matrix4x4_t43_Matrix4x4_t43,
	RuntimeInvoker_Void_t1129_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Void_t1129_Bounds_t203,
	RuntimeInvoker_Boolean_t455_Bounds_t203,
	RuntimeInvoker_Boolean_t455_Bounds_t203_Vector3_t3,
	RuntimeInvoker_Boolean_t455_BoundsU26_t2783_Vector3U26_t2780,
	RuntimeInvoker_Single_t113_Bounds_t203_Vector3_t3,
	RuntimeInvoker_Single_t113_BoundsU26_t2783_Vector3U26_t2780,
	RuntimeInvoker_Boolean_t455_RayU26_t2784_BoundsU26_t2783_SingleU26_t2774,
	RuntimeInvoker_Boolean_t455_Ray_t71,
	RuntimeInvoker_Boolean_t455_Ray_t71_SingleU26_t2774,
	RuntimeInvoker_Vector3_t3_BoundsU26_t2783_Vector3U26_t2780,
	RuntimeInvoker_Boolean_t455_Bounds_t203_Bounds_t203,
	RuntimeInvoker_Single_t113_Vector4_t90_Vector4_t90,
	RuntimeInvoker_Single_t113_Vector4_t90,
	RuntimeInvoker_Vector4_t90,
	RuntimeInvoker_Vector4_t90_Vector4_t90_Vector4_t90,
	RuntimeInvoker_Vector4_t90_Vector4_t90_Single_t113,
	RuntimeInvoker_Boolean_t455_Vector4_t90_Vector4_t90,
	RuntimeInvoker_Vector3_t3_Vector4_t90,
	RuntimeInvoker_Vector3_t3_Single_t113,
	RuntimeInvoker_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Single_t113,
	RuntimeInvoker_Single_t113_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Single_t113_Single_t113,
	RuntimeInvoker_Single_t113_Single_t113_Single_t113_SingleU26_t2774_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Rect_t18,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785,
	RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Color_t11,
	RuntimeInvoker_Void_t1129_Int32_t106_Color_t11,
	RuntimeInvoker_Void_t1129_Int32_t106_ColorU26_t2778,
	RuntimeInvoker_Void_t1129_Object_t_Vector4_t90,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_ColorU26_t2778,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Vector2U26_t2785,
	RuntimeInvoker_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_SphericalHarmonicsL2U26_t2786,
	RuntimeInvoker_Void_t1129_Color_t11_SphericalHarmonicsL2U26_t2786,
	RuntimeInvoker_Void_t1129_ColorU26_t2778_SphericalHarmonicsL2U26_t2786,
	RuntimeInvoker_Void_t1129_Vector3_t3_Color_t11_Single_t113,
	RuntimeInvoker_Void_t1129_Vector3_t3_Color_t11_SphericalHarmonicsL2U26_t2786,
	RuntimeInvoker_Void_t1129_Vector3U26_t2780_ColorU26_t2778_SphericalHarmonicsL2U26_t2786,
	RuntimeInvoker_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218_Single_t113,
	RuntimeInvoker_SphericalHarmonicsL2_t218_Single_t113_SphericalHarmonicsL2_t218,
	RuntimeInvoker_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218,
	RuntimeInvoker_Boolean_t455_SphericalHarmonicsL2_t218_SphericalHarmonicsL2_t218,
	RuntimeInvoker_Void_t1129_Vector4U26_t2787,
	RuntimeInvoker_Vector4_t90_Object_t,
	RuntimeInvoker_Vector2_t15_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Vector2U26_t2785,
	RuntimeInvoker_RuntimePlatform_t161,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Matrix4x4U26_t2772,
	RuntimeInvoker_CameraClearFlags_t351,
	RuntimeInvoker_Vector3_t3_Object_t_Vector3U26_t2780,
	RuntimeInvoker_Ray_t71_Vector3_t3,
	RuntimeInvoker_Ray_t71_Object_t_Vector3U26_t2780,
	RuntimeInvoker_Object_t_Ray_t71_Single_t113_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2784_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2784_Single_t113_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector3_t3_Vector3_t3_Color_t11,
	RuntimeInvoker_Void_t1129_Vector3U26_t2780_Vector3U26_t2780_ColorU26_t2778_Single_t113_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_IntPtr_t,
	RuntimeInvoker_RenderBuffer_t350,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Void_t1129_IntPtr_t_RenderBufferU26_t2788_RenderBufferU26_t2788,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_TouchPhase_t231,
	RuntimeInvoker_Single_t113_Object_t,
	RuntimeInvoker_Void_t1129_Vector3U26_t2780,
	RuntimeInvoker_Touch_t233_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Object_t,
	RuntimeInvoker_Void_t1129_QuaternionU26_t2782,
	RuntimeInvoker_Void_t1129_Vector3_t3_Int32_t106,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Single_t113,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Single_t113_Object_t_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Vector3_t3_Vector3_t3_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113_Int32_t106,
	RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Ray_t71_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Vector3_t3_Vector3_t3_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Vector3U26_t2780_Vector3U26_t2780_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Vector3U26_t2780_Vector3U26_t2780_RaycastHitU26_t2789_Single_t113_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Ray_t71_RaycastHitU26_t2789_Single_t113,
	RuntimeInvoker_Boolean_t455_Object_t_RayU26_t2784_RaycastHitU26_t2789_Single_t113,
	RuntimeInvoker_Boolean_t455_Ray_t71_RaycastHitU26_t2789_Single_t113,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Vector2_t15_Vector3_t3_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Vector2U26_t2785_Vector3U26_t2780_Int32_t106_Int32_t106,
	RuntimeInvoker_CollisionFlags_t149_Vector3_t3,
	RuntimeInvoker_CollisionFlags_t149_Object_t_Vector3U26_t2780,
	RuntimeInvoker_Void_t1129_Vector2_t15_Vector2_t15_Single_t113_Int32_t106_Single_t113_Single_t113_RaycastHit2DU26_t2790,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785_Vector2U26_t2785_Single_t113_Int32_t106_Single_t113_Single_t113_RaycastHit2DU26_t2790,
	RuntimeInvoker_RaycastHit2D_t252_Vector2_t15_Vector2_t15_Single_t113_Int32_t106,
	RuntimeInvoker_RaycastHit2D_t252_Vector2_t15_Vector2_t15_Single_t113_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Object_t_Vector2_t15_Vector2_t15_Single_t113_Int32_t106,
	RuntimeInvoker_Object_t_Vector2U26_t2785_Vector2U26_t2785_Single_t113_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Object_t_SByte_t1124_Object_t_Object_t,
	RuntimeInvoker_SendMessageOptions_t159,
	RuntimeInvoker_AnimatorStateInfo_t267,
	RuntimeInvoker_AnimatorClipInfo_t268,
	RuntimeInvoker_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int16_t1125,
	RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int16_t1125_CharacterInfoU26_t2791,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Color_t11_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Vector2_t15_Vector2_t15_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Color_t11_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Single_t113_Single_t113_Single_t113_Single_t113_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_ColorU26_t2778_Int32_t106_Single_t113_Single_t113_Int32_t106_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_Single_t113_Single_t113_Single_t113_Single_t113_SByte_t1124,
	RuntimeInvoker_TextGenerationSettings_t288_TextGenerationSettings_t288,
	RuntimeInvoker_Single_t113_Object_t_TextGenerationSettings_t288,
	RuntimeInvoker_Boolean_t455_Object_t_TextGenerationSettings_t288,
	RuntimeInvoker_RenderMode_t292,
	RuntimeInvoker_Boolean_t455_Vector2_t15_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_ColorU26_t2778,
	RuntimeInvoker_Void_t1129_Object_t_RectU26_t2779,
	RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Vector2U26_t2785_Object_t,
	RuntimeInvoker_Vector2_t15_Vector2_t15_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Vector2_t15_Object_t_Object_t_Vector2U26_t2785,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785_Object_t_Object_t_Vector2U26_t2785,
	RuntimeInvoker_Rect_t18_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t_Vector3U26_t2780,
	RuntimeInvoker_Boolean_t455_Object_t_Vector2_t15_Object_t_Vector2U26_t2785,
	RuntimeInvoker_Ray_t71_Object_t_Vector2_t15,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Void_t1129_Ray_t71,
	RuntimeInvoker_EventType_t301,
	RuntimeInvoker_EventType_t301_Int32_t106,
	RuntimeInvoker_EventModifiers_t302,
	RuntimeInvoker_Char_t699,
	RuntimeInvoker_Void_t1129_Int16_t1125,
	RuntimeInvoker_KeyCode_t151,
	RuntimeInvoker_Void_t1129_DateTime_t308,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_Int32_t106_SByte_t1124_Single_t113,
	RuntimeInvoker_Boolean_t455_Rect_t18_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Int32_t106_Single_t113_Single_t113_Object_t,
	RuntimeInvoker_Void_t1129_Rect_t18_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1129_RectU26_t2779_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t455_Rect_t18_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t455_RectU26_t2779_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Single_t113,
	RuntimeInvoker_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Rect_t18,
	RuntimeInvoker_Void_t1129_Int32_t106_RectU26_t2779,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_Single_t113_Single_t113_Object_t,
	RuntimeInvoker_IntPtr_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Int32_t106_Object_t,
	RuntimeInvoker_UserState_t372,
	RuntimeInvoker_Void_t1129_Object_t_Double_t465_SByte_t1124_SByte_t1124_DateTime_t308,
	RuntimeInvoker_Double_t465,
	RuntimeInvoker_Void_t1129_Double_t465,
	RuntimeInvoker_DateTime_t308,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1124_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_DateTime_t308_Object_t_Int32_t106,
	RuntimeInvoker_Int64_t466,
	RuntimeInvoker_Void_t1129_Int64_t466,
	RuntimeInvoker_UserScope_t373,
	RuntimeInvoker_Range_t366,
	RuntimeInvoker_Void_t1129_Range_t366,
	RuntimeInvoker_TimeScope_t374,
	RuntimeInvoker_Void_t1129_Int32_t106_HitInfo_t368,
	RuntimeInvoker_Boolean_t455_HitInfo_t368_HitInfo_t368,
	RuntimeInvoker_Boolean_t455_HitInfo_t368,
	RuntimeInvoker_Void_t1129_Object_t_StringU26_t2792_StringU26_t2792,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_StreamingContext_t441,
	RuntimeInvoker_Boolean_t455_Color_t11_Color_t11,
	RuntimeInvoker_Boolean_t455_TextGenerationSettings_t288,
	RuntimeInvoker_PersistentListenerMode_t390,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Int32_t106_RaycastResult_t94_RaycastResult_t94,
	RuntimeInvoker_MoveDirection_t509,
	RuntimeInvoker_RaycastResult_t94,
	RuntimeInvoker_Void_t1129_RaycastResult_t94,
	RuntimeInvoker_InputButton_t514,
	RuntimeInvoker_RaycastResult_t94_Object_t,
	RuntimeInvoker_MoveDirection_t509_Single_t113_Single_t113,
	RuntimeInvoker_MoveDirection_t509_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Object_t_Single_t113_Single_t113_Single_t113,
	RuntimeInvoker_Boolean_t455_Int32_t106_PointerEventDataU26_t2793_SByte_t1124,
	RuntimeInvoker_Object_t_Touch_t233_BooleanU26_t2794_BooleanU26_t2794,
	RuntimeInvoker_FramePressState_t515_Int32_t106,
	RuntimeInvoker_Boolean_t455_Vector2_t15_Vector2_t15_Single_t113_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_InputMode_t523,
	RuntimeInvoker_LayerMask_t30,
	RuntimeInvoker_Void_t1129_LayerMask_t30,
	RuntimeInvoker_Int32_t106_RaycastHit_t78_RaycastHit_t78,
	RuntimeInvoker_ColorTweenMode_t529,
	RuntimeInvoker_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_ColorBlock_t546,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t_Object_t,
	RuntimeInvoker_FontStyle_t327,
	RuntimeInvoker_TextAnchor_t278,
	RuntimeInvoker_HorizontalWrapMode_t279,
	RuntimeInvoker_VerticalWrapMode_t280,
	RuntimeInvoker_Void_t1129_Color_t11_Single_t113_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Color_t11_Single_t113_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Color_t11_Single_t113,
	RuntimeInvoker_Void_t1129_Single_t113_Single_t113_SByte_t1124,
	RuntimeInvoker_BlockingObjects_t566,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Vector2_t15_Object_t,
	RuntimeInvoker_Type_t572,
	RuntimeInvoker_FillMethod_t573,
	RuntimeInvoker_Vector4_t90_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Color32_t187_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Vector2_t15_Vector2_t15_Color32_t187_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Vector4_t90_Vector4_t90_Rect_t18,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Single_t113_SByte_t1124_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Single_t113_Single_t113_SByte_t1124_Int32_t106,
	RuntimeInvoker_Vector2_t15_Vector2_t15_Rect_t18,
	RuntimeInvoker_ContentType_t575,
	RuntimeInvoker_LineType_t578,
	RuntimeInvoker_InputType_t576,
	RuntimeInvoker_TouchScreenKeyboardType_t201,
	RuntimeInvoker_CharacterValidation_t577,
	RuntimeInvoker_Void_t1129_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Vector2_t15_Object_t,
	RuntimeInvoker_Int32_t106_Vector2_t15,
	RuntimeInvoker_EditState_t582_Object_t,
	RuntimeInvoker_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Int32_t106_Int32_t106_SByte_t1124,
	RuntimeInvoker_Single_t113_Int32_t106_Object_t,
	RuntimeInvoker_Char_t699_Object_t_Int32_t106_Int16_t1125,
	RuntimeInvoker_Void_t1129_Int32_t106_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int16_t1125_Object_t_Object_t,
	RuntimeInvoker_Char_t699_Object_t,
	RuntimeInvoker_Void_t1129_Rect_t18_SByte_t1124,
	RuntimeInvoker_Mode_t593,
	RuntimeInvoker_Navigation_t594,
	RuntimeInvoker_Direction_t599,
	RuntimeInvoker_Void_t1129_Single_t113_SByte_t1124,
	RuntimeInvoker_Axis_t601,
	RuntimeInvoker_MovementType_t604,
	RuntimeInvoker_ScrollbarVisibility_t605,
	RuntimeInvoker_Void_t1129_Single_t113_Int32_t106,
	RuntimeInvoker_Bounds_t203,
	RuntimeInvoker_Void_t1129_Navigation_t594,
	RuntimeInvoker_Transition_t609,
	RuntimeInvoker_Void_t1129_ColorBlock_t546,
	RuntimeInvoker_SpriteState_t611,
	RuntimeInvoker_Void_t1129_SpriteState_t611,
	RuntimeInvoker_SelectionState_t610,
	RuntimeInvoker_Vector3_t3_Object_t_Vector2_t15,
	RuntimeInvoker_Void_t1129_Color_t11_SByte_t1124,
	RuntimeInvoker_Boolean_t455_ColorU26_t2778_Color_t11,
	RuntimeInvoker_Direction_t615,
	RuntimeInvoker_Axis_t617,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_TextGenerationSettings_t288_Vector2_t15,
	RuntimeInvoker_Vector2_t15_Int32_t106,
	RuntimeInvoker_Rect_t18_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Rect_t18_Rect_t18_Rect_t18,
	RuntimeInvoker_AspectMode_t631,
	RuntimeInvoker_Single_t113_Single_t113_Int32_t106,
	RuntimeInvoker_ScaleMode_t633,
	RuntimeInvoker_ScreenMatchMode_t634,
	RuntimeInvoker_Unit_t635,
	RuntimeInvoker_FitMode_t637,
	RuntimeInvoker_Corner_t639,
	RuntimeInvoker_Axis_t640,
	RuntimeInvoker_Constraint_t641,
	RuntimeInvoker_Single_t113_Int32_t106_Single_t113,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Boolean_t455_LayoutRebuilder_t648,
	RuntimeInvoker_Single_t113_Object_t_Int32_t106,
	RuntimeInvoker_Single_t113_Object_t_Object_t_Single_t113,
	RuntimeInvoker_Single_t113_Object_t_Object_t_Single_t113_ILayoutElementU26_t2795,
	RuntimeInvoker_Void_t1129_UIVertexU26_t2796_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector3_t3_Color32_t187_Vector2_t15_Vector2_t15_Vector3_t3_Vector4_t90,
	RuntimeInvoker_Void_t1129_Vector3_t3_Color32_t187_Vector2_t15,
	RuntimeInvoker_Void_t1129_UIVertex_t296,
	RuntimeInvoker_Void_t1129_Object_t_Color32_t187_Int32_t106_Int32_t106_Single_t113_Single_t113,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_TypeCode_t736_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_Object_t_Object_t,
	RuntimeInvoker_UInt32_t467_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2797,
	RuntimeInvoker_DictionaryEntry_t943,
	RuntimeInvoker_EditorBrowsableState_t785,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Int16_t1125_Int16_t1125,
	RuntimeInvoker_Boolean_t455_Object_t_IPAddressU26_t2798,
	RuntimeInvoker_AddressFamily_t790,
	RuntimeInvoker_Object_t_Int64_t466,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775,
	RuntimeInvoker_Boolean_t455_Object_t_IPv6AddressU26_t2799,
	RuntimeInvoker_UInt16_t960_Int16_t1125,
	RuntimeInvoker_Object_t_SByte_t1124,
	RuntimeInvoker_SecurityProtocolType_t809,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Int32_t106_SByte_t1124,
	RuntimeInvoker_AsnDecodeStatus_t860_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_SByte_t1124,
	RuntimeInvoker_X509ChainStatusFlags_t847_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t847_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_X509ChainStatusFlags_t847_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_X509ChainStatusFlags_t847,
	RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32_t106_Int32_t106,
	RuntimeInvoker_X509RevocationFlag_t854,
	RuntimeInvoker_X509RevocationMode_t855,
	RuntimeInvoker_X509VerificationFlags_t859,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_X509KeyUsageFlags_t852,
	RuntimeInvoker_X509KeyUsageFlags_t852_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Byte_t454_Int16_t1125,
	RuntimeInvoker_Byte_t454_Int16_t1125_Int16_t1125,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_RegexOptions_t878,
	RuntimeInvoker_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Category_t885_Object_t,
	RuntimeInvoker_Boolean_t455_UInt16_t960_Int16_t1125,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int16_t1125,
	RuntimeInvoker_Void_t1129_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_UInt16_t960_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int16_t1125_Object_t_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_UInt16_t960,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_SByte_t1124_Object_t,
	RuntimeInvoker_Void_t1129_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_SByte_t1124_Int32_t106_Object_t,
	RuntimeInvoker_UInt16_t960_UInt16_t960_UInt16_t960,
	RuntimeInvoker_OpFlags_t880_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_UInt16_t960_UInt16_t960,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775_Int32U26_t2775_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Boolean_t455_UInt16_t960_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int32_t106_SByte_t1124_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_SByte_t1124_Int32_t106,
	RuntimeInvoker_Interval_t900,
	RuntimeInvoker_Boolean_t455_Interval_t900,
	RuntimeInvoker_Void_t1129_Interval_t900,
	RuntimeInvoker_Interval_t900_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Double_t465_Interval_t900,
	RuntimeInvoker_Object_t_Interval_t900_Object_t_Object_t,
	RuntimeInvoker_Double_t465_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2775,
	RuntimeInvoker_Object_t_RegexOptionsU26_t2800,
	RuntimeInvoker_Void_t1129_RegexOptionsU26_t2800_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Int32U26_t2775_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Category_t885,
	RuntimeInvoker_Int32_t106_Int16_t1125_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Int16_t1125,
	RuntimeInvoker_Char_t699_Int16_t1125,
	RuntimeInvoker_Int32_t106_Int32U26_t2775,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_UInt16_t960_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_UInt16_t960,
	RuntimeInvoker_Position_t881,
	RuntimeInvoker_UriHostNameType_t934_Object_t,
	RuntimeInvoker_Object_t_Int16_t1125,
	RuntimeInvoker_Void_t1129_StringU26_t2792,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Char_t699_Object_t_Int32U26_t2775_CharU26_t2801,
	RuntimeInvoker_Void_t1129_Object_t_UriFormatExceptionU26_t2802,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_UInt32_t467_Object_t_Int32_t106,
	RuntimeInvoker_Sign_t985_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_ConfidenceFactor_t989,
	RuntimeInvoker_Void_t1129_SByte_t1124_Object_t,
	RuntimeInvoker_Byte_t454,
	RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_ByteU26_t2803_Int32U26_t2775_ByteU5BU5DU26_t2804,
	RuntimeInvoker_DateTime_t308_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t967,
	RuntimeInvoker_RSAParameters_t965_SByte_t1124,
	RuntimeInvoker_Void_t1129_RSAParameters_t965,
	RuntimeInvoker_DSAParameters_t967_BooleanU26_t2794,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t_SByte_t1124,
	RuntimeInvoker_Boolean_t455_DateTime_t308,
	RuntimeInvoker_X509ChainStatusFlags_t1013,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Byte_t454,
	RuntimeInvoker_Void_t1129_Byte_t454_Byte_t454,
	RuntimeInvoker_AlertLevel_t1026,
	RuntimeInvoker_AlertDescription_t1027,
	RuntimeInvoker_Object_t_Byte_t454,
	RuntimeInvoker_Void_t1129_Int16_t1125_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Int16_t1125_SByte_t1124_SByte_t1124,
	RuntimeInvoker_CipherAlgorithmType_t1029,
	RuntimeInvoker_HashAlgorithmType_t1047,
	RuntimeInvoker_ExchangeAlgorithmType_t1045,
	RuntimeInvoker_CipherMode_t760,
	RuntimeInvoker_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_ByteU5BU5DU26_t2804_ByteU5BU5DU26_t2804,
	RuntimeInvoker_Object_t_Byte_t454_Object_t,
	RuntimeInvoker_Object_t_Int16_t1125_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_Int16_t1125_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t1061,
	RuntimeInvoker_SecurityCompressionType_t1060,
	RuntimeInvoker_HandshakeType_t1075,
	RuntimeInvoker_HandshakeState_t1046,
	RuntimeInvoker_UInt64_t1123,
	RuntimeInvoker_SecurityProtocolType_t1061_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t454_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t454_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Byte_t454_Object_t,
	RuntimeInvoker_Object_t_Byte_t454_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Int64_t466_Int64_t466_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Byte_t454_Byte_t454_Object_t,
	RuntimeInvoker_RSAParameters_t965,
	RuntimeInvoker_Void_t1129_Object_t_Byte_t454,
	RuntimeInvoker_Void_t1129_Object_t_Byte_t454_Byte_t454,
	RuntimeInvoker_Void_t1129_Object_t_Byte_t454_Object_t,
	RuntimeInvoker_ContentType_t1040,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_ObjectU5BU5DU26_t2805,
	RuntimeInvoker_Int32_t106_Object_t_ObjectU5BU5DU26_t2805,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Byte_t454_Object_t,
	RuntimeInvoker_Decimal_t738_Object_t,
	RuntimeInvoker_Int16_t1125_Object_t,
	RuntimeInvoker_Int64_t466_Object_t,
	RuntimeInvoker_SByte_t1124_Object_t,
	RuntimeInvoker_UInt16_t960_Object_t,
	RuntimeInvoker_UInt32_t467_Object_t,
	RuntimeInvoker_UInt64_t1123_Object_t,
	RuntimeInvoker_Boolean_t455_SByte_t1124_Object_t_Int32_t106_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int32U26_t2775_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_SByte_t1124_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Object_t_Object_t_BooleanU26_t2794_BooleanU26_t2794,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Object_t_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_Int32U26_t2775_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Int32U26_t2775_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Int16_t1125_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775_ExceptionU26_t2806,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32U26_t2775,
	RuntimeInvoker_TypeCode_t736,
	RuntimeInvoker_Int32_t106_Int64_t466,
	RuntimeInvoker_Boolean_t455_Int64_t466,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int64U26_t2807_ExceptionU26_t2806,
	RuntimeInvoker_Int64_t466_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int64U26_t2807_ExceptionU26_t2806,
	RuntimeInvoker_Int64_t466_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int64U26_t2807,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int64U26_t2807,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_UInt32U26_t2808_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_UInt32U26_t2808_ExceptionU26_t2806,
	RuntimeInvoker_UInt32_t467_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_UInt32_t467_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_UInt32U26_t2808,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_UInt32U26_t2808,
	RuntimeInvoker_UInt64_t1123_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_UInt64U26_t2809_ExceptionU26_t2806,
	RuntimeInvoker_UInt64_t1123_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_UInt64U26_t2809,
	RuntimeInvoker_Int32_t106_SByte_t1124,
	RuntimeInvoker_Boolean_t455_SByte_t1124,
	RuntimeInvoker_Byte_t454_Object_t_Object_t,
	RuntimeInvoker_Byte_t454_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_ByteU26_t2803,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_ByteU26_t2803,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_SByteU26_t2810_ExceptionU26_t2806,
	RuntimeInvoker_SByte_t1124_Object_t_Object_t,
	RuntimeInvoker_SByte_t1124_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_SByteU26_t2810,
	RuntimeInvoker_Boolean_t455_Object_t_SByte_t1124_Int16U26_t2811_ExceptionU26_t2806,
	RuntimeInvoker_Int16_t1125_Object_t_Object_t,
	RuntimeInvoker_Int16_t1125_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int16U26_t2811,
	RuntimeInvoker_UInt16_t960_Object_t_Object_t,
	RuntimeInvoker_UInt16_t960_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_UInt16U26_t2812,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_UInt16U26_t2812,
	RuntimeInvoker_Void_t1129_ByteU2AU26_t2813_ByteU2AU26_t2813_DoubleU2AU26_t2814_UInt16U2AU26_t2815_UInt16U2AU26_t2815_UInt16U2AU26_t2815_UInt16U2AU26_t2815,
	RuntimeInvoker_UnicodeCategory_t1275_Int16_t1125,
	RuntimeInvoker_Char_t699_Int16_t1125_Object_t,
	RuntimeInvoker_Void_t1129_Int16_t1125_Int32_t106,
	RuntimeInvoker_Char_t699_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_SByte_t1124_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106_SByte_t1124_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Int16_t1125_Int32_t106,
	RuntimeInvoker_Object_t_Int32_t106_Int16_t1125,
	RuntimeInvoker_Object_t_Int16_t1125_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_Int32U26_t2775_Int32U26_t2775_Int32U26_t2775_BooleanU26_t2794_StringU26_t2792,
	RuntimeInvoker_Void_t1129_Int32_t106_Int16_t1125,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Object_t_Int16_t1125_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Single_t113,
	RuntimeInvoker_Single_t113_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Double_t465,
	RuntimeInvoker_Boolean_t455_Double_t465,
	RuntimeInvoker_Double_t465_Object_t_Object_t,
	RuntimeInvoker_Double_t465_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_DoubleU26_t2816_ExceptionU26_t2806,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_DoubleU26_t2816,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Object_t_Decimal_t738,
	RuntimeInvoker_Decimal_t738_Decimal_t738_Decimal_t738,
	RuntimeInvoker_UInt64_t1123_Decimal_t738,
	RuntimeInvoker_Int64_t466_Decimal_t738,
	RuntimeInvoker_Boolean_t455_Decimal_t738_Decimal_t738,
	RuntimeInvoker_Decimal_t738_Decimal_t738,
	RuntimeInvoker_Int32_t106_Decimal_t738_Decimal_t738,
	RuntimeInvoker_Int32_t106_Decimal_t738,
	RuntimeInvoker_Boolean_t455_Decimal_t738,
	RuntimeInvoker_Decimal_t738_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Int32U26_t2775_BooleanU26_t2794_BooleanU26_t2794_Int32U26_t2775_SByte_t1124,
	RuntimeInvoker_Decimal_t738_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_DecimalU26_t2817_SByte_t1124,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_UInt64U26_t2809,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_Int64U26_t2807,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_DecimalU26_t2817,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_Int32_t106,
	RuntimeInvoker_Double_t465_DecimalU26_t2817,
	RuntimeInvoker_Void_t1129_DecimalU26_t2817_Int32_t106,
	RuntimeInvoker_Int32_t106_DecimalU26_t2817_DecimalU26_t2817_DecimalU26_t2817,
	RuntimeInvoker_Byte_t454_Decimal_t738,
	RuntimeInvoker_SByte_t1124_Decimal_t738,
	RuntimeInvoker_Int16_t1125_Decimal_t738,
	RuntimeInvoker_UInt16_t960_Decimal_t738,
	RuntimeInvoker_UInt32_t467_Decimal_t738,
	RuntimeInvoker_Decimal_t738_SByte_t1124,
	RuntimeInvoker_Decimal_t738_Int16_t1125,
	RuntimeInvoker_Decimal_t738_Int32_t106,
	RuntimeInvoker_Decimal_t738_Int64_t466,
	RuntimeInvoker_Decimal_t738_Single_t113,
	RuntimeInvoker_Decimal_t738_Double_t465,
	RuntimeInvoker_Single_t113_Decimal_t738,
	RuntimeInvoker_Double_t465_Decimal_t738,
	RuntimeInvoker_Boolean_t455_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int64_t466,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t106_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t467,
	RuntimeInvoker_UInt64_t1123_IntPtr_t,
	RuntimeInvoker_UInt32_t467_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t466,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2818,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_UInt64_t1123_Object_t_Int32_t106,
	RuntimeInvoker_Object_t_Object_t_Int16_t1125,
	RuntimeInvoker_Object_t_Object_t_Int64_t466,
	RuntimeInvoker_Int64_t466_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Object_t_Int64_t466_Int64_t466,
	RuntimeInvoker_Object_t_Int64_t466_Int64_t466_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int64_t466_Int64_t466,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_Int64_t466_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t1399,
	RuntimeInvoker_MemberTypes_t1379,
	RuntimeInvoker_RuntimeTypeHandle_t1130,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_TypeCode_t736_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1130,
	RuntimeInvoker_RuntimeTypeHandle_t1130_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_RuntimeFieldHandle_t1132,
	RuntimeInvoker_Void_t1129_IntPtr_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_ContractionU5BU5DU26_t2819_Level2MapU5BU5DU26_t2820,
	RuntimeInvoker_Void_t1129_Object_t_CodePointIndexerU26_t2821_ByteU2AU26_t2813_ByteU2AU26_t2813_CodePointIndexerU26_t2821_ByteU2AU26_t2813,
	RuntimeInvoker_Byte_t454_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124,
	RuntimeInvoker_Byte_t454_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int32_t106,
	RuntimeInvoker_ExtenderType_t1176_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_Int32_t106_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_BooleanU26_t2794,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int16_t1125_Int32_t106_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_ContextU26_t2822,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Object_t_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32_t106_ContractionU26_t2823_ContextU26_t2822,
	RuntimeInvoker_Boolean_t455_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106_Object_t_SByte_t1124_ContextU26_t2822,
	RuntimeInvoker_Boolean_t455_Object_t_Int32U26_t2775_Int32_t106_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32_t106_ContractionU26_t2823_ContextU26_t2822,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Object_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_SByte_t1124_ByteU5BU5DU26_t2804_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_ConfidenceFactor_t1185,
	RuntimeInvoker_Sign_t1187_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t967_SByte_t1124,
	RuntimeInvoker_Void_t1129_DSAParameters_t967,
	RuntimeInvoker_Int16_t1125_Object_t_Int32_t106,
	RuntimeInvoker_Double_t465_Object_t_Int32_t106,
	RuntimeInvoker_Object_t_Int16_t1125_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Single_t113_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Int32_t106_Single_t113_Object_t,
	RuntimeInvoker_Boolean_t455_Int32_t106_SByte_t1124_MethodBaseU26_t2824_Int32U26_t2775_Int32U26_t2775_StringU26_t2792_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Int32_t106_DateTime_t308,
	RuntimeInvoker_DayOfWeek_t1700_DateTime_t308,
	RuntimeInvoker_Int32_t106_Int32U26_t2775_Int32_t106_Int32_t106,
	RuntimeInvoker_DayOfWeek_t1700_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32U26_t2775_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_DateTime_t308_DateTime_t308_TimeSpan_t846,
	RuntimeInvoker_TimeSpan_t846,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Object_t_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32U26_t2775,
	RuntimeInvoker_Decimal_t738,
	RuntimeInvoker_SByte_t1124,
	RuntimeInvoker_UInt16_t960,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_SByte_t1124_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_Int32_t106,
	RuntimeInvoker_Int32_t106_IntPtr_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t106_Int32_t106_MonoIOErrorU26_t2825,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_FileAttributes_t1285_Object_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_MonoFileType_t1294_IntPtr_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_Boolean_t455_Object_t_MonoIOStatU26_t2826_MonoIOErrorU26_t2825,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_MonoIOErrorU26_t2825,
	RuntimeInvoker_Boolean_t455_IntPtr_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_Int32_t106_IntPtr_t_Object_t_Int32_t106_Int32_t106_MonoIOErrorU26_t2825,
	RuntimeInvoker_Int64_t466_IntPtr_t_Int64_t466_Int32_t106_MonoIOErrorU26_t2825,
	RuntimeInvoker_Int64_t466_IntPtr_t_MonoIOErrorU26_t2825,
	RuntimeInvoker_Boolean_t455_IntPtr_t_Int64_t466_MonoIOErrorU26_t2825,
	RuntimeInvoker_Void_t1129_Object_t_Int32_t106_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1369,
	RuntimeInvoker_RuntimeMethodHandle_t1743,
	RuntimeInvoker_MethodAttributes_t1380,
	RuntimeInvoker_MethodToken_t1335,
	RuntimeInvoker_FieldAttributes_t1377,
	RuntimeInvoker_RuntimeFieldHandle_t1132,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_OpCode_t1339,
	RuntimeInvoker_Void_t1129_OpCode_t1339_Object_t,
	RuntimeInvoker_StackBehaviour_t1343,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_SByte_t1124_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2775_ModuleU26_t2827,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_AssemblyNameFlags_t1363,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_ObjectU5BU5DU26_t2805_Object_t_Object_t_Object_t_ObjectU26_t2828,
	RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_ObjectU5BU5DU26_t2805_Object_t,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_EventAttributes_t1375,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1132,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t441,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1743,
	RuntimeInvoker_Void_t1129_Object_t_MonoEventInfoU26_t2829,
	RuntimeInvoker_MonoEventInfo_t1384_Object_t,
	RuntimeInvoker_Void_t1129_IntPtr_t_MonoMethodInfoU26_t2830,
	RuntimeInvoker_MonoMethodInfo_t1387_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1380_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1369_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2806,
	RuntimeInvoker_Void_t1129_Object_t_MonoPropertyInfoU26_t2831_Int32_t106,
	RuntimeInvoker_PropertyAttributes_t1395,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int32_t106_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t1391,
	RuntimeInvoker_Void_t1129_Int64_t466_ResourceInfoU26_t2832,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Int32_t106,
	RuntimeInvoker_GCHandle_t1432_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Object_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Byte_t454_IntPtr_t_Int32_t106,
	RuntimeInvoker_Void_t1129_IntPtr_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Void_t1129_BooleanU26_t2794,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2792,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2792,
	RuntimeInvoker_Void_t1129_SByte_t1124_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_TimeSpan_t846,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_SByte_t1124_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t441_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t441_ISurrogateSelectorU26_t2833,
	RuntimeInvoker_Void_t1129_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t846_Object_t,
	RuntimeInvoker_Object_t_StringU26_t2792,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2828,
	RuntimeInvoker_Boolean_t455_Object_t_StringU26_t2792_StringU26_t2792,
	RuntimeInvoker_WellKnownObjectMode_t1538,
	RuntimeInvoker_StreamingContext_t441,
	RuntimeInvoker_TypeFilterLevel_t1554,
	RuntimeInvoker_Void_t1129_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Object_t_Byte_t454_Object_t_SByte_t1124_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t454_Object_t_SByte_t1124_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_ObjectU26_t2828_HeaderU5BU5DU26_t2834,
	RuntimeInvoker_Void_t1129_Byte_t454_Object_t_SByte_t1124_ObjectU26_t2828_HeaderU5BU5DU26_t2834,
	RuntimeInvoker_Boolean_t455_Byte_t454_Object_t,
	RuntimeInvoker_Void_t1129_Byte_t454_Object_t_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835,
	RuntimeInvoker_Void_t1129_Object_t_SByte_t1124_SByte_t1124_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835,
	RuntimeInvoker_Void_t1129_Object_t_Int64U26_t2807_ObjectU26_t2828_SerializationInfoU26_t2835,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466_ObjectU26_t2828_SerializationInfoU26_t2835,
	RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Object_t_Int64_t466_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int64U26_t2807_ObjectU26_t2828,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64U26_t2807_ObjectU26_t2828,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Int64_t466_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Int64_t466_Int64_t466_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t466_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t454,
	RuntimeInvoker_Void_t1129_Int64_t466_Int32_t106_Int64_t466,
	RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Int64_t466_Object_t_Int64_t466_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_SByte_t1124_Object_t_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_StreamingContext_t441,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_StreamingContext_t441,
	RuntimeInvoker_Void_t1129_StreamingContext_t441,
	RuntimeInvoker_Object_t_StreamingContext_t441_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_Int16_t1125,
	RuntimeInvoker_Void_t1129_Object_t_DateTime_t308,
	RuntimeInvoker_SerializationEntry_t1571,
	RuntimeInvoker_StreamingContextStates_t1574,
	RuntimeInvoker_CspProviderFlags_t1578,
	RuntimeInvoker_UInt32_t467_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Object_t_SByte_t1124,
	RuntimeInvoker_Void_t1129_Int64_t466_Object_t_Int32_t106,
	RuntimeInvoker_UInt32_t467_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_UInt32U26_t2808_Int32_t106_UInt32U26_t2808_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1129_Int64_t466_Int64_t466,
	RuntimeInvoker_UInt64_t1123_Int64_t466_Int32_t106,
	RuntimeInvoker_UInt64_t1123_Int64_t466_Int64_t466_Int64_t466,
	RuntimeInvoker_UInt64_t1123_Int64_t466,
	RuntimeInvoker_PaddingMode_t764,
	RuntimeInvoker_Void_t1129_StringBuilderU26_t2836_Int32_t106,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_EncoderFallbackBufferU26_t2837_CharU5BU5DU26_t2838,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_DecoderFallbackBufferU26_t2839,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int16_t1125_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int16_t1125_Int16_t1125_Int32_t106,
	RuntimeInvoker_Void_t1129_Int16_t1125_Int16_t1125_Int32_t106,
	RuntimeInvoker_Object_t_Int32U26_t2775,
	RuntimeInvoker_Object_t_Int32_t106_Object_t_Int32_t106,
	RuntimeInvoker_Void_t1129_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_SByte_t1124_Int32_t106_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_SByte_t1124_Int32U26_t2775_BooleanU26_t2794_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_CharU26_t2801_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_CharU26_t2801_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_Object_t_Int64_t466_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_Object_t_Int64_t466_Int32_t106_Object_t_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Object_t_Int32_t106_UInt32U26_t2808_UInt32U26_t2808_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Int32_t106_UInt32U26_t2808_UInt32U26_t2808_Object_t_DecoderFallbackBufferU26_t2839_ByteU5BU5DU26_t2804_SByte_t1124,
	RuntimeInvoker_Void_t1129_SByte_t1124_Int32_t106,
	RuntimeInvoker_IntPtr_t_SByte_t1124_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Boolean_t455_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t1124_SByte_t1124_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Boolean_t455_TimeSpan_t846_TimeSpan_t846,
	RuntimeInvoker_Boolean_t455_Int64_t466_Int64_t466_SByte_t1124,
	RuntimeInvoker_Boolean_t455_IntPtr_t_Int32_t106_SByte_t1124,
	RuntimeInvoker_Int64_t466_Double_t465,
	RuntimeInvoker_Object_t_Double_t465,
	RuntimeInvoker_Int64_t466_Object_t_Int32_t106,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t106_Int32_t106,
	RuntimeInvoker_Byte_t454_SByte_t1124,
	RuntimeInvoker_Byte_t454_Double_t465,
	RuntimeInvoker_Byte_t454_Single_t113,
	RuntimeInvoker_Byte_t454_Int64_t466,
	RuntimeInvoker_Char_t699_SByte_t1124,
	RuntimeInvoker_Char_t699_Int64_t466,
	RuntimeInvoker_Char_t699_Single_t113,
	RuntimeInvoker_Char_t699_Object_t_Object_t,
	RuntimeInvoker_DateTime_t308_Object_t_Object_t,
	RuntimeInvoker_DateTime_t308_Int16_t1125,
	RuntimeInvoker_DateTime_t308_Int32_t106,
	RuntimeInvoker_DateTime_t308_Int64_t466,
	RuntimeInvoker_DateTime_t308_Single_t113,
	RuntimeInvoker_DateTime_t308_SByte_t1124,
	RuntimeInvoker_Double_t465_SByte_t1124,
	RuntimeInvoker_Double_t465_Double_t465,
	RuntimeInvoker_Double_t465_Single_t113,
	RuntimeInvoker_Double_t465_Int32_t106,
	RuntimeInvoker_Double_t465_Int64_t466,
	RuntimeInvoker_Double_t465_Int16_t1125,
	RuntimeInvoker_Int16_t1125_SByte_t1124,
	RuntimeInvoker_Int16_t1125_Double_t465,
	RuntimeInvoker_Int16_t1125_Single_t113,
	RuntimeInvoker_Int16_t1125_Int32_t106,
	RuntimeInvoker_Int16_t1125_Int64_t466,
	RuntimeInvoker_Int64_t466_SByte_t1124,
	RuntimeInvoker_Int64_t466_Int16_t1125,
	RuntimeInvoker_Int64_t466_Single_t113,
	RuntimeInvoker_Int64_t466_Int64_t466,
	RuntimeInvoker_SByte_t1124_SByte_t1124,
	RuntimeInvoker_SByte_t1124_Int16_t1125,
	RuntimeInvoker_SByte_t1124_Double_t465,
	RuntimeInvoker_SByte_t1124_Single_t113,
	RuntimeInvoker_SByte_t1124_Int32_t106,
	RuntimeInvoker_SByte_t1124_Int64_t466,
	RuntimeInvoker_Single_t113_SByte_t1124,
	RuntimeInvoker_Single_t113_Double_t465,
	RuntimeInvoker_Single_t113_Int64_t466,
	RuntimeInvoker_Single_t113_Int16_t1125,
	RuntimeInvoker_UInt16_t960_SByte_t1124,
	RuntimeInvoker_UInt16_t960_Double_t465,
	RuntimeInvoker_UInt16_t960_Single_t113,
	RuntimeInvoker_UInt16_t960_Int32_t106,
	RuntimeInvoker_UInt16_t960_Int64_t466,
	RuntimeInvoker_UInt32_t467_SByte_t1124,
	RuntimeInvoker_UInt32_t467_Int16_t1125,
	RuntimeInvoker_UInt32_t467_Double_t465,
	RuntimeInvoker_UInt32_t467_Single_t113,
	RuntimeInvoker_UInt32_t467_Int64_t466,
	RuntimeInvoker_UInt64_t1123_SByte_t1124,
	RuntimeInvoker_UInt64_t1123_Int16_t1125,
	RuntimeInvoker_UInt64_t1123_Double_t465,
	RuntimeInvoker_UInt64_t1123_Single_t113,
	RuntimeInvoker_UInt64_t1123_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_SByte_t1124_TimeSpan_t846,
	RuntimeInvoker_Void_t1129_Int64_t466_Int32_t106,
	RuntimeInvoker_DayOfWeek_t1700,
	RuntimeInvoker_DateTimeKind_t1697,
	RuntimeInvoker_DateTime_t308_TimeSpan_t846,
	RuntimeInvoker_DateTime_t308_Double_t465,
	RuntimeInvoker_Int32_t106_DateTime_t308_DateTime_t308,
	RuntimeInvoker_DateTime_t308_DateTime_t308_Int32_t106,
	RuntimeInvoker_DateTime_t308_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Int32_t106_DateTimeU26_t2840_DateTimeOffsetU26_t2841_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_SByte_t1124_SByte_t1124_Int32U26_t2775,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Object_t_Object_t_SByte_t1124_Int32U26_t2775,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_Int32U26_t2775,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Boolean_t455_Object_t_Int32_t106_Object_t_SByte_t1124_Int32U26_t2775,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_SByte_t1124_DateTimeU26_t2840_DateTimeOffsetU26_t2841_Object_t_Int32_t106_SByte_t1124_BooleanU26_t2794_BooleanU26_t2794,
	RuntimeInvoker_DateTime_t308_Object_t_Object_t_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_Object_t_Object_t_Int32_t106_DateTimeU26_t2840_SByte_t1124_BooleanU26_t2794_SByte_t1124_ExceptionU26_t2806,
	RuntimeInvoker_DateTime_t308_DateTime_t308_TimeSpan_t846,
	RuntimeInvoker_Boolean_t455_DateTime_t308_DateTime_t308,
	RuntimeInvoker_Void_t1129_DateTime_t308_TimeSpan_t846,
	RuntimeInvoker_Void_t1129_Int64_t466_TimeSpan_t846,
	RuntimeInvoker_Int32_t106_DateTimeOffset_t1698,
	RuntimeInvoker_Boolean_t455_DateTimeOffset_t1698,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int16_t1125,
	RuntimeInvoker_Object_t_Int16_t1125_Object_t_BooleanU26_t2794_BooleanU26_t2794,
	RuntimeInvoker_Object_t_Int16_t1125_Object_t_BooleanU26_t2794_BooleanU26_t2794_SByte_t1124,
	RuntimeInvoker_Object_t_DateTime_t308_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t308_Nullable_1_t1802_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_MonoEnumInfo_t1711,
	RuntimeInvoker_Void_t1129_Object_t_MonoEnumInfoU26_t2842,
	RuntimeInvoker_Int32_t106_Int16_t1125_Int16_t1125,
	RuntimeInvoker_Int32_t106_Int64_t466_Int64_t466,
	RuntimeInvoker_PlatformID_t1740,
	RuntimeInvoker_Void_t1129_Int32_t106_Int16_t1125_Int16_t1125_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Int32_t106_Guid_t1720,
	RuntimeInvoker_Boolean_t455_Guid_t1720,
	RuntimeInvoker_Guid_t1720,
	RuntimeInvoker_Object_t_SByte_t1124_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Double_t465_Double_t465_Double_t465,
	RuntimeInvoker_TypeAttributes_t1399_Object_t,
	RuntimeInvoker_Object_t_SByte_t1124_SByte_t1124,
	RuntimeInvoker_Void_t1129_UInt64U2AU26_t2843_Int32U2AU26_t2844_CharU2AU26_t2845_CharU2AU26_t2845_Int64U2AU26_t2846_Int32U2AU26_t2844,
	RuntimeInvoker_Void_t1129_Int32_t106_Int64_t466,
	RuntimeInvoker_Void_t1129_Object_t_Double_t465_Int32_t106,
	RuntimeInvoker_Void_t1129_Object_t_Decimal_t738,
	RuntimeInvoker_Object_t_Object_t_SByte_t1124_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1125_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t466_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t113_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t465_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t738_Object_t,
	RuntimeInvoker_Object_t_Single_t113_Object_t,
	RuntimeInvoker_Object_t_Double_t465_Object_t,
	RuntimeInvoker_Void_t1129_Object_t_BooleanU26_t2794_SByte_t1124_Int32U26_t2775_Int32U26_t2775,
	RuntimeInvoker_Object_t_Object_t_Int32_t106_Int32_t106_Object_t_SByte_t1124_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1129_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_Int64_t466_Int32_t106_Int32_t106_Int32_t106_Int32_t106_Int32_t106,
	RuntimeInvoker_TimeSpan_t846_TimeSpan_t846,
	RuntimeInvoker_Int32_t106_TimeSpan_t846_TimeSpan_t846,
	RuntimeInvoker_Int32_t106_TimeSpan_t846,
	RuntimeInvoker_Boolean_t455_TimeSpan_t846,
	RuntimeInvoker_TimeSpan_t846_Double_t465,
	RuntimeInvoker_TimeSpan_t846_Double_t465_Int64_t466,
	RuntimeInvoker_TimeSpan_t846_TimeSpan_t846_TimeSpan_t846,
	RuntimeInvoker_TimeSpan_t846_DateTime_t308,
	RuntimeInvoker_Boolean_t455_DateTime_t308_Object_t,
	RuntimeInvoker_DateTime_t308_DateTime_t308,
	RuntimeInvoker_TimeSpan_t846_DateTime_t308_TimeSpan_t846,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int64U5BU5DU26_t2847_StringU5BU5DU26_t2848,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_ObjectU26_t2828_Object_t,
	RuntimeInvoker_Void_t1129_ObjectU26_t2828_Object_t,
	RuntimeInvoker_Boolean_t455_Int32_t106_Object_t,
	RuntimeInvoker_Enumerator_t2337,
	RuntimeInvoker_Enumerator_t1896,
	RuntimeInvoker_Void_t1129_Int32_t106_ObjectU26_t2828,
	RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Int32_t106,
	RuntimeInvoker_Void_t1129_ObjectU5BU5DU26_t2805_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2086,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2086,
	RuntimeInvoker_KeyValuePair_2_t2086_Object_t_Object_t,
	RuntimeInvoker_Boolean_t455_Object_t_ObjectU26_t2828,
	RuntimeInvoker_Enumerator_t2090,
	RuntimeInvoker_DictionaryEntry_t943_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2086,
	RuntimeInvoker_Enumerator_t2089,
	RuntimeInvoker_Int32_t106_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Enumerator_t1855,
	RuntimeInvoker_Void_t1129_Object_t_Object_t_Single_t113,
	RuntimeInvoker_Enumerator_t2069,
	RuntimeInvoker_Enumerator_t2070,
	RuntimeInvoker_KeyValuePair_2_t2066,
	RuntimeInvoker_Void_t1129_FloatTween_t535,
	RuntimeInvoker_Void_t1129_ColorTween_t532,
	RuntimeInvoker_Boolean_t455_TypeU26_t2849_Int32_t106,
	RuntimeInvoker_Boolean_t455_BooleanU26_t2794_SByte_t1124,
	RuntimeInvoker_Boolean_t455_FillMethodU26_t2850_Int32_t106,
	RuntimeInvoker_Boolean_t455_SingleU26_t2774_Single_t113,
	RuntimeInvoker_Boolean_t455_ContentTypeU26_t2851_Int32_t106,
	RuntimeInvoker_Boolean_t455_LineTypeU26_t2852_Int32_t106,
	RuntimeInvoker_Boolean_t455_InputTypeU26_t2853_Int32_t106,
	RuntimeInvoker_Boolean_t455_TouchScreenKeyboardTypeU26_t2854_Int32_t106,
	RuntimeInvoker_Boolean_t455_CharacterValidationU26_t2855_Int32_t106,
	RuntimeInvoker_Boolean_t455_CharU26_t2801_Int16_t1125,
	RuntimeInvoker_Boolean_t455_DirectionU26_t2856_Int32_t106,
	RuntimeInvoker_Boolean_t455_NavigationU26_t2857_Navigation_t594,
	RuntimeInvoker_Boolean_t455_TransitionU26_t2858_Int32_t106,
	RuntimeInvoker_Boolean_t455_ColorBlockU26_t2859_ColorBlock_t546,
	RuntimeInvoker_Boolean_t455_SpriteStateU26_t2860_SpriteState_t611,
	RuntimeInvoker_Boolean_t455_DirectionU26_t2861_Int32_t106,
	RuntimeInvoker_Boolean_t455_AspectModeU26_t2862_Int32_t106,
	RuntimeInvoker_Boolean_t455_FitModeU26_t2863_Int32_t106,
	RuntimeInvoker_Void_t1129_CornerU26_t2864_Int32_t106,
	RuntimeInvoker_Void_t1129_AxisU26_t2865_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector2U26_t2785_Vector2_t15,
	RuntimeInvoker_Void_t1129_ConstraintU26_t2866_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32U26_t2775_Int32_t106,
	RuntimeInvoker_Void_t1129_SingleU26_t2774_Single_t113,
	RuntimeInvoker_Void_t1129_BooleanU26_t2794_SByte_t1124,
	RuntimeInvoker_Void_t1129_TextAnchorU26_t2867_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_Double_t465,
	RuntimeInvoker_RaycastResult_t94_Int32_t106,
	RuntimeInvoker_Boolean_t455_RaycastResult_t94,
	RuntimeInvoker_Int32_t106_RaycastResult_t94,
	RuntimeInvoker_Void_t1129_Int32_t106_RaycastResult_t94,
	RuntimeInvoker_Void_t1129_RaycastResultU5BU5DU26_t2868_Int32_t106,
	RuntimeInvoker_Void_t1129_RaycastResultU5BU5DU26_t2868_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_RaycastResult_t94_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_RaycastResult_t94_RaycastResult_t94_Object_t,
	RuntimeInvoker_GcAchievementData_t347_Int32_t106,
	RuntimeInvoker_Void_t1129_GcAchievementData_t347,
	RuntimeInvoker_Boolean_t455_GcAchievementData_t347,
	RuntimeInvoker_Int32_t106_GcAchievementData_t347,
	RuntimeInvoker_Void_t1129_Int32_t106_GcAchievementData_t347,
	RuntimeInvoker_GcScoreData_t348_Int32_t106,
	RuntimeInvoker_Boolean_t455_GcScoreData_t348,
	RuntimeInvoker_Int32_t106_GcScoreData_t348,
	RuntimeInvoker_Void_t1129_Int32_t106_GcScoreData_t348,
	RuntimeInvoker_Int32_t106_Vector3_t3,
	RuntimeInvoker_Void_t1129_Int32_t106_Vector3_t3,
	RuntimeInvoker_Void_t1129_Vector4_t90,
	RuntimeInvoker_Boolean_t455_Vector4_t90,
	RuntimeInvoker_Int32_t106_Vector4_t90,
	RuntimeInvoker_Boolean_t455_Vector2_t15,
	RuntimeInvoker_Void_t1129_Int32_t106_Vector2_t15,
	RuntimeInvoker_Color32_t187_Int32_t106,
	RuntimeInvoker_Void_t1129_Color32_t187,
	RuntimeInvoker_Boolean_t455_Color32_t187,
	RuntimeInvoker_Int32_t106_Color32_t187,
	RuntimeInvoker_Void_t1129_Int32_t106_Color32_t187,
	RuntimeInvoker_Void_t1129_Vector3U5BU5DU26_t2869_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector3U5BU5DU26_t2869_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Vector3_t3_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Vector3_t3_Vector3_t3_Object_t,
	RuntimeInvoker_Void_t1129_Vector4U5BU5DU26_t2870_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector4U5BU5DU26_t2870_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Vector4_t90_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Vector4_t90_Vector4_t90_Object_t,
	RuntimeInvoker_Void_t1129_Vector2U5BU5DU26_t2871_Int32_t106,
	RuntimeInvoker_Void_t1129_Vector2U5BU5DU26_t2871_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Vector2_t15_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Vector2_t15_Vector2_t15_Object_t,
	RuntimeInvoker_Void_t1129_Color32U5BU5DU26_t2872_Int32_t106,
	RuntimeInvoker_Void_t1129_Color32U5BU5DU26_t2872_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_Color32_t187_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Color32_t187_Color32_t187_Object_t,
	RuntimeInvoker_Void_t1129_Int32U5BU5DU26_t2873_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32U5BU5DU26_t2873_Int32_t106_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_IntPtr_t,
	RuntimeInvoker_ContactPoint_t247_Int32_t106,
	RuntimeInvoker_Void_t1129_ContactPoint_t247,
	RuntimeInvoker_Boolean_t455_ContactPoint_t247,
	RuntimeInvoker_Int32_t106_ContactPoint_t247,
	RuntimeInvoker_Void_t1129_Int32_t106_ContactPoint_t247,
	RuntimeInvoker_RaycastHit_t78_Int32_t106,
	RuntimeInvoker_Void_t1129_RaycastHit_t78,
	RuntimeInvoker_Boolean_t455_RaycastHit_t78,
	RuntimeInvoker_Int32_t106_RaycastHit_t78,
	RuntimeInvoker_Void_t1129_Int32_t106_RaycastHit_t78,
	RuntimeInvoker_RaycastHit2D_t252_Int32_t106,
	RuntimeInvoker_Void_t1129_RaycastHit2D_t252,
	RuntimeInvoker_Boolean_t455_RaycastHit2D_t252,
	RuntimeInvoker_Int32_t106_RaycastHit2D_t252,
	RuntimeInvoker_Void_t1129_Int32_t106_RaycastHit2D_t252,
	RuntimeInvoker_ContactPoint2D_t255_Int32_t106,
	RuntimeInvoker_Void_t1129_ContactPoint2D_t255,
	RuntimeInvoker_Boolean_t455_ContactPoint2D_t255,
	RuntimeInvoker_Int32_t106_ContactPoint2D_t255,
	RuntimeInvoker_Void_t1129_Int32_t106_ContactPoint2D_t255,
	RuntimeInvoker_Keyframe_t269_Int32_t106,
	RuntimeInvoker_Void_t1129_Keyframe_t269,
	RuntimeInvoker_Boolean_t455_Keyframe_t269,
	RuntimeInvoker_Int32_t106_Keyframe_t269,
	RuntimeInvoker_Void_t1129_Int32_t106_Keyframe_t269,
	RuntimeInvoker_CharacterInfo_t281_Int32_t106,
	RuntimeInvoker_Void_t1129_CharacterInfo_t281,
	RuntimeInvoker_Boolean_t455_CharacterInfo_t281,
	RuntimeInvoker_Int32_t106_CharacterInfo_t281,
	RuntimeInvoker_Void_t1129_Int32_t106_CharacterInfo_t281,
	RuntimeInvoker_UIVertex_t296_Int32_t106,
	RuntimeInvoker_Boolean_t455_UIVertex_t296,
	RuntimeInvoker_Int32_t106_UIVertex_t296,
	RuntimeInvoker_Void_t1129_Int32_t106_UIVertex_t296,
	RuntimeInvoker_Void_t1129_UIVertexU5BU5DU26_t2874_Int32_t106,
	RuntimeInvoker_Void_t1129_UIVertexU5BU5DU26_t2874_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_UIVertex_t296_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_UIVertex_t296_UIVertex_t296_Object_t,
	RuntimeInvoker_UICharInfo_t285_Int32_t106,
	RuntimeInvoker_Void_t1129_UICharInfo_t285,
	RuntimeInvoker_Boolean_t455_UICharInfo_t285,
	RuntimeInvoker_Int32_t106_UICharInfo_t285,
	RuntimeInvoker_Void_t1129_Int32_t106_UICharInfo_t285,
	RuntimeInvoker_Void_t1129_UICharInfoU5BU5DU26_t2875_Int32_t106,
	RuntimeInvoker_Void_t1129_UICharInfoU5BU5DU26_t2875_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_UICharInfo_t285_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_UICharInfo_t285_UICharInfo_t285_Object_t,
	RuntimeInvoker_UILineInfo_t286_Int32_t106,
	RuntimeInvoker_Void_t1129_UILineInfo_t286,
	RuntimeInvoker_Boolean_t455_UILineInfo_t286,
	RuntimeInvoker_Int32_t106_UILineInfo_t286,
	RuntimeInvoker_Void_t1129_Int32_t106_UILineInfo_t286,
	RuntimeInvoker_Void_t1129_UILineInfoU5BU5DU26_t2876_Int32_t106,
	RuntimeInvoker_Void_t1129_UILineInfoU5BU5DU26_t2876_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_UILineInfo_t286_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_UILineInfo_t286_UILineInfo_t286_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2047_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2047,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2047,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2047,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2047,
	RuntimeInvoker_Link_t1229_Int32_t106,
	RuntimeInvoker_Void_t1129_Link_t1229,
	RuntimeInvoker_Boolean_t455_Link_t1229,
	RuntimeInvoker_Int32_t106_Link_t1229,
	RuntimeInvoker_Void_t1129_Int32_t106_Link_t1229,
	RuntimeInvoker_DictionaryEntry_t943_Int32_t106,
	RuntimeInvoker_Void_t1129_DictionaryEntry_t943,
	RuntimeInvoker_Boolean_t455_DictionaryEntry_t943,
	RuntimeInvoker_Int32_t106_DictionaryEntry_t943,
	RuntimeInvoker_Void_t1129_Int32_t106_DictionaryEntry_t943,
	RuntimeInvoker_KeyValuePair_2_t2066_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2066,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2066,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2066,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2066,
	RuntimeInvoker_KeyValuePair_2_t2086_Int32_t106,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2086,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2086,
	RuntimeInvoker_ParameterModifier_t1392_Int32_t106,
	RuntimeInvoker_Void_t1129_ParameterModifier_t1392,
	RuntimeInvoker_Boolean_t455_ParameterModifier_t1392,
	RuntimeInvoker_Int32_t106_ParameterModifier_t1392,
	RuntimeInvoker_Void_t1129_Int32_t106_ParameterModifier_t1392,
	RuntimeInvoker_HitInfo_t368_Int32_t106,
	RuntimeInvoker_Void_t1129_HitInfo_t368,
	RuntimeInvoker_Int32_t106_HitInfo_t368,
	RuntimeInvoker_KeyValuePair_2_t2114_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2114,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2114,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2114,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2114,
	RuntimeInvoker_TextEditOp_t387_Int32_t106,
	RuntimeInvoker_ContentType_t575_Int32_t106,
	RuntimeInvoker_KeyValuePair_2_t2342_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2342,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2342,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2342,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2342,
	RuntimeInvoker_X509ChainStatus_t843_Int32_t106,
	RuntimeInvoker_Void_t1129_X509ChainStatus_t843,
	RuntimeInvoker_Boolean_t455_X509ChainStatus_t843,
	RuntimeInvoker_Int32_t106_X509ChainStatus_t843,
	RuntimeInvoker_Void_t1129_Int32_t106_X509ChainStatus_t843,
	RuntimeInvoker_KeyValuePair_2_t2362_Int32_t106,
	RuntimeInvoker_Void_t1129_KeyValuePair_2_t2362,
	RuntimeInvoker_Boolean_t455_KeyValuePair_2_t2362,
	RuntimeInvoker_Int32_t106_KeyValuePair_2_t2362,
	RuntimeInvoker_Void_t1129_Int32_t106_KeyValuePair_2_t2362,
	RuntimeInvoker_Int32_t106_Object_t_Int32_t106_Int32_t106_Int32_t106_Object_t,
	RuntimeInvoker_Mark_t893_Int32_t106,
	RuntimeInvoker_Void_t1129_Mark_t893,
	RuntimeInvoker_Boolean_t455_Mark_t893,
	RuntimeInvoker_Int32_t106_Mark_t893,
	RuntimeInvoker_Void_t1129_Int32_t106_Mark_t893,
	RuntimeInvoker_UriScheme_t930_Int32_t106,
	RuntimeInvoker_Void_t1129_UriScheme_t930,
	RuntimeInvoker_Boolean_t455_UriScheme_t930,
	RuntimeInvoker_Int32_t106_UriScheme_t930,
	RuntimeInvoker_Void_t1129_Int32_t106_UriScheme_t930,
	RuntimeInvoker_ClientCertificateType_t1074_Int32_t106,
	RuntimeInvoker_TableRange_t1162_Int32_t106,
	RuntimeInvoker_Void_t1129_TableRange_t1162,
	RuntimeInvoker_Boolean_t455_TableRange_t1162,
	RuntimeInvoker_Int32_t106_TableRange_t1162,
	RuntimeInvoker_Void_t1129_Int32_t106_TableRange_t1162,
	RuntimeInvoker_Slot_t1239_Int32_t106,
	RuntimeInvoker_Void_t1129_Slot_t1239,
	RuntimeInvoker_Boolean_t455_Slot_t1239,
	RuntimeInvoker_Int32_t106_Slot_t1239,
	RuntimeInvoker_Void_t1129_Int32_t106_Slot_t1239,
	RuntimeInvoker_Slot_t1247_Int32_t106,
	RuntimeInvoker_Void_t1129_Slot_t1247,
	RuntimeInvoker_Boolean_t455_Slot_t1247,
	RuntimeInvoker_Int32_t106_Slot_t1247,
	RuntimeInvoker_Void_t1129_Int32_t106_Slot_t1247,
	RuntimeInvoker_ILTokenInfo_t1326_Int32_t106,
	RuntimeInvoker_Void_t1129_ILTokenInfo_t1326,
	RuntimeInvoker_Boolean_t455_ILTokenInfo_t1326,
	RuntimeInvoker_Int32_t106_ILTokenInfo_t1326,
	RuntimeInvoker_Void_t1129_Int32_t106_ILTokenInfo_t1326,
	RuntimeInvoker_LabelData_t1328_Int32_t106,
	RuntimeInvoker_Void_t1129_LabelData_t1328,
	RuntimeInvoker_Boolean_t455_LabelData_t1328,
	RuntimeInvoker_Int32_t106_LabelData_t1328,
	RuntimeInvoker_Void_t1129_Int32_t106_LabelData_t1328,
	RuntimeInvoker_LabelFixup_t1327_Int32_t106,
	RuntimeInvoker_Void_t1129_LabelFixup_t1327,
	RuntimeInvoker_Boolean_t455_LabelFixup_t1327,
	RuntimeInvoker_Int32_t106_LabelFixup_t1327,
	RuntimeInvoker_Void_t1129_Int32_t106_LabelFixup_t1327,
	RuntimeInvoker_CustomAttributeTypedArgument_t1374_Int32_t106,
	RuntimeInvoker_Void_t1129_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Boolean_t455_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Void_t1129_Int32_t106_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_CustomAttributeNamedArgument_t1373_Int32_t106,
	RuntimeInvoker_Void_t1129_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Boolean_t455_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Void_t1129_Int32_t106_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Void_t1129_CustomAttributeTypedArgumentU5BU5DU26_t2877_Int32_t106,
	RuntimeInvoker_Void_t1129_CustomAttributeTypedArgumentU5BU5DU26_t2877_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_CustomAttributeTypedArgument_t1374_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Void_t1129_CustomAttributeNamedArgumentU5BU5DU26_t2878_Int32_t106,
	RuntimeInvoker_Void_t1129_CustomAttributeNamedArgumentU5BU5DU26_t2878_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_Object_t_CustomAttributeNamedArgument_t1373_Int32_t106_Int32_t106,
	RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373_Object_t,
	RuntimeInvoker_Int32_t106_Object_t_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_ResourceInfo_t1403_Int32_t106,
	RuntimeInvoker_Void_t1129_ResourceInfo_t1403,
	RuntimeInvoker_Boolean_t455_ResourceInfo_t1403,
	RuntimeInvoker_Int32_t106_ResourceInfo_t1403,
	RuntimeInvoker_Void_t1129_Int32_t106_ResourceInfo_t1403,
	RuntimeInvoker_ResourceCacheItem_t1404_Int32_t106,
	RuntimeInvoker_Void_t1129_ResourceCacheItem_t1404,
	RuntimeInvoker_Boolean_t455_ResourceCacheItem_t1404,
	RuntimeInvoker_Int32_t106_ResourceCacheItem_t1404,
	RuntimeInvoker_Void_t1129_Int32_t106_ResourceCacheItem_t1404,
	RuntimeInvoker_Void_t1129_Int32_t106_DateTime_t308,
	RuntimeInvoker_Void_t1129_Decimal_t738,
	RuntimeInvoker_Void_t1129_Int32_t106_Decimal_t738,
	RuntimeInvoker_TimeSpan_t846_Int32_t106,
	RuntimeInvoker_Void_t1129_Int32_t106_TimeSpan_t846,
	RuntimeInvoker_TypeTag_t1542_Int32_t106,
	RuntimeInvoker_Boolean_t455_Byte_t454,
	RuntimeInvoker_Int32_t106_Byte_t454,
	RuntimeInvoker_Void_t1129_Int32_t106_Byte_t454,
	RuntimeInvoker_Enumerator_t1876,
	RuntimeInvoker_Boolean_t455_RaycastResult_t94_RaycastResult_t94,
	RuntimeInvoker_Object_t_RaycastResult_t94_Object_t_Object_t,
	RuntimeInvoker_Object_t_RaycastResult_t94_RaycastResult_t94_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1921,
	RuntimeInvoker_GcAchievementData_t347,
	RuntimeInvoker_GcScoreData_t348,
	RuntimeInvoker_Color32_t187,
	RuntimeInvoker_Vector3_t3_Object_t,
	RuntimeInvoker_Enumerator_t1943,
	RuntimeInvoker_Object_t_Vector3_t3_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Vector3_t3_Vector3_t3,
	RuntimeInvoker_Object_t_Vector3_t3_Vector3_t3_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1953,
	RuntimeInvoker_Object_t_Vector4_t90_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Vector4_t90_Vector4_t90,
	RuntimeInvoker_Object_t_Vector4_t90_Vector4_t90_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1963,
	RuntimeInvoker_Object_t_Vector2_t15_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Vector2_t15_Vector2_t15,
	RuntimeInvoker_Object_t_Vector2_t15_Vector2_t15_Object_t_Object_t,
	RuntimeInvoker_Color32_t187_Object_t,
	RuntimeInvoker_Enumerator_t1973,
	RuntimeInvoker_Boolean_t455_Color32_t187_Color32_t187,
	RuntimeInvoker_Object_t_Color32_t187_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_Color32_t187_Color32_t187,
	RuntimeInvoker_Object_t_Color32_t187_Color32_t187_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1983,
	RuntimeInvoker_ContactPoint_t247,
	RuntimeInvoker_RaycastHit_t78,
	RuntimeInvoker_RaycastHit2D_t252,
	RuntimeInvoker_ContactPoint2D_t255,
	RuntimeInvoker_Keyframe_t269,
	RuntimeInvoker_CharacterInfo_t281,
	RuntimeInvoker_UIVertex_t296,
	RuntimeInvoker_UIVertex_t296_Object_t,
	RuntimeInvoker_Enumerator_t2012,
	RuntimeInvoker_Boolean_t455_UIVertex_t296_UIVertex_t296,
	RuntimeInvoker_Object_t_UIVertex_t296_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_UIVertex_t296_UIVertex_t296,
	RuntimeInvoker_Object_t_UIVertex_t296_UIVertex_t296_Object_t_Object_t,
	RuntimeInvoker_UICharInfo_t285,
	RuntimeInvoker_UICharInfo_t285_Object_t,
	RuntimeInvoker_Enumerator_t2022,
	RuntimeInvoker_Boolean_t455_UICharInfo_t285_UICharInfo_t285,
	RuntimeInvoker_Object_t_UICharInfo_t285_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_UICharInfo_t285_UICharInfo_t285,
	RuntimeInvoker_Object_t_UICharInfo_t285_UICharInfo_t285_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t286,
	RuntimeInvoker_UILineInfo_t286_Object_t,
	RuntimeInvoker_Enumerator_t2032,
	RuntimeInvoker_Boolean_t455_UILineInfo_t286_UILineInfo_t286,
	RuntimeInvoker_Object_t_UILineInfo_t286_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_UILineInfo_t286_UILineInfo_t286,
	RuntimeInvoker_Object_t_UILineInfo_t286_UILineInfo_t286_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2047_Object_t_Int32_t106,
	RuntimeInvoker_Enumerator_t2052,
	RuntimeInvoker_DictionaryEntry_t943_Object_t_Int32_t106,
	RuntimeInvoker_KeyValuePair_2_t2047,
	RuntimeInvoker_Link_t1229,
	RuntimeInvoker_Enumerator_t2051,
	RuntimeInvoker_DictionaryEntry_t943_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2047_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2066_Int32_t106_Object_t,
	RuntimeInvoker_Boolean_t455_Int32_t106_ObjectU26_t2828,
	RuntimeInvoker_DictionaryEntry_t943_Int32_t106_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2066_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2086_Object_t,
	RuntimeInvoker_ParameterModifier_t1392,
	RuntimeInvoker_HitInfo_t368,
	RuntimeInvoker_TextEditOp_t387_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2114_Object_t_Int32_t106,
	RuntimeInvoker_TextEditOp_t387_Object_t_Int32_t106,
	RuntimeInvoker_Boolean_t455_Object_t_TextEditOpU26_t2879,
	RuntimeInvoker_Enumerator_t2119,
	RuntimeInvoker_KeyValuePair_2_t2114,
	RuntimeInvoker_TextEditOp_t387,
	RuntimeInvoker_Enumerator_t2118,
	RuntimeInvoker_KeyValuePair_2_t2114_Object_t,
	RuntimeInvoker_Object_t_Single_t113_Object_t_Object_t,
	RuntimeInvoker_Object_t_RaycastHit_t78_RaycastHit_t78_Object_t_Object_t,
	RuntimeInvoker_Object_t_Color_t11_Object_t_Object_t,
	RuntimeInvoker_Object_t_FloatTween_t535,
	RuntimeInvoker_Object_t_ColorTween_t532,
	RuntimeInvoker_KeyValuePair_2_t2342_Object_t_SByte_t1124,
	RuntimeInvoker_Boolean_t455_Object_t_BooleanU26_t2794,
	RuntimeInvoker_Enumerator_t2347,
	RuntimeInvoker_DictionaryEntry_t943_Object_t_SByte_t1124,
	RuntimeInvoker_KeyValuePair_2_t2342,
	RuntimeInvoker_Enumerator_t2346,
	RuntimeInvoker_KeyValuePair_2_t2342_Object_t,
	RuntimeInvoker_Boolean_t455_SByte_t1124_SByte_t1124,
	RuntimeInvoker_X509ChainStatus_t843,
	RuntimeInvoker_KeyValuePair_2_t2362_Int32_t106_Int32_t106,
	RuntimeInvoker_Boolean_t455_Int32_t106_Int32U26_t2775,
	RuntimeInvoker_Enumerator_t2366,
	RuntimeInvoker_DictionaryEntry_t943_Int32_t106_Int32_t106,
	RuntimeInvoker_KeyValuePair_2_t2362,
	RuntimeInvoker_Enumerator_t2365,
	RuntimeInvoker_KeyValuePair_2_t2362_Object_t,
	RuntimeInvoker_Mark_t893,
	RuntimeInvoker_UriScheme_t930,
	RuntimeInvoker_ClientCertificateType_t1074,
	RuntimeInvoker_TableRange_t1162,
	RuntimeInvoker_Slot_t1239,
	RuntimeInvoker_Slot_t1247,
	RuntimeInvoker_ILTokenInfo_t1326,
	RuntimeInvoker_LabelData_t1328,
	RuntimeInvoker_LabelFixup_t1327,
	RuntimeInvoker_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_CustomAttributeTypedArgument_t1374_Object_t,
	RuntimeInvoker_Enumerator_t2414,
	RuntimeInvoker_Boolean_t455_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1374_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1374_CustomAttributeTypedArgument_t1374_Object_t_Object_t,
	RuntimeInvoker_CustomAttributeNamedArgument_t1373_Object_t,
	RuntimeInvoker_Enumerator_t2425,
	RuntimeInvoker_Boolean_t455_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1373_Object_t_Object_t,
	RuntimeInvoker_Int32_t106_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1373_CustomAttributeNamedArgument_t1373_Object_t_Object_t,
	RuntimeInvoker_ResourceInfo_t1403,
	RuntimeInvoker_ResourceCacheItem_t1404,
	RuntimeInvoker_TypeTag_t1542,
	RuntimeInvoker_Int32_t106_DateTimeOffset_t1698_DateTimeOffset_t1698,
	RuntimeInvoker_Boolean_t455_DateTimeOffset_t1698_DateTimeOffset_t1698,
	RuntimeInvoker_Boolean_t455_Nullable_1_t1802,
	RuntimeInvoker_Int32_t106_Guid_t1720_Guid_t1720,
	RuntimeInvoker_Boolean_t455_Guid_t1720_Guid_t1720,
};
