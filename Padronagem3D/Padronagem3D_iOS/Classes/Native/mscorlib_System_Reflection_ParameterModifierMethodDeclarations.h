﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void ParameterModifier_t1392_marshal(const ParameterModifier_t1392& unmarshaled, ParameterModifier_t1392_marshaled& marshaled);
extern "C" void ParameterModifier_t1392_marshal_back(const ParameterModifier_t1392_marshaled& marshaled, ParameterModifier_t1392& unmarshaled);
extern "C" void ParameterModifier_t1392_marshal_cleanup(ParameterModifier_t1392_marshaled& marshaled);
