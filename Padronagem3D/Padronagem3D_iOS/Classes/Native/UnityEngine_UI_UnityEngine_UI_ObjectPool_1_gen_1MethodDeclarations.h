﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m11719(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t1906 *, UnityAction_1_t1907 *, UnityAction_1_t1907 *, const MethodInfo*))ObjectPool_1__ctor_m11576_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::get_countAll()
#define ObjectPool_1_get_countAll_m11720(__this, method) (( int32_t (*) (ObjectPool_1_t1906 *, const MethodInfo*))ObjectPool_1_get_countAll_m11578_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m11721(__this, ___value, method) (( void (*) (ObjectPool_1_t1906 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11580_gshared)(__this, ___value, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::Get()
#define ObjectPool_1_Get_m11722(__this, method) (( List_1_t703 * (*) (ObjectPool_1_t1906 *, const MethodInfo*))ObjectPool_1_Get_m11582_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::Release(T)
#define ObjectPool_1_Release_m11723(__this, ___element, method) (( void (*) (ObjectPool_1_t1906 *, List_1_t703 *, const MethodInfo*))ObjectPool_1_Release_m11584_gshared)(__this, ___element, method)
