﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
extern "C" Color_t11  ColorBlock_get_normalColor_m2548 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
extern "C" Color_t11  ColorBlock_get_highlightedColor_m2549 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
extern "C" Color_t11  ColorBlock_get_pressedColor_m2550 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
extern "C" Color_t11  ColorBlock_get_disabledColor_m2551 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
extern "C" float ColorBlock_get_colorMultiplier_m2552 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
extern "C" void ColorBlock_set_colorMultiplier_m2553 (ColorBlock_t546 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
extern "C" float ColorBlock_get_fadeDuration_m2554 (ColorBlock_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
extern "C" void ColorBlock_set_fadeDuration_m2555 (ColorBlock_t546 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
extern "C" ColorBlock_t546  ColorBlock_get_defaultColorBlock_m2556 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
