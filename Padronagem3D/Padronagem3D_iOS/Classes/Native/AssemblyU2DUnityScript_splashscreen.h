﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUITexture
struct GUITexture_t141;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// splashscreen
struct  splashscreen_t136  : public MonoBehaviour_t2
{
	// UnityEngine.GUITexture splashscreen::guiObject
	GUITexture_t141 * ___guiObject_2;
	// System.Single splashscreen::fadeTime
	float ___fadeTime_3;
	// System.Int32 splashscreen::nextscene
	int32_t ___nextscene_4;
};
