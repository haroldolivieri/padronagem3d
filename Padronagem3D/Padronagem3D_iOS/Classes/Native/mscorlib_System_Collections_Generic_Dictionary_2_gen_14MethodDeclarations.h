﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2112;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2046;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t2508;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t2509;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t942;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t2117;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void Dictionary_2__ctor_m14291_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m14291(__this, method) (( void (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2__ctor_m14291_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m14293_gshared (Dictionary_2_t2112 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m14293(__this, ___comparer, method) (( void (*) (Dictionary_2_t2112 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14293_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m14295_gshared (Dictionary_2_t2112 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m14295(__this, ___capacity, method) (( void (*) (Dictionary_2_t2112 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14295_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m14297_gshared (Dictionary_2_t2112 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m14297(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2112 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m14297_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m14299_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14299(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14299_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m14301_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14301(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14301_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m14303_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m14303(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14303_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m14305_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m14305(__this, ___key, method) (( bool (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14305_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m14307_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m14307(__this, ___key, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14307_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14309_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14309(__this, method) (( bool (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14309_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14311_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14311(__this, method) (( Object_t * (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14313_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14313(__this, method) (( bool (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14315_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2_t2114  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14315(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2112 *, KeyValuePair_2_t2114 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14315_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14317_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2_t2114  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14317(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2112 *, KeyValuePair_2_t2114 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14317_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14319_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2U5BU5D_t2508* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14319(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2112 *, KeyValuePair_2U5BU5D_t2508*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14319_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14321_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2_t2114  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14321(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2112 *, KeyValuePair_2_t2114 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14321_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m14323_gshared (Dictionary_2_t2112 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14323(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2112 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14323_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14325_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14325(__this, method) (( Object_t * (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14325_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14327_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14327(__this, method) (( Object_t* (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14327_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14329_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14329(__this, method) (( Object_t * (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14329_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m14331_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m14331(__this, method) (( int32_t (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_get_Count_m14331_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m14333_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m14333(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m14333_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m14335_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m14335(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m14335_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m14337_gshared (Dictionary_2_t2112 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m14337(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2112 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14337_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m14339_gshared (Dictionary_2_t2112 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m14339(__this, ___size, method) (( void (*) (Dictionary_2_t2112 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14339_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m14341_gshared (Dictionary_2_t2112 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m14341(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2112 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14341_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2114  Dictionary_2_make_pair_m14343_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m14343(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2114  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m14343_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m14345_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m14345(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m14345_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m14347_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2U5BU5D_t2508* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m14347(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2112 *, KeyValuePair_2U5BU5D_t2508*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14347_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern "C" void Dictionary_2_Resize_m14349_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m14349(__this, method) (( void (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_Resize_m14349_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m14351_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m14351(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m14351_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C" void Dictionary_2_Clear_m14353_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m14353(__this, method) (( void (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_Clear_m14353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m14355_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m14355(__this, ___key, method) (( bool (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m14355_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m14357_gshared (Dictionary_2_t2112 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m14357(__this, ___value, method) (( bool (*) (Dictionary_2_t2112 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m14357_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m14359_gshared (Dictionary_2_t2112 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m14359(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2112 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m14359_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m14361_gshared (Dictionary_2_t2112 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m14361(__this, ___sender, method) (( void (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14361_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m14363_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m14363(__this, ___key, method) (( bool (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m14363_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m14365_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m14365(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2112 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m14365_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C" ValueCollection_t2117 * Dictionary_2_get_Values_m14367_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m14367(__this, method) (( ValueCollection_t2117 * (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_get_Values_m14367_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m14369_gshared (Dictionary_2_t2112 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m14369(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14369_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m14371_gshared (Dictionary_2_t2112 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m14371(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2112 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14371_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m14373_gshared (Dictionary_2_t2112 * __this, KeyValuePair_2_t2114  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m14373(__this, ___pair, method) (( bool (*) (Dictionary_2_t2112 *, KeyValuePair_2_t2114 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14373_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C" Enumerator_t2119  Dictionary_2_GetEnumerator_m14375_gshared (Dictionary_2_t2112 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m14375(__this, method) (( Enumerator_t2119  (*) (Dictionary_2_t2112 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14375_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t943  Dictionary_2_U3CCopyToU3Em__0_m14377_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m14377(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14377_gshared)(__this /* static, unused */, ___key, ___value, method)
