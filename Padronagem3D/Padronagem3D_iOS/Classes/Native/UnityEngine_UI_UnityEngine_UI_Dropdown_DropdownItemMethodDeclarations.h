﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t547;
// UnityEngine.UI.Text
struct Text_t548;
// UnityEngine.UI.Image
struct Image_t549;
// UnityEngine.RectTransform
struct RectTransform_t211;
// UnityEngine.UI.Toggle
struct Toggle_t550;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t48;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t92;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::get_text()
extern "C" Text_t548 * DropdownItem_get_text_m2557 (DropdownItem_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_text(UnityEngine.UI.Text)
extern "C" void DropdownItem_set_text_m2558 (DropdownItem_t547 * __this, Text_t548 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::get_image()
extern "C" Image_t549 * DropdownItem_get_image_m2559 (DropdownItem_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_image(UnityEngine.UI.Image)
extern "C" void DropdownItem_set_image_m2560 (DropdownItem_t547 * __this, Image_t549 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::get_rectTransform()
extern "C" RectTransform_t211 * DropdownItem_get_rectTransform_m2561 (DropdownItem_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_rectTransform(UnityEngine.RectTransform)
extern "C" void DropdownItem_set_rectTransform_m2562 (DropdownItem_t547 * __this, RectTransform_t211 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::get_toggle()
extern "C" Toggle_t550 * DropdownItem_get_toggle_m2563 (DropdownItem_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_toggle(UnityEngine.UI.Toggle)
extern "C" void DropdownItem_set_toggle_m2564 (DropdownItem_t547 * __this, Toggle_t550 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C" void DropdownItem_OnPointerEnter_m2565 (DropdownItem_t547 * __this, PointerEventData_t48 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C" void DropdownItem_OnCancel_m2566 (DropdownItem_t547 * __this, BaseEventData_t92 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
