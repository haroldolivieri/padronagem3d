﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m11854(__this, ___object, ___method, method) (( void (*) (Comparison_1_t1916 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11278_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Transform>::Invoke(T,T)
#define Comparison_1_Invoke_m11855(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t1916 *, Transform_t33 *, Transform_t33 *, const MethodInfo*))Comparison_1_Invoke_m11279_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Transform>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m11856(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t1916 *, Transform_t33 *, Transform_t33 *, AsyncCallback_t9 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11280_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m11857(__this, ___result, method) (( int32_t (*) (Comparison_1_t1916 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11281_gshared)(__this, ___result, method)
