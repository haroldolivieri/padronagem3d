﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Enum
struct Enum_t150;

#include "codegen/il2cpp-codegen.h"

// System.Boolean UnityScript.Lang.Extensions::op_Implicit(System.Enum)
extern "C" bool Extensions_op_Implicit_m678 (Object_t * __this /* static, unused */, Enum_t150 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
