﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15239(__this, ___dictionary, method) (( void (*) (Enumerator_t686 *, Dictionary_2_t522 *, const MethodInfo*))Enumerator__ctor_m13785_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15240(__this, method) (( Object_t * (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m15241(__this, method) (( void (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13787_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15242(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13788_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15243(__this, method) (( Object_t * (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13789_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15244(__this, method) (( Object_t * (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13790_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m3593(__this, method) (( bool (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_MoveNext_m13791_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m3590(__this, method) (( KeyValuePair_2_t685  (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_get_Current_m13792_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15245(__this, method) (( int32_t (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_get_CurrentKey_m13793_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15246(__this, method) (( PointerEventData_t48 * (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_get_CurrentValue_m13794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Reset()
#define Enumerator_Reset_m15247(__this, method) (( void (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_Reset_m13795_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m15248(__this, method) (( void (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_VerifyState_m13796_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15249(__this, method) (( void (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_VerifyCurrent_m13797_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m15250(__this, method) (( void (*) (Enumerator_t686 *, const MethodInfo*))Enumerator_Dispose_m13798_gshared)(__this, method)
