﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen_1MethodDeclarations.h"

// System.Void Boo.Lang.GenericGenerator`1<UnityEngine.YieldInstruction>::.ctor()
#define GenericGenerator_1__ctor_m698(__this, method) (( void (*) (GenericGenerator_1_t138 *, const MethodInfo*))GenericGenerator_1__ctor_m701_gshared)(__this, method)
// System.Collections.IEnumerator Boo.Lang.GenericGenerator`1<UnityEngine.YieldInstruction>::System.Collections.IEnumerable.GetEnumerator()
#define GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m11929(__this, method) (( Object_t * (*) (GenericGenerator_1_t138 *, const MethodInfo*))GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m11917_gshared)(__this, method)
// System.String Boo.Lang.GenericGenerator`1<UnityEngine.YieldInstruction>::ToString()
#define GenericGenerator_1_ToString_m11930(__this, method) (( String_t* (*) (GenericGenerator_1_t138 *, const MethodInfo*))GenericGenerator_1_ToString_m11919_gshared)(__this, method)
