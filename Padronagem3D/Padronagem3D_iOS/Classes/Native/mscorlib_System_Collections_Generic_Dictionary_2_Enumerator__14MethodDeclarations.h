﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2340;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17602_gshared (Enumerator_t2347 * __this, Dictionary_2_t2340 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17602(__this, ___dictionary, method) (( void (*) (Enumerator_t2347 *, Dictionary_2_t2340 *, const MethodInfo*))Enumerator__ctor_m17602_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17603_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17603(__this, method) (( Object_t * (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17604_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17604(__this, method) (( void (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17604_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t943  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17605_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17605(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17605_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17606_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17606(__this, method) (( Object_t * (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17606_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17607_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17607(__this, method) (( Object_t * (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17608_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17608(__this, method) (( bool (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_MoveNext_m17608_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t2342  Enumerator_get_Current_m17609_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17609(__this, method) (( KeyValuePair_2_t2342  (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_get_Current_m17609_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17610_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17610(__this, method) (( Object_t * (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_get_CurrentKey_m17610_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m17611_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17611(__this, method) (( bool (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_get_CurrentValue_m17611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m17612_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_Reset_m17612(__this, method) (( void (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_Reset_m17612_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m17613_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17613(__this, method) (( void (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_VerifyState_m17613_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17614_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17614(__this, method) (( void (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_VerifyCurrent_m17614_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17615_gshared (Enumerator_t2347 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17615(__this, method) (( void (*) (Enumerator_t2347 *, const MethodInfo*))Enumerator_Dispose_m17615_gshared)(__this, method)
