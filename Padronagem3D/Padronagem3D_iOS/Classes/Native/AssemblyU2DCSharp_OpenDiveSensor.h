﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t47;
// UnityEngine.Texture
struct Texture_t14;
// NaturalOrientation
struct NaturalOrientation_t63;
// System.EventHandler
struct EventHandler_t66;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// OpenDiveSensor
struct  OpenDiveSensor_t65  : public MonoBehaviour_t2
{
	// System.Boolean OpenDiveSensor::AddRotationGameobject
	bool ___AddRotationGameobject_2;
	// UnityEngine.GameObject OpenDiveSensor::RotationGameobject
	GameObject_t47 * ___RotationGameobject_3;
	// UnityEngine.Texture OpenDiveSensor::NoGyroTexture
	Texture_t14 * ___NoGyroTexture_4;
	// System.Boolean OpenDiveSensor::correctCenterTransition
	bool ___correctCenterTransition_5;
	// NaturalOrientation OpenDiveSensor::no
	NaturalOrientation_t63 * ___no_6;
	// System.Boolean OpenDiveSensor::mbShowErrorMessage
	bool ___mbShowErrorMessage_7;
	// System.Boolean OpenDiveSensor::mbUseGyro
	bool ___mbUseGyro_8;
	// System.Single OpenDiveSensor::q0
	float ___q0_9;
	// System.Single OpenDiveSensor::q1
	float ___q1_10;
	// System.Single OpenDiveSensor::q2
	float ___q2_11;
	// System.Single OpenDiveSensor::q3
	float ___q3_12;
	// System.Single OpenDiveSensor::m0
	float ___m0_13;
	// System.Single OpenDiveSensor::m1
	float ___m1_14;
	// System.Single OpenDiveSensor::m2
	float ___m2_15;
	// System.Single OpenDiveSensor::magnet_value
	float ___magnet_value_16;
	// System.Int32 OpenDiveSensor::magnet_trigger
	int32_t ___magnet_trigger_17;
	// System.Int32 OpenDiveSensor::magnet_detected
	int32_t ___magnet_detected_18;
	// UnityEngine.Quaternion OpenDiveSensor::rot
	Quaternion_t50  ___rot_19;
	// UnityEngine.Quaternion OpenDiveSensor::centerTransition
	Quaternion_t50  ___centerTransition_20;
	// System.Int32 OpenDiveSensor::is_tablet
	int32_t ___is_tablet_21;
	// System.Boolean OpenDiveSensor::mbMagnetDown
	bool ___mbMagnetDown_22;
	// System.EventHandler OpenDiveSensor::MagnetTriggered
	EventHandler_t66 * ___MagnetTriggered_23;
};
