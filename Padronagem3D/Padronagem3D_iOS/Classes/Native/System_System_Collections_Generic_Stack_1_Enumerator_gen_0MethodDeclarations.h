﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m14173(__this, ___t, method) (( void (*) (Enumerator_t2099 *, Stack_1_t460 *, const MethodInfo*))Enumerator__ctor_m11596_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14174(__this, method) (( void (*) (Enumerator_t2099 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11597_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14175(__this, method) (( Object_t * (*) (Enumerator_t2099 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11598_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m14176(__this, method) (( void (*) (Enumerator_t2099 *, const MethodInfo*))Enumerator_Dispose_m11599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m14177(__this, method) (( bool (*) (Enumerator_t2099 *, const MethodInfo*))Enumerator_MoveNext_m11600_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m14178(__this, method) (( Type_t * (*) (Enumerator_t2099 *, const MethodInfo*))Enumerator_get_Current_m11601_gshared)(__this, method)
