﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2034;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t433;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t2495;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t436;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m13400_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13400(__this, method) (( void (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1__ctor_m13400_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13401_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13401(__this, method) (( bool (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13401_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13402_gshared (Collection_1_t2034 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13402(__this, ___array, ___index, method) (( void (*) (Collection_1_t2034 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13402_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13403_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13403(__this, method) (( Object_t * (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13403_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13404_gshared (Collection_1_t2034 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13404(__this, ___value, method) (( int32_t (*) (Collection_1_t2034 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13404_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13405_gshared (Collection_1_t2034 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13405(__this, ___value, method) (( bool (*) (Collection_1_t2034 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13405_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13406_gshared (Collection_1_t2034 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13406(__this, ___value, method) (( int32_t (*) (Collection_1_t2034 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13406_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13407_gshared (Collection_1_t2034 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13407(__this, ___index, ___value, method) (( void (*) (Collection_1_t2034 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13407_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13408_gshared (Collection_1_t2034 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13408(__this, ___value, method) (( void (*) (Collection_1_t2034 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13408_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13409_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13409(__this, method) (( bool (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13409_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13410_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13410(__this, method) (( Object_t * (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13410_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13411_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13411(__this, method) (( bool (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13411_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13412_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13412(__this, method) (( bool (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13412_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13413_gshared (Collection_1_t2034 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13413(__this, ___index, method) (( Object_t * (*) (Collection_1_t2034 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13413_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13414_gshared (Collection_1_t2034 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13414(__this, ___index, ___value, method) (( void (*) (Collection_1_t2034 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13414_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m13415_gshared (Collection_1_t2034 * __this, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_Add_m13415(__this, ___item, method) (( void (*) (Collection_1_t2034 *, UILineInfo_t286 , const MethodInfo*))Collection_1_Add_m13415_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m13416_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13416(__this, method) (( void (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_Clear_m13416_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m13417_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13417(__this, method) (( void (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_ClearItems_m13417_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m13418_gshared (Collection_1_t2034 * __this, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13418(__this, ___item, method) (( bool (*) (Collection_1_t2034 *, UILineInfo_t286 , const MethodInfo*))Collection_1_Contains_m13418_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13419_gshared (Collection_1_t2034 * __this, UILineInfoU5BU5D_t433* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13419(__this, ___array, ___index, method) (( void (*) (Collection_1_t2034 *, UILineInfoU5BU5D_t433*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13419_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13420_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13420(__this, method) (( Object_t* (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_GetEnumerator_m13420_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13421_gshared (Collection_1_t2034 * __this, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13421(__this, ___item, method) (( int32_t (*) (Collection_1_t2034 *, UILineInfo_t286 , const MethodInfo*))Collection_1_IndexOf_m13421_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13422_gshared (Collection_1_t2034 * __this, int32_t ___index, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13422(__this, ___index, ___item, method) (( void (*) (Collection_1_t2034 *, int32_t, UILineInfo_t286 , const MethodInfo*))Collection_1_Insert_m13422_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13423_gshared (Collection_1_t2034 * __this, int32_t ___index, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13423(__this, ___index, ___item, method) (( void (*) (Collection_1_t2034 *, int32_t, UILineInfo_t286 , const MethodInfo*))Collection_1_InsertItem_m13423_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m13424_gshared (Collection_1_t2034 * __this, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13424(__this, ___item, method) (( bool (*) (Collection_1_t2034 *, UILineInfo_t286 , const MethodInfo*))Collection_1_Remove_m13424_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13425_gshared (Collection_1_t2034 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13425(__this, ___index, method) (( void (*) (Collection_1_t2034 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13425_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13426_gshared (Collection_1_t2034 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13426(__this, ___index, method) (( void (*) (Collection_1_t2034 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13426_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13427_gshared (Collection_1_t2034 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13427(__this, method) (( int32_t (*) (Collection_1_t2034 *, const MethodInfo*))Collection_1_get_Count_m13427_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t286  Collection_1_get_Item_m13428_gshared (Collection_1_t2034 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13428(__this, ___index, method) (( UILineInfo_t286  (*) (Collection_1_t2034 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13428_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13429_gshared (Collection_1_t2034 * __this, int32_t ___index, UILineInfo_t286  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13429(__this, ___index, ___value, method) (( void (*) (Collection_1_t2034 *, int32_t, UILineInfo_t286 , const MethodInfo*))Collection_1_set_Item_m13429_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13430_gshared (Collection_1_t2034 * __this, int32_t ___index, UILineInfo_t286  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13430(__this, ___index, ___item, method) (( void (*) (Collection_1_t2034 *, int32_t, UILineInfo_t286 , const MethodInfo*))Collection_1_SetItem_m13430_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13431_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13431(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13431_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t286  Collection_1_ConvertItem_m13432_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13432(__this /* static, unused */, ___item, method) (( UILineInfo_t286  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13432_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13433_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13433(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13433_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13434_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13434(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13434_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13435_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13435(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13435_gshared)(__this /* static, unused */, ___list, method)
