﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_t2309;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_t2310;

#include "mscorlib_System_Object.h"

// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_t710  : public Object_t
{
};
struct ListPool_1_t710_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::s_ListPool
	ObjectPool_1_t2309 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<>f__am$cache1
	UnityAction_1_t2310 * ___U3CU3Ef__amU24cache1_1;
};
