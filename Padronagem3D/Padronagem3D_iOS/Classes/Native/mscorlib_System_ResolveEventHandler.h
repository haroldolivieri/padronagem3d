﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly
struct Assembly_t962;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t1742;
// System.IAsyncResult
struct IAsyncResult_t8;
// System.AsyncCallback
struct AsyncCallback_t9;

#include "mscorlib_System_MulticastDelegate.h"

// System.ResolveEventHandler
struct  ResolveEventHandler_t1686  : public MulticastDelegate_t7
{
};
