﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t658;
// UnityEngine.Mesh
struct Mesh_t182;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m3525 (PositionAsUV1_t658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.PositionAsUV1::ModifyMesh(UnityEngine.Mesh)
extern "C" void PositionAsUV1_ModifyMesh_m3526 (PositionAsUV1_t658 * __this, Mesh_t182 * ___mesh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
