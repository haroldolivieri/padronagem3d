﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1874  : public Array_t { };
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct IEventSystemHandlerU5BU5D_t1885  : public Array_t { };
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t2157  : public Array_t { };
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct BaseRaycasterU5BU5D_t2163  : public Array_t { };
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t2169  : public Array_t { };
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct PointerEventDataU5BU5D_t2182  : public Array_t { };
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct ButtonStateU5BU5D_t2184  : public Array_t { };
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t2193  : public Array_t { };
// UnityEngine.UI.Dropdown/OptionData[]
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_t2196  : public Array_t { };
// UnityEngine.UI.Dropdown/DropdownItem[]
// UnityEngine.UI.Dropdown/DropdownItem[]
struct DropdownItemU5BU5D_t2202  : public Array_t { };
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct TextU5BU5D_t2223  : public Array_t { };
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t2233  : public Array_t { };
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct IndexedSet_1U5BU5D_t2237  : public Array_t { };
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct ContentTypeU5BU5D_t677  : public Array_t { };
// UnityEngine.UI.RectMask2D[]
// UnityEngine.UI.RectMask2D[]
struct RectMask2DU5BU5D_t2252  : public Array_t { };
// UnityEngine.UI.IClippable[]
// UnityEngine.UI.IClippable[]
struct IClippableU5BU5D_t2258  : public Array_t { };
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t2266  : public Array_t { };
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t2278  : public Array_t { };
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t2284  : public Array_t { };
// UnityEngine.UI.IClipper[]
// UnityEngine.UI.IClipper[]
struct IClipperU5BU5D_t2290  : public Array_t { };
