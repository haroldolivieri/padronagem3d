﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2340;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2046;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t2535;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t2536;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t942;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t2345;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m17478_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17478(__this, method) (( void (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2__ctor_m17478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17479_gshared (Dictionary_2_t2340 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17479(__this, ___comparer, method) (( void (*) (Dictionary_2_t2340 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17479_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17481_gshared (Dictionary_2_t2340 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17481(__this, ___capacity, method) (( void (*) (Dictionary_2_t2340 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17481_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17483_gshared (Dictionary_2_t2340 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17483(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2340 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m17483_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17485_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17485(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17485_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17487_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17487(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17487_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17489_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17489(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17489_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17491_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17491(__this, ___key, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17491_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17493_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17493(__this, ___key, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17493_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17495_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17495(__this, method) (( bool (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17497_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17497(__this, method) (( Object_t * (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17497_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17499_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17499(__this, method) (( bool (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17499_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17501_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2_t2342  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17501(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2340 *, KeyValuePair_2_t2342 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17501_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17503_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2_t2342  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17503(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2340 *, KeyValuePair_2_t2342 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17503_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17505_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2U5BU5D_t2535* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17505(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2340 *, KeyValuePair_2U5BU5D_t2535*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17505_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17507_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2_t2342  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17507(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2340 *, KeyValuePair_2_t2342 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17507_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17509_gshared (Dictionary_2_t2340 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17509(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2340 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17509_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17511_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17511(__this, method) (( Object_t * (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17511_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17513_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17513(__this, method) (( Object_t* (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17513_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17515_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17515(__this, method) (( Object_t * (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17515_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17517_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17517(__this, method) (( int32_t (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_get_Count_m17517_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m17519_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17519(__this, ___key, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m17519_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17521_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17521(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m17521_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17523_gshared (Dictionary_2_t2340 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17523(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2340 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17523_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17525_gshared (Dictionary_2_t2340 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17525(__this, ___size, method) (( void (*) (Dictionary_2_t2340 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17525_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17527_gshared (Dictionary_2_t2340 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17527(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2340 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17527_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2342  Dictionary_2_make_pair_m17529_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17529(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2342  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m17529_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m17531_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17531(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m17531_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17533_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2U5BU5D_t2535* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17533(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2340 *, KeyValuePair_2U5BU5D_t2535*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17533_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m17535_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17535(__this, method) (( void (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_Resize_m17535_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17537_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17537(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m17537_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m17539_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17539(__this, method) (( void (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_Clear_m17539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17541_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17541(__this, ___key, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m17541_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17543_gshared (Dictionary_2_t2340 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17543(__this, ___value, method) (( bool (*) (Dictionary_2_t2340 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m17543_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17545_gshared (Dictionary_2_t2340 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17545(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2340 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m17545_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17547_gshared (Dictionary_2_t2340 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17547(__this, ___sender, method) (( void (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17547_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17549_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17549(__this, ___key, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m17549_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17551_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17551(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m17551_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t2345 * Dictionary_2_get_Values_m17553_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17553(__this, method) (( ValueCollection_t2345 * (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_get_Values_m17553_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m17555_gshared (Dictionary_2_t2340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17555(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17555_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m17557_gshared (Dictionary_2_t2340 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17557(__this, ___value, method) (( bool (*) (Dictionary_2_t2340 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17557_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17559_gshared (Dictionary_2_t2340 * __this, KeyValuePair_2_t2342  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17559(__this, ___pair, method) (( bool (*) (Dictionary_2_t2340 *, KeyValuePair_2_t2342 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17559_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t2347  Dictionary_2_GetEnumerator_m17561_gshared (Dictionary_2_t2340 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17561(__this, method) (( Enumerator_t2347  (*) (Dictionary_2_t2340 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17561_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t943  Dictionary_2_U3CCopyToU3Em__0_m17563_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17563(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17563_gshared)(__this /* static, unused */, ___key, ___value, method)
