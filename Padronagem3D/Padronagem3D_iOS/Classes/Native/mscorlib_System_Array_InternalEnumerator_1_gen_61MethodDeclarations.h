﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17752_gshared (InternalEnumerator_1_t2363 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17752(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2363 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17752_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17753_gshared (InternalEnumerator_1_t2363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17753(__this, method) (( void (*) (InternalEnumerator_1_t2363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17753_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754_gshared (InternalEnumerator_1_t2363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17755_gshared (InternalEnumerator_1_t2363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17755(__this, method) (( void (*) (InternalEnumerator_1_t2363 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17755_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17756_gshared (InternalEnumerator_1_t2363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17756(__this, method) (( bool (*) (InternalEnumerator_1_t2363 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17756_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t2362  InternalEnumerator_1_get_Current_m17757_gshared (InternalEnumerator_1_t2363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17757(__this, method) (( KeyValuePair_2_t2362  (*) (InternalEnumerator_1_t2363 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17757_gshared)(__this, method)
