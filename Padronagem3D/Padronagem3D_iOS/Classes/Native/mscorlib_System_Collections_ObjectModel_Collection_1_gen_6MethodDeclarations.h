﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2014;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t431;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t2490;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t434;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m13100_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13100(__this, method) (( void (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1__ctor_m13100_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13101_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13101(__this, method) (( bool (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13101_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13102_gshared (Collection_1_t2014 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13102(__this, ___array, ___index, method) (( void (*) (Collection_1_t2014 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13102_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13103_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13103(__this, method) (( Object_t * (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13103_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13104_gshared (Collection_1_t2014 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13104(__this, ___value, method) (( int32_t (*) (Collection_1_t2014 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13104_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13105_gshared (Collection_1_t2014 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13105(__this, ___value, method) (( bool (*) (Collection_1_t2014 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13105_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13106_gshared (Collection_1_t2014 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13106(__this, ___value, method) (( int32_t (*) (Collection_1_t2014 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13106_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13107_gshared (Collection_1_t2014 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13107(__this, ___index, ___value, method) (( void (*) (Collection_1_t2014 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13107_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13108_gshared (Collection_1_t2014 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13108(__this, ___value, method) (( void (*) (Collection_1_t2014 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13108_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13109_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13109(__this, method) (( bool (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13109_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13110_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13110(__this, method) (( Object_t * (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13110_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13111_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13111(__this, method) (( bool (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13111_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13112_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13112(__this, method) (( bool (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13112_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13113_gshared (Collection_1_t2014 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13113(__this, ___index, method) (( Object_t * (*) (Collection_1_t2014 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13113_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13114_gshared (Collection_1_t2014 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13114(__this, ___index, ___value, method) (( void (*) (Collection_1_t2014 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13114_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m13115_gshared (Collection_1_t2014 * __this, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_Add_m13115(__this, ___item, method) (( void (*) (Collection_1_t2014 *, UIVertex_t296 , const MethodInfo*))Collection_1_Add_m13115_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m13116_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13116(__this, method) (( void (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_Clear_m13116_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m13117_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13117(__this, method) (( void (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_ClearItems_m13117_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m13118_gshared (Collection_1_t2014 * __this, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13118(__this, ___item, method) (( bool (*) (Collection_1_t2014 *, UIVertex_t296 , const MethodInfo*))Collection_1_Contains_m13118_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13119_gshared (Collection_1_t2014 * __this, UIVertexU5BU5D_t431* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13119(__this, ___array, ___index, method) (( void (*) (Collection_1_t2014 *, UIVertexU5BU5D_t431*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13119_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13120_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13120(__this, method) (( Object_t* (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_GetEnumerator_m13120_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13121_gshared (Collection_1_t2014 * __this, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13121(__this, ___item, method) (( int32_t (*) (Collection_1_t2014 *, UIVertex_t296 , const MethodInfo*))Collection_1_IndexOf_m13121_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13122_gshared (Collection_1_t2014 * __this, int32_t ___index, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13122(__this, ___index, ___item, method) (( void (*) (Collection_1_t2014 *, int32_t, UIVertex_t296 , const MethodInfo*))Collection_1_Insert_m13122_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13123_gshared (Collection_1_t2014 * __this, int32_t ___index, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13123(__this, ___index, ___item, method) (( void (*) (Collection_1_t2014 *, int32_t, UIVertex_t296 , const MethodInfo*))Collection_1_InsertItem_m13123_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m13124_gshared (Collection_1_t2014 * __this, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13124(__this, ___item, method) (( bool (*) (Collection_1_t2014 *, UIVertex_t296 , const MethodInfo*))Collection_1_Remove_m13124_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13125_gshared (Collection_1_t2014 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13125(__this, ___index, method) (( void (*) (Collection_1_t2014 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13125_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13126_gshared (Collection_1_t2014 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13126(__this, ___index, method) (( void (*) (Collection_1_t2014 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13126_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13127_gshared (Collection_1_t2014 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13127(__this, method) (( int32_t (*) (Collection_1_t2014 *, const MethodInfo*))Collection_1_get_Count_m13127_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t296  Collection_1_get_Item_m13128_gshared (Collection_1_t2014 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13128(__this, ___index, method) (( UIVertex_t296  (*) (Collection_1_t2014 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13128_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13129_gshared (Collection_1_t2014 * __this, int32_t ___index, UIVertex_t296  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13129(__this, ___index, ___value, method) (( void (*) (Collection_1_t2014 *, int32_t, UIVertex_t296 , const MethodInfo*))Collection_1_set_Item_m13129_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13130_gshared (Collection_1_t2014 * __this, int32_t ___index, UIVertex_t296  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13130(__this, ___index, ___item, method) (( void (*) (Collection_1_t2014 *, int32_t, UIVertex_t296 , const MethodInfo*))Collection_1_SetItem_m13130_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13131_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13131(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13131_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t296  Collection_1_ConvertItem_m13132_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13132(__this /* static, unused */, ___item, method) (( UIVertex_t296  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13132_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13133_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13133(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13133_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13134_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13134(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13134_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13135_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13135(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13135_gshared)(__this /* static, unused */, ___list, method)
