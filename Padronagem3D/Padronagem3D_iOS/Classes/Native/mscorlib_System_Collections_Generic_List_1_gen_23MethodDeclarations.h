﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_36MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.ctor()
#define List_1__ctor_m3621(__this, method) (( void (*) (List_1_t553 *, const MethodInfo*))List_1__ctor_m11065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15497(__this, ___collection, method) (( void (*) (List_1_t553 *, Object_t*, const MethodInfo*))List_1__ctor_m11067_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.ctor(System.Int32)
#define List_1__ctor_m15498(__this, ___capacity, method) (( void (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1__ctor_m11069_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.cctor()
#define List_1__cctor_m15499(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11071_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15500(__this, method) (( Object_t* (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15501(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t553 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m11075_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15502(__this, method) (( Object_t * (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m11077_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15503(__this, ___item, method) (( int32_t (*) (List_1_t553 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m11079_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15504(__this, ___item, method) (( bool (*) (List_1_t553 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m11081_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15505(__this, ___item, method) (( int32_t (*) (List_1_t553 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m11083_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15506(__this, ___index, ___item, method) (( void (*) (List_1_t553 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m11085_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15507(__this, ___item, method) (( void (*) (List_1_t553 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m11087_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15508(__this, method) (( bool (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11089_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15509(__this, method) (( bool (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m11091_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15510(__this, method) (( Object_t * (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m11093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15511(__this, method) (( bool (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m11095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15512(__this, method) (( bool (*) (List_1_t553 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m11097_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15513(__this, ___index, method) (( Object_t * (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m11099_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15514(__this, ___index, ___value, method) (( void (*) (List_1_t553 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m11101_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(T)
#define List_1_Add_m15515(__this, ___item, method) (( void (*) (List_1_t553 *, OptionData_t551 *, const MethodInfo*))List_1_Add_m11103_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15516(__this, ___newCount, method) (( void (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11105_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15517(__this, ___collection, method) (( void (*) (List_1_t553 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11107_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15518(__this, ___enumerable, method) (( void (*) (List_1_t553 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11109_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15519(__this, ___collection, method) (( void (*) (List_1_t553 *, Object_t*, const MethodInfo*))List_1_AddRange_m11111_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::AsReadOnly()
#define List_1_AsReadOnly_m15520(__this, method) (( ReadOnlyCollection_1_t2197 * (*) (List_1_t553 *, const MethodInfo*))List_1_AsReadOnly_m11113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Clear()
#define List_1_Clear_m15521(__this, method) (( void (*) (List_1_t553 *, const MethodInfo*))List_1_Clear_m11115_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Contains(T)
#define List_1_Contains_m15522(__this, ___item, method) (( bool (*) (List_1_t553 *, OptionData_t551 *, const MethodInfo*))List_1_Contains_m11117_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15523(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t553 *, OptionDataU5BU5D_t2196*, int32_t, const MethodInfo*))List_1_CopyTo_m11119_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Find(System.Predicate`1<T>)
#define List_1_Find_m15524(__this, ___match, method) (( OptionData_t551 * (*) (List_1_t553 *, Predicate_1_t2199 *, const MethodInfo*))List_1_Find_m11121_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15525(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2199 *, const MethodInfo*))List_1_CheckMatch_m11123_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15526(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t553 *, int32_t, int32_t, Predicate_1_t2199 *, const MethodInfo*))List_1_GetIndex_m11125_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::GetEnumerator()
#define List_1_GetEnumerator_m15527(__this, method) (( Enumerator_t2200  (*) (List_1_t553 *, const MethodInfo*))List_1_GetEnumerator_m11127_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::IndexOf(T)
#define List_1_IndexOf_m15528(__this, ___item, method) (( int32_t (*) (List_1_t553 *, OptionData_t551 *, const MethodInfo*))List_1_IndexOf_m11129_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15529(__this, ___start, ___delta, method) (( void (*) (List_1_t553 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11131_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15530(__this, ___index, method) (( void (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11133_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Insert(System.Int32,T)
#define List_1_Insert_m15531(__this, ___index, ___item, method) (( void (*) (List_1_t553 *, int32_t, OptionData_t551 *, const MethodInfo*))List_1_Insert_m11135_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15532(__this, ___collection, method) (( void (*) (List_1_t553 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11137_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Remove(T)
#define List_1_Remove_m15533(__this, ___item, method) (( bool (*) (List_1_t553 *, OptionData_t551 *, const MethodInfo*))List_1_Remove_m11139_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15534(__this, ___match, method) (( int32_t (*) (List_1_t553 *, Predicate_1_t2199 *, const MethodInfo*))List_1_RemoveAll_m11141_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15535(__this, ___index, method) (( void (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_RemoveAt_m11143_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Reverse()
#define List_1_Reverse_m15536(__this, method) (( void (*) (List_1_t553 *, const MethodInfo*))List_1_Reverse_m11145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Sort()
#define List_1_Sort_m15537(__this, method) (( void (*) (List_1_t553 *, const MethodInfo*))List_1_Sort_m11147_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15538(__this, ___comparison, method) (( void (*) (List_1_t553 *, Comparison_1_t2201 *, const MethodInfo*))List_1_Sort_m11149_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::ToArray()
#define List_1_ToArray_m15539(__this, method) (( OptionDataU5BU5D_t2196* (*) (List_1_t553 *, const MethodInfo*))List_1_ToArray_m11150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::TrimExcess()
#define List_1_TrimExcess_m15540(__this, method) (( void (*) (List_1_t553 *, const MethodInfo*))List_1_TrimExcess_m11152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Capacity()
#define List_1_get_Capacity_m15541(__this, method) (( int32_t (*) (List_1_t553 *, const MethodInfo*))List_1_get_Capacity_m11154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15542(__this, ___value, method) (( void (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11156_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Count()
#define List_1_get_Count_m15543(__this, method) (( int32_t (*) (List_1_t553 *, const MethodInfo*))List_1_get_Count_m11158_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Item(System.Int32)
#define List_1_get_Item_m15544(__this, ___index, method) (( OptionData_t551 * (*) (List_1_t553 *, int32_t, const MethodInfo*))List_1_get_Item_m11160_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::set_Item(System.Int32,T)
#define List_1_set_Item_m15545(__this, ___index, ___value, method) (( void (*) (List_1_t553 *, int32_t, OptionData_t551 *, const MethodInfo*))List_1_set_Item_m11162_gshared)(__this, ___index, ___value, method)
