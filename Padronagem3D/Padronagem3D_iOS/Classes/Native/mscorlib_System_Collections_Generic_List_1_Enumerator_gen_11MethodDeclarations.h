﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t423;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m12610_gshared (Enumerator_t1973 * __this, List_1_t423 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m12610(__this, ___l, method) (( void (*) (Enumerator_t1973 *, List_1_t423 *, const MethodInfo*))Enumerator__ctor_m12610_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m12611_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m12611(__this, method) (( void (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12611_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12612_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12612(__this, method) (( Object_t * (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12612_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C" void Enumerator_Dispose_m12613_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12613(__this, method) (( void (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_Dispose_m12613_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C" void Enumerator_VerifyState_m12614_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m12614(__this, method) (( void (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_VerifyState_m12614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12615_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12615(__this, method) (( bool (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_MoveNext_m12615_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C" Color32_t187  Enumerator_get_Current_m12616_gshared (Enumerator_t1973 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12616(__this, method) (( Color32_t187  (*) (Enumerator_t1973 *, const MethodInfo*))Enumerator_get_Current_m12616_gshared)(__this, method)
