﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// fps
struct  fps_t132  : public MonoBehaviour_t2
{
	// System.Single fps::updateInterval
	float ___updateInterval_2;
	// System.Single fps::accum
	float ___accum_3;
	// System.Int32 fps::frames
	int32_t ___frames_4;
	// System.Single fps::timeleft
	float ___timeleft_5;
};
