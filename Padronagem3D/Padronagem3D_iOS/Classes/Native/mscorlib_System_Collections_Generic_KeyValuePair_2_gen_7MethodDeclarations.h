﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m14461(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2125 *, Event_t84 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m14384_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m14462(__this, method) (( Event_t84 * (*) (KeyValuePair_2_t2125 *, const MethodInfo*))KeyValuePair_2_get_Key_m14385_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14463(__this, ___value, method) (( void (*) (KeyValuePair_2_t2125 *, Event_t84 *, const MethodInfo*))KeyValuePair_2_set_Key_m14386_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m14464(__this, method) (( int32_t (*) (KeyValuePair_2_t2125 *, const MethodInfo*))KeyValuePair_2_get_Value_m14387_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14465(__this, ___value, method) (( void (*) (KeyValuePair_2_t2125 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m14388_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m14466(__this, method) (( String_t* (*) (KeyValuePair_2_t2125 *, const MethodInfo*))KeyValuePair_2_ToString_m14389_gshared)(__this, method)
