﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Rijndael
struct Rijndael_t1113;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Rijndael::.ctor()
extern "C" void Rijndael__ctor_m9585 (Rijndael_t1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create()
extern "C" Rijndael_t1113 * Rijndael_Create_m5868 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create(System.String)
extern "C" Rijndael_t1113 * Rijndael_Create_m9586 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
