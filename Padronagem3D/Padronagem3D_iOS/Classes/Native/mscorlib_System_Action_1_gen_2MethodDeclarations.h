﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m11960(__this, ___object, ___method, method) (( void (*) (Action_1_t172 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m11939_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::Invoke(T)
#define Action_1_Invoke_m2147(__this, ___obj, method) (( void (*) (Action_1_t172 *, IScoreU5BU5D_t365*, const MethodInfo*))Action_1_Invoke_m11940_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m11961(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t172 *, IScoreU5BU5D_t365*, AsyncCallback_t9 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m11942_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m11962(__this, ___result, method) (( void (*) (Action_1_t172 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m11944_gshared)(__this, ___result, method)
