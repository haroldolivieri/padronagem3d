﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiveFPSController/$Die$8
struct U24DieU248_t129;
// DiveFPSController
struct DiveFPSController_t128;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AsyncOperation>
struct IEnumerator_1_t144;

#include "codegen/il2cpp-codegen.h"

// System.Void DiveFPSController/$Die$8::.ctor(DiveFPSController)
extern "C" void U24DieU248__ctor_m618 (U24DieU248_t129 * __this, DiveFPSController_t128 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AsyncOperation> DiveFPSController/$Die$8::GetEnumerator()
extern "C" Object_t* U24DieU248_GetEnumerator_m619 (U24DieU248_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
