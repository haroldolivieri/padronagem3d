﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
#define IndexedSet_1__ctor_m3666(__this, method) (( void (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1__ctor_m15366_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16172(__this, method) (( Object_t * (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m16173(__this, ___item, method) (( void (*) (IndexedSet_1_t698 *, Graphic_t564 *, const MethodInfo*))IndexedSet_1_Add_m15370_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m16174(__this, ___item, method) (( bool (*) (IndexedSet_1_t698 *, Graphic_t564 *, const MethodInfo*))IndexedSet_1_Remove_m15372_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m16175(__this, method) (( Object_t* (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m15374_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m16176(__this, method) (( void (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1_Clear_m15376_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m16177(__this, ___item, method) (( bool (*) (IndexedSet_1_t698 *, Graphic_t564 *, const MethodInfo*))IndexedSet_1_Contains_m15378_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m16178(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t698 *, GraphicU5BU5D_t2233*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m15380_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m16179(__this, method) (( int32_t (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1_get_Count_m15382_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m16180(__this, method) (( bool (*) (IndexedSet_1_t698 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m15384_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m16181(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t698 *, Graphic_t564 *, const MethodInfo*))IndexedSet_1_IndexOf_m15386_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m16182(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t698 *, int32_t, Graphic_t564 *, const MethodInfo*))IndexedSet_1_Insert_m15388_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m16183(__this, ___index, method) (( void (*) (IndexedSet_1_t698 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m15390_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m16184(__this, ___index, method) (( Graphic_t564 * (*) (IndexedSet_1_t698 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m15392_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m16185(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t698 *, int32_t, Graphic_t564 *, const MethodInfo*))IndexedSet_1_set_Item_m15394_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m16186(__this, ___match, method) (( void (*) (IndexedSet_1_t698 *, Predicate_1_t2235 *, const MethodInfo*))IndexedSet_1_RemoveAll_m15395_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m16187(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t698 *, Comparison_1_t569 *, const MethodInfo*))IndexedSet_1_Sort_m15396_gshared)(__this, ___sortLayoutFunction, method)
