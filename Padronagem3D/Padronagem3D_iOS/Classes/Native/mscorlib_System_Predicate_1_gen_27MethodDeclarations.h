﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.UI.Dropdown/DropdownItem>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m15675(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2205 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m11250_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Dropdown/DropdownItem>::Invoke(T)
#define Predicate_1_Invoke_m15676(__this, ___obj, method) (( bool (*) (Predicate_1_t2205 *, DropdownItem_t547 *, const MethodInfo*))Predicate_1_Invoke_m11251_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.Dropdown/DropdownItem>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m15677(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2205 *, DropdownItem_t547 *, AsyncCallback_t9 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m11252_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Dropdown/DropdownItem>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m15678(__this, ___result, method) (( bool (*) (Predicate_1_t2205 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m11253_gshared)(__this, ___result, method)
