﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boo.Lang.GenericGenerator`1<System.Object>
struct GenericGenerator_1_t143;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
extern "C" void GenericGenerator_1__ctor_m701_gshared (GenericGenerator_1_t143 * __this, const MethodInfo* method);
#define GenericGenerator_1__ctor_m701(__this, method) (( void (*) (GenericGenerator_1_t143 *, const MethodInfo*))GenericGenerator_1__ctor_m701_gshared)(__this, method)
// System.Collections.IEnumerator Boo.Lang.GenericGenerator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m11917_gshared (GenericGenerator_1_t143 * __this, const MethodInfo* method);
#define GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m11917(__this, method) (( Object_t * (*) (GenericGenerator_1_t143 *, const MethodInfo*))GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m11917_gshared)(__this, method)
// System.String Boo.Lang.GenericGenerator`1<System.Object>::ToString()
extern "C" String_t* GenericGenerator_1_ToString_m11919_gshared (GenericGenerator_1_t143 * __this, const MethodInfo* method);
#define GenericGenerator_1_ToString_m11919(__this, method) (( String_t* (*) (GenericGenerator_1_t143 *, const MethodInfo*))GenericGenerator_1_ToString_m11919_gshared)(__this, method)
