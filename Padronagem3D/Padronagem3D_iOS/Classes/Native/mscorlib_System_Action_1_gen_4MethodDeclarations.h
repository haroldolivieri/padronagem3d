﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3651(__this, ___object, ___method, method) (( void (*) (Action_1_t284 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m11939_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m2169(__this, ___obj, method) (( void (*) (Action_1_t284 *, Font_t283 *, const MethodInfo*))Action_1_Invoke_m11940_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m13009(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t284 *, Font_t283 *, AsyncCallback_t9 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m11942_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m13010(__this, ___result, method) (( void (*) (Action_1_t284 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m11944_gshared)(__this, ___result, method)
