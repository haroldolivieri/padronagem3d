﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t289;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m13063_gshared (Enumerator_t2012 * __this, List_1_t289 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m13063(__this, ___l, method) (( void (*) (Enumerator_t2012 *, List_1_t289 *, const MethodInfo*))Enumerator__ctor_m13063_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m13064_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m13064(__this, method) (( void (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13064_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13065_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13065(__this, method) (( Object_t * (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m13066_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13066(__this, method) (( void (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_Dispose_m13066_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m13067_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13067(__this, method) (( void (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_VerifyState_m13067_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13068_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13068(__this, method) (( bool (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_MoveNext_m13068_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t296  Enumerator_get_Current_m13069_gshared (Enumerator_t2012 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13069(__this, method) (( UIVertex_t296  (*) (Enumerator_t2012 *, const MethodInfo*))Enumerator_get_Current_m13069_gshared)(__this, method)
