﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boo.Lang.List`1<System.Object>
struct List_1_t2325;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Boo.Lang.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m17259_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1__ctor_m17259(__this, method) (( void (*) (List_1_t2325 *, const MethodInfo*))List_1__ctor_m17259_gshared)(__this, method)
// System.Void Boo.Lang.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m17260_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17260(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17260_gshared)(__this /* static, unused */, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17261_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17261(__this, ___item, method) (( void (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17261_gshared)(__this, ___item, method)
// System.Collections.IEnumerator Boo.Lang.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17262_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17262(__this, method) (( Object_t * (*) (List_1_t2325 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17262_gshared)(__this, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void List_1_System_Collections_Generic_IListU3CTU3E_Insert_m17263_gshared (List_1_t2325 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_Generic_IListU3CTU3E_Insert_m17263(__this, ___index, ___item, method) (( void (*) (List_1_t2325 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_Generic_IListU3CTU3E_Insert_m17263_gshared)(__this, ___index, ___item, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void List_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17264_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17264(__this, ___index, method) (( void (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17264_gshared)(__this, ___index, method)
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17265_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17265(__this, ___item, method) (( bool (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17265_gshared)(__this, ___item, method)
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17266_gshared (List_1_t2325 * __this, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17266(__this, ___value, method) (( int32_t (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17266_gshared)(__this, ___value, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17267_gshared (List_1_t2325 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17267(__this, ___index, ___value, method) (( void (*) (List_1_t2325 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17267_gshared)(__this, ___index, ___value, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17268_gshared (List_1_t2325 * __this, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17268(__this, ___value, method) (( void (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17268_gshared)(__this, ___value, method)
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17269_gshared (List_1_t2325 * __this, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17269(__this, ___value, method) (( int32_t (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17269_gshared)(__this, ___value, method)
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17270_gshared (List_1_t2325 * __this, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17270(__this, ___value, method) (( bool (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17270_gshared)(__this, ___value, method)
// System.Object Boo.Lang.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17271_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17271(__this, ___index, method) (( Object_t * (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17271_gshared)(__this, ___index, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17272_gshared (List_1_t2325 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17272(__this, ___index, ___value, method) (( void (*) (List_1_t2325 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17272_gshared)(__this, ___index, ___value, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void List_1_System_Collections_IList_RemoveAt_m17273_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_RemoveAt_m17273(__this, ___index, method) (( void (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_RemoveAt_m17273_gshared)(__this, ___index, method)
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17274_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17274(__this, method) (( bool (*) (List_1_t2325 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17274_gshared)(__this, method)
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17275_gshared (List_1_t2325 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17275(__this, ___array, ___index, method) (( void (*) (List_1_t2325 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17275_gshared)(__this, ___array, ___index, method)
// System.Int32 Boo.Lang.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m17276_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_get_Count_m17276(__this, method) (( int32_t (*) (List_1_t2325 *, const MethodInfo*))List_1_get_Count_m17276_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Boo.Lang.List`1<System.Object>::GetEnumerator()
extern "C" Object_t* List_1_GetEnumerator_m17277_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17277(__this, method) (( Object_t* (*) (List_1_t2325 *, const MethodInfo*))List_1_GetEnumerator_m17277_gshared)(__this, method)
// System.Void Boo.Lang.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17278_gshared (List_1_t2325 * __this, ObjectU5BU5D_t115* ___target, int32_t ___index, const MethodInfo* method);
#define List_1_CopyTo_m17278(__this, ___target, ___index, method) (( void (*) (List_1_t2325 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))List_1_CopyTo_m17278_gshared)(__this, ___target, ___index, method)
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsSynchronized()
extern "C" bool List_1_get_IsSynchronized_m17279_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_get_IsSynchronized_m17279(__this, method) (( bool (*) (List_1_t2325 *, const MethodInfo*))List_1_get_IsSynchronized_m17279_gshared)(__this, method)
// System.Object Boo.Lang.List`1<System.Object>::get_SyncRoot()
extern "C" Object_t * List_1_get_SyncRoot_m17280_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_get_SyncRoot_m17280(__this, method) (( Object_t * (*) (List_1_t2325 *, const MethodInfo*))List_1_get_SyncRoot_m17280_gshared)(__this, method)
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsReadOnly()
extern "C" bool List_1_get_IsReadOnly_m17281_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_get_IsReadOnly_m17281(__this, method) (( bool (*) (List_1_t2325 *, const MethodInfo*))List_1_get_IsReadOnly_m17281_gshared)(__this, method)
// T Boo.Lang.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m17282_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17282(__this, ___index, method) (( Object_t * (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_get_Item_m17282_gshared)(__this, ___index, method)
// System.Void Boo.Lang.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17283_gshared (List_1_t2325 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m17283(__this, ___index, ___value, method) (( void (*) (List_1_t2325 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m17283_gshared)(__this, ___index, ___value, method)
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Push(T)
extern "C" List_1_t2325 * List_1_Push_m17284_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Push_m17284(__this, ___item, method) (( List_1_t2325 * (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_Push_m17284_gshared)(__this, ___item, method)
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T)
extern "C" List_1_t2325 * List_1_Add_m17285_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m17285(__this, ___item, method) (( List_1_t2325 * (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_Add_m17285_gshared)(__this, ___item, method)
// System.String Boo.Lang.List`1<System.Object>::ToString()
extern "C" String_t* List_1_ToString_m17286_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_ToString_m17286(__this, method) (( String_t* (*) (List_1_t2325 *, const MethodInfo*))List_1_ToString_m17286_gshared)(__this, method)
// System.String Boo.Lang.List`1<System.Object>::Join(System.String)
extern "C" String_t* List_1_Join_m17287_gshared (List_1_t2325 * __this, String_t* ___separator, const MethodInfo* method);
#define List_1_Join_m17287(__this, ___separator, method) (( String_t* (*) (List_1_t2325 *, String_t*, const MethodInfo*))List_1_Join_m17287_gshared)(__this, ___separator, method)
// System.Int32 Boo.Lang.List`1<System.Object>::GetHashCode()
extern "C" int32_t List_1_GetHashCode_m17288_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_GetHashCode_m17288(__this, method) (( int32_t (*) (List_1_t2325 *, const MethodInfo*))List_1_GetHashCode_m17288_gshared)(__this, method)
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(System.Object)
extern "C" bool List_1_Equals_m17289_gshared (List_1_t2325 * __this, Object_t * ___other, const MethodInfo* method);
#define List_1_Equals_m17289(__this, ___other, method) (( bool (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_Equals_m17289_gshared)(__this, ___other, method)
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(Boo.Lang.List`1<T>)
extern "C" bool List_1_Equals_m17290_gshared (List_1_t2325 * __this, List_1_t2325 * ___other, const MethodInfo* method);
#define List_1_Equals_m17290(__this, ___other, method) (( bool (*) (List_1_t2325 *, List_1_t2325 *, const MethodInfo*))List_1_Equals_m17290_gshared)(__this, ___other, method)
// System.Void Boo.Lang.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m17291_gshared (List_1_t2325 * __this, const MethodInfo* method);
#define List_1_Clear_m17291(__this, method) (( void (*) (List_1_t2325 *, const MethodInfo*))List_1_Clear_m17291_gshared)(__this, method)
// System.Boolean Boo.Lang.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m17292_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m17292(__this, ___item, method) (( bool (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_Contains_m17292_gshared)(__this, ___item, method)
// System.Int32 Boo.Lang.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17293_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m17293(__this, ___item, method) (( int32_t (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_IndexOf_m17293_gshared)(__this, ___item, method)
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Insert(System.Int32,T)
extern "C" List_1_t2325 * List_1_Insert_m17294_gshared (List_1_t2325 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m17294(__this, ___index, ___item, method) (( List_1_t2325 * (*) (List_1_t2325 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m17294_gshared)(__this, ___index, ___item, method)
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Remove(T)
extern "C" List_1_t2325 * List_1_Remove_m17295_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m17295(__this, ___item, method) (( List_1_t2325 * (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_Remove_m17295_gshared)(__this, ___item, method)
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" List_1_t2325 * List_1_RemoveAt_m17296_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17296(__this, ___index, method) (( List_1_t2325 * (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17296_gshared)(__this, ___index, method)
// System.Void Boo.Lang.List`1<System.Object>::EnsureCapacity(System.Int32)
extern "C" void List_1_EnsureCapacity_m17297_gshared (List_1_t2325 * __this, int32_t ___minCapacity, const MethodInfo* method);
#define List_1_EnsureCapacity_m17297(__this, ___minCapacity, method) (( void (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_EnsureCapacity_m17297_gshared)(__this, ___minCapacity, method)
// T[] Boo.Lang.List`1<System.Object>::NewArray(System.Int32)
extern "C" ObjectU5BU5D_t115* List_1_NewArray_m17298_gshared (List_1_t2325 * __this, int32_t ___minCapacity, const MethodInfo* method);
#define List_1_NewArray_m17298(__this, ___minCapacity, method) (( ObjectU5BU5D_t115* (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_NewArray_m17298_gshared)(__this, ___minCapacity, method)
// System.Void Boo.Lang.List`1<System.Object>::InnerRemoveAt(System.Int32)
extern "C" void List_1_InnerRemoveAt_m17299_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_InnerRemoveAt_m17299(__this, ___index, method) (( void (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_InnerRemoveAt_m17299_gshared)(__this, ___index, method)
// System.Boolean Boo.Lang.List`1<System.Object>::InnerRemove(T)
extern "C" bool List_1_InnerRemove_m17300_gshared (List_1_t2325 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_InnerRemove_m17300(__this, ___item, method) (( bool (*) (List_1_t2325 *, Object_t *, const MethodInfo*))List_1_InnerRemove_m17300_gshared)(__this, ___item, method)
// System.Int32 Boo.Lang.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" int32_t List_1_CheckIndex_m17301_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17301(__this, ___index, method) (( int32_t (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17301_gshared)(__this, ___index, method)
// System.Int32 Boo.Lang.List`1<System.Object>::NormalizeIndex(System.Int32)
extern "C" int32_t List_1_NormalizeIndex_m17302_gshared (List_1_t2325 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_NormalizeIndex_m17302(__this, ___index, method) (( int32_t (*) (List_1_t2325 *, int32_t, const MethodInfo*))List_1_NormalizeIndex_m17302_gshared)(__this, ___index, method)
// T Boo.Lang.List`1<System.Object>::Coerce(System.Object)
extern "C" Object_t * List_1_Coerce_m17303_gshared (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method);
#define List_1_Coerce_m17303(__this /* static, unused */, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))List_1_Coerce_m17303_gshared)(__this /* static, unused */, ___value, method)
