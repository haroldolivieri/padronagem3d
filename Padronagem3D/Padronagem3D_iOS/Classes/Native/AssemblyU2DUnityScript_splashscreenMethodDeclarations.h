﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// splashscreen
struct splashscreen_t136;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// UnityEngine.GUITexture
struct GUITexture_t141;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Fade.h"

// System.Void splashscreen::.ctor()
extern "C" void splashscreen__ctor_m645 (splashscreen_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator splashscreen::Start()
extern "C" Object_t * splashscreen_Start_m646 (splashscreen_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator splashscreen::FadeGUITexture(UnityEngine.GUITexture,System.Single,Fade)
extern "C" Object_t * splashscreen_FadeGUITexture_m647 (splashscreen_t136 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void splashscreen::Main()
extern "C" void splashscreen_Main_m648 (splashscreen_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
