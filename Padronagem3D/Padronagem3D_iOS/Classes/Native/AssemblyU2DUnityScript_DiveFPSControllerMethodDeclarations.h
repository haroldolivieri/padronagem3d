﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DiveFPSController
struct DiveFPSController_t128;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t145;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// UnityEngine.Collider
struct Collider_t79;

#include "codegen/il2cpp-codegen.h"

// System.Void DiveFPSController::.ctor()
extern "C" void DiveFPSController__ctor_m620 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::Awake()
extern "C" void DiveFPSController_Awake_m621 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::toggle_autowalk()
extern "C" void DiveFPSController_toggle_autowalk_m622 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::JumpUp()
extern "C" void DiveFPSController_JumpUp_m623 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::Start()
extern "C" void DiveFPSController_Start_m624 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" void DiveFPSController_OnControllerColliderHit_m625 (DiveFPSController_t128 * __this, ControllerColliderHit_t145 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DiveFPSController::Die()
extern "C" Object_t * DiveFPSController_Die_m626 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::Update()
extern "C" void DiveFPSController_Update_m627 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::LateUpdate()
extern "C" void DiveFPSController_LateUpdate_m628 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::OnGUI()
extern "C" void DiveFPSController_OnGUI_m629 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::OnTriggerEnter(UnityEngine.Collider)
extern "C" void DiveFPSController_OnTriggerEnter_m630 (DiveFPSController_t128 * __this, Collider_t79 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::OnTriggerExit(UnityEngine.Collider)
extern "C" void DiveFPSController_OnTriggerExit_m631 (DiveFPSController_t128 * __this, Collider_t79 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DiveFPSController::Main()
extern "C" void DiveFPSController_Main_m632 (DiveFPSController_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
