﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenDiveSensor
struct OpenDiveSensor_t65;
// System.EventHandler
struct EventHandler_t66;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenDiveSensor::.ctor()
extern "C" void OpenDiveSensor__ctor_m298 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::add_MagnetTriggered(System.EventHandler)
extern "C" void OpenDiveSensor_add_MagnetTriggered_m299 (OpenDiveSensor_t65 * __this, EventHandler_t66 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::remove_MagnetTriggered(System.EventHandler)
extern "C" void OpenDiveSensor_remove_MagnetTriggered_m300 (OpenDiveSensor_t65 * __this, EventHandler_t66 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::initialize_sensors()
extern "C" void OpenDiveSensor_initialize_sensors_m301 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::stop_sensors()
extern "C" void OpenDiveSensor_stop_sensors_m302 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OpenDiveSensor::get_q0()
extern "C" float OpenDiveSensor_get_q0_m303 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OpenDiveSensor::get_q1()
extern "C" float OpenDiveSensor_get_q1_m304 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OpenDiveSensor::get_q2()
extern "C" float OpenDiveSensor_get_q2_m305 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OpenDiveSensor::get_q3()
extern "C" float OpenDiveSensor_get_q3_m306 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::DiveUpdateGyroData()
extern "C" void OpenDiveSensor_DiveUpdateGyroData_m307 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OpenDiveSensor::get_q(System.Single&,System.Single&,System.Single&,System.Single&)
extern "C" int32_t OpenDiveSensor_get_q_m308 (Object_t * __this /* static, unused */, float* ___q0, float* ___q1, float* ___q2, float* ___q3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OpenDiveSensor::get_magnet(System.Int32&,System.Int32&,System.Single&)
extern "C" int32_t OpenDiveSensor_get_magnet_m309 (Object_t * __this /* static, unused */, int32_t* ___detected, int32_t* ___t1, float* ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OpenDiveSensor::get_m(System.Single&,System.Single&,System.Single&)
extern "C" int32_t OpenDiveSensor_get_m_m310 (Object_t * __this /* static, unused */, float* ___m0, float* ___m1, float* ___m2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::Start()
extern "C" void OpenDiveSensor_Start_m311 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::OnMagnetTriggered()
extern "C" void OpenDiveSensor_OnMagnetTriggered_m312 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::Update()
extern "C" void OpenDiveSensor_Update_m313 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::OnGUI()
extern "C" void OpenDiveSensor_OnGUI_m314 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenDiveSensor::OnApplicationQuit()
extern "C" void OpenDiveSensor_OnApplicationQuit_m315 (OpenDiveSensor_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
