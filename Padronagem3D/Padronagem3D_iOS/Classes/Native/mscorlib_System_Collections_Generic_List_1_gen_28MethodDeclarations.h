﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_36MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::.ctor()
#define List_1__ctor_m3707(__this, method) (( void (*) (List_1_t598 *, const MethodInfo*))List_1__ctor_m11065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16313(__this, ___collection, method) (( void (*) (List_1_t598 *, Object_t*, const MethodInfo*))List_1__ctor_m11067_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::.ctor(System.Int32)
#define List_1__ctor_m16314(__this, ___capacity, method) (( void (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1__ctor_m11069_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::.cctor()
#define List_1__cctor_m16315(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11071_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16316(__this, method) (( Object_t* (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16317(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t598 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m11075_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16318(__this, method) (( Object_t * (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m11077_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16319(__this, ___item, method) (( int32_t (*) (List_1_t598 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m11079_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16320(__this, ___item, method) (( bool (*) (List_1_t598 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m11081_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16321(__this, ___item, method) (( int32_t (*) (List_1_t598 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m11083_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16322(__this, ___index, ___item, method) (( void (*) (List_1_t598 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m11085_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16323(__this, ___item, method) (( void (*) (List_1_t598 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m11087_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16324(__this, method) (( bool (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11089_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16325(__this, method) (( bool (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m11091_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16326(__this, method) (( Object_t * (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m11093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16327(__this, method) (( bool (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m11095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16328(__this, method) (( bool (*) (List_1_t598 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m11097_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16329(__this, ___index, method) (( Object_t * (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m11099_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16330(__this, ___index, ___value, method) (( void (*) (List_1_t598 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m11101_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Add(T)
#define List_1_Add_m16331(__this, ___item, method) (( void (*) (List_1_t598 *, RectMask2D_t590 *, const MethodInfo*))List_1_Add_m11103_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16332(__this, ___newCount, method) (( void (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11105_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16333(__this, ___collection, method) (( void (*) (List_1_t598 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11107_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16334(__this, ___enumerable, method) (( void (*) (List_1_t598 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11109_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16335(__this, ___collection, method) (( void (*) (List_1_t598 *, Object_t*, const MethodInfo*))List_1_AddRange_m11111_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::AsReadOnly()
#define List_1_AsReadOnly_m16336(__this, method) (( ReadOnlyCollection_1_t2253 * (*) (List_1_t598 *, const MethodInfo*))List_1_AsReadOnly_m11113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Clear()
#define List_1_Clear_m16337(__this, method) (( void (*) (List_1_t598 *, const MethodInfo*))List_1_Clear_m11115_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Contains(T)
#define List_1_Contains_m16338(__this, ___item, method) (( bool (*) (List_1_t598 *, RectMask2D_t590 *, const MethodInfo*))List_1_Contains_m11117_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16339(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t598 *, RectMask2DU5BU5D_t2252*, int32_t, const MethodInfo*))List_1_CopyTo_m11119_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Find(System.Predicate`1<T>)
#define List_1_Find_m16340(__this, ___match, method) (( RectMask2D_t590 * (*) (List_1_t598 *, Predicate_1_t2255 *, const MethodInfo*))List_1_Find_m11121_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16341(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2255 *, const MethodInfo*))List_1_CheckMatch_m11123_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16342(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t598 *, int32_t, int32_t, Predicate_1_t2255 *, const MethodInfo*))List_1_GetIndex_m11125_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::GetEnumerator()
#define List_1_GetEnumerator_m16343(__this, method) (( Enumerator_t2256  (*) (List_1_t598 *, const MethodInfo*))List_1_GetEnumerator_m11127_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::IndexOf(T)
#define List_1_IndexOf_m16344(__this, ___item, method) (( int32_t (*) (List_1_t598 *, RectMask2D_t590 *, const MethodInfo*))List_1_IndexOf_m11129_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16345(__this, ___start, ___delta, method) (( void (*) (List_1_t598 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11131_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16346(__this, ___index, method) (( void (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11133_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Insert(System.Int32,T)
#define List_1_Insert_m16347(__this, ___index, ___item, method) (( void (*) (List_1_t598 *, int32_t, RectMask2D_t590 *, const MethodInfo*))List_1_Insert_m11135_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16348(__this, ___collection, method) (( void (*) (List_1_t598 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11137_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Remove(T)
#define List_1_Remove_m16349(__this, ___item, method) (( bool (*) (List_1_t598 *, RectMask2D_t590 *, const MethodInfo*))List_1_Remove_m11139_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16350(__this, ___match, method) (( int32_t (*) (List_1_t598 *, Predicate_1_t2255 *, const MethodInfo*))List_1_RemoveAll_m11141_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16351(__this, ___index, method) (( void (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_RemoveAt_m11143_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Reverse()
#define List_1_Reverse_m16352(__this, method) (( void (*) (List_1_t598 *, const MethodInfo*))List_1_Reverse_m11145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Sort()
#define List_1_Sort_m16353(__this, method) (( void (*) (List_1_t598 *, const MethodInfo*))List_1_Sort_m11147_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16354(__this, ___comparison, method) (( void (*) (List_1_t598 *, Comparison_1_t2257 *, const MethodInfo*))List_1_Sort_m11149_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::ToArray()
#define List_1_ToArray_m16355(__this, method) (( RectMask2DU5BU5D_t2252* (*) (List_1_t598 *, const MethodInfo*))List_1_ToArray_m11150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::TrimExcess()
#define List_1_TrimExcess_m16356(__this, method) (( void (*) (List_1_t598 *, const MethodInfo*))List_1_TrimExcess_m11152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Capacity()
#define List_1_get_Capacity_m16357(__this, method) (( int32_t (*) (List_1_t598 *, const MethodInfo*))List_1_get_Capacity_m11154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16358(__this, ___value, method) (( void (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11156_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Count()
#define List_1_get_Count_m16359(__this, method) (( int32_t (*) (List_1_t598 *, const MethodInfo*))List_1_get_Count_m11158_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32)
#define List_1_get_Item_m16360(__this, ___index, method) (( RectMask2D_t590 * (*) (List_1_t598 *, int32_t, const MethodInfo*))List_1_get_Item_m11160_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::set_Item(System.Int32,T)
#define List_1_set_Item_m16361(__this, ___index, ___value, method) (( void (*) (List_1_t598 *, int32_t, RectMask2D_t590 *, const MethodInfo*))List_1_set_Item_m11162_gshared)(__this, ___index, ___value, method)
