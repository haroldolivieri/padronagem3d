﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12111_gshared (InternalEnumerator_1_t1940 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12111(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1940 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12111_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12112_gshared (InternalEnumerator_1_t1940 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12112(__this, method) (( void (*) (InternalEnumerator_1_t1940 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12112_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12113_gshared (InternalEnumerator_1_t1940 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12113(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1940 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12113_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12114_gshared (InternalEnumerator_1_t1940 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12114(__this, method) (( void (*) (InternalEnumerator_1_t1940 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12114_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12115_gshared (InternalEnumerator_1_t1940 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12115(__this, method) (( bool (*) (InternalEnumerator_1_t1940 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12115_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
extern "C" Vector4_t90  InternalEnumerator_1_get_Current_m12116_gshared (InternalEnumerator_1_t1940 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12116(__this, method) (( Vector4_t90  (*) (InternalEnumerator_1_t1940 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12116_gshared)(__this, method)
