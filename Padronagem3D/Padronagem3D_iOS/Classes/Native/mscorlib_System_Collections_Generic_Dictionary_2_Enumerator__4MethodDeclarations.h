﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2084;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14071_gshared (Enumerator_t2090 * __this, Dictionary_2_t2084 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m14071(__this, ___dictionary, method) (( void (*) (Enumerator_t2090 *, Dictionary_2_t2084 *, const MethodInfo*))Enumerator__ctor_m14071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14072(__this, method) (( Object_t * (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m14073(__this, method) (( void (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14073_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t943  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075(__this, method) (( Object_t * (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076(__this, method) (( Object_t * (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14077_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14077(__this, method) (( bool (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_MoveNext_m14077_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2086  Enumerator_get_Current_m14078_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14078(__this, method) (( KeyValuePair_2_t2086  (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_get_Current_m14078_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m14079_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m14079(__this, method) (( Object_t * (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_get_CurrentKey_m14079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m14080_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m14080(__this, method) (( Object_t * (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_get_CurrentValue_m14080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C" void Enumerator_Reset_m14081_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_Reset_m14081(__this, method) (( void (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_Reset_m14081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m14082_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14082(__this, method) (( void (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_VerifyState_m14082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m14083_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m14083(__this, method) (( void (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_VerifyCurrent_m14083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14084_gshared (Enumerator_t2090 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14084(__this, method) (( void (*) (Enumerator_t2090 *, const MethodInfo*))Enumerator_Dispose_m14084_gshared)(__this, method)
