﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t2016;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m13141_gshared (DefaultComparer_t2016 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13141(__this, method) (( void (*) (DefaultComparer_t2016 *, const MethodInfo*))DefaultComparer__ctor_m13141_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m13142_gshared (DefaultComparer_t2016 * __this, UIVertex_t296  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m13142(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2016 *, UIVertex_t296 , const MethodInfo*))DefaultComparer_GetHashCode_m13142_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m13143_gshared (DefaultComparer_t2016 * __this, UIVertex_t296  ___x, UIVertex_t296  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m13143(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2016 *, UIVertex_t296 , UIVertex_t296 , const MethodInfo*))DefaultComparer_Equals_m13143_gshared)(__this, ___x, ___y, method)
