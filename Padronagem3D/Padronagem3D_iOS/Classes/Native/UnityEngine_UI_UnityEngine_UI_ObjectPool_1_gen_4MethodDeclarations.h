﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m17151(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2305 *, UnityAction_1_t2306 *, UnityAction_1_t2306 *, const MethodInfo*))ObjectPool_1__ctor_m11576_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_countAll()
#define ObjectPool_1_get_countAll_m17152(__this, method) (( int32_t (*) (ObjectPool_1_t2305 *, const MethodInfo*))ObjectPool_1_get_countAll_m11578_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m17153(__this, ___value, method) (( void (*) (ObjectPool_1_t2305 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11580_gshared)(__this, ___value, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Get()
#define ObjectPool_1_Get_m17154(__this, method) (( List_1_t417 * (*) (ObjectPool_1_t2305 *, const MethodInfo*))ObjectPool_1_Get_m11582_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Release(T)
#define ObjectPool_1_Release_m17155(__this, ___element, method) (( void (*) (ObjectPool_1_t2305 *, List_1_t417 *, const MethodInfo*))ObjectPool_1_Release_m11584_gshared)(__this, ___element, method)
