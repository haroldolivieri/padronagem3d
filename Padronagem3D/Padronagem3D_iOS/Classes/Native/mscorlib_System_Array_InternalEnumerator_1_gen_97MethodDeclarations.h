﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"

// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18400_gshared (InternalEnumerator_1_t2436 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18400(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2436 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18400_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18401_gshared (InternalEnumerator_1_t2436 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18401(__this, method) (( void (*) (InternalEnumerator_1_t2436 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18401_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18402_gshared (InternalEnumerator_1_t2436 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18402(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2436 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18402_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18403_gshared (InternalEnumerator_1_t2436 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18403(__this, method) (( void (*) (InternalEnumerator_1_t2436 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18403_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18404_gshared (InternalEnumerator_1_t2436 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18404(__this, method) (( bool (*) (InternalEnumerator_1_t2436 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18404_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern "C" ResourceInfo_t1403  InternalEnumerator_1_get_Current_m18405_gshared (InternalEnumerator_1_t2436 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18405(__this, method) (( ResourceInfo_t1403  (*) (InternalEnumerator_1_t2436 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18405_gshared)(__this, method)
