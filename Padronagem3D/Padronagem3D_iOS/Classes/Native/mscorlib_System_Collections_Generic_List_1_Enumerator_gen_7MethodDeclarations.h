﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m11847(__this, ___l, method) (( void (*) (Enumerator_t1915 *, List_1_t507 *, const MethodInfo*))Enumerator__ctor_m11163_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m11848(__this, method) (( void (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11164_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11849(__this, method) (( Object_t * (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::Dispose()
#define Enumerator_Dispose_m11850(__this, method) (( void (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_Dispose_m11166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::VerifyState()
#define Enumerator_VerifyState_m11851(__this, method) (( void (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_VerifyState_m11167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::MoveNext()
#define Enumerator_MoveNext_m11852(__this, method) (( bool (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_MoveNext_m11168_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::get_Current()
#define Enumerator_get_Current_m11853(__this, method) (( Transform_t33 * (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_get_Current_m11169_gshared)(__this, method)
