﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t421;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m12466_gshared (Enumerator_t1963 * __this, List_1_t421 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m12466(__this, ___l, method) (( void (*) (Enumerator_t1963 *, List_1_t421 *, const MethodInfo*))Enumerator__ctor_m12466_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m12467_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m12467(__this, method) (( void (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12467_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12468_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12468(__this, method) (( Object_t * (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12468_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C" void Enumerator_Dispose_m12469_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12469(__this, method) (( void (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_Dispose_m12469_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C" void Enumerator_VerifyState_m12470_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m12470(__this, method) (( void (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_VerifyState_m12470_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12471_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12471(__this, method) (( bool (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_MoveNext_m12471_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t15  Enumerator_get_Current_m12472_gshared (Enumerator_t1963 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12472(__this, method) (( Vector2_t15  (*) (Enumerator_t1963 *, const MethodInfo*))Enumerator_get_Current_m12472_gshared)(__this, method)
