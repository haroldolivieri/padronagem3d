﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m13759_gshared (KeyValuePair_2_t2066 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m13759(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2066 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m13759_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m13760_gshared (KeyValuePair_2_t2066 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m13760(__this, method) (( int32_t (*) (KeyValuePair_2_t2066 *, const MethodInfo*))KeyValuePair_2_get_Key_m13760_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m13761_gshared (KeyValuePair_2_t2066 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m13761(__this, ___value, method) (( void (*) (KeyValuePair_2_t2066 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m13761_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m13762_gshared (KeyValuePair_2_t2066 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m13762(__this, method) (( Object_t * (*) (KeyValuePair_2_t2066 *, const MethodInfo*))KeyValuePair_2_get_Value_m13762_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m13763_gshared (KeyValuePair_2_t2066 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m13763(__this, ___value, method) (( void (*) (KeyValuePair_2_t2066 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m13763_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m13764_gshared (KeyValuePair_2_t2066 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m13764(__this, method) (( String_t* (*) (KeyValuePair_2_t2066 *, const MethodInfo*))KeyValuePair_2_ToString_m13764_gshared)(__this, method)
