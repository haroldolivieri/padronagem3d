﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t47;
// UnityEngine.Texture2D
struct Texture2D_t126;
// UnityEngine.AsyncOperation
struct AsyncOperation_t127;
struct AsyncOperation_t127_marshaled;
// DiveFPSController
struct DiveFPSController_t128;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen.h"
#include "UnityEngine_UnityEngine_Color.h"

// DiveFPSController/$Die$8/$
struct  U24_t124  : public GenericGeneratorEnumerator_1_t125
{
	// UnityEngine.GameObject DiveFPSController/$Die$8/$::$fade$9
	GameObject_t47 * ___U24fadeU249_2;
	// UnityEngine.Texture2D DiveFPSController/$Die$8/$::$tex$10
	Texture2D_t126 * ___U24texU2410_3;
	// System.Single DiveFPSController/$Die$8/$::$alpha$11
	float ___U24alphaU2411_4;
	// UnityEngine.AsyncOperation DiveFPSController/$Die$8/$::$async$12
	AsyncOperation_t127 * ___U24asyncU2412_5;
	// System.Single DiveFPSController/$Die$8/$::$$2$13
	float ___U24U242U2413_6;
	// UnityEngine.Color DiveFPSController/$Die$8/$::$$3$14
	Color_t11  ___U24U243U2414_7;
	// DiveFPSController DiveFPSController/$Die$8/$::$self_$15
	DiveFPSController_t128 * ___U24self_U2415_8;
};
