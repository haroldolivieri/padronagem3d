﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t2421;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1810;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2541;
// System.Exception
struct Exception_t108;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m18203_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgumentU5BU5D_t1810* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m18203(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgumentU5BU5D_t1810*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m18203_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18204_gshared (ArrayReadOnlyList_1_t2421 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18204(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2421 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18204_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1374  ArrayReadOnlyList_1_get_Item_m18205_gshared (ArrayReadOnlyList_1_t2421 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m18205(__this, ___index, method) (( CustomAttributeTypedArgument_t1374  (*) (ArrayReadOnlyList_1_t2421 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m18205_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m18206_gshared (ArrayReadOnlyList_1_t2421 * __this, int32_t ___index, CustomAttributeTypedArgument_t1374  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m18206(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, int32_t, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m18206_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m18207_gshared (ArrayReadOnlyList_1_t2421 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m18207(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2421 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m18207_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m18208_gshared (ArrayReadOnlyList_1_t2421 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m18208(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2421 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m18208_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m18209_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m18209(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_Add_m18209_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m18210_gshared (ArrayReadOnlyList_1_t2421 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m18210(__this, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m18210_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m18211_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m18211(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m18211_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m18212_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgumentU5BU5D_t1810* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m18212(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgumentU5BU5D_t1810*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m18212_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m18213_gshared (ArrayReadOnlyList_1_t2421 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m18213(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2421 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m18213_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m18214_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m18214(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m18214_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m18215_gshared (ArrayReadOnlyList_1_t2421 * __this, int32_t ___index, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m18215(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, int32_t, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m18215_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m18216_gshared (ArrayReadOnlyList_1_t2421 * __this, CustomAttributeTypedArgument_t1374  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m18216(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2421 *, CustomAttributeTypedArgument_t1374 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m18216_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m18217_gshared (ArrayReadOnlyList_1_t2421 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m18217(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2421 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m18217_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C" Exception_t108 * ArrayReadOnlyList_1_ReadOnlyError_m18218_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m18218(__this /* static, unused */, method) (( Exception_t108 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m18218_gshared)(__this /* static, unused */, method)
