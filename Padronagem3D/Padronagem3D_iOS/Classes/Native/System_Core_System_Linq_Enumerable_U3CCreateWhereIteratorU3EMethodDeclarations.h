﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m11871_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m11871(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m11871_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m11872_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m11872(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m11872_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m11873_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m11873(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m11873_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m11874_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m11874(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m11874_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m11875_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m11875(__this, method) (( Object_t* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m11875_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern "C" bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m11876_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m11876(__this, method) (( bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m11876_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m11877_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m11877(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m11877_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m11878_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m11878(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1918 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m11878_gshared)(__this, method)
