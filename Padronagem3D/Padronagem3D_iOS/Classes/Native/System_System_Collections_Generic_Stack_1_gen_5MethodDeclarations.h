﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor()
#define Stack_1__ctor_m17156(__this, method) (( void (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1__ctor_m11585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m17157(__this, method) (( bool (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m11586_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m17158(__this, method) (( Object_t * (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m11587_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m17159(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2307 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m11588_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17160(__this, method) (( Object_t* (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11589_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m17161(__this, method) (( Object_t * (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m11590_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Peek()
#define Stack_1_Peek_m17162(__this, method) (( List_1_t417 * (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_Peek_m11591_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Pop()
#define Stack_1_Pop_m17163(__this, method) (( List_1_t417 * (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_Pop_m11592_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Push(T)
#define Stack_1_Push_m17164(__this, ___t, method) (( void (*) (Stack_1_t2307 *, List_1_t417 *, const MethodInfo*))Stack_1_Push_m11593_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_Count()
#define Stack_1_get_Count_m17165(__this, method) (( int32_t (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_get_Count_m11594_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::GetEnumerator()
#define Stack_1_GetEnumerator_m17166(__this, method) (( Enumerator_t2527  (*) (Stack_1_t2307 *, const MethodInfo*))Stack_1_GetEnumerator_m11595_gshared)(__this, method)
