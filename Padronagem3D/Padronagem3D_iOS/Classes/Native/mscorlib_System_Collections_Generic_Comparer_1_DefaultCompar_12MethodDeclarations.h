﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t2457;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m18582_gshared (DefaultComparer_t2457 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18582(__this, method) (( void (*) (DefaultComparer_t2457 *, const MethodInfo*))DefaultComparer__ctor_m18582_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18583_gshared (DefaultComparer_t2457 * __this, DateTimeOffset_t1698  ___x, DateTimeOffset_t1698  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18583(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2457 *, DateTimeOffset_t1698 , DateTimeOffset_t1698 , const MethodInfo*))DefaultComparer_Compare_m18583_gshared)(__this, ___x, ___y, method)
