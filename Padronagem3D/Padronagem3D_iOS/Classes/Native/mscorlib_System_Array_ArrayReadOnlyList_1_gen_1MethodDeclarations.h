﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t2432;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1811;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t2543;
// System.Exception
struct Exception_t108;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m18370_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgumentU5BU5D_t1811* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m18370(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgumentU5BU5D_t1811*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m18370_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18371_gshared (ArrayReadOnlyList_1_t2432 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18371(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2432 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18371_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1373  ArrayReadOnlyList_1_get_Item_m18372_gshared (ArrayReadOnlyList_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m18372(__this, ___index, method) (( CustomAttributeNamedArgument_t1373  (*) (ArrayReadOnlyList_1_t2432 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m18372_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m18373_gshared (ArrayReadOnlyList_1_t2432 * __this, int32_t ___index, CustomAttributeNamedArgument_t1373  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m18373(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, int32_t, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m18373_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m18374_gshared (ArrayReadOnlyList_1_t2432 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m18374(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2432 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m18374_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m18375_gshared (ArrayReadOnlyList_1_t2432 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m18375(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2432 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m18375_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m18376_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m18376(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_Add_m18376_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m18377_gshared (ArrayReadOnlyList_1_t2432 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m18377(__this, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m18377_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m18378_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m18378(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m18378_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m18379_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgumentU5BU5D_t1811* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m18379(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgumentU5BU5D_t1811*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m18379_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m18380_gshared (ArrayReadOnlyList_1_t2432 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m18380(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2432 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m18380_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m18381_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m18381(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m18381_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m18382_gshared (ArrayReadOnlyList_1_t2432 * __this, int32_t ___index, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m18382(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, int32_t, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m18382_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m18383_gshared (ArrayReadOnlyList_1_t2432 * __this, CustomAttributeNamedArgument_t1373  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m18383(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2432 *, CustomAttributeNamedArgument_t1373 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m18383_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m18384_gshared (ArrayReadOnlyList_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m18384(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2432 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m18384_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t108 * ArrayReadOnlyList_1_ReadOnlyError_m18385_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m18385(__this /* static, unused */, method) (( Exception_t108 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m18385_gshared)(__this /* static, unused */, method)
