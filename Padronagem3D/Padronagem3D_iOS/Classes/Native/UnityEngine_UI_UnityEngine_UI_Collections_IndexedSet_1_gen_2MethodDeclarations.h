﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2192;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Predicate`1<System.Object>
struct Predicate_1_t1861;
// System.Comparison`1<System.Object>
struct Comparison_1_t1867;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m15366_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m15366(__this, method) (( void (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1__ctor_m15366_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15368(__this, method) (( Object_t * (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m15370_gshared (IndexedSet_1_t2192 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m15370(__this, ___item, method) (( void (*) (IndexedSet_1_t2192 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m15370_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m15372_gshared (IndexedSet_1_t2192 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m15372(__this, ___item, method) (( bool (*) (IndexedSet_1_t2192 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m15372_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m15374_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m15374(__this, method) (( Object_t* (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m15374_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m15376_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m15376(__this, method) (( void (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1_Clear_m15376_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m15378_gshared (IndexedSet_1_t2192 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m15378(__this, ___item, method) (( bool (*) (IndexedSet_1_t2192 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m15378_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m15380_gshared (IndexedSet_1_t2192 * __this, ObjectU5BU5D_t115* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m15380(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2192 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m15380_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m15382_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m15382(__this, method) (( int32_t (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1_get_Count_m15382_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m15384_gshared (IndexedSet_1_t2192 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m15384(__this, method) (( bool (*) (IndexedSet_1_t2192 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m15384_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m15386_gshared (IndexedSet_1_t2192 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m15386(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2192 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m15386_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m15388_gshared (IndexedSet_1_t2192 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m15388(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2192 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m15388_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m15390_gshared (IndexedSet_1_t2192 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m15390(__this, ___index, method) (( void (*) (IndexedSet_1_t2192 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m15390_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m15392_gshared (IndexedSet_1_t2192 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m15392(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2192 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m15392_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m15394_gshared (IndexedSet_1_t2192 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m15394(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2192 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m15394_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m15395_gshared (IndexedSet_1_t2192 * __this, Predicate_1_t1861 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m15395(__this, ___match, method) (( void (*) (IndexedSet_1_t2192 *, Predicate_1_t1861 *, const MethodInfo*))IndexedSet_1_RemoveAll_m15395_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m15396_gshared (IndexedSet_1_t2192 * __this, Comparison_1_t1867 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m15396(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2192 *, Comparison_1_t1867 *, const MethodInfo*))IndexedSet_1_Sort_m15396_gshared)(__this, ___sortLayoutFunction, method)
