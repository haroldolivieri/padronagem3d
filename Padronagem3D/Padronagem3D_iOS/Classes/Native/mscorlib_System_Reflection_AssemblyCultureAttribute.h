﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Reflection.AssemblyCultureAttribute
struct  AssemblyCultureAttribute_t1140  : public Attribute_t215
{
	// System.String System.Reflection.AssemblyCultureAttribute::name
	String_t* ___name_0;
};
