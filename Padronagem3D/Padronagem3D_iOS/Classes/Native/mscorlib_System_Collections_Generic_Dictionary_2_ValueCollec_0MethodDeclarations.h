﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14141(__this, ___host, method) (( void (*) (Enumerator_t459 *, Dictionary_2_t325 *, const MethodInfo*))Enumerator__ctor_m14065_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14142(__this, method) (( Object_t * (*) (Enumerator_t459 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14066_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14143(__this, method) (( void (*) (Enumerator_t459 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m14144(__this, method) (( void (*) (Enumerator_t459 *, const MethodInfo*))Enumerator_Dispose_m14068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m14145(__this, method) (( bool (*) (Enumerator_t459 *, const MethodInfo*))Enumerator_MoveNext_m14069_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m14146(__this, method) (( GUIStyle_t315 * (*) (Enumerator_t459 *, const MethodInfo*))Enumerator_get_Current_m14070_gshared)(__this, method)
