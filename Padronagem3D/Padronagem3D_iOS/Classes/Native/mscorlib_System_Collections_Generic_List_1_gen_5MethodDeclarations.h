﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t423;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color32>
struct IEnumerable_1_t2484;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t2485;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Color32>
struct ICollection_1_t2486;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t1974;
// UnityEngine.Color32[]
struct Color32U5BU5D_t422;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t1979;
// System.Comparison`1<UnityEngine.Color32>
struct Comparison_1_t1982;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
extern "C" void List_1__ctor_m12561_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1__ctor_m12561(__this, method) (( void (*) (List_1_t423 *, const MethodInfo*))List_1__ctor_m12561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12562_gshared (List_1_t423 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12562(__this, ___collection, method) (( void (*) (List_1_t423 *, Object_t*, const MethodInfo*))List_1__ctor_m12562_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12563_gshared (List_1_t423 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12563(__this, ___capacity, method) (( void (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1__ctor_m12563_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.cctor()
extern "C" void List_1__cctor_m12564_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12564(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12564_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12565_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12565(__this, method) (( Object_t* (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12565_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12566_gshared (List_1_t423 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12566(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t423 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12566_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12567_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12567(__this, method) (( Object_t * (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12567_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12568_gshared (List_1_t423 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12568(__this, ___item, method) (( int32_t (*) (List_1_t423 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12568_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12569_gshared (List_1_t423 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12569(__this, ___item, method) (( bool (*) (List_1_t423 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12569_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12570_gshared (List_1_t423 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12570(__this, ___item, method) (( int32_t (*) (List_1_t423 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12570_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12571_gshared (List_1_t423 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12571(__this, ___index, ___item, method) (( void (*) (List_1_t423 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12571_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12572_gshared (List_1_t423 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12572(__this, ___item, method) (( void (*) (List_1_t423 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12572_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12573_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12573(__this, method) (( bool (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12573_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12574_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12574(__this, method) (( bool (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12574_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12575_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12575(__this, method) (( Object_t * (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12576_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12576(__this, method) (( bool (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12577_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12577(__this, method) (( bool (*) (List_1_t423 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12577_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12578_gshared (List_1_t423 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12578(__this, ___index, method) (( Object_t * (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12578_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12579_gshared (List_1_t423 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12579(__this, ___index, ___value, method) (( void (*) (List_1_t423 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12579_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(T)
extern "C" void List_1_Add_m12580_gshared (List_1_t423 * __this, Color32_t187  ___item, const MethodInfo* method);
#define List_1_Add_m12580(__this, ___item, method) (( void (*) (List_1_t423 *, Color32_t187 , const MethodInfo*))List_1_Add_m12580_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12581_gshared (List_1_t423 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12581(__this, ___newCount, method) (( void (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12581_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12582_gshared (List_1_t423 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12582(__this, ___collection, method) (( void (*) (List_1_t423 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12582_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12583_gshared (List_1_t423 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12583(__this, ___enumerable, method) (( void (*) (List_1_t423 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12583_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3766_gshared (List_1_t423 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3766(__this, ___collection, method) (( void (*) (List_1_t423 *, Object_t*, const MethodInfo*))List_1_AddRange_m3766_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1974 * List_1_AsReadOnly_m12584_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12584(__this, method) (( ReadOnlyCollection_1_t1974 * (*) (List_1_t423 *, const MethodInfo*))List_1_AsReadOnly_m12584_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear()
extern "C" void List_1_Clear_m12585_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_Clear_m12585(__this, method) (( void (*) (List_1_t423 *, const MethodInfo*))List_1_Clear_m12585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Contains(T)
extern "C" bool List_1_Contains_m12586_gshared (List_1_t423 * __this, Color32_t187  ___item, const MethodInfo* method);
#define List_1_Contains_m12586(__this, ___item, method) (( bool (*) (List_1_t423 *, Color32_t187 , const MethodInfo*))List_1_Contains_m12586_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12587_gshared (List_1_t423 * __this, Color32U5BU5D_t422* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12587(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t423 *, Color32U5BU5D_t422*, int32_t, const MethodInfo*))List_1_CopyTo_m12587_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::Find(System.Predicate`1<T>)
extern "C" Color32_t187  List_1_Find_m12588_gshared (List_1_t423 * __this, Predicate_1_t1979 * ___match, const MethodInfo* method);
#define List_1_Find_m12588(__this, ___match, method) (( Color32_t187  (*) (List_1_t423 *, Predicate_1_t1979 *, const MethodInfo*))List_1_Find_m12588_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12589_gshared (Object_t * __this /* static, unused */, Predicate_1_t1979 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12589(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1979 *, const MethodInfo*))List_1_CheckMatch_m12589_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12590_gshared (List_1_t423 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1979 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12590(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t423 *, int32_t, int32_t, Predicate_1_t1979 *, const MethodInfo*))List_1_GetIndex_m12590_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Enumerator_t1973  List_1_GetEnumerator_m12591_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12591(__this, method) (( Enumerator_t1973  (*) (List_1_t423 *, const MethodInfo*))List_1_GetEnumerator_m12591_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12592_gshared (List_1_t423 * __this, Color32_t187  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12592(__this, ___item, method) (( int32_t (*) (List_1_t423 *, Color32_t187 , const MethodInfo*))List_1_IndexOf_m12592_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12593_gshared (List_1_t423 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12593(__this, ___start, ___delta, method) (( void (*) (List_1_t423 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12593_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12594_gshared (List_1_t423 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12594(__this, ___index, method) (( void (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12594_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12595_gshared (List_1_t423 * __this, int32_t ___index, Color32_t187  ___item, const MethodInfo* method);
#define List_1_Insert_m12595(__this, ___index, ___item, method) (( void (*) (List_1_t423 *, int32_t, Color32_t187 , const MethodInfo*))List_1_Insert_m12595_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12596_gshared (List_1_t423 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12596(__this, ___collection, method) (( void (*) (List_1_t423 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12596_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Remove(T)
extern "C" bool List_1_Remove_m12597_gshared (List_1_t423 * __this, Color32_t187  ___item, const MethodInfo* method);
#define List_1_Remove_m12597(__this, ___item, method) (( bool (*) (List_1_t423 *, Color32_t187 , const MethodInfo*))List_1_Remove_m12597_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12598_gshared (List_1_t423 * __this, Predicate_1_t1979 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12598(__this, ___match, method) (( int32_t (*) (List_1_t423 *, Predicate_1_t1979 *, const MethodInfo*))List_1_RemoveAll_m12598_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12599_gshared (List_1_t423 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12599(__this, ___index, method) (( void (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12599_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Reverse()
extern "C" void List_1_Reverse_m12600_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_Reverse_m12600(__this, method) (( void (*) (List_1_t423 *, const MethodInfo*))List_1_Reverse_m12600_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort()
extern "C" void List_1_Sort_m12601_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_Sort_m12601(__this, method) (( void (*) (List_1_t423 *, const MethodInfo*))List_1_Sort_m12601_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12602_gshared (List_1_t423 * __this, Comparison_1_t1982 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12602(__this, ___comparison, method) (( void (*) (List_1_t423 *, Comparison_1_t1982 *, const MethodInfo*))List_1_Sort_m12602_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Color32>::ToArray()
extern "C" Color32U5BU5D_t422* List_1_ToArray_m12603_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_ToArray_m12603(__this, method) (( Color32U5BU5D_t422* (*) (List_1_t423 *, const MethodInfo*))List_1_ToArray_m12603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::TrimExcess()
extern "C" void List_1_TrimExcess_m12604_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12604(__this, method) (( void (*) (List_1_t423 *, const MethodInfo*))List_1_TrimExcess_m12604_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12605_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12605(__this, method) (( int32_t (*) (List_1_t423 *, const MethodInfo*))List_1_get_Capacity_m12605_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12606_gshared (List_1_t423 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12606(__this, ___value, method) (( void (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12606_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t List_1_get_Count_m12607_gshared (List_1_t423 * __this, const MethodInfo* method);
#define List_1_get_Count_m12607(__this, method) (( int32_t (*) (List_1_t423 *, const MethodInfo*))List_1_get_Count_m12607_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t187  List_1_get_Item_m12608_gshared (List_1_t423 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12608(__this, ___index, method) (( Color32_t187  (*) (List_1_t423 *, int32_t, const MethodInfo*))List_1_get_Item_m12608_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12609_gshared (List_1_t423 * __this, int32_t ___index, Color32_t187  ___value, const MethodInfo* method);
#define List_1_set_Item_m12609(__this, ___index, ___value, method) (( void (*) (List_1_t423 *, int32_t, Color32_t187 , const MethodInfo*))List_1_set_Item_m12609_gshared)(__this, ___index, ___value, method)
