﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// Teleport
struct  Teleport_t1  : public MonoBehaviour_t2
{
	// UnityEngine.Vector3 Teleport::startingPosition
	Vector3_t3  ___startingPosition_2;
};
