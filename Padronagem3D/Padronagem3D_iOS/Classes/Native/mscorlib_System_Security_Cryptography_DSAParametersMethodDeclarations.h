﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void DSAParameters_t967_marshal(const DSAParameters_t967& unmarshaled, DSAParameters_t967_marshaled& marshaled);
extern "C" void DSAParameters_t967_marshal_back(const DSAParameters_t967_marshaled& marshaled, DSAParameters_t967& unmarshaled);
extern "C" void DSAParameters_t967_marshal_cleanup(DSAParameters_t967_marshaled& marshaled);
