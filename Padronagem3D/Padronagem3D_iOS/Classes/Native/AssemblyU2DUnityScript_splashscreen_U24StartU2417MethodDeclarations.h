﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// splashscreen/$Start$17
struct U24StartU2417_t137;
// splashscreen
struct splashscreen_t136;
// System.Collections.Generic.IEnumerator`1<UnityEngine.YieldInstruction>
struct IEnumerator_1_t146;

#include "codegen/il2cpp-codegen.h"

// System.Void splashscreen/$Start$17::.ctor(splashscreen)
extern "C" void U24StartU2417__ctor_m639 (U24StartU2417_t137 * __this, splashscreen_t136 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.YieldInstruction> splashscreen/$Start$17::GetEnumerator()
extern "C" Object_t* U24StartU2417_GetEnumerator_m640 (U24StartU2417_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
