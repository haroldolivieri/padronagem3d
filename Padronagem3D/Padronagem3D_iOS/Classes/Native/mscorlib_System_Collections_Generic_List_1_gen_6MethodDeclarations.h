﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t424;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2487;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2475;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2488;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t1984;
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Predicate`1<System.Int32>
struct Predicate_1_t1990;
// System.Comparison`1<System.Int32>
struct Comparison_1_t1994;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m12705_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1__ctor_m12705(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1__ctor_m12705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12706_gshared (List_1_t424 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12706(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1__ctor_m12706_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12707_gshared (List_1_t424 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12707(__this, ___capacity, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1__ctor_m12707_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m12708_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12708(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12709_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12709(__this, method) (( Object_t* (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12710_gshared (List_1_t424 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12710(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t424 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12710_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12711_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12711(__this, method) (( Object_t * (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12711_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12712_gshared (List_1_t424 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12712(__this, ___item, method) (( int32_t (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12712_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12713_gshared (List_1_t424 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12713(__this, ___item, method) (( bool (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12713_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12714_gshared (List_1_t424 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12714(__this, ___item, method) (( int32_t (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12714_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12715_gshared (List_1_t424 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12715(__this, ___index, ___item, method) (( void (*) (List_1_t424 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12715_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12716_gshared (List_1_t424 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12716(__this, ___item, method) (( void (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12717_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12717(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12717_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12718_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12718(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12718_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12719_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12719(__this, method) (( Object_t * (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12720_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12720(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12720_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12721_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12721(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12721_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12722_gshared (List_1_t424 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12722(__this, ___index, method) (( Object_t * (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12722_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12723_gshared (List_1_t424 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12723(__this, ___index, ___value, method) (( void (*) (List_1_t424 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12723_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m12724_gshared (List_1_t424 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m12724(__this, ___item, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_Add_m12724_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12725_gshared (List_1_t424 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12725(__this, ___newCount, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12725_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12726_gshared (List_1_t424 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12726(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12726_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12727_gshared (List_1_t424 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12727(__this, ___enumerable, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12727_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3769_gshared (List_1_t424 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3769(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddRange_m3769_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1984 * List_1_AsReadOnly_m12728_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12728(__this, method) (( ReadOnlyCollection_1_t1984 * (*) (List_1_t424 *, const MethodInfo*))List_1_AsReadOnly_m12728_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m12729_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_Clear_m12729(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Clear_m12729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m12730_gshared (List_1_t424 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m12730(__this, ___item, method) (( bool (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_Contains_m12730_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12731_gshared (List_1_t424 * __this, Int32U5BU5D_t107* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12731(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t424 *, Int32U5BU5D_t107*, int32_t, const MethodInfo*))List_1_CopyTo_m12731_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m12732_gshared (List_1_t424 * __this, Predicate_1_t1990 * ___match, const MethodInfo* method);
#define List_1_Find_m12732(__this, ___match, method) (( int32_t (*) (List_1_t424 *, Predicate_1_t1990 *, const MethodInfo*))List_1_Find_m12732_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12733_gshared (Object_t * __this /* static, unused */, Predicate_1_t1990 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12733(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1990 *, const MethodInfo*))List_1_CheckMatch_m12733_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12734_gshared (List_1_t424 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1990 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12734(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t424 *, int32_t, int32_t, Predicate_1_t1990 *, const MethodInfo*))List_1_GetIndex_m12734_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t1983  List_1_GetEnumerator_m12735_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12735(__this, method) (( Enumerator_t1983  (*) (List_1_t424 *, const MethodInfo*))List_1_GetEnumerator_m12735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12736_gshared (List_1_t424 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m12736(__this, ___item, method) (( int32_t (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_IndexOf_m12736_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12737_gshared (List_1_t424 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12737(__this, ___start, ___delta, method) (( void (*) (List_1_t424 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12737_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12738_gshared (List_1_t424 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12738(__this, ___index, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12738_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12739_gshared (List_1_t424 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m12739(__this, ___index, ___item, method) (( void (*) (List_1_t424 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m12739_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12740_gshared (List_1_t424 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12740(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12740_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m12741_gshared (List_1_t424 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m12741(__this, ___item, method) (( bool (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_Remove_m12741_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12742_gshared (List_1_t424 * __this, Predicate_1_t1990 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12742(__this, ___match, method) (( int32_t (*) (List_1_t424 *, Predicate_1_t1990 *, const MethodInfo*))List_1_RemoveAll_m12742_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12743_gshared (List_1_t424 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12743(__this, ___index, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12743_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m12744_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_Reverse_m12744(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Reverse_m12744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m12745_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_Sort_m12745(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Sort_m12745_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12746_gshared (List_1_t424 * __this, Comparison_1_t1994 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12746(__this, ___comparison, method) (( void (*) (List_1_t424 *, Comparison_1_t1994 *, const MethodInfo*))List_1_Sort_m12746_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t107* List_1_ToArray_m12747_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_ToArray_m12747(__this, method) (( Int32U5BU5D_t107* (*) (List_1_t424 *, const MethodInfo*))List_1_ToArray_m12747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m12748_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12748(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_TrimExcess_m12748_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12749_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12749(__this, method) (( int32_t (*) (List_1_t424 *, const MethodInfo*))List_1_get_Capacity_m12749_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12750_gshared (List_1_t424 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12750(__this, ___value, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12750_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m12751_gshared (List_1_t424 * __this, const MethodInfo* method);
#define List_1_get_Count_m12751(__this, method) (( int32_t (*) (List_1_t424 *, const MethodInfo*))List_1_get_Count_m12751_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m12752_gshared (List_1_t424 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12752(__this, ___index, method) (( int32_t (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_get_Item_m12752_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12753_gshared (List_1_t424 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m12753(__this, ___index, ___value, method) (( void (*) (List_1_t424 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m12753_gshared)(__this, ___index, ___value, method)
