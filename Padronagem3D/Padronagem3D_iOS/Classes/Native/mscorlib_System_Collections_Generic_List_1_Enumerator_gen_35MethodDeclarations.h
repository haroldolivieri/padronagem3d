﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m17130(__this, ___l, method) (( void (*) (Enumerator_t2302 *, List_1_t647 *, const MethodInfo*))Enumerator__ctor_m11163_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17131(__this, method) (( void (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11164_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17132(__this, method) (( Object_t * (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m17133(__this, method) (( void (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_Dispose_m11166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::VerifyState()
#define Enumerator_VerifyState_m17134(__this, method) (( void (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_VerifyState_m11167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m17135(__this, method) (( bool (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_MoveNext_m11168_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m17136(__this, method) (( RectTransform_t211 * (*) (Enumerator_t2302 *, const MethodInfo*))Enumerator_get_Current_m11169_gshared)(__this, method)
