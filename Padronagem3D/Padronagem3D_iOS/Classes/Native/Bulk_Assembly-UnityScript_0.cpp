﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DiveFPSController/$Die$8/$
struct U24_t124;
// DiveFPSController
struct DiveFPSController_t128;
// UnityEngine.GUITexture
struct GUITexture_t141;
// System.Object
struct Object_t;
// DiveFPSController/$Die$8
struct U24DieU248_t129;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AsyncOperation>
struct IEnumerator_1_t144;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t145;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// UnityEngine.Collider
struct Collider_t79;
// fps
struct fps_t132;
// UnityEngine.GUIText
struct GUIText_t152;
// splashscreen/$Start$17/$
struct U24_t134;
// splashscreen
struct splashscreen_t136;
// splashscreen/$Start$17
struct U24StartU2417_t137;
// System.Collections.Generic.IEnumerator`1<UnityEngine.YieldInstruction>
struct IEnumerator_1_t146;
// splashscreen/$FadeGUITexture$22/$
struct U24_t139;
// splashscreen/$FadeGUITexture$22
struct U24FadeGUITextureU2422_t142;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E.h"
#include "AssemblyU2DUnityScript_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DUnityScript_DiveFPSController_U24DieU248_U24.h"
#include "AssemblyU2DUnityScript_DiveFPSController_U24DieU248_U24MethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "AssemblyU2DUnityScript_DiveFPSController.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_genMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_GUITexture.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#include "AssemblyU2DUnityScript_DiveFPSController_U24DieU248.h"
#include "AssemblyU2DUnityScript_DiveFPSController_U24DieU248MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_genMethodDeclarations.h"
#include "AssemblyU2DUnityScript_DiveFPSControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterControllerMethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServicesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit.h"
#include "UnityEngine_UnityEngine_ControllerColliderHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_ExtensionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"
#include "mscorlib_System_Enum.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "AssemblyU2DUnityScript_fps.h"
#include "AssemblyU2DUnityScript_fpsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"
#include "AssemblyU2DUnityScript_Fade.h"
#include "AssemblyU2DUnityScript_FadeMethodDeclarations.h"
#include "AssemblyU2DUnityScript_splashscreen_U24StartU2417_U24.h"
#include "AssemblyU2DUnityScript_splashscreen_U24StartU2417_U24MethodDeclarations.h"
#include "AssemblyU2DUnityScript_splashscreen.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_0.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "AssemblyU2DUnityScript_splashscreenMethodDeclarations.h"
#include "AssemblyU2DUnityScript_splashscreen_U24StartU2417.h"
#include "AssemblyU2DUnityScript_splashscreen_U24StartU2417MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen_0MethodDeclarations.h"
#include "AssemblyU2DUnityScript_splashscreen_U24FadeGUITextureU2422_U.h"
#include "AssemblyU2DUnityScript_splashscreen_U24FadeGUITextureU2422_UMethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_1MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen_1.h"
#include "AssemblyU2DUnityScript_splashscreen_U24FadeGUITextureU2422.h"
#include "AssemblyU2DUnityScript_splashscreen_U24FadeGUITextureU2422MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen_1MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m607_gshared (GameObject_t47 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m607(__this, method) (( Object_t * (*) (GameObject_t47 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m607_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUITexture>()
#define GameObject_GetComponent_TisGUITexture_t141_m653(__this, method) (( GUITexture_t141 * (*) (GameObject_t47 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m607_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m602_gshared (Component_t122 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m602(__this, method) (( Object_t * (*) (Component_t122 *, const MethodInfo*))Component_GetComponent_TisObject_t_m602_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t152_m688(__this, method) (( GUIText_t152 * (*) (Component_t122 *, const MethodInfo*))Component_GetComponent_TisObject_t_m602_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DiveFPSController/$Die$8/$::.ctor(DiveFPSController)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m649_MethodInfo_var;
extern "C" void U24__ctor_m616 (U24_t124 * __this, DiveFPSController_t128 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGeneratorEnumerator_1__ctor_m649_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m649(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m649_MethodInfo_var);
		DiveFPSController_t128 * L_0 = ___self_;
		__this->___U24self_U2415_8 = L_0;
		return;
	}
}
// System.Boolean DiveFPSController/$Die$8/$::MoveNext()
extern const Il2CppType* GUITexture_t141_0_0_0_var;
extern TypeInfo* GameObject_t47_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t126_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m661_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m664_MethodInfo_var;
extern "C" bool U24_MoveNext_m617 (U24_t124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUITexture_t141_0_0_0_var = il2cpp_codegen_type_from_index(70);
		GameObject_t47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Texture2D_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(72);
		GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		GenericGeneratorEnumerator_1_YieldDefault_m661_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		GenericGeneratorEnumerator_1_Yield_m664_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Color_t11  V_1 = {0};
	float V_2 = 0.0f;
	Color_t11  V_3 = {0};
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = (((GenericGeneratorEnumerator_1_t125 *)__this)->____state_1);
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_0187;
		}
		if (L_0 == 2)
		{
			goto IL_0131;
		}
		if (L_0 == 3)
		{
			goto IL_017f;
		}
	}

IL_001b:
	{
		GameObject_t47 * L_1 = (GameObject_t47 *)il2cpp_codegen_object_new (GameObject_t47_il2cpp_TypeInfo_var);
		GameObject__ctor_m650(L_1, /*hidden argument*/NULL);
		__this->___U24fadeU249_2 = L_1;
		GameObject_t47 * L_2 = (__this->___U24fadeU249_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m651(NULL /*static, unused*/, LoadTypeToken(GUITexture_t141_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_AddComponent_m652(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t47 * L_4 = (__this->___U24fadeU249_2);
		NullCheck(L_4);
		GUITexture_t141 * L_5 = GameObject_GetComponent_TisGUITexture_t141_m653(L_4, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var);
		int32_t L_6 = Screen_get_width_m353(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_m355(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t18  L_8 = {0};
		Rect__ctor_m337(&L_8, (((float)((float)0))), (((float)((float)0))), (((float)((float)L_6))), (((float)((float)L_7))), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUITexture_set_pixelInset_m654(L_5, L_8, /*hidden argument*/NULL);
		Texture2D_t126 * L_9 = (Texture2D_t126 *)il2cpp_codegen_object_new (Texture2D_t126_il2cpp_TypeInfo_var);
		Texture2D__ctor_m655(L_9, 1, 1, /*hidden argument*/NULL);
		__this->___U24texU2410_3 = L_9;
		Texture2D_t126 * L_10 = (__this->___U24texU2410_3);
		Color_t11  L_11 = Color_get_black_m416(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture2D_SetPixel_m656(L_10, 0, 0, L_11, /*hidden argument*/NULL);
		Texture2D_t126 * L_12 = (__this->___U24texU2410_3);
		NullCheck(L_12);
		Texture2D_Apply_m657(L_12, /*hidden argument*/NULL);
		GameObject_t47 * L_13 = (__this->___U24fadeU249_2);
		NullCheck(L_13);
		GUITexture_t141 * L_14 = GameObject_GetComponent_TisGUITexture_t141_m653(L_13, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var);
		Texture2D_t126 * L_15 = (__this->___U24texU2410_3);
		NullCheck(L_14);
		GUITexture_set_texture_m658(L_14, L_15, /*hidden argument*/NULL);
		__this->___U24alphaU2411_4 = (((float)((float)0)));
		goto IL_0131;
	}

IL_00ae:
	{
		float L_16 = (__this->___U24alphaU2411_4);
		float L_17 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		DiveFPSController_t128 * L_18 = (__this->___U24self_U2415_8);
		NullCheck(L_18);
		float L_19 = (L_18->___fadeduration_28);
		__this->___U24alphaU2411_4 = ((float)((float)L_16+(float)((float)((float)L_17/(float)L_19))));
		float L_20 = (__this->___U24alphaU2411_4);
		float L_21 = L_20;
		V_0 = L_21;
		__this->___U24U242U2413_6 = L_21;
		float L_22 = V_0;
		GameObject_t47 * L_23 = (__this->___U24fadeU249_2);
		NullCheck(L_23);
		GUITexture_t141 * L_24 = GameObject_GetComponent_TisGUITexture_t141_m653(L_23, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var);
		NullCheck(L_24);
		Color_t11  L_25 = GUITexture_get_color_m659(L_24, /*hidden argument*/NULL);
		Color_t11  L_26 = L_25;
		V_1 = L_26;
		__this->___U24U243U2414_7 = L_26;
		Color_t11  L_27 = V_1;
		Color_t11 * L_28 = &(__this->___U24U243U2414_7);
		float L_29 = (__this->___U24U242U2413_6);
		float L_30 = L_29;
		V_2 = L_30;
		L_28->___a_3 = L_30;
		float L_31 = V_2;
		GameObject_t47 * L_32 = (__this->___U24fadeU249_2);
		NullCheck(L_32);
		GUITexture_t141 * L_33 = GameObject_GetComponent_TisGUITexture_t141_m653(L_32, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t141_m653_MethodInfo_var);
		Color_t11  L_34 = (__this->___U24U243U2414_7);
		Color_t11  L_35 = L_34;
		V_3 = L_35;
		NullCheck(L_33);
		GUITexture_set_color_m660(L_33, L_35, /*hidden argument*/NULL);
		Color_t11  L_36 = V_3;
		bool L_37 = GenericGeneratorEnumerator_1_YieldDefault_m661(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m661_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_37));
		goto IL_0188;
	}

IL_0131:
	{
		float L_38 = (__this->___U24alphaU2411_4);
		if ((((float)L_38) < ((float)(1.0f))))
		{
			goto IL_00ae;
		}
	}
	{
		DiveFPSController_t128 * L_39 = (__this->___U24self_U2415_8);
		NullCheck(L_39);
		int32_t L_40 = (L_39->___reload_once_27);
		if (L_40)
		{
			goto IL_017f;
		}
	}
	{
		DiveFPSController_t128 * L_41 = (__this->___U24self_U2415_8);
		NullCheck(L_41);
		L_41->___reload_once_27 = 1;
		int32_t L_42 = Application_get_loadedLevel_m662(NULL /*static, unused*/, /*hidden argument*/NULL);
		AsyncOperation_t127 * L_43 = Application_LoadLevelAsync_m663(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		__this->___U24asyncU2412_5 = L_43;
		AsyncOperation_t127 * L_44 = (__this->___U24asyncU2412_5);
		bool L_45 = GenericGeneratorEnumerator_1_Yield_m664(__this, 3, L_44, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m664_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_45));
		goto IL_0188;
	}

IL_017f:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m661(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m661_MethodInfo_var);
	}

IL_0187:
	{
		G_B8_0 = 0;
	}

IL_0188:
	{
		return G_B8_0;
	}
}
// System.Void DiveFPSController/$Die$8::.ctor(DiveFPSController)
extern const MethodInfo* GenericGenerator_1__ctor_m665_MethodInfo_var;
extern "C" void U24DieU248__ctor_m618 (U24DieU248_t129 * __this, DiveFPSController_t128 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGenerator_1__ctor_m665_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m665(__this, /*hidden argument*/GenericGenerator_1__ctor_m665_MethodInfo_var);
		DiveFPSController_t128 * L_0 = ___self_;
		__this->___U24self_U2416_0 = L_0;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.AsyncOperation> DiveFPSController/$Die$8::GetEnumerator()
extern TypeInfo* U24_t124_il2cpp_TypeInfo_var;
extern "C" Object_t* U24DieU248_GetEnumerator_m619 (U24DieU248_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiveFPSController_t128 * L_0 = (__this->___U24self_U2416_0);
		U24_t124 * L_1 = (U24_t124 *)il2cpp_codegen_object_new (U24_t124_il2cpp_TypeInfo_var);
		U24__ctor_m616(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DiveFPSController::.ctor()
extern "C" void DiveFPSController__ctor_m620 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m320(__this, /*hidden argument*/NULL);
		__this->___downmovement_5 = (0.1f);
		__this->___max_speed_7 = (0.1f);
		__this->___max_speed_air_8 = (0.13f);
		__this->___max_speed_ground_9 = (0.1f);
		__this->___acceleration_10 = ((int32_t)10);
		__this->___acceleration_air_11 = ((int32_t)10);
		__this->___gravity_12 = (-0.18f);
		__this->___friction_13 = (0.8f);
		__this->___jumpspeed_17 = (0.16f);
		__this->___fallkillspeed_19 = (-0.38f);
		__this->___inhibit_autowalk_26 = 1;
		__this->___fadeduration_28 = (2.0f);
		return;
	}
}
// System.Void DiveFPSController::Awake()
extern const Il2CppType* CharacterController_t131_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharacterController_t131_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t113_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeServices_t148_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral50;
extern "C" void DiveFPSController_Awake_m621 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharacterController_t131_0_0_0_var = il2cpp_codegen_type_from_index(74);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CharacterController_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(74);
		Single_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		RuntimeServices_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		_stringLiteral50 = il2cpp_codegen_string_literal_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m651(NULL /*static, unused*/, LoadTypeToken(CharacterController_t131_0_0_0_var), /*hidden argument*/NULL);
		Component_t122 * L_1 = Component_GetComponent_m666(__this, L_0, /*hidden argument*/NULL);
		__this->___controller_15 = ((CharacterController_t131 *)CastclassSealed(L_1, CharacterController_t131_il2cpp_TypeInfo_var));
		CharacterController_t131 * L_2 = (__this->___controller_15);
		NullCheck(L_2);
		float L_3 = CharacterController_get_slopeLimit_m667(L_2, /*hidden argument*/NULL);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t113_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t148_il2cpp_TypeInfo_var);
		String_t* L_6 = RuntimeServices_op_Addition_m668(NULL /*static, unused*/, _stringLiteral50, L_5, /*hidden argument*/NULL);
		Debug_Log_m388(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DiveFPSController::toggle_autowalk()
extern "C" void DiveFPSController_toggle_autowalk_m622 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___autowalk_25);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->___autowalk_25 = 1;
		goto IL_001e;
	}

IL_0017:
	{
		__this->___autowalk_25 = 0;
	}

IL_001e:
	{
		return;
	}
}
// System.Void DiveFPSController::JumpUp()
extern "C" void DiveFPSController_JumpUp_m623 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		__this->___jumpcommand_23 = 1;
		return;
	}
}
// System.Void DiveFPSController::Start()
extern "C" void DiveFPSController_Start_m624 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		__this->___reload_once_27 = 0;
		return;
	}
}
// System.Void DiveFPSController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" void DiveFPSController_OnControllerColliderHit_m625 (DiveFPSController_t128 * __this, ControllerColliderHit_t145 * ___hit, const MethodInfo* method)
{
	Vector3_t3  V_0 = {0};
	Vector3_t3  V_1 = {0};
	{
		ControllerColliderHit_t145 * L_0 = ___hit;
		NullCheck(L_0);
		Vector3_t3  L_1 = ControllerColliderHit_get_normal_m669(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		if ((((float)L_2) <= ((float)(((float)((float)0))))))
		{
			goto IL_0050;
		}
	}
	{
		ControllerColliderHit_t145 * L_3 = ___hit;
		NullCheck(L_3);
		Vector3_t3  L_4 = ControllerColliderHit_get_moveDirection_m670(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___y_2);
		if ((((float)L_5) >= ((float)(((float)((float)0))))))
		{
			goto IL_0050;
		}
	}
	{
		ControllerColliderHit_t145 * L_6 = ___hit;
		NullCheck(L_6);
		Vector3_t3  L_7 = ControllerColliderHit_get_normal_m669(L_6, /*hidden argument*/NULL);
		__this->___groundNormal_16 = L_7;
		__this->___grounded_4 = 1;
		ControllerColliderHit_t145 * L_8 = ___hit;
		NullCheck(L_8);
		GameObject_t47 * L_9 = ControllerColliderHit_get_gameObject_m671(L_8, /*hidden argument*/NULL);
		__this->___ground_gameobject_21 = L_9;
		__this->___stopmovingup_18 = 0;
	}

IL_0050:
	{
		return;
	}
}
// System.Collections.IEnumerator DiveFPSController::Die()
extern TypeInfo* U24DieU248_t129_il2cpp_TypeInfo_var;
extern "C" Object_t * DiveFPSController_Die_m626 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24DieU248_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24DieU248_t129 * L_0 = (U24DieU248_t129 *)il2cpp_codegen_object_new (U24DieU248_t129_il2cpp_TypeInfo_var);
		U24DieU248__ctor_m618(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t* L_1 = U24DieU248_GetEnumerator_m619(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DiveFPSController::Update()
extern TypeInfo* Input_t89_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t77_il2cpp_TypeInfo_var;
extern TypeInfo* CollisionFlags_t149_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral52;
extern Il2CppCodeGenString* _stringLiteral53;
extern Il2CppCodeGenString* _stringLiteral54;
extern "C" void DiveFPSController_Update_m627 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Mathf_t77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CollisionFlags_t149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		Vector3_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		_stringLiteral52 = il2cpp_codegen_string_literal_from_index(52);
		_stringLiteral53 = il2cpp_codegen_string_literal_from_index(53);
		_stringLiteral54 = il2cpp_codegen_string_literal_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3  V_0 = {0};
	float V_1 = 0.0f;
	Vector3_t3  V_2 = {0};
	Quaternion_t50  V_3 = {0};
	Vector3_t3  V_4 = {0};
	Quaternion_t50  V_5 = {0};
	Vector3_t3  V_6 = {0};
	{
		Vector3_t3 * L_0 = &(__this->___velocity_6);
		float L_1 = (L_0->___y_2);
		float L_2 = (__this->___fallkillspeed_19);
		if ((((float)L_1) >= ((float)L_2)))
		{
			goto IL_0023;
		}
	}
	{
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(9 /* System.Collections.IEnumerator DiveFPSController::Die() */, __this);
		MonoBehaviour_StartCoroutine_Auto_m672(__this, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		Transform_t33 * L_4 = Component_get_transform_m321(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3  L_5 = Transform_get_position_m481(L_4, /*hidden argument*/NULL);
		Transform_t33 * L_6 = Component_get_transform_m321(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3  L_7 = Transform_get_position_m481(L_6, /*hidden argument*/NULL);
		Vector3_t3  L_8 = (__this->___groundNormal_16);
		Vector3_t3  L_9 = Vector3_op_Addition_m443(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Color_t11  L_10 = Color_get_red_m326(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawLine_m673(NULL /*static, unused*/, L_5, L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t89_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m588(NULL /*static, unused*/, _stringLiteral51, /*hidden argument*/NULL);
		float L_12 = Input_GetAxis_m588(NULL /*static, unused*/, _stringLiteral52, /*hidden argument*/NULL);
		Vector3_t3  L_13 = {0};
		Vector3__ctor_m386(&L_13, L_11, (((float)((float)0))), L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (__this->___autowalk_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_15 = (__this->___inhibit_autowalk_26);
		Vector3_t3  L_16 = {0};
		Vector3__ctor_m386(&L_16, (((float)((float)0))), (((float)((float)0))), (((float)((float)((int32_t)((int32_t)1*(int32_t)L_15))))), /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0089:
	{
		Vector3_t3  L_17 = V_0;
		Vector3_t3  L_18 = Vector3_get_zero_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Inequality_m674(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00be;
		}
	}
	{
		float L_20 = Vector3_get_magnitude_m562((&V_0), /*hidden argument*/NULL);
		V_1 = L_20;
		Vector3_t3  L_21 = V_0;
		float L_22 = V_1;
		Vector3_t3  L_23 = Vector3_op_Division_m675(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		float L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t77_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Min_m676(NULL /*static, unused*/, (((float)((float)1))), L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		float L_26 = V_1;
		float L_27 = V_1;
		V_1 = ((float)((float)L_26*(float)L_27));
		Vector3_t3  L_28 = V_0;
		float L_29 = V_1;
		Vector3_t3  L_30 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
	}

IL_00be:
	{
		Vector3_t3  L_31 = V_0;
		__this->___inputMoveDirection_2 = L_31;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t89_il2cpp_TypeInfo_var);
		bool L_32 = Input_GetButton_m677(NULL /*static, unused*/, _stringLiteral53, /*hidden argument*/NULL);
		__this->___inputJump_3 = L_32;
		int32_t L_33 = (__this->___jumpcommand_23);
		if ((!(((uint32_t)L_33) == ((uint32_t)1))))
		{
			goto IL_00ef;
		}
	}
	{
		__this->___inputJump_3 = 1;
		__this->___jumpcommand_23 = 0;
	}

IL_00ef:
	{
		int32_t L_34 = (__this->___collisionFlags_20);
		int32_t L_35 = ((int32_t)((int32_t)L_34&(int32_t)4));
		Object_t * L_36 = Box(CollisionFlags_t149_il2cpp_TypeInfo_var, &L_35);
		bool L_37 = Extensions_op_Implicit_m678(NULL /*static, unused*/, (Enum_t150 *)L_36, /*hidden argument*/NULL);
		__this->___grounded_4 = L_37;
		bool L_38 = (__this->___floating_24);
		if (!L_38)
		{
			goto IL_0122;
		}
	}
	{
		Vector3_t3 * L_39 = &(__this->___velocity_6);
		L_39->___y_2 = (0.1f);
	}

IL_0122:
	{
		bool L_40 = (__this->___inputJump_3);
		if (!L_40)
		{
			goto IL_0178;
		}
	}
	{
		bool L_41 = (__this->___grounded_4);
		if (!L_41)
		{
			goto IL_0178;
		}
	}
	{
		Vector3_t3 * L_42 = &(__this->___velocity_6);
		Vector3_t3 * L_43 = &(__this->___velocity_6);
		float L_44 = (L_43->___y_2);
		float L_45 = (__this->___jumpspeed_17);
		L_42->___y_2 = ((float)((float)L_44+(float)L_45));
		Vector3_t3 * L_46 = &(__this->___velocity_6);
		Vector3_t3 * L_47 = &(__this->___velocity_6);
		float L_48 = (L_47->___z_3);
		L_46->___z_3 = ((float)((float)L_48*(float)(1.5f)));
		__this->___grounded_4 = 0;
	}

IL_0178:
	{
		bool L_49 = (__this->___grounded_4);
		if (!L_49)
		{
			goto IL_01b5;
		}
	}
	{
		Vector3_t3  L_50 = (__this->___velocity_6);
		Vector3_t3  L_51 = (__this->___inputMoveDirection_2);
		int32_t L_52 = (__this->___acceleration_10);
		Vector3_t3  L_53 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_51, (((float)((float)L_52))), /*hidden argument*/NULL);
		float L_54 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3  L_55 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		Vector3_t3  L_56 = Vector3_op_Addition_m443(NULL /*static, unused*/, L_50, L_55, /*hidden argument*/NULL);
		__this->___velocity_6 = L_56;
		goto IL_01e2;
	}

IL_01b5:
	{
		Vector3_t3  L_57 = (__this->___velocity_6);
		Vector3_t3  L_58 = (__this->___inputMoveDirection_2);
		int32_t L_59 = (__this->___acceleration_air_11);
		Vector3_t3  L_60 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_58, (((float)((float)L_59))), /*hidden argument*/NULL);
		float L_61 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3  L_62 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		Vector3_t3  L_63 = Vector3_op_Addition_m443(NULL /*static, unused*/, L_57, L_62, /*hidden argument*/NULL);
		__this->___velocity_6 = L_63;
	}

IL_01e2:
	{
		Vector3_t3 * L_64 = &(__this->___velocity_6);
		float L_65 = (L_64->___x_1);
		Vector3_t3 * L_66 = &(__this->___velocity_6);
		float L_67 = (L_66->___z_3);
		Vector3_t3  L_68 = {0};
		Vector3__ctor_m386(&L_68, L_65, (((float)((float)0))), L_67, /*hidden argument*/NULL);
		V_2 = L_68;
		Vector3_t3  L_69 = {0};
		Vector3__ctor_m386(&L_69, (((float)((float)0))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t3  L_70 = V_2;
		float L_71 = (__this->___friction_13);
		Vector3_t3  L_72 = Vector3_Lerp_m679(NULL /*static, unused*/, L_69, L_70, L_71, /*hidden argument*/NULL);
		V_2 = L_72;
		Vector3_t3 * L_73 = &(__this->___velocity_6);
		float L_74 = ((&V_2)->___x_1);
		L_73->___x_1 = L_74;
		Vector3_t3 * L_75 = &(__this->___velocity_6);
		float L_76 = ((&V_2)->___z_3);
		L_75->___z_3 = L_76;
		Vector3_t3 * L_77 = &(__this->___velocity_6);
		Vector3_t3 * L_78 = &(__this->___velocity_6);
		float L_79 = (L_78->___y_2);
		float L_80 = (__this->___gravity_12);
		float L_81 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_77->___y_2 = ((float)((float)L_79+(float)((float)((float)L_80*(float)L_81))));
		bool L_82 = (__this->___grounded_4);
		if (!L_82)
		{
			goto IL_0277;
		}
	}
	{
		Vector3_t3 * L_83 = &(__this->___velocity_6);
		L_83->___y_2 = (((float)((float)0)));
	}

IL_0277:
	{
		bool L_84 = (__this->___grounded_4);
		if (!L_84)
		{
			goto IL_028e;
		}
	}
	{
		float L_85 = (__this->___max_speed_ground_9);
		__this->___max_speed_7 = L_85;
	}

IL_028e:
	{
		bool L_86 = (__this->___grounded_4);
		if (L_86)
		{
			goto IL_02a5;
		}
	}
	{
		float L_87 = (__this->___max_speed_air_8);
		__this->___max_speed_7 = L_87;
	}

IL_02a5:
	{
		float L_88 = Vector3_get_magnitude_m562((&V_2), /*hidden argument*/NULL);
		float L_89 = (__this->___max_speed_7);
		if ((((float)L_88) <= ((float)L_89)))
		{
			goto IL_02d2;
		}
	}
	{
		Vector3_t3  L_90 = V_2;
		float L_91 = Vector3_get_magnitude_m562((&V_2), /*hidden argument*/NULL);
		Vector3_t3  L_92 = Vector3_op_Division_m675(NULL /*static, unused*/, L_90, L_91, /*hidden argument*/NULL);
		V_2 = L_92;
		Vector3_t3  L_93 = V_2;
		float L_94 = (__this->___max_speed_7);
		Vector3_t3  L_95 = Vector3_op_Multiply_m332(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		V_2 = L_95;
	}

IL_02d2:
	{
		Vector3_t3 * L_96 = &(__this->___velocity_6);
		float L_97 = ((&V_2)->___x_1);
		L_96->___x_1 = L_97;
		Vector3_t3 * L_98 = &(__this->___velocity_6);
		float L_99 = ((&V_2)->___z_3);
		L_98->___z_3 = L_99;
		Vector3_t3 * L_100 = &(__this->___velocity_6);
		float L_101 = (L_100->___y_2);
		(&V_2)->___y_2 = L_101;
		GameObject_t47 * L_102 = (__this->___cameraObject_14);
		NullCheck(L_102);
		Transform_t33 * L_103 = GameObject_get_transform_m396(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		Quaternion_t50  L_104 = Transform_get_rotation_m484(L_103, /*hidden argument*/NULL);
		V_5 = L_104;
		Vector3_t3  L_105 = Quaternion_get_eulerAngles_m680((&V_5), /*hidden argument*/NULL);
		V_6 = L_105;
		float L_106 = ((&V_6)->___y_2);
		Quaternion_t50  L_107 = Quaternion_Euler_m681(NULL /*static, unused*/, (((float)((float)0))), L_106, (((float)((float)0))), /*hidden argument*/NULL);
		V_3 = L_107;
		Initobj (Vector3_t3_il2cpp_TypeInfo_var, (&V_4));
		GameObject_t47 * L_108 = (__this->___ground_gameobject_21);
		bool L_109 = Object_op_Inequality_m349(NULL /*static, unused*/, L_108, (Object_t82 *)NULL, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_036a;
		}
	}
	{
		GameObject_t47 * L_110 = (__this->___ground_gameobject_21);
		NullCheck(L_110);
		Transform_t33 * L_111 = GameObject_get_transform_m396(L_110, /*hidden argument*/NULL);
		NullCheck(L_111);
		Vector3_t3  L_112 = Transform_get_position_m481(L_111, /*hidden argument*/NULL);
		Vector3_t3  L_113 = (__this->___last_ground_pos_22);
		Vector3_t3  L_114 = Vector3_op_Subtraction_m561(NULL /*static, unused*/, L_112, L_113, /*hidden argument*/NULL);
		V_4 = L_114;
	}

IL_036a:
	{
		CharacterController_t131 * L_115 = (__this->___controller_15);
		Quaternion_t50  L_116 = V_3;
		Vector3_t3  L_117 = V_2;
		Vector3_t3  L_118 = Quaternion_op_Multiply_m487(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		Vector3_t3  L_119 = V_4;
		Vector3_t3  L_120 = Vector3_op_Addition_m443(NULL /*static, unused*/, L_118, L_119, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_121 = CharacterController_Move_m682(L_115, L_120, /*hidden argument*/NULL);
		__this->___collisionFlags_20 = L_121;
		int32_t L_122 = (__this->___collisionFlags_20);
		if (!((int32_t)((int32_t)L_122&(int32_t)2)))
		{
			goto IL_03bf;
		}
	}
	{
		bool L_123 = (__this->___stopmovingup_18);
		if (L_123)
		{
			goto IL_03bf;
		}
	}
	{
		MonoBehaviour_print_m683(NULL /*static, unused*/, _stringLiteral54, /*hidden argument*/NULL);
		Vector3_t3 * L_124 = &(__this->___velocity_6);
		L_124->___y_2 = (((float)((float)0)));
		__this->___stopmovingup_18 = 1;
	}

IL_03bf:
	{
		GameObject_t47 * L_125 = (__this->___ground_gameobject_21);
		bool L_126 = Object_op_Inequality_m349(NULL /*static, unused*/, L_125, (Object_t82 *)NULL, /*hidden argument*/NULL);
		if (!L_126)
		{
			goto IL_03e2;
		}
	}
	{
		bool L_127 = (__this->___grounded_4);
		if (L_127)
		{
			goto IL_03e2;
		}
	}
	{
		__this->___ground_gameobject_21 = (GameObject_t47 *)NULL;
	}

IL_03e2:
	{
		GameObject_t47 * L_128 = (__this->___ground_gameobject_21);
		bool L_129 = Object_op_Inequality_m349(NULL /*static, unused*/, L_128, (Object_t82 *)NULL, /*hidden argument*/NULL);
		if (!L_129)
		{
			goto IL_0409;
		}
	}
	{
		GameObject_t47 * L_130 = (__this->___ground_gameobject_21);
		NullCheck(L_130);
		Transform_t33 * L_131 = GameObject_get_transform_m396(L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		Vector3_t3  L_132 = Transform_get_position_m481(L_131, /*hidden argument*/NULL);
		__this->___last_ground_pos_22 = L_132;
	}

IL_0409:
	{
		return;
	}
}
// System.Void DiveFPSController::LateUpdate()
extern "C" void DiveFPSController_LateUpdate_m628 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DiveFPSController::OnGUI()
extern TypeInfo* KeyCode_t151_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeServices_t148_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t80_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral55;
extern Il2CppCodeGenString* _stringLiteral56;
extern "C" void DiveFPSController_OnGUI_m629 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyCode_t151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(79);
		RuntimeServices_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		GUI_t80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		_stringLiteral55 = il2cpp_codegen_string_literal_from_index(55);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	Event_t84 * V_0 = {0};
	{
		Event_t84 * L_0 = Event_get_current_m359(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t84 * L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = Event_get_isKey_m684(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0059;
		}
	}
	{
		Event_t84 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = Event_get_keyCode_m685(L_3, /*hidden argument*/NULL);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(KeyCode_t151_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t148_il2cpp_TypeInfo_var);
		String_t* L_7 = RuntimeServices_op_Addition_m668(NULL /*static, unused*/, _stringLiteral55, L_6, /*hidden argument*/NULL);
		Debug_Log_m388(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Rect_t18  L_8 = {0};
		Rect__ctor_m337(&L_8, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)20)))), /*hidden argument*/NULL);
		Event_t84 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = Event_get_keyCode_m685(L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(KeyCode_t151_il2cpp_TypeInfo_var, &L_11);
		String_t* L_13 = RuntimeServices_op_Addition_m668(NULL /*static, unused*/, _stringLiteral56, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t80_il2cpp_TypeInfo_var);
		GUI_Label_m686(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void DiveFPSController::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral57;
extern "C" void DiveFPSController_OnTriggerEnter_m630 (DiveFPSController_t128 * __this, Collider_t79 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral57 = il2cpp_codegen_string_literal_from_index(57);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t79 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m558(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m687(NULL /*static, unused*/, L_1, _stringLiteral57, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		__this->___floating_24 = 1;
		__this->___inhibit_autowalk_26 = (((int32_t)((int32_t)(0.1f))));
	}

IL_0028:
	{
		return;
	}
}
// System.Void DiveFPSController::OnTriggerExit(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral57;
extern "C" void DiveFPSController_OnTriggerExit_m631 (DiveFPSController_t128 * __this, Collider_t79 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral57 = il2cpp_codegen_string_literal_from_index(57);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t79 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m558(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m687(NULL /*static, unused*/, L_1, _stringLiteral57, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		__this->___floating_24 = 0;
		__this->___inhibit_autowalk_26 = 1;
	}

IL_0023:
	{
		return;
	}
}
// System.Void DiveFPSController::Main()
extern "C" void DiveFPSController_Main_m632 (DiveFPSController_t128 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void fps::.ctor()
extern "C" void fps__ctor_m633 (fps_t132 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m320(__this, /*hidden argument*/NULL);
		__this->___updateInterval_2 = (0.5f);
		return;
	}
}
// System.Void fps::Start()
extern const MethodInfo* Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern "C" void fps_Start_m634 (fps_t132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIText_t152 * L_0 = Component_GetComponent_TisGUIText_t152_m688(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var);
		bool L_1 = Object_op_Implicit_m351(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_print_m683(NULL /*static, unused*/, _stringLiteral58, /*hidden argument*/NULL);
		Behaviour_set_enabled_m348(__this, 0, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0026:
	{
		float L_2 = (__this->___updateInterval_2);
		__this->___timeleft_5 = L_2;
	}

IL_0032:
	{
		return;
	}
}
// System.Void fps::Update()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeServices_t148_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral60;
extern "C" void fps_Update_m635 (fps_t132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		RuntimeServices_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		_stringLiteral59 = il2cpp_codegen_string_literal_from_index(59);
		_stringLiteral60 = il2cpp_codegen_string_literal_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___timeleft_5);
		float L_1 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___timeleft_5 = ((float)((float)L_0-(float)L_1));
		float L_2 = (__this->___accum_3);
		float L_3 = Time_get_timeScale_m689(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___accum_3 = ((float)((float)L_2+(float)((float)((float)L_3/(float)L_4))));
		int32_t L_5 = (__this->___frames_4);
		__this->___frames_4 = ((int32_t)((int32_t)L_5+(int32_t)1));
		float L_6 = (__this->___timeleft_5);
		if ((((float)L_6) > ((float)(((float)((float)0))))))
		{
			goto IL_009a;
		}
	}
	{
		GUIText_t152 * L_7 = Component_GetComponent_TisGUIText_t152_m688(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t152_m688_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		float L_9 = (__this->___accum_3);
		int32_t L_10 = (__this->___frames_4);
		V_0 = ((float)((float)L_9/(float)(((float)((float)L_10)))));
		String_t* L_11 = Single_ToString_m690((&V_0), _stringLiteral59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t148_il2cpp_TypeInfo_var);
		String_t* L_12 = RuntimeServices_op_Addition_m691(NULL /*static, unused*/, L_8, L_11, /*hidden argument*/NULL);
		String_t* L_13 = RuntimeServices_op_Addition_m691(NULL /*static, unused*/, L_12, _stringLiteral60, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIText_set_text_m692(L_7, L_13, /*hidden argument*/NULL);
		float L_14 = (__this->___updateInterval_2);
		__this->___timeleft_5 = L_14;
		__this->___accum_3 = (((float)((float)0)));
		__this->___frames_4 = 0;
	}

IL_009a:
	{
		return;
	}
}
// System.Void fps::Main()
extern "C" void fps_Main_m636 (fps_t132 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void splashscreen/$Start$17/$::.ctor(splashscreen)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m693_MethodInfo_var;
extern "C" void U24__ctor_m637 (U24_t134 * __this, splashscreen_t136 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGeneratorEnumerator_1__ctor_m693_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m693(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m693_MethodInfo_var);
		splashscreen_t136 * L_0 = ___self_;
		__this->___U24self_U2420_4 = L_0;
		return;
	}
}
// System.Boolean splashscreen/$Start$17/$::MoveNext()
extern TypeInfo* WaitForSeconds_t153_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m697_MethodInfo_var;
extern "C" bool U24_MoveNext_m638 (U24_t134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		GenericGeneratorEnumerator_1_YieldDefault_m697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Color_t11  V_1 = {0};
	float V_2 = 0.0f;
	Color_t11  V_3 = {0};
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = (((GenericGeneratorEnumerator_1_t135 *)__this)->____state_1);
		if (L_0 == 0)
		{
			goto IL_0027;
		}
		if (L_0 == 1)
		{
			goto IL_0148;
		}
		if (L_0 == 2)
		{
			goto IL_0092;
		}
		if (L_0 == 3)
		{
			goto IL_00cb;
		}
		if (L_0 == 4)
		{
			goto IL_00e1;
		}
		if (L_0 == 5)
		{
			goto IL_011a;
		}
		if (L_0 == 6)
		{
			goto IL_0130;
		}
	}

IL_0027:
	{
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___U24U244U2418_2 = L_1;
		int32_t L_2 = V_0;
		splashscreen_t136 * L_3 = (__this->___U24self_U2420_4);
		NullCheck(L_3);
		GUITexture_t141 * L_4 = (L_3->___guiObject_2);
		NullCheck(L_4);
		Color_t11  L_5 = GUITexture_get_color_m659(L_4, /*hidden argument*/NULL);
		Color_t11  L_6 = L_5;
		V_1 = L_6;
		__this->___U24U245U2419_3 = L_6;
		Color_t11  L_7 = V_1;
		Color_t11 * L_8 = &(__this->___U24U245U2419_3);
		int32_t L_9 = (__this->___U24U244U2418_2);
		float L_10 = (((float)((float)L_9)));
		V_2 = L_10;
		L_8->___a_3 = L_10;
		float L_11 = V_2;
		splashscreen_t136 * L_12 = (__this->___U24self_U2420_4);
		NullCheck(L_12);
		GUITexture_t141 * L_13 = (L_12->___guiObject_2);
		Color_t11  L_14 = (__this->___U24U245U2419_3);
		Color_t11  L_15 = L_14;
		V_3 = L_15;
		NullCheck(L_13);
		GUITexture_set_color_m660(L_13, L_15, /*hidden argument*/NULL);
		Color_t11  L_16 = V_3;
		WaitForSeconds_t153 * L_17 = (WaitForSeconds_t153 *)il2cpp_codegen_object_new (WaitForSeconds_t153_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m694(L_17, (0.5f), /*hidden argument*/NULL);
		bool L_18 = GenericGeneratorEnumerator_1_Yield_m695(__this, 2, L_17, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_18));
		goto IL_0149;
	}

IL_0092:
	{
		splashscreen_t136 * L_19 = (__this->___U24self_U2420_4);
		splashscreen_t136 * L_20 = (__this->___U24self_U2420_4);
		splashscreen_t136 * L_21 = (__this->___U24self_U2420_4);
		NullCheck(L_21);
		GUITexture_t141 * L_22 = (L_21->___guiObject_2);
		splashscreen_t136 * L_23 = (__this->___U24self_U2420_4);
		NullCheck(L_23);
		float L_24 = (L_23->___fadeTime_3);
		NullCheck(L_20);
		Object_t * L_25 = (Object_t *)VirtFuncInvoker3< Object_t *, GUITexture_t141 *, float, int32_t >::Invoke(5 /* System.Collections.IEnumerator splashscreen::FadeGUITexture(UnityEngine.GUITexture,System.Single,Fade) */, L_20, L_22, L_24, 0);
		NullCheck(L_19);
		Coroutine_t154 * L_26 = MonoBehaviour_StartCoroutine_Auto_m672(L_19, L_25, /*hidden argument*/NULL);
		bool L_27 = GenericGeneratorEnumerator_1_Yield_m695(__this, 3, L_26, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_27));
		goto IL_0149;
	}

IL_00cb:
	{
		WaitForSeconds_t153 * L_28 = (WaitForSeconds_t153 *)il2cpp_codegen_object_new (WaitForSeconds_t153_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m694(L_28, (0.25f), /*hidden argument*/NULL);
		bool L_29 = GenericGeneratorEnumerator_1_Yield_m695(__this, 4, L_28, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_29));
		goto IL_0149;
	}

IL_00e1:
	{
		splashscreen_t136 * L_30 = (__this->___U24self_U2420_4);
		splashscreen_t136 * L_31 = (__this->___U24self_U2420_4);
		splashscreen_t136 * L_32 = (__this->___U24self_U2420_4);
		NullCheck(L_32);
		GUITexture_t141 * L_33 = (L_32->___guiObject_2);
		splashscreen_t136 * L_34 = (__this->___U24self_U2420_4);
		NullCheck(L_34);
		float L_35 = (L_34->___fadeTime_3);
		NullCheck(L_31);
		Object_t * L_36 = (Object_t *)VirtFuncInvoker3< Object_t *, GUITexture_t141 *, float, int32_t >::Invoke(5 /* System.Collections.IEnumerator splashscreen::FadeGUITexture(UnityEngine.GUITexture,System.Single,Fade) */, L_31, L_33, L_35, 1);
		NullCheck(L_30);
		Coroutine_t154 * L_37 = MonoBehaviour_StartCoroutine_Auto_m672(L_30, L_36, /*hidden argument*/NULL);
		bool L_38 = GenericGeneratorEnumerator_1_Yield_m695(__this, 5, L_37, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_38));
		goto IL_0149;
	}

IL_011a:
	{
		WaitForSeconds_t153 * L_39 = (WaitForSeconds_t153 *)il2cpp_codegen_object_new (WaitForSeconds_t153_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m694(L_39, (0.25f), /*hidden argument*/NULL);
		bool L_40 = GenericGeneratorEnumerator_1_Yield_m695(__this, 6, L_39, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m695_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_40));
		goto IL_0149;
	}

IL_0130:
	{
		splashscreen_t136 * L_41 = (__this->___U24self_U2420_4);
		NullCheck(L_41);
		int32_t L_42 = (L_41->___nextscene_4);
		Application_LoadLevel_m696(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m697(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m697_MethodInfo_var);
	}

IL_0148:
	{
		G_B8_0 = 0;
	}

IL_0149:
	{
		return G_B8_0;
	}
}
// System.Void splashscreen/$Start$17::.ctor(splashscreen)
extern const MethodInfo* GenericGenerator_1__ctor_m698_MethodInfo_var;
extern "C" void U24StartU2417__ctor_m639 (U24StartU2417_t137 * __this, splashscreen_t136 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGenerator_1__ctor_m698_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m698(__this, /*hidden argument*/GenericGenerator_1__ctor_m698_MethodInfo_var);
		splashscreen_t136 * L_0 = ___self_;
		__this->___U24self_U2421_0 = L_0;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.YieldInstruction> splashscreen/$Start$17::GetEnumerator()
extern TypeInfo* U24_t134_il2cpp_TypeInfo_var;
extern "C" Object_t* U24StartU2417_GetEnumerator_m640 (U24StartU2417_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	{
		splashscreen_t136 * L_0 = (__this->___U24self_U2421_0);
		U24_t134 * L_1 = (U24_t134 *)il2cpp_codegen_object_new (U24_t134_il2cpp_TypeInfo_var);
		U24__ctor_m637(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void splashscreen/$FadeGUITexture$22/$::.ctor(UnityEngine.GUITexture,System.Single,Fade)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m699_MethodInfo_var;
extern "C" void U24__ctor_m641 (U24_t139 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGeneratorEnumerator_1__ctor_m699_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483701);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m699(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m699_MethodInfo_var);
		GUITexture_t141 * L_0 = ___guiObject;
		__this->___U24guiObjectU2429_8 = L_0;
		float L_1 = ___timer;
		__this->___U24timerU2430_9 = L_1;
		int32_t L_2 = ___fadeType;
		__this->___U24fadeTypeU2431_10 = L_2;
		return;
	}
}
// System.Boolean splashscreen/$FadeGUITexture$22/$::MoveNext()
extern TypeInfo* Mathf_t77_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m700_MethodInfo_var;
extern "C" bool U24_MoveNext_m642 (U24_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GenericGeneratorEnumerator_1_YieldDefault_m700_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483702);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Color_t11  V_1 = {0};
	float V_2 = 0.0f;
	Color_t11  V_3 = {0};
	U24_t139 * G_B3_0 = {0};
	U24_t139 * G_B2_0 = {0};
	float G_B4_0 = 0.0f;
	U24_t139 * G_B4_1 = {0};
	U24_t139 * G_B6_0 = {0};
	U24_t139 * G_B5_0 = {0};
	float G_B7_0 = 0.0f;
	U24_t139 * G_B7_1 = {0};
	int32_t G_B12_0 = 0;
	{
		int32_t L_0 = (((GenericGeneratorEnumerator_1_t140 *)__this)->____state_1);
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0115;
		}
		if (L_0 == 2)
		{
			goto IL_00fd;
		}
	}

IL_0017:
	{
		int32_t L_1 = (__this->___U24fadeTypeU2431_10);
		G_B2_0 = __this;
		if ((!(((uint32_t)L_1) == ((uint32_t)0))))
		{
			G_B3_0 = __this;
			goto IL_002b;
		}
	}
	{
		G_B4_0 = (((float)((float)0)));
		G_B4_1 = G_B2_0;
		goto IL_0030;
	}

IL_002b:
	{
		G_B4_0 = (1.0f);
		G_B4_1 = G_B3_0;
	}

IL_0030:
	{
		NullCheck(G_B4_1);
		G_B4_1->___U24startU2423_2 = G_B4_0;
		int32_t L_2 = (__this->___U24fadeTypeU2431_10);
		G_B5_0 = __this;
		if ((!(((uint32_t)L_2) == ((uint32_t)0))))
		{
			G_B6_0 = __this;
			goto IL_004c;
		}
	}
	{
		G_B7_0 = (1.0f);
		G_B7_1 = G_B5_0;
		goto IL_004e;
	}

IL_004c:
	{
		G_B7_0 = (((float)((float)0)));
		G_B7_1 = G_B6_0;
	}

IL_004e:
	{
		NullCheck(G_B7_1);
		G_B7_1->___U24endU2424_3 = G_B7_0;
		__this->___U24iU2425_4 = (((float)((float)0)));
		float L_3 = (__this->___U24timerU2430_9);
		__this->___U24stepU2426_5 = ((float)((float)(1.0f)/(float)L_3));
		goto IL_00fd;
	}

IL_0072:
	{
		float L_4 = (__this->___U24iU2425_4);
		float L_5 = (__this->___U24stepU2426_5);
		float L_6 = Time_get_deltaTime_m586(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U24iU2425_4 = ((float)((float)L_4+(float)((float)((float)L_5*(float)L_6))));
		float L_7 = (__this->___U24startU2423_2);
		float L_8 = (__this->___U24endU2424_3);
		float L_9 = (__this->___U24iU2425_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t77_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Lerp_m439(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		float L_11 = ((float)((float)L_10*(float)(0.5f)));
		V_0 = L_11;
		__this->___U24U246U2427_6 = L_11;
		float L_12 = V_0;
		GUITexture_t141 * L_13 = (__this->___U24guiObjectU2429_8);
		NullCheck(L_13);
		Color_t11  L_14 = GUITexture_get_color_m659(L_13, /*hidden argument*/NULL);
		Color_t11  L_15 = L_14;
		V_1 = L_15;
		__this->___U24U247U2428_7 = L_15;
		Color_t11  L_16 = V_1;
		Color_t11 * L_17 = &(__this->___U24U247U2428_7);
		float L_18 = (__this->___U24U246U2427_6);
		float L_19 = L_18;
		V_2 = L_19;
		L_17->___a_3 = L_19;
		float L_20 = V_2;
		GUITexture_t141 * L_21 = (__this->___U24guiObjectU2429_8);
		Color_t11  L_22 = (__this->___U24U247U2428_7);
		Color_t11  L_23 = L_22;
		V_3 = L_23;
		NullCheck(L_21);
		GUITexture_set_color_m660(L_21, L_23, /*hidden argument*/NULL);
		Color_t11  L_24 = V_3;
		bool L_25 = GenericGeneratorEnumerator_1_YieldDefault_m700(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m700_MethodInfo_var);
		G_B12_0 = ((int32_t)(L_25));
		goto IL_0116;
	}

IL_00fd:
	{
		float L_26 = (__this->___U24iU2425_4);
		if ((((float)L_26) < ((float)(1.0f))))
		{
			goto IL_0072;
		}
	}
	{
		GenericGeneratorEnumerator_1_YieldDefault_m700(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m700_MethodInfo_var);
	}

IL_0115:
	{
		G_B12_0 = 0;
	}

IL_0116:
	{
		return G_B12_0;
	}
}
// System.Void splashscreen/$FadeGUITexture$22::.ctor(UnityEngine.GUITexture,System.Single,Fade)
extern const MethodInfo* GenericGenerator_1__ctor_m701_MethodInfo_var;
extern "C" void U24FadeGUITextureU2422__ctor_m643 (U24FadeGUITextureU2422_t142 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericGenerator_1__ctor_m701_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483703);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m701(__this, /*hidden argument*/GenericGenerator_1__ctor_m701_MethodInfo_var);
		GUITexture_t141 * L_0 = ___guiObject;
		__this->___U24guiObjectU2432_0 = L_0;
		float L_1 = ___timer;
		__this->___U24timerU2433_1 = L_1;
		int32_t L_2 = ___fadeType;
		__this->___U24fadeTypeU2434_2 = L_2;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> splashscreen/$FadeGUITexture$22::GetEnumerator()
extern TypeInfo* U24_t139_il2cpp_TypeInfo_var;
extern "C" Object_t* U24FadeGUITextureU2422_GetEnumerator_m644 (U24FadeGUITextureU2422_t142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUITexture_t141 * L_0 = (__this->___U24guiObjectU2432_0);
		float L_1 = (__this->___U24timerU2433_1);
		int32_t L_2 = (__this->___U24fadeTypeU2434_2);
		U24_t139 * L_3 = (U24_t139 *)il2cpp_codegen_object_new (U24_t139_il2cpp_TypeInfo_var);
		U24__ctor_m641(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void splashscreen::.ctor()
extern "C" void splashscreen__ctor_m645 (splashscreen_t136 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m320(__this, /*hidden argument*/NULL);
		__this->___fadeTime_3 = (1.0f);
		__this->___nextscene_4 = 1;
		return;
	}
}
// System.Collections.IEnumerator splashscreen::Start()
extern TypeInfo* U24StartU2417_t137_il2cpp_TypeInfo_var;
extern "C" Object_t * splashscreen_Start_m646 (splashscreen_t136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24StartU2417_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24StartU2417_t137 * L_0 = (U24StartU2417_t137 *)il2cpp_codegen_object_new (U24StartU2417_t137_il2cpp_TypeInfo_var);
		U24StartU2417__ctor_m639(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t* L_1 = U24StartU2417_GetEnumerator_m640(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator splashscreen::FadeGUITexture(UnityEngine.GUITexture,System.Single,Fade)
extern TypeInfo* U24FadeGUITextureU2422_t142_il2cpp_TypeInfo_var;
extern "C" Object_t * splashscreen_FadeGUITexture_m647 (splashscreen_t136 * __this, GUITexture_t141 * ___guiObject, float ___timer, int32_t ___fadeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U24FadeGUITextureU2422_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUITexture_t141 * L_0 = ___guiObject;
		float L_1 = ___timer;
		int32_t L_2 = ___fadeType;
		U24FadeGUITextureU2422_t142 * L_3 = (U24FadeGUITextureU2422_t142 *)il2cpp_codegen_object_new (U24FadeGUITextureU2422_t142_il2cpp_TypeInfo_var);
		U24FadeGUITextureU2422__ctor_m643(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Object_t* L_4 = U24FadeGUITextureU2422_GetEnumerator_m644(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void splashscreen::Main()
extern "C" void splashscreen_Main_m648 (splashscreen_t136 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
