﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.DivideByZeroException
struct DivideByZeroException_t1703;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.DivideByZeroException::.ctor()
extern "C" void DivideByZeroException__ctor_m10552 (DivideByZeroException_t1703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DivideByZeroException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DivideByZeroException__ctor_m10553 (DivideByZeroException_t1703 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
