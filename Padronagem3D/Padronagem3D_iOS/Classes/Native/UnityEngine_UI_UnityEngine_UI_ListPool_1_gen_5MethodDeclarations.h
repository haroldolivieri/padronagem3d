﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t424;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C" void ListPool_1__cctor_m17237_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m17237(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m17237_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C" List_1_t424 * ListPool_1_Get_m3764_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3764(__this /* static, unused */, method) (( List_1_t424 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3764_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3774_gshared (Object_t * __this /* static, unused */, List_1_t424 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3774(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t424 *, const MethodInfo*))ListPool_1_Release_m3774_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m17238_gshared (Object_t * __this /* static, unused */, List_1_t424 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m17238(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t424 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m17238_gshared)(__this /* static, unused */, ___l, method)
