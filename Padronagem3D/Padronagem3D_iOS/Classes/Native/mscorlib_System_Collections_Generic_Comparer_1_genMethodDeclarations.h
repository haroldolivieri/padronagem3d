﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1862;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C" void Comparer_1__ctor_m11254_gshared (Comparer_1_t1862 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m11254(__this, method) (( void (*) (Comparer_1_t1862 *, const MethodInfo*))Comparer_1__ctor_m11254_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern "C" void Comparer_1__cctor_m11255_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m11255(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m11255_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m11256_gshared (Comparer_1_t1862 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m11256(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t1862 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m11256_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C" Comparer_1_t1862 * Comparer_1_get_Default_m11257_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m11257(__this /* static, unused */, method) (( Comparer_1_t1862 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m11257_gshared)(__this /* static, unused */, method)
