﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2422;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m18219_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m18219(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m18219_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" CustomAttributeTypedArgument_t1374  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18220_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18220(__this, method) (( CustomAttributeTypedArgument_t1374  (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18220_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18221(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18221_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18222_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18222(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18222_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18223_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18223(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18223_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m18224_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2422 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m18224(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2422 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m18224_gshared)(__this, method)
