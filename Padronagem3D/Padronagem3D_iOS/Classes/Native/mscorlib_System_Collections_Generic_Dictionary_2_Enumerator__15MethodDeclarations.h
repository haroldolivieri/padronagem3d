﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17670(__this, ___dictionary, method) (( void (*) (Enumerator_t2356 *, Dictionary_2_t813 *, const MethodInfo*))Enumerator__ctor_m17602_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17671(__this, method) (( Object_t * (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17672(__this, method) (( void (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17604_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17673(__this, method) (( DictionaryEntry_t943  (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17605_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17674(__this, method) (( Object_t * (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17606_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17675(__this, method) (( Object_t * (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17607_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m17676(__this, method) (( bool (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_MoveNext_m17608_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m17677(__this, method) (( KeyValuePair_2_t2354  (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_get_Current_m17609_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17678(__this, method) (( String_t* (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_get_CurrentKey_m17610_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17679(__this, method) (( bool (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_get_CurrentValue_m17611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m17680(__this, method) (( void (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_Reset_m17612_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m17681(__this, method) (( void (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_VerifyState_m17613_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17682(__this, method) (( void (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_VerifyCurrent_m17614_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m17683(__this, method) (( void (*) (Enumerator_t2356 *, const MethodInfo*))Enumerator_Dispose_m17615_gshared)(__this, method)
