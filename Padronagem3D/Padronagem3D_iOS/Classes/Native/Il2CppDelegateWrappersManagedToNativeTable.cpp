﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_OnGUICallback_t6 ();
extern "C" void pinvoke_delegate_wrapper_VREventCallback_t56 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t195 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t210 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t224 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t226 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t228 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t258 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t260 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t261 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t282 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t293 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t305 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t323 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t242 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t394 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t583 ();
extern "C" void pinvoke_delegate_wrapper_DispatcherFactory_t717 ();
extern "C" void pinvoke_delegate_wrapper_Dispatcher_t718 ();
extern "C" void pinvoke_delegate_wrapper_Action_t27 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t862 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t903 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t799 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t938 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1009 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1087 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t1065 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1066 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t1049 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1050 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1128 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t9 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1194 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1202 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1290 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1291 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1376 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1390 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1569 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1755 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1131 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1383 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1756 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1757 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1759 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1679 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1760 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1688 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1685 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t66 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1686 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t445 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[52] = 
{
	pinvoke_delegate_wrapper_OnGUICallback_t6,
	pinvoke_delegate_wrapper_VREventCallback_t56,
	pinvoke_delegate_wrapper_StateChanged_t195,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t210,
	pinvoke_delegate_wrapper_LogCallback_t224,
	pinvoke_delegate_wrapper_CameraCallback_t226,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t228,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t258,
	pinvoke_delegate_wrapper_PCMReaderCallback_t260,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t261,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t282,
	pinvoke_delegate_wrapper_WillRenderCanvases_t293,
	pinvoke_delegate_wrapper_WindowFunction_t305,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t323,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t242,
	pinvoke_delegate_wrapper_UnityAction_t394,
	pinvoke_delegate_wrapper_OnValidateInput_t583,
	pinvoke_delegate_wrapper_DispatcherFactory_t717,
	pinvoke_delegate_wrapper_Dispatcher_t718,
	pinvoke_delegate_wrapper_Action_t27,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t862,
	pinvoke_delegate_wrapper_CostDelegate_t903,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t799,
	pinvoke_delegate_wrapper_MatchEvaluator_t938,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1009,
	pinvoke_delegate_wrapper_PrimalityTest_t1087,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t1065,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1066,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t1049,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1050,
	pinvoke_delegate_wrapper_Swapper_t1128,
	pinvoke_delegate_wrapper_AsyncCallback_t9,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1194,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1202,
	pinvoke_delegate_wrapper_ReadDelegate_t1290,
	pinvoke_delegate_wrapper_WriteDelegate_t1291,
	pinvoke_delegate_wrapper_AddEventAdapter_t1376,
	pinvoke_delegate_wrapper_GetterAdapter_t1390,
	pinvoke_delegate_wrapper_CallbackHandler_t1569,
	pinvoke_delegate_wrapper_PrimalityTest_t1755,
	pinvoke_delegate_wrapper_MemberFilter_t1131,
	pinvoke_delegate_wrapper_TypeFilter_t1383,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1756,
	pinvoke_delegate_wrapper_HeaderHandler_t1757,
	pinvoke_delegate_wrapper_ThreadStart_t1759,
	pinvoke_delegate_wrapper_TimerCallback_t1679,
	pinvoke_delegate_wrapper_WaitCallback_t1760,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1688,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1685,
	pinvoke_delegate_wrapper_EventHandler_t66,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1686,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t445,
};
