﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// DInput
struct  DInput_t67  : public Object_t
{
};
struct DInput_t67_StaticFields{
	// System.Boolean DInput::use_cardboard_trigger
	bool ___use_cardboard_trigger_0;
	// System.Boolean DInput::use_analog_value
	bool ___use_analog_value_1;
	// System.Boolean DInput::magnet_detected
	bool ___magnet_detected_2;
	// System.Int32 DInput::magnet_trigger
	int32_t ___magnet_trigger_3;
	// System.Boolean DInput::magnet_trigger_posedge
	bool ___magnet_trigger_posedge_4;
	// System.Boolean DInput::magnet_trigger_negedge
	bool ___magnet_trigger_negedge_5;
	// System.Single DInput::magnet_value
	float ___magnet_value_6;
	// System.Boolean DInput::invert
	bool ___invert_7;
	// System.Boolean DInput::use_IPD_Correction
	bool ___use_IPD_Correction_8;
	// System.Single DInput::IPDCorrectionValue
	float ___IPDCorrectionValue_9;
};
