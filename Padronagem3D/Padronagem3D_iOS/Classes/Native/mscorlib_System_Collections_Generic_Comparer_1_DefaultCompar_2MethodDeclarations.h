﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t1961;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C" void DefaultComparer__ctor_m12411_gshared (DefaultComparer_t1961 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12411(__this, method) (( void (*) (DefaultComparer_t1961 *, const MethodInfo*))DefaultComparer__ctor_m12411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m12412_gshared (DefaultComparer_t1961 * __this, Vector4_t90  ___x, Vector4_t90  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m12412(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1961 *, Vector4_t90 , Vector4_t90 , const MethodInfo*))DefaultComparer_Compare_m12412_gshared)(__this, ___x, ___y, method)
