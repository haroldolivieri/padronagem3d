﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m14108(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2094 *, String_t*, GUIStyle_t315 *, const MethodInfo*))KeyValuePair_2__ctor_m14045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m14109(__this, method) (( String_t* (*) (KeyValuePair_2_t2094 *, const MethodInfo*))KeyValuePair_2_get_Key_m14046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14110(__this, ___value, method) (( void (*) (KeyValuePair_2_t2094 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m14047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m14111(__this, method) (( GUIStyle_t315 * (*) (KeyValuePair_2_t2094 *, const MethodInfo*))KeyValuePair_2_get_Value_m14048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14112(__this, ___value, method) (( void (*) (KeyValuePair_2_t2094 *, GUIStyle_t315 *, const MethodInfo*))KeyValuePair_2_set_Value_m14049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m14113(__this, method) (( String_t* (*) (KeyValuePair_2_t2094 *, const MethodInfo*))KeyValuePair_2_ToString_m14050_gshared)(__this, method)
