﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2381;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t147;
// System.Exception
struct Exception_t108;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m17881_gshared (ArrayReadOnlyList_1_t2381 * __this, ObjectU5BU5D_t115* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m17881(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, ObjectU5BU5D_t115*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m17881_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17882_gshared (ArrayReadOnlyList_1_t2381 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17882(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2381 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17882_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m17883_gshared (ArrayReadOnlyList_1_t2381 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m17883(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2381 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m17883_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m17884_gshared (ArrayReadOnlyList_1_t2381 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m17884(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m17884_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m17885_gshared (ArrayReadOnlyList_1_t2381 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m17885(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2381 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m17885_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m17886_gshared (ArrayReadOnlyList_1_t2381 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m17886(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2381 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m17886_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m17887_gshared (ArrayReadOnlyList_1_t2381 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m17887(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m17887_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m17888_gshared (ArrayReadOnlyList_1_t2381 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m17888(__this, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m17888_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m17889_gshared (ArrayReadOnlyList_1_t2381 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m17889(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2381 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m17889_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m17890_gshared (ArrayReadOnlyList_1_t2381 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m17890(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m17890_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m17891_gshared (ArrayReadOnlyList_1_t2381 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m17891(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2381 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m17891_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m17892_gshared (ArrayReadOnlyList_1_t2381 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m17892(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2381 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m17892_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m17893_gshared (ArrayReadOnlyList_1_t2381 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m17893(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m17893_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m17894_gshared (ArrayReadOnlyList_1_t2381 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m17894(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2381 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m17894_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m17895_gshared (ArrayReadOnlyList_1_t2381 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m17895(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2381 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m17895_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t108 * ArrayReadOnlyList_1_ReadOnlyError_m17896_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m17896(__this /* static, unused */, method) (( Exception_t108 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m17896_gshared)(__this /* static, unused */, method)
