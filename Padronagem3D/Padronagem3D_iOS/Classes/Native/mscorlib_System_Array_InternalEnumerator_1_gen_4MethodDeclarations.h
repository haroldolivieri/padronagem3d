﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"

// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11272_gshared (InternalEnumerator_1_t1866 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11272(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1866 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11272_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11273_gshared (InternalEnumerator_1_t1866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11273(__this, method) (( void (*) (InternalEnumerator_1_t1866 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11273_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11274_gshared (InternalEnumerator_1_t1866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11274(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1866 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11275_gshared (InternalEnumerator_1_t1866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11275(__this, method) (( void (*) (InternalEnumerator_1_t1866 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11275_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11276_gshared (InternalEnumerator_1_t1866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11276(__this, method) (( bool (*) (InternalEnumerator_1_t1866 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11276_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m11277_gshared (InternalEnumerator_1_t1866 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11277(__this, method) (( uint16_t (*) (InternalEnumerator_1_t1866 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11277_gshared)(__this, method)
