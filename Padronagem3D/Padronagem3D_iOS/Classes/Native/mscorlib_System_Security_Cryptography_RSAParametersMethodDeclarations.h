﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void RSAParameters_t965_marshal(const RSAParameters_t965& unmarshaled, RSAParameters_t965_marshaled& marshaled);
extern "C" void RSAParameters_t965_marshal_back(const RSAParameters_t965_marshaled& marshaled, RSAParameters_t965& unmarshaled);
extern "C" void RSAParameters_t965_marshal_cleanup(RSAParameters_t965_marshaled& marshaled);
