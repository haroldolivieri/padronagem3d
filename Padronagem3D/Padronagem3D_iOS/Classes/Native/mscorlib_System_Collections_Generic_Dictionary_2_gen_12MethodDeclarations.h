﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2084;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2046;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2505;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t2506;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t942;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t2088;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m13954_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m13954(__this, method) (( void (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2__ctor_m13954_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m13955_gshared (Dictionary_2_t2084 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m13955(__this, ___comparer, method) (( void (*) (Dictionary_2_t2084 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13955_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m13957_gshared (Dictionary_2_t2084 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m13957(__this, ___capacity, method) (( void (*) (Dictionary_2_t2084 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13957_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m13959_gshared (Dictionary_2_t2084 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m13959(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2084 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m13959_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m13961_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m13961(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13961_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13963_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m13963(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13963_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13965_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m13965(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13965_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m13967_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m13967(__this, ___key, method) (( bool (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13967_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13969_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m13969(__this, ___key, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13969_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13971_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13971(__this, method) (( bool (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13971_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13973_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13973(__this, method) (( Object_t * (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13973_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13975_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13975(__this, method) (( bool (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13975_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13977_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2_t2086  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13977(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2084 *, KeyValuePair_2_t2086 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13977_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13979_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2_t2086  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13979(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2084 *, KeyValuePair_2_t2086 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13979_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13981_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2U5BU5D_t2505* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13981(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2084 *, KeyValuePair_2U5BU5D_t2505*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13981_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13983_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2_t2086  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13983(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2084 *, KeyValuePair_2_t2086 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13983_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13985_gshared (Dictionary_2_t2084 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m13985(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2084 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13985_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13987_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13987(__this, method) (( Object_t * (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13987_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13989_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13989(__this, method) (( Object_t* (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13989_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13991_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13991(__this, method) (( Object_t * (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13991_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m13993_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m13993(__this, method) (( int32_t (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_get_Count_m13993_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m13995_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m13995(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m13995_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m13997_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m13997(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m13997_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m13999_gshared (Dictionary_2_t2084 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m13999(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2084 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13999_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m14001_gshared (Dictionary_2_t2084 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m14001(__this, ___size, method) (( void (*) (Dictionary_2_t2084 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14001_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m14003_gshared (Dictionary_2_t2084 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m14003(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2084 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14003_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2086  Dictionary_2_make_pair_m14005_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m14005(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2086  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m14005_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m14007_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m14007(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m14007_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m14009_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2U5BU5D_t2505* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m14009(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2084 *, KeyValuePair_2U5BU5D_t2505*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14009_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m14011_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m14011(__this, method) (( void (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_Resize_m14011_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m14013_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m14013(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_Add_m14013_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m14015_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m14015(__this, method) (( void (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_Clear_m14015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m14017_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m14017(__this, ___key, method) (( bool (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m14017_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m14019_gshared (Dictionary_2_t2084 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m14019(__this, ___value, method) (( bool (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m14019_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m14021_gshared (Dictionary_2_t2084 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m14021(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2084 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m14021_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m14023_gshared (Dictionary_2_t2084 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m14023(__this, ___sender, method) (( void (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14023_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m14025_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m14025(__this, ___key, method) (( bool (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m14025_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m14027_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m14027(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2084 *, Object_t *, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m14027_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C" ValueCollection_t2088 * Dictionary_2_get_Values_m14028_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m14028(__this, method) (( ValueCollection_t2088 * (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_get_Values_m14028_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m14030_gshared (Dictionary_2_t2084 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m14030(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14030_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m14032_gshared (Dictionary_2_t2084 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m14032(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2084 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14032_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m14034_gshared (Dictionary_2_t2084 * __this, KeyValuePair_2_t2086  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m14034(__this, ___pair, method) (( bool (*) (Dictionary_2_t2084 *, KeyValuePair_2_t2086 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14034_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t2090  Dictionary_2_GetEnumerator_m14036_gshared (Dictionary_2_t2084 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m14036(__this, method) (( Enumerator_t2090  (*) (Dictionary_2_t2084 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14036_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t943  Dictionary_2_U3CCopyToU3Em__0_m14038_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m14038(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14038_gshared)(__this /* static, unused */, ___key, ___value, method)
