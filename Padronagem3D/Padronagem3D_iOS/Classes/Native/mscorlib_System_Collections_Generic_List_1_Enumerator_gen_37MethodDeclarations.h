﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t2424;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18341_gshared (Enumerator_t2425 * __this, List_1_t2424 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18341(__this, ___l, method) (( void (*) (Enumerator_t2425 *, List_1_t2424 *, const MethodInfo*))Enumerator__ctor_m18341_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18342_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18342(__this, method) (( void (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18342_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18343_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18343(__this, method) (( Object_t * (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18343_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void Enumerator_Dispose_m18344_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18344(__this, method) (( void (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_Dispose_m18344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C" void Enumerator_VerifyState_m18345_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18345(__this, method) (( void (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_VerifyState_m18345_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18346_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18346(__this, method) (( bool (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_MoveNext_m18346_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1373  Enumerator_get_Current_m18347_gshared (Enumerator_t2425 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18347(__this, method) (( CustomAttributeNamedArgument_t1373  (*) (Enumerator_t2425 *, const MethodInfo*))Enumerator_get_Current_m18347_gshared)(__this, method)
