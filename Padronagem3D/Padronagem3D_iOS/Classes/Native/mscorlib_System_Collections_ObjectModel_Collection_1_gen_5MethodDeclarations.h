﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t1986;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2475;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t1985;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m12791_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12791(__this, method) (( void (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1__ctor_m12791_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12792_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12792(__this, method) (( bool (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12792_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12793_gshared (Collection_1_t1986 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12793(__this, ___array, ___index, method) (( void (*) (Collection_1_t1986 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12793_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12794_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12794(__this, method) (( Object_t * (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12794_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12795_gshared (Collection_1_t1986 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12795(__this, ___value, method) (( int32_t (*) (Collection_1_t1986 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12795_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12796_gshared (Collection_1_t1986 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12796(__this, ___value, method) (( bool (*) (Collection_1_t1986 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12796_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12797_gshared (Collection_1_t1986 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12797(__this, ___value, method) (( int32_t (*) (Collection_1_t1986 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12797_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12798_gshared (Collection_1_t1986 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12798(__this, ___index, ___value, method) (( void (*) (Collection_1_t1986 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12798_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12799_gshared (Collection_1_t1986 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12799(__this, ___value, method) (( void (*) (Collection_1_t1986 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12799_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12800_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12800(__this, method) (( bool (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12800_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12801_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12801(__this, method) (( Object_t * (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12801_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12802_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12802(__this, method) (( bool (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12802_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12803_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12803(__this, method) (( bool (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12803_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12804_gshared (Collection_1_t1986 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12804(__this, ___index, method) (( Object_t * (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12804_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12805_gshared (Collection_1_t1986 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12805(__this, ___index, ___value, method) (( void (*) (Collection_1_t1986 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12805_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m12806_gshared (Collection_1_t1986 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m12806(__this, ___item, method) (( void (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_Add_m12806_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m12807_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12807(__this, method) (( void (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_Clear_m12807_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m12808_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12808(__this, method) (( void (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_ClearItems_m12808_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m12809_gshared (Collection_1_t1986 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m12809(__this, ___item, method) (( bool (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_Contains_m12809_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12810_gshared (Collection_1_t1986 * __this, Int32U5BU5D_t107* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12810(__this, ___array, ___index, method) (( void (*) (Collection_1_t1986 *, Int32U5BU5D_t107*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12810_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12811_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12811(__this, method) (( Object_t* (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_GetEnumerator_m12811_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12812_gshared (Collection_1_t1986 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12812(__this, ___item, method) (( int32_t (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m12812_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12813_gshared (Collection_1_t1986 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m12813(__this, ___index, ___item, method) (( void (*) (Collection_1_t1986 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m12813_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12814_gshared (Collection_1_t1986 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12814(__this, ___index, ___item, method) (( void (*) (Collection_1_t1986 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m12814_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m12815_gshared (Collection_1_t1986 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m12815(__this, ___item, method) (( bool (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_Remove_m12815_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12816_gshared (Collection_1_t1986 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12816(__this, ___index, method) (( void (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12816_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12817_gshared (Collection_1_t1986 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12817(__this, ___index, method) (( void (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12817_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12818_gshared (Collection_1_t1986 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12818(__this, method) (( int32_t (*) (Collection_1_t1986 *, const MethodInfo*))Collection_1_get_Count_m12818_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m12819_gshared (Collection_1_t1986 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12819(__this, ___index, method) (( int32_t (*) (Collection_1_t1986 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12819_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12820_gshared (Collection_1_t1986 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12820(__this, ___index, ___value, method) (( void (*) (Collection_1_t1986 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m12820_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12821_gshared (Collection_1_t1986 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12821(__this, ___index, ___item, method) (( void (*) (Collection_1_t1986 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m12821_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12822_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12822(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12822_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m12823_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12823(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12823_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12824_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12824(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12824_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12825_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12825(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12825_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12826_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12826(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12826_gshared)(__this /* static, unused */, ___list, method)
