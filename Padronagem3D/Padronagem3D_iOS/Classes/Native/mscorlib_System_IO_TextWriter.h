﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t469;
// System.IO.TextWriter
struct TextWriter_t979;

#include "mscorlib_System_Object.h"

// System.IO.TextWriter
struct  TextWriter_t979  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t469* ___CoreNewLine_0;
};
struct TextWriter_t979_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t979 * ___Null_1;
};
