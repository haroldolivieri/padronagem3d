﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1053;

#include "mscorlib_System_Object.h"

// System.IO.Stream
struct  Stream_t1053  : public Object_t
{
};
struct Stream_t1053_StaticFields{
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1053 * ___Null_0;
};
