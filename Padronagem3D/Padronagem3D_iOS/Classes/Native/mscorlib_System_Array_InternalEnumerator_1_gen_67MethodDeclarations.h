﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Delegate>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m17847(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2375 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11054_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Delegate>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17848(__this, method) (( void (*) (InternalEnumerator_1_t2375 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11056_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Delegate>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17849(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2375 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11058_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Delegate>::Dispose()
#define InternalEnumerator_1_Dispose_m17850(__this, method) (( void (*) (InternalEnumerator_1_t2375 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11060_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Delegate>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17851(__this, method) (( bool (*) (InternalEnumerator_1_t2375 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11062_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Delegate>::get_Current()
#define InternalEnumerator_1_get_Current_m17852(__this, method) (( Delegate_t81 * (*) (InternalEnumerator_1_t2375 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11064_gshared)(__this, method)
