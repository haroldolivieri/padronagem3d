﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.StringComparer
struct StringComparer_t457;

#include "mscorlib_System_Object.h"

// System.StringComparer
struct  StringComparer_t457  : public Object_t
{
};
struct StringComparer_t457_StaticFields{
	// System.StringComparer System.StringComparer::invariantCultureIgnoreCase
	StringComparer_t457 * ___invariantCultureIgnoreCase_0;
	// System.StringComparer System.StringComparer::invariantCulture
	StringComparer_t457 * ___invariantCulture_1;
	// System.StringComparer System.StringComparer::ordinalIgnoreCase
	StringComparer_t457 * ___ordinalIgnoreCase_2;
	// System.StringComparer System.StringComparer::ordinal
	StringComparer_t457 * ___ordinal_3;
};
