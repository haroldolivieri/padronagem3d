﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// CardboardOnGUIWindow[]
// CardboardOnGUIWindow[]
struct CardboardOnGUIWindowU5BU5D_t83  : public Array_t { };
// CardboardEye[]
// CardboardEye[]
struct CardboardEyeU5BU5D_t54  : public Array_t { };
