﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2044;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2046;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t440;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2497;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t2498;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t942;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t2050;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m13459_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m13459(__this, method) (( void (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2__ctor_m13459_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m13461_gshared (Dictionary_2_t2044 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m13461(__this, ___comparer, method) (( void (*) (Dictionary_2_t2044 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13461_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m13462_gshared (Dictionary_2_t2044 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m13462(__this, ___capacity, method) (( void (*) (Dictionary_2_t2044 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13462_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m13464_gshared (Dictionary_2_t2044 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m13464(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2044 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2__ctor_m13464_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m13466_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m13466(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13466_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13468_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m13468(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13468_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13470_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m13470(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13470_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m13472_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m13472(__this, ___key, method) (( bool (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13472_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13474_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m13474(__this, ___key, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13474_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13476_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13476(__this, method) (( bool (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13476_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13478_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13478(__this, method) (( Object_t * (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13478_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13480_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13480(__this, method) (( bool (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13482_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2_t2047  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13482(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2044 *, KeyValuePair_2_t2047 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13482_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13484_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2_t2047  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13484(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2044 *, KeyValuePair_2_t2047 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13484_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13486_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2U5BU5D_t2497* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13486(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2044 *, KeyValuePair_2U5BU5D_t2497*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13486_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13488_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2_t2047  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13488(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2044 *, KeyValuePair_2_t2047 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13488_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13490_gshared (Dictionary_2_t2044 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m13490(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2044 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13490_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13492_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13492(__this, method) (( Object_t * (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13492_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13494_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13494(__this, method) (( Object_t* (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13494_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13496_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13496(__this, method) (( Object_t * (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13496_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m13498_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m13498(__this, method) (( int32_t (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_get_Count_m13498_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m13500_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m13500(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m13500_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m13502_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m13502(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m13502_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m13504_gshared (Dictionary_2_t2044 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m13504(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2044 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13504_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m13506_gshared (Dictionary_2_t2044 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m13506(__this, ___size, method) (( void (*) (Dictionary_2_t2044 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13506_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m13508_gshared (Dictionary_2_t2044 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m13508(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2044 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13508_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2047  Dictionary_2_make_pair_m13510_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m13510(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2047  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m13510_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m13512_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m13512(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m13512_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m13514_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2U5BU5D_t2497* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m13514(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2044 *, KeyValuePair_2U5BU5D_t2497*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13514_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m13516_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m13516(__this, method) (( void (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_Resize_m13516_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m13518_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m13518(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m13518_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m13520_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m13520(__this, method) (( void (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_Clear_m13520_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m13522_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m13522(__this, ___key, method) (( bool (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m13522_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m13524_gshared (Dictionary_2_t2044 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m13524(__this, ___value, method) (( bool (*) (Dictionary_2_t2044 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m13524_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m13526_gshared (Dictionary_2_t2044 * __this, SerializationInfo_t440 * ___info, StreamingContext_t441  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m13526(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2044 *, SerializationInfo_t440 *, StreamingContext_t441 , const MethodInfo*))Dictionary_2_GetObjectData_m13526_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m13528_gshared (Dictionary_2_t2044 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m13528(__this, ___sender, method) (( void (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13528_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m13530_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m13530(__this, ___key, method) (( bool (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m13530_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m13532_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m13532(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2044 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m13532_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t2050 * Dictionary_2_get_Values_m13534_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m13534(__this, method) (( ValueCollection_t2050 * (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_get_Values_m13534_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m13536_gshared (Dictionary_2_t2044 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m13536(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13536_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m13538_gshared (Dictionary_2_t2044 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m13538(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2044 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13538_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m13540_gshared (Dictionary_2_t2044 * __this, KeyValuePair_2_t2047  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m13540(__this, ___pair, method) (( bool (*) (Dictionary_2_t2044 *, KeyValuePair_2_t2047 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13540_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2052  Dictionary_2_GetEnumerator_m13542_gshared (Dictionary_2_t2044 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m13542(__this, method) (( Enumerator_t2052  (*) (Dictionary_2_t2044 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13542_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t943  Dictionary_2_U3CCopyToU3Em__0_m13544_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m13544(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t943  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13544_gshared)(__this /* static, unused */, ___key, ___value, method)
