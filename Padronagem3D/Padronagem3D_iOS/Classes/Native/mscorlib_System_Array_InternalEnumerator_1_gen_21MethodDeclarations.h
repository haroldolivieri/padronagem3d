﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12117_gshared (InternalEnumerator_1_t1941 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12117(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1941 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12117_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12118_gshared (InternalEnumerator_1_t1941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12118(__this, method) (( void (*) (InternalEnumerator_1_t1941 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12118_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12119_gshared (InternalEnumerator_1_t1941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12119(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1941 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12119_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12120_gshared (InternalEnumerator_1_t1941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12120(__this, method) (( void (*) (InternalEnumerator_1_t1941 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12120_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12121_gshared (InternalEnumerator_1_t1941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12121(__this, method) (( bool (*) (InternalEnumerator_1_t1941 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12121_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t15  InternalEnumerator_1_get_Current_m12122_gshared (InternalEnumerator_1_t1941 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12122(__this, method) (( Vector2_t15  (*) (InternalEnumerator_1_t1941 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12122_gshared)(__this, method)
