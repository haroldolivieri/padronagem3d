﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t1993;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m12848_gshared (DefaultComparer_t1993 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12848(__this, method) (( void (*) (DefaultComparer_t1993 *, const MethodInfo*))DefaultComparer__ctor_m12848_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m12849_gshared (DefaultComparer_t1993 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m12849(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1993 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m12849_gshared)(__this, ___x, ___y, method)
