﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18052_gshared (InternalEnumerator_1_t2411 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18052(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2411 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18052_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18053_gshared (InternalEnumerator_1_t2411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18053(__this, method) (( void (*) (InternalEnumerator_1_t2411 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18053_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18054_gshared (InternalEnumerator_1_t2411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18054(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2411 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18054_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18055_gshared (InternalEnumerator_1_t2411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18055(__this, method) (( void (*) (InternalEnumerator_1_t2411 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18055_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18056_gshared (InternalEnumerator_1_t2411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18056(__this, method) (( bool (*) (InternalEnumerator_1_t2411 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18056_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1373  InternalEnumerator_1_get_Current_m18057_gshared (InternalEnumerator_1_t2411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18057(__this, method) (( CustomAttributeNamedArgument_t1373  (*) (InternalEnumerator_1_t2411 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18057_gshared)(__this, method)
