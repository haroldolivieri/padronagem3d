﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_70.h"

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17865_gshared (InternalEnumerator_1_t2378 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17865(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2378 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17865_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17866_gshared (InternalEnumerator_1_t2378 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17866(__this, method) (( void (*) (InternalEnumerator_1_t2378 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17866_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17867_gshared (InternalEnumerator_1_t2378 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17867(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2378 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17867_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17868_gshared (InternalEnumerator_1_t2378 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17868(__this, method) (( void (*) (InternalEnumerator_1_t2378 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17868_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17869_gshared (InternalEnumerator_1_t2378 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17869(__this, method) (( bool (*) (InternalEnumerator_1_t2378 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17869_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m17870_gshared (InternalEnumerator_1_t2378 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17870(__this, method) (( int8_t (*) (InternalEnumerator_1_t2378 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17870_gshared)(__this, method)
