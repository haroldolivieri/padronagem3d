﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m3569(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t508 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m11602_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Invoke(T0)
#define UnityAction_1_Invoke_m11617(__this, ___arg0, method) (( void (*) (UnityAction_1_t508 *, List_1_t659 *, const MethodInfo*))UnityAction_1_Invoke_m11603_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m11618(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t508 *, List_1_t659 *, AsyncCallback_t9 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m11604_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m11619(__this, ___result, method) (( void (*) (UnityAction_1_t508 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m11605_gshared)(__this, ___result, method)
