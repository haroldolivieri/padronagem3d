﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t421;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C" void ListPool_1__cctor_m17193_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m17193(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m17193_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C" List_1_t421 * ListPool_1_Get_m3762_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3762(__this /* static, unused */, method) (( List_1_t421 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3762_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3772_gshared (Object_t * __this /* static, unused */, List_1_t421 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3772(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t421 *, const MethodInfo*))ListPool_1_Release_m3772_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m17194_gshared (Object_t * __this /* static, unused */, List_1_t421 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m17194(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t421 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m17194_gshared)(__this /* static, unused */, ___l, method)
