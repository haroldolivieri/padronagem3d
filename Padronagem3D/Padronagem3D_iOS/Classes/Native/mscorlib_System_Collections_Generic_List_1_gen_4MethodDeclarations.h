﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t421;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t2481;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t2482;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t70;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t2483;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t1964;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t420;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t1969;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t1972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" void List_1__ctor_m12417_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1__ctor_m12417(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1__ctor_m12417_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12418_gshared (List_1_t421 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12418(__this, ___collection, method) (( void (*) (List_1_t421 *, Object_t*, const MethodInfo*))List_1__ctor_m12418_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12419_gshared (List_1_t421 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12419(__this, ___capacity, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1__ctor_m12419_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C" void List_1__cctor_m12420_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12420(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12420_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12421_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12421(__this, method) (( Object_t* (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12421_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12422_gshared (List_1_t421 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12422(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t421 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12422_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12423_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12423(__this, method) (( Object_t * (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12423_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12424_gshared (List_1_t421 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12424(__this, ___item, method) (( int32_t (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12424_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12425_gshared (List_1_t421 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12425(__this, ___item, method) (( bool (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12425_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12426_gshared (List_1_t421 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12426(__this, ___item, method) (( int32_t (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12427_gshared (List_1_t421 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12427(__this, ___index, ___item, method) (( void (*) (List_1_t421 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12428_gshared (List_1_t421 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12428(__this, ___item, method) (( void (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12428_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12429_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12429(__this, method) (( bool (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12429_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12430_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12430(__this, method) (( bool (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12430_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12431_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12431(__this, method) (( Object_t * (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12432_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12432(__this, method) (( bool (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12432_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12433_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12433(__this, method) (( bool (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12433_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12434_gshared (List_1_t421 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12434(__this, ___index, method) (( Object_t * (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12434_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12435_gshared (List_1_t421 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12435(__this, ___index, ___value, method) (( void (*) (List_1_t421 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12435_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C" void List_1_Add_m12436_gshared (List_1_t421 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define List_1_Add_m12436(__this, ___item, method) (( void (*) (List_1_t421 *, Vector2_t15 , const MethodInfo*))List_1_Add_m12436_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12437_gshared (List_1_t421 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12437(__this, ___newCount, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12437_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12438_gshared (List_1_t421 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12438(__this, ___collection, method) (( void (*) (List_1_t421 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12438_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12439_gshared (List_1_t421 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12439(__this, ___enumerable, method) (( void (*) (List_1_t421 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12439_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3767_gshared (List_1_t421 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3767(__this, ___collection, method) (( void (*) (List_1_t421 *, Object_t*, const MethodInfo*))List_1_AddRange_m3767_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1964 * List_1_AsReadOnly_m12440_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12440(__this, method) (( ReadOnlyCollection_1_t1964 * (*) (List_1_t421 *, const MethodInfo*))List_1_AsReadOnly_m12440_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" void List_1_Clear_m12441_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_Clear_m12441(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1_Clear_m12441_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool List_1_Contains_m12442_gshared (List_1_t421 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define List_1_Contains_m12442(__this, ___item, method) (( bool (*) (List_1_t421 *, Vector2_t15 , const MethodInfo*))List_1_Contains_m12442_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12443_gshared (List_1_t421 * __this, Vector2U5BU5D_t420* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12443(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t421 *, Vector2U5BU5D_t420*, int32_t, const MethodInfo*))List_1_CopyTo_m12443_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::Find(System.Predicate`1<T>)
extern "C" Vector2_t15  List_1_Find_m12444_gshared (List_1_t421 * __this, Predicate_1_t1969 * ___match, const MethodInfo* method);
#define List_1_Find_m12444(__this, ___match, method) (( Vector2_t15  (*) (List_1_t421 *, Predicate_1_t1969 *, const MethodInfo*))List_1_Find_m12444_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12445_gshared (Object_t * __this /* static, unused */, Predicate_1_t1969 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12445(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1969 *, const MethodInfo*))List_1_CheckMatch_m12445_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12446_gshared (List_1_t421 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1969 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12446(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t421 *, int32_t, int32_t, Predicate_1_t1969 *, const MethodInfo*))List_1_GetIndex_m12446_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Enumerator_t1963  List_1_GetEnumerator_m12447_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12447(__this, method) (( Enumerator_t1963  (*) (List_1_t421 *, const MethodInfo*))List_1_GetEnumerator_m12447_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12448_gshared (List_1_t421 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12448(__this, ___item, method) (( int32_t (*) (List_1_t421 *, Vector2_t15 , const MethodInfo*))List_1_IndexOf_m12448_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12449_gshared (List_1_t421 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12449(__this, ___start, ___delta, method) (( void (*) (List_1_t421 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12449_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12450_gshared (List_1_t421 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12450(__this, ___index, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12450_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12451_gshared (List_1_t421 * __this, int32_t ___index, Vector2_t15  ___item, const MethodInfo* method);
#define List_1_Insert_m12451(__this, ___index, ___item, method) (( void (*) (List_1_t421 *, int32_t, Vector2_t15 , const MethodInfo*))List_1_Insert_m12451_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12452_gshared (List_1_t421 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12452(__this, ___collection, method) (( void (*) (List_1_t421 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12452_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool List_1_Remove_m12453_gshared (List_1_t421 * __this, Vector2_t15  ___item, const MethodInfo* method);
#define List_1_Remove_m12453(__this, ___item, method) (( bool (*) (List_1_t421 *, Vector2_t15 , const MethodInfo*))List_1_Remove_m12453_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12454_gshared (List_1_t421 * __this, Predicate_1_t1969 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12454(__this, ___match, method) (( int32_t (*) (List_1_t421 *, Predicate_1_t1969 *, const MethodInfo*))List_1_RemoveAll_m12454_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12455_gshared (List_1_t421 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12455(__this, ___index, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12455_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse()
extern "C" void List_1_Reverse_m12456_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_Reverse_m12456(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1_Reverse_m12456_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort()
extern "C" void List_1_Sort_m12457_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_Sort_m12457(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1_Sort_m12457_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12458_gshared (List_1_t421 * __this, Comparison_1_t1972 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12458(__this, ___comparison, method) (( void (*) (List_1_t421 *, Comparison_1_t1972 *, const MethodInfo*))List_1_Sort_m12458_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" Vector2U5BU5D_t420* List_1_ToArray_m12459_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_ToArray_m12459(__this, method) (( Vector2U5BU5D_t420* (*) (List_1_t421 *, const MethodInfo*))List_1_ToArray_m12459_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::TrimExcess()
extern "C" void List_1_TrimExcess_m12460_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12460(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1_TrimExcess_m12460_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12461_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12461(__this, method) (( int32_t (*) (List_1_t421 *, const MethodInfo*))List_1_get_Capacity_m12461_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12462_gshared (List_1_t421 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12462(__this, ___value, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12462_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t List_1_get_Count_m12463_gshared (List_1_t421 * __this, const MethodInfo* method);
#define List_1_get_Count_m12463(__this, method) (( int32_t (*) (List_1_t421 *, const MethodInfo*))List_1_get_Count_m12463_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t15  List_1_get_Item_m12464_gshared (List_1_t421 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12464(__this, ___index, method) (( Vector2_t15  (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_get_Item_m12464_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12465_gshared (List_1_t421 * __this, int32_t ___index, Vector2_t15  ___value, const MethodInfo* method);
#define List_1_set_Item_m12465(__this, ___index, ___value, method) (( void (*) (List_1_t421 *, int32_t, Vector2_t15 , const MethodInfo*))List_1_set_Item_m12465_gshared)(__this, ___index, ___value, method)
