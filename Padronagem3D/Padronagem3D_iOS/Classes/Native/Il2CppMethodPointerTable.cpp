﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void Teleport__ctor_m0 ();
extern "C" void Teleport_Start_m1 ();
extern "C" void Teleport_SetGazedAt_m2 ();
extern "C" void Teleport_Reset_m3 ();
extern "C" void Teleport_ToggleVRMode_m4 ();
extern "C" void Teleport_TeleportRandomly_m5 ();
extern "C" void TeleportLegacyUI__ctor_m6 ();
extern "C" void TeleportLegacyUI_Awake_m7 ();
extern "C" void TeleportLegacyUI_Update_m8 ();
extern "C" void TeleportLegacyUI_OnGUI_m9 ();
extern "C" void TeleportLegacyUI_OnDestroy_m10 ();
extern "C" void OnGUICallback__ctor_m11 ();
extern "C" void OnGUICallback_Invoke_m12 ();
extern "C" void OnGUICallback_BeginInvoke_m13 ();
extern "C" void OnGUICallback_EndInvoke_m14 ();
extern "C" void CardboardOnGUI__ctor_m15 ();
extern "C" void CardboardOnGUI_add_onGUICallback_m16 ();
extern "C" void CardboardOnGUI_remove_onGUICallback_m17 ();
extern "C" void CardboardOnGUI_OKToDraw_m18 ();
extern "C" void CardboardOnGUI_get_IsGUIVisible_m19 ();
extern "C" void CardboardOnGUI_set_IsGUIVisible_m20 ();
extern "C" void CardboardOnGUI_get_Triggered_m21 ();
extern "C" void CardboardOnGUI_Awake_m22 ();
extern "C" void CardboardOnGUI_Start_m23 ();
extern "C" void CardboardOnGUI_Create_m24 ();
extern "C" void CardboardOnGUI_LateUpdate_m25 ();
extern "C" void CardboardOnGUI_OnGUI_m26 ();
extern "C" void CardboardOnGUIMouse__ctor_m27 ();
extern "C" void CardboardOnGUIMouse_LateUpdate_m28 ();
extern "C" void CardboardOnGUIMouse_DrawPointerImage_m29 ();
extern "C" void CardboardOnGUIWindow__ctor_m30 ();
extern "C" void CardboardOnGUIWindow_Reset_m31 ();
extern "C" void CardboardOnGUIWindow_Awake_m32 ();
extern "C" void CardboardOnGUIWindow_Create_m33 ();
extern "C" void CardboardOnGUIWindow_OnDisable_m34 ();
extern "C" void CardboardOnGUIWindow_LateUpdate_m35 ();
extern "C" void SkyboxMesh__ctor_m36 ();
extern "C" void SkyboxMesh_Awake_m37 ();
extern "C" void StereoLensFlare__ctor_m38 ();
extern "C" void StereoLensFlare_Awake_m39 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0__ctor_m40 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m42 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0_MoveNext_m43 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0_Dispose_m44 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator0_Reset_m45 ();
extern "C" void Cardboard__ctor_m46 ();
extern "C" void Cardboard__cctor_m47 ();
extern "C" void Cardboard_add_OnTrigger_m48 ();
extern "C" void Cardboard_remove_OnTrigger_m49 ();
extern "C" void Cardboard_add_OnTilt_m50 ();
extern "C" void Cardboard_remove_OnTilt_m51 ();
extern "C" void Cardboard_get_SDK_m52 ();
extern "C" void Cardboard_get_DistortionCorrection_m53 ();
extern "C" void Cardboard_set_DistortionCorrection_m54 ();
extern "C" void Cardboard_get_VRModeEnabled_m55 ();
extern "C" void Cardboard_set_VRModeEnabled_m56 ();
extern "C" void Cardboard_get_EnableAlignmentMarker_m57 ();
extern "C" void Cardboard_set_EnableAlignmentMarker_m58 ();
extern "C" void Cardboard_get_EnableSettingsButton_m59 ();
extern "C" void Cardboard_set_EnableSettingsButton_m60 ();
extern "C" void Cardboard_get_TapIsTrigger_m61 ();
extern "C" void Cardboard_set_TapIsTrigger_m62 ();
extern "C" void Cardboard_get_NeckModelScale_m63 ();
extern "C" void Cardboard_set_NeckModelScale_m64 ();
extern "C" void Cardboard_get_AutoDriftCorrection_m65 ();
extern "C" void Cardboard_set_AutoDriftCorrection_m66 ();
extern "C" void Cardboard_get_SyncWithCardboardApp_m67 ();
extern "C" void Cardboard_set_SyncWithCardboardApp_m68 ();
extern "C" void Cardboard_get_NativeDistortionCorrectionSupported_m69 ();
extern "C" void Cardboard_set_NativeDistortionCorrectionSupported_m70 ();
extern "C" void Cardboard_get_NativeUILayerSupported_m71 ();
extern "C" void Cardboard_set_NativeUILayerSupported_m72 ();
extern "C" void Cardboard_get_StereoScreen_m73 ();
extern "C" void Cardboard_set_StereoScreen_m74 ();
extern "C" void Cardboard_get_UseDistortionEffect_m75 ();
extern "C" void Cardboard_get_Profile_m76 ();
extern "C" void Cardboard_get_HeadPose_m77 ();
extern "C" void Cardboard_EyePose_m78 ();
extern "C" void Cardboard_Projection_m79 ();
extern "C" void Cardboard_Viewport_m80 ();
extern "C" void Cardboard_get_ComfortableViewingRange_m81 ();
extern "C" void Cardboard_InitDevice_m82 ();
extern "C" void Cardboard_Awake_m83 ();
extern "C" void Cardboard_get_Triggered_m84 ();
extern "C" void Cardboard_set_Triggered_m85 ();
extern "C" void Cardboard_get_Tilted_m86 ();
extern "C" void Cardboard_set_Tilted_m87 ();
extern "C" void Cardboard_UpdateState_m88 ();
extern "C" void Cardboard_DispatchEvents_m89 ();
extern "C" void Cardboard_AddDummyCamera_m90 ();
extern "C" void Cardboard_EndOfFrame_m91 ();
extern "C" void Cardboard_CreateStereoScreen_m92 ();
extern "C" void Cardboard_Recenter_m93 ();
extern "C" void Cardboard_SetTouchCoordinates_m94 ();
extern "C" void Cardboard_ShowSettingsDialog_m95 ();
extern "C" void Cardboard_OnEnable_m96 ();
extern "C" void Cardboard_OnDisable_m97 ();
extern "C" void Cardboard_OnApplicationPause_m98 ();
extern "C" void Cardboard_OnApplicationFocus_m99 ();
extern "C" void Cardboard_OnLevelWasLoaded_m100 ();
extern "C" void Cardboard_OnDestroy_m101 ();
extern "C" void Cardboard_OnApplicationQuit_m102 ();
extern "C" void Cardboard_get_nativeDistortionCorrection_m103 ();
extern "C" void Cardboard_set_nativeDistortionCorrection_m104 ();
extern "C" void Cardboard_get_InCardboard_m105 ();
extern "C" void Cardboard_get_CardboardTriggered_m106 ();
extern "C" void Cardboard_get_HeadView_m107 ();
extern "C" void Cardboard_get_HeadRotation_m108 ();
extern "C" void Cardboard_get_HeadPosition_m109 ();
extern "C" void Cardboard_EyeView_m110 ();
extern "C" void Cardboard_EyeOffset_m111 ();
extern "C" void Cardboard_UndistortedProjection_m112 ();
extern "C" void Cardboard_EyeRect_m113 ();
extern "C" void Cardboard_get_MinimumComfortDistance_m114 ();
extern "C" void Cardboard_get_MaximumComfortDistance_m115 ();
extern "C" void CardboardEye__ctor_m116 ();
extern "C" void CardboardEye_get_Controller_m117 ();
extern "C" void CardboardEye_get_Head_m118 ();
extern "C" void CardboardEye_Awake_m119 ();
extern "C" void CardboardEye_Start_m120 ();
extern "C" void CardboardEye_FixProjection_m121 ();
extern "C" void CardboardEye_Setup_m122 ();
extern "C" void CardboardEye_Render_m123 ();
extern "C" void CardboardEye_OnPreCull_m124 ();
extern "C" void CardboardEye_CopyCameraAndMakeSideBySide_m125 ();
extern "C" void CardboardHead__ctor_m126 ();
extern "C" void CardboardHead_get_Gaze_m127 ();
extern "C" void CardboardHead_Update_m128 ();
extern "C" void CardboardHead_LateUpdate_m129 ();
extern "C" void CardboardHead_UpdateHead_m130 ();
extern "C" void Distortion_distort_m131 ();
extern "C" void CardboardProfile__ctor_m132 ();
extern "C" void CardboardProfile__cctor_m133 ();
extern "C" void CardboardProfile_Clone_m134 ();
extern "C" void CardboardProfile_get_VerticalLensOffset_m135 ();
extern "C" void CardboardProfile_GetKnownProfile_m136 ();
extern "C" void CardboardProfile_GetLeftEyeVisibleTanAngles_m137 ();
extern "C" void CardboardProfile_GetLeftEyeNoLensTanAngles_m138 ();
extern "C" void CardboardProfile_GetLeftEyeVisibleScreenRect_m139 ();
extern "C" void CardboardProfile_solveLeastSquares_m140 ();
extern "C" void CardboardProfile_ApproximateInverse_m141 ();
extern "C" void CardboardProfile_ApproximateInverse_m142 ();
extern "C" void CardboardUILayer__ctor_m143 ();
extern "C" void CardboardUILayer__cctor_m144 ();
extern "C" void CardboardUILayer_ComputeMatrix_m145 ();
extern "C" void CardboardUILayer_Draw_m146 ();
extern "C" void GazeInputModule__ctor_m147 ();
extern "C" void GazeInputModule_ShouldActivateModule_m148 ();
extern "C" void GazeInputModule_DeactivateModule_m149 ();
extern "C" void GazeInputModule_IsPointerOverGameObject_m150 ();
extern "C" void GazeInputModule_Process_m151 ();
extern "C" void GazeInputModule_CastRayFromGaze_m152 ();
extern "C" void GazeInputModule_UpdateCurrentObject_m153 ();
extern "C" void GazeInputModule_PlaceCursor_m154 ();
extern "C" void GazeInputModule_HandlePendingClick_m155 ();
extern "C" void GazeInputModule_HandleTrigger_m156 ();
extern "C" void Pose3D__ctor_m157 ();
extern "C" void Pose3D__ctor_m158 ();
extern "C" void Pose3D__ctor_m159 ();
extern "C" void Pose3D__cctor_m160 ();
extern "C" void Pose3D_get_Position_m161 ();
extern "C" void Pose3D_set_Position_m162 ();
extern "C" void Pose3D_get_Orientation_m163 ();
extern "C" void Pose3D_set_Orientation_m164 ();
extern "C" void Pose3D_get_Matrix_m165 ();
extern "C" void Pose3D_set_Matrix_m166 ();
extern "C" void Pose3D_get_RightHandedMatrix_m167 ();
extern "C" void Pose3D_Set_m168 ();
extern "C" void Pose3D_Set_m169 ();
extern "C" void MutablePose3D__ctor_m170 ();
extern "C" void MutablePose3D_Set_m171 ();
extern "C" void MutablePose3D_Set_m172 ();
extern "C" void MutablePose3D_SetRightHanded_m173 ();
extern "C" void RadialUndistortionEffect__ctor_m174 ();
extern "C" void RadialUndistortionEffect_Awake_m175 ();
extern "C" void RadialUndistortionEffect_OnRenderImage_m176 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1__ctor_m177 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m178 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m179 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1_MoveNext_m180 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Dispose_m181 ();
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Reset_m182 ();
extern "C" void StereoController__ctor_m183 ();
extern "C" void StereoController_get_Eyes_m184 ();
extern "C" void StereoController_InvalidateEyes_m185 ();
extern "C" void StereoController_get_Head_m186 ();
extern "C" void StereoController_get_StereoScreen_m187 ();
extern "C" void StereoController_get_ScreenHeight_m188 ();
extern "C" void StereoController_Awake_m189 ();
extern "C" void StereoController_AddStereoRig_m190 ();
extern "C" void StereoController_CreateEye_m191 ();
extern "C" void StereoController_ComputeStereoAdjustment_m192 ();
extern "C" void StereoController_OnEnable_m193 ();
extern "C" void StereoController_OnDisable_m194 ();
extern "C" void StereoController_OnPreCull_m195 ();
extern "C" void StereoController_EndOfFrame_m196 ();
extern "C" void StereoController_U3Cget_EyesU3Em__0_m197 ();
extern "C" void StereoController_U3Cget_HeadU3Em__1_m198 ();
extern "C" void VREventCallback__ctor_m199 ();
extern "C" void VREventCallback_Invoke_m200 ();
extern "C" void VREventCallback_BeginInvoke_m201 ();
extern "C" void VREventCallback_EndInvoke_m202 ();
extern "C" void BaseCardboardDevice__ctor_m203 ();
extern "C" void BaseCardboardDevice_SupportsNativeDistortionCorrection_m204 ();
extern "C" void BaseCardboardDevice_SupportsNativeUILayer_m205 ();
extern "C" void BaseCardboardDevice_SetDistortionCorrectionEnabled_m206 ();
extern "C" void BaseCardboardDevice_SetAlignmentMarkerEnabled_m207 ();
extern "C" void BaseCardboardDevice_SetNeckModelScale_m208 ();
extern "C" void BaseCardboardDevice_SetAutoDriftCorrectionEnabled_m209 ();
extern "C" void BaseCardboardDevice_SetElectronicDisplayStabilizationEnabled_m210 ();
extern "C" void BaseCardboardDevice_Init_m211 ();
extern "C" void BaseCardboardDevice_SetStereoScreen_m212 ();
extern "C" void BaseCardboardDevice_UpdateState_m213 ();
extern "C" void BaseCardboardDevice_UpdateScreenData_m214 ();
extern "C" void BaseCardboardDevice_Recenter_m215 ();
extern "C" void BaseCardboardDevice_PostRender_m216 ();
extern "C" void BaseCardboardDevice_OnApplicationQuit_m217 ();
extern "C" void BaseCardboardDevice_UpdateView_m218 ();
extern "C" void BaseCardboardDevice_UpdateProfile_m219 ();
extern "C" void BaseCardboardDevice_ExtractMatrix_m220 ();
extern "C" void BaseCardboardDevice_ProcessEvents_m221 ();
extern "C" void BaseCardboardDevice_OnVREvent_m222 ();
extern "C" void BaseCardboardDevice_Start_m223 ();
extern "C" void BaseCardboardDevice_SetEventCallback_m224 ();
extern "C" void BaseCardboardDevice_SetTextureId_m225 ();
extern "C" void BaseCardboardDevice_EnableDistortionCorrection_m226 ();
extern "C" void BaseCardboardDevice_EnableAlignmentMarker_m227 ();
extern "C" void BaseCardboardDevice_EnableAutoDriftCorrection_m228 ();
extern "C" void BaseCardboardDevice_EnableElectronicDisplayStabilization_m229 ();
extern "C" void BaseCardboardDevice_SetNeckModelFactor_m230 ();
extern "C" void BaseCardboardDevice_ResetHeadTracker_m231 ();
extern "C" void BaseCardboardDevice_GetProfile_m232 ();
extern "C" void BaseCardboardDevice_GetHeadPose_m233 ();
extern "C" void BaseCardboardDevice_GetViewParameters_m234 ();
extern "C" void BaseCardboardDevice_Stop_m235 ();
extern "C" void BaseVRDevice__ctor_m236 ();
extern "C" void BaseVRDevice__cctor_m237 ();
extern "C" void BaseVRDevice_get_Profile_m238 ();
extern "C" void BaseVRDevice_set_Profile_m239 ();
extern "C" void BaseVRDevice_SupportsNativeDistortionCorrection_m240 ();
extern "C" void BaseVRDevice_SupportsNativeUILayer_m241 ();
extern "C" void BaseVRDevice_SupportsUnityRenderEvent_m242 ();
extern "C" void BaseVRDevice_CreateStereoScreen_m243 ();
extern "C" void BaseVRDevice_SetDefaultDeviceProfile_m244 ();
extern "C" void BaseVRDevice_ShowSettingsDialog_m245 ();
extern "C" void BaseVRDevice_GetHeadPose_m246 ();
extern "C" void BaseVRDevice_GetEyePose_m247 ();
extern "C" void BaseVRDevice_GetProjection_m248 ();
extern "C" void BaseVRDevice_GetViewport_m249 ();
extern "C" void BaseVRDevice_SetTouchCoordinates_m250 ();
extern "C" void BaseVRDevice_OnPause_m251 ();
extern "C" void BaseVRDevice_OnFocus_m252 ();
extern "C" void BaseVRDevice_Reset_m253 ();
extern "C" void BaseVRDevice_OnApplicationQuit_m254 ();
extern "C" void BaseVRDevice_Destroy_m255 ();
extern "C" void BaseVRDevice_ComputeEyesFromProfile_m256 ();
extern "C" void BaseVRDevice_MakeProjection_m257 ();
extern "C" void BaseVRDevice_GetDevice_m258 ();
extern "C" void CardboardiOSDevice__ctor_m259 ();
extern "C" void CardboardiOSDevice_SupportsNativeDistortionCorrection_m260 ();
extern "C" void CardboardiOSDevice_SupportsNativeUILayer_m261 ();
extern "C" void CardboardiOSDevice_SetVRModeEnabled_m262 ();
extern "C" void CardboardiOSDevice_SetSettingsButtonEnabled_m263 ();
extern "C" void CardboardiOSDevice_SetAutoDriftCorrectionEnabled_m264 ();
extern "C" void CardboardiOSDevice_SetTapIsTrigger_m265 ();
extern "C" void CardboardiOSDevice_SetDefaultDeviceProfile_m266 ();
extern "C" void CardboardiOSDevice_Init_m267 ();
extern "C" void CardboardiOSDevice_PostRender_m268 ();
extern "C" void CardboardiOSDevice_OnFocus_m269 ();
extern "C" void CardboardiOSDevice_OnPause_m270 ();
extern "C" void CardboardiOSDevice_ShowSettingsDialog_m271 ();
extern "C" void CardboardiOSDevice_isOpenGLAPI_m272 ();
extern "C" void CardboardiOSDevice_setVRModeEnabled_m273 ();
extern "C" void CardboardiOSDevice_setSettingsButtonEnabled_m274 ();
extern "C" void CardboardiOSDevice_setSyncWithCardboardEnabled_m275 ();
extern "C" void CardboardiOSDevice_readProfile_m276 ();
extern "C" void CardboardiOSDevice_setDefaultDeviceProfile_m277 ();
extern "C" void CardboardiOSDevice_isOnboardingDone_m278 ();
extern "C" void CardboardiOSDevice_launchSettingsDialog_m279 ();
extern "C" void CardboardiOSDevice_launchOnboardingDialog_m280 ();
extern "C" void DiveJava__ctor_m281 ();
extern "C" void DiveJava__cctor_m282 ();
extern "C" void DiveJava_Start_m283 ();
extern "C" void DiveJava_Update_m284 ();
extern "C" void DiveJava_setFullscreen_m285 ();
extern "C" void DiveJava_init_m286 ();
extern "C" void DiveMouseLook__ctor_m287 ();
extern "C" void DiveMouseLook_Update_m288 ();
extern "C" void DiveMouseLook_Start_m289 ();
extern "C" void NaturalOrientation__ctor_m290 ();
extern "C" void NaturalOrientation__cctor_m291 ();
extern "C" void OffsetCenter__ctor_m292 ();
extern "C" void OffsetCenter_Start_m293 ();
extern "C" void OffsetCenter_Update_m294 ();
extern "C" void OffsetCenter_SetVanishingPoint_m295 ();
extern "C" void OffsetCenter_PerspectiveOffCenter_m296 ();
extern "C" void OffsetCenter_setCorrectionFactor_m297 ();
extern "C" void OpenDiveSensor__ctor_m298 ();
extern "C" void OpenDiveSensor_add_MagnetTriggered_m299 ();
extern "C" void OpenDiveSensor_remove_MagnetTriggered_m300 ();
extern "C" void OpenDiveSensor_initialize_sensors_m301 ();
extern "C" void OpenDiveSensor_stop_sensors_m302 ();
extern "C" void OpenDiveSensor_get_q0_m303 ();
extern "C" void OpenDiveSensor_get_q1_m304 ();
extern "C" void OpenDiveSensor_get_q2_m305 ();
extern "C" void OpenDiveSensor_get_q3_m306 ();
extern "C" void OpenDiveSensor_DiveUpdateGyroData_m307 ();
extern "C" void OpenDiveSensor_get_q_m308 ();
extern "C" void OpenDiveSensor_get_magnet_m309 ();
extern "C" void OpenDiveSensor_get_m_m310 ();
extern "C" void OpenDiveSensor_Start_m311 ();
extern "C" void OpenDiveSensor_OnMagnetTriggered_m312 ();
extern "C" void OpenDiveSensor_Update_m313 ();
extern "C" void OpenDiveSensor_OnGUI_m314 ();
extern "C" void OpenDiveSensor_OnApplicationQuit_m315 ();
extern "C" void DInput__cctor_m316 ();
extern "C" void DInput_save_m317 ();
extern "C" void DInput_load_m318 ();
extern "C" void U3CPrivateImplementationDetailsU3E__ctor_m319 ();
extern "C" void U24__ctor_m616 ();
extern "C" void U24_MoveNext_m617 ();
extern "C" void U24DieU248__ctor_m618 ();
extern "C" void U24DieU248_GetEnumerator_m619 ();
extern "C" void DiveFPSController__ctor_m620 ();
extern "C" void DiveFPSController_Awake_m621 ();
extern "C" void DiveFPSController_toggle_autowalk_m622 ();
extern "C" void DiveFPSController_JumpUp_m623 ();
extern "C" void DiveFPSController_Start_m624 ();
extern "C" void DiveFPSController_OnControllerColliderHit_m625 ();
extern "C" void DiveFPSController_Die_m626 ();
extern "C" void DiveFPSController_Update_m627 ();
extern "C" void DiveFPSController_LateUpdate_m628 ();
extern "C" void DiveFPSController_OnGUI_m629 ();
extern "C" void DiveFPSController_OnTriggerEnter_m630 ();
extern "C" void DiveFPSController_OnTriggerExit_m631 ();
extern "C" void DiveFPSController_Main_m632 ();
extern "C" void fps__ctor_m633 ();
extern "C" void fps_Start_m634 ();
extern "C" void fps_Update_m635 ();
extern "C" void fps_Main_m636 ();
extern "C" void U24__ctor_m637 ();
extern "C" void U24_MoveNext_m638 ();
extern "C" void U24StartU2417__ctor_m639 ();
extern "C" void U24StartU2417_GetEnumerator_m640 ();
extern "C" void U24__ctor_m641 ();
extern "C" void U24_MoveNext_m642 ();
extern "C" void U24FadeGUITextureU2422__ctor_m643 ();
extern "C" void U24FadeGUITextureU2422_GetEnumerator_m644 ();
extern "C" void splashscreen__ctor_m645 ();
extern "C" void splashscreen_Start_m646 ();
extern "C" void splashscreen_FadeGUITexture_m647 ();
extern "C" void splashscreen_Main_m648 ();
extern "C" void AssetBundleCreateRequest__ctor_m702 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m703 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m704 ();
extern "C" void AssetBundleRequest__ctor_m705 ();
extern "C" void AssetBundleRequest_get_asset_m706 ();
extern "C" void AssetBundleRequest_get_allAssets_m707 ();
extern "C" void AssetBundle_LoadAsset_m708 ();
extern "C" void AssetBundle_LoadAsset_Internal_m709 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m710 ();
extern "C" void SystemInfo_get_supportsRenderTextures_m346 ();
extern "C" void WaitForSeconds__ctor_m694 ();
extern "C" void WaitForFixedUpdate__ctor_m711 ();
extern "C" void WaitForEndOfFrame__ctor_m391 ();
extern "C" void Coroutine__ctor_m712 ();
extern "C" void Coroutine_ReleaseCoroutine_m713 ();
extern "C" void Coroutine_Finalize_m714 ();
extern "C" void ScriptableObject__ctor_m715 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m716 ();
extern "C" void ScriptableObject_CreateInstance_m717 ();
extern "C" void ScriptableObject_CreateInstance_m718 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m719 ();
extern "C" void UnhandledExceptionHandler__ctor_m720 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m721 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m722 ();
extern "C" void UnhandledExceptionHandler_PrintException_m723 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m724 ();
extern "C" void GameCenterPlatform__ctor_m725 ();
extern "C" void GameCenterPlatform__cctor_m726 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m727 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m728 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m729 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m730 ();
extern "C" void GameCenterPlatform_Internal_UserName_m731 ();
extern "C" void GameCenterPlatform_Internal_UserID_m732 ();
extern "C" void GameCenterPlatform_Internal_Underage_m733 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m734 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m735 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m736 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m737 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m738 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m739 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m740 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m741 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m742 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m743 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m744 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m745 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m746 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m747 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m748 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m749 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m750 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m751 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m752 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m753 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m754 ();
extern "C" void GameCenterPlatform_ClearFriends_m755 ();
extern "C" void GameCenterPlatform_SetFriends_m756 ();
extern "C" void GameCenterPlatform_SetFriendImage_m757 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m758 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m759 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m760 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m761 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m762 ();
extern "C" void GameCenterPlatform_get_localUser_m763 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m764 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m765 ();
extern "C" void GameCenterPlatform_ReportProgress_m766 ();
extern "C" void GameCenterPlatform_LoadAchievements_m767 ();
extern "C" void GameCenterPlatform_ReportScore_m768 ();
extern "C" void GameCenterPlatform_LoadScores_m769 ();
extern "C" void GameCenterPlatform_LoadScores_m770 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m771 ();
extern "C" void GameCenterPlatform_GetLoading_m772 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m773 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m774 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m775 ();
extern "C" void GameCenterPlatform_ClearUsers_m776 ();
extern "C" void GameCenterPlatform_SetUser_m777 ();
extern "C" void GameCenterPlatform_SetUserImage_m778 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m779 ();
extern "C" void GameCenterPlatform_LoadUsers_m780 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m781 ();
extern "C" void GameCenterPlatform_SafeClearArray_m782 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m783 ();
extern "C" void GameCenterPlatform_CreateAchievement_m784 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m785 ();
extern "C" void GcLeaderboard__ctor_m786 ();
extern "C" void GcLeaderboard_Finalize_m787 ();
extern "C" void GcLeaderboard_Contains_m788 ();
extern "C" void GcLeaderboard_SetScores_m789 ();
extern "C" void GcLeaderboard_SetLocalScore_m790 ();
extern "C" void GcLeaderboard_SetMaxRange_m791 ();
extern "C" void GcLeaderboard_SetTitle_m792 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m793 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m794 ();
extern "C" void GcLeaderboard_Loading_m795 ();
extern "C" void GcLeaderboard_Dispose_m796 ();
extern "C" void QualitySettings_get_activeColorSpace_m797 ();
extern "C" void Mesh__ctor_m798 ();
extern "C" void Mesh_Internal_Create_m799 ();
extern "C" void Mesh_Clear_m800 ();
extern "C" void Mesh_Clear_m801 ();
extern "C" void Mesh_get_vertices_m802 ();
extern "C" void Mesh_SetVertices_m803 ();
extern "C" void Mesh_SetVerticesInternal_m804 ();
extern "C" void Mesh_get_normals_m805 ();
extern "C" void Mesh_SetNormals_m806 ();
extern "C" void Mesh_SetNormalsInternal_m807 ();
extern "C" void Mesh_get_tangents_m808 ();
extern "C" void Mesh_SetTangents_m809 ();
extern "C" void Mesh_SetTangentsInternal_m810 ();
extern "C" void Mesh_get_uv_m811 ();
extern "C" void Mesh_get_uv2_m812 ();
extern "C" void Mesh_SetUVs_m813 ();
extern "C" void Mesh_SetUVInternal_m814 ();
extern "C" void Mesh_get_colors32_m815 ();
extern "C" void Mesh_SetColors_m816 ();
extern "C" void Mesh_SetColors32Internal_m817 ();
extern "C" void Mesh_RecalculateBounds_m818 ();
extern "C" void Mesh_SetTriangles_m819 ();
extern "C" void Mesh_SetTrianglesInternal_m820 ();
extern "C" void Mesh_GetIndices_m821 ();
extern "C" void BoneWeight_get_weight0_m822 ();
extern "C" void BoneWeight_set_weight0_m823 ();
extern "C" void BoneWeight_get_weight1_m824 ();
extern "C" void BoneWeight_set_weight1_m825 ();
extern "C" void BoneWeight_get_weight2_m826 ();
extern "C" void BoneWeight_set_weight2_m827 ();
extern "C" void BoneWeight_get_weight3_m828 ();
extern "C" void BoneWeight_set_weight3_m829 ();
extern "C" void BoneWeight_get_boneIndex0_m830 ();
extern "C" void BoneWeight_set_boneIndex0_m831 ();
extern "C" void BoneWeight_get_boneIndex1_m832 ();
extern "C" void BoneWeight_set_boneIndex1_m833 ();
extern "C" void BoneWeight_get_boneIndex2_m834 ();
extern "C" void BoneWeight_set_boneIndex2_m835 ();
extern "C" void BoneWeight_get_boneIndex3_m836 ();
extern "C" void BoneWeight_set_boneIndex3_m837 ();
extern "C" void BoneWeight_GetHashCode_m838 ();
extern "C" void BoneWeight_Equals_m839 ();
extern "C" void BoneWeight_op_Equality_m840 ();
extern "C" void BoneWeight_op_Inequality_m841 ();
extern "C" void Renderer_set_enabled_m385 ();
extern "C" void Renderer_get_material_m324 ();
extern "C" void Renderer_set_material_m384 ();
extern "C" void Renderer_get_sortingLayerID_m842 ();
extern "C" void Renderer_get_sortingOrder_m843 ();
extern "C" void Graphics_DrawTexture_m468 ();
extern "C" void Graphics_DrawTexture_m844 ();
extern "C" void Graphics_DrawTexture_m845 ();
extern "C" void Graphics_DrawTexture_m846 ();
extern "C" void Graphics_DrawTexture_m847 ();
extern "C" void Graphics_Blit_m548 ();
extern "C" void Graphics_Blit_m549 ();
extern "C" void Graphics_Blit_m848 ();
extern "C" void Graphics_Internal_BlitMaterial_m849 ();
extern "C" void Screen_get_width_m353 ();
extern "C" void Screen_get_height_m355 ();
extern "C" void Screen_get_dpi_m494 ();
extern "C" void Screen_set_sleepTimeout_m408 ();
extern "C" void GL_Vertex3_m499 ();
extern "C" void GL_Begin_m498 ();
extern "C" void GL_End_m500 ();
extern "C" void GL_LoadPixelMatrix_m497 ();
extern "C" void GL_LoadPixelMatrixArgs_m850 ();
extern "C" void GL_LoadPixelMatrix_m466 ();
extern "C" void GL_PushMatrix_m465 ();
extern "C" void GL_PopMatrix_m469 ();
extern "C" void GL_Clear_m363 ();
extern "C" void GL_Clear_m851 ();
extern "C" void GL_Internal_Clear_m852 ();
extern "C" void GL_INTERNAL_CALL_Internal_Clear_m853 ();
extern "C" void GL_InvalidateState_m570 ();
extern "C" void GL_IssuePluginEvent_m569 ();
extern "C" void GUITexture_get_color_m659 ();
extern "C" void GUITexture_set_color_m660 ();
extern "C" void GUITexture_INTERNAL_get_color_m854 ();
extern "C" void GUITexture_INTERNAL_set_color_m855 ();
extern "C" void GUITexture_set_texture_m658 ();
extern "C" void GUITexture_set_pixelInset_m654 ();
extern "C" void GUITexture_INTERNAL_set_pixelInset_m856 ();
extern "C" void GUILayer_HitTest_m857 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m858 ();
extern "C" void Texture__ctor_m859 ();
extern "C" void Texture_Internal_GetWidth_m860 ();
extern "C" void Texture_Internal_GetHeight_m861 ();
extern "C" void Texture_get_width_m862 ();
extern "C" void Texture_set_width_m863 ();
extern "C" void Texture_get_height_m864 ();
extern "C" void Texture_set_height_m865 ();
extern "C" void Texture_GetNativeTextureID_m566 ();
extern "C" void Texture2D__ctor_m655 ();
extern "C" void Texture2D_Internal_Create_m866 ();
extern "C" void Texture2D_get_whiteTexture_m867 ();
extern "C" void Texture2D_SetPixel_m656 ();
extern "C" void Texture2D_INTERNAL_CALL_SetPixel_m868 ();
extern "C" void Texture2D_GetPixelBilinear_m869 ();
extern "C" void Texture2D_Apply_m870 ();
extern "C" void Texture2D_Apply_m657 ();
extern "C" void RenderTexture__ctor_m356 ();
extern "C" void RenderTexture_Internal_CreateRenderTexture_m871 ();
extern "C" void RenderTexture_GetTemporary_m872 ();
extern "C" void RenderTexture_GetTemporary_m464 ();
extern "C" void RenderTexture_ReleaseTemporary_m470 ();
extern "C" void RenderTexture_Internal_GetWidth_m873 ();
extern "C" void RenderTexture_Internal_SetWidth_m874 ();
extern "C" void RenderTexture_Internal_GetHeight_m875 ();
extern "C" void RenderTexture_Internal_SetHeight_m876 ();
extern "C" void RenderTexture_Internal_SetSRGBReadWrite_m877 ();
extern "C" void RenderTexture_get_width_m352 ();
extern "C" void RenderTexture_set_width_m878 ();
extern "C" void RenderTexture_get_height_m354 ();
extern "C" void RenderTexture_set_height_m879 ();
extern "C" void RenderTexture_get_depth_m462 ();
extern "C" void RenderTexture_set_depth_m880 ();
extern "C" void RenderTexture_get_format_m463 ();
extern "C" void RenderTexture_set_format_m881 ();
extern "C" void RenderTexture_Create_m357 ();
extern "C" void RenderTexture_INTERNAL_CALL_Create_m882 ();
extern "C" void RenderTexture_Release_m350 ();
extern "C" void RenderTexture_INTERNAL_CALL_Release_m883 ();
extern "C" void RenderTexture_IsCreated_m401 ();
extern "C" void RenderTexture_INTERNAL_CALL_IsCreated_m884 ();
extern "C" void RenderTexture_get_active_m361 ();
extern "C" void RenderTexture_set_active_m362 ();
extern "C" void StateChanged__ctor_m885 ();
extern "C" void StateChanged_Invoke_m886 ();
extern "C" void StateChanged_BeginInvoke_m887 ();
extern "C" void StateChanged_EndInvoke_m888 ();
extern "C" void CullingGroup_Finalize_m889 ();
extern "C" void CullingGroup_Dispose_m890 ();
extern "C" void CullingGroup_SendEvents_m891 ();
extern "C" void CullingGroup_FinalizerFailure_m892 ();
extern "C" void GradientColorKey__ctor_m893 ();
extern "C" void GradientAlphaKey__ctor_m894 ();
extern "C" void Gradient__ctor_m895 ();
extern "C" void Gradient_Init_m896 ();
extern "C" void Gradient_Cleanup_m897 ();
extern "C" void Gradient_Finalize_m898 ();
extern "C" void TouchScreenKeyboard__ctor_m899 ();
extern "C" void TouchScreenKeyboard_Destroy_m900 ();
extern "C" void TouchScreenKeyboard_Finalize_m901 ();
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m902 ();
extern "C" void TouchScreenKeyboard_get_isSupported_m903 ();
extern "C" void TouchScreenKeyboard_Open_m904 ();
extern "C" void TouchScreenKeyboard_Open_m905 ();
extern "C" void TouchScreenKeyboard_Open_m906 ();
extern "C" void TouchScreenKeyboard_get_text_m907 ();
extern "C" void TouchScreenKeyboard_set_text_m908 ();
extern "C" void TouchScreenKeyboard_set_hideInput_m909 ();
extern "C" void TouchScreenKeyboard_get_active_m910 ();
extern "C" void TouchScreenKeyboard_set_active_m911 ();
extern "C" void TouchScreenKeyboard_get_done_m912 ();
extern "C" void TouchScreenKeyboard_get_wasCanceled_m913 ();
extern "C" void LayerMask_get_value_m473 ();
extern "C" void LayerMask_set_value_m914 ();
extern "C" void LayerMask_LayerToName_m915 ();
extern "C" void LayerMask_NameToLayer_m916 ();
extern "C" void LayerMask_GetMask_m917 ();
extern "C" void LayerMask_op_Implicit_m918 ();
extern "C" void LayerMask_op_Implicit_m423 ();
extern "C" void Vector2__ctor_m373 ();
extern "C" void Vector2_get_Item_m919 ();
extern "C" void Vector2_set_Item_m920 ();
extern "C" void Vector2_Scale_m921 ();
extern "C" void Vector2_Normalize_m922 ();
extern "C" void Vector2_get_normalized_m436 ();
extern "C" void Vector2_ToString_m923 ();
extern "C" void Vector2_GetHashCode_m924 ();
extern "C" void Vector2_Equals_m925 ();
extern "C" void Vector2_Dot_m926 ();
extern "C" void Vector2_get_magnitude_m927 ();
extern "C" void Vector2_get_sqrMagnitude_m374 ();
extern "C" void Vector2_SqrMagnitude_m928 ();
extern "C" void Vector2_get_zero_m365 ();
extern "C" void Vector2_get_one_m929 ();
extern "C" void Vector2_get_up_m930 ();
extern "C" void Vector2_op_Addition_m931 ();
extern "C" void Vector2_op_Subtraction_m932 ();
extern "C" void Vector2_op_Multiply_m437 ();
extern "C" void Vector2_op_Division_m933 ();
extern "C" void Vector2_op_Equality_m934 ();
extern "C" void Vector2_op_Inequality_m935 ();
extern "C" void Vector2_op_Implicit_m435 ();
extern "C" void Vector2_op_Implicit_m936 ();
extern "C" void Vector3__ctor_m386 ();
extern "C" void Vector3__ctor_m937 ();
extern "C" void Vector3_Lerp_m679 ();
extern "C" void Vector3_get_Item_m938 ();
extern "C" void Vector3_set_Item_m939 ();
extern "C" void Vector3_GetHashCode_m940 ();
extern "C" void Vector3_Equals_m941 ();
extern "C" void Vector3_Normalize_m942 ();
extern "C" void Vector3_get_normalized_m943 ();
extern "C" void Vector3_ToString_m944 ();
extern "C" void Vector3_ToString_m945 ();
extern "C" void Vector3_Dot_m946 ();
extern "C" void Vector3_Distance_m947 ();
extern "C" void Vector3_Magnitude_m948 ();
extern "C" void Vector3_get_magnitude_m562 ();
extern "C" void Vector3_SqrMagnitude_m949 ();
extern "C" void Vector3_get_sqrMagnitude_m950 ();
extern "C" void Vector3_Min_m951 ();
extern "C" void Vector3_Max_m952 ();
extern "C" void Vector3_get_zero_m397 ();
extern "C" void Vector3_get_one_m477 ();
extern "C" void Vector3_get_forward_m442 ();
extern "C" void Vector3_get_back_m953 ();
extern "C" void Vector3_get_up_m954 ();
extern "C" void Vector3_get_down_m955 ();
extern "C" void Vector3_get_left_m956 ();
extern "C" void Vector3_get_right_m474 ();
extern "C" void Vector3_op_Addition_m443 ();
extern "C" void Vector3_op_Subtraction_m561 ();
extern "C" void Vector3_op_Multiply_m332 ();
extern "C" void Vector3_op_Multiply_m441 ();
extern "C" void Vector3_op_Division_m675 ();
extern "C" void Vector3_op_Equality_m957 ();
extern "C" void Vector3_op_Inequality_m674 ();
extern "C" void Color__ctor_m958 ();
extern "C" void Color__ctor_m492 ();
extern "C" void Color_ToString_m959 ();
extern "C" void Color_GetHashCode_m960 ();
extern "C" void Color_Equals_m961 ();
extern "C" void Color_Lerp_m962 ();
extern "C" void Color_get_red_m326 ();
extern "C" void Color_get_green_m325 ();
extern "C" void Color_get_white_m963 ();
extern "C" void Color_get_black_m416 ();
extern "C" void Color_get_clear_m339 ();
extern "C" void Color_op_Multiply_m964 ();
extern "C" void Color_op_Implicit_m965 ();
extern "C" void Color_op_Implicit_m966 ();
extern "C" void Color32__ctor_m967 ();
extern "C" void Color32_ToString_m968 ();
extern "C" void Color32_op_Implicit_m969 ();
extern "C" void Color32_op_Implicit_m970 ();
extern "C" void Quaternion__ctor_m971 ();
extern "C" void Quaternion_get_identity_m475 ();
extern "C" void Quaternion_Dot_m972 ();
extern "C" void Quaternion_AngleAxis_m596 ();
extern "C" void Quaternion_INTERNAL_CALL_AngleAxis_m973 ();
extern "C" void Quaternion_LookRotation_m547 ();
extern "C" void Quaternion_INTERNAL_CALL_LookRotation_m974 ();
extern "C" void Quaternion_Inverse_m975 ();
extern "C" void Quaternion_INTERNAL_CALL_Inverse_m976 ();
extern "C" void Quaternion_ToString_m977 ();
extern "C" void Quaternion_get_eulerAngles_m680 ();
extern "C" void Quaternion_Euler_m681 ();
extern "C" void Quaternion_Internal_ToEulerRad_m978 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m979 ();
extern "C" void Quaternion_Internal_FromEulerRad_m980 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m981 ();
extern "C" void Quaternion_GetHashCode_m982 ();
extern "C" void Quaternion_Equals_m983 ();
extern "C" void Quaternion_op_Multiply_m485 ();
extern "C" void Quaternion_op_Multiply_m487 ();
extern "C" void Quaternion_op_Inequality_m984 ();
extern "C" void Rect__ctor_m337 ();
extern "C" void Rect_Set_m571 ();
extern "C" void Rect_get_x_m452 ();
extern "C" void Rect_set_x_m453 ();
extern "C" void Rect_get_y_m456 ();
extern "C" void Rect_set_y_m457 ();
extern "C" void Rect_get_position_m380 ();
extern "C" void Rect_get_center_m478 ();
extern "C" void Rect_set_center_m479 ();
extern "C" void Rect_get_min_m985 ();
extern "C" void Rect_get_max_m986 ();
extern "C" void Rect_get_width_m370 ();
extern "C" void Rect_set_width_m454 ();
extern "C" void Rect_get_height_m372 ();
extern "C" void Rect_set_height_m455 ();
extern "C" void Rect_get_size_m382 ();
extern "C" void Rect_get_xMin_m369 ();
extern "C" void Rect_get_yMin_m371 ();
extern "C" void Rect_get_xMax_m583 ();
extern "C" void Rect_get_yMax_m987 ();
extern "C" void Rect_ToString_m988 ();
extern "C" void Rect_Contains_m989 ();
extern "C" void Rect_Overlaps_m990 ();
extern "C" void Rect_GetHashCode_m991 ();
extern "C" void Rect_Equals_m992 ();
extern "C" void Rect_op_Inequality_m993 ();
extern "C" void Rect_op_Equality_m994 ();
extern "C" void Matrix4x4_get_Item_m433 ();
extern "C" void Matrix4x4_set_Item_m434 ();
extern "C" void Matrix4x4_get_Item_m995 ();
extern "C" void Matrix4x4_set_Item_m996 ();
extern "C" void Matrix4x4_GetHashCode_m997 ();
extern "C" void Matrix4x4_Equals_m998 ();
extern "C" void Matrix4x4_Inverse_m999 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m1000 ();
extern "C" void Matrix4x4_Transpose_m1001 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m1002 ();
extern "C" void Matrix4x4_Invert_m1003 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m1004 ();
extern "C" void Matrix4x4_get_inverse_m568 ();
extern "C" void Matrix4x4_get_transpose_m1005 ();
extern "C" void Matrix4x4_get_isIdentity_m1006 ();
extern "C" void Matrix4x4_GetColumn_m545 ();
extern "C" void Matrix4x4_GetRow_m1007 ();
extern "C" void Matrix4x4_SetColumn_m1008 ();
extern "C" void Matrix4x4_SetRow_m1009 ();
extern "C" void Matrix4x4_MultiplyPoint_m1010 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m1011 ();
extern "C" void Matrix4x4_MultiplyVector_m1012 ();
extern "C" void Matrix4x4_Scale_m543 ();
extern "C" void Matrix4x4_get_zero_m584 ();
extern "C" void Matrix4x4_get_identity_m542 ();
extern "C" void Matrix4x4_SetTRS_m1013 ();
extern "C" void Matrix4x4_TRS_m495 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m1014 ();
extern "C" void Matrix4x4_ToString_m1015 ();
extern "C" void Matrix4x4_ToString_m1016 ();
extern "C" void Matrix4x4_Ortho_m1017 ();
extern "C" void Matrix4x4_Perspective_m1018 ();
extern "C" void Matrix4x4_op_Multiply_m544 ();
extern "C" void Matrix4x4_op_Multiply_m1019 ();
extern "C" void Matrix4x4_op_Equality_m1020 ();
extern "C" void Matrix4x4_op_Inequality_m1021 ();
extern "C" void Bounds__ctor_m1022 ();
extern "C" void Bounds_GetHashCode_m1023 ();
extern "C" void Bounds_Equals_m1024 ();
extern "C" void Bounds_get_center_m1025 ();
extern "C" void Bounds_set_center_m1026 ();
extern "C" void Bounds_get_size_m1027 ();
extern "C" void Bounds_set_size_m1028 ();
extern "C" void Bounds_get_extents_m1029 ();
extern "C" void Bounds_set_extents_m1030 ();
extern "C" void Bounds_get_min_m1031 ();
extern "C" void Bounds_set_min_m1032 ();
extern "C" void Bounds_get_max_m1033 ();
extern "C" void Bounds_set_max_m1034 ();
extern "C" void Bounds_SetMinMax_m1035 ();
extern "C" void Bounds_Encapsulate_m1036 ();
extern "C" void Bounds_Encapsulate_m1037 ();
extern "C" void Bounds_Expand_m1038 ();
extern "C" void Bounds_Expand_m1039 ();
extern "C" void Bounds_Intersects_m1040 ();
extern "C" void Bounds_Internal_Contains_m1041 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m1042 ();
extern "C" void Bounds_Contains_m1043 ();
extern "C" void Bounds_Internal_SqrDistance_m1044 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m1045 ();
extern "C" void Bounds_SqrDistance_m1046 ();
extern "C" void Bounds_Internal_IntersectRay_m1047 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m1048 ();
extern "C" void Bounds_IntersectRay_m1049 ();
extern "C" void Bounds_IntersectRay_m1050 ();
extern "C" void Bounds_Internal_GetClosestPoint_m1051 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m1052 ();
extern "C" void Bounds_ClosestPoint_m1053 ();
extern "C" void Bounds_ToString_m1054 ();
extern "C" void Bounds_ToString_m1055 ();
extern "C" void Bounds_op_Equality_m1056 ();
extern "C" void Bounds_op_Inequality_m1057 ();
extern "C" void Vector4__ctor_m448 ();
extern "C" void Vector4__ctor_m451 ();
extern "C" void Vector4_get_Item_m1058 ();
extern "C" void Vector4_set_Item_m1059 ();
extern "C" void Vector4_GetHashCode_m1060 ();
extern "C" void Vector4_Equals_m1061 ();
extern "C" void Vector4_ToString_m1062 ();
extern "C" void Vector4_Dot_m1063 ();
extern "C" void Vector4_SqrMagnitude_m1064 ();
extern "C" void Vector4_get_sqrMagnitude_m1065 ();
extern "C" void Vector4_get_zero_m1066 ();
extern "C" void Vector4_op_Subtraction_m1067 ();
extern "C" void Vector4_op_Division_m449 ();
extern "C" void Vector4_op_Equality_m1068 ();
extern "C" void Vector4_op_Implicit_m546 ();
extern "C" void Ray__ctor_m483 ();
extern "C" void Ray_get_origin_m1069 ();
extern "C" void Ray_get_direction_m1070 ();
extern "C" void Ray_GetPoint_m1071 ();
extern "C" void Ray_ToString_m1072 ();
extern "C" void Plane__ctor_m1073 ();
extern "C" void Plane_get_normal_m1074 ();
extern "C" void Plane_get_distance_m1075 ();
extern "C" void Plane_Raycast_m1076 ();
extern "C" void MathfInternal__cctor_m1077 ();
extern "C" void Mathf__cctor_m1078 ();
extern "C" void Mathf_Sin_m1079 ();
extern "C" void Mathf_Cos_m1080 ();
extern "C" void Mathf_Atan_m1081 ();
extern "C" void Mathf_Sqrt_m1082 ();
extern "C" void Mathf_Abs_m1083 ();
extern "C" void Mathf_Min_m676 ();
extern "C" void Mathf_Min_m565 ();
extern "C" void Mathf_Max_m1084 ();
extern "C" void Mathf_Max_m564 ();
extern "C" void Mathf_Pow_m1085 ();
extern "C" void Mathf_Log_m1086 ();
extern "C" void Mathf_Floor_m1087 ();
extern "C" void Mathf_Round_m1088 ();
extern "C" void Mathf_CeilToInt_m1089 ();
extern "C" void Mathf_FloorToInt_m1090 ();
extern "C" void Mathf_RoundToInt_m1091 ();
extern "C" void Mathf_Sign_m1092 ();
extern "C" void Mathf_Clamp_m330 ();
extern "C" void Mathf_Clamp_m1093 ();
extern "C" void Mathf_Clamp01_m398 ();
extern "C" void Mathf_Lerp_m439 ();
extern "C" void Mathf_SmoothStep_m480 ();
extern "C" void Mathf_Approximately_m399 ();
extern "C" void Mathf_SmoothDamp_m1094 ();
extern "C" void Mathf_Repeat_m1095 ();
extern "C" void Mathf_InverseLerp_m1096 ();
extern "C" void DrivenRectTransformTracker_Add_m1097 ();
extern "C" void DrivenRectTransformTracker_Clear_m1098 ();
extern "C" void ReapplyDrivenProperties__ctor_m1099 ();
extern "C" void ReapplyDrivenProperties_Invoke_m1100 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m1101 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m1102 ();
extern "C" void RectTransform_add_reapplyDrivenProperties_m1103 ();
extern "C" void RectTransform_remove_reapplyDrivenProperties_m1104 ();
extern "C" void RectTransform_get_rect_m1105 ();
extern "C" void RectTransform_INTERNAL_get_rect_m1106 ();
extern "C" void RectTransform_get_anchorMin_m1107 ();
extern "C" void RectTransform_set_anchorMin_m1108 ();
extern "C" void RectTransform_INTERNAL_get_anchorMin_m1109 ();
extern "C" void RectTransform_INTERNAL_set_anchorMin_m1110 ();
extern "C" void RectTransform_get_anchorMax_m1111 ();
extern "C" void RectTransform_set_anchorMax_m1112 ();
extern "C" void RectTransform_INTERNAL_get_anchorMax_m1113 ();
extern "C" void RectTransform_INTERNAL_set_anchorMax_m1114 ();
extern "C" void RectTransform_get_anchoredPosition_m1115 ();
extern "C" void RectTransform_set_anchoredPosition_m1116 ();
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m1117 ();
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m1118 ();
extern "C" void RectTransform_get_sizeDelta_m1119 ();
extern "C" void RectTransform_set_sizeDelta_m1120 ();
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m1121 ();
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m1122 ();
extern "C" void RectTransform_get_pivot_m1123 ();
extern "C" void RectTransform_set_pivot_m1124 ();
extern "C" void RectTransform_INTERNAL_get_pivot_m1125 ();
extern "C" void RectTransform_INTERNAL_set_pivot_m1126 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m1127 ();
extern "C" void RectTransform_GetLocalCorners_m1128 ();
extern "C" void RectTransform_GetWorldCorners_m1129 ();
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m1130 ();
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m1131 ();
extern "C" void RectTransform_GetParentSize_m1132 ();
extern "C" void ResourceRequest__ctor_m1133 ();
extern "C" void ResourceRequest_get_asset_m1134 ();
extern "C" void Resources_Load_m1135 ();
extern "C" void SerializePrivateVariables__ctor_m1136 ();
extern "C" void SerializeField__ctor_m1137 ();
extern "C" void Shader_Find_m377 ();
extern "C" void Shader_SetGlobalColor_m1138 ();
extern "C" void Shader_SetGlobalColor_m1139 ();
extern "C" void Shader_INTERNAL_CALL_SetGlobalColor_m1140 ();
extern "C" void Shader_SetGlobalVector_m450 ();
extern "C" void Shader_PropertyToID_m1141 ();
extern "C" void Material__ctor_m378 ();
extern "C" void Material__ctor_m1142 ();
extern "C" void Material_set_color_m327 ();
extern "C" void Material_get_mainTexture_m1143 ();
extern "C" void Material_set_mainTexture_m379 ();
extern "C" void Material_set_mainTextureOffset_m381 ();
extern "C" void Material_set_mainTextureScale_m383 ();
extern "C" void Material_SetColor_m1144 ();
extern "C" void Material_SetColor_m1145 ();
extern "C" void Material_INTERNAL_CALL_SetColor_m1146 ();
extern "C" void Material_SetTexture_m1147 ();
extern "C" void Material_SetTexture_m1148 ();
extern "C" void Material_GetTexture_m1149 ();
extern "C" void Material_GetTexture_m1150 ();
extern "C" void Material_SetTextureOffset_m1151 ();
extern "C" void Material_INTERNAL_CALL_SetTextureOffset_m1152 ();
extern "C" void Material_SetTextureScale_m1153 ();
extern "C" void Material_INTERNAL_CALL_SetTextureScale_m1154 ();
extern "C" void Material_SetFloat_m1155 ();
extern "C" void Material_SetFloat_m1156 ();
extern "C" void Material_SetInt_m1157 ();
extern "C" void Material_HasProperty_m1158 ();
extern "C" void Material_HasProperty_m1159 ();
extern "C" void Material_SetPass_m496 ();
extern "C" void Material_Internal_CreateWithShader_m1160 ();
extern "C" void Material_Internal_CreateWithMaterial_m1161 ();
extern "C" void SortingLayer_GetLayerValueFromID_m1162 ();
extern "C" void SphericalHarmonicsL2_Clear_m1163 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m1164 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m1165 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m1166 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m1167 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m1168 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m1169 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m1170 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m1171 ();
extern "C" void SphericalHarmonicsL2_get_Item_m1172 ();
extern "C" void SphericalHarmonicsL2_set_Item_m1173 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m1174 ();
extern "C" void SphericalHarmonicsL2_Equals_m1175 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m1176 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m1177 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m1178 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m1179 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m1180 ();
extern "C" void Sprite_get_rect_m1181 ();
extern "C" void Sprite_INTERNAL_get_rect_m1182 ();
extern "C" void Sprite_get_pixelsPerUnit_m1183 ();
extern "C" void Sprite_get_texture_m1184 ();
extern "C" void Sprite_get_textureRect_m1185 ();
extern "C" void Sprite_INTERNAL_get_textureRect_m1186 ();
extern "C" void Sprite_get_border_m1187 ();
extern "C" void Sprite_INTERNAL_get_border_m1188 ();
extern "C" void DataUtility_GetInnerUV_m1189 ();
extern "C" void DataUtility_GetOuterUV_m1190 ();
extern "C" void DataUtility_GetPadding_m1191 ();
extern "C" void DataUtility_GetMinSize_m1192 ();
extern "C" void DataUtility_Internal_GetMinSize_m1193 ();
extern "C" void UnityString_Format_m1194 ();
extern "C" void AsyncOperation__ctor_m1195 ();
extern "C" void AsyncOperation_InternalDestroy_m1196 ();
extern "C" void AsyncOperation_Finalize_m1197 ();
extern "C" void LogCallback__ctor_m1198 ();
extern "C" void LogCallback_Invoke_m1199 ();
extern "C" void LogCallback_BeginInvoke_m1200 ();
extern "C" void LogCallback_EndInvoke_m1201 ();
extern "C" void Application_get_loadedLevel_m662 ();
extern "C" void Application_LoadLevel_m696 ();
extern "C" void Application_LoadLevelAsync_m663 ();
extern "C" void Application_LoadLevelAsync_m1202 ();
extern "C" void Application_get_isPlaying_m426 ();
extern "C" void Application_get_isEditor_m425 ();
extern "C" void Application_get_platform_m591 ();
extern "C" void Application_get_isMobilePlatform_m577 ();
extern "C" void Application_get_unityVersion_m579 ();
extern "C" void Application_set_targetFrameRate_m407 ();
extern "C" void Application_CallLogCallback_m1203 ();
extern "C" void Behaviour__ctor_m1204 ();
extern "C" void Behaviour_get_enabled_m343 ();
extern "C" void Behaviour_set_enabled_m348 ();
extern "C" void Behaviour_get_isActiveAndEnabled_m1205 ();
extern "C" void CameraCallback__ctor_m1206 ();
extern "C" void CameraCallback_Invoke_m1207 ();
extern "C" void CameraCallback_BeginInvoke_m1208 ();
extern "C" void CameraCallback_EndInvoke_m1209 ();
extern "C" void Camera_set_fieldOfView_m447 ();
extern "C" void Camera_get_nearClipPlane_m444 ();
extern "C" void Camera_get_farClipPlane_m445 ();
extern "C" void Camera_get_depth_m1210 ();
extern "C" void Camera_set_depth_m420 ();
extern "C" void Camera_get_cullingMask_m472 ();
extern "C" void Camera_set_cullingMask_m418 ();
extern "C" void Camera_get_eventMask_m1211 ();
extern "C" void Camera_set_backgroundColor_m417 ();
extern "C" void Camera_INTERNAL_set_backgroundColor_m1212 ();
extern "C" void Camera_get_rect_m432 ();
extern "C" void Camera_set_rect_m458 ();
extern "C" void Camera_INTERNAL_get_rect_m1213 ();
extern "C" void Camera_INTERNAL_set_rect_m1214 ();
extern "C" void Camera_get_pixelRect_m461 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m1215 ();
extern "C" void Camera_get_targetTexture_m467 ();
extern "C" void Camera_set_targetTexture_m459 ();
extern "C" void Camera_get_projectionMatrix_m438 ();
extern "C" void Camera_set_projectionMatrix_m446 ();
extern "C" void Camera_INTERNAL_get_projectionMatrix_m1216 ();
extern "C" void Camera_INTERNAL_set_projectionMatrix_m1217 ();
extern "C" void Camera_get_clearFlags_m1218 ();
extern "C" void Camera_set_clearFlags_m415 ();
extern "C" void Camera_ScreenToViewportPoint_m1219 ();
extern "C" void Camera_INTERNAL_CALL_ScreenToViewportPoint_m1220 ();
extern "C" void Camera_ScreenPointToRay_m1221 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m1222 ();
extern "C" void Camera_get_main_m333 ();
extern "C" void Camera_get_allCamerasCount_m1223 ();
extern "C" void Camera_GetAllCameras_m1224 ();
extern "C" void Camera_FireOnPreCull_m1225 ();
extern "C" void Camera_FireOnPreRender_m1226 ();
extern "C" void Camera_FireOnPostRender_m1227 ();
extern "C" void Camera_Render_m460 ();
extern "C" void Camera_set_useOcclusionCulling_m419 ();
extern "C" void Camera_CopyFrom_m471 ();
extern "C" void Camera_RaycastTry_m1228 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m1229 ();
extern "C" void Camera_RaycastTry2D_m1230 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m1231 ();
extern "C" void Debug_DrawLine_m673 ();
extern "C" void Debug_INTERNAL_CALL_DrawLine_m1232 ();
extern "C" void Debug_Internal_Log_m1233 ();
extern "C" void Debug_Internal_LogException_m1234 ();
extern "C" void Debug_Log_m388 ();
extern "C" void Debug_LogError_m400 ();
extern "C" void Debug_LogError_m1235 ();
extern "C" void Debug_LogException_m1236 ();
extern "C" void Debug_LogException_m1237 ();
extern "C" void Debug_LogWarning_m347 ();
extern "C" void Debug_LogWarning_m1238 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m1239 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m1240 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m1241 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m1242 ();
extern "C" void Display__ctor_m1243 ();
extern "C" void Display__ctor_m1244 ();
extern "C" void Display__cctor_m1245 ();
extern "C" void Display_add_onDisplaysUpdated_m1246 ();
extern "C" void Display_remove_onDisplaysUpdated_m1247 ();
extern "C" void Display_get_renderingWidth_m1248 ();
extern "C" void Display_get_renderingHeight_m1249 ();
extern "C" void Display_get_systemWidth_m1250 ();
extern "C" void Display_get_systemHeight_m1251 ();
extern "C" void Display_get_colorBuffer_m1252 ();
extern "C" void Display_get_depthBuffer_m1253 ();
extern "C" void Display_Activate_m1254 ();
extern "C" void Display_Activate_m1255 ();
extern "C" void Display_SetParams_m1256 ();
extern "C" void Display_SetRenderingResolution_m1257 ();
extern "C" void Display_MultiDisplayLicense_m1258 ();
extern "C" void Display_RelativeMouseAt_m1259 ();
extern "C" void Display_get_main_m1260 ();
extern "C" void Display_RecreateDisplayList_m1261 ();
extern "C" void Display_FireDisplaysUpdated_m1262 ();
extern "C" void Display_GetSystemExtImpl_m1263 ();
extern "C" void Display_GetRenderingExtImpl_m1264 ();
extern "C" void Display_GetRenderingBuffersImpl_m1265 ();
extern "C" void Display_SetRenderingResolutionImpl_m1266 ();
extern "C" void Display_ActivateDisplayImpl_m1267 ();
extern "C" void Display_SetParamsImpl_m1268 ();
extern "C" void Display_MultiDisplayLicenseImpl_m1269 ();
extern "C" void Display_RelativeMouseAtImpl_m1270 ();
extern "C" void MonoBehaviour__ctor_m320 ();
extern "C" void MonoBehaviour_StartCoroutine_m1271 ();
extern "C" void MonoBehaviour_StartCoroutine_Auto_m672 ();
extern "C" void MonoBehaviour_StartCoroutine_m1272 ();
extern "C" void MonoBehaviour_StartCoroutine_m421 ();
extern "C" void MonoBehaviour_StopCoroutine_m422 ();
extern "C" void MonoBehaviour_StopCoroutine_m1273 ();
extern "C" void MonoBehaviour_StopCoroutine_m1274 ();
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1275 ();
extern "C" void MonoBehaviour_StopCoroutine_Auto_m1276 ();
extern "C" void MonoBehaviour_print_m683 ();
extern "C" void Touch_get_fingerId_m1277 ();
extern "C" void Touch_get_position_m1278 ();
extern "C" void Touch_get_phase_m1279 ();
extern "C" void Input__cctor_m1280 ();
extern "C" void Input_GetKeyDownInt_m1281 ();
extern "C" void Input_GetAxis_m588 ();
extern "C" void Input_GetAxisRaw_m1282 ();
extern "C" void Input_GetButton_m677 ();
extern "C" void Input_GetButtonDown_m1283 ();
extern "C" void Input_GetKeyDown_m410 ();
extern "C" void Input_GetMouseButton_m525 ();
extern "C" void Input_GetMouseButtonDown_m409 ();
extern "C" void Input_GetMouseButtonUp_m1284 ();
extern "C" void Input_get_mousePosition_m1285 ();
extern "C" void Input_INTERNAL_get_mousePosition_m1286 ();
extern "C" void Input_get_mouseScrollDelta_m1287 ();
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m1288 ();
extern "C" void Input_get_mousePresent_m1289 ();
extern "C" void Input_GetTouch_m1290 ();
extern "C" void Input_get_touchCount_m1291 ();
extern "C" void Input_get_touchSupported_m1292 ();
extern "C" void Input_set_imeCompositionMode_m1293 ();
extern "C" void Input_get_compositionString_m1294 ();
extern "C" void Input_set_compositionCursorPos_m1295 ();
extern "C" void Input_INTERNAL_set_compositionCursorPos_m1296 ();
extern "C" void Object__ctor_m1297 ();
extern "C" void Object_Internal_CloneSingle_m1298 ();
extern "C" void Object_Destroy_m1299 ();
extern "C" void Object_Destroy_m389 ();
extern "C" void Object_DestroyImmediate_m1300 ();
extern "C" void Object_DestroyImmediate_m1301 ();
extern "C" void Object_FindObjectsOfType_m1302 ();
extern "C" void Object_get_name_m558 ();
extern "C" void Object_set_name_m1303 ();
extern "C" void Object_set_hideFlags_m1304 ();
extern "C" void Object_ToString_m1305 ();
extern "C" void Object_Equals_m1306 ();
extern "C" void Object_GetHashCode_m1307 ();
extern "C" void Object_CompareBaseObjects_m1308 ();
extern "C" void Object_IsNativeObjectAlive_m1309 ();
extern "C" void Object_GetInstanceID_m1310 ();
extern "C" void Object_GetCachedPtr_m1311 ();
extern "C" void Object_CheckNullArgument_m1312 ();
extern "C" void Object_FindObjectOfType_m1313 ();
extern "C" void Object_op_Implicit_m351 ();
extern "C" void Object_op_Equality_m342 ();
extern "C" void Object_op_Inequality_m349 ();
extern "C" void Component__ctor_m1314 ();
extern "C" void Component_get_transform_m321 ();
extern "C" void Component_get_gameObject_m344 ();
extern "C" void Component_GetComponent_m666 ();
extern "C" void Component_GetComponentFastPath_m1315 ();
extern "C" void Component_GetComponentInChildren_m1316 ();
extern "C" void Component_GetComponentInParent_m1317 ();
extern "C" void Component_GetComponentsForListInternal_m1318 ();
extern "C" void Component_GetComponents_m1319 ();
extern "C" void GameObject__ctor_m394 ();
extern "C" void GameObject__ctor_m650 ();
extern "C" void GameObject_GetComponent_m1320 ();
extern "C" void GameObject_GetComponentFastPath_m1321 ();
extern "C" void GameObject_GetComponentInChildren_m1322 ();
extern "C" void GameObject_GetComponentInParent_m1323 ();
extern "C" void GameObject_GetComponentsInternal_m1324 ();
extern "C" void GameObject_get_transform_m396 ();
extern "C" void GameObject_get_layer_m1325 ();
extern "C" void GameObject_set_layer_m1326 ();
extern "C" void GameObject_SetActive_m507 ();
extern "C" void GameObject_get_activeSelf_m1327 ();
extern "C" void GameObject_get_activeInHierarchy_m345 ();
extern "C" void GameObject_SendMessage_m1328 ();
extern "C" void GameObject_Internal_AddComponentWithType_m1329 ();
extern "C" void GameObject_AddComponent_m652 ();
extern "C" void GameObject_Internal_CreateGameObject_m1330 ();
extern "C" void Enumerator__ctor_m1331 ();
extern "C" void Enumerator_get_Current_m1332 ();
extern "C" void Enumerator_MoveNext_m1333 ();
extern "C" void Enumerator_Reset_m1334 ();
extern "C" void Transform_get_position_m481 ();
extern "C" void Transform_set_position_m488 ();
extern "C" void Transform_INTERNAL_get_position_m1335 ();
extern "C" void Transform_INTERNAL_set_position_m1336 ();
extern "C" void Transform_get_localPosition_m322 ();
extern "C" void Transform_set_localPosition_m328 ();
extern "C" void Transform_INTERNAL_get_localPosition_m1337 ();
extern "C" void Transform_INTERNAL_set_localPosition_m1338 ();
extern "C" void Transform_get_localEulerAngles_m587 ();
extern "C" void Transform_set_localEulerAngles_m589 ();
extern "C" void Transform_INTERNAL_get_localEulerAngles_m1339 ();
extern "C" void Transform_INTERNAL_set_localEulerAngles_m1340 ();
extern "C" void Transform_get_forward_m482 ();
extern "C" void Transform_get_rotation_m484 ();
extern "C" void Transform_set_rotation_m486 ();
extern "C" void Transform_INTERNAL_get_rotation_m1341 ();
extern "C" void Transform_INTERNAL_set_rotation_m1342 ();
extern "C" void Transform_get_localRotation_m1343 ();
extern "C" void Transform_set_localRotation_m476 ();
extern "C" void Transform_INTERNAL_get_localRotation_m1344 ();
extern "C" void Transform_INTERNAL_set_localRotation_m1345 ();
extern "C" void Transform_get_localScale_m1346 ();
extern "C" void Transform_set_localScale_m387 ();
extern "C" void Transform_INTERNAL_get_localScale_m1347 ();
extern "C" void Transform_INTERNAL_set_localScale_m1348 ();
extern "C" void Transform_get_parent_m424 ();
extern "C" void Transform_set_parent_m413 ();
extern "C" void Transform_get_parentInternal_m1349 ();
extern "C" void Transform_set_parentInternal_m1350 ();
extern "C" void Transform_SetParent_m1351 ();
extern "C" void Transform_SetParent_m1352 ();
extern "C" void Transform_get_worldToLocalMatrix_m1353 ();
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m1354 ();
extern "C" void Transform_Rotate_m1355 ();
extern "C" void Transform_Rotate_m590 ();
extern "C" void Transform_Rotate_m1356 ();
extern "C" void Transform_TransformPoint_m1357 ();
extern "C" void Transform_INTERNAL_CALL_TransformPoint_m1358 ();
extern "C" void Transform_InverseTransformPoint_m1359 ();
extern "C" void Transform_INTERNAL_CALL_InverseTransformPoint_m1360 ();
extern "C" void Transform_get_childCount_m1361 ();
extern "C" void Transform_SetAsFirstSibling_m1362 ();
extern "C" void Transform_get_lossyScale_m440 ();
extern "C" void Transform_INTERNAL_get_lossyScale_m1363 ();
extern "C" void Transform_IsChildOf_m1364 ();
extern "C" void Transform_GetEnumerator_m1365 ();
extern "C" void Transform_GetChild_m1366 ();
extern "C" void Time_get_timeSinceLevelLoad_m595 ();
extern "C" void Time_get_deltaTime_m586 ();
extern "C" void Time_get_unscaledTime_m523 ();
extern "C" void Time_get_unscaledDeltaTime_m1367 ();
extern "C" void Time_get_smoothDeltaTime_m567 ();
extern "C" void Time_get_timeScale_m689 ();
extern "C" void Random_get_value_m331 ();
extern "C" void Random_get_onUnitSphere_m329 ();
extern "C" void Random_INTERNAL_get_onUnitSphere_m1368 ();
extern "C" void YieldInstruction__ctor_m1369 ();
extern "C" void PlayerPrefsException__ctor_m1370 ();
extern "C" void PlayerPrefs_TrySetInt_m1371 ();
extern "C" void PlayerPrefs_TrySetFloat_m1372 ();
extern "C" void PlayerPrefs_SetInt_m598 ();
extern "C" void PlayerPrefs_GetInt_m1373 ();
extern "C" void PlayerPrefs_GetInt_m600 ();
extern "C" void PlayerPrefs_SetFloat_m599 ();
extern "C" void PlayerPrefs_GetFloat_m1374 ();
extern "C" void PlayerPrefs_GetFloat_m601 ();
extern "C" void UnityAdsInternal__ctor_m1375 ();
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m1376 ();
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m1377 ();
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m1378 ();
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m1379 ();
extern "C" void UnityAdsInternal_add_onShow_m1380 ();
extern "C" void UnityAdsInternal_remove_onShow_m1381 ();
extern "C" void UnityAdsInternal_add_onHide_m1382 ();
extern "C" void UnityAdsInternal_remove_onHide_m1383 ();
extern "C" void UnityAdsInternal_add_onVideoCompleted_m1384 ();
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m1385 ();
extern "C" void UnityAdsInternal_add_onVideoStarted_m1386 ();
extern "C" void UnityAdsInternal_remove_onVideoStarted_m1387 ();
extern "C" void UnityAdsInternal_RegisterNative_m1388 ();
extern "C" void UnityAdsInternal_Init_m1389 ();
extern "C" void UnityAdsInternal_Show_m1390 ();
extern "C" void UnityAdsInternal_CanShowAds_m1391 ();
extern "C" void UnityAdsInternal_SetLogLevel_m1392 ();
extern "C" void UnityAdsInternal_SetCampaignDataURL_m1393 ();
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m1394 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m1395 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m1396 ();
extern "C" void UnityAdsInternal_CallUnityAdsShow_m1397 ();
extern "C" void UnityAdsInternal_CallUnityAdsHide_m1398 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m1399 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m1400 ();
extern "C" void Particle_get_position_m1401 ();
extern "C" void Particle_set_position_m1402 ();
extern "C" void Particle_get_velocity_m1403 ();
extern "C" void Particle_set_velocity_m1404 ();
extern "C" void Particle_get_energy_m1405 ();
extern "C" void Particle_set_energy_m1406 ();
extern "C" void Particle_get_startEnergy_m1407 ();
extern "C" void Particle_set_startEnergy_m1408 ();
extern "C" void Particle_get_size_m1409 ();
extern "C" void Particle_set_size_m1410 ();
extern "C" void Particle_get_rotation_m1411 ();
extern "C" void Particle_set_rotation_m1412 ();
extern "C" void Particle_get_angularVelocity_m1413 ();
extern "C" void Particle_set_angularVelocity_m1414 ();
extern "C" void Particle_get_color_m1415 ();
extern "C" void Particle_set_color_m1416 ();
extern "C" void ControllerColliderHit_get_gameObject_m671 ();
extern "C" void ControllerColliderHit_get_normal_m669 ();
extern "C" void ControllerColliderHit_get_moveDirection_m670 ();
extern "C" void Physics_Raycast_m1417 ();
extern "C" void Physics_Raycast_m1418 ();
extern "C" void Physics_Raycast_m1419 ();
extern "C" void Physics_RaycastAll_m1420 ();
extern "C" void Physics_RaycastAll_m1421 ();
extern "C" void Physics_RaycastAll_m1422 ();
extern "C" void Physics_INTERNAL_CALL_RaycastAll_m1423 ();
extern "C" void Physics_Internal_Raycast_m1424 ();
extern "C" void Physics_INTERNAL_CALL_Internal_Raycast_m1425 ();
extern "C" void Rigidbody_set_freezeRotation_m593 ();
extern "C" void Collider_Internal_Raycast_m1426 ();
extern "C" void Collider_INTERNAL_CALL_Internal_Raycast_m1427 ();
extern "C" void Collider_Raycast_m336 ();
extern "C" void RaycastHit_CalculateRaycastTexCoord_m1428 ();
extern "C" void RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1429 ();
extern "C" void RaycastHit_get_point_m1430 ();
extern "C" void RaycastHit_get_normal_m1431 ();
extern "C" void RaycastHit_get_distance_m367 ();
extern "C" void RaycastHit_get_textureCoord_m368 ();
extern "C" void RaycastHit_get_collider_m1432 ();
extern "C" void CharacterController_Move_m682 ();
extern "C" void CharacterController_INTERNAL_CALL_Move_m1433 ();
extern "C" void CharacterController_get_slopeLimit_m667 ();
extern "C" void Physics2D__cctor_m1434 ();
extern "C" void Physics2D_Internal_Raycast_m1435 ();
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m1436 ();
extern "C" void Physics2D_Raycast_m1437 ();
extern "C" void Physics2D_Raycast_m1438 ();
extern "C" void Physics2D_RaycastAll_m1439 ();
extern "C" void Physics2D_INTERNAL_CALL_RaycastAll_m1440 ();
extern "C" void RaycastHit2D_get_point_m1441 ();
extern "C" void RaycastHit2D_get_normal_m1442 ();
extern "C" void RaycastHit2D_get_fraction_m1443 ();
extern "C" void RaycastHit2D_get_collider_m1444 ();
extern "C" void RaycastHit2D_get_rigidbody_m1445 ();
extern "C" void RaycastHit2D_get_transform_m1446 ();
extern "C" void Collider2D_get_attachedRigidbody_m1447 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m1448 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m1449 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m1450 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m1451 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m1452 ();
extern "C" void PCMReaderCallback__ctor_m1453 ();
extern "C" void PCMReaderCallback_Invoke_m1454 ();
extern "C" void PCMReaderCallback_BeginInvoke_m1455 ();
extern "C" void PCMReaderCallback_EndInvoke_m1456 ();
extern "C" void PCMSetPositionCallback__ctor_m1457 ();
extern "C" void PCMSetPositionCallback_Invoke_m1458 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m1459 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m1460 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m1461 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m1462 ();
extern "C" void WebCamDevice_get_name_m1463 ();
extern "C" void WebCamDevice_get_isFrontFacing_m1464 ();
extern "C" void AnimationEvent__ctor_m1465 ();
extern "C" void AnimationEvent_get_data_m1466 ();
extern "C" void AnimationEvent_set_data_m1467 ();
extern "C" void AnimationEvent_get_stringParameter_m1468 ();
extern "C" void AnimationEvent_set_stringParameter_m1469 ();
extern "C" void AnimationEvent_get_floatParameter_m1470 ();
extern "C" void AnimationEvent_set_floatParameter_m1471 ();
extern "C" void AnimationEvent_get_intParameter_m1472 ();
extern "C" void AnimationEvent_set_intParameter_m1473 ();
extern "C" void AnimationEvent_get_objectReferenceParameter_m1474 ();
extern "C" void AnimationEvent_set_objectReferenceParameter_m1475 ();
extern "C" void AnimationEvent_get_functionName_m1476 ();
extern "C" void AnimationEvent_set_functionName_m1477 ();
extern "C" void AnimationEvent_get_time_m1478 ();
extern "C" void AnimationEvent_set_time_m1479 ();
extern "C" void AnimationEvent_get_messageOptions_m1480 ();
extern "C" void AnimationEvent_set_messageOptions_m1481 ();
extern "C" void AnimationEvent_get_isFiredByLegacy_m1482 ();
extern "C" void AnimationEvent_get_isFiredByAnimator_m1483 ();
extern "C" void AnimationEvent_get_animationState_m1484 ();
extern "C" void AnimationEvent_get_animatorStateInfo_m1485 ();
extern "C" void AnimationEvent_get_animatorClipInfo_m1486 ();
extern "C" void AnimationEvent_GetHash_m1487 ();
extern "C" void AnimationCurve__ctor_m1488 ();
extern "C" void AnimationCurve__ctor_m1489 ();
extern "C" void AnimationCurve_Cleanup_m1490 ();
extern "C" void AnimationCurve_Finalize_m1491 ();
extern "C" void AnimationCurve_Init_m1492 ();
extern "C" void AnimatorStateInfo_IsName_m1493 ();
extern "C" void AnimatorStateInfo_get_fullPathHash_m1494 ();
extern "C" void AnimatorStateInfo_get_nameHash_m1495 ();
extern "C" void AnimatorStateInfo_get_shortNameHash_m1496 ();
extern "C" void AnimatorStateInfo_get_normalizedTime_m1497 ();
extern "C" void AnimatorStateInfo_get_length_m1498 ();
extern "C" void AnimatorStateInfo_get_speed_m1499 ();
extern "C" void AnimatorStateInfo_get_speedMultiplier_m1500 ();
extern "C" void AnimatorStateInfo_get_tagHash_m1501 ();
extern "C" void AnimatorStateInfo_IsTag_m1502 ();
extern "C" void AnimatorStateInfo_get_loop_m1503 ();
extern "C" void AnimatorTransitionInfo_IsName_m1504 ();
extern "C" void AnimatorTransitionInfo_IsUserName_m1505 ();
extern "C" void AnimatorTransitionInfo_get_fullPathHash_m1506 ();
extern "C" void AnimatorTransitionInfo_get_nameHash_m1507 ();
extern "C" void AnimatorTransitionInfo_get_userNameHash_m1508 ();
extern "C" void AnimatorTransitionInfo_get_normalizedTime_m1509 ();
extern "C" void AnimatorTransitionInfo_get_anyState_m1510 ();
extern "C" void AnimatorTransitionInfo_get_entry_m1511 ();
extern "C" void AnimatorTransitionInfo_get_exit_m1512 ();
extern "C" void Animator_SetTrigger_m1513 ();
extern "C" void Animator_ResetTrigger_m1514 ();
extern "C" void Animator_get_runtimeAnimatorController_m1515 ();
extern "C" void Animator_StringToHash_m1516 ();
extern "C" void Animator_SetTriggerString_m1517 ();
extern "C" void Animator_ResetTriggerString_m1518 ();
extern "C" void HumanBone_get_boneName_m1519 ();
extern "C" void HumanBone_set_boneName_m1520 ();
extern "C" void HumanBone_get_humanName_m1521 ();
extern "C" void HumanBone_set_humanName_m1522 ();
extern "C" void GUIText_set_text_m692 ();
extern "C" void CharacterInfo_get_advance_m1523 ();
extern "C" void CharacterInfo_set_advance_m1524 ();
extern "C" void CharacterInfo_get_glyphWidth_m1525 ();
extern "C" void CharacterInfo_set_glyphWidth_m1526 ();
extern "C" void CharacterInfo_get_glyphHeight_m1527 ();
extern "C" void CharacterInfo_set_glyphHeight_m1528 ();
extern "C" void CharacterInfo_get_bearing_m1529 ();
extern "C" void CharacterInfo_set_bearing_m1530 ();
extern "C" void CharacterInfo_get_minY_m1531 ();
extern "C" void CharacterInfo_set_minY_m1532 ();
extern "C" void CharacterInfo_get_maxY_m1533 ();
extern "C" void CharacterInfo_set_maxY_m1534 ();
extern "C" void CharacterInfo_get_minX_m1535 ();
extern "C" void CharacterInfo_set_minX_m1536 ();
extern "C" void CharacterInfo_get_maxX_m1537 ();
extern "C" void CharacterInfo_set_maxX_m1538 ();
extern "C" void CharacterInfo_get_uvBottomLeftUnFlipped_m1539 ();
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m1540 ();
extern "C" void CharacterInfo_get_uvBottomRightUnFlipped_m1541 ();
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m1542 ();
extern "C" void CharacterInfo_get_uvTopRightUnFlipped_m1543 ();
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m1544 ();
extern "C" void CharacterInfo_get_uvTopLeftUnFlipped_m1545 ();
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m1546 ();
extern "C" void CharacterInfo_get_uvBottomLeft_m1547 ();
extern "C" void CharacterInfo_set_uvBottomLeft_m1548 ();
extern "C" void CharacterInfo_get_uvBottomRight_m1549 ();
extern "C" void CharacterInfo_set_uvBottomRight_m1550 ();
extern "C" void CharacterInfo_get_uvTopRight_m1551 ();
extern "C" void CharacterInfo_set_uvTopRight_m1552 ();
extern "C" void CharacterInfo_get_uvTopLeft_m1553 ();
extern "C" void CharacterInfo_set_uvTopLeft_m1554 ();
extern "C" void FontTextureRebuildCallback__ctor_m1555 ();
extern "C" void FontTextureRebuildCallback_Invoke_m1556 ();
extern "C" void FontTextureRebuildCallback_BeginInvoke_m1557 ();
extern "C" void FontTextureRebuildCallback_EndInvoke_m1558 ();
extern "C" void Font__ctor_m1559 ();
extern "C" void Font__ctor_m1560 ();
extern "C" void Font__ctor_m1561 ();
extern "C" void Font_add_textureRebuilt_m1562 ();
extern "C" void Font_remove_textureRebuilt_m1563 ();
extern "C" void Font_add_m_FontTextureRebuildCallback_m1564 ();
extern "C" void Font_remove_m_FontTextureRebuildCallback_m1565 ();
extern "C" void Font_GetOSInstalledFontNames_m1566 ();
extern "C" void Font_Internal_CreateFont_m1567 ();
extern "C" void Font_Internal_CreateDynamicFont_m1568 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m1569 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m1570 ();
extern "C" void Font_get_material_m1571 ();
extern "C" void Font_set_material_m1572 ();
extern "C" void Font_HasCharacter_m1573 ();
extern "C" void Font_get_fontNames_m1574 ();
extern "C" void Font_set_fontNames_m1575 ();
extern "C" void Font_get_characterInfo_m1576 ();
extern "C" void Font_set_characterInfo_m1577 ();
extern "C" void Font_RequestCharactersInTexture_m1578 ();
extern "C" void Font_RequestCharactersInTexture_m1579 ();
extern "C" void Font_RequestCharactersInTexture_m1580 ();
extern "C" void Font_InvokeTextureRebuilt_Internal_m1581 ();
extern "C" void Font_get_textureRebuildCallback_m1582 ();
extern "C" void Font_set_textureRebuildCallback_m1583 ();
extern "C" void Font_GetMaxVertsForString_m1584 ();
extern "C" void Font_GetCharacterInfo_m1585 ();
extern "C" void Font_GetCharacterInfo_m1586 ();
extern "C" void Font_GetCharacterInfo_m1587 ();
extern "C" void Font_get_dynamic_m1588 ();
extern "C" void Font_get_ascent_m1589 ();
extern "C" void Font_get_lineHeight_m1590 ();
extern "C" void Font_get_fontSize_m1591 ();
extern "C" void TextGenerator__ctor_m1592 ();
extern "C" void TextGenerator__ctor_m1593 ();
extern "C" void TextGenerator_System_IDisposable_Dispose_m1594 ();
extern "C" void TextGenerator_Init_m1595 ();
extern "C" void TextGenerator_Dispose_cpp_m1596 ();
extern "C" void TextGenerator_Populate_Internal_m1597 ();
extern "C" void TextGenerator_Populate_Internal_cpp_m1598 ();
extern "C" void TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1599 ();
extern "C" void TextGenerator_get_rectExtents_m1600 ();
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m1601 ();
extern "C" void TextGenerator_get_vertexCount_m1602 ();
extern "C" void TextGenerator_GetVerticesInternal_m1603 ();
extern "C" void TextGenerator_GetVerticesArray_m1604 ();
extern "C" void TextGenerator_get_characterCount_m1605 ();
extern "C" void TextGenerator_get_characterCountVisible_m1606 ();
extern "C" void TextGenerator_GetCharactersInternal_m1607 ();
extern "C" void TextGenerator_GetCharactersArray_m1608 ();
extern "C" void TextGenerator_get_lineCount_m1609 ();
extern "C" void TextGenerator_GetLinesInternal_m1610 ();
extern "C" void TextGenerator_GetLinesArray_m1611 ();
extern "C" void TextGenerator_get_fontSizeUsedForBestFit_m1612 ();
extern "C" void TextGenerator_Finalize_m1613 ();
extern "C" void TextGenerator_ValidatedSettings_m1614 ();
extern "C" void TextGenerator_Invalidate_m1615 ();
extern "C" void TextGenerator_GetCharacters_m1616 ();
extern "C" void TextGenerator_GetLines_m1617 ();
extern "C" void TextGenerator_GetVertices_m1618 ();
extern "C" void TextGenerator_GetPreferredWidth_m1619 ();
extern "C" void TextGenerator_GetPreferredHeight_m1620 ();
extern "C" void TextGenerator_Populate_m1621 ();
extern "C" void TextGenerator_PopulateAlways_m1622 ();
extern "C" void TextGenerator_get_verts_m1623 ();
extern "C" void TextGenerator_get_characters_m1624 ();
extern "C" void TextGenerator_get_lines_m1625 ();
extern "C" void WillRenderCanvases__ctor_m1626 ();
extern "C" void WillRenderCanvases_Invoke_m1627 ();
extern "C" void WillRenderCanvases_BeginInvoke_m1628 ();
extern "C" void WillRenderCanvases_EndInvoke_m1629 ();
extern "C" void Canvas_add_willRenderCanvases_m1630 ();
extern "C" void Canvas_remove_willRenderCanvases_m1631 ();
extern "C" void Canvas_get_renderMode_m1632 ();
extern "C" void Canvas_get_isRootCanvas_m1633 ();
extern "C" void Canvas_get_worldCamera_m1634 ();
extern "C" void Canvas_get_scaleFactor_m1635 ();
extern "C" void Canvas_set_scaleFactor_m1636 ();
extern "C" void Canvas_get_referencePixelsPerUnit_m1637 ();
extern "C" void Canvas_set_referencePixelsPerUnit_m1638 ();
extern "C" void Canvas_get_pixelPerfect_m1639 ();
extern "C" void Canvas_get_renderOrder_m1640 ();
extern "C" void Canvas_get_overrideSorting_m1641 ();
extern "C" void Canvas_set_overrideSorting_m1642 ();
extern "C" void Canvas_get_sortingOrder_m1643 ();
extern "C" void Canvas_set_sortingOrder_m1644 ();
extern "C" void Canvas_get_sortingLayerID_m1645 ();
extern "C" void Canvas_set_sortingLayerID_m1646 ();
extern "C" void Canvas_get_cachedSortingLayerValue_m1647 ();
extern "C" void Canvas_GetDefaultCanvasMaterial_m1648 ();
extern "C" void Canvas_SendWillRenderCanvases_m1649 ();
extern "C" void Canvas_ForceUpdateCanvases_m1650 ();
extern "C" void CanvasGroup_get_alpha_m1651 ();
extern "C" void CanvasGroup_set_alpha_m1652 ();
extern "C" void CanvasGroup_get_interactable_m1653 ();
extern "C" void CanvasGroup_get_blocksRaycasts_m1654 ();
extern "C" void CanvasGroup_get_ignoreParentGroups_m1655 ();
extern "C" void CanvasGroup_IsRaycastLocationValid_m1656 ();
extern "C" void UIVertex__cctor_m1657 ();
extern "C" void CanvasRenderer_SetColor_m1658 ();
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m1659 ();
extern "C" void CanvasRenderer_GetColor_m1660 ();
extern "C" void CanvasRenderer_EnableRectClipping_m1661 ();
extern "C" void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1662 ();
extern "C" void CanvasRenderer_DisableRectClipping_m1663 ();
extern "C" void CanvasRenderer_set_hasPopInstruction_m1664 ();
extern "C" void CanvasRenderer_get_materialCount_m1665 ();
extern "C" void CanvasRenderer_set_materialCount_m1666 ();
extern "C" void CanvasRenderer_SetMaterial_m1667 ();
extern "C" void CanvasRenderer_SetMaterial_m1668 ();
extern "C" void CanvasRenderer_set_popMaterialCount_m1669 ();
extern "C" void CanvasRenderer_SetPopMaterial_m1670 ();
extern "C" void CanvasRenderer_SetTexture_m1671 ();
extern "C" void CanvasRenderer_SetMesh_m1672 ();
extern "C" void CanvasRenderer_Clear_m1673 ();
extern "C" void CanvasRenderer_get_cull_m1674 ();
extern "C" void CanvasRenderer_set_cull_m1675 ();
extern "C" void CanvasRenderer_get_absoluteDepth_m1676 ();
extern "C" void CanvasRenderer_get_hasMoved_m1677 ();
extern "C" void RectTransformUtility__cctor_m1678 ();
extern "C" void RectTransformUtility_RectangleContainsScreenPoint_m1679 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1680 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m1681 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m1682 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1683 ();
extern "C" void RectTransformUtility_PixelAdjustRect_m1684 ();
extern "C" void RectTransformUtility_ScreenPointToWorldPointInRectangle_m1685 ();
extern "C" void RectTransformUtility_ScreenPointToLocalPointInRectangle_m1686 ();
extern "C" void RectTransformUtility_ScreenPointToRay_m1687 ();
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m1688 ();
extern "C" void RectTransformUtility_FlipLayoutAxes_m1689 ();
extern "C" void RectTransformUtility_GetTransposed_m1690 ();
extern "C" void Event__ctor_m1691 ();
extern "C" void Event__ctor_m1692 ();
extern "C" void Event__ctor_m1693 ();
extern "C" void Event_Finalize_m1694 ();
extern "C" void Event_get_mousePosition_m1695 ();
extern "C" void Event_set_mousePosition_m1696 ();
extern "C" void Event_get_delta_m1697 ();
extern "C" void Event_set_delta_m1698 ();
extern "C" void Event_get_mouseRay_m1699 ();
extern "C" void Event_set_mouseRay_m1700 ();
extern "C" void Event_get_shift_m1701 ();
extern "C" void Event_set_shift_m1702 ();
extern "C" void Event_get_control_m1703 ();
extern "C" void Event_set_control_m1704 ();
extern "C" void Event_get_alt_m1705 ();
extern "C" void Event_set_alt_m1706 ();
extern "C" void Event_get_command_m1707 ();
extern "C" void Event_set_command_m1708 ();
extern "C" void Event_get_capsLock_m1709 ();
extern "C" void Event_set_capsLock_m1710 ();
extern "C" void Event_get_numeric_m1711 ();
extern "C" void Event_set_numeric_m1712 ();
extern "C" void Event_get_functionKey_m1713 ();
extern "C" void Event_get_current_m359 ();
extern "C" void Event_set_current_m1714 ();
extern "C" void Event_Internal_MakeMasterEventCurrent_m1715 ();
extern "C" void Event_get_isKey_m684 ();
extern "C" void Event_get_isMouse_m1716 ();
extern "C" void Event_KeyboardEvent_m1717 ();
extern "C" void Event_GetHashCode_m1718 ();
extern "C" void Event_Equals_m1719 ();
extern "C" void Event_ToString_m1720 ();
extern "C" void Event_Init_m1721 ();
extern "C" void Event_Cleanup_m1722 ();
extern "C" void Event_InitCopy_m1723 ();
extern "C" void Event_InitPtr_m1724 ();
extern "C" void Event_get_rawType_m1725 ();
extern "C" void Event_get_type_m360 ();
extern "C" void Event_set_type_m1726 ();
extern "C" void Event_GetTypeForControl_m1727 ();
extern "C" void Event_Internal_SetMousePosition_m1728 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m1729 ();
extern "C" void Event_Internal_GetMousePosition_m1730 ();
extern "C" void Event_Internal_SetMouseDelta_m1731 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m1732 ();
extern "C" void Event_Internal_GetMouseDelta_m1733 ();
extern "C" void Event_get_button_m1734 ();
extern "C" void Event_set_button_m1735 ();
extern "C" void Event_get_modifiers_m1736 ();
extern "C" void Event_set_modifiers_m1737 ();
extern "C" void Event_get_pressure_m1738 ();
extern "C" void Event_set_pressure_m1739 ();
extern "C" void Event_get_clickCount_m1740 ();
extern "C" void Event_set_clickCount_m1741 ();
extern "C" void Event_get_character_m1742 ();
extern "C" void Event_set_character_m1743 ();
extern "C" void Event_get_commandName_m1744 ();
extern "C" void Event_set_commandName_m1745 ();
extern "C" void Event_get_keyCode_m685 ();
extern "C" void Event_set_keyCode_m1746 ();
extern "C" void Event_Internal_SetNativeEvent_m1747 ();
extern "C" void Event_Use_m1748 ();
extern "C" void Event_PopEvent_m1749 ();
extern "C" void Event_GetEventCount_m1750 ();
extern "C" void ScrollViewState__ctor_m1751 ();
extern "C" void WindowFunction__ctor_m1752 ();
extern "C" void WindowFunction_Invoke_m1753 ();
extern "C" void WindowFunction_BeginInvoke_m1754 ();
extern "C" void WindowFunction_EndInvoke_m1755 ();
extern "C" void GUI__cctor_m1756 ();
extern "C" void GUI_set_nextScrollStepTime_m1757 ();
extern "C" void GUI_set_skin_m1758 ();
extern "C" void GUI_get_skin_m1759 ();
extern "C" void GUI_DoSetSkin_m1760 ();
extern "C" void GUI_Label_m686 ();
extern "C" void GUI_Label_m1761 ();
extern "C" void GUI_DrawTexture_m375 ();
extern "C" void GUI_DrawTexture_m1762 ();
extern "C" void GUI_DrawTexture_m597 ();
extern "C" void GUI_Button_m338 ();
extern "C" void GUI_CallWindowDelegate_m1763 ();
extern "C" void GUI_get_color_m1764 ();
extern "C" void GUI_INTERNAL_get_color_m1765 ();
extern "C" void GUI_set_changed_m1766 ();
extern "C" void GUI_DoLabel_m1767 ();
extern "C" void GUI_INTERNAL_CALL_DoLabel_m1768 ();
extern "C" void GUI_get_blendMaterial_m1769 ();
extern "C" void GUI_get_blitMaterial_m1770 ();
extern "C" void GUI_DoButton_m1771 ();
extern "C" void GUI_INTERNAL_CALL_DoButton_m1772 ();
extern "C" void GUIContent__ctor_m1773 ();
extern "C" void GUIContent__ctor_m1774 ();
extern "C" void GUIContent__cctor_m1775 ();
extern "C" void GUIContent_Temp_m1776 ();
extern "C" void GUIContent_ClearStaticCache_m1777 ();
extern "C" void GUILayout_Width_m1778 ();
extern "C" void GUILayout_Height_m1779 ();
extern "C" void LayoutCache__ctor_m1780 ();
extern "C" void GUILayoutUtility__cctor_m1781 ();
extern "C" void GUILayoutUtility_SelectIDList_m1782 ();
extern "C" void GUILayoutUtility_Begin_m1783 ();
extern "C" void GUILayoutUtility_BeginWindow_m1784 ();
extern "C" void GUILayoutUtility_Layout_m1785 ();
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m1786 ();
extern "C" void GUILayoutUtility_LayoutFreeGroup_m1787 ();
extern "C" void GUILayoutUtility_LayoutSingleGroup_m1788 ();
extern "C" void GUILayoutUtility_get_spaceStyle_m1789 ();
extern "C" void GUILayoutUtility_Internal_GetWindowRect_m1790 ();
extern "C" void GUILayoutUtility_Internal_MoveWindow_m1791 ();
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m1792 ();
extern "C" void GUILayoutEntry__ctor_m1793 ();
extern "C" void GUILayoutEntry__cctor_m1794 ();
extern "C" void GUILayoutEntry_get_style_m1795 ();
extern "C" void GUILayoutEntry_set_style_m1796 ();
extern "C" void GUILayoutEntry_get_margin_m1797 ();
extern "C" void GUILayoutEntry_CalcWidth_m1798 ();
extern "C" void GUILayoutEntry_CalcHeight_m1799 ();
extern "C" void GUILayoutEntry_SetHorizontal_m1800 ();
extern "C" void GUILayoutEntry_SetVertical_m1801 ();
extern "C" void GUILayoutEntry_ApplyStyleSettings_m1802 ();
extern "C" void GUILayoutEntry_ApplyOptions_m1803 ();
extern "C" void GUILayoutEntry_ToString_m1804 ();
extern "C" void GUILayoutGroup__ctor_m1805 ();
extern "C" void GUILayoutGroup_get_margin_m1806 ();
extern "C" void GUILayoutGroup_ApplyOptions_m1807 ();
extern "C" void GUILayoutGroup_ApplyStyleSettings_m1808 ();
extern "C" void GUILayoutGroup_ResetCursor_m1809 ();
extern "C" void GUILayoutGroup_CalcWidth_m1810 ();
extern "C" void GUILayoutGroup_SetHorizontal_m1811 ();
extern "C" void GUILayoutGroup_CalcHeight_m1812 ();
extern "C" void GUILayoutGroup_SetVertical_m1813 ();
extern "C" void GUILayoutGroup_ToString_m1814 ();
extern "C" void GUIScrollGroup__ctor_m1815 ();
extern "C" void GUIScrollGroup_CalcWidth_m1816 ();
extern "C" void GUIScrollGroup_SetHorizontal_m1817 ();
extern "C" void GUIScrollGroup_CalcHeight_m1818 ();
extern "C" void GUIScrollGroup_SetVertical_m1819 ();
extern "C" void GUILayoutOption__ctor_m1820 ();
extern "C" void GUISettings__ctor_m1821 ();
extern "C" void SkinChangedDelegate__ctor_m1822 ();
extern "C" void SkinChangedDelegate_Invoke_m1823 ();
extern "C" void SkinChangedDelegate_BeginInvoke_m1824 ();
extern "C" void SkinChangedDelegate_EndInvoke_m1825 ();
extern "C" void GUISkin__ctor_m1826 ();
extern "C" void GUISkin_OnEnable_m1827 ();
extern "C" void GUISkin_get_font_m1828 ();
extern "C" void GUISkin_set_font_m1829 ();
extern "C" void GUISkin_get_box_m1830 ();
extern "C" void GUISkin_set_box_m1831 ();
extern "C" void GUISkin_get_label_m1832 ();
extern "C" void GUISkin_set_label_m1833 ();
extern "C" void GUISkin_get_textField_m1834 ();
extern "C" void GUISkin_set_textField_m1835 ();
extern "C" void GUISkin_get_textArea_m1836 ();
extern "C" void GUISkin_set_textArea_m1837 ();
extern "C" void GUISkin_get_button_m1838 ();
extern "C" void GUISkin_set_button_m1839 ();
extern "C" void GUISkin_get_toggle_m1840 ();
extern "C" void GUISkin_set_toggle_m1841 ();
extern "C" void GUISkin_get_window_m1842 ();
extern "C" void GUISkin_set_window_m1843 ();
extern "C" void GUISkin_get_horizontalSlider_m1844 ();
extern "C" void GUISkin_set_horizontalSlider_m1845 ();
extern "C" void GUISkin_get_horizontalSliderThumb_m1846 ();
extern "C" void GUISkin_set_horizontalSliderThumb_m1847 ();
extern "C" void GUISkin_get_verticalSlider_m1848 ();
extern "C" void GUISkin_set_verticalSlider_m1849 ();
extern "C" void GUISkin_get_verticalSliderThumb_m1850 ();
extern "C" void GUISkin_set_verticalSliderThumb_m1851 ();
extern "C" void GUISkin_get_horizontalScrollbar_m1852 ();
extern "C" void GUISkin_set_horizontalScrollbar_m1853 ();
extern "C" void GUISkin_get_horizontalScrollbarThumb_m1854 ();
extern "C" void GUISkin_set_horizontalScrollbarThumb_m1855 ();
extern "C" void GUISkin_get_horizontalScrollbarLeftButton_m1856 ();
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m1857 ();
extern "C" void GUISkin_get_horizontalScrollbarRightButton_m1858 ();
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m1859 ();
extern "C" void GUISkin_get_verticalScrollbar_m1860 ();
extern "C" void GUISkin_set_verticalScrollbar_m1861 ();
extern "C" void GUISkin_get_verticalScrollbarThumb_m1862 ();
extern "C" void GUISkin_set_verticalScrollbarThumb_m1863 ();
extern "C" void GUISkin_get_verticalScrollbarUpButton_m1864 ();
extern "C" void GUISkin_set_verticalScrollbarUpButton_m1865 ();
extern "C" void GUISkin_get_verticalScrollbarDownButton_m1866 ();
extern "C" void GUISkin_set_verticalScrollbarDownButton_m1867 ();
extern "C" void GUISkin_get_scrollView_m1868 ();
extern "C" void GUISkin_set_scrollView_m1869 ();
extern "C" void GUISkin_get_customStyles_m1870 ();
extern "C" void GUISkin_set_customStyles_m1871 ();
extern "C" void GUISkin_get_settings_m1872 ();
extern "C" void GUISkin_get_error_m1873 ();
extern "C" void GUISkin_Apply_m1874 ();
extern "C" void GUISkin_BuildStyleCache_m1875 ();
extern "C" void GUISkin_GetStyle_m1876 ();
extern "C" void GUISkin_FindStyle_m1877 ();
extern "C" void GUISkin_MakeCurrent_m1878 ();
extern "C" void GUISkin_GetEnumerator_m1879 ();
extern "C" void GUIStyleState__ctor_m1880 ();
extern "C" void GUIStyleState__ctor_m1881 ();
extern "C" void GUIStyleState_Finalize_m1882 ();
extern "C" void GUIStyleState_Init_m1883 ();
extern "C" void GUIStyleState_Cleanup_m1884 ();
extern "C" void GUIStyleState_GetBackgroundInternal_m1885 ();
extern "C" void GUIStyleState_set_textColor_m1886 ();
extern "C" void GUIStyleState_INTERNAL_set_textColor_m1887 ();
extern "C" void RectOffset__ctor_m1888 ();
extern "C" void RectOffset__ctor_m1889 ();
extern "C" void RectOffset_Finalize_m1890 ();
extern "C" void RectOffset_ToString_m1891 ();
extern "C" void RectOffset_Init_m1892 ();
extern "C" void RectOffset_Cleanup_m1893 ();
extern "C" void RectOffset_get_left_m1894 ();
extern "C" void RectOffset_set_left_m1895 ();
extern "C" void RectOffset_get_right_m1896 ();
extern "C" void RectOffset_set_right_m1897 ();
extern "C" void RectOffset_get_top_m1898 ();
extern "C" void RectOffset_set_top_m1899 ();
extern "C" void RectOffset_get_bottom_m1900 ();
extern "C" void RectOffset_set_bottom_m1901 ();
extern "C" void RectOffset_get_horizontal_m1902 ();
extern "C" void RectOffset_get_vertical_m1903 ();
extern "C" void GUIStyle__ctor_m1904 ();
extern "C" void GUIStyle__cctor_m1905 ();
extern "C" void GUIStyle_Finalize_m1906 ();
extern "C" void GUIStyle_get_normal_m1907 ();
extern "C" void GUIStyle_get_margin_m1908 ();
extern "C" void GUIStyle_get_padding_m1909 ();
extern "C" void GUIStyle_get_none_m1910 ();
extern "C" void GUIStyle_ToString_m1911 ();
extern "C" void GUIStyle_Init_m1912 ();
extern "C" void GUIStyle_Cleanup_m1913 ();
extern "C" void GUIStyle_get_name_m1914 ();
extern "C" void GUIStyle_set_name_m1915 ();
extern "C" void GUIStyle_GetStyleStatePtr_m1916 ();
extern "C" void GUIStyle_GetRectOffsetPtr_m1917 ();
extern "C" void GUIStyle_get_fixedWidth_m1918 ();
extern "C" void GUIStyle_get_fixedHeight_m1919 ();
extern "C" void GUIStyle_get_stretchWidth_m1920 ();
extern "C" void GUIStyle_set_stretchWidth_m1921 ();
extern "C" void GUIStyle_get_stretchHeight_m1922 ();
extern "C" void GUIStyle_set_stretchHeight_m1923 ();
extern "C" void GUIStyle_SetDefaultFont_m1924 ();
extern "C" void GUIUtility__cctor_m1925 ();
extern "C" void GUIUtility_get_pixelsPerPoint_m1926 ();
extern "C" void GUIUtility_GetDefaultSkin_m1927 ();
extern "C" void GUIUtility_BeginGUI_m1928 ();
extern "C" void GUIUtility_EndGUI_m1929 ();
extern "C" void GUIUtility_EndGUIFromException_m1930 ();
extern "C" void GUIUtility_CheckOnGUI_m1931 ();
extern "C" void GUIUtility_Internal_GetPixelsPerPoint_m1932 ();
extern "C" void GUIUtility_get_systemCopyBuffer_m1933 ();
extern "C" void GUIUtility_set_systemCopyBuffer_m1934 ();
extern "C" void GUIUtility_Internal_GetDefaultSkin_m1935 ();
extern "C" void GUIUtility_Internal_ExitGUI_m1936 ();
extern "C" void GUIUtility_Internal_GetGUIDepth_m1937 ();
extern "C" void MonoPInvokeCallbackAttribute__ctor_m1938 ();
extern "C" void WrapperlessIcall__ctor_m1939 ();
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m1940 ();
extern "C" void AttributeHelperEngine__cctor_m1941 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1942 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m1943 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m1944 ();
extern "C" void DisallowMultipleComponent__ctor_m1945 ();
extern "C" void RequireComponent__ctor_m1946 ();
extern "C" void AddComponentMenu__ctor_m1947 ();
extern "C" void AddComponentMenu__ctor_m1948 ();
extern "C" void ExecuteInEditMode__ctor_m1949 ();
extern "C" void HideInInspector__ctor_m1950 ();
extern "C" void SetupCoroutine__ctor_m1951 ();
extern "C" void SetupCoroutine_InvokeMember_m1952 ();
extern "C" void SetupCoroutine_InvokeStatic_m1953 ();
extern "C" void WritableAttribute__ctor_m1954 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m1955 ();
extern "C" void GcUserProfileData_ToUserProfile_m1956 ();
extern "C" void GcUserProfileData_AddToArray_m1957 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m1958 ();
extern "C" void GcAchievementData_ToAchievement_m1959 ();
extern "C" void GcScoreData_ToScore_m1960 ();
extern "C" void Resolution_get_width_m1961 ();
extern "C" void Resolution_set_width_m1962 ();
extern "C" void Resolution_get_height_m1963 ();
extern "C" void Resolution_set_height_m1964 ();
extern "C" void Resolution_get_refreshRate_m1965 ();
extern "C" void Resolution_set_refreshRate_m1966 ();
extern "C" void Resolution_ToString_m1967 ();
extern "C" void LocalUser__ctor_m1968 ();
extern "C" void LocalUser_SetFriends_m1969 ();
extern "C" void LocalUser_SetAuthenticated_m1970 ();
extern "C" void LocalUser_SetUnderage_m1971 ();
extern "C" void LocalUser_get_authenticated_m1972 ();
extern "C" void UserProfile__ctor_m1973 ();
extern "C" void UserProfile__ctor_m1974 ();
extern "C" void UserProfile_ToString_m1975 ();
extern "C" void UserProfile_SetUserName_m1976 ();
extern "C" void UserProfile_SetUserID_m1977 ();
extern "C" void UserProfile_SetImage_m1978 ();
extern "C" void UserProfile_get_userName_m1979 ();
extern "C" void UserProfile_get_id_m1980 ();
extern "C" void UserProfile_get_isFriend_m1981 ();
extern "C" void UserProfile_get_state_m1982 ();
extern "C" void Achievement__ctor_m1983 ();
extern "C" void Achievement__ctor_m1984 ();
extern "C" void Achievement__ctor_m1985 ();
extern "C" void Achievement_ToString_m1986 ();
extern "C" void Achievement_get_id_m1987 ();
extern "C" void Achievement_set_id_m1988 ();
extern "C" void Achievement_get_percentCompleted_m1989 ();
extern "C" void Achievement_set_percentCompleted_m1990 ();
extern "C" void Achievement_get_completed_m1991 ();
extern "C" void Achievement_get_hidden_m1992 ();
extern "C" void Achievement_get_lastReportedDate_m1993 ();
extern "C" void AchievementDescription__ctor_m1994 ();
extern "C" void AchievementDescription_ToString_m1995 ();
extern "C" void AchievementDescription_SetImage_m1996 ();
extern "C" void AchievementDescription_get_id_m1997 ();
extern "C" void AchievementDescription_set_id_m1998 ();
extern "C" void AchievementDescription_get_title_m1999 ();
extern "C" void AchievementDescription_get_achievedDescription_m2000 ();
extern "C" void AchievementDescription_get_unachievedDescription_m2001 ();
extern "C" void AchievementDescription_get_hidden_m2002 ();
extern "C" void AchievementDescription_get_points_m2003 ();
extern "C" void Score__ctor_m2004 ();
extern "C" void Score__ctor_m2005 ();
extern "C" void Score_ToString_m2006 ();
extern "C" void Score_get_leaderboardID_m2007 ();
extern "C" void Score_set_leaderboardID_m2008 ();
extern "C" void Score_get_value_m2009 ();
extern "C" void Score_set_value_m2010 ();
extern "C" void Leaderboard__ctor_m2011 ();
extern "C" void Leaderboard_ToString_m2012 ();
extern "C" void Leaderboard_SetLocalUserScore_m2013 ();
extern "C" void Leaderboard_SetMaxRange_m2014 ();
extern "C" void Leaderboard_SetScores_m2015 ();
extern "C" void Leaderboard_SetTitle_m2016 ();
extern "C" void Leaderboard_GetUserFilter_m2017 ();
extern "C" void Leaderboard_get_id_m2018 ();
extern "C" void Leaderboard_set_id_m2019 ();
extern "C" void Leaderboard_get_userScope_m2020 ();
extern "C" void Leaderboard_set_userScope_m2021 ();
extern "C" void Leaderboard_get_range_m2022 ();
extern "C" void Leaderboard_set_range_m2023 ();
extern "C" void Leaderboard_get_timeScope_m2024 ();
extern "C" void Leaderboard_set_timeScope_m2025 ();
extern "C" void HitInfo_SendMessage_m2026 ();
extern "C" void HitInfo_Compare_m2027 ();
extern "C" void HitInfo_op_Implicit_m2028 ();
extern "C" void SendMouseEvents__cctor_m2029 ();
extern "C" void SendMouseEvents_SetMouseMoved_m2030 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m2031 ();
extern "C" void SendMouseEvents_SendEvents_m2032 ();
extern "C" void Range__ctor_m2033 ();
extern "C" void PropertyAttribute__ctor_m2034 ();
extern "C" void TooltipAttribute__ctor_m2035 ();
extern "C" void SpaceAttribute__ctor_m2036 ();
extern "C" void SpaceAttribute__ctor_m2037 ();
extern "C" void RangeAttribute__ctor_m2038 ();
extern "C" void TextAreaAttribute__ctor_m2039 ();
extern "C" void SelectionBaseAttribute__ctor_m2040 ();
extern "C" void SliderState__ctor_m2041 ();
extern "C" void StackTraceUtility__ctor_m2042 ();
extern "C" void StackTraceUtility__cctor_m2043 ();
extern "C" void StackTraceUtility_SetProjectFolder_m2044 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m2045 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m2046 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m2047 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m2048 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m2049 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m2050 ();
extern "C" void UnityException__ctor_m2051 ();
extern "C" void UnityException__ctor_m2052 ();
extern "C" void UnityException__ctor_m2053 ();
extern "C" void UnityException__ctor_m2054 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m2055 ();
extern "C" void StateMachineBehaviour__ctor_m2056 ();
extern "C" void TextEditor__ctor_m2057 ();
extern "C" void TextGenerationSettings_CompareColors_m2058 ();
extern "C" void TextGenerationSettings_CompareVector2_m2059 ();
extern "C" void TextGenerationSettings_Equals_m2060 ();
extern "C" void TrackedReference_Equals_m2061 ();
extern "C" void TrackedReference_GetHashCode_m2062 ();
extern "C" void TrackedReference_op_Equality_m2063 ();
extern "C" void ArgumentCache__ctor_m2064 ();
extern "C" void ArgumentCache_get_unityObjectArgument_m2065 ();
extern "C" void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2066 ();
extern "C" void ArgumentCache_get_intArgument_m2067 ();
extern "C" void ArgumentCache_get_floatArgument_m2068 ();
extern "C" void ArgumentCache_get_stringArgument_m2069 ();
extern "C" void ArgumentCache_get_boolArgument_m2070 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m2071 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m2072 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m2073 ();
extern "C" void BaseInvokableCall__ctor_m2074 ();
extern "C" void BaseInvokableCall__ctor_m2075 ();
extern "C" void BaseInvokableCall_AllowInvoke_m2076 ();
extern "C" void InvokableCall__ctor_m2077 ();
extern "C" void InvokableCall__ctor_m2078 ();
extern "C" void InvokableCall_Invoke_m2079 ();
extern "C" void InvokableCall_Find_m2080 ();
extern "C" void PersistentCall__ctor_m2081 ();
extern "C" void PersistentCall_get_target_m2082 ();
extern "C" void PersistentCall_get_methodName_m2083 ();
extern "C" void PersistentCall_get_mode_m2084 ();
extern "C" void PersistentCall_get_arguments_m2085 ();
extern "C" void PersistentCall_IsValid_m2086 ();
extern "C" void PersistentCall_GetRuntimeCall_m2087 ();
extern "C" void PersistentCall_GetObjectCall_m2088 ();
extern "C" void PersistentCallGroup__ctor_m2089 ();
extern "C" void PersistentCallGroup_Initialize_m2090 ();
extern "C" void InvokableCallList__ctor_m2091 ();
extern "C" void InvokableCallList_AddPersistentInvokableCall_m2092 ();
extern "C" void InvokableCallList_AddListener_m2093 ();
extern "C" void InvokableCallList_RemoveListener_m2094 ();
extern "C" void InvokableCallList_ClearPersistent_m2095 ();
extern "C" void InvokableCallList_Invoke_m2096 ();
extern "C" void UnityEventBase__ctor_m2097 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2098 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2099 ();
extern "C" void UnityEventBase_FindMethod_m2100 ();
extern "C" void UnityEventBase_FindMethod_m2101 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m2102 ();
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m2103 ();
extern "C" void UnityEventBase_AddCall_m2104 ();
extern "C" void UnityEventBase_RemoveListener_m2105 ();
extern "C" void UnityEventBase_Invoke_m2106 ();
extern "C" void UnityEventBase_ToString_m2107 ();
extern "C" void UnityEventBase_GetValidMethodInfo_m2108 ();
extern "C" void UnityEvent__ctor_m2109 ();
extern "C" void UnityEvent_AddListener_m2110 ();
extern "C" void UnityEvent_FindMethod_Impl_m2111 ();
extern "C" void UnityEvent_GetDelegate_m2112 ();
extern "C" void UnityEvent_GetDelegate_m2113 ();
extern "C" void UnityEvent_Invoke_m2114 ();
extern "C" void DefaultValueAttribute__ctor_m2115 ();
extern "C" void DefaultValueAttribute_get_Value_m2116 ();
extern "C" void DefaultValueAttribute_Equals_m2117 ();
extern "C" void DefaultValueAttribute_GetHashCode_m2118 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m2119 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m2120 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m2121 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m2122 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m2123 ();
extern "C" void GenericStack__ctor_m2124 ();
extern "C" void NetFxCoreExtensions_CreateDelegate_m2125 ();
extern "C" void NetFxCoreExtensions_GetMethodInfo_m2126 ();
extern "C" void UnityAdsDelegate__ctor_m2127 ();
extern "C" void UnityAdsDelegate_Invoke_m2128 ();
extern "C" void UnityAdsDelegate_BeginInvoke_m2129 ();
extern "C" void UnityAdsDelegate_EndInvoke_m2130 ();
extern "C" void UnityAction__ctor_m2131 ();
extern "C" void UnityAction_Invoke_m2132 ();
extern "C" void UnityAction_BeginInvoke_m2133 ();
extern "C" void UnityAction_EndInvoke_m2134 ();
extern "C" void EventSystem__ctor_m2247 ();
extern "C" void EventSystem__cctor_m2248 ();
extern "C" void EventSystem_get_current_m2249 ();
extern "C" void EventSystem_set_current_m2250 ();
extern "C" void EventSystem_get_sendNavigationEvents_m2251 ();
extern "C" void EventSystem_set_sendNavigationEvents_m2252 ();
extern "C" void EventSystem_get_pixelDragThreshold_m2253 ();
extern "C" void EventSystem_set_pixelDragThreshold_m2254 ();
extern "C" void EventSystem_get_currentInputModule_m2255 ();
extern "C" void EventSystem_get_firstSelectedGameObject_m2256 ();
extern "C" void EventSystem_set_firstSelectedGameObject_m2257 ();
extern "C" void EventSystem_get_currentSelectedGameObject_m518 ();
extern "C" void EventSystem_get_lastSelectedGameObject_m2258 ();
extern "C" void EventSystem_UpdateModules_m2259 ();
extern "C" void EventSystem_get_alreadySelecting_m2260 ();
extern "C" void EventSystem_SetSelectedGameObject_m506 ();
extern "C" void EventSystem_get_baseEventDataCache_m2261 ();
extern "C" void EventSystem_SetSelectedGameObject_m2262 ();
extern "C" void EventSystem_RaycastComparer_m2263 ();
extern "C" void EventSystem_RaycastAll_m512 ();
extern "C" void EventSystem_IsPointerOverGameObject_m2264 ();
extern "C" void EventSystem_IsPointerOverGameObject_m2265 ();
extern "C" void EventSystem_OnEnable_m2266 ();
extern "C" void EventSystem_OnDisable_m2267 ();
extern "C" void EventSystem_TickModules_m2268 ();
extern "C" void EventSystem_Update_m2269 ();
extern "C" void EventSystem_ChangeEventModule_m2270 ();
extern "C" void EventSystem_ToString_m2271 ();
extern "C" void TriggerEvent__ctor_m2272 ();
extern "C" void Entry__ctor_m2273 ();
extern "C" void EventTrigger__ctor_m2274 ();
extern "C" void EventTrigger_get_triggers_m2275 ();
extern "C" void EventTrigger_set_triggers_m2276 ();
extern "C" void EventTrigger_Execute_m2277 ();
extern "C" void EventTrigger_OnPointerEnter_m2278 ();
extern "C" void EventTrigger_OnPointerExit_m2279 ();
extern "C" void EventTrigger_OnDrag_m2280 ();
extern "C" void EventTrigger_OnDrop_m2281 ();
extern "C" void EventTrigger_OnPointerDown_m2282 ();
extern "C" void EventTrigger_OnPointerUp_m2283 ();
extern "C" void EventTrigger_OnPointerClick_m2284 ();
extern "C" void EventTrigger_OnSelect_m2285 ();
extern "C" void EventTrigger_OnDeselect_m2286 ();
extern "C" void EventTrigger_OnScroll_m2287 ();
extern "C" void EventTrigger_OnMove_m2288 ();
extern "C" void EventTrigger_OnUpdateSelected_m2289 ();
extern "C" void EventTrigger_OnInitializePotentialDrag_m2290 ();
extern "C" void EventTrigger_OnBeginDrag_m2291 ();
extern "C" void EventTrigger_OnEndDrag_m2292 ();
extern "C" void EventTrigger_OnSubmit_m2293 ();
extern "C" void EventTrigger_OnCancel_m2294 ();
extern "C" void ExecuteEvents__cctor_m2295 ();
extern "C" void ExecuteEvents_Execute_m2296 ();
extern "C" void ExecuteEvents_Execute_m2297 ();
extern "C" void ExecuteEvents_Execute_m2298 ();
extern "C" void ExecuteEvents_Execute_m2299 ();
extern "C" void ExecuteEvents_Execute_m2300 ();
extern "C" void ExecuteEvents_Execute_m2301 ();
extern "C" void ExecuteEvents_Execute_m2302 ();
extern "C" void ExecuteEvents_Execute_m2303 ();
extern "C" void ExecuteEvents_Execute_m2304 ();
extern "C" void ExecuteEvents_Execute_m2305 ();
extern "C" void ExecuteEvents_Execute_m2306 ();
extern "C" void ExecuteEvents_Execute_m2307 ();
extern "C" void ExecuteEvents_Execute_m2308 ();
extern "C" void ExecuteEvents_Execute_m2309 ();
extern "C" void ExecuteEvents_Execute_m2310 ();
extern "C" void ExecuteEvents_Execute_m2311 ();
extern "C" void ExecuteEvents_Execute_m2312 ();
extern "C" void ExecuteEvents_get_pointerEnterHandler_m2313 ();
extern "C" void ExecuteEvents_get_pointerExitHandler_m2314 ();
extern "C" void ExecuteEvents_get_pointerDownHandler_m538 ();
extern "C" void ExecuteEvents_get_pointerUpHandler_m527 ();
extern "C" void ExecuteEvents_get_pointerClickHandler_m529 ();
extern "C" void ExecuteEvents_get_initializePotentialDrag_m2315 ();
extern "C" void ExecuteEvents_get_beginDragHandler_m2316 ();
extern "C" void ExecuteEvents_get_dragHandler_m2317 ();
extern "C" void ExecuteEvents_get_endDragHandler_m2318 ();
extern "C" void ExecuteEvents_get_dropHandler_m2319 ();
extern "C" void ExecuteEvents_get_scrollHandler_m2320 ();
extern "C" void ExecuteEvents_get_updateSelectedHandler_m519 ();
extern "C" void ExecuteEvents_get_selectHandler_m2321 ();
extern "C" void ExecuteEvents_get_deselectHandler_m2322 ();
extern "C" void ExecuteEvents_get_moveHandler_m2323 ();
extern "C" void ExecuteEvents_get_submitHandler_m2324 ();
extern "C" void ExecuteEvents_get_cancelHandler_m2325 ();
extern "C" void ExecuteEvents_GetEventChain_m2326 ();
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m2327 ();
extern "C" void RaycasterManager__cctor_m2328 ();
extern "C" void RaycasterManager_AddRaycaster_m2329 ();
extern "C" void RaycasterManager_GetRaycasters_m2330 ();
extern "C" void RaycasterManager_RemoveRaycasters_m2331 ();
extern "C" void RaycastResult_get_gameObject_m516 ();
extern "C" void RaycastResult_set_gameObject_m2332 ();
extern "C" void RaycastResult_get_isValid_m2333 ();
extern "C" void RaycastResult_ToString_m2334 ();
extern "C" void UIBehaviour__ctor_m2335 ();
extern "C" void UIBehaviour_Awake_m2336 ();
extern "C" void UIBehaviour_OnEnable_m2337 ();
extern "C" void UIBehaviour_Start_m2338 ();
extern "C" void UIBehaviour_OnDisable_m2339 ();
extern "C" void UIBehaviour_OnDestroy_m2340 ();
extern "C" void UIBehaviour_IsActive_m2341 ();
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m2342 ();
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m2343 ();
extern "C" void UIBehaviour_OnTransformParentChanged_m2344 ();
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m2345 ();
extern "C" void UIBehaviour_OnCanvasGroupChanged_m2346 ();
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m2347 ();
extern "C" void UIBehaviour_IsDestroyed_m2348 ();
extern "C" void AxisEventData__ctor_m2349 ();
extern "C" void AxisEventData_set_moveVector_m2350 ();
extern "C" void AxisEventData_get_moveDir_m2351 ();
extern "C" void AxisEventData_set_moveDir_m2352 ();
extern "C" void BaseEventData__ctor_m2353 ();
extern "C" void BaseEventData_Reset_m510 ();
extern "C" void BaseEventData_Use_m2354 ();
extern "C" void BaseEventData_get_used_m2355 ();
extern "C" void BaseEventData_set_selectedObject_m2356 ();
extern "C" void PointerEventData__ctor_m509 ();
extern "C" void PointerEventData_get_pointerEnter_m508 ();
extern "C" void PointerEventData_set_pointerEnter_m2357 ();
extern "C" void PointerEventData_get_lastPress_m2358 ();
extern "C" void PointerEventData_set_lastPress_m2359 ();
extern "C" void PointerEventData_set_rawPointerPress_m532 ();
extern "C" void PointerEventData_get_pointerDrag_m2360 ();
extern "C" void PointerEventData_set_pointerDrag_m2361 ();
extern "C" void PointerEventData_get_pointerCurrentRaycast_m515 ();
extern "C" void PointerEventData_set_pointerCurrentRaycast_m514 ();
extern "C" void PointerEventData_get_pointerPressRaycast_m2362 ();
extern "C" void PointerEventData_set_pointerPressRaycast_m537 ();
extern "C" void PointerEventData_get_eligibleForClick_m522 ();
extern "C" void PointerEventData_set_eligibleForClick_m533 ();
extern "C" void PointerEventData_get_pointerId_m2363 ();
extern "C" void PointerEventData_set_pointerId_m2364 ();
extern "C" void PointerEventData_get_position_m535 ();
extern "C" void PointerEventData_set_position_m511 ();
extern "C" void PointerEventData_get_delta_m2365 ();
extern "C" void PointerEventData_set_delta_m2366 ();
extern "C" void PointerEventData_get_pressPosition_m2367 ();
extern "C" void PointerEventData_set_pressPosition_m536 ();
extern "C" void PointerEventData_get_clickTime_m524 ();
extern "C" void PointerEventData_set_clickTime_m541 ();
extern "C" void PointerEventData_get_clickCount_m2368 ();
extern "C" void PointerEventData_set_clickCount_m534 ();
extern "C" void PointerEventData_get_scrollDelta_m2369 ();
extern "C" void PointerEventData_set_scrollDelta_m2370 ();
extern "C" void PointerEventData_get_useDragThreshold_m2371 ();
extern "C" void PointerEventData_set_useDragThreshold_m2372 ();
extern "C" void PointerEventData_get_dragging_m2373 ();
extern "C" void PointerEventData_set_dragging_m2374 ();
extern "C" void PointerEventData_get_button_m2375 ();
extern "C" void PointerEventData_set_button_m2376 ();
extern "C" void PointerEventData_IsPointerMoving_m2377 ();
extern "C" void PointerEventData_get_enterEventCamera_m521 ();
extern "C" void PointerEventData_get_pressEventCamera_m2378 ();
extern "C" void PointerEventData_get_pointerPress_m526 ();
extern "C" void PointerEventData_set_pointerPress_m531 ();
extern "C" void PointerEventData_ToString_m2379 ();
extern "C" void BaseInputModule__ctor_m501 ();
extern "C" void BaseInputModule_get_eventSystem_m505 ();
extern "C" void BaseInputModule_OnEnable_m2380 ();
extern "C" void BaseInputModule_OnDisable_m2381 ();
extern "C" void BaseInputModule_FindFirstRaycast_m513 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m2382 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m2383 ();
extern "C" void BaseInputModule_FindCommonRoot_m2384 ();
extern "C" void BaseInputModule_HandlePointerExitAndEnter_m504 ();
extern "C" void BaseInputModule_GetAxisEventData_m2385 ();
extern "C" void BaseInputModule_GetBaseEventData_m2386 ();
extern "C" void BaseInputModule_IsPointerOverGameObject_m2387 ();
extern "C" void BaseInputModule_ShouldActivateModule_m502 ();
extern "C" void BaseInputModule_DeactivateModule_m503 ();
extern "C" void BaseInputModule_ActivateModule_m2388 ();
extern "C" void BaseInputModule_UpdateModule_m2389 ();
extern "C" void BaseInputModule_IsModuleSupported_m2390 ();
extern "C" void ButtonState__ctor_m2391 ();
extern "C" void ButtonState_get_eventData_m2392 ();
extern "C" void ButtonState_set_eventData_m2393 ();
extern "C" void ButtonState_get_button_m2394 ();
extern "C" void ButtonState_set_button_m2395 ();
extern "C" void MouseState__ctor_m2396 ();
extern "C" void MouseState_GetButtonState_m2397 ();
extern "C" void MouseState_SetButtonState_m2398 ();
extern "C" void MouseButtonEventData__ctor_m2399 ();
extern "C" void MouseButtonEventData_PressedThisFrame_m2400 ();
extern "C" void MouseButtonEventData_ReleasedThisFrame_m2401 ();
extern "C" void PointerInputModule__ctor_m2402 ();
extern "C" void PointerInputModule_GetPointerData_m2403 ();
extern "C" void PointerInputModule_RemovePointerData_m2404 ();
extern "C" void PointerInputModule_GetTouchPointerEventData_m2405 ();
extern "C" void PointerInputModule_CopyFromTo_m2406 ();
extern "C" void PointerInputModule_StateForMouseButton_m2407 ();
extern "C" void PointerInputModule_GetMousePointerEventData_m2408 ();
extern "C" void PointerInputModule_GetMousePointerEventData_m2409 ();
extern "C" void PointerInputModule_GetLastPointerEventData_m2410 ();
extern "C" void PointerInputModule_ShouldStartDrag_m2411 ();
extern "C" void PointerInputModule_ProcessMove_m2412 ();
extern "C" void PointerInputModule_ProcessDrag_m2413 ();
extern "C" void PointerInputModule_IsPointerOverGameObject_m2414 ();
extern "C" void PointerInputModule_ClearSelection_m2415 ();
extern "C" void PointerInputModule_ToString_m2416 ();
extern "C" void PointerInputModule_DeselectIfSelectionChanged_m2417 ();
extern "C" void StandaloneInputModule__ctor_m2418 ();
extern "C" void StandaloneInputModule_get_inputMode_m2419 ();
extern "C" void StandaloneInputModule_get_allowActivationOnMobileDevice_m2420 ();
extern "C" void StandaloneInputModule_set_allowActivationOnMobileDevice_m2421 ();
extern "C" void StandaloneInputModule_get_forceModuleActive_m2422 ();
extern "C" void StandaloneInputModule_set_forceModuleActive_m2423 ();
extern "C" void StandaloneInputModule_get_inputActionsPerSecond_m2424 ();
extern "C" void StandaloneInputModule_set_inputActionsPerSecond_m2425 ();
extern "C" void StandaloneInputModule_get_repeatDelay_m2426 ();
extern "C" void StandaloneInputModule_set_repeatDelay_m2427 ();
extern "C" void StandaloneInputModule_get_horizontalAxis_m2428 ();
extern "C" void StandaloneInputModule_set_horizontalAxis_m2429 ();
extern "C" void StandaloneInputModule_get_verticalAxis_m2430 ();
extern "C" void StandaloneInputModule_set_verticalAxis_m2431 ();
extern "C" void StandaloneInputModule_get_submitButton_m2432 ();
extern "C" void StandaloneInputModule_set_submitButton_m2433 ();
extern "C" void StandaloneInputModule_get_cancelButton_m2434 ();
extern "C" void StandaloneInputModule_set_cancelButton_m2435 ();
extern "C" void StandaloneInputModule_UpdateModule_m2436 ();
extern "C" void StandaloneInputModule_IsModuleSupported_m2437 ();
extern "C" void StandaloneInputModule_ShouldActivateModule_m2438 ();
extern "C" void StandaloneInputModule_ActivateModule_m2439 ();
extern "C" void StandaloneInputModule_DeactivateModule_m2440 ();
extern "C" void StandaloneInputModule_Process_m2441 ();
extern "C" void StandaloneInputModule_SendSubmitEventToSelectedObject_m2442 ();
extern "C" void StandaloneInputModule_GetRawMoveVector_m2443 ();
extern "C" void StandaloneInputModule_SendMoveEventToSelectedObject_m2444 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m2445 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m2446 ();
extern "C" void StandaloneInputModule_SendUpdateEventToSelectedObject_m2447 ();
extern "C" void StandaloneInputModule_ProcessMousePress_m2448 ();
extern "C" void TouchInputModule__ctor_m2449 ();
extern "C" void TouchInputModule_get_allowActivationOnStandalone_m2450 ();
extern "C" void TouchInputModule_set_allowActivationOnStandalone_m2451 ();
extern "C" void TouchInputModule_get_forceModuleActive_m2452 ();
extern "C" void TouchInputModule_set_forceModuleActive_m2453 ();
extern "C" void TouchInputModule_UpdateModule_m2454 ();
extern "C" void TouchInputModule_IsModuleSupported_m2455 ();
extern "C" void TouchInputModule_ShouldActivateModule_m2456 ();
extern "C" void TouchInputModule_UseFakeInput_m2457 ();
extern "C" void TouchInputModule_Process_m2458 ();
extern "C" void TouchInputModule_FakeTouches_m2459 ();
extern "C" void TouchInputModule_ProcessTouchEvents_m2460 ();
extern "C" void TouchInputModule_ProcessTouchPress_m2461 ();
extern "C" void TouchInputModule_DeactivateModule_m2462 ();
extern "C" void TouchInputModule_ToString_m2463 ();
extern "C" void BaseRaycaster__ctor_m2464 ();
extern "C" void BaseRaycaster_get_priority_m2465 ();
extern "C" void BaseRaycaster_get_sortOrderPriority_m2466 ();
extern "C" void BaseRaycaster_get_renderOrderPriority_m2467 ();
extern "C" void BaseRaycaster_ToString_m2468 ();
extern "C" void BaseRaycaster_OnEnable_m2469 ();
extern "C" void BaseRaycaster_OnDisable_m2470 ();
extern "C" void Physics2DRaycaster__ctor_m2471 ();
extern "C" void Physics2DRaycaster_Raycast_m2472 ();
extern "C" void PhysicsRaycaster__ctor_m2473 ();
extern "C" void PhysicsRaycaster_get_eventCamera_m2474 ();
extern "C" void PhysicsRaycaster_get_depth_m2475 ();
extern "C" void PhysicsRaycaster_get_finalEventMask_m2476 ();
extern "C" void PhysicsRaycaster_get_eventMask_m2477 ();
extern "C" void PhysicsRaycaster_set_eventMask_m2478 ();
extern "C" void PhysicsRaycaster_Raycast_m2479 ();
extern "C" void PhysicsRaycaster_U3CRaycastU3Em__1_m2480 ();
extern "C" void ColorTweenCallback__ctor_m2481 ();
extern "C" void ColorTween_get_startColor_m2482 ();
extern "C" void ColorTween_set_startColor_m2483 ();
extern "C" void ColorTween_get_targetColor_m2484 ();
extern "C" void ColorTween_set_targetColor_m2485 ();
extern "C" void ColorTween_get_tweenMode_m2486 ();
extern "C" void ColorTween_set_tweenMode_m2487 ();
extern "C" void ColorTween_get_duration_m2488 ();
extern "C" void ColorTween_set_duration_m2489 ();
extern "C" void ColorTween_get_ignoreTimeScale_m2490 ();
extern "C" void ColorTween_set_ignoreTimeScale_m2491 ();
extern "C" void ColorTween_TweenValue_m2492 ();
extern "C" void ColorTween_AddOnChangedCallback_m2493 ();
extern "C" void ColorTween_GetIgnoreTimescale_m2494 ();
extern "C" void ColorTween_GetDuration_m2495 ();
extern "C" void ColorTween_ValidTarget_m2496 ();
extern "C" void FloatTweenCallback__ctor_m2497 ();
extern "C" void FloatTween_get_startValue_m2498 ();
extern "C" void FloatTween_set_startValue_m2499 ();
extern "C" void FloatTween_get_targetValue_m2500 ();
extern "C" void FloatTween_set_targetValue_m2501 ();
extern "C" void FloatTween_get_duration_m2502 ();
extern "C" void FloatTween_set_duration_m2503 ();
extern "C" void FloatTween_get_ignoreTimeScale_m2504 ();
extern "C" void FloatTween_set_ignoreTimeScale_m2505 ();
extern "C" void FloatTween_TweenValue_m2506 ();
extern "C" void FloatTween_AddOnChangedCallback_m2507 ();
extern "C" void FloatTween_GetIgnoreTimescale_m2508 ();
extern "C" void FloatTween_GetDuration_m2509 ();
extern "C" void FloatTween_ValidTarget_m2510 ();
extern "C" void AnimationTriggers__ctor_m2511 ();
extern "C" void AnimationTriggers_get_normalTrigger_m2512 ();
extern "C" void AnimationTriggers_get_highlightedTrigger_m2513 ();
extern "C" void AnimationTriggers_get_pressedTrigger_m2514 ();
extern "C" void AnimationTriggers_get_disabledTrigger_m2515 ();
extern "C" void ButtonClickedEvent__ctor_m2516 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1__ctor_m2517 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2518 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2519 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m2520 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Dispose_m2521 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Reset_m2522 ();
extern "C" void Button__ctor_m2523 ();
extern "C" void Button_get_onClick_m2524 ();
extern "C" void Button_set_onClick_m2525 ();
extern "C" void Button_Press_m2526 ();
extern "C" void Button_OnPointerClick_m2527 ();
extern "C" void Button_OnSubmit_m2528 ();
extern "C" void Button_OnFinishSubmit_m2529 ();
extern "C" void CanvasUpdateRegistry__ctor_m2530 ();
extern "C" void CanvasUpdateRegistry__cctor_m2531 ();
extern "C" void CanvasUpdateRegistry_get_instance_m2532 ();
extern "C" void CanvasUpdateRegistry_ObjectValidForUpdate_m2533 ();
extern "C" void CanvasUpdateRegistry_PerformUpdate_m2534 ();
extern "C" void CanvasUpdateRegistry_ParentCount_m2535 ();
extern "C" void CanvasUpdateRegistry_SortLayoutList_m2536 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m2537 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m2538 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m2539 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m2540 ();
extern "C" void CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m2541 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m2542 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m2543 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingLayout_m2544 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingGraphics_m2545 ();
extern "C" void CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m2546 ();
extern "C" void CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m2547 ();
extern "C" void ColorBlock_get_normalColor_m2548 ();
extern "C" void ColorBlock_get_highlightedColor_m2549 ();
extern "C" void ColorBlock_get_pressedColor_m2550 ();
extern "C" void ColorBlock_get_disabledColor_m2551 ();
extern "C" void ColorBlock_get_colorMultiplier_m2552 ();
extern "C" void ColorBlock_set_colorMultiplier_m2553 ();
extern "C" void ColorBlock_get_fadeDuration_m2554 ();
extern "C" void ColorBlock_set_fadeDuration_m2555 ();
extern "C" void ColorBlock_get_defaultColorBlock_m2556 ();
extern "C" void DropdownItem_get_text_m2557 ();
extern "C" void DropdownItem_set_text_m2558 ();
extern "C" void DropdownItem_get_image_m2559 ();
extern "C" void DropdownItem_set_image_m2560 ();
extern "C" void DropdownItem_get_rectTransform_m2561 ();
extern "C" void DropdownItem_set_rectTransform_m2562 ();
extern "C" void DropdownItem_get_toggle_m2563 ();
extern "C" void DropdownItem_set_toggle_m2564 ();
extern "C" void DropdownItem_OnPointerEnter_m2565 ();
extern "C" void DropdownItem_OnCancel_m2566 ();
extern "C" void OptionData__ctor_m2567 ();
extern "C" void OptionData_get_text_m2568 ();
extern "C" void OptionData_get_image_m2569 ();
extern "C" void OptionDataList__ctor_m2570 ();
extern "C" void OptionDataList_get_options_m2571 ();
extern "C" void OptionDataList_set_options_m2572 ();
extern "C" void DropdownEvent__ctor_m2573 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2__ctor_m2574 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2575 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2576 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_MoveNext_m2577 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_Dispose_m2578 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_Reset_m2579 ();
extern "C" void U3CShowU3Ec__AnonStorey6__ctor_m2580 ();
extern "C" void U3CShowU3Ec__AnonStorey6_U3CU3Em__4_m2581 ();
extern "C" void Dropdown__ctor_m2582 ();
extern "C" void Dropdown_get_template_m2583 ();
extern "C" void Dropdown_set_template_m2584 ();
extern "C" void Dropdown_get_captionText_m2585 ();
extern "C" void Dropdown_set_captionText_m2586 ();
extern "C" void Dropdown_get_captionImage_m2587 ();
extern "C" void Dropdown_set_captionImage_m2588 ();
extern "C" void Dropdown_get_itemText_m2589 ();
extern "C" void Dropdown_set_itemText_m2590 ();
extern "C" void Dropdown_get_itemImage_m2591 ();
extern "C" void Dropdown_set_itemImage_m2592 ();
extern "C" void Dropdown_get_options_m2593 ();
extern "C" void Dropdown_set_options_m2594 ();
extern "C" void Dropdown_get_onValueChanged_m2595 ();
extern "C" void Dropdown_set_onValueChanged_m2596 ();
extern "C" void Dropdown_get_value_m2597 ();
extern "C" void Dropdown_set_value_m2598 ();
extern "C" void Dropdown_Awake_m2599 ();
extern "C" void Dropdown_Refresh_m2600 ();
extern "C" void Dropdown_SetupTemplate_m2601 ();
extern "C" void Dropdown_OnPointerClick_m2602 ();
extern "C" void Dropdown_OnSubmit_m2603 ();
extern "C" void Dropdown_OnCancel_m2604 ();
extern "C" void Dropdown_Show_m2605 ();
extern "C" void Dropdown_CreateBlocker_m2606 ();
extern "C" void Dropdown_DestroyBlocker_m2607 ();
extern "C" void Dropdown_CreateDropdownList_m2608 ();
extern "C" void Dropdown_DestroyDropdownList_m2609 ();
extern "C" void Dropdown_CreateItem_m2610 ();
extern "C" void Dropdown_DestroyItem_m2611 ();
extern "C" void Dropdown_AddItem_m2612 ();
extern "C" void Dropdown_AlphaFadeList_m2613 ();
extern "C" void Dropdown_AlphaFadeList_m2614 ();
extern "C" void Dropdown_SetAlpha_m2615 ();
extern "C" void Dropdown_Hide_m2616 ();
extern "C" void Dropdown_DelayedDestroyDropdownList_m2617 ();
extern "C" void Dropdown_OnSelectItem_m2618 ();
extern "C" void FontData__ctor_m2619 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2620 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2621 ();
extern "C" void FontData_get_defaultFontData_m2622 ();
extern "C" void FontData_get_font_m2623 ();
extern "C" void FontData_set_font_m2624 ();
extern "C" void FontData_get_fontSize_m2625 ();
extern "C" void FontData_set_fontSize_m2626 ();
extern "C" void FontData_get_fontStyle_m2627 ();
extern "C" void FontData_set_fontStyle_m2628 ();
extern "C" void FontData_get_bestFit_m2629 ();
extern "C" void FontData_set_bestFit_m2630 ();
extern "C" void FontData_get_minSize_m2631 ();
extern "C" void FontData_set_minSize_m2632 ();
extern "C" void FontData_get_maxSize_m2633 ();
extern "C" void FontData_set_maxSize_m2634 ();
extern "C" void FontData_get_alignment_m2635 ();
extern "C" void FontData_set_alignment_m2636 ();
extern "C" void FontData_get_richText_m2637 ();
extern "C" void FontData_set_richText_m2638 ();
extern "C" void FontData_get_horizontalOverflow_m2639 ();
extern "C" void FontData_set_horizontalOverflow_m2640 ();
extern "C" void FontData_get_verticalOverflow_m2641 ();
extern "C" void FontData_set_verticalOverflow_m2642 ();
extern "C" void FontData_get_lineSpacing_m2643 ();
extern "C" void FontData_set_lineSpacing_m2644 ();
extern "C" void FontUpdateTracker__cctor_m2645 ();
extern "C" void FontUpdateTracker_TrackText_m2646 ();
extern "C" void FontUpdateTracker_RebuildForFont_m2647 ();
extern "C" void FontUpdateTracker_UntrackText_m2648 ();
extern "C" void Graphic__ctor_m2649 ();
extern "C" void Graphic__cctor_m2650 ();
extern "C" void Graphic_get_defaultGraphicMaterial_m2651 ();
extern "C" void Graphic_get_color_m2652 ();
extern "C" void Graphic_set_color_m2653 ();
extern "C" void Graphic_get_raycastTarget_m2654 ();
extern "C" void Graphic_set_raycastTarget_m2655 ();
extern "C" void Graphic_SetAllDirty_m2656 ();
extern "C" void Graphic_SetLayoutDirty_m2657 ();
extern "C" void Graphic_SetVerticesDirty_m2658 ();
extern "C" void Graphic_SetMaterialDirty_m2659 ();
extern "C" void Graphic_OnRectTransformDimensionsChange_m2660 ();
extern "C" void Graphic_OnBeforeTransformParentChanged_m2661 ();
extern "C" void Graphic_OnTransformParentChanged_m2662 ();
extern "C" void Graphic_get_depth_m2663 ();
extern "C" void Graphic_get_rectTransform_m2664 ();
extern "C" void Graphic_get_canvas_m2665 ();
extern "C" void Graphic_CacheCanvas_m2666 ();
extern "C" void Graphic_get_canvasRenderer_m2667 ();
extern "C" void Graphic_get_defaultMaterial_m2668 ();
extern "C" void Graphic_get_material_m2669 ();
extern "C" void Graphic_set_material_m2670 ();
extern "C" void Graphic_get_materialForRendering_m2671 ();
extern "C" void Graphic_get_mainTexture_m2672 ();
extern "C" void Graphic_OnEnable_m2673 ();
extern "C" void Graphic_OnDisable_m2674 ();
extern "C" void Graphic_OnCanvasHierarchyChanged_m2675 ();
extern "C" void Graphic_Rebuild_m2676 ();
extern "C" void Graphic_UpdateMaterial_m2677 ();
extern "C" void Graphic_UpdateGeometry_m2678 ();
extern "C" void Graphic_get_workerMesh_m2679 ();
extern "C" void Graphic_OnFillVBO_m2680 ();
extern "C" void Graphic_OnPopulateMesh_m2681 ();
extern "C" void Graphic_OnDidApplyAnimationProperties_m2682 ();
extern "C" void Graphic_SetNativeSize_m2683 ();
extern "C" void Graphic_Raycast_m2684 ();
extern "C" void Graphic_PixelAdjustPoint_m2685 ();
extern "C" void Graphic_GetPixelAdjustedRect_m2686 ();
extern "C" void Graphic_CrossFadeColor_m2687 ();
extern "C" void Graphic_CrossFadeColor_m2688 ();
extern "C" void Graphic_CreateColorFromAlpha_m2689 ();
extern "C" void Graphic_CrossFadeAlpha_m2690 ();
extern "C" void Graphic_RegisterDirtyLayoutCallback_m2691 ();
extern "C" void Graphic_UnregisterDirtyLayoutCallback_m2692 ();
extern "C" void Graphic_RegisterDirtyVerticesCallback_m2693 ();
extern "C" void Graphic_UnregisterDirtyVerticesCallback_m2694 ();
extern "C" void Graphic_RegisterDirtyMaterialCallback_m2695 ();
extern "C" void Graphic_UnregisterDirtyMaterialCallback_m2696 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m2697 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_get_transform_m2698 ();
extern "C" void GraphicRaycaster__ctor_m2699 ();
extern "C" void GraphicRaycaster__cctor_m2700 ();
extern "C" void GraphicRaycaster_get_sortOrderPriority_m2701 ();
extern "C" void GraphicRaycaster_get_renderOrderPriority_m2702 ();
extern "C" void GraphicRaycaster_get_ignoreReversedGraphics_m2703 ();
extern "C" void GraphicRaycaster_set_ignoreReversedGraphics_m2704 ();
extern "C" void GraphicRaycaster_get_blockingObjects_m2705 ();
extern "C" void GraphicRaycaster_set_blockingObjects_m2706 ();
extern "C" void GraphicRaycaster_get_canvas_m2707 ();
extern "C" void GraphicRaycaster_Raycast_m2708 ();
extern "C" void GraphicRaycaster_get_eventCamera_m2709 ();
extern "C" void GraphicRaycaster_Raycast_m2710 ();
extern "C" void GraphicRaycaster_U3CRaycastU3Em__5_m2711 ();
extern "C" void GraphicRegistry__ctor_m2712 ();
extern "C" void GraphicRegistry__cctor_m2713 ();
extern "C" void GraphicRegistry_get_instance_m2714 ();
extern "C" void GraphicRegistry_RegisterGraphicForCanvas_m2715 ();
extern "C" void GraphicRegistry_UnregisterGraphicForCanvas_m2716 ();
extern "C" void GraphicRegistry_GetGraphicsForCanvas_m2717 ();
extern "C" void Image__ctor_m2718 ();
extern "C" void Image__cctor_m2719 ();
extern "C" void Image_get_sprite_m2720 ();
extern "C" void Image_set_sprite_m2721 ();
extern "C" void Image_get_overrideSprite_m2722 ();
extern "C" void Image_set_overrideSprite_m2723 ();
extern "C" void Image_get_type_m2724 ();
extern "C" void Image_set_type_m2725 ();
extern "C" void Image_get_preserveAspect_m2726 ();
extern "C" void Image_set_preserveAspect_m2727 ();
extern "C" void Image_get_fillCenter_m2728 ();
extern "C" void Image_set_fillCenter_m2729 ();
extern "C" void Image_get_fillMethod_m2730 ();
extern "C" void Image_set_fillMethod_m2731 ();
extern "C" void Image_get_fillAmount_m2732 ();
extern "C" void Image_set_fillAmount_m2733 ();
extern "C" void Image_get_fillClockwise_m2734 ();
extern "C" void Image_set_fillClockwise_m2735 ();
extern "C" void Image_get_fillOrigin_m2736 ();
extern "C" void Image_set_fillOrigin_m2737 ();
extern "C" void Image_get_eventAlphaThreshold_m2738 ();
extern "C" void Image_set_eventAlphaThreshold_m2739 ();
extern "C" void Image_get_mainTexture_m2740 ();
extern "C" void Image_get_hasBorder_m2741 ();
extern "C" void Image_get_pixelsPerUnit_m2742 ();
extern "C" void Image_OnBeforeSerialize_m2743 ();
extern "C" void Image_OnAfterDeserialize_m2744 ();
extern "C" void Image_GetDrawingDimensions_m2745 ();
extern "C" void Image_SetNativeSize_m2746 ();
extern "C" void Image_OnPopulateMesh_m2747 ();
extern "C" void Image_GenerateSimpleSprite_m2748 ();
extern "C" void Image_GenerateSlicedSprite_m2749 ();
extern "C" void Image_GenerateTiledSprite_m2750 ();
extern "C" void Image_AddQuad_m2751 ();
extern "C" void Image_AddQuad_m2752 ();
extern "C" void Image_GetAdjustedBorders_m2753 ();
extern "C" void Image_GenerateFilledSprite_m2754 ();
extern "C" void Image_RadialCut_m2755 ();
extern "C" void Image_RadialCut_m2756 ();
extern "C" void Image_CalculateLayoutInputHorizontal_m2757 ();
extern "C" void Image_CalculateLayoutInputVertical_m2758 ();
extern "C" void Image_get_minWidth_m2759 ();
extern "C" void Image_get_preferredWidth_m2760 ();
extern "C" void Image_get_flexibleWidth_m2761 ();
extern "C" void Image_get_minHeight_m2762 ();
extern "C" void Image_get_preferredHeight_m2763 ();
extern "C" void Image_get_flexibleHeight_m2764 ();
extern "C" void Image_get_layoutPriority_m2765 ();
extern "C" void Image_IsRaycastLocationValid_m2766 ();
extern "C" void Image_MapCoordinate_m2767 ();
extern "C" void SubmitEvent__ctor_m2768 ();
extern "C" void OnChangeEvent__ctor_m2769 ();
extern "C" void OnValidateInput__ctor_m2770 ();
extern "C" void OnValidateInput_Invoke_m2771 ();
extern "C" void OnValidateInput_BeginInvoke_m2772 ();
extern "C" void OnValidateInput_EndInvoke_m2773 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3__ctor_m2774 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2775 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2776 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_MoveNext_m2777 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_Dispose_m2778 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_Reset_m2779 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4__ctor_m2780 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2781 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2782 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_MoveNext_m2783 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Dispose_m2784 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Reset_m2785 ();
extern "C" void InputField__ctor_m2786 ();
extern "C" void InputField__cctor_m2787 ();
extern "C" void InputField_get_mesh_m2788 ();
extern "C" void InputField_get_cachedInputTextGenerator_m2789 ();
extern "C" void InputField_set_shouldHideMobileInput_m2790 ();
extern "C" void InputField_get_shouldHideMobileInput_m2791 ();
extern "C" void InputField_get_text_m2792 ();
extern "C" void InputField_set_text_m2793 ();
extern "C" void InputField_get_isFocused_m2794 ();
extern "C" void InputField_get_caretBlinkRate_m2795 ();
extern "C" void InputField_set_caretBlinkRate_m2796 ();
extern "C" void InputField_get_textComponent_m2797 ();
extern "C" void InputField_set_textComponent_m2798 ();
extern "C" void InputField_get_placeholder_m2799 ();
extern "C" void InputField_set_placeholder_m2800 ();
extern "C" void InputField_get_selectionColor_m2801 ();
extern "C" void InputField_set_selectionColor_m2802 ();
extern "C" void InputField_get_onEndEdit_m2803 ();
extern "C" void InputField_set_onEndEdit_m2804 ();
extern "C" void InputField_get_onValueChange_m2805 ();
extern "C" void InputField_set_onValueChange_m2806 ();
extern "C" void InputField_get_onValidateInput_m2807 ();
extern "C" void InputField_set_onValidateInput_m2808 ();
extern "C" void InputField_get_characterLimit_m2809 ();
extern "C" void InputField_set_characterLimit_m2810 ();
extern "C" void InputField_get_contentType_m2811 ();
extern "C" void InputField_set_contentType_m2812 ();
extern "C" void InputField_get_lineType_m2813 ();
extern "C" void InputField_set_lineType_m2814 ();
extern "C" void InputField_get_inputType_m2815 ();
extern "C" void InputField_set_inputType_m2816 ();
extern "C" void InputField_get_keyboardType_m2817 ();
extern "C" void InputField_set_keyboardType_m2818 ();
extern "C" void InputField_get_characterValidation_m2819 ();
extern "C" void InputField_set_characterValidation_m2820 ();
extern "C" void InputField_get_multiLine_m2821 ();
extern "C" void InputField_get_asteriskChar_m2822 ();
extern "C" void InputField_set_asteriskChar_m2823 ();
extern "C" void InputField_get_wasCanceled_m2824 ();
extern "C" void InputField_ClampPos_m2825 ();
extern "C" void InputField_get_caretPositionInternal_m2826 ();
extern "C" void InputField_set_caretPositionInternal_m2827 ();
extern "C" void InputField_get_caretSelectPositionInternal_m2828 ();
extern "C" void InputField_set_caretSelectPositionInternal_m2829 ();
extern "C" void InputField_get_hasSelection_m2830 ();
extern "C" void InputField_get_caretPosition_m2831 ();
extern "C" void InputField_set_caretPosition_m2832 ();
extern "C" void InputField_get_selectionAnchorPosition_m2833 ();
extern "C" void InputField_set_selectionAnchorPosition_m2834 ();
extern "C" void InputField_get_selectionFocusPosition_m2835 ();
extern "C" void InputField_set_selectionFocusPosition_m2836 ();
extern "C" void InputField_OnEnable_m2837 ();
extern "C" void InputField_OnDisable_m2838 ();
extern "C" void InputField_CaretBlink_m2839 ();
extern "C" void InputField_SetCaretVisible_m2840 ();
extern "C" void InputField_SetCaretActive_m2841 ();
extern "C" void InputField_OnFocus_m2842 ();
extern "C" void InputField_SelectAll_m2843 ();
extern "C" void InputField_MoveTextEnd_m2844 ();
extern "C" void InputField_MoveTextStart_m2845 ();
extern "C" void InputField_get_clipboard_m2846 ();
extern "C" void InputField_set_clipboard_m2847 ();
extern "C" void InputField_InPlaceEditing_m2848 ();
extern "C" void InputField_LateUpdate_m2849 ();
extern "C" void InputField_ScreenToLocal_m2850 ();
extern "C" void InputField_GetUnclampedCharacterLineFromPosition_m2851 ();
extern "C" void InputField_GetCharacterIndexFromPosition_m2852 ();
extern "C" void InputField_MayDrag_m2853 ();
extern "C" void InputField_OnBeginDrag_m2854 ();
extern "C" void InputField_OnDrag_m2855 ();
extern "C" void InputField_MouseDragOutsideRect_m2856 ();
extern "C" void InputField_OnEndDrag_m2857 ();
extern "C" void InputField_OnPointerDown_m2858 ();
extern "C" void InputField_KeyPressed_m2859 ();
extern "C" void InputField_IsValidChar_m2860 ();
extern "C" void InputField_ProcessEvent_m2861 ();
extern "C" void InputField_OnUpdateSelected_m2862 ();
extern "C" void InputField_GetSelectedString_m2863 ();
extern "C" void InputField_FindtNextWordBegin_m2864 ();
extern "C" void InputField_MoveRight_m2865 ();
extern "C" void InputField_FindtPrevWordBegin_m2866 ();
extern "C" void InputField_MoveLeft_m2867 ();
extern "C" void InputField_DetermineCharacterLine_m2868 ();
extern "C" void InputField_LineUpCharacterPosition_m2869 ();
extern "C" void InputField_LineDownCharacterPosition_m2870 ();
extern "C" void InputField_MoveDown_m2871 ();
extern "C" void InputField_MoveDown_m2872 ();
extern "C" void InputField_MoveUp_m2873 ();
extern "C" void InputField_MoveUp_m2874 ();
extern "C" void InputField_Delete_m2875 ();
extern "C" void InputField_ForwardSpace_m2876 ();
extern "C" void InputField_Backspace_m2877 ();
extern "C" void InputField_Insert_m2878 ();
extern "C" void InputField_SendOnValueChangedAndUpdateLabel_m2879 ();
extern "C" void InputField_SendOnValueChanged_m2880 ();
extern "C" void InputField_SendOnSubmit_m2881 ();
extern "C" void InputField_Append_m2882 ();
extern "C" void InputField_Append_m2883 ();
extern "C" void InputField_UpdateLabel_m2884 ();
extern "C" void InputField_IsSelectionVisible_m2885 ();
extern "C" void InputField_GetLineStartPosition_m2886 ();
extern "C" void InputField_GetLineEndPosition_m2887 ();
extern "C" void InputField_SetDrawRangeToContainCaretPosition_m2888 ();
extern "C" void InputField_MarkGeometryAsDirty_m2889 ();
extern "C" void InputField_Rebuild_m2890 ();
extern "C" void InputField_UpdateGeometry_m2891 ();
extern "C" void InputField_AssignPositioningIfNeeded_m2892 ();
extern "C" void InputField_OnFillVBO_m2893 ();
extern "C" void InputField_GenerateCursor_m2894 ();
extern "C" void InputField_CreateCursorVerts_m2895 ();
extern "C" void InputField_SumLineHeights_m2896 ();
extern "C" void InputField_GenerateHightlight_m2897 ();
extern "C" void InputField_Validate_m2898 ();
extern "C" void InputField_ActivateInputField_m2899 ();
extern "C" void InputField_ActivateInputFieldInternal_m2900 ();
extern "C" void InputField_OnSelect_m2901 ();
extern "C" void InputField_OnPointerClick_m2902 ();
extern "C" void InputField_DeactivateInputField_m2903 ();
extern "C" void InputField_OnDeselect_m2904 ();
extern "C" void InputField_OnSubmit_m2905 ();
extern "C" void InputField_EnforceContentType_m2906 ();
extern "C" void InputField_SetToCustomIfContentTypeIsNot_m2907 ();
extern "C" void InputField_SetToCustom_m2908 ();
extern "C" void InputField_DoStateTransition_m2909 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m2910 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_get_transform_m2911 ();
extern "C" void Mask__ctor_m2912 ();
extern "C" void Mask_get_rectTransform_m2913 ();
extern "C" void Mask_get_showMaskGraphic_m2914 ();
extern "C" void Mask_set_showMaskGraphic_m2915 ();
extern "C" void Mask_get_graphic_m2916 ();
extern "C" void Mask_MaskEnabled_m2917 ();
extern "C" void Mask_OnSiblingGraphicEnabledDisabled_m2918 ();
extern "C" void Mask_OnEnable_m2919 ();
extern "C" void Mask_OnDisable_m2920 ();
extern "C" void Mask_IsRaycastLocationValid_m2921 ();
extern "C" void Mask_GetModifiedMaterial_m2922 ();
extern "C" void CullStateChangedEvent__ctor_m2923 ();
extern "C" void MaskableGraphic__ctor_m2924 ();
extern "C" void MaskableGraphic_get_onCullStateChanged_m2925 ();
extern "C" void MaskableGraphic_set_onCullStateChanged_m2926 ();
extern "C" void MaskableGraphic_get_maskable_m2927 ();
extern "C" void MaskableGraphic_set_maskable_m2928 ();
extern "C" void MaskableGraphic_GetModifiedMaterial_m2929 ();
extern "C" void MaskableGraphic_Cull_m2930 ();
extern "C" void MaskableGraphic_SetClipRect_m2931 ();
extern "C" void MaskableGraphic_OnEnable_m2932 ();
extern "C" void MaskableGraphic_OnDisable_m2933 ();
extern "C" void MaskableGraphic_OnTransformParentChanged_m2934 ();
extern "C" void MaskableGraphic_ParentMaskStateChanged_m2935 ();
extern "C" void MaskableGraphic_OnCanvasHierarchyChanged_m2936 ();
extern "C" void MaskableGraphic_get_canvasRect_m2937 ();
extern "C" void MaskableGraphic_UpdateClipParent_m2938 ();
extern "C" void MaskableGraphic_RecalculateClipping_m2939 ();
extern "C" void MaskableGraphic_RecalculateMasking_m2940 ();
extern "C" void MaskableGraphic_UnityEngine_UI_IClippable_get_rectTransform_m2941 ();
extern "C" void MaskUtilities_Notify2DMaskStateChanged_m2942 ();
extern "C" void MaskUtilities_NotifyStencilStateChanged_m2943 ();
extern "C" void MaskUtilities_FindRootSortOverrideCanvas_m2944 ();
extern "C" void MaskUtilities_GetStencilDepth_m2945 ();
extern "C" void MaskUtilities_GetRectMaskForClippable_m2946 ();
extern "C" void MaskUtilities_GetRectMasksForClip_m2947 ();
extern "C" void Misc_DestroyImmediate_m2948 ();
extern "C" void Navigation_get_mode_m2949 ();
extern "C" void Navigation_set_mode_m2950 ();
extern "C" void Navigation_get_selectOnUp_m2951 ();
extern "C" void Navigation_set_selectOnUp_m2952 ();
extern "C" void Navigation_get_selectOnDown_m2953 ();
extern "C" void Navigation_set_selectOnDown_m2954 ();
extern "C" void Navigation_get_selectOnLeft_m2955 ();
extern "C" void Navigation_set_selectOnLeft_m2956 ();
extern "C" void Navigation_get_selectOnRight_m2957 ();
extern "C" void Navigation_set_selectOnRight_m2958 ();
extern "C" void Navigation_get_defaultNavigation_m2959 ();
extern "C" void RawImage__ctor_m2960 ();
extern "C" void RawImage_get_mainTexture_m2961 ();
extern "C" void RawImage_get_texture_m2962 ();
extern "C" void RawImage_set_texture_m2963 ();
extern "C" void RawImage_get_uvRect_m2964 ();
extern "C" void RawImage_set_uvRect_m2965 ();
extern "C" void RawImage_SetNativeSize_m2966 ();
extern "C" void RawImage_OnPopulateMesh_m2967 ();
extern "C" void RectMask2D__ctor_m2968 ();
extern "C" void RectMask2D_get_canvasRect_m2969 ();
extern "C" void RectMask2D_get_rectTransform_m2970 ();
extern "C" void RectMask2D_OnEnable_m2971 ();
extern "C" void RectMask2D_OnDisable_m2972 ();
extern "C" void RectMask2D_IsRaycastLocationValid_m2973 ();
extern "C" void RectMask2D_PerformClipping_m2974 ();
extern "C" void RectMask2D_AddClippable_m2975 ();
extern "C" void RectMask2D_RemoveClippable_m2976 ();
extern "C" void RectMask2D_OnTransformParentChanged_m2977 ();
extern "C" void RectMask2D_OnCanvasHierarchyChanged_m2978 ();
extern "C" void ScrollEvent__ctor_m2979 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5__ctor_m2980 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2981 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2982 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_MoveNext_m2983 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_Dispose_m2984 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_Reset_m2985 ();
extern "C" void Scrollbar__ctor_m2986 ();
extern "C" void Scrollbar_get_handleRect_m2987 ();
extern "C" void Scrollbar_set_handleRect_m2988 ();
extern "C" void Scrollbar_get_direction_m2989 ();
extern "C" void Scrollbar_set_direction_m2990 ();
extern "C" void Scrollbar_get_value_m2991 ();
extern "C" void Scrollbar_set_value_m2992 ();
extern "C" void Scrollbar_get_size_m2993 ();
extern "C" void Scrollbar_set_size_m2994 ();
extern "C" void Scrollbar_get_numberOfSteps_m2995 ();
extern "C" void Scrollbar_set_numberOfSteps_m2996 ();
extern "C" void Scrollbar_get_onValueChanged_m2997 ();
extern "C" void Scrollbar_set_onValueChanged_m2998 ();
extern "C" void Scrollbar_get_stepSize_m2999 ();
extern "C" void Scrollbar_Rebuild_m3000 ();
extern "C" void Scrollbar_OnEnable_m3001 ();
extern "C" void Scrollbar_OnDisable_m3002 ();
extern "C" void Scrollbar_UpdateCachedReferences_m3003 ();
extern "C" void Scrollbar_Set_m3004 ();
extern "C" void Scrollbar_Set_m3005 ();
extern "C" void Scrollbar_OnRectTransformDimensionsChange_m3006 ();
extern "C" void Scrollbar_get_axis_m3007 ();
extern "C" void Scrollbar_get_reverseValue_m3008 ();
extern "C" void Scrollbar_UpdateVisuals_m3009 ();
extern "C" void Scrollbar_UpdateDrag_m3010 ();
extern "C" void Scrollbar_MayDrag_m3011 ();
extern "C" void Scrollbar_OnBeginDrag_m3012 ();
extern "C" void Scrollbar_OnDrag_m3013 ();
extern "C" void Scrollbar_OnPointerDown_m3014 ();
extern "C" void Scrollbar_ClickRepeat_m3015 ();
extern "C" void Scrollbar_OnPointerUp_m3016 ();
extern "C" void Scrollbar_OnMove_m3017 ();
extern "C" void Scrollbar_FindSelectableOnLeft_m3018 ();
extern "C" void Scrollbar_FindSelectableOnRight_m3019 ();
extern "C" void Scrollbar_FindSelectableOnUp_m3020 ();
extern "C" void Scrollbar_FindSelectableOnDown_m3021 ();
extern "C" void Scrollbar_OnInitializePotentialDrag_m3022 ();
extern "C" void Scrollbar_SetDirection_m3023 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m3024 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m3025 ();
extern "C" void ScrollRectEvent__ctor_m3026 ();
extern "C" void ScrollRect__ctor_m3027 ();
extern "C" void ScrollRect_get_content_m3028 ();
extern "C" void ScrollRect_set_content_m3029 ();
extern "C" void ScrollRect_get_horizontal_m3030 ();
extern "C" void ScrollRect_set_horizontal_m3031 ();
extern "C" void ScrollRect_get_vertical_m3032 ();
extern "C" void ScrollRect_set_vertical_m3033 ();
extern "C" void ScrollRect_get_movementType_m3034 ();
extern "C" void ScrollRect_set_movementType_m3035 ();
extern "C" void ScrollRect_get_elasticity_m3036 ();
extern "C" void ScrollRect_set_elasticity_m3037 ();
extern "C" void ScrollRect_get_inertia_m3038 ();
extern "C" void ScrollRect_set_inertia_m3039 ();
extern "C" void ScrollRect_get_decelerationRate_m3040 ();
extern "C" void ScrollRect_set_decelerationRate_m3041 ();
extern "C" void ScrollRect_get_scrollSensitivity_m3042 ();
extern "C" void ScrollRect_set_scrollSensitivity_m3043 ();
extern "C" void ScrollRect_get_viewport_m3044 ();
extern "C" void ScrollRect_set_viewport_m3045 ();
extern "C" void ScrollRect_get_horizontalScrollbar_m3046 ();
extern "C" void ScrollRect_set_horizontalScrollbar_m3047 ();
extern "C" void ScrollRect_get_verticalScrollbar_m3048 ();
extern "C" void ScrollRect_set_verticalScrollbar_m3049 ();
extern "C" void ScrollRect_get_horizontalScrollbarVisibility_m3050 ();
extern "C" void ScrollRect_set_horizontalScrollbarVisibility_m3051 ();
extern "C" void ScrollRect_get_verticalScrollbarVisibility_m3052 ();
extern "C" void ScrollRect_set_verticalScrollbarVisibility_m3053 ();
extern "C" void ScrollRect_get_horizontalScrollbarSpacing_m3054 ();
extern "C" void ScrollRect_set_horizontalScrollbarSpacing_m3055 ();
extern "C" void ScrollRect_get_verticalScrollbarSpacing_m3056 ();
extern "C" void ScrollRect_set_verticalScrollbarSpacing_m3057 ();
extern "C" void ScrollRect_get_onValueChanged_m3058 ();
extern "C" void ScrollRect_set_onValueChanged_m3059 ();
extern "C" void ScrollRect_get_viewRect_m3060 ();
extern "C" void ScrollRect_get_velocity_m3061 ();
extern "C" void ScrollRect_set_velocity_m3062 ();
extern "C" void ScrollRect_get_rectTransform_m3063 ();
extern "C" void ScrollRect_Rebuild_m3064 ();
extern "C" void ScrollRect_UpdateCachedData_m3065 ();
extern "C" void ScrollRect_OnEnable_m3066 ();
extern "C" void ScrollRect_OnDisable_m3067 ();
extern "C" void ScrollRect_IsActive_m3068 ();
extern "C" void ScrollRect_EnsureLayoutHasRebuilt_m3069 ();
extern "C" void ScrollRect_StopMovement_m3070 ();
extern "C" void ScrollRect_OnScroll_m3071 ();
extern "C" void ScrollRect_OnInitializePotentialDrag_m3072 ();
extern "C" void ScrollRect_OnBeginDrag_m3073 ();
extern "C" void ScrollRect_OnEndDrag_m3074 ();
extern "C" void ScrollRect_OnDrag_m3075 ();
extern "C" void ScrollRect_SetContentAnchoredPosition_m3076 ();
extern "C" void ScrollRect_LateUpdate_m3077 ();
extern "C" void ScrollRect_UpdatePrevData_m3078 ();
extern "C" void ScrollRect_UpdateScrollbars_m3079 ();
extern "C" void ScrollRect_get_normalizedPosition_m3080 ();
extern "C" void ScrollRect_set_normalizedPosition_m3081 ();
extern "C" void ScrollRect_get_horizontalNormalizedPosition_m3082 ();
extern "C" void ScrollRect_set_horizontalNormalizedPosition_m3083 ();
extern "C" void ScrollRect_get_verticalNormalizedPosition_m3084 ();
extern "C" void ScrollRect_set_verticalNormalizedPosition_m3085 ();
extern "C" void ScrollRect_SetHorizontalNormalizedPosition_m3086 ();
extern "C" void ScrollRect_SetVerticalNormalizedPosition_m3087 ();
extern "C" void ScrollRect_SetNormalizedPosition_m3088 ();
extern "C" void ScrollRect_RubberDelta_m3089 ();
extern "C" void ScrollRect_OnRectTransformDimensionsChange_m3090 ();
extern "C" void ScrollRect_get_hScrollingNeeded_m3091 ();
extern "C" void ScrollRect_get_vScrollingNeeded_m3092 ();
extern "C" void ScrollRect_CalculateLayoutInputHorizontal_m3093 ();
extern "C" void ScrollRect_CalculateLayoutInputVertical_m3094 ();
extern "C" void ScrollRect_get_minWidth_m3095 ();
extern "C" void ScrollRect_get_preferredWidth_m3096 ();
extern "C" void ScrollRect_get_flexibleWidth_m3097 ();
extern "C" void ScrollRect_get_minHeight_m3098 ();
extern "C" void ScrollRect_get_preferredHeight_m3099 ();
extern "C" void ScrollRect_get_flexibleHeight_m3100 ();
extern "C" void ScrollRect_get_layoutPriority_m3101 ();
extern "C" void ScrollRect_SetLayoutHorizontal_m3102 ();
extern "C" void ScrollRect_SetLayoutVertical_m3103 ();
extern "C" void ScrollRect_UpdateScrollbarVisibility_m3104 ();
extern "C" void ScrollRect_UpdateScrollbarLayout_m3105 ();
extern "C" void ScrollRect_UpdateBounds_m3106 ();
extern "C" void ScrollRect_GetBounds_m3107 ();
extern "C" void ScrollRect_CalculateOffset_m3108 ();
extern "C" void ScrollRect_SetDirty_m3109 ();
extern "C" void ScrollRect_SetDirtyCaching_m3110 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m3111 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m3112 ();
extern "C" void Selectable__ctor_m3113 ();
extern "C" void Selectable__cctor_m3114 ();
extern "C" void Selectable_get_allSelectables_m3115 ();
extern "C" void Selectable_get_navigation_m3116 ();
extern "C" void Selectable_set_navigation_m3117 ();
extern "C" void Selectable_get_transition_m3118 ();
extern "C" void Selectable_set_transition_m3119 ();
extern "C" void Selectable_get_colors_m3120 ();
extern "C" void Selectable_set_colors_m3121 ();
extern "C" void Selectable_get_spriteState_m3122 ();
extern "C" void Selectable_set_spriteState_m3123 ();
extern "C" void Selectable_get_animationTriggers_m3124 ();
extern "C" void Selectable_set_animationTriggers_m3125 ();
extern "C" void Selectable_get_targetGraphic_m3126 ();
extern "C" void Selectable_set_targetGraphic_m3127 ();
extern "C" void Selectable_get_interactable_m3128 ();
extern "C" void Selectable_set_interactable_m3129 ();
extern "C" void Selectable_get_isPointerInside_m3130 ();
extern "C" void Selectable_set_isPointerInside_m3131 ();
extern "C" void Selectable_get_isPointerDown_m3132 ();
extern "C" void Selectable_set_isPointerDown_m3133 ();
extern "C" void Selectable_get_hasSelection_m3134 ();
extern "C" void Selectable_set_hasSelection_m3135 ();
extern "C" void Selectable_get_image_m3136 ();
extern "C" void Selectable_set_image_m3137 ();
extern "C" void Selectable_get_animator_m3138 ();
extern "C" void Selectable_Awake_m3139 ();
extern "C" void Selectable_OnCanvasGroupChanged_m3140 ();
extern "C" void Selectable_IsInteractable_m3141 ();
extern "C" void Selectable_OnDidApplyAnimationProperties_m3142 ();
extern "C" void Selectable_OnEnable_m3143 ();
extern "C" void Selectable_OnSetProperty_m3144 ();
extern "C" void Selectable_OnDisable_m3145 ();
extern "C" void Selectable_get_currentSelectionState_m3146 ();
extern "C" void Selectable_InstantClearState_m3147 ();
extern "C" void Selectable_DoStateTransition_m3148 ();
extern "C" void Selectable_FindSelectable_m3149 ();
extern "C" void Selectable_GetPointOnRectEdge_m3150 ();
extern "C" void Selectable_Navigate_m3151 ();
extern "C" void Selectable_FindSelectableOnLeft_m3152 ();
extern "C" void Selectable_FindSelectableOnRight_m3153 ();
extern "C" void Selectable_FindSelectableOnUp_m3154 ();
extern "C" void Selectable_FindSelectableOnDown_m3155 ();
extern "C" void Selectable_OnMove_m3156 ();
extern "C" void Selectable_StartColorTween_m3157 ();
extern "C" void Selectable_DoSpriteSwap_m3158 ();
extern "C" void Selectable_TriggerAnimation_m3159 ();
extern "C" void Selectable_IsHighlighted_m3160 ();
extern "C" void Selectable_IsPressed_m3161 ();
extern "C" void Selectable_IsPressed_m3162 ();
extern "C" void Selectable_UpdateSelectionState_m3163 ();
extern "C" void Selectable_EvaluateAndTransitionToSelectionState_m3164 ();
extern "C" void Selectable_InternalEvaluateAndTransitionToSelectionState_m3165 ();
extern "C" void Selectable_OnPointerDown_m3166 ();
extern "C" void Selectable_OnPointerUp_m3167 ();
extern "C" void Selectable_OnPointerEnter_m3168 ();
extern "C" void Selectable_OnPointerExit_m3169 ();
extern "C" void Selectable_OnSelect_m3170 ();
extern "C" void Selectable_OnDeselect_m3171 ();
extern "C" void Selectable_Select_m3172 ();
extern "C" void SetPropertyUtility_SetColor_m3173 ();
extern "C" void SliderEvent__ctor_m3174 ();
extern "C" void Slider__ctor_m3175 ();
extern "C" void Slider_get_fillRect_m3176 ();
extern "C" void Slider_set_fillRect_m3177 ();
extern "C" void Slider_get_handleRect_m3178 ();
extern "C" void Slider_set_handleRect_m3179 ();
extern "C" void Slider_get_direction_m3180 ();
extern "C" void Slider_set_direction_m3181 ();
extern "C" void Slider_get_minValue_m3182 ();
extern "C" void Slider_set_minValue_m3183 ();
extern "C" void Slider_get_maxValue_m3184 ();
extern "C" void Slider_set_maxValue_m3185 ();
extern "C" void Slider_get_wholeNumbers_m3186 ();
extern "C" void Slider_set_wholeNumbers_m3187 ();
extern "C" void Slider_get_value_m3188 ();
extern "C" void Slider_set_value_m3189 ();
extern "C" void Slider_get_normalizedValue_m3190 ();
extern "C" void Slider_set_normalizedValue_m3191 ();
extern "C" void Slider_get_onValueChanged_m3192 ();
extern "C" void Slider_set_onValueChanged_m3193 ();
extern "C" void Slider_get_stepSize_m3194 ();
extern "C" void Slider_Rebuild_m3195 ();
extern "C" void Slider_OnEnable_m3196 ();
extern "C" void Slider_OnDisable_m3197 ();
extern "C" void Slider_OnDidApplyAnimationProperties_m3198 ();
extern "C" void Slider_UpdateCachedReferences_m3199 ();
extern "C" void Slider_ClampValue_m3200 ();
extern "C" void Slider_Set_m3201 ();
extern "C" void Slider_Set_m3202 ();
extern "C" void Slider_OnRectTransformDimensionsChange_m3203 ();
extern "C" void Slider_get_axis_m3204 ();
extern "C" void Slider_get_reverseValue_m3205 ();
extern "C" void Slider_UpdateVisuals_m3206 ();
extern "C" void Slider_UpdateDrag_m3207 ();
extern "C" void Slider_MayDrag_m3208 ();
extern "C" void Slider_OnPointerDown_m3209 ();
extern "C" void Slider_OnDrag_m3210 ();
extern "C" void Slider_OnMove_m3211 ();
extern "C" void Slider_FindSelectableOnLeft_m3212 ();
extern "C" void Slider_FindSelectableOnRight_m3213 ();
extern "C" void Slider_FindSelectableOnUp_m3214 ();
extern "C" void Slider_FindSelectableOnDown_m3215 ();
extern "C" void Slider_OnInitializePotentialDrag_m3216 ();
extern "C" void Slider_SetDirection_m3217 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m3218 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_get_transform_m3219 ();
extern "C" void SpriteState_get_highlightedSprite_m3220 ();
extern "C" void SpriteState_get_pressedSprite_m3221 ();
extern "C" void SpriteState_get_disabledSprite_m3222 ();
extern "C" void MatEntry__ctor_m3223 ();
extern "C" void StencilMaterial__cctor_m3224 ();
extern "C" void StencilMaterial_Add_m3225 ();
extern "C" void StencilMaterial_Add_m3226 ();
extern "C" void StencilMaterial_Remove_m3227 ();
extern "C" void Text__ctor_m3228 ();
extern "C" void Text__cctor_m3229 ();
extern "C" void Text_get_cachedTextGenerator_m3230 ();
extern "C" void Text_get_cachedTextGeneratorForLayout_m3231 ();
extern "C" void Text_get_mainTexture_m3232 ();
extern "C" void Text_FontTextureChanged_m3233 ();
extern "C" void Text_get_font_m3234 ();
extern "C" void Text_set_font_m3235 ();
extern "C" void Text_get_text_m3236 ();
extern "C" void Text_set_text_m3237 ();
extern "C" void Text_get_supportRichText_m3238 ();
extern "C" void Text_set_supportRichText_m3239 ();
extern "C" void Text_get_resizeTextForBestFit_m3240 ();
extern "C" void Text_set_resizeTextForBestFit_m3241 ();
extern "C" void Text_get_resizeTextMinSize_m3242 ();
extern "C" void Text_set_resizeTextMinSize_m3243 ();
extern "C" void Text_get_resizeTextMaxSize_m3244 ();
extern "C" void Text_set_resizeTextMaxSize_m3245 ();
extern "C" void Text_get_alignment_m3246 ();
extern "C" void Text_set_alignment_m3247 ();
extern "C" void Text_get_fontSize_m3248 ();
extern "C" void Text_set_fontSize_m3249 ();
extern "C" void Text_get_horizontalOverflow_m3250 ();
extern "C" void Text_set_horizontalOverflow_m3251 ();
extern "C" void Text_get_verticalOverflow_m3252 ();
extern "C" void Text_set_verticalOverflow_m3253 ();
extern "C" void Text_get_lineSpacing_m3254 ();
extern "C" void Text_set_lineSpacing_m3255 ();
extern "C" void Text_get_fontStyle_m3256 ();
extern "C" void Text_set_fontStyle_m3257 ();
extern "C" void Text_get_pixelsPerUnit_m3258 ();
extern "C" void Text_OnEnable_m3259 ();
extern "C" void Text_OnDisable_m3260 ();
extern "C" void Text_UpdateGeometry_m3261 ();
extern "C" void Text_GetGenerationSettings_m3262 ();
extern "C" void Text_GetTextAnchorPivot_m3263 ();
extern "C" void Text_OnPopulateMesh_m3264 ();
extern "C" void Text_CalculateLayoutInputHorizontal_m3265 ();
extern "C" void Text_CalculateLayoutInputVertical_m3266 ();
extern "C" void Text_get_minWidth_m3267 ();
extern "C" void Text_get_preferredWidth_m3268 ();
extern "C" void Text_get_flexibleWidth_m3269 ();
extern "C" void Text_get_minHeight_m3270 ();
extern "C" void Text_get_preferredHeight_m3271 ();
extern "C" void Text_get_flexibleHeight_m3272 ();
extern "C" void Text_get_layoutPriority_m3273 ();
extern "C" void ToggleEvent__ctor_m3274 ();
extern "C" void Toggle__ctor_m3275 ();
extern "C" void Toggle_get_group_m3276 ();
extern "C" void Toggle_set_group_m3277 ();
extern "C" void Toggle_Rebuild_m3278 ();
extern "C" void Toggle_OnEnable_m3279 ();
extern "C" void Toggle_OnDisable_m3280 ();
extern "C" void Toggle_OnDidApplyAnimationProperties_m3281 ();
extern "C" void Toggle_SetToggleGroup_m3282 ();
extern "C" void Toggle_get_isOn_m3283 ();
extern "C" void Toggle_set_isOn_m3284 ();
extern "C" void Toggle_Set_m3285 ();
extern "C" void Toggle_Set_m3286 ();
extern "C" void Toggle_PlayEffect_m3287 ();
extern "C" void Toggle_Start_m3288 ();
extern "C" void Toggle_InternalToggle_m3289 ();
extern "C" void Toggle_OnPointerClick_m3290 ();
extern "C" void Toggle_OnSubmit_m3291 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m3292 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_get_transform_m3293 ();
extern "C" void ToggleGroup__ctor_m3294 ();
extern "C" void ToggleGroup_get_allowSwitchOff_m3295 ();
extern "C" void ToggleGroup_set_allowSwitchOff_m3296 ();
extern "C" void ToggleGroup_ValidateToggleIsInGroup_m3297 ();
extern "C" void ToggleGroup_NotifyToggleOn_m3298 ();
extern "C" void ToggleGroup_UnregisterToggle_m3299 ();
extern "C" void ToggleGroup_RegisterToggle_m3300 ();
extern "C" void ToggleGroup_AnyTogglesOn_m3301 ();
extern "C" void ToggleGroup_ActiveToggles_m3302 ();
extern "C" void ToggleGroup_SetAllTogglesOff_m3303 ();
extern "C" void ToggleGroup_U3CAnyTogglesOnU3Em__6_m3304 ();
extern "C" void ToggleGroup_U3CActiveTogglesU3Em__7_m3305 ();
extern "C" void ClipperRegistry__ctor_m3306 ();
extern "C" void ClipperRegistry_get_instance_m3307 ();
extern "C" void ClipperRegistry_Cull_m3308 ();
extern "C" void ClipperRegistry_Register_m3309 ();
extern "C" void ClipperRegistry_Unregister_m3310 ();
extern "C" void Clipping_FindCullAndClipWorldRect_m3311 ();
extern "C" void Clipping_RectIntersect_m3312 ();
extern "C" void RectangularVertexClipper__ctor_m3313 ();
extern "C" void RectangularVertexClipper_GetCanvasRect_m3314 ();
extern "C" void AspectRatioFitter__ctor_m3315 ();
extern "C" void AspectRatioFitter_get_aspectMode_m3316 ();
extern "C" void AspectRatioFitter_set_aspectMode_m3317 ();
extern "C" void AspectRatioFitter_get_aspectRatio_m3318 ();
extern "C" void AspectRatioFitter_set_aspectRatio_m3319 ();
extern "C" void AspectRatioFitter_get_rectTransform_m3320 ();
extern "C" void AspectRatioFitter_OnEnable_m3321 ();
extern "C" void AspectRatioFitter_OnDisable_m3322 ();
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m3323 ();
extern "C" void AspectRatioFitter_UpdateRect_m3324 ();
extern "C" void AspectRatioFitter_GetSizeDeltaToProduceSize_m3325 ();
extern "C" void AspectRatioFitter_GetParentSize_m3326 ();
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m3327 ();
extern "C" void AspectRatioFitter_SetLayoutVertical_m3328 ();
extern "C" void AspectRatioFitter_SetDirty_m3329 ();
extern "C" void CanvasScaler__ctor_m3330 ();
extern "C" void CanvasScaler_get_uiScaleMode_m3331 ();
extern "C" void CanvasScaler_set_uiScaleMode_m3332 ();
extern "C" void CanvasScaler_get_referencePixelsPerUnit_m3333 ();
extern "C" void CanvasScaler_set_referencePixelsPerUnit_m3334 ();
extern "C" void CanvasScaler_get_scaleFactor_m3335 ();
extern "C" void CanvasScaler_set_scaleFactor_m3336 ();
extern "C" void CanvasScaler_get_referenceResolution_m3337 ();
extern "C" void CanvasScaler_set_referenceResolution_m3338 ();
extern "C" void CanvasScaler_get_screenMatchMode_m3339 ();
extern "C" void CanvasScaler_set_screenMatchMode_m3340 ();
extern "C" void CanvasScaler_get_matchWidthOrHeight_m3341 ();
extern "C" void CanvasScaler_set_matchWidthOrHeight_m3342 ();
extern "C" void CanvasScaler_get_physicalUnit_m3343 ();
extern "C" void CanvasScaler_set_physicalUnit_m3344 ();
extern "C" void CanvasScaler_get_fallbackScreenDPI_m3345 ();
extern "C" void CanvasScaler_set_fallbackScreenDPI_m3346 ();
extern "C" void CanvasScaler_get_defaultSpriteDPI_m3347 ();
extern "C" void CanvasScaler_set_defaultSpriteDPI_m3348 ();
extern "C" void CanvasScaler_get_dynamicPixelsPerUnit_m3349 ();
extern "C" void CanvasScaler_set_dynamicPixelsPerUnit_m3350 ();
extern "C" void CanvasScaler_OnEnable_m3351 ();
extern "C" void CanvasScaler_OnDisable_m3352 ();
extern "C" void CanvasScaler_Update_m3353 ();
extern "C" void CanvasScaler_Handle_m3354 ();
extern "C" void CanvasScaler_HandleWorldCanvas_m3355 ();
extern "C" void CanvasScaler_HandleConstantPixelSize_m3356 ();
extern "C" void CanvasScaler_HandleScaleWithScreenSize_m3357 ();
extern "C" void CanvasScaler_HandleConstantPhysicalSize_m3358 ();
extern "C" void CanvasScaler_SetScaleFactor_m3359 ();
extern "C" void CanvasScaler_SetReferencePixelsPerUnit_m3360 ();
extern "C" void ContentSizeFitter__ctor_m3361 ();
extern "C" void ContentSizeFitter_get_horizontalFit_m3362 ();
extern "C" void ContentSizeFitter_set_horizontalFit_m3363 ();
extern "C" void ContentSizeFitter_get_verticalFit_m3364 ();
extern "C" void ContentSizeFitter_set_verticalFit_m3365 ();
extern "C" void ContentSizeFitter_get_rectTransform_m3366 ();
extern "C" void ContentSizeFitter_OnEnable_m3367 ();
extern "C" void ContentSizeFitter_OnDisable_m3368 ();
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m3369 ();
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m3370 ();
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m3371 ();
extern "C" void ContentSizeFitter_SetLayoutVertical_m3372 ();
extern "C" void ContentSizeFitter_SetDirty_m3373 ();
extern "C" void GridLayoutGroup__ctor_m3374 ();
extern "C" void GridLayoutGroup_get_startCorner_m3375 ();
extern "C" void GridLayoutGroup_set_startCorner_m3376 ();
extern "C" void GridLayoutGroup_get_startAxis_m3377 ();
extern "C" void GridLayoutGroup_set_startAxis_m3378 ();
extern "C" void GridLayoutGroup_get_cellSize_m3379 ();
extern "C" void GridLayoutGroup_set_cellSize_m3380 ();
extern "C" void GridLayoutGroup_get_spacing_m3381 ();
extern "C" void GridLayoutGroup_set_spacing_m3382 ();
extern "C" void GridLayoutGroup_get_constraint_m3383 ();
extern "C" void GridLayoutGroup_set_constraint_m3384 ();
extern "C" void GridLayoutGroup_get_constraintCount_m3385 ();
extern "C" void GridLayoutGroup_set_constraintCount_m3386 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m3387 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m3388 ();
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m3389 ();
extern "C" void GridLayoutGroup_SetLayoutVertical_m3390 ();
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m3391 ();
extern "C" void HorizontalLayoutGroup__ctor_m3392 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m3393 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m3394 ();
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m3395 ();
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m3396 ();
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m3397 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_spacing_m3398 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m3399 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3400 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m3401 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3402 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m3403 ();
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3404 ();
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3405 ();
extern "C" void LayoutElement__ctor_m3406 ();
extern "C" void LayoutElement_get_ignoreLayout_m3407 ();
extern "C" void LayoutElement_set_ignoreLayout_m3408 ();
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m3409 ();
extern "C" void LayoutElement_CalculateLayoutInputVertical_m3410 ();
extern "C" void LayoutElement_get_minWidth_m3411 ();
extern "C" void LayoutElement_set_minWidth_m3412 ();
extern "C" void LayoutElement_get_minHeight_m3413 ();
extern "C" void LayoutElement_set_minHeight_m3414 ();
extern "C" void LayoutElement_get_preferredWidth_m3415 ();
extern "C" void LayoutElement_set_preferredWidth_m3416 ();
extern "C" void LayoutElement_get_preferredHeight_m3417 ();
extern "C" void LayoutElement_set_preferredHeight_m3418 ();
extern "C" void LayoutElement_get_flexibleWidth_m3419 ();
extern "C" void LayoutElement_set_flexibleWidth_m3420 ();
extern "C" void LayoutElement_get_flexibleHeight_m3421 ();
extern "C" void LayoutElement_set_flexibleHeight_m3422 ();
extern "C" void LayoutElement_get_layoutPriority_m3423 ();
extern "C" void LayoutElement_OnEnable_m3424 ();
extern "C" void LayoutElement_OnTransformParentChanged_m3425 ();
extern "C" void LayoutElement_OnDisable_m3426 ();
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m3427 ();
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m3428 ();
extern "C" void LayoutElement_SetDirty_m3429 ();
extern "C" void LayoutGroup__ctor_m3430 ();
extern "C" void LayoutGroup_get_padding_m3431 ();
extern "C" void LayoutGroup_set_padding_m3432 ();
extern "C" void LayoutGroup_get_childAlignment_m3433 ();
extern "C" void LayoutGroup_set_childAlignment_m3434 ();
extern "C" void LayoutGroup_get_rectTransform_m3435 ();
extern "C" void LayoutGroup_get_rectChildren_m3436 ();
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m3437 ();
extern "C" void LayoutGroup_get_minWidth_m3438 ();
extern "C" void LayoutGroup_get_preferredWidth_m3439 ();
extern "C" void LayoutGroup_get_flexibleWidth_m3440 ();
extern "C" void LayoutGroup_get_minHeight_m3441 ();
extern "C" void LayoutGroup_get_preferredHeight_m3442 ();
extern "C" void LayoutGroup_get_flexibleHeight_m3443 ();
extern "C" void LayoutGroup_get_layoutPriority_m3444 ();
extern "C" void LayoutGroup_OnEnable_m3445 ();
extern "C" void LayoutGroup_OnDisable_m3446 ();
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m3447 ();
extern "C" void LayoutGroup_GetTotalMinSize_m3448 ();
extern "C" void LayoutGroup_GetTotalPreferredSize_m3449 ();
extern "C" void LayoutGroup_GetTotalFlexibleSize_m3450 ();
extern "C" void LayoutGroup_GetStartOffset_m3451 ();
extern "C" void LayoutGroup_SetLayoutInputForAxis_m3452 ();
extern "C" void LayoutGroup_SetChildAlongAxis_m3453 ();
extern "C" void LayoutGroup_get_isRootLayoutGroup_m3454 ();
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m3455 ();
extern "C" void LayoutGroup_OnTransformChildrenChanged_m3456 ();
extern "C" void LayoutGroup_SetDirty_m3457 ();
extern "C" void LayoutRebuilder__ctor_m3458 ();
extern "C" void LayoutRebuilder__cctor_m3459 ();
extern "C" void LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m3460 ();
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m3461 ();
extern "C" void LayoutRebuilder_get_transform_m3462 ();
extern "C" void LayoutRebuilder_IsDestroyed_m3463 ();
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m3464 ();
extern "C" void LayoutRebuilder_ForceRebuildLayoutImmediate_m3465 ();
extern "C" void LayoutRebuilder_PerformLayoutControl_m3466 ();
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m3467 ();
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m3468 ();
extern "C" void LayoutRebuilder_ValidLayoutGroup_m3469 ();
extern "C" void LayoutRebuilder_ValidController_m3470 ();
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m3471 ();
extern "C" void LayoutRebuilder_Equals_m3472 ();
extern "C" void LayoutRebuilder_GetHashCode_m3473 ();
extern "C" void LayoutRebuilder_ToString_m3474 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__8_m3475 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m3476 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m3477 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m3478 ();
extern "C" void LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3479 ();
extern "C" void LayoutUtility_GetMinSize_m3480 ();
extern "C" void LayoutUtility_GetPreferredSize_m3481 ();
extern "C" void LayoutUtility_GetFlexibleSize_m3482 ();
extern "C" void LayoutUtility_GetMinWidth_m3483 ();
extern "C" void LayoutUtility_GetPreferredWidth_m3484 ();
extern "C" void LayoutUtility_GetFlexibleWidth_m3485 ();
extern "C" void LayoutUtility_GetMinHeight_m3486 ();
extern "C" void LayoutUtility_GetPreferredHeight_m3487 ();
extern "C" void LayoutUtility_GetFlexibleHeight_m3488 ();
extern "C" void LayoutUtility_GetLayoutProperty_m3489 ();
extern "C" void LayoutUtility_GetLayoutProperty_m3490 ();
extern "C" void LayoutUtility_U3CGetMinWidthU3Em__D_m3491 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__E_m3492 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__F_m3493 ();
extern "C" void LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3494 ();
extern "C" void LayoutUtility_U3CGetMinHeightU3Em__11_m3495 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__12_m3496 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__13_m3497 ();
extern "C" void LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3498 ();
extern "C" void VerticalLayoutGroup__ctor_m3499 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3500 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m3501 ();
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m3502 ();
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m3503 ();
extern "C" void VertexHelper__ctor_m3504 ();
extern "C" void VertexHelper__ctor_m3505 ();
extern "C" void VertexHelper__cctor_m3506 ();
extern "C" void VertexHelper_get_currentVertCount_m3507 ();
extern "C" void VertexHelper_PopulateUIVertex_m3508 ();
extern "C" void VertexHelper_FillMesh_m3509 ();
extern "C" void VertexHelper_Dispose_m3510 ();
extern "C" void VertexHelper_AddVert_m3511 ();
extern "C" void VertexHelper_AddVert_m3512 ();
extern "C" void VertexHelper_AddVert_m3513 ();
extern "C" void VertexHelper_AddTriangle_m3514 ();
extern "C" void VertexHelper_AddUIVertexQuad_m3515 ();
extern "C" void VertexHelper_AddUIVertexTriangleStream_m3516 ();
extern "C" void VertexHelper_GetUIVertexStream_m3517 ();
extern "C" void BaseMeshEffect__ctor_m3518 ();
extern "C" void BaseMeshEffect_get_graphic_m3519 ();
extern "C" void BaseMeshEffect_OnEnable_m3520 ();
extern "C" void BaseMeshEffect_OnDisable_m3521 ();
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m3522 ();
extern "C" void Outline__ctor_m3523 ();
extern "C" void Outline_ModifyMesh_m3524 ();
extern "C" void PositionAsUV1__ctor_m3525 ();
extern "C" void PositionAsUV1_ModifyMesh_m3526 ();
extern "C" void Shadow__ctor_m3527 ();
extern "C" void Shadow_get_effectColor_m3528 ();
extern "C" void Shadow_set_effectColor_m3529 ();
extern "C" void Shadow_get_effectDistance_m3530 ();
extern "C" void Shadow_set_effectDistance_m3531 ();
extern "C" void Shadow_get_useGraphicAlpha_m3532 ();
extern "C" void Shadow_set_useGraphicAlpha_m3533 ();
extern "C" void Shadow_ApplyShadowZeroAlloc_m3534 ();
extern "C" void Shadow_ApplyShadow_m3535 ();
extern "C" void Shadow_ModifyMesh_m3536 ();
extern "C" void Builtins_join_m3780 ();
extern "C" void DispatcherFactory__ctor_m3781 ();
extern "C" void DispatcherFactory_Invoke_m3782 ();
extern "C" void DispatcherFactory_BeginInvoke_m3783 ();
extern "C" void DispatcherFactory_EndInvoke_m3784 ();
extern "C" void DispatcherCache__ctor_m3785 ();
extern "C" void DispatcherCache__cctor_m3786 ();
extern "C" void DispatcherCache_Get_m3787 ();
extern "C" void _EqualityComparer__ctor_m3788 ();
extern "C" void _EqualityComparer_GetHashCode_m3789 ();
extern "C" void _EqualityComparer_Equals_m3790 ();
extern "C" void DispatcherKey__ctor_m3791 ();
extern "C" void DispatcherKey__cctor_m3792 ();
extern "C" void ExtensionRegistry__ctor_m3793 ();
extern "C" void ExtensionRegistry_get_Extensions_m3794 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m3795 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m3796 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3797 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerable_GetEnumerator_m3798 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m3799 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m3800 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m3801 ();
extern "C" void U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m3802 ();
extern "C" void U3CCoerceU3Ec__AnonStorey1D__ctor_m3803 ();
extern "C" void U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m3804 ();
extern "C" void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m3805 ();
extern "C" void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m3806 ();
extern "C" void RuntimeServices__cctor_m3807 ();
extern "C" void RuntimeServices_GetDispatcher_m3808 ();
extern "C" void RuntimeServices_Coerce_m3809 ();
extern "C" void RuntimeServices_CreateCoerceDispatcher_m3810 ();
extern "C" void RuntimeServices_EmitPromotionDispatcher_m3811 ();
extern "C" void RuntimeServices_IsPromotableNumeric_m3812 ();
extern "C" void RuntimeServices_EmitImplicitConversionDispatcher_m3813 ();
extern "C" void RuntimeServices_CoercibleDispatcher_m3814 ();
extern "C" void RuntimeServices_IdentityDispatcher_m3815 ();
extern "C" void RuntimeServices_IsNumeric_m3816 ();
extern "C" void RuntimeServices_op_Addition_m691 ();
extern "C" void RuntimeServices_op_Addition_m668 ();
extern "C" void RuntimeServices_EqualityOperator_m3817 ();
extern "C" void RuntimeServices_ArrayEqualityImpl_m3818 ();
extern "C" void RuntimeServices_GetConvertTypeCode_m3819 ();
extern "C" void RuntimeServices_EqualityOperator_m3820 ();
extern "C" void RuntimeServices_IsPromotableNumeric_m3821 ();
extern "C" void RuntimeServices_FindImplicitConversionOperator_m3822 ();
extern "C" void RuntimeServices_GetExtensionMethods_m3823 ();
extern "C" void RuntimeServices_FindImplicitConversionMethod_m3824 ();
extern "C" void Dispatcher__ctor_m3825 ();
extern "C" void Dispatcher_Invoke_m3826 ();
extern "C" void Dispatcher_BeginInvoke_m3827 ();
extern "C" void Dispatcher_EndInvoke_m3828 ();
extern "C" void ExtensionAttribute__ctor_m3839 ();
extern "C" void Locale_GetText_m3840 ();
extern "C" void Locale_GetText_m3841 ();
extern "C" void KeyBuilder_get_Rng_m3842 ();
extern "C" void KeyBuilder_Key_m3843 ();
extern "C" void KeyBuilder_IV_m3844 ();
extern "C" void SymmetricTransform__ctor_m3845 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m3846 ();
extern "C" void SymmetricTransform_Finalize_m3847 ();
extern "C" void SymmetricTransform_Dispose_m3848 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m3849 ();
extern "C" void SymmetricTransform_Transform_m3850 ();
extern "C" void SymmetricTransform_CBC_m3851 ();
extern "C" void SymmetricTransform_CFB_m3852 ();
extern "C" void SymmetricTransform_OFB_m3853 ();
extern "C" void SymmetricTransform_CTS_m3854 ();
extern "C" void SymmetricTransform_CheckInput_m3855 ();
extern "C" void SymmetricTransform_TransformBlock_m3856 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m3857 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m3858 ();
extern "C" void SymmetricTransform_Random_m3859 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m3860 ();
extern "C" void SymmetricTransform_FinalEncrypt_m3861 ();
extern "C" void SymmetricTransform_FinalDecrypt_m3862 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m3863 ();
extern "C" void Check_Source_m3864 ();
extern "C" void Check_SourceAndSelector_m3865 ();
extern "C" void Check_SourceAndPredicate_m3866 ();
extern "C" void Aes__ctor_m3867 ();
extern "C" void AesManaged__ctor_m3868 ();
extern "C" void AesManaged_GenerateIV_m3869 ();
extern "C" void AesManaged_GenerateKey_m3870 ();
extern "C" void AesManaged_CreateDecryptor_m3871 ();
extern "C" void AesManaged_CreateEncryptor_m3872 ();
extern "C" void AesManaged_get_IV_m3873 ();
extern "C" void AesManaged_set_IV_m3874 ();
extern "C" void AesManaged_get_Key_m3875 ();
extern "C" void AesManaged_set_Key_m3876 ();
extern "C" void AesManaged_get_KeySize_m3877 ();
extern "C" void AesManaged_set_KeySize_m3878 ();
extern "C" void AesManaged_CreateDecryptor_m3879 ();
extern "C" void AesManaged_CreateEncryptor_m3880 ();
extern "C" void AesManaged_Dispose_m3881 ();
extern "C" void AesTransform__ctor_m3882 ();
extern "C" void AesTransform__cctor_m3883 ();
extern "C" void AesTransform_ECB_m3884 ();
extern "C" void AesTransform_SubByte_m3885 ();
extern "C" void AesTransform_Encrypt128_m3886 ();
extern "C" void AesTransform_Decrypt128_m3887 ();
extern "C" void Action__ctor_m3888 ();
extern "C" void Action_Invoke_m411 ();
extern "C" void Action_BeginInvoke_m3889 ();
extern "C" void Action_EndInvoke_m3890 ();
extern "C" void Locale_GetText_m3911 ();
extern "C" void Locale_GetText_m3912 ();
extern "C" void MonoTODOAttribute__ctor_m3913 ();
extern "C" void MonoTODOAttribute__ctor_m3914 ();
extern "C" void HybridDictionary__ctor_m3915 ();
extern "C" void HybridDictionary__ctor_m3916 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m3917 ();
extern "C" void HybridDictionary_get_inner_m3918 ();
extern "C" void HybridDictionary_get_Count_m3919 ();
extern "C" void HybridDictionary_get_IsSynchronized_m3920 ();
extern "C" void HybridDictionary_get_Item_m3921 ();
extern "C" void HybridDictionary_set_Item_m3922 ();
extern "C" void HybridDictionary_get_SyncRoot_m3923 ();
extern "C" void HybridDictionary_Add_m3924 ();
extern "C" void HybridDictionary_Contains_m3925 ();
extern "C" void HybridDictionary_CopyTo_m3926 ();
extern "C" void HybridDictionary_GetEnumerator_m3927 ();
extern "C" void HybridDictionary_Remove_m3928 ();
extern "C" void HybridDictionary_Switch_m3929 ();
extern "C" void DictionaryNode__ctor_m3930 ();
extern "C" void DictionaryNodeEnumerator__ctor_m3931 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m3932 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m3933 ();
extern "C" void DictionaryNodeEnumerator_Reset_m3934 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m3935 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m3936 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m3937 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m3938 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m3939 ();
extern "C" void ListDictionary__ctor_m3940 ();
extern "C" void ListDictionary__ctor_m3941 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m3942 ();
extern "C" void ListDictionary_FindEntry_m3943 ();
extern "C" void ListDictionary_FindEntry_m3944 ();
extern "C" void ListDictionary_AddImpl_m3945 ();
extern "C" void ListDictionary_get_Count_m3946 ();
extern "C" void ListDictionary_get_IsSynchronized_m3947 ();
extern "C" void ListDictionary_get_SyncRoot_m3948 ();
extern "C" void ListDictionary_CopyTo_m3949 ();
extern "C" void ListDictionary_get_Item_m3950 ();
extern "C" void ListDictionary_set_Item_m3951 ();
extern "C" void ListDictionary_Add_m3952 ();
extern "C" void ListDictionary_Clear_m3953 ();
extern "C" void ListDictionary_Contains_m3954 ();
extern "C" void ListDictionary_GetEnumerator_m3955 ();
extern "C" void ListDictionary_Remove_m3956 ();
extern "C" void _Item__ctor_m3957 ();
extern "C" void _KeysEnumerator__ctor_m3958 ();
extern "C" void _KeysEnumerator_get_Current_m3959 ();
extern "C" void _KeysEnumerator_MoveNext_m3960 ();
extern "C" void _KeysEnumerator_Reset_m3961 ();
extern "C" void KeysCollection__ctor_m3962 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m3963 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_IsSynchronized_m3964 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m3965 ();
extern "C" void KeysCollection_get_Count_m3966 ();
extern "C" void KeysCollection_GetEnumerator_m3967 ();
extern "C" void NameObjectCollectionBase__ctor_m3968 ();
extern "C" void NameObjectCollectionBase__ctor_m3969 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m3970 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m3971 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3972 ();
extern "C" void NameObjectCollectionBase_Init_m3973 ();
extern "C" void NameObjectCollectionBase_get_Keys_m3974 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m3975 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m3976 ();
extern "C" void NameObjectCollectionBase_get_Count_m3977 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m3978 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m3979 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m3980 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3981 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3982 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m3983 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m3984 ();
extern "C" void NameValueCollection__ctor_m3985 ();
extern "C" void NameValueCollection__ctor_m3986 ();
extern "C" void NameValueCollection_Add_m3987 ();
extern "C" void NameValueCollection_Get_m3988 ();
extern "C" void NameValueCollection_AsSingleString_m3989 ();
extern "C" void NameValueCollection_GetKey_m3990 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m3991 ();
extern "C" void EditorBrowsableAttribute__ctor_m3992 ();
extern "C" void EditorBrowsableAttribute_get_State_m3993 ();
extern "C" void EditorBrowsableAttribute_Equals_m3994 ();
extern "C" void EditorBrowsableAttribute_GetHashCode_m3995 ();
extern "C" void TypeConverterAttribute__ctor_m3996 ();
extern "C" void TypeConverterAttribute__ctor_m3997 ();
extern "C" void TypeConverterAttribute__cctor_m3998 ();
extern "C" void TypeConverterAttribute_Equals_m3999 ();
extern "C" void TypeConverterAttribute_GetHashCode_m4000 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m4001 ();
extern "C" void DefaultCertificatePolicy__ctor_m4002 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m4003 ();
extern "C" void FileWebRequest__ctor_m4004 ();
extern "C" void FileWebRequest__ctor_m4005 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4006 ();
extern "C" void FileWebRequest_GetObjectData_m4007 ();
extern "C" void FileWebRequestCreator__ctor_m4008 ();
extern "C" void FileWebRequestCreator_Create_m4009 ();
extern "C" void FtpRequestCreator__ctor_m4010 ();
extern "C" void FtpRequestCreator_Create_m4011 ();
extern "C" void FtpWebRequest__ctor_m4012 ();
extern "C" void FtpWebRequest__cctor_m4013 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m4014 ();
extern "C" void GlobalProxySelection_get_Select_m4015 ();
extern "C" void HttpRequestCreator__ctor_m4016 ();
extern "C" void HttpRequestCreator_Create_m4017 ();
extern "C" void HttpVersion__cctor_m4018 ();
extern "C" void HttpWebRequest__ctor_m4019 ();
extern "C" void HttpWebRequest__ctor_m4020 ();
extern "C" void HttpWebRequest__cctor_m4021 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4022 ();
extern "C" void HttpWebRequest_get_Address_m4023 ();
extern "C" void HttpWebRequest_get_ServicePoint_m4024 ();
extern "C" void HttpWebRequest_GetServicePoint_m4025 ();
extern "C" void HttpWebRequest_GetObjectData_m4026 ();
extern "C" void IPAddress__ctor_m4027 ();
extern "C" void IPAddress__ctor_m4028 ();
extern "C" void IPAddress__cctor_m4029 ();
extern "C" void IPAddress_SwapShort_m4030 ();
extern "C" void IPAddress_HostToNetworkOrder_m4031 ();
extern "C" void IPAddress_NetworkToHostOrder_m4032 ();
extern "C" void IPAddress_Parse_m4033 ();
extern "C" void IPAddress_TryParse_m4034 ();
extern "C" void IPAddress_ParseIPV4_m4035 ();
extern "C" void IPAddress_ParseIPV6_m4036 ();
extern "C" void IPAddress_get_InternalIPv4Address_m4037 ();
extern "C" void IPAddress_get_ScopeId_m4038 ();
extern "C" void IPAddress_get_AddressFamily_m4039 ();
extern "C" void IPAddress_IsLoopback_m4040 ();
extern "C" void IPAddress_ToString_m4041 ();
extern "C" void IPAddress_ToString_m4042 ();
extern "C" void IPAddress_Equals_m4043 ();
extern "C" void IPAddress_GetHashCode_m4044 ();
extern "C" void IPAddress_Hash_m4045 ();
extern "C" void IPv6Address__ctor_m4046 ();
extern "C" void IPv6Address__ctor_m4047 ();
extern "C" void IPv6Address__ctor_m4048 ();
extern "C" void IPv6Address__cctor_m4049 ();
extern "C" void IPv6Address_Parse_m4050 ();
extern "C" void IPv6Address_Fill_m4051 ();
extern "C" void IPv6Address_TryParse_m4052 ();
extern "C" void IPv6Address_TryParse_m4053 ();
extern "C" void IPv6Address_get_Address_m4054 ();
extern "C" void IPv6Address_get_ScopeId_m4055 ();
extern "C" void IPv6Address_set_ScopeId_m4056 ();
extern "C" void IPv6Address_IsLoopback_m4057 ();
extern "C" void IPv6Address_SwapUShort_m4058 ();
extern "C" void IPv6Address_AsIPv4Int_m4059 ();
extern "C" void IPv6Address_IsIPv4Compatible_m4060 ();
extern "C" void IPv6Address_IsIPv4Mapped_m4061 ();
extern "C" void IPv6Address_ToString_m4062 ();
extern "C" void IPv6Address_ToString_m4063 ();
extern "C" void IPv6Address_Equals_m4064 ();
extern "C" void IPv6Address_GetHashCode_m4065 ();
extern "C" void IPv6Address_Hash_m4066 ();
extern "C" void ServicePoint__ctor_m4067 ();
extern "C" void ServicePoint_get_Address_m4068 ();
extern "C" void ServicePoint_get_CurrentConnections_m4069 ();
extern "C" void ServicePoint_get_IdleSince_m4070 ();
extern "C" void ServicePoint_set_IdleSince_m4071 ();
extern "C" void ServicePoint_set_Expect100Continue_m4072 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m4073 ();
extern "C" void ServicePoint_set_SendContinue_m4074 ();
extern "C" void ServicePoint_set_UsesProxy_m4075 ();
extern "C" void ServicePoint_set_UseConnect_m4076 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m4077 ();
extern "C" void SPKey__ctor_m4078 ();
extern "C" void SPKey_GetHashCode_m4079 ();
extern "C" void SPKey_Equals_m4080 ();
extern "C" void ServicePointManager__cctor_m4081 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m4082 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m4083 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m4084 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m4085 ();
extern "C" void ServicePointManager_FindServicePoint_m4086 ();
extern "C" void ServicePointManager_RecycleServicePoints_m4087 ();
extern "C" void WebHeaderCollection__ctor_m4088 ();
extern "C" void WebHeaderCollection__ctor_m4089 ();
extern "C" void WebHeaderCollection__ctor_m4090 ();
extern "C" void WebHeaderCollection__cctor_m4091 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4092 ();
extern "C" void WebHeaderCollection_Add_m4093 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m4094 ();
extern "C" void WebHeaderCollection_IsRestricted_m4095 ();
extern "C" void WebHeaderCollection_OnDeserialization_m4096 ();
extern "C" void WebHeaderCollection_ToString_m4097 ();
extern "C" void WebHeaderCollection_GetObjectData_m4098 ();
extern "C" void WebHeaderCollection_get_Count_m4099 ();
extern "C" void WebHeaderCollection_get_Keys_m4100 ();
extern "C" void WebHeaderCollection_Get_m4101 ();
extern "C" void WebHeaderCollection_GetKey_m4102 ();
extern "C" void WebHeaderCollection_GetEnumerator_m4103 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m4104 ();
extern "C" void WebHeaderCollection_IsHeaderName_m4105 ();
extern "C" void WebProxy__ctor_m4106 ();
extern "C" void WebProxy__ctor_m4107 ();
extern "C" void WebProxy__ctor_m4108 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4109 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m4110 ();
extern "C" void WebProxy_GetProxy_m4111 ();
extern "C" void WebProxy_IsBypassed_m4112 ();
extern "C" void WebProxy_GetObjectData_m4113 ();
extern "C" void WebProxy_CheckBypassList_m4114 ();
extern "C" void WebRequest__ctor_m4115 ();
extern "C" void WebRequest__ctor_m4116 ();
extern "C" void WebRequest__cctor_m4117 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4118 ();
extern "C" void WebRequest_AddDynamicPrefix_m4119 ();
extern "C" void WebRequest_GetMustImplement_m4120 ();
extern "C" void WebRequest_get_DefaultWebProxy_m4121 ();
extern "C" void WebRequest_GetDefaultWebProxy_m4122 ();
extern "C" void WebRequest_GetObjectData_m4123 ();
extern "C" void WebRequest_AddPrefix_m4124 ();
extern "C" void PublicKey__ctor_m4125 ();
extern "C" void PublicKey_get_EncodedKeyValue_m4126 ();
extern "C" void PublicKey_get_EncodedParameters_m4127 ();
extern "C" void PublicKey_get_Key_m4128 ();
extern "C" void PublicKey_get_Oid_m4129 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m4130 ();
extern "C" void PublicKey_DecodeDSA_m4131 ();
extern "C" void PublicKey_DecodeRSA_m4132 ();
extern "C" void X500DistinguishedName__ctor_m4133 ();
extern "C" void X500DistinguishedName_Decode_m4134 ();
extern "C" void X500DistinguishedName_GetSeparator_m4135 ();
extern "C" void X500DistinguishedName_DecodeRawData_m4136 ();
extern "C" void X500DistinguishedName_Canonize_m4137 ();
extern "C" void X500DistinguishedName_AreEqual_m4138 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4139 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4140 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4141 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m4142 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4143 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m4144 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m4145 ();
extern "C" void X509BasicConstraintsExtension_Decode_m4146 ();
extern "C" void X509BasicConstraintsExtension_Encode_m4147 ();
extern "C" void X509BasicConstraintsExtension_ToString_m4148 ();
extern "C" void X509Certificate2__ctor_m4149 ();
extern "C" void X509Certificate2__cctor_m4150 ();
extern "C" void X509Certificate2_get_Extensions_m4151 ();
extern "C" void X509Certificate2_get_IssuerName_m4152 ();
extern "C" void X509Certificate2_get_NotAfter_m4153 ();
extern "C" void X509Certificate2_get_NotBefore_m4154 ();
extern "C" void X509Certificate2_get_PrivateKey_m4155 ();
extern "C" void X509Certificate2_get_PublicKey_m4156 ();
extern "C" void X509Certificate2_get_SerialNumber_m4157 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m4158 ();
extern "C" void X509Certificate2_get_SubjectName_m4159 ();
extern "C" void X509Certificate2_get_Thumbprint_m4160 ();
extern "C" void X509Certificate2_get_Version_m4161 ();
extern "C" void X509Certificate2_GetNameInfo_m4162 ();
extern "C" void X509Certificate2_Find_m4163 ();
extern "C" void X509Certificate2_GetValueAsString_m4164 ();
extern "C" void X509Certificate2_ImportPkcs12_m4165 ();
extern "C" void X509Certificate2_Import_m4166 ();
extern "C" void X509Certificate2_Reset_m4167 ();
extern "C" void X509Certificate2_ToString_m4168 ();
extern "C" void X509Certificate2_ToString_m4169 ();
extern "C" void X509Certificate2_AppendBuffer_m4170 ();
extern "C" void X509Certificate2_Verify_m4171 ();
extern "C" void X509Certificate2_get_MonoCertificate_m4172 ();
extern "C" void X509Certificate2Collection__ctor_m4173 ();
extern "C" void X509Certificate2Collection__ctor_m4174 ();
extern "C" void X509Certificate2Collection_get_Item_m4175 ();
extern "C" void X509Certificate2Collection_Add_m4176 ();
extern "C" void X509Certificate2Collection_AddRange_m4177 ();
extern "C" void X509Certificate2Collection_Contains_m4178 ();
extern "C" void X509Certificate2Collection_Find_m4179 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m4180 ();
extern "C" void X509Certificate2Enumerator__ctor_m4181 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m4182 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m4183 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m4184 ();
extern "C" void X509Certificate2Enumerator_get_Current_m4185 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m4186 ();
extern "C" void X509Certificate2Enumerator_Reset_m4187 ();
extern "C" void X509CertificateEnumerator__ctor_m4188 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4189 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4190 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4191 ();
extern "C" void X509CertificateEnumerator_get_Current_m4192 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4193 ();
extern "C" void X509CertificateEnumerator_Reset_m4194 ();
extern "C" void X509CertificateCollection__ctor_m4195 ();
extern "C" void X509CertificateCollection__ctor_m4196 ();
extern "C" void X509CertificateCollection_get_Item_m4197 ();
extern "C" void X509CertificateCollection_AddRange_m4198 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4199 ();
extern "C" void X509CertificateCollection_GetHashCode_m4200 ();
extern "C" void X509Chain__ctor_m4201 ();
extern "C" void X509Chain__ctor_m4202 ();
extern "C" void X509Chain__cctor_m4203 ();
extern "C" void X509Chain_get_ChainPolicy_m4204 ();
extern "C" void X509Chain_Build_m4205 ();
extern "C" void X509Chain_Reset_m4206 ();
extern "C" void X509Chain_get_Roots_m4207 ();
extern "C" void X509Chain_get_CertificateAuthorities_m4208 ();
extern "C" void X509Chain_get_CertificateCollection_m4209 ();
extern "C" void X509Chain_BuildChainFrom_m4210 ();
extern "C" void X509Chain_SelectBestFromCollection_m4211 ();
extern "C" void X509Chain_FindParent_m4212 ();
extern "C" void X509Chain_IsChainComplete_m4213 ();
extern "C" void X509Chain_IsSelfIssued_m4214 ();
extern "C" void X509Chain_ValidateChain_m4215 ();
extern "C" void X509Chain_Process_m4216 ();
extern "C" void X509Chain_PrepareForNextCertificate_m4217 ();
extern "C" void X509Chain_WrapUp_m4218 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m4219 ();
extern "C" void X509Chain_IsSignedWith_m4220 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m4221 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4222 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4223 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4224 ();
extern "C" void X509Chain_CheckRevocationOnChain_m4225 ();
extern "C" void X509Chain_CheckRevocation_m4226 ();
extern "C" void X509Chain_CheckRevocation_m4227 ();
extern "C" void X509Chain_FindCrl_m4228 ();
extern "C" void X509Chain_ProcessCrlExtensions_m4229 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m4230 ();
extern "C" void X509ChainElement__ctor_m4231 ();
extern "C" void X509ChainElement_get_Certificate_m4232 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m4233 ();
extern "C" void X509ChainElement_get_StatusFlags_m4234 ();
extern "C" void X509ChainElement_set_StatusFlags_m4235 ();
extern "C" void X509ChainElement_Count_m4236 ();
extern "C" void X509ChainElement_Set_m4237 ();
extern "C" void X509ChainElement_UncompressFlags_m4238 ();
extern "C" void X509ChainElementCollection__ctor_m4239 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4240 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m4241 ();
extern "C" void X509ChainElementCollection_get_Count_m4242 ();
extern "C" void X509ChainElementCollection_get_IsSynchronized_m4243 ();
extern "C" void X509ChainElementCollection_get_Item_m4244 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m4245 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m4246 ();
extern "C" void X509ChainElementCollection_Add_m4247 ();
extern "C" void X509ChainElementCollection_Clear_m4248 ();
extern "C" void X509ChainElementCollection_Contains_m4249 ();
extern "C" void X509ChainElementEnumerator__ctor_m4250 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m4251 ();
extern "C" void X509ChainElementEnumerator_get_Current_m4252 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m4253 ();
extern "C" void X509ChainElementEnumerator_Reset_m4254 ();
extern "C" void X509ChainPolicy__ctor_m4255 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m4256 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m4257 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m4258 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m4259 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m4260 ();
extern "C" void X509ChainPolicy_Reset_m4261 ();
extern "C" void X509ChainStatus__ctor_m4262 ();
extern "C" void X509ChainStatus_get_Status_m4263 ();
extern "C" void X509ChainStatus_set_Status_m4264 ();
extern "C" void X509ChainStatus_set_StatusInformation_m4265 ();
extern "C" void X509ChainStatus_GetInformation_m4266 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m4267 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m4268 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m4269 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m4270 ();
extern "C" void X509Extension__ctor_m4271 ();
extern "C" void X509Extension__ctor_m4272 ();
extern "C" void X509Extension_get_Critical_m4273 ();
extern "C" void X509Extension_set_Critical_m4274 ();
extern "C" void X509Extension_CopyFrom_m4275 ();
extern "C" void X509Extension_FormatUnkownData_m4276 ();
extern "C" void X509ExtensionCollection__ctor_m4277 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m4278 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4279 ();
extern "C" void X509ExtensionCollection_get_Count_m4280 ();
extern "C" void X509ExtensionCollection_get_IsSynchronized_m4281 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m4282 ();
extern "C" void X509ExtensionCollection_get_Item_m4283 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m4284 ();
extern "C" void X509ExtensionEnumerator__ctor_m4285 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4286 ();
extern "C" void X509ExtensionEnumerator_get_Current_m4287 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m4288 ();
extern "C" void X509ExtensionEnumerator_Reset_m4289 ();
extern "C" void X509KeyUsageExtension__ctor_m4290 ();
extern "C" void X509KeyUsageExtension__ctor_m4291 ();
extern "C" void X509KeyUsageExtension__ctor_m4292 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m4293 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m4294 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m4295 ();
extern "C" void X509KeyUsageExtension_Decode_m4296 ();
extern "C" void X509KeyUsageExtension_Encode_m4297 ();
extern "C" void X509KeyUsageExtension_ToString_m4298 ();
extern "C" void X509Store__ctor_m4299 ();
extern "C" void X509Store_get_Certificates_m4300 ();
extern "C" void X509Store_get_Factory_m4301 ();
extern "C" void X509Store_get_Store_m4302 ();
extern "C" void X509Store_Close_m4303 ();
extern "C" void X509Store_Open_m4304 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4305 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4306 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4307 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4308 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4309 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4310 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4311 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m4312 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m4313 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m4314 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m4315 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m4316 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m4317 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m4318 ();
extern "C" void AsnEncodedData__ctor_m4319 ();
extern "C" void AsnEncodedData__ctor_m4320 ();
extern "C" void AsnEncodedData__ctor_m4321 ();
extern "C" void AsnEncodedData_get_Oid_m4322 ();
extern "C" void AsnEncodedData_set_Oid_m4323 ();
extern "C" void AsnEncodedData_get_RawData_m4324 ();
extern "C" void AsnEncodedData_set_RawData_m4325 ();
extern "C" void AsnEncodedData_CopyFrom_m4326 ();
extern "C" void AsnEncodedData_ToString_m4327 ();
extern "C" void AsnEncodedData_Default_m4328 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m4329 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m4330 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m4331 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m4332 ();
extern "C" void AsnEncodedData_SubjectAltName_m4333 ();
extern "C" void AsnEncodedData_NetscapeCertType_m4334 ();
extern "C" void Oid__ctor_m4335 ();
extern "C" void Oid__ctor_m4336 ();
extern "C" void Oid__ctor_m4337 ();
extern "C" void Oid__ctor_m4338 ();
extern "C" void Oid_get_FriendlyName_m4339 ();
extern "C" void Oid_get_Value_m4340 ();
extern "C" void Oid_GetName_m4341 ();
extern "C" void OidCollection__ctor_m4342 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m4343 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m4344 ();
extern "C" void OidCollection_get_Count_m4345 ();
extern "C" void OidCollection_get_IsSynchronized_m4346 ();
extern "C" void OidCollection_get_Item_m4347 ();
extern "C" void OidCollection_get_SyncRoot_m4348 ();
extern "C" void OidCollection_Add_m4349 ();
extern "C" void OidEnumerator__ctor_m4350 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m4351 ();
extern "C" void OidEnumerator_MoveNext_m4352 ();
extern "C" void OidEnumerator_Reset_m4353 ();
extern "C" void MatchAppendEvaluator__ctor_m4354 ();
extern "C" void MatchAppendEvaluator_Invoke_m4355 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m4356 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m4357 ();
extern "C" void BaseMachine__ctor_m4358 ();
extern "C" void BaseMachine_Replace_m4359 ();
extern "C" void BaseMachine_Scan_m4360 ();
extern "C" void BaseMachine_LTRReplace_m4361 ();
extern "C" void BaseMachine_RTLReplace_m4362 ();
extern "C" void Capture__ctor_m4363 ();
extern "C" void Capture__ctor_m4364 ();
extern "C" void Capture_get_Index_m4365 ();
extern "C" void Capture_get_Length_m4366 ();
extern "C" void Capture_get_Value_m4367 ();
extern "C" void Capture_ToString_m4368 ();
extern "C" void Capture_get_Text_m4369 ();
extern "C" void CaptureCollection__ctor_m4370 ();
extern "C" void CaptureCollection_get_Count_m4371 ();
extern "C" void CaptureCollection_get_IsSynchronized_m4372 ();
extern "C" void CaptureCollection_SetValue_m4373 ();
extern "C" void CaptureCollection_get_SyncRoot_m4374 ();
extern "C" void CaptureCollection_CopyTo_m4375 ();
extern "C" void CaptureCollection_GetEnumerator_m4376 ();
extern "C" void Group__ctor_m4377 ();
extern "C" void Group__ctor_m4378 ();
extern "C" void Group__ctor_m4379 ();
extern "C" void Group__cctor_m4380 ();
extern "C" void Group_get_Captures_m4381 ();
extern "C" void Group_get_Success_m4382 ();
extern "C" void GroupCollection__ctor_m4383 ();
extern "C" void GroupCollection_get_Count_m4384 ();
extern "C" void GroupCollection_get_IsSynchronized_m4385 ();
extern "C" void GroupCollection_get_Item_m4386 ();
extern "C" void GroupCollection_SetValue_m4387 ();
extern "C" void GroupCollection_get_SyncRoot_m4388 ();
extern "C" void GroupCollection_CopyTo_m4389 ();
extern "C" void GroupCollection_GetEnumerator_m4390 ();
extern "C" void Match__ctor_m4391 ();
extern "C" void Match__ctor_m4392 ();
extern "C" void Match__ctor_m4393 ();
extern "C" void Match__cctor_m4394 ();
extern "C" void Match_get_Empty_m4395 ();
extern "C" void Match_get_Groups_m4396 ();
extern "C" void Match_NextMatch_m4397 ();
extern "C" void Match_get_Regex_m4398 ();
extern "C" void Enumerator__ctor_m4399 ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m4400 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m4401 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m4402 ();
extern "C" void MatchCollection__ctor_m4403 ();
extern "C" void MatchCollection_get_Count_m4404 ();
extern "C" void MatchCollection_get_IsSynchronized_m4405 ();
extern "C" void MatchCollection_get_Item_m4406 ();
extern "C" void MatchCollection_get_SyncRoot_m4407 ();
extern "C" void MatchCollection_CopyTo_m4408 ();
extern "C" void MatchCollection_GetEnumerator_m4409 ();
extern "C" void MatchCollection_TryToGet_m4410 ();
extern "C" void MatchCollection_get_FullList_m4411 ();
extern "C" void Regex__ctor_m4412 ();
extern "C" void Regex__ctor_m578 ();
extern "C" void Regex__ctor_m4413 ();
extern "C" void Regex__ctor_m4414 ();
extern "C" void Regex__cctor_m4415 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m4416 ();
extern "C" void Regex_Replace_m2225 ();
extern "C" void Regex_Replace_m4417 ();
extern "C" void Regex_validate_options_m4418 ();
extern "C" void Regex_Init_m4419 ();
extern "C" void Regex_InitNewRegex_m4420 ();
extern "C" void Regex_CreateMachineFactory_m4421 ();
extern "C" void Regex_get_Options_m4422 ();
extern "C" void Regex_get_RightToLeft_m4423 ();
extern "C" void Regex_GroupNumberFromName_m4424 ();
extern "C" void Regex_GetGroupIndex_m4425 ();
extern "C" void Regex_default_startat_m4426 ();
extern "C" void Regex_IsMatch_m4427 ();
extern "C" void Regex_IsMatch_m4428 ();
extern "C" void Regex_Match_m4429 ();
extern "C" void Regex_Matches_m4430 ();
extern "C" void Regex_Matches_m4431 ();
extern "C" void Regex_Replace_m580 ();
extern "C" void Regex_Replace_m4432 ();
extern "C" void Regex_ToString_m4433 ();
extern "C" void Regex_get_GroupCount_m4434 ();
extern "C" void Regex_get_Gap_m4435 ();
extern "C" void Regex_CreateMachine_m4436 ();
extern "C" void Regex_GetGroupNamesArray_m4437 ();
extern "C" void Regex_get_GroupNumbers_m4438 ();
extern "C" void Key__ctor_m4439 ();
extern "C" void Key_GetHashCode_m4440 ();
extern "C" void Key_Equals_m4441 ();
extern "C" void Key_ToString_m4442 ();
extern "C" void FactoryCache__ctor_m4443 ();
extern "C" void FactoryCache_Add_m4444 ();
extern "C" void FactoryCache_Cleanup_m4445 ();
extern "C" void FactoryCache_Lookup_m4446 ();
extern "C" void Node__ctor_m4447 ();
extern "C" void MRUList__ctor_m4448 ();
extern "C" void MRUList_Use_m4449 ();
extern "C" void MRUList_Evict_m4450 ();
extern "C" void CategoryUtils_CategoryFromName_m4451 ();
extern "C" void CategoryUtils_IsCategory_m4452 ();
extern "C" void CategoryUtils_IsCategory_m4453 ();
extern "C" void LinkRef__ctor_m4454 ();
extern "C" void InterpreterFactory__ctor_m4455 ();
extern "C" void InterpreterFactory_NewInstance_m4456 ();
extern "C" void InterpreterFactory_get_GroupCount_m4457 ();
extern "C" void InterpreterFactory_get_Gap_m4458 ();
extern "C" void InterpreterFactory_set_Gap_m4459 ();
extern "C" void InterpreterFactory_get_Mapping_m4460 ();
extern "C" void InterpreterFactory_set_Mapping_m4461 ();
extern "C" void InterpreterFactory_get_NamesMapping_m4462 ();
extern "C" void InterpreterFactory_set_NamesMapping_m4463 ();
extern "C" void PatternLinkStack__ctor_m4464 ();
extern "C" void PatternLinkStack_set_BaseAddress_m4465 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m4466 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m4467 ();
extern "C" void PatternLinkStack_GetOffset_m4468 ();
extern "C" void PatternLinkStack_GetCurrent_m4469 ();
extern "C" void PatternLinkStack_SetCurrent_m4470 ();
extern "C" void PatternCompiler__ctor_m4471 ();
extern "C" void PatternCompiler_EncodeOp_m4472 ();
extern "C" void PatternCompiler_GetMachineFactory_m4473 ();
extern "C" void PatternCompiler_EmitFalse_m4474 ();
extern "C" void PatternCompiler_EmitTrue_m4475 ();
extern "C" void PatternCompiler_EmitCount_m4476 ();
extern "C" void PatternCompiler_EmitCharacter_m4477 ();
extern "C" void PatternCompiler_EmitCategory_m4478 ();
extern "C" void PatternCompiler_EmitNotCategory_m4479 ();
extern "C" void PatternCompiler_EmitRange_m4480 ();
extern "C" void PatternCompiler_EmitSet_m4481 ();
extern "C" void PatternCompiler_EmitString_m4482 ();
extern "C" void PatternCompiler_EmitPosition_m4483 ();
extern "C" void PatternCompiler_EmitOpen_m4484 ();
extern "C" void PatternCompiler_EmitClose_m4485 ();
extern "C" void PatternCompiler_EmitBalanceStart_m4486 ();
extern "C" void PatternCompiler_EmitBalance_m4487 ();
extern "C" void PatternCompiler_EmitReference_m4488 ();
extern "C" void PatternCompiler_EmitIfDefined_m4489 ();
extern "C" void PatternCompiler_EmitSub_m4490 ();
extern "C" void PatternCompiler_EmitTest_m4491 ();
extern "C" void PatternCompiler_EmitBranch_m4492 ();
extern "C" void PatternCompiler_EmitJump_m4493 ();
extern "C" void PatternCompiler_EmitRepeat_m4494 ();
extern "C" void PatternCompiler_EmitUntil_m4495 ();
extern "C" void PatternCompiler_EmitFastRepeat_m4496 ();
extern "C" void PatternCompiler_EmitIn_m4497 ();
extern "C" void PatternCompiler_EmitAnchor_m4498 ();
extern "C" void PatternCompiler_EmitInfo_m4499 ();
extern "C" void PatternCompiler_NewLink_m4500 ();
extern "C" void PatternCompiler_ResolveLink_m4501 ();
extern "C" void PatternCompiler_EmitBranchEnd_m4502 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m4503 ();
extern "C" void PatternCompiler_MakeFlags_m4504 ();
extern "C" void PatternCompiler_Emit_m4505 ();
extern "C" void PatternCompiler_Emit_m4506 ();
extern "C" void PatternCompiler_Emit_m4507 ();
extern "C" void PatternCompiler_get_CurrentAddress_m4508 ();
extern "C" void PatternCompiler_BeginLink_m4509 ();
extern "C" void PatternCompiler_EmitLink_m4510 ();
extern "C" void LinkStack__ctor_m4511 ();
extern "C" void LinkStack_Push_m4512 ();
extern "C" void LinkStack_Pop_m4513 ();
extern "C" void Mark_get_IsDefined_m4514 ();
extern "C" void Mark_get_Index_m4515 ();
extern "C" void Mark_get_Length_m4516 ();
extern "C" void IntStack_Pop_m4517 ();
extern "C" void IntStack_Push_m4518 ();
extern "C" void IntStack_get_Count_m4519 ();
extern "C" void IntStack_set_Count_m4520 ();
extern "C" void RepeatContext__ctor_m4521 ();
extern "C" void RepeatContext_get_Count_m4522 ();
extern "C" void RepeatContext_set_Count_m4523 ();
extern "C" void RepeatContext_get_Start_m4524 ();
extern "C" void RepeatContext_set_Start_m4525 ();
extern "C" void RepeatContext_get_IsMinimum_m4526 ();
extern "C" void RepeatContext_get_IsMaximum_m4527 ();
extern "C" void RepeatContext_get_IsLazy_m4528 ();
extern "C" void RepeatContext_get_Expression_m4529 ();
extern "C" void RepeatContext_get_Previous_m4530 ();
extern "C" void Interpreter__ctor_m4531 ();
extern "C" void Interpreter_ReadProgramCount_m4532 ();
extern "C" void Interpreter_Scan_m4533 ();
extern "C" void Interpreter_Reset_m4534 ();
extern "C" void Interpreter_Eval_m4535 ();
extern "C" void Interpreter_EvalChar_m4536 ();
extern "C" void Interpreter_TryMatch_m4537 ();
extern "C" void Interpreter_IsPosition_m4538 ();
extern "C" void Interpreter_IsWordChar_m4539 ();
extern "C" void Interpreter_GetString_m4540 ();
extern "C" void Interpreter_Open_m4541 ();
extern "C" void Interpreter_Close_m4542 ();
extern "C" void Interpreter_Balance_m4543 ();
extern "C" void Interpreter_Checkpoint_m4544 ();
extern "C" void Interpreter_Backtrack_m4545 ();
extern "C" void Interpreter_ResetGroups_m4546 ();
extern "C" void Interpreter_GetLastDefined_m4547 ();
extern "C" void Interpreter_CreateMark_m4548 ();
extern "C" void Interpreter_GetGroupInfo_m4549 ();
extern "C" void Interpreter_PopulateGroup_m4550 ();
extern "C" void Interpreter_GenerateMatch_m4551 ();
extern "C" void Interval__ctor_m4552 ();
extern "C" void Interval_get_Empty_m4553 ();
extern "C" void Interval_get_IsDiscontiguous_m4554 ();
extern "C" void Interval_get_IsSingleton_m4555 ();
extern "C" void Interval_get_IsEmpty_m4556 ();
extern "C" void Interval_get_Size_m4557 ();
extern "C" void Interval_IsDisjoint_m4558 ();
extern "C" void Interval_IsAdjacent_m4559 ();
extern "C" void Interval_Contains_m4560 ();
extern "C" void Interval_Contains_m4561 ();
extern "C" void Interval_Intersects_m4562 ();
extern "C" void Interval_Merge_m4563 ();
extern "C" void Interval_CompareTo_m4564 ();
extern "C" void Enumerator__ctor_m4565 ();
extern "C" void Enumerator_get_Current_m4566 ();
extern "C" void Enumerator_MoveNext_m4567 ();
extern "C" void Enumerator_Reset_m4568 ();
extern "C" void CostDelegate__ctor_m4569 ();
extern "C" void CostDelegate_Invoke_m4570 ();
extern "C" void CostDelegate_BeginInvoke_m4571 ();
extern "C" void CostDelegate_EndInvoke_m4572 ();
extern "C" void IntervalCollection__ctor_m4573 ();
extern "C" void IntervalCollection_get_Item_m4574 ();
extern "C" void IntervalCollection_Add_m4575 ();
extern "C" void IntervalCollection_Normalize_m4576 ();
extern "C" void IntervalCollection_GetMetaCollection_m4577 ();
extern "C" void IntervalCollection_Optimize_m4578 ();
extern "C" void IntervalCollection_get_Count_m4579 ();
extern "C" void IntervalCollection_get_IsSynchronized_m4580 ();
extern "C" void IntervalCollection_get_SyncRoot_m4581 ();
extern "C" void IntervalCollection_CopyTo_m4582 ();
extern "C" void IntervalCollection_GetEnumerator_m4583 ();
extern "C" void Parser__ctor_m4584 ();
extern "C" void Parser_ParseDecimal_m4585 ();
extern "C" void Parser_ParseOctal_m4586 ();
extern "C" void Parser_ParseHex_m4587 ();
extern "C" void Parser_ParseNumber_m4588 ();
extern "C" void Parser_ParseName_m4589 ();
extern "C" void Parser_ParseRegularExpression_m4590 ();
extern "C" void Parser_GetMapping_m4591 ();
extern "C" void Parser_ParseGroup_m4592 ();
extern "C" void Parser_ParseGroupingConstruct_m4593 ();
extern "C" void Parser_ParseAssertionType_m4594 ();
extern "C" void Parser_ParseOptions_m4595 ();
extern "C" void Parser_ParseCharacterClass_m4596 ();
extern "C" void Parser_ParseRepetitionBounds_m4597 ();
extern "C" void Parser_ParseUnicodeCategory_m4598 ();
extern "C" void Parser_ParseSpecial_m4599 ();
extern "C" void Parser_ParseEscape_m4600 ();
extern "C" void Parser_ParseName_m4601 ();
extern "C" void Parser_IsNameChar_m4602 ();
extern "C" void Parser_ParseNumber_m4603 ();
extern "C" void Parser_ParseDigit_m4604 ();
extern "C" void Parser_ConsumeWhitespace_m4605 ();
extern "C" void Parser_ResolveReferences_m4606 ();
extern "C" void Parser_HandleExplicitNumericGroups_m4607 ();
extern "C" void Parser_IsIgnoreCase_m4608 ();
extern "C" void Parser_IsMultiline_m4609 ();
extern "C" void Parser_IsExplicitCapture_m4610 ();
extern "C" void Parser_IsSingleline_m4611 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m4612 ();
extern "C" void Parser_IsECMAScript_m4613 ();
extern "C" void Parser_NewParseException_m4614 ();
extern "C" void QuickSearch__ctor_m4615 ();
extern "C" void QuickSearch__cctor_m4616 ();
extern "C" void QuickSearch_get_Length_m4617 ();
extern "C" void QuickSearch_Search_m4618 ();
extern "C" void QuickSearch_SetupShiftTable_m4619 ();
extern "C" void QuickSearch_GetShiftDistance_m4620 ();
extern "C" void QuickSearch_GetChar_m4621 ();
extern "C" void ReplacementEvaluator__ctor_m4622 ();
extern "C" void ReplacementEvaluator_Evaluate_m4623 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m4624 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4625 ();
extern "C" void ReplacementEvaluator_Ensure_m4626 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m4627 ();
extern "C" void ReplacementEvaluator_AddInt_m4628 ();
extern "C" void ReplacementEvaluator_Compile_m4629 ();
extern "C" void ReplacementEvaluator_CompileTerm_m4630 ();
extern "C" void ExpressionCollection__ctor_m4631 ();
extern "C" void ExpressionCollection_Add_m4632 ();
extern "C" void ExpressionCollection_get_Item_m4633 ();
extern "C" void ExpressionCollection_set_Item_m4634 ();
extern "C" void ExpressionCollection_OnValidate_m4635 ();
extern "C" void Expression__ctor_m4636 ();
extern "C" void Expression_GetFixedWidth_m4637 ();
extern "C" void Expression_GetAnchorInfo_m4638 ();
extern "C" void CompositeExpression__ctor_m4639 ();
extern "C" void CompositeExpression_get_Expressions_m4640 ();
extern "C" void CompositeExpression_GetWidth_m4641 ();
extern "C" void CompositeExpression_IsComplex_m4642 ();
extern "C" void Group__ctor_m4643 ();
extern "C" void Group_AppendExpression_m4644 ();
extern "C" void Group_Compile_m4645 ();
extern "C" void Group_GetWidth_m4646 ();
extern "C" void Group_GetAnchorInfo_m4647 ();
extern "C" void RegularExpression__ctor_m4648 ();
extern "C" void RegularExpression_set_GroupCount_m4649 ();
extern "C" void RegularExpression_Compile_m4650 ();
extern "C" void CapturingGroup__ctor_m4651 ();
extern "C" void CapturingGroup_get_Index_m4652 ();
extern "C" void CapturingGroup_set_Index_m4653 ();
extern "C" void CapturingGroup_get_Name_m4654 ();
extern "C" void CapturingGroup_set_Name_m4655 ();
extern "C" void CapturingGroup_get_IsNamed_m4656 ();
extern "C" void CapturingGroup_Compile_m4657 ();
extern "C" void CapturingGroup_IsComplex_m4658 ();
extern "C" void CapturingGroup_CompareTo_m4659 ();
extern "C" void BalancingGroup__ctor_m4660 ();
extern "C" void BalancingGroup_set_Balance_m4661 ();
extern "C" void BalancingGroup_Compile_m4662 ();
extern "C" void NonBacktrackingGroup__ctor_m4663 ();
extern "C" void NonBacktrackingGroup_Compile_m4664 ();
extern "C" void NonBacktrackingGroup_IsComplex_m4665 ();
extern "C" void Repetition__ctor_m4666 ();
extern "C" void Repetition_get_Expression_m4667 ();
extern "C" void Repetition_set_Expression_m4668 ();
extern "C" void Repetition_get_Minimum_m4669 ();
extern "C" void Repetition_Compile_m4670 ();
extern "C" void Repetition_GetWidth_m4671 ();
extern "C" void Repetition_GetAnchorInfo_m4672 ();
extern "C" void Assertion__ctor_m4673 ();
extern "C" void Assertion_get_TrueExpression_m4674 ();
extern "C" void Assertion_set_TrueExpression_m4675 ();
extern "C" void Assertion_get_FalseExpression_m4676 ();
extern "C" void Assertion_set_FalseExpression_m4677 ();
extern "C" void Assertion_GetWidth_m4678 ();
extern "C" void CaptureAssertion__ctor_m4679 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m4680 ();
extern "C" void CaptureAssertion_Compile_m4681 ();
extern "C" void CaptureAssertion_IsComplex_m4682 ();
extern "C" void CaptureAssertion_get_Alternate_m4683 ();
extern "C" void ExpressionAssertion__ctor_m4684 ();
extern "C" void ExpressionAssertion_set_Reverse_m4685 ();
extern "C" void ExpressionAssertion_set_Negate_m4686 ();
extern "C" void ExpressionAssertion_get_TestExpression_m4687 ();
extern "C" void ExpressionAssertion_set_TestExpression_m4688 ();
extern "C" void ExpressionAssertion_Compile_m4689 ();
extern "C" void ExpressionAssertion_IsComplex_m4690 ();
extern "C" void Alternation__ctor_m4691 ();
extern "C" void Alternation_get_Alternatives_m4692 ();
extern "C" void Alternation_AddAlternative_m4693 ();
extern "C" void Alternation_Compile_m4694 ();
extern "C" void Alternation_GetWidth_m4695 ();
extern "C" void Literal__ctor_m4696 ();
extern "C" void Literal_CompileLiteral_m4697 ();
extern "C" void Literal_Compile_m4698 ();
extern "C" void Literal_GetWidth_m4699 ();
extern "C" void Literal_GetAnchorInfo_m4700 ();
extern "C" void Literal_IsComplex_m4701 ();
extern "C" void PositionAssertion__ctor_m4702 ();
extern "C" void PositionAssertion_Compile_m4703 ();
extern "C" void PositionAssertion_GetWidth_m4704 ();
extern "C" void PositionAssertion_IsComplex_m4705 ();
extern "C" void PositionAssertion_GetAnchorInfo_m4706 ();
extern "C" void Reference__ctor_m4707 ();
extern "C" void Reference_get_CapturingGroup_m4708 ();
extern "C" void Reference_set_CapturingGroup_m4709 ();
extern "C" void Reference_get_IgnoreCase_m4710 ();
extern "C" void Reference_Compile_m4711 ();
extern "C" void Reference_GetWidth_m4712 ();
extern "C" void Reference_IsComplex_m4713 ();
extern "C" void BackslashNumber__ctor_m4714 ();
extern "C" void BackslashNumber_ResolveReference_m4715 ();
extern "C" void BackslashNumber_Compile_m4716 ();
extern "C" void CharacterClass__ctor_m4717 ();
extern "C" void CharacterClass__ctor_m4718 ();
extern "C" void CharacterClass__cctor_m4719 ();
extern "C" void CharacterClass_AddCategory_m4720 ();
extern "C" void CharacterClass_AddCharacter_m4721 ();
extern "C" void CharacterClass_AddRange_m4722 ();
extern "C" void CharacterClass_Compile_m4723 ();
extern "C" void CharacterClass_GetWidth_m4724 ();
extern "C" void CharacterClass_IsComplex_m4725 ();
extern "C" void CharacterClass_GetIntervalCost_m4726 ();
extern "C" void AnchorInfo__ctor_m4727 ();
extern "C" void AnchorInfo__ctor_m4728 ();
extern "C" void AnchorInfo__ctor_m4729 ();
extern "C" void AnchorInfo_get_Offset_m4730 ();
extern "C" void AnchorInfo_get_Width_m4731 ();
extern "C" void AnchorInfo_get_Length_m4732 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m4733 ();
extern "C" void AnchorInfo_get_IsComplete_m4734 ();
extern "C" void AnchorInfo_get_Substring_m4735 ();
extern "C" void AnchorInfo_get_IgnoreCase_m4736 ();
extern "C" void AnchorInfo_get_Position_m4737 ();
extern "C" void AnchorInfo_get_IsSubstring_m4738 ();
extern "C" void AnchorInfo_get_IsPosition_m4739 ();
extern "C" void AnchorInfo_GetInterval_m4740 ();
extern "C" void DefaultUriParser__ctor_m4741 ();
extern "C" void DefaultUriParser__ctor_m4742 ();
extern "C" void UriScheme__ctor_m4743 ();
extern "C" void Uri__ctor_m4744 ();
extern "C" void Uri__ctor_m4745 ();
extern "C" void Uri__ctor_m4746 ();
extern "C" void Uri__cctor_m4747 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4748 ();
extern "C" void Uri_get_AbsoluteUri_m4749 ();
extern "C" void Uri_get_Authority_m4750 ();
extern "C" void Uri_get_Host_m4751 ();
extern "C" void Uri_get_IsFile_m4752 ();
extern "C" void Uri_get_IsLoopback_m4753 ();
extern "C" void Uri_get_IsUnc_m4754 ();
extern "C" void Uri_get_Scheme_m4755 ();
extern "C" void Uri_get_IsAbsoluteUri_m4756 ();
extern "C" void Uri_CheckHostName_m4757 ();
extern "C" void Uri_IsIPv4Address_m4758 ();
extern "C" void Uri_IsDomainAddress_m4759 ();
extern "C" void Uri_CheckSchemeName_m4760 ();
extern "C" void Uri_IsAlpha_m4761 ();
extern "C" void Uri_Equals_m4762 ();
extern "C" void Uri_InternalEquals_m4763 ();
extern "C" void Uri_GetHashCode_m4764 ();
extern "C" void Uri_GetLeftPart_m4765 ();
extern "C" void Uri_FromHex_m4766 ();
extern "C" void Uri_HexEscape_m4767 ();
extern "C" void Uri_IsHexDigit_m4768 ();
extern "C" void Uri_IsHexEncoding_m4769 ();
extern "C" void Uri_AppendQueryAndFragment_m4770 ();
extern "C" void Uri_ToString_m4771 ();
extern "C" void Uri_EscapeString_m4772 ();
extern "C" void Uri_EscapeString_m4773 ();
extern "C" void Uri_ParseUri_m4774 ();
extern "C" void Uri_Unescape_m4775 ();
extern "C" void Uri_Unescape_m4776 ();
extern "C" void Uri_ParseAsWindowsUNC_m4777 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m4778 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m4779 ();
extern "C" void Uri_Parse_m4780 ();
extern "C" void Uri_ParseNoExceptions_m4781 ();
extern "C" void Uri_CompactEscaped_m4782 ();
extern "C" void Uri_Reduce_m4783 ();
extern "C" void Uri_HexUnescapeMultiByte_m4784 ();
extern "C" void Uri_GetSchemeDelimiter_m4785 ();
extern "C" void Uri_GetDefaultPort_m4786 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m4787 ();
extern "C" void Uri_IsPredefinedScheme_m4788 ();
extern "C" void Uri_get_Parser_m4789 ();
extern "C" void Uri_EnsureAbsoluteUri_m4790 ();
extern "C" void Uri_op_Equality_m4791 ();
extern "C" void Uri_op_Inequality_m406 ();
extern "C" void UriFormatException__ctor_m4792 ();
extern "C" void UriFormatException__ctor_m4793 ();
extern "C" void UriFormatException__ctor_m4794 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4795 ();
extern "C" void UriParser__ctor_m4796 ();
extern "C" void UriParser__cctor_m4797 ();
extern "C" void UriParser_InitializeAndValidate_m4798 ();
extern "C" void UriParser_OnRegister_m4799 ();
extern "C" void UriParser_set_SchemeName_m4800 ();
extern "C" void UriParser_get_DefaultPort_m4801 ();
extern "C" void UriParser_set_DefaultPort_m4802 ();
extern "C" void UriParser_CreateDefaults_m4803 ();
extern "C" void UriParser_InternalRegister_m4804 ();
extern "C" void UriParser_GetParser_m4805 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m4806 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m4807 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m4808 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m4809 ();
extern "C" void MatchEvaluator__ctor_m4810 ();
extern "C" void MatchEvaluator_Invoke_m4811 ();
extern "C" void MatchEvaluator_BeginInvoke_m4812 ();
extern "C" void MatchEvaluator_EndInvoke_m4813 ();
extern "C" void Extensions_op_Implicit_m678 ();
extern "C" void Locale_GetText_m4980 ();
extern "C" void ModulusRing__ctor_m4981 ();
extern "C" void ModulusRing_BarrettReduction_m4982 ();
extern "C" void ModulusRing_Multiply_m4983 ();
extern "C" void ModulusRing_Difference_m4984 ();
extern "C" void ModulusRing_Pow_m4985 ();
extern "C" void ModulusRing_Pow_m4986 ();
extern "C" void Kernel_AddSameSign_m4987 ();
extern "C" void Kernel_Subtract_m4988 ();
extern "C" void Kernel_MinusEq_m4989 ();
extern "C" void Kernel_PlusEq_m4990 ();
extern "C" void Kernel_Compare_m4991 ();
extern "C" void Kernel_SingleByteDivideInPlace_m4992 ();
extern "C" void Kernel_DwordMod_m4993 ();
extern "C" void Kernel_DwordDivMod_m4994 ();
extern "C" void Kernel_multiByteDivide_m4995 ();
extern "C" void Kernel_LeftShift_m4996 ();
extern "C" void Kernel_RightShift_m4997 ();
extern "C" void Kernel_Multiply_m4998 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m4999 ();
extern "C" void Kernel_modInverse_m5000 ();
extern "C" void Kernel_modInverse_m5001 ();
extern "C" void BigInteger__ctor_m5002 ();
extern "C" void BigInteger__ctor_m5003 ();
extern "C" void BigInteger__ctor_m5004 ();
extern "C" void BigInteger__ctor_m5005 ();
extern "C" void BigInteger__ctor_m5006 ();
extern "C" void BigInteger__cctor_m5007 ();
extern "C" void BigInteger_get_Rng_m5008 ();
extern "C" void BigInteger_GenerateRandom_m5009 ();
extern "C" void BigInteger_GenerateRandom_m5010 ();
extern "C" void BigInteger_BitCount_m5011 ();
extern "C" void BigInteger_TestBit_m5012 ();
extern "C" void BigInteger_SetBit_m5013 ();
extern "C" void BigInteger_SetBit_m5014 ();
extern "C" void BigInteger_LowestSetBit_m5015 ();
extern "C" void BigInteger_GetBytes_m5016 ();
extern "C" void BigInteger_ToString_m5017 ();
extern "C" void BigInteger_ToString_m5018 ();
extern "C" void BigInteger_Normalize_m5019 ();
extern "C" void BigInteger_Clear_m5020 ();
extern "C" void BigInteger_GetHashCode_m5021 ();
extern "C" void BigInteger_ToString_m5022 ();
extern "C" void BigInteger_Equals_m5023 ();
extern "C" void BigInteger_ModInverse_m5024 ();
extern "C" void BigInteger_ModPow_m5025 ();
extern "C" void BigInteger_GeneratePseudoPrime_m5026 ();
extern "C" void BigInteger_Incr2_m5027 ();
extern "C" void BigInteger_op_Implicit_m5028 ();
extern "C" void BigInteger_op_Implicit_m5029 ();
extern "C" void BigInteger_op_Addition_m5030 ();
extern "C" void BigInteger_op_Subtraction_m5031 ();
extern "C" void BigInteger_op_Modulus_m5032 ();
extern "C" void BigInteger_op_Modulus_m5033 ();
extern "C" void BigInteger_op_Division_m5034 ();
extern "C" void BigInteger_op_Multiply_m5035 ();
extern "C" void BigInteger_op_LeftShift_m5036 ();
extern "C" void BigInteger_op_RightShift_m5037 ();
extern "C" void BigInteger_op_Equality_m5038 ();
extern "C" void BigInteger_op_Inequality_m5039 ();
extern "C" void BigInteger_op_Equality_m5040 ();
extern "C" void BigInteger_op_Inequality_m5041 ();
extern "C" void BigInteger_op_GreaterThan_m5042 ();
extern "C" void BigInteger_op_LessThan_m5043 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m5044 ();
extern "C" void BigInteger_op_LessThanOrEqual_m5045 ();
extern "C" void PrimalityTests_GetSPPRounds_m5046 ();
extern "C" void PrimalityTests_RabinMillerTest_m5047 ();
extern "C" void PrimeGeneratorBase__ctor_m5048 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m5049 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m5050 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m5051 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m5052 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m5053 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m5054 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m5055 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m5056 ();
extern "C" void ASN1__ctor_m4882 ();
extern "C" void ASN1__ctor_m4883 ();
extern "C" void ASN1__ctor_m4867 ();
extern "C" void ASN1_get_Count_m4870 ();
extern "C" void ASN1_get_Tag_m4868 ();
extern "C" void ASN1_get_Length_m4895 ();
extern "C" void ASN1_get_Value_m4869 ();
extern "C" void ASN1_set_Value_m5057 ();
extern "C" void ASN1_CompareArray_m5058 ();
extern "C" void ASN1_CompareValue_m4894 ();
extern "C" void ASN1_Add_m4884 ();
extern "C" void ASN1_GetBytes_m5059 ();
extern "C" void ASN1_Decode_m5060 ();
extern "C" void ASN1_DecodeTLV_m5061 ();
extern "C" void ASN1_get_Item_m4871 ();
extern "C" void ASN1_Element_m5062 ();
extern "C" void ASN1_ToString_m5063 ();
extern "C" void ASN1Convert_FromInt32_m4885 ();
extern "C" void ASN1Convert_FromOid_m5064 ();
extern "C" void ASN1Convert_ToInt32_m4881 ();
extern "C" void ASN1Convert_ToOid_m4934 ();
extern "C" void ASN1Convert_ToDateTime_m5065 ();
extern "C" void BitConverterLE_GetUIntBytes_m5066 ();
extern "C" void BitConverterLE_GetBytes_m5067 ();
extern "C" void ContentInfo__ctor_m5068 ();
extern "C" void ContentInfo__ctor_m5069 ();
extern "C" void ContentInfo__ctor_m5070 ();
extern "C" void ContentInfo__ctor_m5071 ();
extern "C" void ContentInfo_get_ASN1_m5072 ();
extern "C" void ContentInfo_get_Content_m5073 ();
extern "C" void ContentInfo_set_Content_m5074 ();
extern "C" void ContentInfo_get_ContentType_m5075 ();
extern "C" void ContentInfo_set_ContentType_m5076 ();
extern "C" void ContentInfo_GetASN1_m5077 ();
extern "C" void EncryptedData__ctor_m5078 ();
extern "C" void EncryptedData__ctor_m5079 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m5080 ();
extern "C" void EncryptedData_get_EncryptedContent_m5081 ();
extern "C" void ARC4Managed__ctor_m5082 ();
extern "C" void ARC4Managed_Finalize_m5083 ();
extern "C" void ARC4Managed_Dispose_m5084 ();
extern "C" void ARC4Managed_get_Key_m5085 ();
extern "C" void ARC4Managed_set_Key_m5086 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m5087 ();
extern "C" void ARC4Managed_CreateEncryptor_m5088 ();
extern "C" void ARC4Managed_CreateDecryptor_m5089 ();
extern "C" void ARC4Managed_GenerateIV_m5090 ();
extern "C" void ARC4Managed_GenerateKey_m5091 ();
extern "C" void ARC4Managed_KeySetup_m5092 ();
extern "C" void ARC4Managed_CheckInput_m5093 ();
extern "C" void ARC4Managed_TransformBlock_m5094 ();
extern "C" void ARC4Managed_InternalTransformBlock_m5095 ();
extern "C" void ARC4Managed_TransformFinalBlock_m5096 ();
extern "C" void CryptoConvert_ToHex_m4948 ();
extern "C" void KeyBuilder_get_Rng_m5097 ();
extern "C" void KeyBuilder_Key_m5098 ();
extern "C" void MD2__ctor_m5099 ();
extern "C" void MD2_Create_m5100 ();
extern "C" void MD2_Create_m5101 ();
extern "C" void MD2Managed__ctor_m5102 ();
extern "C" void MD2Managed__cctor_m5103 ();
extern "C" void MD2Managed_Padding_m5104 ();
extern "C" void MD2Managed_Initialize_m5105 ();
extern "C" void MD2Managed_HashCore_m5106 ();
extern "C" void MD2Managed_HashFinal_m5107 ();
extern "C" void MD2Managed_MD2Transform_m5108 ();
extern "C" void PKCS1__cctor_m5109 ();
extern "C" void PKCS1_Compare_m5110 ();
extern "C" void PKCS1_I2OSP_m5111 ();
extern "C" void PKCS1_OS2IP_m5112 ();
extern "C" void PKCS1_RSASP1_m5113 ();
extern "C" void PKCS1_RSAVP1_m5114 ();
extern "C" void PKCS1_Sign_v15_m5115 ();
extern "C" void PKCS1_Verify_v15_m5116 ();
extern "C" void PKCS1_Verify_v15_m5117 ();
extern "C" void PKCS1_Encode_v15_m5118 ();
extern "C" void PrivateKeyInfo__ctor_m5119 ();
extern "C" void PrivateKeyInfo__ctor_m5120 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m5121 ();
extern "C" void PrivateKeyInfo_Decode_m5122 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m5123 ();
extern "C" void PrivateKeyInfo_Normalize_m5124 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m5125 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m5126 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m5127 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m5128 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m5129 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m5130 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m5131 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m5132 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m5133 ();
extern "C" void RC4__ctor_m5134 ();
extern "C" void RC4__cctor_m5135 ();
extern "C" void RC4_get_IV_m5136 ();
extern "C" void RC4_set_IV_m5137 ();
extern "C" void KeyGeneratedEventHandler__ctor_m5138 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m5139 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m5140 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m5141 ();
extern "C" void RSAManaged__ctor_m5142 ();
extern "C" void RSAManaged__ctor_m5143 ();
extern "C" void RSAManaged_Finalize_m5144 ();
extern "C" void RSAManaged_GenerateKeyPair_m5145 ();
extern "C" void RSAManaged_get_KeySize_m5146 ();
extern "C" void RSAManaged_get_PublicOnly_m4862 ();
extern "C" void RSAManaged_DecryptValue_m5147 ();
extern "C" void RSAManaged_EncryptValue_m5148 ();
extern "C" void RSAManaged_ExportParameters_m5149 ();
extern "C" void RSAManaged_ImportParameters_m5150 ();
extern "C" void RSAManaged_Dispose_m5151 ();
extern "C" void RSAManaged_ToXmlString_m5152 ();
extern "C" void RSAManaged_GetPaddedValue_m5153 ();
extern "C" void SafeBag__ctor_m5154 ();
extern "C" void SafeBag_get_BagOID_m5155 ();
extern "C" void SafeBag_get_ASN1_m5156 ();
extern "C" void DeriveBytes__ctor_m5157 ();
extern "C" void DeriveBytes__cctor_m5158 ();
extern "C" void DeriveBytes_set_HashName_m5159 ();
extern "C" void DeriveBytes_set_IterationCount_m5160 ();
extern "C" void DeriveBytes_set_Password_m5161 ();
extern "C" void DeriveBytes_set_Salt_m5162 ();
extern "C" void DeriveBytes_Adjust_m5163 ();
extern "C" void DeriveBytes_Derive_m5164 ();
extern "C" void DeriveBytes_DeriveKey_m5165 ();
extern "C" void DeriveBytes_DeriveIV_m5166 ();
extern "C" void DeriveBytes_DeriveMAC_m5167 ();
extern "C" void PKCS12__ctor_m5168 ();
extern "C" void PKCS12__ctor_m4896 ();
extern "C" void PKCS12__ctor_m4897 ();
extern "C" void PKCS12__cctor_m5169 ();
extern "C" void PKCS12_Decode_m5170 ();
extern "C" void PKCS12_Finalize_m5171 ();
extern "C" void PKCS12_set_Password_m5172 ();
extern "C" void PKCS12_get_IterationCount_m5173 ();
extern "C" void PKCS12_set_IterationCount_m5174 ();
extern "C" void PKCS12_get_Keys_m4900 ();
extern "C" void PKCS12_get_Certificates_m4898 ();
extern "C" void PKCS12_get_RNG_m5175 ();
extern "C" void PKCS12_Compare_m5176 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m5177 ();
extern "C" void PKCS12_Decrypt_m5178 ();
extern "C" void PKCS12_Decrypt_m5179 ();
extern "C" void PKCS12_Encrypt_m5180 ();
extern "C" void PKCS12_GetExistingParameters_m5181 ();
extern "C" void PKCS12_AddPrivateKey_m5182 ();
extern "C" void PKCS12_ReadSafeBag_m5183 ();
extern "C" void PKCS12_CertificateSafeBag_m5184 ();
extern "C" void PKCS12_MAC_m5185 ();
extern "C" void PKCS12_GetBytes_m5186 ();
extern "C" void PKCS12_EncryptedContentInfo_m5187 ();
extern "C" void PKCS12_AddCertificate_m5188 ();
extern "C" void PKCS12_AddCertificate_m5189 ();
extern "C" void PKCS12_RemoveCertificate_m5190 ();
extern "C" void PKCS12_RemoveCertificate_m5191 ();
extern "C" void PKCS12_Clone_m5192 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m5193 ();
extern "C" void X501__cctor_m5194 ();
extern "C" void X501_ToString_m5195 ();
extern "C" void X501_ToString_m4875 ();
extern "C" void X501_AppendEntry_m5196 ();
extern "C" void X509Certificate__ctor_m4903 ();
extern "C" void X509Certificate__cctor_m5197 ();
extern "C" void X509Certificate_Parse_m5198 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m5199 ();
extern "C" void X509Certificate_get_DSA_m4864 ();
extern "C" void X509Certificate_set_DSA_m4901 ();
extern "C" void X509Certificate_get_Extensions_m4920 ();
extern "C" void X509Certificate_get_Hash_m5200 ();
extern "C" void X509Certificate_get_IssuerName_m5201 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m5202 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m5203 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m5204 ();
extern "C" void X509Certificate_get_PublicKey_m5205 ();
extern "C" void X509Certificate_get_RSA_m5206 ();
extern "C" void X509Certificate_set_RSA_m5207 ();
extern "C" void X509Certificate_get_RawData_m5208 ();
extern "C" void X509Certificate_get_SerialNumber_m5209 ();
extern "C" void X509Certificate_get_Signature_m5210 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m5211 ();
extern "C" void X509Certificate_get_SubjectName_m5212 ();
extern "C" void X509Certificate_get_ValidFrom_m5213 ();
extern "C" void X509Certificate_get_ValidUntil_m5214 ();
extern "C" void X509Certificate_get_Version_m4893 ();
extern "C" void X509Certificate_get_IsCurrent_m5215 ();
extern "C" void X509Certificate_WasCurrent_m5216 ();
extern "C" void X509Certificate_VerifySignature_m5217 ();
extern "C" void X509Certificate_VerifySignature_m5218 ();
extern "C" void X509Certificate_VerifySignature_m4919 ();
extern "C" void X509Certificate_get_IsSelfSigned_m5219 ();
extern "C" void X509Certificate_GetIssuerName_m4888 ();
extern "C" void X509Certificate_GetSubjectName_m4891 ();
extern "C" void X509Certificate_GetObjectData_m5220 ();
extern "C" void X509Certificate_PEM_m5221 ();
extern "C" void X509CertificateEnumerator__ctor_m5222 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m5223 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m5224 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m5225 ();
extern "C" void X509CertificateEnumerator_get_Current_m4945 ();
extern "C" void X509CertificateEnumerator_MoveNext_m5226 ();
extern "C" void X509CertificateEnumerator_Reset_m5227 ();
extern "C" void X509CertificateCollection__ctor_m5228 ();
extern "C" void X509CertificateCollection__ctor_m5229 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m5230 ();
extern "C" void X509CertificateCollection_get_Item_m4899 ();
extern "C" void X509CertificateCollection_Add_m5231 ();
extern "C" void X509CertificateCollection_AddRange_m5232 ();
extern "C" void X509CertificateCollection_Contains_m5233 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4944 ();
extern "C" void X509CertificateCollection_GetHashCode_m5234 ();
extern "C" void X509CertificateCollection_IndexOf_m5235 ();
extern "C" void X509CertificateCollection_Remove_m5236 ();
extern "C" void X509CertificateCollection_Compare_m5237 ();
extern "C" void X509Chain__ctor_m5238 ();
extern "C" void X509Chain__ctor_m5239 ();
extern "C" void X509Chain_get_Status_m5240 ();
extern "C" void X509Chain_get_TrustAnchors_m5241 ();
extern "C" void X509Chain_Build_m5242 ();
extern "C" void X509Chain_IsValid_m5243 ();
extern "C" void X509Chain_FindCertificateParent_m5244 ();
extern "C" void X509Chain_FindCertificateRoot_m5245 ();
extern "C" void X509Chain_IsTrusted_m5246 ();
extern "C" void X509Chain_IsParent_m5247 ();
extern "C" void X509CrlEntry__ctor_m5248 ();
extern "C" void X509CrlEntry_get_SerialNumber_m5249 ();
extern "C" void X509CrlEntry_get_RevocationDate_m4927 ();
extern "C" void X509CrlEntry_get_Extensions_m4933 ();
extern "C" void X509Crl__ctor_m5250 ();
extern "C" void X509Crl_Parse_m5251 ();
extern "C" void X509Crl_get_Extensions_m4922 ();
extern "C" void X509Crl_get_Hash_m5252 ();
extern "C" void X509Crl_get_IssuerName_m4930 ();
extern "C" void X509Crl_get_NextUpdate_m4928 ();
extern "C" void X509Crl_Compare_m5253 ();
extern "C" void X509Crl_GetCrlEntry_m4926 ();
extern "C" void X509Crl_GetCrlEntry_m5254 ();
extern "C" void X509Crl_GetHashName_m5255 ();
extern "C" void X509Crl_VerifySignature_m5256 ();
extern "C" void X509Crl_VerifySignature_m5257 ();
extern "C" void X509Crl_VerifySignature_m4925 ();
extern "C" void X509Extension__ctor_m5258 ();
extern "C" void X509Extension__ctor_m5259 ();
extern "C" void X509Extension_Decode_m5260 ();
extern "C" void X509Extension_Encode_m5261 ();
extern "C" void X509Extension_get_Oid_m4932 ();
extern "C" void X509Extension_get_Critical_m4931 ();
extern "C" void X509Extension_get_Value_m4936 ();
extern "C" void X509Extension_Equals_m5262 ();
extern "C" void X509Extension_GetHashCode_m5263 ();
extern "C" void X509Extension_WriteLine_m5264 ();
extern "C" void X509Extension_ToString_m5265 ();
extern "C" void X509ExtensionCollection__ctor_m5266 ();
extern "C" void X509ExtensionCollection__ctor_m5267 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m5268 ();
extern "C" void X509ExtensionCollection_IndexOf_m5269 ();
extern "C" void X509ExtensionCollection_get_Item_m4921 ();
extern "C" void X509Store__ctor_m5270 ();
extern "C" void X509Store_get_Certificates_m4943 ();
extern "C" void X509Store_get_Crls_m4929 ();
extern "C" void X509Store_Load_m5271 ();
extern "C" void X509Store_LoadCertificate_m5272 ();
extern "C" void X509Store_LoadCrl_m5273 ();
extern "C" void X509Store_CheckStore_m5274 ();
extern "C" void X509Store_BuildCertificatesCollection_m5275 ();
extern "C" void X509Store_BuildCrlsCollection_m5276 ();
extern "C" void X509StoreManager_get_CurrentUser_m4940 ();
extern "C" void X509StoreManager_get_LocalMachine_m4941 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m5277 ();
extern "C" void X509Stores__ctor_m5278 ();
extern "C" void X509Stores_get_TrustedRoot_m5279 ();
extern "C" void X509Stores_Open_m4942 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m4923 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m5280 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m4924 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m5281 ();
extern "C" void BasicConstraintsExtension__ctor_m5282 ();
extern "C" void BasicConstraintsExtension_Decode_m5283 ();
extern "C" void BasicConstraintsExtension_Encode_m5284 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m5285 ();
extern "C" void BasicConstraintsExtension_ToString_m5286 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m5287 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m5288 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m5289 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m5290 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m5291 ();
extern "C" void GeneralNames__ctor_m5292 ();
extern "C" void GeneralNames_get_DNSNames_m5293 ();
extern "C" void GeneralNames_get_IPAddresses_m5294 ();
extern "C" void GeneralNames_ToString_m5295 ();
extern "C" void KeyUsageExtension__ctor_m5296 ();
extern "C" void KeyUsageExtension_Decode_m5297 ();
extern "C" void KeyUsageExtension_Encode_m5298 ();
extern "C" void KeyUsageExtension_Support_m5299 ();
extern "C" void KeyUsageExtension_ToString_m5300 ();
extern "C" void NetscapeCertTypeExtension__ctor_m5301 ();
extern "C" void NetscapeCertTypeExtension_Decode_m5302 ();
extern "C" void NetscapeCertTypeExtension_Support_m5303 ();
extern "C" void NetscapeCertTypeExtension_ToString_m5304 ();
extern "C" void SubjectAltNameExtension__ctor_m5305 ();
extern "C" void SubjectAltNameExtension_Decode_m5306 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m5307 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m5308 ();
extern "C" void SubjectAltNameExtension_ToString_m5309 ();
extern "C" void HMAC__ctor_m5310 ();
extern "C" void HMAC_get_Key_m5311 ();
extern "C" void HMAC_set_Key_m5312 ();
extern "C" void HMAC_Initialize_m5313 ();
extern "C" void HMAC_HashFinal_m5314 ();
extern "C" void HMAC_HashCore_m5315 ();
extern "C" void HMAC_initializePad_m5316 ();
extern "C" void MD5SHA1__ctor_m5317 ();
extern "C" void MD5SHA1_Initialize_m5318 ();
extern "C" void MD5SHA1_HashFinal_m5319 ();
extern "C" void MD5SHA1_HashCore_m5320 ();
extern "C" void MD5SHA1_CreateSignature_m5321 ();
extern "C" void MD5SHA1_VerifySignature_m5322 ();
extern "C" void Alert__ctor_m5323 ();
extern "C" void Alert__ctor_m5324 ();
extern "C" void Alert_get_Level_m5325 ();
extern "C" void Alert_get_Description_m5326 ();
extern "C" void Alert_get_IsWarning_m5327 ();
extern "C" void Alert_get_IsCloseNotify_m5328 ();
extern "C" void Alert_inferAlertLevel_m5329 ();
extern "C" void Alert_GetAlertMessage_m5330 ();
extern "C" void CipherSuite__ctor_m5331 ();
extern "C" void CipherSuite__cctor_m5332 ();
extern "C" void CipherSuite_get_EncryptionCipher_m5333 ();
extern "C" void CipherSuite_get_DecryptionCipher_m5334 ();
extern "C" void CipherSuite_get_ClientHMAC_m5335 ();
extern "C" void CipherSuite_get_ServerHMAC_m5336 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m5337 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m5338 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m5339 ();
extern "C" void CipherSuite_get_HashSize_m5340 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m5341 ();
extern "C" void CipherSuite_get_CipherMode_m5342 ();
extern "C" void CipherSuite_get_Code_m5343 ();
extern "C" void CipherSuite_get_Name_m5344 ();
extern "C" void CipherSuite_get_IsExportable_m5345 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m5346 ();
extern "C" void CipherSuite_get_KeyBlockSize_m5347 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m5348 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m5349 ();
extern "C" void CipherSuite_get_IvSize_m5350 ();
extern "C" void CipherSuite_get_Context_m5351 ();
extern "C" void CipherSuite_set_Context_m5352 ();
extern "C" void CipherSuite_Write_m5353 ();
extern "C" void CipherSuite_Write_m5354 ();
extern "C" void CipherSuite_InitializeCipher_m5355 ();
extern "C" void CipherSuite_EncryptRecord_m5356 ();
extern "C" void CipherSuite_DecryptRecord_m5357 ();
extern "C" void CipherSuite_CreatePremasterSecret_m5358 ();
extern "C" void CipherSuite_PRF_m5359 ();
extern "C" void CipherSuite_Expand_m5360 ();
extern "C" void CipherSuite_createEncryptionCipher_m5361 ();
extern "C" void CipherSuite_createDecryptionCipher_m5362 ();
extern "C" void CipherSuiteCollection__ctor_m5363 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m5364 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m5365 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m5366 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m5367 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m5368 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m5369 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m5370 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m5371 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m5372 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m5373 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m5374 ();
extern "C" void CipherSuiteCollection_get_Item_m5375 ();
extern "C" void CipherSuiteCollection_get_Item_m5376 ();
extern "C" void CipherSuiteCollection_set_Item_m5377 ();
extern "C" void CipherSuiteCollection_get_Item_m5378 ();
extern "C" void CipherSuiteCollection_get_Count_m5379 ();
extern "C" void CipherSuiteCollection_get_IsFixedSize_m5380 ();
extern "C" void CipherSuiteCollection_get_IsReadOnly_m5381 ();
extern "C" void CipherSuiteCollection_CopyTo_m5382 ();
extern "C" void CipherSuiteCollection_Clear_m5383 ();
extern "C" void CipherSuiteCollection_IndexOf_m5384 ();
extern "C" void CipherSuiteCollection_IndexOf_m5385 ();
extern "C" void CipherSuiteCollection_Add_m5386 ();
extern "C" void CipherSuiteCollection_add_m5387 ();
extern "C" void CipherSuiteCollection_add_m5388 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m5389 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m5390 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m5391 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m5392 ();
extern "C" void ClientContext__ctor_m5393 ();
extern "C" void ClientContext_get_SslStream_m5394 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m5395 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m5396 ();
extern "C" void ClientContext_Clear_m5397 ();
extern "C" void ClientRecordProtocol__ctor_m5398 ();
extern "C" void ClientRecordProtocol_GetMessage_m5399 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m5400 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m5401 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m5402 ();
extern "C" void ClientSessionInfo__ctor_m5403 ();
extern "C" void ClientSessionInfo__cctor_m5404 ();
extern "C" void ClientSessionInfo_Finalize_m5405 ();
extern "C" void ClientSessionInfo_get_HostName_m5406 ();
extern "C" void ClientSessionInfo_get_Id_m5407 ();
extern "C" void ClientSessionInfo_get_Valid_m5408 ();
extern "C" void ClientSessionInfo_GetContext_m5409 ();
extern "C" void ClientSessionInfo_SetContext_m5410 ();
extern "C" void ClientSessionInfo_KeepAlive_m5411 ();
extern "C" void ClientSessionInfo_Dispose_m5412 ();
extern "C" void ClientSessionInfo_Dispose_m5413 ();
extern "C" void ClientSessionInfo_CheckDisposed_m5414 ();
extern "C" void ClientSessionCache__cctor_m5415 ();
extern "C" void ClientSessionCache_Add_m5416 ();
extern "C" void ClientSessionCache_FromHost_m5417 ();
extern "C" void ClientSessionCache_FromContext_m5418 ();
extern "C" void ClientSessionCache_SetContextInCache_m5419 ();
extern "C" void ClientSessionCache_SetContextFromCache_m5420 ();
extern "C" void Context__ctor_m5421 ();
extern "C" void Context_get_AbbreviatedHandshake_m5422 ();
extern "C" void Context_set_AbbreviatedHandshake_m5423 ();
extern "C" void Context_get_ProtocolNegotiated_m5424 ();
extern "C" void Context_set_ProtocolNegotiated_m5425 ();
extern "C" void Context_get_SecurityProtocol_m5426 ();
extern "C" void Context_set_SecurityProtocol_m5427 ();
extern "C" void Context_get_SecurityProtocolFlags_m5428 ();
extern "C" void Context_get_Protocol_m5429 ();
extern "C" void Context_get_SessionId_m5430 ();
extern "C" void Context_set_SessionId_m5431 ();
extern "C" void Context_get_CompressionMethod_m5432 ();
extern "C" void Context_set_CompressionMethod_m5433 ();
extern "C" void Context_get_ServerSettings_m5434 ();
extern "C" void Context_get_ClientSettings_m5435 ();
extern "C" void Context_get_LastHandshakeMsg_m5436 ();
extern "C" void Context_set_LastHandshakeMsg_m5437 ();
extern "C" void Context_get_HandshakeState_m5438 ();
extern "C" void Context_set_HandshakeState_m5439 ();
extern "C" void Context_get_ReceivedConnectionEnd_m5440 ();
extern "C" void Context_set_ReceivedConnectionEnd_m5441 ();
extern "C" void Context_get_SentConnectionEnd_m5442 ();
extern "C" void Context_set_SentConnectionEnd_m5443 ();
extern "C" void Context_get_SupportedCiphers_m5444 ();
extern "C" void Context_set_SupportedCiphers_m5445 ();
extern "C" void Context_get_HandshakeMessages_m5446 ();
extern "C" void Context_get_WriteSequenceNumber_m5447 ();
extern "C" void Context_set_WriteSequenceNumber_m5448 ();
extern "C" void Context_get_ReadSequenceNumber_m5449 ();
extern "C" void Context_set_ReadSequenceNumber_m5450 ();
extern "C" void Context_get_ClientRandom_m5451 ();
extern "C" void Context_set_ClientRandom_m5452 ();
extern "C" void Context_get_ServerRandom_m5453 ();
extern "C" void Context_set_ServerRandom_m5454 ();
extern "C" void Context_get_RandomCS_m5455 ();
extern "C" void Context_set_RandomCS_m5456 ();
extern "C" void Context_get_RandomSC_m5457 ();
extern "C" void Context_set_RandomSC_m5458 ();
extern "C" void Context_get_MasterSecret_m5459 ();
extern "C" void Context_set_MasterSecret_m5460 ();
extern "C" void Context_get_ClientWriteKey_m5461 ();
extern "C" void Context_set_ClientWriteKey_m5462 ();
extern "C" void Context_get_ServerWriteKey_m5463 ();
extern "C" void Context_set_ServerWriteKey_m5464 ();
extern "C" void Context_get_ClientWriteIV_m5465 ();
extern "C" void Context_set_ClientWriteIV_m5466 ();
extern "C" void Context_get_ServerWriteIV_m5467 ();
extern "C" void Context_set_ServerWriteIV_m5468 ();
extern "C" void Context_get_RecordProtocol_m5469 ();
extern "C" void Context_set_RecordProtocol_m5470 ();
extern "C" void Context_GetUnixTime_m5471 ();
extern "C" void Context_GetSecureRandomBytes_m5472 ();
extern "C" void Context_Clear_m5473 ();
extern "C" void Context_ClearKeyInfo_m5474 ();
extern "C" void Context_DecodeProtocolCode_m5475 ();
extern "C" void Context_ChangeProtocol_m5476 ();
extern "C" void Context_get_Current_m5477 ();
extern "C" void Context_get_Negotiating_m5478 ();
extern "C" void Context_get_Read_m5479 ();
extern "C" void Context_get_Write_m5480 ();
extern "C" void Context_StartSwitchingSecurityParameters_m5481 ();
extern "C" void Context_EndSwitchingSecurityParameters_m5482 ();
extern "C" void HttpsClientStream__ctor_m5483 ();
extern "C" void HttpsClientStream_get_TrustFailure_m5484 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m5485 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m5486 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m5487 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m5488 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m5489 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m5490 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m5491 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m5492 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m5493 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m5494 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m5495 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m5496 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5497 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5498 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5499 ();
extern "C" void SendRecordAsyncResult__ctor_m5500 ();
extern "C" void SendRecordAsyncResult_get_Message_m5501 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m5502 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m5503 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m5504 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m5505 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m5506 ();
extern "C" void SendRecordAsyncResult_SetComplete_m5507 ();
extern "C" void SendRecordAsyncResult_SetComplete_m5508 ();
extern "C" void RecordProtocol__ctor_m5509 ();
extern "C" void RecordProtocol__cctor_m5510 ();
extern "C" void RecordProtocol_get_Context_m5511 ();
extern "C" void RecordProtocol_SendRecord_m5512 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m5513 ();
extern "C" void RecordProtocol_GetMessage_m5514 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m5515 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m5516 ();
extern "C" void RecordProtocol_EndReceiveRecord_m5517 ();
extern "C" void RecordProtocol_ReceiveRecord_m5518 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m5519 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m5520 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m5521 ();
extern "C" void RecordProtocol_ProcessAlert_m5522 ();
extern "C" void RecordProtocol_SendAlert_m5523 ();
extern "C" void RecordProtocol_SendAlert_m5524 ();
extern "C" void RecordProtocol_SendAlert_m5525 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m5526 ();
extern "C" void RecordProtocol_BeginSendRecord_m5527 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m5528 ();
extern "C" void RecordProtocol_BeginSendRecord_m5529 ();
extern "C" void RecordProtocol_EndSendRecord_m5530 ();
extern "C" void RecordProtocol_SendRecord_m5531 ();
extern "C" void RecordProtocol_EncodeRecord_m5532 ();
extern "C" void RecordProtocol_EncodeRecord_m5533 ();
extern "C" void RecordProtocol_encryptRecordFragment_m5534 ();
extern "C" void RecordProtocol_decryptRecordFragment_m5535 ();
extern "C" void RecordProtocol_Compare_m5536 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m5537 ();
extern "C" void RecordProtocol_MapV2CipherCode_m5538 ();
extern "C" void RSASslSignatureDeformatter__ctor_m5539 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m5540 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m5541 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m5542 ();
extern "C" void RSASslSignatureFormatter__ctor_m5543 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m5544 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m5545 ();
extern "C" void RSASslSignatureFormatter_SetKey_m5546 ();
extern "C" void SecurityParameters__ctor_m5547 ();
extern "C" void SecurityParameters_get_Cipher_m5548 ();
extern "C" void SecurityParameters_set_Cipher_m5549 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m5550 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m5551 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m5552 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m5553 ();
extern "C" void SecurityParameters_Clear_m5554 ();
extern "C" void ValidationResult_get_Trusted_m5555 ();
extern "C" void ValidationResult_get_ErrorCode_m5556 ();
extern "C" void SslClientStream__ctor_m5557 ();
extern "C" void SslClientStream__ctor_m5558 ();
extern "C" void SslClientStream__ctor_m5559 ();
extern "C" void SslClientStream__ctor_m5560 ();
extern "C" void SslClientStream__ctor_m5561 ();
extern "C" void SslClientStream_add_ServerCertValidation_m5562 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m5563 ();
extern "C" void SslClientStream_add_ClientCertSelection_m5564 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m5565 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m5566 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m5567 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m5568 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m5569 ();
extern "C" void SslClientStream_get_InputBuffer_m5570 ();
extern "C" void SslClientStream_get_ClientCertificates_m5571 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m5572 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m5573 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m5574 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m5575 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m5576 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m5577 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m5578 ();
extern "C" void SslClientStream_Finalize_m5579 ();
extern "C" void SslClientStream_Dispose_m5580 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m5581 ();
extern "C" void SslClientStream_SafeReceiveRecord_m5582 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m5583 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m5584 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m5585 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m5586 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m5587 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m5588 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m5589 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m5590 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m5591 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m5592 ();
extern "C" void SslCipherSuite__ctor_m5593 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m5594 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m5595 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m5596 ();
extern "C" void SslCipherSuite_ComputeKeys_m5597 ();
extern "C" void SslCipherSuite_prf_m5598 ();
extern "C" void SslHandshakeHash__ctor_m5599 ();
extern "C" void SslHandshakeHash_Initialize_m5600 ();
extern "C" void SslHandshakeHash_HashFinal_m5601 ();
extern "C" void SslHandshakeHash_HashCore_m5602 ();
extern "C" void SslHandshakeHash_CreateSignature_m5603 ();
extern "C" void SslHandshakeHash_initializePad_m5604 ();
extern "C" void InternalAsyncResult__ctor_m5605 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m5606 ();
extern "C" void InternalAsyncResult_get_FromWrite_m5607 ();
extern "C" void InternalAsyncResult_get_Buffer_m5608 ();
extern "C" void InternalAsyncResult_get_Offset_m5609 ();
extern "C" void InternalAsyncResult_get_Count_m5610 ();
extern "C" void InternalAsyncResult_get_BytesRead_m5611 ();
extern "C" void InternalAsyncResult_get_AsyncState_m5612 ();
extern "C" void InternalAsyncResult_get_AsyncException_m5613 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m5614 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m5615 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m5616 ();
extern "C" void InternalAsyncResult_SetComplete_m5617 ();
extern "C" void InternalAsyncResult_SetComplete_m5618 ();
extern "C" void InternalAsyncResult_SetComplete_m5619 ();
extern "C" void InternalAsyncResult_SetComplete_m5620 ();
extern "C" void SslStreamBase__ctor_m5621 ();
extern "C" void SslStreamBase__cctor_m5622 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m5623 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m5624 ();
extern "C" void SslStreamBase_NegotiateHandshake_m5625 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m5626 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m5627 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m5628 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m5629 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m5630 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m5631 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m5632 ();
extern "C" void SslStreamBase_get_CipherStrength_m5633 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m5634 ();
extern "C" void SslStreamBase_get_HashStrength_m5635 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m5636 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m5637 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m5638 ();
extern "C" void SslStreamBase_get_ServerCertificate_m5639 ();
extern "C" void SslStreamBase_get_ServerCertificates_m5640 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m5641 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m5642 ();
extern "C" void SslStreamBase_BeginRead_m5643 ();
extern "C" void SslStreamBase_InternalBeginRead_m5644 ();
extern "C" void SslStreamBase_InternalReadCallback_m5645 ();
extern "C" void SslStreamBase_InternalBeginWrite_m5646 ();
extern "C" void SslStreamBase_InternalWriteCallback_m5647 ();
extern "C" void SslStreamBase_BeginWrite_m5648 ();
extern "C" void SslStreamBase_EndRead_m5649 ();
extern "C" void SslStreamBase_EndWrite_m5650 ();
extern "C" void SslStreamBase_Close_m5651 ();
extern "C" void SslStreamBase_Flush_m5652 ();
extern "C" void SslStreamBase_Read_m5653 ();
extern "C" void SslStreamBase_Read_m5654 ();
extern "C" void SslStreamBase_Seek_m5655 ();
extern "C" void SslStreamBase_SetLength_m5656 ();
extern "C" void SslStreamBase_Write_m5657 ();
extern "C" void SslStreamBase_Write_m5658 ();
extern "C" void SslStreamBase_get_CanRead_m5659 ();
extern "C" void SslStreamBase_get_CanSeek_m5660 ();
extern "C" void SslStreamBase_get_CanWrite_m5661 ();
extern "C" void SslStreamBase_get_Length_m5662 ();
extern "C" void SslStreamBase_get_Position_m5663 ();
extern "C" void SslStreamBase_set_Position_m5664 ();
extern "C" void SslStreamBase_Finalize_m5665 ();
extern "C" void SslStreamBase_Dispose_m5666 ();
extern "C" void SslStreamBase_resetBuffer_m5667 ();
extern "C" void SslStreamBase_checkDisposed_m5668 ();
extern "C" void TlsCipherSuite__ctor_m5669 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m5670 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m5671 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m5672 ();
extern "C" void TlsCipherSuite_ComputeKeys_m5673 ();
extern "C" void TlsClientSettings__ctor_m5674 ();
extern "C" void TlsClientSettings_get_TargetHost_m5675 ();
extern "C" void TlsClientSettings_set_TargetHost_m5676 ();
extern "C" void TlsClientSettings_get_Certificates_m5677 ();
extern "C" void TlsClientSettings_set_Certificates_m5678 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m5679 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m5680 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m5681 ();
extern "C" void TlsException__ctor_m5682 ();
extern "C" void TlsException__ctor_m5683 ();
extern "C" void TlsException__ctor_m5684 ();
extern "C" void TlsException__ctor_m5685 ();
extern "C" void TlsException__ctor_m5686 ();
extern "C" void TlsException__ctor_m5687 ();
extern "C" void TlsException_get_Alert_m5688 ();
extern "C" void TlsServerSettings__ctor_m5689 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m5690 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m5691 ();
extern "C" void TlsServerSettings_get_Certificates_m5692 ();
extern "C" void TlsServerSettings_set_Certificates_m5693 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m5694 ();
extern "C" void TlsServerSettings_get_RsaParameters_m5695 ();
extern "C" void TlsServerSettings_set_RsaParameters_m5696 ();
extern "C" void TlsServerSettings_set_SignedParams_m5697 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m5698 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m5699 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m5700 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m5701 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m5702 ();
extern "C" void TlsStream__ctor_m5703 ();
extern "C" void TlsStream__ctor_m5704 ();
extern "C" void TlsStream_get_EOF_m5705 ();
extern "C" void TlsStream_get_CanWrite_m5706 ();
extern "C" void TlsStream_get_CanRead_m5707 ();
extern "C" void TlsStream_get_CanSeek_m5708 ();
extern "C" void TlsStream_get_Position_m5709 ();
extern "C" void TlsStream_set_Position_m5710 ();
extern "C" void TlsStream_get_Length_m5711 ();
extern "C" void TlsStream_ReadSmallValue_m5712 ();
extern "C" void TlsStream_ReadByte_m5713 ();
extern "C" void TlsStream_ReadInt16_m5714 ();
extern "C" void TlsStream_ReadInt24_m5715 ();
extern "C" void TlsStream_ReadBytes_m5716 ();
extern "C" void TlsStream_Write_m5717 ();
extern "C" void TlsStream_Write_m5718 ();
extern "C" void TlsStream_WriteInt24_m5719 ();
extern "C" void TlsStream_Write_m5720 ();
extern "C" void TlsStream_Write_m5721 ();
extern "C" void TlsStream_Reset_m5722 ();
extern "C" void TlsStream_ToArray_m5723 ();
extern "C" void TlsStream_Flush_m5724 ();
extern "C" void TlsStream_SetLength_m5725 ();
extern "C" void TlsStream_Seek_m5726 ();
extern "C" void TlsStream_Read_m5727 ();
extern "C" void TlsStream_Write_m5728 ();
extern "C" void HandshakeMessage__ctor_m5729 ();
extern "C" void HandshakeMessage__ctor_m5730 ();
extern "C" void HandshakeMessage__ctor_m5731 ();
extern "C" void HandshakeMessage_get_Context_m5732 ();
extern "C" void HandshakeMessage_get_HandshakeType_m5733 ();
extern "C" void HandshakeMessage_get_ContentType_m5734 ();
extern "C" void HandshakeMessage_Process_m5735 ();
extern "C" void HandshakeMessage_Update_m5736 ();
extern "C" void HandshakeMessage_EncodeMessage_m5737 ();
extern "C" void HandshakeMessage_Compare_m5738 ();
extern "C" void TlsClientCertificate__ctor_m5739 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m5740 ();
extern "C" void TlsClientCertificate_Update_m5741 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m5742 ();
extern "C" void TlsClientCertificate_SendCertificates_m5743 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m5744 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m5745 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m5746 ();
extern "C" void TlsClientCertificateVerify__ctor_m5747 ();
extern "C" void TlsClientCertificateVerify_Update_m5748 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m5749 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m5750 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m5751 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m5752 ();
extern "C" void TlsClientFinished__ctor_m5753 ();
extern "C" void TlsClientFinished__cctor_m5754 ();
extern "C" void TlsClientFinished_Update_m5755 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m5756 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m5757 ();
extern "C" void TlsClientHello__ctor_m5758 ();
extern "C" void TlsClientHello_Update_m5759 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m5760 ();
extern "C" void TlsClientHello_ProcessAsTls1_m5761 ();
extern "C" void TlsClientKeyExchange__ctor_m5762 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m5763 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m5764 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m5765 ();
extern "C" void TlsServerCertificate__ctor_m5766 ();
extern "C" void TlsServerCertificate_Update_m5767 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m5768 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m5769 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m5770 ();
extern "C" void TlsServerCertificate_validateCertificates_m5771 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m5772 ();
extern "C" void TlsServerCertificate_checkDomainName_m5773 ();
extern "C" void TlsServerCertificate_Match_m5774 ();
extern "C" void TlsServerCertificateRequest__ctor_m5775 ();
extern "C" void TlsServerCertificateRequest_Update_m5776 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m5777 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m5778 ();
extern "C" void TlsServerFinished__ctor_m5779 ();
extern "C" void TlsServerFinished__cctor_m5780 ();
extern "C" void TlsServerFinished_Update_m5781 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m5782 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m5783 ();
extern "C" void TlsServerHello__ctor_m5784 ();
extern "C" void TlsServerHello_Update_m5785 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m5786 ();
extern "C" void TlsServerHello_ProcessAsTls1_m5787 ();
extern "C" void TlsServerHello_processProtocol_m5788 ();
extern "C" void TlsServerHelloDone__ctor_m5789 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m5790 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m5791 ();
extern "C" void TlsServerKeyExchange__ctor_m5792 ();
extern "C" void TlsServerKeyExchange_Update_m5793 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m5794 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m5795 ();
extern "C" void TlsServerKeyExchange_verifySignature_m5796 ();
extern "C" void PrimalityTest__ctor_m5797 ();
extern "C" void PrimalityTest_Invoke_m5798 ();
extern "C" void PrimalityTest_BeginInvoke_m5799 ();
extern "C" void PrimalityTest_EndInvoke_m5800 ();
extern "C" void CertificateValidationCallback__ctor_m5801 ();
extern "C" void CertificateValidationCallback_Invoke_m5802 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m5803 ();
extern "C" void CertificateValidationCallback_EndInvoke_m5804 ();
extern "C" void CertificateValidationCallback2__ctor_m5805 ();
extern "C" void CertificateValidationCallback2_Invoke_m5806 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m5807 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m5808 ();
extern "C" void CertificateSelectionCallback__ctor_m5809 ();
extern "C" void CertificateSelectionCallback_Invoke_m5810 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m5811 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m5812 ();
extern "C" void PrivateKeySelectionCallback__ctor_m5813 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m5814 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m5815 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m5816 ();
extern "C" void Object__ctor_m390 ();
extern "C" void Object_Equals_m5894 ();
extern "C" void Object_Equals_m4977 ();
extern "C" void Object_Finalize_m2138 ();
extern "C" void Object_GetHashCode_m5895 ();
extern "C" void Object_GetType_m2181 ();
extern "C" void Object_MemberwiseClone_m5896 ();
extern "C" void Object_ToString_m2242 ();
extern "C" void Object_ReferenceEquals_m2180 ();
extern "C" void Object_InternalGetHashCode_m5897 ();
extern "C" void ValueType__ctor_m5898 ();
extern "C" void ValueType_InternalEquals_m5899 ();
extern "C" void ValueType_DefaultEquals_m5900 ();
extern "C" void ValueType_Equals_m5901 ();
extern "C" void ValueType_InternalGetHashCode_m5902 ();
extern "C" void ValueType_GetHashCode_m5903 ();
extern "C" void ValueType_ToString_m5904 ();
extern "C" void Attribute__ctor_m2163 ();
extern "C" void Attribute_CheckParameters_m5905 ();
extern "C" void Attribute_GetCustomAttribute_m5906 ();
extern "C" void Attribute_GetCustomAttribute_m5907 ();
extern "C" void Attribute_GetHashCode_m2243 ();
extern "C" void Attribute_IsDefined_m5908 ();
extern "C" void Attribute_IsDefined_m5909 ();
extern "C" void Attribute_IsDefined_m5910 ();
extern "C" void Attribute_IsDefined_m5911 ();
extern "C" void Attribute_Equals_m5912 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m5913 ();
extern "C" void Int32_System_IConvertible_ToByte_m5914 ();
extern "C" void Int32_System_IConvertible_ToChar_m5915 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m5916 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m5917 ();
extern "C" void Int32_System_IConvertible_ToDouble_m5918 ();
extern "C" void Int32_System_IConvertible_ToInt16_m5919 ();
extern "C" void Int32_System_IConvertible_ToInt32_m5920 ();
extern "C" void Int32_System_IConvertible_ToInt64_m5921 ();
extern "C" void Int32_System_IConvertible_ToSByte_m5922 ();
extern "C" void Int32_System_IConvertible_ToSingle_m5923 ();
extern "C" void Int32_System_IConvertible_ToType_m5924 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m5925 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m5926 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m5927 ();
extern "C" void Int32_CompareTo_m5928 ();
extern "C" void Int32_Equals_m5929 ();
extern "C" void Int32_GetHashCode_m2152 ();
extern "C" void Int32_CompareTo_m3543 ();
extern "C" void Int32_Equals_m2154 ();
extern "C" void Int32_ProcessTrailingWhitespace_m5930 ();
extern "C" void Int32_Parse_m5931 ();
extern "C" void Int32_Parse_m5932 ();
extern "C" void Int32_CheckStyle_m5933 ();
extern "C" void Int32_JumpOverWhite_m5934 ();
extern "C" void Int32_FindSign_m5935 ();
extern "C" void Int32_FindCurrency_m5936 ();
extern "C" void Int32_FindExponent_m5937 ();
extern "C" void Int32_FindOther_m5938 ();
extern "C" void Int32_ValidDigit_m5939 ();
extern "C" void Int32_GetFormatException_m5940 ();
extern "C" void Int32_Parse_m5941 ();
extern "C" void Int32_Parse_m4955 ();
extern "C" void Int32_Parse_m5942 ();
extern "C" void Int32_TryParse_m5943 ();
extern "C" void Int32_TryParse_m4847 ();
extern "C" void Int32_ToString_m2219 ();
extern "C" void Int32_ToString_m5859 ();
extern "C" void Int32_ToString_m4950 ();
extern "C" void Int32_ToString_m5862 ();
extern "C" void Int32_GetTypeCode_m5944 ();
extern "C" void SerializableAttribute__ctor_m5945 ();
extern "C" void AttributeUsageAttribute__ctor_m5946 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m5947 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m5948 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m5949 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m5950 ();
extern "C" void ComVisibleAttribute__ctor_m5951 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m5952 ();
extern "C" void Int64_System_IConvertible_ToByte_m5953 ();
extern "C" void Int64_System_IConvertible_ToChar_m5954 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m5955 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m5956 ();
extern "C" void Int64_System_IConvertible_ToDouble_m5957 ();
extern "C" void Int64_System_IConvertible_ToInt16_m5958 ();
extern "C" void Int64_System_IConvertible_ToInt32_m5959 ();
extern "C" void Int64_System_IConvertible_ToInt64_m5960 ();
extern "C" void Int64_System_IConvertible_ToSByte_m5961 ();
extern "C" void Int64_System_IConvertible_ToSingle_m5962 ();
extern "C" void Int64_System_IConvertible_ToType_m5963 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m5964 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m5965 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m5966 ();
extern "C" void Int64_CompareTo_m5967 ();
extern "C" void Int64_Equals_m5968 ();
extern "C" void Int64_GetHashCode_m5969 ();
extern "C" void Int64_CompareTo_m5970 ();
extern "C" void Int64_Equals_m5971 ();
extern "C" void Int64_Parse_m5972 ();
extern "C" void Int64_Parse_m5973 ();
extern "C" void Int64_Parse_m5974 ();
extern "C" void Int64_Parse_m5975 ();
extern "C" void Int64_Parse_m5976 ();
extern "C" void Int64_TryParse_m5977 ();
extern "C" void Int64_TryParse_m4843 ();
extern "C" void Int64_ToString_m4844 ();
extern "C" void Int64_ToString_m5978 ();
extern "C" void Int64_ToString_m5979 ();
extern "C" void Int64_ToString_m5980 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m5981 ();
extern "C" void UInt32_System_IConvertible_ToByte_m5982 ();
extern "C" void UInt32_System_IConvertible_ToChar_m5983 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m5984 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m5985 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m5986 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m5987 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m5988 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m5989 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m5990 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m5991 ();
extern "C" void UInt32_System_IConvertible_ToType_m5992 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m5993 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m5994 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m5995 ();
extern "C" void UInt32_CompareTo_m5996 ();
extern "C" void UInt32_Equals_m5997 ();
extern "C" void UInt32_GetHashCode_m5998 ();
extern "C" void UInt32_CompareTo_m5999 ();
extern "C" void UInt32_Equals_m6000 ();
extern "C" void UInt32_Parse_m6001 ();
extern "C" void UInt32_Parse_m6002 ();
extern "C" void UInt32_Parse_m6003 ();
extern "C" void UInt32_Parse_m6004 ();
extern "C" void UInt32_TryParse_m4970 ();
extern "C" void UInt32_TryParse_m6005 ();
extern "C" void UInt32_ToString_m6006 ();
extern "C" void UInt32_ToString_m6007 ();
extern "C" void UInt32_ToString_m6008 ();
extern "C" void UInt32_ToString_m6009 ();
extern "C" void CLSCompliantAttribute__ctor_m6010 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m6011 ();
extern "C" void UInt64_System_IConvertible_ToByte_m6012 ();
extern "C" void UInt64_System_IConvertible_ToChar_m6013 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m6014 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m6015 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m6016 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m6017 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m6018 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m6019 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m6020 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m6021 ();
extern "C" void UInt64_System_IConvertible_ToType_m6022 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m6023 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m6024 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m6025 ();
extern "C" void UInt64_CompareTo_m6026 ();
extern "C" void UInt64_Equals_m6027 ();
extern "C" void UInt64_GetHashCode_m6028 ();
extern "C" void UInt64_CompareTo_m6029 ();
extern "C" void UInt64_Equals_m6030 ();
extern "C" void UInt64_Parse_m6031 ();
extern "C" void UInt64_Parse_m6032 ();
extern "C" void UInt64_Parse_m6033 ();
extern "C" void UInt64_TryParse_m6034 ();
extern "C" void UInt64_ToString_m6035 ();
extern "C" void UInt64_ToString_m5822 ();
extern "C" void UInt64_ToString_m6036 ();
extern "C" void UInt64_ToString_m6037 ();
extern "C" void Byte_System_IConvertible_ToType_m6038 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m6039 ();
extern "C" void Byte_System_IConvertible_ToByte_m6040 ();
extern "C" void Byte_System_IConvertible_ToChar_m6041 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m6042 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m6043 ();
extern "C" void Byte_System_IConvertible_ToDouble_m6044 ();
extern "C" void Byte_System_IConvertible_ToInt16_m6045 ();
extern "C" void Byte_System_IConvertible_ToInt32_m6046 ();
extern "C" void Byte_System_IConvertible_ToInt64_m6047 ();
extern "C" void Byte_System_IConvertible_ToSByte_m6048 ();
extern "C" void Byte_System_IConvertible_ToSingle_m6049 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m6050 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m6051 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m6052 ();
extern "C" void Byte_CompareTo_m6053 ();
extern "C" void Byte_Equals_m6054 ();
extern "C" void Byte_GetHashCode_m6055 ();
extern "C" void Byte_CompareTo_m6056 ();
extern "C" void Byte_Equals_m6057 ();
extern "C" void Byte_Parse_m6058 ();
extern "C" void Byte_Parse_m6059 ();
extern "C" void Byte_Parse_m6060 ();
extern "C" void Byte_TryParse_m6061 ();
extern "C" void Byte_TryParse_m6062 ();
extern "C" void Byte_ToString_m5860 ();
extern "C" void Byte_ToString_m4890 ();
extern "C" void Byte_ToString_m5821 ();
extern "C" void Byte_ToString_m5826 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m6063 ();
extern "C" void SByte_System_IConvertible_ToByte_m6064 ();
extern "C" void SByte_System_IConvertible_ToChar_m6065 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m6066 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m6067 ();
extern "C" void SByte_System_IConvertible_ToDouble_m6068 ();
extern "C" void SByte_System_IConvertible_ToInt16_m6069 ();
extern "C" void SByte_System_IConvertible_ToInt32_m6070 ();
extern "C" void SByte_System_IConvertible_ToInt64_m6071 ();
extern "C" void SByte_System_IConvertible_ToSByte_m6072 ();
extern "C" void SByte_System_IConvertible_ToSingle_m6073 ();
extern "C" void SByte_System_IConvertible_ToType_m6074 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m6075 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m6076 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m6077 ();
extern "C" void SByte_CompareTo_m6078 ();
extern "C" void SByte_Equals_m6079 ();
extern "C" void SByte_GetHashCode_m6080 ();
extern "C" void SByte_CompareTo_m6081 ();
extern "C" void SByte_Equals_m6082 ();
extern "C" void SByte_Parse_m6083 ();
extern "C" void SByte_Parse_m6084 ();
extern "C" void SByte_Parse_m6085 ();
extern "C" void SByte_TryParse_m6086 ();
extern "C" void SByte_ToString_m6087 ();
extern "C" void SByte_ToString_m6088 ();
extern "C" void SByte_ToString_m6089 ();
extern "C" void SByte_ToString_m6090 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m6091 ();
extern "C" void Int16_System_IConvertible_ToByte_m6092 ();
extern "C" void Int16_System_IConvertible_ToChar_m6093 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m6094 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m6095 ();
extern "C" void Int16_System_IConvertible_ToDouble_m6096 ();
extern "C" void Int16_System_IConvertible_ToInt16_m6097 ();
extern "C" void Int16_System_IConvertible_ToInt32_m6098 ();
extern "C" void Int16_System_IConvertible_ToInt64_m6099 ();
extern "C" void Int16_System_IConvertible_ToSByte_m6100 ();
extern "C" void Int16_System_IConvertible_ToSingle_m6101 ();
extern "C" void Int16_System_IConvertible_ToType_m6102 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m6103 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m6104 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m6105 ();
extern "C" void Int16_CompareTo_m6106 ();
extern "C" void Int16_Equals_m6107 ();
extern "C" void Int16_GetHashCode_m6108 ();
extern "C" void Int16_CompareTo_m6109 ();
extern "C" void Int16_Equals_m6110 ();
extern "C" void Int16_Parse_m6111 ();
extern "C" void Int16_Parse_m6112 ();
extern "C" void Int16_Parse_m6113 ();
extern "C" void Int16_TryParse_m6114 ();
extern "C" void Int16_ToString_m6115 ();
extern "C" void Int16_ToString_m6116 ();
extern "C" void Int16_ToString_m6117 ();
extern "C" void Int16_ToString_m6118 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m6119 ();
extern "C" void UInt16_System_IConvertible_ToByte_m6120 ();
extern "C" void UInt16_System_IConvertible_ToChar_m6121 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m6122 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m6123 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m6124 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m6125 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m6126 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m6127 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m6128 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m6129 ();
extern "C" void UInt16_System_IConvertible_ToType_m6130 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m6131 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m6132 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m6133 ();
extern "C" void UInt16_CompareTo_m6134 ();
extern "C" void UInt16_Equals_m6135 ();
extern "C" void UInt16_GetHashCode_m6136 ();
extern "C" void UInt16_CompareTo_m6137 ();
extern "C" void UInt16_Equals_m6138 ();
extern "C" void UInt16_Parse_m6139 ();
extern "C" void UInt16_Parse_m6140 ();
extern "C" void UInt16_TryParse_m6141 ();
extern "C" void UInt16_TryParse_m6142 ();
extern "C" void UInt16_ToString_m6143 ();
extern "C" void UInt16_ToString_m6144 ();
extern "C" void UInt16_ToString_m6145 ();
extern "C" void UInt16_ToString_m6146 ();
extern "C" void Char__cctor_m6147 ();
extern "C" void Char_System_IConvertible_ToType_m6148 ();
extern "C" void Char_System_IConvertible_ToBoolean_m6149 ();
extern "C" void Char_System_IConvertible_ToByte_m6150 ();
extern "C" void Char_System_IConvertible_ToChar_m6151 ();
extern "C" void Char_System_IConvertible_ToDateTime_m6152 ();
extern "C" void Char_System_IConvertible_ToDecimal_m6153 ();
extern "C" void Char_System_IConvertible_ToDouble_m6154 ();
extern "C" void Char_System_IConvertible_ToInt16_m6155 ();
extern "C" void Char_System_IConvertible_ToInt32_m6156 ();
extern "C" void Char_System_IConvertible_ToInt64_m6157 ();
extern "C" void Char_System_IConvertible_ToSByte_m6158 ();
extern "C" void Char_System_IConvertible_ToSingle_m6159 ();
extern "C" void Char_System_IConvertible_ToUInt16_m6160 ();
extern "C" void Char_System_IConvertible_ToUInt32_m6161 ();
extern "C" void Char_System_IConvertible_ToUInt64_m6162 ();
extern "C" void Char_GetDataTablePointers_m6163 ();
extern "C" void Char_CompareTo_m6164 ();
extern "C" void Char_Equals_m6165 ();
extern "C" void Char_CompareTo_m6166 ();
extern "C" void Char_Equals_m6167 ();
extern "C" void Char_GetHashCode_m6168 ();
extern "C" void Char_GetUnicodeCategory_m4961 ();
extern "C" void Char_IsDigit_m4959 ();
extern "C" void Char_IsLetter_m3695 ();
extern "C" void Char_IsLetterOrDigit_m4958 ();
extern "C" void Char_IsLower_m3696 ();
extern "C" void Char_IsSurrogate_m6169 ();
extern "C" void Char_IsUpper_m3698 ();
extern "C" void Char_IsWhiteSpace_m4960 ();
extern "C" void Char_IsWhiteSpace_m4877 ();
extern "C" void Char_CheckParameter_m6170 ();
extern "C" void Char_Parse_m6171 ();
extern "C" void Char_ToLower_m3699 ();
extern "C" void Char_ToLowerInvariant_m6172 ();
extern "C" void Char_ToLower_m6173 ();
extern "C" void Char_ToUpper_m3697 ();
extern "C" void Char_ToUpperInvariant_m4879 ();
extern "C" void Char_ToString_m3688 ();
extern "C" void Char_ToString_m6174 ();
extern "C" void Char_GetTypeCode_m6175 ();
extern "C" void String__ctor_m6176 ();
extern "C" void String__ctor_m6177 ();
extern "C" void String__ctor_m6178 ();
extern "C" void String__ctor_m6179 ();
extern "C" void String__cctor_m6180 ();
extern "C" void String_System_IConvertible_ToBoolean_m6181 ();
extern "C" void String_System_IConvertible_ToByte_m6182 ();
extern "C" void String_System_IConvertible_ToChar_m6183 ();
extern "C" void String_System_IConvertible_ToDateTime_m6184 ();
extern "C" void String_System_IConvertible_ToDecimal_m6185 ();
extern "C" void String_System_IConvertible_ToDouble_m6186 ();
extern "C" void String_System_IConvertible_ToInt16_m6187 ();
extern "C" void String_System_IConvertible_ToInt32_m6188 ();
extern "C" void String_System_IConvertible_ToInt64_m6189 ();
extern "C" void String_System_IConvertible_ToSByte_m6190 ();
extern "C" void String_System_IConvertible_ToSingle_m6191 ();
extern "C" void String_System_IConvertible_ToType_m6192 ();
extern "C" void String_System_IConvertible_ToUInt16_m6193 ();
extern "C" void String_System_IConvertible_ToUInt32_m6194 ();
extern "C" void String_System_IConvertible_ToUInt64_m6195 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m6196 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m6197 ();
extern "C" void String_Equals_m6198 ();
extern "C" void String_Equals_m6199 ();
extern "C" void String_Equals_m4938 ();
extern "C" void String_get_Chars_m2175 ();
extern "C" void String_Clone_m6200 ();
extern "C" void String_CopyTo_m6201 ();
extern "C" void String_ToCharArray_m4842 ();
extern "C" void String_ToCharArray_m6202 ();
extern "C" void String_Split_m2210 ();
extern "C" void String_Split_m6203 ();
extern "C" void String_Split_m6204 ();
extern "C" void String_Split_m6205 ();
extern "C" void String_Split_m4880 ();
extern "C" void String_Substring_m3690 ();
extern "C" void String_Substring_m2176 ();
extern "C" void String_SubstringUnchecked_m6206 ();
extern "C" void String_Trim_m2206 ();
extern "C" void String_Trim_m6207 ();
extern "C" void String_TrimStart_m4972 ();
extern "C" void String_TrimEnd_m4878 ();
extern "C" void String_FindNotWhiteSpace_m6208 ();
extern "C" void String_FindNotInTable_m6209 ();
extern "C" void String_Compare_m6210 ();
extern "C" void String_Compare_m6211 ();
extern "C" void String_Compare_m4858 ();
extern "C" void String_Compare_m5893 ();
extern "C" void String_CompareTo_m6212 ();
extern "C" void String_CompareTo_m6213 ();
extern "C" void String_CompareOrdinal_m6214 ();
extern "C" void String_CompareOrdinalUnchecked_m6215 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m6216 ();
extern "C" void String_EndsWith_m2212 ();
extern "C" void String_IndexOfAny_m6217 ();
extern "C" void String_IndexOfAny_m3686 ();
extern "C" void String_IndexOfAny_m5844 ();
extern "C" void String_IndexOfAnyUnchecked_m6218 ();
extern "C" void String_IndexOf_m4913 ();
extern "C" void String_IndexOf_m6219 ();
extern "C" void String_IndexOfOrdinal_m6220 ();
extern "C" void String_IndexOfOrdinalUnchecked_m6221 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m6222 ();
extern "C" void String_IndexOf_m3700 ();
extern "C" void String_IndexOf_m4973 ();
extern "C" void String_IndexOf_m4974 ();
extern "C" void String_IndexOfUnchecked_m6223 ();
extern "C" void String_IndexOf_m2211 ();
extern "C" void String_IndexOf_m2214 ();
extern "C" void String_IndexOf_m6224 ();
extern "C" void String_LastIndexOfAny_m6225 ();
extern "C" void String_LastIndexOfAny_m3687 ();
extern "C" void String_LastIndexOfAnyUnchecked_m6226 ();
extern "C" void String_LastIndexOf_m4848 ();
extern "C" void String_LastIndexOf_m6227 ();
extern "C" void String_LastIndexOf_m4975 ();
extern "C" void String_LastIndexOfUnchecked_m6228 ();
extern "C" void String_LastIndexOf_m2217 ();
extern "C" void String_LastIndexOf_m6229 ();
extern "C" void String_Contains_m3694 ();
extern "C" void String_IsNullOrEmpty_m2173 ();
extern "C" void String_PadRight_m6230 ();
extern "C" void String_StartsWith_m2204 ();
extern "C" void String_Replace_m2216 ();
extern "C" void String_Replace_m2215 ();
extern "C" void String_ReplaceUnchecked_m6231 ();
extern "C" void String_ReplaceFallback_m6232 ();
extern "C" void String_Remove_m2213 ();
extern "C" void String_ToLower_m2177 ();
extern "C" void String_ToLower_m4971 ();
extern "C" void String_ToLowerInvariant_m6233 ();
extern "C" void String_ToString_m2203 ();
extern "C" void String_ToString_m6234 ();
extern "C" void String_Format_m3619 ();
extern "C" void String_Format_m6235 ();
extern "C" void String_Format_m6236 ();
extern "C" void String_Format_m2164 ();
extern "C" void String_Format_m5871 ();
extern "C" void String_FormatHelper_m6237 ();
extern "C" void String_Concat_m2183 ();
extern "C" void String_Concat_m3540 ();
extern "C" void String_Concat_m559 ();
extern "C" void String_Concat_m405 ();
extern "C" void String_Concat_m2207 ();
extern "C" void String_Concat_m2182 ();
extern "C" void String_Concat_m4845 ();
extern "C" void String_ConcatInternal_m6238 ();
extern "C" void String_Insert_m2218 ();
extern "C" void String_Join_m404 ();
extern "C" void String_Join_m6239 ();
extern "C" void String_JoinUnchecked_m6240 ();
extern "C" void String_get_Length_m2136 ();
extern "C" void String_ParseFormatSpecifier_m6241 ();
extern "C" void String_ParseDecimal_m6242 ();
extern "C" void String_InternalSetChar_m6243 ();
extern "C" void String_InternalSetLength_m6244 ();
extern "C" void String_GetHashCode_m2168 ();
extern "C" void String_GetCaseInsensitiveHashCode_m6245 ();
extern "C" void String_CreateString_m6246 ();
extern "C" void String_CreateString_m6247 ();
extern "C" void String_CreateString_m6248 ();
extern "C" void String_CreateString_m6249 ();
extern "C" void String_CreateString_m6250 ();
extern "C" void String_CreateString_m6251 ();
extern "C" void String_CreateString_m4964 ();
extern "C" void String_CreateString_m3691 ();
extern "C" void String_memcpy4_m6252 ();
extern "C" void String_memcpy2_m6253 ();
extern "C" void String_memcpy1_m6254 ();
extern "C" void String_memcpy_m6255 ();
extern "C" void String_CharCopy_m6256 ();
extern "C" void String_CharCopyReverse_m6257 ();
extern "C" void String_CharCopy_m6258 ();
extern "C" void String_CharCopy_m6259 ();
extern "C" void String_CharCopyReverse_m6260 ();
extern "C" void String_InternalSplit_m6261 ();
extern "C" void String_InternalAllocateStr_m6262 ();
extern "C" void String_op_Equality_m687 ();
extern "C" void String_op_Inequality_m3685 ();
extern "C" void Single_System_IConvertible_ToBoolean_m6263 ();
extern "C" void Single_System_IConvertible_ToByte_m6264 ();
extern "C" void Single_System_IConvertible_ToChar_m6265 ();
extern "C" void Single_System_IConvertible_ToDateTime_m6266 ();
extern "C" void Single_System_IConvertible_ToDecimal_m6267 ();
extern "C" void Single_System_IConvertible_ToDouble_m6268 ();
extern "C" void Single_System_IConvertible_ToInt16_m6269 ();
extern "C" void Single_System_IConvertible_ToInt32_m6270 ();
extern "C" void Single_System_IConvertible_ToInt64_m6271 ();
extern "C" void Single_System_IConvertible_ToSByte_m6272 ();
extern "C" void Single_System_IConvertible_ToSingle_m6273 ();
extern "C" void Single_System_IConvertible_ToType_m6274 ();
extern "C" void Single_System_IConvertible_ToUInt16_m6275 ();
extern "C" void Single_System_IConvertible_ToUInt32_m6276 ();
extern "C" void Single_System_IConvertible_ToUInt64_m6277 ();
extern "C" void Single_CompareTo_m6278 ();
extern "C" void Single_Equals_m6279 ();
extern "C" void Single_CompareTo_m3544 ();
extern "C" void Single_Equals_m2161 ();
extern "C" void Single_GetHashCode_m2153 ();
extern "C" void Single_IsInfinity_m6280 ();
extern "C" void Single_IsNaN_m6281 ();
extern "C" void Single_IsNegativeInfinity_m6282 ();
extern "C" void Single_IsPositiveInfinity_m6283 ();
extern "C" void Single_Parse_m6284 ();
extern "C" void Single_ToString_m6285 ();
extern "C" void Single_ToString_m6286 ();
extern "C" void Single_ToString_m690 ();
extern "C" void Single_ToString_m6287 ();
extern "C" void Single_GetTypeCode_m6288 ();
extern "C" void Double_System_IConvertible_ToType_m6289 ();
extern "C" void Double_System_IConvertible_ToBoolean_m6290 ();
extern "C" void Double_System_IConvertible_ToByte_m6291 ();
extern "C" void Double_System_IConvertible_ToChar_m6292 ();
extern "C" void Double_System_IConvertible_ToDateTime_m6293 ();
extern "C" void Double_System_IConvertible_ToDecimal_m6294 ();
extern "C" void Double_System_IConvertible_ToDouble_m6295 ();
extern "C" void Double_System_IConvertible_ToInt16_m6296 ();
extern "C" void Double_System_IConvertible_ToInt32_m6297 ();
extern "C" void Double_System_IConvertible_ToInt64_m6298 ();
extern "C" void Double_System_IConvertible_ToSByte_m6299 ();
extern "C" void Double_System_IConvertible_ToSingle_m6300 ();
extern "C" void Double_System_IConvertible_ToUInt16_m6301 ();
extern "C" void Double_System_IConvertible_ToUInt32_m6302 ();
extern "C" void Double_System_IConvertible_ToUInt64_m6303 ();
extern "C" void Double_CompareTo_m6304 ();
extern "C" void Double_Equals_m6305 ();
extern "C" void Double_CompareTo_m6306 ();
extern "C" void Double_Equals_m6307 ();
extern "C" void Double_GetHashCode_m6308 ();
extern "C" void Double_IsInfinity_m6309 ();
extern "C" void Double_IsNaN_m6310 ();
extern "C" void Double_IsNegativeInfinity_m6311 ();
extern "C" void Double_IsPositiveInfinity_m6312 ();
extern "C" void Double_Parse_m6313 ();
extern "C" void Double_Parse_m6314 ();
extern "C" void Double_Parse_m6315 ();
extern "C" void Double_Parse_m6316 ();
extern "C" void Double_TryParseStringConstant_m6317 ();
extern "C" void Double_ParseImpl_m6318 ();
extern "C" void Double_ToString_m6319 ();
extern "C" void Double_ToString_m6320 ();
extern "C" void Double_ToString_m6321 ();
extern "C" void Decimal__ctor_m6322 ();
extern "C" void Decimal__ctor_m6323 ();
extern "C" void Decimal__ctor_m6324 ();
extern "C" void Decimal__ctor_m6325 ();
extern "C" void Decimal__ctor_m6326 ();
extern "C" void Decimal__ctor_m6327 ();
extern "C" void Decimal__ctor_m6328 ();
extern "C" void Decimal__cctor_m6329 ();
extern "C" void Decimal_System_IConvertible_ToType_m6330 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m6331 ();
extern "C" void Decimal_System_IConvertible_ToByte_m6332 ();
extern "C" void Decimal_System_IConvertible_ToChar_m6333 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m6334 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m6335 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m6336 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m6337 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m6338 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m6339 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m6340 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m6341 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m6342 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m6343 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m6344 ();
extern "C" void Decimal_GetBits_m6345 ();
extern "C" void Decimal_Add_m6346 ();
extern "C" void Decimal_Subtract_m6347 ();
extern "C" void Decimal_GetHashCode_m6348 ();
extern "C" void Decimal_u64_m6349 ();
extern "C" void Decimal_s64_m6350 ();
extern "C" void Decimal_Equals_m6351 ();
extern "C" void Decimal_Equals_m6352 ();
extern "C" void Decimal_IsZero_m6353 ();
extern "C" void Decimal_Floor_m6354 ();
extern "C" void Decimal_Multiply_m6355 ();
extern "C" void Decimal_Divide_m6356 ();
extern "C" void Decimal_Compare_m6357 ();
extern "C" void Decimal_CompareTo_m6358 ();
extern "C" void Decimal_CompareTo_m6359 ();
extern "C" void Decimal_Equals_m6360 ();
extern "C" void Decimal_Parse_m6361 ();
extern "C" void Decimal_ThrowAtPos_m6362 ();
extern "C" void Decimal_ThrowInvalidExp_m6363 ();
extern "C" void Decimal_stripStyles_m6364 ();
extern "C" void Decimal_Parse_m6365 ();
extern "C" void Decimal_PerformParse_m6366 ();
extern "C" void Decimal_ToString_m6367 ();
extern "C" void Decimal_ToString_m6368 ();
extern "C" void Decimal_ToString_m6369 ();
extern "C" void Decimal_decimal2UInt64_m6370 ();
extern "C" void Decimal_decimal2Int64_m6371 ();
extern "C" void Decimal_decimalIncr_m6372 ();
extern "C" void Decimal_string2decimal_m6373 ();
extern "C" void Decimal_decimalSetExponent_m6374 ();
extern "C" void Decimal_decimal2double_m6375 ();
extern "C" void Decimal_decimalFloorAndTrunc_m6376 ();
extern "C" void Decimal_decimalMult_m6377 ();
extern "C" void Decimal_decimalDiv_m6378 ();
extern "C" void Decimal_decimalCompare_m6379 ();
extern "C" void Decimal_op_Increment_m6380 ();
extern "C" void Decimal_op_Subtraction_m6381 ();
extern "C" void Decimal_op_Multiply_m6382 ();
extern "C" void Decimal_op_Division_m6383 ();
extern "C" void Decimal_op_Explicit_m6384 ();
extern "C" void Decimal_op_Explicit_m6385 ();
extern "C" void Decimal_op_Explicit_m6386 ();
extern "C" void Decimal_op_Explicit_m6387 ();
extern "C" void Decimal_op_Explicit_m6388 ();
extern "C" void Decimal_op_Explicit_m6389 ();
extern "C" void Decimal_op_Explicit_m6390 ();
extern "C" void Decimal_op_Explicit_m6391 ();
extern "C" void Decimal_op_Implicit_m6392 ();
extern "C" void Decimal_op_Implicit_m6393 ();
extern "C" void Decimal_op_Implicit_m6394 ();
extern "C" void Decimal_op_Implicit_m6395 ();
extern "C" void Decimal_op_Implicit_m6396 ();
extern "C" void Decimal_op_Implicit_m6397 ();
extern "C" void Decimal_op_Implicit_m6398 ();
extern "C" void Decimal_op_Implicit_m6399 ();
extern "C" void Decimal_op_Explicit_m6400 ();
extern "C" void Decimal_op_Explicit_m6401 ();
extern "C" void Decimal_op_Explicit_m6402 ();
extern "C" void Decimal_op_Explicit_m6403 ();
extern "C" void Decimal_op_Inequality_m6404 ();
extern "C" void Decimal_op_Equality_m3838 ();
extern "C" void Decimal_op_GreaterThan_m6405 ();
extern "C" void Decimal_op_LessThan_m6406 ();
extern "C" void Boolean__cctor_m6407 ();
extern "C" void Boolean_System_IConvertible_ToType_m6408 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m6409 ();
extern "C" void Boolean_System_IConvertible_ToByte_m6410 ();
extern "C" void Boolean_System_IConvertible_ToChar_m6411 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m6412 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m6413 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m6414 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m6415 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m6416 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m6417 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m6418 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m6419 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m6420 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m6421 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m6422 ();
extern "C" void Boolean_CompareTo_m6423 ();
extern "C" void Boolean_Equals_m6424 ();
extern "C" void Boolean_CompareTo_m6425 ();
extern "C" void Boolean_Equals_m6426 ();
extern "C" void Boolean_GetHashCode_m6427 ();
extern "C" void Boolean_Parse_m6428 ();
extern "C" void Boolean_ToString_m6429 ();
extern "C" void Boolean_GetTypeCode_m6430 ();
extern "C" void Boolean_ToString_m6431 ();
extern "C" void IntPtr__ctor_m2165 ();
extern "C" void IntPtr__ctor_m6432 ();
extern "C" void IntPtr__ctor_m6433 ();
extern "C" void IntPtr__ctor_m6434 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6435 ();
extern "C" void IntPtr_get_Size_m6436 ();
extern "C" void IntPtr_Equals_m6437 ();
extern "C" void IntPtr_GetHashCode_m6438 ();
extern "C" void IntPtr_ToInt64_m6439 ();
extern "C" void IntPtr_ToPointer_m2157 ();
extern "C" void IntPtr_ToString_m6440 ();
extern "C" void IntPtr_ToString_m6441 ();
extern "C" void IntPtr_op_Equality_m2224 ();
extern "C" void IntPtr_op_Inequality_m2156 ();
extern "C" void IntPtr_op_Explicit_m6442 ();
extern "C" void IntPtr_op_Explicit_m6443 ();
extern "C" void IntPtr_op_Explicit_m6444 ();
extern "C" void IntPtr_op_Explicit_m2223 ();
extern "C" void IntPtr_op_Explicit_m6445 ();
extern "C" void UIntPtr__ctor_m6446 ();
extern "C" void UIntPtr__ctor_m6447 ();
extern "C" void UIntPtr__ctor_m6448 ();
extern "C" void UIntPtr__cctor_m6449 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6450 ();
extern "C" void UIntPtr_Equals_m6451 ();
extern "C" void UIntPtr_GetHashCode_m6452 ();
extern "C" void UIntPtr_ToUInt32_m6453 ();
extern "C" void UIntPtr_ToUInt64_m6454 ();
extern "C" void UIntPtr_ToPointer_m6455 ();
extern "C" void UIntPtr_ToString_m6456 ();
extern "C" void UIntPtr_get_Size_m6457 ();
extern "C" void UIntPtr_op_Equality_m6458 ();
extern "C" void UIntPtr_op_Inequality_m6459 ();
extern "C" void UIntPtr_op_Explicit_m6460 ();
extern "C" void UIntPtr_op_Explicit_m6461 ();
extern "C" void UIntPtr_op_Explicit_m6462 ();
extern "C" void UIntPtr_op_Explicit_m6463 ();
extern "C" void UIntPtr_op_Explicit_m6464 ();
extern "C" void UIntPtr_op_Explicit_m6465 ();
extern "C" void MulticastDelegate_GetObjectData_m6466 ();
extern "C" void MulticastDelegate_Equals_m6467 ();
extern "C" void MulticastDelegate_GetHashCode_m6468 ();
extern "C" void MulticastDelegate_GetInvocationList_m6469 ();
extern "C" void MulticastDelegate_CombineImpl_m6470 ();
extern "C" void MulticastDelegate_BaseEquals_m6471 ();
extern "C" void MulticastDelegate_KPM_m6472 ();
extern "C" void MulticastDelegate_RemoveImpl_m6473 ();
extern "C" void Delegate_get_Method_m2246 ();
extern "C" void Delegate_get_Target_m2227 ();
extern "C" void Delegate_CreateDelegate_internal_m6474 ();
extern "C" void Delegate_SetMulticastInvoke_m6475 ();
extern "C" void Delegate_arg_type_match_m6476 ();
extern "C" void Delegate_return_type_match_m6477 ();
extern "C" void Delegate_CreateDelegate_m6478 ();
extern "C" void Delegate_CreateDelegate_m2245 ();
extern "C" void Delegate_CreateDelegate_m6479 ();
extern "C" void Delegate_CreateDelegate_m3834 ();
extern "C" void Delegate_CreateDelegate_m6480 ();
extern "C" void Delegate_GetCandidateMethod_m6481 ();
extern "C" void Delegate_CreateDelegate_m6482 ();
extern "C" void Delegate_CreateDelegate_m6483 ();
extern "C" void Delegate_CreateDelegate_m6484 ();
extern "C" void Delegate_CreateDelegate_m6485 ();
extern "C" void Delegate_Clone_m6486 ();
extern "C" void Delegate_Equals_m6487 ();
extern "C" void Delegate_GetHashCode_m6488 ();
extern "C" void Delegate_GetObjectData_m6489 ();
extern "C" void Delegate_GetInvocationList_m6490 ();
extern "C" void Delegate_Combine_m340 ();
extern "C" void Delegate_Combine_m6491 ();
extern "C" void Delegate_CombineImpl_m6492 ();
extern "C" void Delegate_Remove_m341 ();
extern "C" void Delegate_RemoveImpl_m6493 ();
extern "C" void Enum__ctor_m6494 ();
extern "C" void Enum__cctor_m6495 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m6496 ();
extern "C" void Enum_System_IConvertible_ToByte_m6497 ();
extern "C" void Enum_System_IConvertible_ToChar_m6498 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m6499 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m6500 ();
extern "C" void Enum_System_IConvertible_ToDouble_m6501 ();
extern "C" void Enum_System_IConvertible_ToInt16_m6502 ();
extern "C" void Enum_System_IConvertible_ToInt32_m6503 ();
extern "C" void Enum_System_IConvertible_ToInt64_m6504 ();
extern "C" void Enum_System_IConvertible_ToSByte_m6505 ();
extern "C" void Enum_System_IConvertible_ToSingle_m6506 ();
extern "C" void Enum_System_IConvertible_ToType_m6507 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m6508 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m6509 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m6510 ();
extern "C" void Enum_GetTypeCode_m6511 ();
extern "C" void Enum_get_value_m6512 ();
extern "C" void Enum_get_Value_m6513 ();
extern "C" void Enum_FindPosition_m6514 ();
extern "C" void Enum_GetName_m6515 ();
extern "C" void Enum_IsDefined_m5880 ();
extern "C" void Enum_get_underlying_type_m6516 ();
extern "C" void Enum_GetUnderlyingType_m6517 ();
extern "C" void Enum_FindName_m6518 ();
extern "C" void Enum_GetValue_m6519 ();
extern "C" void Enum_Parse_m2179 ();
extern "C" void Enum_compare_value_to_m6520 ();
extern "C" void Enum_CompareTo_m6521 ();
extern "C" void Enum_ToString_m6522 ();
extern "C" void Enum_ToString_m6523 ();
extern "C" void Enum_ToString_m6524 ();
extern "C" void Enum_ToString_m6525 ();
extern "C" void Enum_ToObject_m6526 ();
extern "C" void Enum_ToObject_m6527 ();
extern "C" void Enum_ToObject_m6528 ();
extern "C" void Enum_ToObject_m6529 ();
extern "C" void Enum_ToObject_m6530 ();
extern "C" void Enum_ToObject_m6531 ();
extern "C" void Enum_ToObject_m6532 ();
extern "C" void Enum_ToObject_m6533 ();
extern "C" void Enum_ToObject_m6534 ();
extern "C" void Enum_Equals_m6535 ();
extern "C" void Enum_get_hashcode_m6536 ();
extern "C" void Enum_GetHashCode_m6537 ();
extern "C" void Enum_FormatSpecifier_X_m6538 ();
extern "C" void Enum_FormatFlags_m6539 ();
extern "C" void Enum_Format_m6540 ();
extern "C" void SimpleEnumerator__ctor_m6541 ();
extern "C" void SimpleEnumerator_get_Current_m6542 ();
extern "C" void SimpleEnumerator_MoveNext_m6543 ();
extern "C" void SimpleEnumerator_Reset_m6544 ();
extern "C" void SimpleEnumerator_Clone_m6545 ();
extern "C" void Swapper__ctor_m6546 ();
extern "C" void Swapper_Invoke_m6547 ();
extern "C" void Swapper_BeginInvoke_m6548 ();
extern "C" void Swapper_EndInvoke_m6549 ();
extern "C" void Array__ctor_m6550 ();
extern "C" void Array_System_Collections_IList_get_Item_m6551 ();
extern "C" void Array_System_Collections_IList_set_Item_m6552 ();
extern "C" void Array_System_Collections_IList_Add_m6553 ();
extern "C" void Array_System_Collections_IList_Clear_m6554 ();
extern "C" void Array_System_Collections_IList_Contains_m6555 ();
extern "C" void Array_System_Collections_IList_IndexOf_m6556 ();
extern "C" void Array_System_Collections_IList_Insert_m6557 ();
extern "C" void Array_System_Collections_IList_Remove_m6558 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m6559 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m6560 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m6561 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m6562 ();
extern "C" void Array_InternalArray__ICollection_Clear_m6563 ();
extern "C" void Array_InternalArray__RemoveAt_m6564 ();
extern "C" void Array_get_Length_m3836 ();
extern "C" void Array_get_LongLength_m6565 ();
extern "C" void Array_get_Rank_m3835 ();
extern "C" void Array_GetRank_m6566 ();
extern "C" void Array_GetLength_m491 ();
extern "C" void Array_GetLongLength_m6567 ();
extern "C" void Array_GetLowerBound_m6568 ();
extern "C" void Array_GetValue_m6569 ();
extern "C" void Array_SetValue_m6570 ();
extern "C" void Array_GetValueImpl_m6571 ();
extern "C" void Array_SetValueImpl_m6572 ();
extern "C" void Array_FastCopy_m6573 ();
extern "C" void Array_CreateInstanceImpl_m6574 ();
extern "C" void Array_get_IsSynchronized_m6575 ();
extern "C" void Array_get_SyncRoot_m6576 ();
extern "C" void Array_get_IsFixedSize_m6577 ();
extern "C" void Array_get_IsReadOnly_m6578 ();
extern "C" void Array_GetEnumerator_m6579 ();
extern "C" void Array_GetUpperBound_m6580 ();
extern "C" void Array_GetValue_m3837 ();
extern "C" void Array_GetValue_m6581 ();
extern "C" void Array_GetValue_m6582 ();
extern "C" void Array_GetValue_m6583 ();
extern "C" void Array_GetValue_m6584 ();
extern "C" void Array_GetValue_m6585 ();
extern "C" void Array_SetValue_m6586 ();
extern "C" void Array_SetValue_m6587 ();
extern "C" void Array_SetValue_m6588 ();
extern "C" void Array_SetValue_m4821 ();
extern "C" void Array_SetValue_m6589 ();
extern "C" void Array_SetValue_m6590 ();
extern "C" void Array_CreateInstance_m6591 ();
extern "C" void Array_CreateInstance_m6592 ();
extern "C" void Array_CreateInstance_m6593 ();
extern "C" void Array_CreateInstance_m6594 ();
extern "C" void Array_CreateInstance_m6595 ();
extern "C" void Array_GetIntArray_m6596 ();
extern "C" void Array_CreateInstance_m6597 ();
extern "C" void Array_GetValue_m6598 ();
extern "C" void Array_SetValue_m6599 ();
extern "C" void Array_BinarySearch_m6600 ();
extern "C" void Array_BinarySearch_m6601 ();
extern "C" void Array_BinarySearch_m6602 ();
extern "C" void Array_BinarySearch_m6603 ();
extern "C" void Array_DoBinarySearch_m6604 ();
extern "C" void Array_Clear_m3896 ();
extern "C" void Array_ClearInternal_m6605 ();
extern "C" void Array_Clone_m6606 ();
extern "C" void Array_Copy_m4965 ();
extern "C" void Array_Copy_m6607 ();
extern "C" void Array_Copy_m6608 ();
extern "C" void Array_Copy_m6609 ();
extern "C" void Array_IndexOf_m6610 ();
extern "C" void Array_IndexOf_m6611 ();
extern "C" void Array_IndexOf_m6612 ();
extern "C" void Array_Initialize_m6613 ();
extern "C" void Array_LastIndexOf_m6614 ();
extern "C" void Array_LastIndexOf_m6615 ();
extern "C" void Array_LastIndexOf_m6616 ();
extern "C" void Array_get_swapper_m6617 ();
extern "C" void Array_Reverse_m5820 ();
extern "C" void Array_Reverse_m5845 ();
extern "C" void Array_Sort_m6618 ();
extern "C" void Array_Sort_m6619 ();
extern "C" void Array_Sort_m6620 ();
extern "C" void Array_Sort_m6621 ();
extern "C" void Array_Sort_m6622 ();
extern "C" void Array_Sort_m6623 ();
extern "C" void Array_Sort_m6624 ();
extern "C" void Array_Sort_m6625 ();
extern "C" void Array_int_swapper_m6626 ();
extern "C" void Array_obj_swapper_m6627 ();
extern "C" void Array_slow_swapper_m6628 ();
extern "C" void Array_double_swapper_m6629 ();
extern "C" void Array_new_gap_m6630 ();
extern "C" void Array_combsort_m6631 ();
extern "C" void Array_combsort_m6632 ();
extern "C" void Array_combsort_m6633 ();
extern "C" void Array_qsort_m6634 ();
extern "C" void Array_swap_m6635 ();
extern "C" void Array_compare_m6636 ();
extern "C" void Array_CopyTo_m6637 ();
extern "C" void Array_CopyTo_m6638 ();
extern "C" void Array_ConstrainedCopy_m6639 ();
extern "C" void Type__ctor_m6640 ();
extern "C" void Type__cctor_m6641 ();
extern "C" void Type_FilterName_impl_m6642 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m6643 ();
extern "C" void Type_FilterAttribute_impl_m6644 ();
extern "C" void Type_get_Attributes_m6645 ();
extern "C" void Type_get_DeclaringType_m6646 ();
extern "C" void Type_get_HasElementType_m6647 ();
extern "C" void Type_get_IsAbstract_m6648 ();
extern "C" void Type_get_IsArray_m6649 ();
extern "C" void Type_get_IsByRef_m6650 ();
extern "C" void Type_get_IsClass_m6651 ();
extern "C" void Type_get_IsContextful_m6652 ();
extern "C" void Type_get_IsEnum_m6653 ();
extern "C" void Type_get_IsExplicitLayout_m6654 ();
extern "C" void Type_get_IsInterface_m6655 ();
extern "C" void Type_get_IsMarshalByRef_m6656 ();
extern "C" void Type_get_IsPointer_m6657 ();
extern "C" void Type_get_IsPrimitive_m6658 ();
extern "C" void Type_get_IsSealed_m6659 ();
extern "C" void Type_get_IsSerializable_m6660 ();
extern "C" void Type_get_IsValueType_m6661 ();
extern "C" void Type_get_MemberType_m6662 ();
extern "C" void Type_get_ReflectedType_m6663 ();
extern "C" void Type_get_TypeHandle_m6664 ();
extern "C" void Type_Equals_m6665 ();
extern "C" void Type_Equals_m6666 ();
extern "C" void Type_EqualsInternal_m6667 ();
extern "C" void Type_internal_from_handle_m6668 ();
extern "C" void Type_internal_from_name_m6669 ();
extern "C" void Type_GetType_m6670 ();
extern "C" void Type_GetType_m2232 ();
extern "C" void Type_GetTypeCodeInternal_m6671 ();
extern "C" void Type_GetTypeCode_m3833 ();
extern "C" void Type_GetTypeFromHandle_m651 ();
extern "C" void Type_GetTypeHandle_m6672 ();
extern "C" void Type_type_is_subtype_of_m6673 ();
extern "C" void Type_type_is_assignable_from_m6674 ();
extern "C" void Type_IsSubclassOf_m6675 ();
extern "C" void Type_IsAssignableFrom_m6676 ();
extern "C" void Type_IsInstanceOfType_m6677 ();
extern "C" void Type_GetHashCode_m6678 ();
extern "C" void Type_GetMethod_m6679 ();
extern "C" void Type_GetMethod_m6680 ();
extern "C" void Type_GetMethod_m6681 ();
extern "C" void Type_GetMethod_m6682 ();
extern "C" void Type_GetProperty_m6683 ();
extern "C" void Type_GetProperty_m6684 ();
extern "C" void Type_GetProperty_m6685 ();
extern "C" void Type_GetProperty_m6686 ();
extern "C" void Type_IsArrayImpl_m6687 ();
extern "C" void Type_IsValueTypeImpl_m6688 ();
extern "C" void Type_IsContextfulImpl_m6689 ();
extern "C" void Type_IsMarshalByRefImpl_m6690 ();
extern "C" void Type_GetConstructor_m6691 ();
extern "C" void Type_GetConstructor_m6692 ();
extern "C" void Type_GetConstructor_m6693 ();
extern "C" void Type_ToString_m6694 ();
extern "C" void Type_get_IsSystemType_m6695 ();
extern "C" void Type_GetGenericArguments_m6696 ();
extern "C" void Type_get_ContainsGenericParameters_m6697 ();
extern "C" void Type_get_IsGenericTypeDefinition_m6698 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m6699 ();
extern "C" void Type_GetGenericTypeDefinition_m6700 ();
extern "C" void Type_get_IsGenericType_m6701 ();
extern "C" void Type_MakeGenericType_m6702 ();
extern "C" void Type_MakeGenericType_m6703 ();
extern "C" void Type_get_IsGenericParameter_m6704 ();
extern "C" void Type_get_IsNested_m6705 ();
extern "C" void Type_GetPseudoCustomAttributes_m6706 ();
extern "C" void MemberInfo__ctor_m6707 ();
extern "C" void MemberInfo_get_Module_m6708 ();
extern "C" void Exception__ctor_m5817 ();
extern "C" void Exception__ctor_m2155 ();
extern "C" void Exception__ctor_m2222 ();
extern "C" void Exception__ctor_m2221 ();
extern "C" void Exception_get_InnerException_m6709 ();
extern "C" void Exception_set_HResult_m2220 ();
extern "C" void Exception_get_ClassName_m6710 ();
extern "C" void Exception_get_Message_m6711 ();
extern "C" void Exception_get_Source_m6712 ();
extern "C" void Exception_get_StackTrace_m6713 ();
extern "C" void Exception_GetObjectData_m4979 ();
extern "C" void Exception_ToString_m6714 ();
extern "C" void Exception_GetFullNameForStackTrace_m6715 ();
extern "C" void Exception_GetType_m6716 ();
extern "C" void RuntimeFieldHandle__ctor_m6717 ();
extern "C" void RuntimeFieldHandle_get_Value_m6718 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m6719 ();
extern "C" void RuntimeFieldHandle_Equals_m6720 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m6721 ();
extern "C" void RuntimeTypeHandle__ctor_m6722 ();
extern "C" void RuntimeTypeHandle_get_Value_m6723 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m6724 ();
extern "C" void RuntimeTypeHandle_Equals_m6725 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m6726 ();
extern "C" void ParamArrayAttribute__ctor_m6727 ();
extern "C" void OutAttribute__ctor_m6728 ();
extern "C" void ObsoleteAttribute__ctor_m6729 ();
extern "C" void ObsoleteAttribute__ctor_m6730 ();
extern "C" void ObsoleteAttribute__ctor_m6731 ();
extern "C" void DllImportAttribute__ctor_m6732 ();
extern "C" void DllImportAttribute_get_Value_m6733 ();
extern "C" void MarshalAsAttribute__ctor_m6734 ();
extern "C" void InAttribute__ctor_m6735 ();
extern "C" void GuidAttribute__ctor_m6736 ();
extern "C" void AssemblyCultureAttribute__ctor_m6737 ();
extern "C" void AssemblyVersionAttribute__ctor_m6738 ();
extern "C" void ComImportAttribute__ctor_m6739 ();
extern "C" void OptionalAttribute__ctor_m6740 ();
extern "C" void CompilerGeneratedAttribute__ctor_m6741 ();
extern "C" void InternalsVisibleToAttribute__ctor_m6742 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m6743 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6744 ();
extern "C" void DebuggerHiddenAttribute__ctor_m6745 ();
extern "C" void DefaultMemberAttribute__ctor_m6746 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m6747 ();
extern "C" void DecimalConstantAttribute__ctor_m6748 ();
extern "C" void FieldOffsetAttribute__ctor_m6749 ();
extern "C" void AsyncCallback__ctor_m5879 ();
extern "C" void AsyncCallback_Invoke_m6750 ();
extern "C" void AsyncCallback_BeginInvoke_m5877 ();
extern "C" void AsyncCallback_EndInvoke_m6751 ();
extern "C" void TypedReference_Equals_m6752 ();
extern "C" void TypedReference_GetHashCode_m6753 ();
extern "C" void ArgIterator_Equals_m6754 ();
extern "C" void ArgIterator_GetHashCode_m6755 ();
extern "C" void MarshalByRefObject__ctor_m4859 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m6756 ();
extern "C" void RuntimeHelpers_InitializeArray_m6757 ();
extern "C" void RuntimeHelpers_InitializeArray_m493 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m6758 ();
extern "C" void Locale_GetText_m6759 ();
extern "C" void Locale_GetText_m6760 ();
extern "C" void MonoTODOAttribute__ctor_m6761 ();
extern "C" void MonoTODOAttribute__ctor_m6762 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m6763 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m6764 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6765 ();
extern "C" void SafeWaitHandle__ctor_m6766 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m6767 ();
extern "C" void TableRange__ctor_m6768 ();
extern "C" void CodePointIndexer__ctor_m6769 ();
extern "C" void CodePointIndexer_ToIndex_m6770 ();
extern "C" void TailoringInfo__ctor_m6771 ();
extern "C" void Contraction__ctor_m6772 ();
extern "C" void ContractionComparer__ctor_m6773 ();
extern "C" void ContractionComparer__cctor_m6774 ();
extern "C" void ContractionComparer_Compare_m6775 ();
extern "C" void Level2Map__ctor_m6776 ();
extern "C" void Level2MapComparer__ctor_m6777 ();
extern "C" void Level2MapComparer__cctor_m6778 ();
extern "C" void Level2MapComparer_Compare_m6779 ();
extern "C" void MSCompatUnicodeTable__cctor_m6780 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m6781 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m6782 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m6783 ();
extern "C" void MSCompatUnicodeTable_Category_m6784 ();
extern "C" void MSCompatUnicodeTable_Level1_m6785 ();
extern "C" void MSCompatUnicodeTable_Level2_m6786 ();
extern "C" void MSCompatUnicodeTable_Level3_m6787 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m6788 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m6789 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m6790 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m6791 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m6792 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m6793 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m6794 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m6795 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m6796 ();
extern "C" void MSCompatUnicodeTable_GetResource_m6797 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m6798 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m6799 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m6800 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m6801 ();
extern "C" void Context__ctor_m6802 ();
extern "C" void PreviousInfo__ctor_m6803 ();
extern "C" void SimpleCollator__ctor_m6804 ();
extern "C" void SimpleCollator__cctor_m6805 ();
extern "C" void SimpleCollator_SetCJKTable_m6806 ();
extern "C" void SimpleCollator_GetNeutralCulture_m6807 ();
extern "C" void SimpleCollator_Category_m6808 ();
extern "C" void SimpleCollator_Level1_m6809 ();
extern "C" void SimpleCollator_Level2_m6810 ();
extern "C" void SimpleCollator_IsHalfKana_m6811 ();
extern "C" void SimpleCollator_GetContraction_m6812 ();
extern "C" void SimpleCollator_GetContraction_m6813 ();
extern "C" void SimpleCollator_GetTailContraction_m6814 ();
extern "C" void SimpleCollator_GetTailContraction_m6815 ();
extern "C" void SimpleCollator_FilterOptions_m6816 ();
extern "C" void SimpleCollator_GetExtenderType_m6817 ();
extern "C" void SimpleCollator_ToDashTypeValue_m6818 ();
extern "C" void SimpleCollator_FilterExtender_m6819 ();
extern "C" void SimpleCollator_IsIgnorable_m6820 ();
extern "C" void SimpleCollator_IsSafe_m6821 ();
extern "C" void SimpleCollator_GetSortKey_m6822 ();
extern "C" void SimpleCollator_GetSortKey_m6823 ();
extern "C" void SimpleCollator_GetSortKey_m6824 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m6825 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m6826 ();
extern "C" void SimpleCollator_CompareOrdinal_m6827 ();
extern "C" void SimpleCollator_CompareQuick_m6828 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m6829 ();
extern "C" void SimpleCollator_Compare_m6830 ();
extern "C" void SimpleCollator_ClearBuffer_m6831 ();
extern "C" void SimpleCollator_QuickCheckPossible_m6832 ();
extern "C" void SimpleCollator_CompareInternal_m6833 ();
extern "C" void SimpleCollator_CompareFlagPair_m6834 ();
extern "C" void SimpleCollator_IsPrefix_m6835 ();
extern "C" void SimpleCollator_IsPrefix_m6836 ();
extern "C" void SimpleCollator_IsPrefix_m6837 ();
extern "C" void SimpleCollator_IsSuffix_m6838 ();
extern "C" void SimpleCollator_IsSuffix_m6839 ();
extern "C" void SimpleCollator_QuickIndexOf_m6840 ();
extern "C" void SimpleCollator_IndexOf_m6841 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m6842 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m6843 ();
extern "C" void SimpleCollator_IndexOfSortKey_m6844 ();
extern "C" void SimpleCollator_IndexOf_m6845 ();
extern "C" void SimpleCollator_LastIndexOf_m6846 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m6847 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6848 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m6849 ();
extern "C" void SimpleCollator_LastIndexOf_m6850 ();
extern "C" void SimpleCollator_MatchesForward_m6851 ();
extern "C" void SimpleCollator_MatchesForwardCore_m6852 ();
extern "C" void SimpleCollator_MatchesPrimitive_m6853 ();
extern "C" void SimpleCollator_MatchesBackward_m6854 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m6855 ();
extern "C" void SortKey__ctor_m6856 ();
extern "C" void SortKey__ctor_m6857 ();
extern "C" void SortKey_Compare_m6858 ();
extern "C" void SortKey_get_OriginalString_m6859 ();
extern "C" void SortKey_get_KeyData_m6860 ();
extern "C" void SortKey_Equals_m6861 ();
extern "C" void SortKey_GetHashCode_m6862 ();
extern "C" void SortKey_ToString_m6863 ();
extern "C" void SortKeyBuffer__ctor_m6864 ();
extern "C" void SortKeyBuffer_Reset_m6865 ();
extern "C" void SortKeyBuffer_Initialize_m6866 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m6867 ();
extern "C" void SortKeyBuffer_AppendKana_m6868 ();
extern "C" void SortKeyBuffer_AppendNormal_m6869 ();
extern "C" void SortKeyBuffer_AppendLevel5_m6870 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m6871 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m6872 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m6873 ();
extern "C" void SortKeyBuffer_GetResult_m6874 ();
extern "C" void PrimeGeneratorBase__ctor_m6875 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m6876 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m6877 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m6878 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m6879 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6880 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6881 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6882 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6883 ();
extern "C" void PrimalityTests_GetSPPRounds_m6884 ();
extern "C" void PrimalityTests_Test_m6885 ();
extern "C" void PrimalityTests_RabinMillerTest_m6886 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m6887 ();
extern "C" void ModulusRing__ctor_m6888 ();
extern "C" void ModulusRing_BarrettReduction_m6889 ();
extern "C" void ModulusRing_Multiply_m6890 ();
extern "C" void ModulusRing_Difference_m6891 ();
extern "C" void ModulusRing_Pow_m6892 ();
extern "C" void ModulusRing_Pow_m6893 ();
extern "C" void Kernel_AddSameSign_m6894 ();
extern "C" void Kernel_Subtract_m6895 ();
extern "C" void Kernel_MinusEq_m6896 ();
extern "C" void Kernel_PlusEq_m6897 ();
extern "C" void Kernel_Compare_m6898 ();
extern "C" void Kernel_SingleByteDivideInPlace_m6899 ();
extern "C" void Kernel_DwordMod_m6900 ();
extern "C" void Kernel_DwordDivMod_m6901 ();
extern "C" void Kernel_multiByteDivide_m6902 ();
extern "C" void Kernel_LeftShift_m6903 ();
extern "C" void Kernel_RightShift_m6904 ();
extern "C" void Kernel_MultiplyByDword_m6905 ();
extern "C" void Kernel_Multiply_m6906 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m6907 ();
extern "C" void Kernel_modInverse_m6908 ();
extern "C" void Kernel_modInverse_m6909 ();
extern "C" void BigInteger__ctor_m6910 ();
extern "C" void BigInteger__ctor_m6911 ();
extern "C" void BigInteger__ctor_m6912 ();
extern "C" void BigInteger__ctor_m6913 ();
extern "C" void BigInteger__ctor_m6914 ();
extern "C" void BigInteger__cctor_m6915 ();
extern "C" void BigInteger_get_Rng_m6916 ();
extern "C" void BigInteger_GenerateRandom_m6917 ();
extern "C" void BigInteger_GenerateRandom_m6918 ();
extern "C" void BigInteger_Randomize_m6919 ();
extern "C" void BigInteger_Randomize_m6920 ();
extern "C" void BigInteger_BitCount_m6921 ();
extern "C" void BigInteger_TestBit_m6922 ();
extern "C" void BigInteger_TestBit_m6923 ();
extern "C" void BigInteger_SetBit_m6924 ();
extern "C" void BigInteger_SetBit_m6925 ();
extern "C" void BigInteger_LowestSetBit_m6926 ();
extern "C" void BigInteger_GetBytes_m6927 ();
extern "C" void BigInteger_ToString_m6928 ();
extern "C" void BigInteger_ToString_m6929 ();
extern "C" void BigInteger_Normalize_m6930 ();
extern "C" void BigInteger_Clear_m6931 ();
extern "C" void BigInteger_GetHashCode_m6932 ();
extern "C" void BigInteger_ToString_m6933 ();
extern "C" void BigInteger_Equals_m6934 ();
extern "C" void BigInteger_ModInverse_m6935 ();
extern "C" void BigInteger_ModPow_m6936 ();
extern "C" void BigInteger_IsProbablePrime_m6937 ();
extern "C" void BigInteger_GeneratePseudoPrime_m6938 ();
extern "C" void BigInteger_Incr2_m6939 ();
extern "C" void BigInteger_op_Implicit_m6940 ();
extern "C" void BigInteger_op_Implicit_m6941 ();
extern "C" void BigInteger_op_Addition_m6942 ();
extern "C" void BigInteger_op_Subtraction_m6943 ();
extern "C" void BigInteger_op_Modulus_m6944 ();
extern "C" void BigInteger_op_Modulus_m6945 ();
extern "C" void BigInteger_op_Division_m6946 ();
extern "C" void BigInteger_op_Multiply_m6947 ();
extern "C" void BigInteger_op_Multiply_m6948 ();
extern "C" void BigInteger_op_LeftShift_m6949 ();
extern "C" void BigInteger_op_RightShift_m6950 ();
extern "C" void BigInteger_op_Equality_m6951 ();
extern "C" void BigInteger_op_Inequality_m6952 ();
extern "C" void BigInteger_op_Equality_m6953 ();
extern "C" void BigInteger_op_Inequality_m6954 ();
extern "C" void BigInteger_op_GreaterThan_m6955 ();
extern "C" void BigInteger_op_LessThan_m6956 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m6957 ();
extern "C" void BigInteger_op_LessThanOrEqual_m6958 ();
extern "C" void CryptoConvert_ToInt32LE_m6959 ();
extern "C" void CryptoConvert_ToUInt32LE_m6960 ();
extern "C" void CryptoConvert_GetBytesLE_m6961 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m6962 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6963 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6964 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m6965 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m6966 ();
extern "C" void KeyBuilder_get_Rng_m6967 ();
extern "C" void KeyBuilder_Key_m6968 ();
extern "C" void KeyBuilder_IV_m6969 ();
extern "C" void BlockProcessor__ctor_m6970 ();
extern "C" void BlockProcessor_Finalize_m6971 ();
extern "C" void BlockProcessor_Initialize_m6972 ();
extern "C" void BlockProcessor_Core_m6973 ();
extern "C" void BlockProcessor_Core_m6974 ();
extern "C" void BlockProcessor_Final_m6975 ();
extern "C" void KeyGeneratedEventHandler__ctor_m6976 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m6977 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m6978 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m6979 ();
extern "C" void DSAManaged__ctor_m6980 ();
extern "C" void DSAManaged_add_KeyGenerated_m6981 ();
extern "C" void DSAManaged_remove_KeyGenerated_m6982 ();
extern "C" void DSAManaged_Finalize_m6983 ();
extern "C" void DSAManaged_Generate_m6984 ();
extern "C" void DSAManaged_GenerateKeyPair_m6985 ();
extern "C" void DSAManaged_add_m6986 ();
extern "C" void DSAManaged_GenerateParams_m6987 ();
extern "C" void DSAManaged_get_Random_m6988 ();
extern "C" void DSAManaged_get_KeySize_m6989 ();
extern "C" void DSAManaged_get_PublicOnly_m6990 ();
extern "C" void DSAManaged_NormalizeArray_m6991 ();
extern "C" void DSAManaged_ExportParameters_m6992 ();
extern "C" void DSAManaged_ImportParameters_m6993 ();
extern "C" void DSAManaged_CreateSignature_m6994 ();
extern "C" void DSAManaged_VerifySignature_m6995 ();
extern "C" void DSAManaged_Dispose_m6996 ();
extern "C" void KeyPairPersistence__ctor_m6997 ();
extern "C" void KeyPairPersistence__ctor_m6998 ();
extern "C" void KeyPairPersistence__cctor_m6999 ();
extern "C" void KeyPairPersistence_get_Filename_m7000 ();
extern "C" void KeyPairPersistence_get_KeyValue_m7001 ();
extern "C" void KeyPairPersistence_set_KeyValue_m7002 ();
extern "C" void KeyPairPersistence_Load_m7003 ();
extern "C" void KeyPairPersistence_Save_m7004 ();
extern "C" void KeyPairPersistence_Remove_m7005 ();
extern "C" void KeyPairPersistence_get_UserPath_m7006 ();
extern "C" void KeyPairPersistence_get_MachinePath_m7007 ();
extern "C" void KeyPairPersistence__CanSecure_m7008 ();
extern "C" void KeyPairPersistence__ProtectUser_m7009 ();
extern "C" void KeyPairPersistence__ProtectMachine_m7010 ();
extern "C" void KeyPairPersistence__IsUserProtected_m7011 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m7012 ();
extern "C" void KeyPairPersistence_CanSecure_m7013 ();
extern "C" void KeyPairPersistence_ProtectUser_m7014 ();
extern "C" void KeyPairPersistence_ProtectMachine_m7015 ();
extern "C" void KeyPairPersistence_IsUserProtected_m7016 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m7017 ();
extern "C" void KeyPairPersistence_get_CanChange_m7018 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m7019 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m7020 ();
extern "C" void KeyPairPersistence_get_ContainerName_m7021 ();
extern "C" void KeyPairPersistence_Copy_m7022 ();
extern "C" void KeyPairPersistence_FromXml_m7023 ();
extern "C" void KeyPairPersistence_ToXml_m7024 ();
extern "C" void MACAlgorithm__ctor_m7025 ();
extern "C" void MACAlgorithm_Initialize_m7026 ();
extern "C" void MACAlgorithm_Core_m7027 ();
extern "C" void MACAlgorithm_Final_m7028 ();
extern "C" void PKCS1__cctor_m7029 ();
extern "C" void PKCS1_Compare_m7030 ();
extern "C" void PKCS1_I2OSP_m7031 ();
extern "C" void PKCS1_OS2IP_m7032 ();
extern "C" void PKCS1_RSAEP_m7033 ();
extern "C" void PKCS1_RSASP1_m7034 ();
extern "C" void PKCS1_RSAVP1_m7035 ();
extern "C" void PKCS1_Encrypt_v15_m7036 ();
extern "C" void PKCS1_Sign_v15_m7037 ();
extern "C" void PKCS1_Verify_v15_m7038 ();
extern "C" void PKCS1_Verify_v15_m7039 ();
extern "C" void PKCS1_Encode_v15_m7040 ();
extern "C" void PrivateKeyInfo__ctor_m7041 ();
extern "C" void PrivateKeyInfo__ctor_m7042 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m7043 ();
extern "C" void PrivateKeyInfo_Decode_m7044 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m7045 ();
extern "C" void PrivateKeyInfo_Normalize_m7046 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m7047 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m7048 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m7049 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m7050 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m7051 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m7052 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m7053 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m7054 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m7055 ();
extern "C" void KeyGeneratedEventHandler__ctor_m7056 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m7057 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m7058 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m7059 ();
extern "C" void RSAManaged__ctor_m7060 ();
extern "C" void RSAManaged_add_KeyGenerated_m7061 ();
extern "C" void RSAManaged_remove_KeyGenerated_m7062 ();
extern "C" void RSAManaged_Finalize_m7063 ();
extern "C" void RSAManaged_GenerateKeyPair_m7064 ();
extern "C" void RSAManaged_get_KeySize_m7065 ();
extern "C" void RSAManaged_get_PublicOnly_m7066 ();
extern "C" void RSAManaged_DecryptValue_m7067 ();
extern "C" void RSAManaged_EncryptValue_m7068 ();
extern "C" void RSAManaged_ExportParameters_m7069 ();
extern "C" void RSAManaged_ImportParameters_m7070 ();
extern "C" void RSAManaged_Dispose_m7071 ();
extern "C" void RSAManaged_ToXmlString_m7072 ();
extern "C" void RSAManaged_get_IsCrtPossible_m7073 ();
extern "C" void RSAManaged_GetPaddedValue_m7074 ();
extern "C" void SymmetricTransform__ctor_m7075 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m7076 ();
extern "C" void SymmetricTransform_Finalize_m7077 ();
extern "C" void SymmetricTransform_Dispose_m7078 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m7079 ();
extern "C" void SymmetricTransform_Transform_m7080 ();
extern "C" void SymmetricTransform_CBC_m7081 ();
extern "C" void SymmetricTransform_CFB_m7082 ();
extern "C" void SymmetricTransform_OFB_m7083 ();
extern "C" void SymmetricTransform_CTS_m7084 ();
extern "C" void SymmetricTransform_CheckInput_m7085 ();
extern "C" void SymmetricTransform_TransformBlock_m7086 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m7087 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m7088 ();
extern "C" void SymmetricTransform_Random_m7089 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m7090 ();
extern "C" void SymmetricTransform_FinalEncrypt_m7091 ();
extern "C" void SymmetricTransform_FinalDecrypt_m7092 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m7093 ();
extern "C" void SafeBag__ctor_m7094 ();
extern "C" void SafeBag_get_BagOID_m7095 ();
extern "C" void SafeBag_get_ASN1_m7096 ();
extern "C" void DeriveBytes__ctor_m7097 ();
extern "C" void DeriveBytes__cctor_m7098 ();
extern "C" void DeriveBytes_set_HashName_m7099 ();
extern "C" void DeriveBytes_set_IterationCount_m7100 ();
extern "C" void DeriveBytes_set_Password_m7101 ();
extern "C" void DeriveBytes_set_Salt_m7102 ();
extern "C" void DeriveBytes_Adjust_m7103 ();
extern "C" void DeriveBytes_Derive_m7104 ();
extern "C" void DeriveBytes_DeriveKey_m7105 ();
extern "C" void DeriveBytes_DeriveIV_m7106 ();
extern "C" void DeriveBytes_DeriveMAC_m7107 ();
extern "C" void PKCS12__ctor_m7108 ();
extern "C" void PKCS12__ctor_m7109 ();
extern "C" void PKCS12__ctor_m7110 ();
extern "C" void PKCS12__cctor_m7111 ();
extern "C" void PKCS12_Decode_m7112 ();
extern "C" void PKCS12_Finalize_m7113 ();
extern "C" void PKCS12_set_Password_m7114 ();
extern "C" void PKCS12_get_IterationCount_m7115 ();
extern "C" void PKCS12_set_IterationCount_m7116 ();
extern "C" void PKCS12_get_Certificates_m7117 ();
extern "C" void PKCS12_get_RNG_m7118 ();
extern "C" void PKCS12_Compare_m7119 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m7120 ();
extern "C" void PKCS12_Decrypt_m7121 ();
extern "C" void PKCS12_Decrypt_m7122 ();
extern "C" void PKCS12_Encrypt_m7123 ();
extern "C" void PKCS12_GetExistingParameters_m7124 ();
extern "C" void PKCS12_AddPrivateKey_m7125 ();
extern "C" void PKCS12_ReadSafeBag_m7126 ();
extern "C" void PKCS12_CertificateSafeBag_m7127 ();
extern "C" void PKCS12_MAC_m7128 ();
extern "C" void PKCS12_GetBytes_m7129 ();
extern "C" void PKCS12_EncryptedContentInfo_m7130 ();
extern "C" void PKCS12_AddCertificate_m7131 ();
extern "C" void PKCS12_AddCertificate_m7132 ();
extern "C" void PKCS12_RemoveCertificate_m7133 ();
extern "C" void PKCS12_RemoveCertificate_m7134 ();
extern "C" void PKCS12_Clone_m7135 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m7136 ();
extern "C" void X501__cctor_m7137 ();
extern "C" void X501_ToString_m7138 ();
extern "C" void X501_ToString_m7139 ();
extern "C" void X501_AppendEntry_m7140 ();
extern "C" void X509Certificate__ctor_m7141 ();
extern "C" void X509Certificate__cctor_m7142 ();
extern "C" void X509Certificate_Parse_m7143 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m7144 ();
extern "C" void X509Certificate_get_DSA_m7145 ();
extern "C" void X509Certificate_get_IssuerName_m7146 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m7147 ();
extern "C" void X509Certificate_get_PublicKey_m7148 ();
extern "C" void X509Certificate_get_RawData_m7149 ();
extern "C" void X509Certificate_get_SubjectName_m7150 ();
extern "C" void X509Certificate_get_ValidFrom_m7151 ();
extern "C" void X509Certificate_get_ValidUntil_m7152 ();
extern "C" void X509Certificate_GetIssuerName_m7153 ();
extern "C" void X509Certificate_GetSubjectName_m7154 ();
extern "C" void X509Certificate_GetObjectData_m7155 ();
extern "C" void X509Certificate_PEM_m7156 ();
extern "C" void X509CertificateEnumerator__ctor_m7157 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m7158 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m7159 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m7160 ();
extern "C" void X509CertificateEnumerator_get_Current_m7161 ();
extern "C" void X509CertificateEnumerator_MoveNext_m7162 ();
extern "C" void X509CertificateEnumerator_Reset_m7163 ();
extern "C" void X509CertificateCollection__ctor_m7164 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m7165 ();
extern "C" void X509CertificateCollection_get_Item_m7166 ();
extern "C" void X509CertificateCollection_Add_m7167 ();
extern "C" void X509CertificateCollection_GetEnumerator_m7168 ();
extern "C" void X509CertificateCollection_GetHashCode_m7169 ();
extern "C" void X509Extension__ctor_m7170 ();
extern "C" void X509Extension_Decode_m7171 ();
extern "C" void X509Extension_Equals_m7172 ();
extern "C" void X509Extension_GetHashCode_m7173 ();
extern "C" void X509Extension_WriteLine_m7174 ();
extern "C" void X509Extension_ToString_m7175 ();
extern "C" void X509ExtensionCollection__ctor_m7176 ();
extern "C" void X509ExtensionCollection__ctor_m7177 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m7178 ();
extern "C" void ASN1__ctor_m7179 ();
extern "C" void ASN1__ctor_m7180 ();
extern "C" void ASN1__ctor_m7181 ();
extern "C" void ASN1_get_Count_m7182 ();
extern "C" void ASN1_get_Tag_m7183 ();
extern "C" void ASN1_get_Length_m7184 ();
extern "C" void ASN1_get_Value_m7185 ();
extern "C" void ASN1_set_Value_m7186 ();
extern "C" void ASN1_CompareArray_m7187 ();
extern "C" void ASN1_CompareValue_m7188 ();
extern "C" void ASN1_Add_m7189 ();
extern "C" void ASN1_GetBytes_m7190 ();
extern "C" void ASN1_Decode_m7191 ();
extern "C" void ASN1_DecodeTLV_m7192 ();
extern "C" void ASN1_get_Item_m7193 ();
extern "C" void ASN1_Element_m7194 ();
extern "C" void ASN1_ToString_m7195 ();
extern "C" void ASN1Convert_FromInt32_m7196 ();
extern "C" void ASN1Convert_FromOid_m7197 ();
extern "C" void ASN1Convert_ToInt32_m7198 ();
extern "C" void ASN1Convert_ToOid_m7199 ();
extern "C" void ASN1Convert_ToDateTime_m7200 ();
extern "C" void BitConverterLE_GetUIntBytes_m7201 ();
extern "C" void BitConverterLE_GetBytes_m7202 ();
extern "C" void BitConverterLE_UShortFromBytes_m7203 ();
extern "C" void BitConverterLE_UIntFromBytes_m7204 ();
extern "C" void BitConverterLE_ULongFromBytes_m7205 ();
extern "C" void BitConverterLE_ToInt16_m7206 ();
extern "C" void BitConverterLE_ToInt32_m7207 ();
extern "C" void BitConverterLE_ToSingle_m7208 ();
extern "C" void BitConverterLE_ToDouble_m7209 ();
extern "C" void ContentInfo__ctor_m7210 ();
extern "C" void ContentInfo__ctor_m7211 ();
extern "C" void ContentInfo__ctor_m7212 ();
extern "C" void ContentInfo__ctor_m7213 ();
extern "C" void ContentInfo_get_ASN1_m7214 ();
extern "C" void ContentInfo_get_Content_m7215 ();
extern "C" void ContentInfo_set_Content_m7216 ();
extern "C" void ContentInfo_get_ContentType_m7217 ();
extern "C" void ContentInfo_set_ContentType_m7218 ();
extern "C" void ContentInfo_GetASN1_m7219 ();
extern "C" void EncryptedData__ctor_m7220 ();
extern "C" void EncryptedData__ctor_m7221 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m7222 ();
extern "C" void EncryptedData_get_EncryptedContent_m7223 ();
extern "C" void StrongName__cctor_m7224 ();
extern "C" void StrongName_get_PublicKey_m7225 ();
extern "C" void StrongName_get_PublicKeyToken_m7226 ();
extern "C" void StrongName_get_TokenAlgorithm_m7227 ();
extern "C" void SecurityParser__ctor_m7228 ();
extern "C" void SecurityParser_LoadXml_m7229 ();
extern "C" void SecurityParser_ToXml_m7230 ();
extern "C" void SecurityParser_OnStartParsing_m7231 ();
extern "C" void SecurityParser_OnProcessingInstruction_m7232 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m7233 ();
extern "C" void SecurityParser_OnStartElement_m7234 ();
extern "C" void SecurityParser_OnEndElement_m7235 ();
extern "C" void SecurityParser_OnChars_m7236 ();
extern "C" void SecurityParser_OnEndParsing_m7237 ();
extern "C" void AttrListImpl__ctor_m7238 ();
extern "C" void AttrListImpl_get_Length_m7239 ();
extern "C" void AttrListImpl_GetName_m7240 ();
extern "C" void AttrListImpl_GetValue_m7241 ();
extern "C" void AttrListImpl_GetValue_m7242 ();
extern "C" void AttrListImpl_get_Names_m7243 ();
extern "C" void AttrListImpl_get_Values_m7244 ();
extern "C" void AttrListImpl_Clear_m7245 ();
extern "C" void AttrListImpl_Add_m7246 ();
extern "C" void SmallXmlParser__ctor_m7247 ();
extern "C" void SmallXmlParser_Error_m7248 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m7249 ();
extern "C" void SmallXmlParser_IsNameChar_m7250 ();
extern "C" void SmallXmlParser_IsWhitespace_m7251 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m7252 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m7253 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m7254 ();
extern "C" void SmallXmlParser_Peek_m7255 ();
extern "C" void SmallXmlParser_Read_m7256 ();
extern "C" void SmallXmlParser_Expect_m7257 ();
extern "C" void SmallXmlParser_ReadUntil_m7258 ();
extern "C" void SmallXmlParser_ReadName_m7259 ();
extern "C" void SmallXmlParser_Parse_m7260 ();
extern "C" void SmallXmlParser_Cleanup_m7261 ();
extern "C" void SmallXmlParser_ReadContent_m7262 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m7263 ();
extern "C" void SmallXmlParser_ReadCharacters_m7264 ();
extern "C" void SmallXmlParser_ReadReference_m7265 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m7266 ();
extern "C" void SmallXmlParser_ReadAttribute_m7267 ();
extern "C" void SmallXmlParser_ReadCDATASection_m7268 ();
extern "C" void SmallXmlParser_ReadComment_m7269 ();
extern "C" void SmallXmlParserException__ctor_m7270 ();
extern "C" void Runtime_GetDisplayName_m7271 ();
extern "C" void KeyNotFoundException__ctor_m7272 ();
extern "C" void KeyNotFoundException__ctor_m7273 ();
extern "C" void SimpleEnumerator__ctor_m7274 ();
extern "C" void SimpleEnumerator__cctor_m7275 ();
extern "C" void SimpleEnumerator_Clone_m7276 ();
extern "C" void SimpleEnumerator_MoveNext_m7277 ();
extern "C" void SimpleEnumerator_get_Current_m7278 ();
extern "C" void SimpleEnumerator_Reset_m7279 ();
extern "C" void ArrayListWrapper__ctor_m7280 ();
extern "C" void ArrayListWrapper_get_Item_m7281 ();
extern "C" void ArrayListWrapper_set_Item_m7282 ();
extern "C" void ArrayListWrapper_get_Count_m7283 ();
extern "C" void ArrayListWrapper_get_Capacity_m7284 ();
extern "C" void ArrayListWrapper_set_Capacity_m7285 ();
extern "C" void ArrayListWrapper_get_IsFixedSize_m7286 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m7287 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m7288 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m7289 ();
extern "C" void ArrayListWrapper_Add_m7290 ();
extern "C" void ArrayListWrapper_Clear_m7291 ();
extern "C" void ArrayListWrapper_Contains_m7292 ();
extern "C" void ArrayListWrapper_IndexOf_m7293 ();
extern "C" void ArrayListWrapper_IndexOf_m7294 ();
extern "C" void ArrayListWrapper_IndexOf_m7295 ();
extern "C" void ArrayListWrapper_Insert_m7296 ();
extern "C" void ArrayListWrapper_InsertRange_m7297 ();
extern "C" void ArrayListWrapper_Remove_m7298 ();
extern "C" void ArrayListWrapper_RemoveAt_m7299 ();
extern "C" void ArrayListWrapper_CopyTo_m7300 ();
extern "C" void ArrayListWrapper_CopyTo_m7301 ();
extern "C" void ArrayListWrapper_CopyTo_m7302 ();
extern "C" void ArrayListWrapper_GetEnumerator_m7303 ();
extern "C" void ArrayListWrapper_AddRange_m7304 ();
extern "C" void ArrayListWrapper_Clone_m7305 ();
extern "C" void ArrayListWrapper_Sort_m7306 ();
extern "C" void ArrayListWrapper_Sort_m7307 ();
extern "C" void ArrayListWrapper_ToArray_m7308 ();
extern "C" void ArrayListWrapper_ToArray_m7309 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m7310 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m7311 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m7312 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m7313 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m7314 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m7315 ();
extern "C" void SynchronizedArrayListWrapper_get_IsFixedSize_m7316 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m7317 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m7318 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m7319 ();
extern "C" void SynchronizedArrayListWrapper_Add_m7320 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m7321 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m7322 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7323 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7324 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7325 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m7326 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m7327 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m7328 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m7329 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7330 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7331 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7332 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m7333 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m7334 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m7335 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m7336 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m7337 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m7338 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m7339 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m7340 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m7341 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m7342 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m7343 ();
extern "C" void FixedSizeArrayListWrapper_get_IsFixedSize_m7344 ();
extern "C" void FixedSizeArrayListWrapper_Add_m7345 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m7346 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m7347 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m7348 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m7349 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m7350 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m7351 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m7352 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m7353 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m7354 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m7355 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m7356 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m7357 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m7358 ();
extern "C" void ArrayList__ctor_m4825 ();
extern "C" void ArrayList__ctor_m4857 ();
extern "C" void ArrayList__ctor_m4935 ();
extern "C" void ArrayList__ctor_m7359 ();
extern "C" void ArrayList__cctor_m7360 ();
extern "C" void ArrayList_get_Item_m7361 ();
extern "C" void ArrayList_set_Item_m7362 ();
extern "C" void ArrayList_get_Count_m7363 ();
extern "C" void ArrayList_get_Capacity_m7364 ();
extern "C" void ArrayList_set_Capacity_m7365 ();
extern "C" void ArrayList_get_IsFixedSize_m7366 ();
extern "C" void ArrayList_get_IsReadOnly_m7367 ();
extern "C" void ArrayList_get_IsSynchronized_m7368 ();
extern "C" void ArrayList_get_SyncRoot_m7369 ();
extern "C" void ArrayList_EnsureCapacity_m7370 ();
extern "C" void ArrayList_Shift_m7371 ();
extern "C" void ArrayList_Add_m7372 ();
extern "C" void ArrayList_Clear_m7373 ();
extern "C" void ArrayList_Contains_m7374 ();
extern "C" void ArrayList_IndexOf_m7375 ();
extern "C" void ArrayList_IndexOf_m7376 ();
extern "C" void ArrayList_IndexOf_m7377 ();
extern "C" void ArrayList_Insert_m7378 ();
extern "C" void ArrayList_InsertRange_m7379 ();
extern "C" void ArrayList_Remove_m7380 ();
extern "C" void ArrayList_RemoveAt_m7381 ();
extern "C" void ArrayList_CopyTo_m7382 ();
extern "C" void ArrayList_CopyTo_m7383 ();
extern "C" void ArrayList_CopyTo_m7384 ();
extern "C" void ArrayList_GetEnumerator_m7385 ();
extern "C" void ArrayList_AddRange_m7386 ();
extern "C" void ArrayList_Sort_m7387 ();
extern "C" void ArrayList_Sort_m7388 ();
extern "C" void ArrayList_ToArray_m7389 ();
extern "C" void ArrayList_ToArray_m7390 ();
extern "C" void ArrayList_Clone_m7391 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m7392 ();
extern "C" void ArrayList_Synchronized_m7393 ();
extern "C" void ArrayList_ReadOnly_m5839 ();
extern "C" void BitArrayEnumerator__ctor_m7394 ();
extern "C" void BitArrayEnumerator_Clone_m7395 ();
extern "C" void BitArrayEnumerator_get_Current_m7396 ();
extern "C" void BitArrayEnumerator_MoveNext_m7397 ();
extern "C" void BitArrayEnumerator_Reset_m7398 ();
extern "C" void BitArrayEnumerator_checkVersion_m7399 ();
extern "C" void BitArray__ctor_m7400 ();
extern "C" void BitArray__ctor_m4968 ();
extern "C" void BitArray_getByte_m7401 ();
extern "C" void BitArray_get_Count_m7402 ();
extern "C" void BitArray_get_IsSynchronized_m7403 ();
extern "C" void BitArray_get_Item_m4963 ();
extern "C" void BitArray_set_Item_m4969 ();
extern "C" void BitArray_get_Length_m4962 ();
extern "C" void BitArray_get_SyncRoot_m7404 ();
extern "C" void BitArray_Clone_m7405 ();
extern "C" void BitArray_CopyTo_m7406 ();
extern "C" void BitArray_Get_m7407 ();
extern "C" void BitArray_Set_m7408 ();
extern "C" void BitArray_GetEnumerator_m7409 ();
extern "C" void CaseInsensitiveComparer__ctor_m7410 ();
extern "C" void CaseInsensitiveComparer__ctor_m7411 ();
extern "C" void CaseInsensitiveComparer__cctor_m7412 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m4814 ();
extern "C" void CaseInsensitiveComparer_Compare_m7413 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m7414 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m7415 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m7416 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m7417 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m7418 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m4815 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m7419 ();
extern "C" void CollectionBase__ctor_m4917 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m7420 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m7421 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_IsSynchronized_m7422 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m7423 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m7424 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m7425 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m7426 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m7427 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsFixedSize_m7428 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsReadOnly_m7429 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m7430 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m7431 ();
extern "C" void CollectionBase_get_Count_m7432 ();
extern "C" void CollectionBase_GetEnumerator_m7433 ();
extern "C" void CollectionBase_Clear_m7434 ();
extern "C" void CollectionBase_RemoveAt_m7435 ();
extern "C" void CollectionBase_get_InnerList_m4911 ();
extern "C" void CollectionBase_get_List_m4966 ();
extern "C" void CollectionBase_OnClear_m7436 ();
extern "C" void CollectionBase_OnClearComplete_m7437 ();
extern "C" void CollectionBase_OnInsert_m7438 ();
extern "C" void CollectionBase_OnInsertComplete_m7439 ();
extern "C" void CollectionBase_OnRemove_m7440 ();
extern "C" void CollectionBase_OnRemoveComplete_m7441 ();
extern "C" void CollectionBase_OnSet_m7442 ();
extern "C" void CollectionBase_OnSetComplete_m7443 ();
extern "C" void CollectionBase_OnValidate_m7444 ();
extern "C" void Comparer__ctor_m7445 ();
extern "C" void Comparer__ctor_m7446 ();
extern "C" void Comparer__cctor_m7447 ();
extern "C" void Comparer_Compare_m7448 ();
extern "C" void Comparer_GetObjectData_m7449 ();
extern "C" void DictionaryEntry__ctor_m4819 ();
extern "C" void DictionaryEntry_get_Key_m7450 ();
extern "C" void DictionaryEntry_get_Value_m7451 ();
extern "C" void KeyMarker__ctor_m7452 ();
extern "C" void KeyMarker__cctor_m7453 ();
extern "C" void Enumerator__ctor_m7454 ();
extern "C" void Enumerator__cctor_m7455 ();
extern "C" void Enumerator_FailFast_m7456 ();
extern "C" void Enumerator_Reset_m7457 ();
extern "C" void Enumerator_MoveNext_m7458 ();
extern "C" void Enumerator_get_Entry_m7459 ();
extern "C" void Enumerator_get_Key_m7460 ();
extern "C" void Enumerator_get_Value_m7461 ();
extern "C" void Enumerator_get_Current_m7462 ();
extern "C" void HashKeys__ctor_m7463 ();
extern "C" void HashKeys_get_Count_m7464 ();
extern "C" void HashKeys_get_IsSynchronized_m7465 ();
extern "C" void HashKeys_get_SyncRoot_m7466 ();
extern "C" void HashKeys_CopyTo_m7467 ();
extern "C" void HashKeys_GetEnumerator_m7468 ();
extern "C" void HashValues__ctor_m7469 ();
extern "C" void HashValues_get_Count_m7470 ();
extern "C" void HashValues_get_IsSynchronized_m7471 ();
extern "C" void HashValues_get_SyncRoot_m7472 ();
extern "C" void HashValues_CopyTo_m7473 ();
extern "C" void HashValues_GetEnumerator_m7474 ();
extern "C" void SyncHashtable__ctor_m7475 ();
extern "C" void SyncHashtable__ctor_m7476 ();
extern "C" void SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m7477 ();
extern "C" void SyncHashtable_GetObjectData_m7478 ();
extern "C" void SyncHashtable_get_Count_m7479 ();
extern "C" void SyncHashtable_get_IsSynchronized_m7480 ();
extern "C" void SyncHashtable_get_SyncRoot_m7481 ();
extern "C" void SyncHashtable_get_Keys_m7482 ();
extern "C" void SyncHashtable_get_Values_m7483 ();
extern "C" void SyncHashtable_get_Item_m7484 ();
extern "C" void SyncHashtable_set_Item_m7485 ();
extern "C" void SyncHashtable_CopyTo_m7486 ();
extern "C" void SyncHashtable_Add_m7487 ();
extern "C" void SyncHashtable_Clear_m7488 ();
extern "C" void SyncHashtable_Contains_m7489 ();
extern "C" void SyncHashtable_GetEnumerator_m7490 ();
extern "C" void SyncHashtable_Remove_m7491 ();
extern "C" void SyncHashtable_ContainsKey_m7492 ();
extern "C" void SyncHashtable_Clone_m7493 ();
extern "C" void Hashtable__ctor_m4954 ();
extern "C" void Hashtable__ctor_m7494 ();
extern "C" void Hashtable__ctor_m7495 ();
extern "C" void Hashtable__ctor_m4957 ();
extern "C" void Hashtable__ctor_m7496 ();
extern "C" void Hashtable__ctor_m4816 ();
extern "C" void Hashtable__ctor_m7497 ();
extern "C" void Hashtable__ctor_m4817 ();
extern "C" void Hashtable__ctor_m4854 ();
extern "C" void Hashtable__ctor_m7498 ();
extern "C" void Hashtable__ctor_m4824 ();
extern "C" void Hashtable__ctor_m7499 ();
extern "C" void Hashtable__cctor_m7500 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m7501 ();
extern "C" void Hashtable_set_comparer_m7502 ();
extern "C" void Hashtable_set_hcp_m7503 ();
extern "C" void Hashtable_get_Count_m7504 ();
extern "C" void Hashtable_get_IsSynchronized_m7505 ();
extern "C" void Hashtable_get_SyncRoot_m7506 ();
extern "C" void Hashtable_get_Keys_m7507 ();
extern "C" void Hashtable_get_Values_m7508 ();
extern "C" void Hashtable_get_Item_m7509 ();
extern "C" void Hashtable_set_Item_m7510 ();
extern "C" void Hashtable_CopyTo_m7511 ();
extern "C" void Hashtable_Add_m7512 ();
extern "C" void Hashtable_Clear_m7513 ();
extern "C" void Hashtable_Contains_m7514 ();
extern "C" void Hashtable_GetEnumerator_m7515 ();
extern "C" void Hashtable_Remove_m7516 ();
extern "C" void Hashtable_ContainsKey_m7517 ();
extern "C" void Hashtable_Clone_m7518 ();
extern "C" void Hashtable_GetObjectData_m7519 ();
extern "C" void Hashtable_OnDeserialization_m7520 ();
extern "C" void Hashtable_Synchronized_m7521 ();
extern "C" void Hashtable_GetHash_m7522 ();
extern "C" void Hashtable_KeyEquals_m7523 ();
extern "C" void Hashtable_AdjustThreshold_m7524 ();
extern "C" void Hashtable_SetTable_m7525 ();
extern "C" void Hashtable_Find_m7526 ();
extern "C" void Hashtable_Rehash_m7527 ();
extern "C" void Hashtable_PutImpl_m7528 ();
extern "C" void Hashtable_CopyToArray_m7529 ();
extern "C" void Hashtable_TestPrime_m7530 ();
extern "C" void Hashtable_CalcPrime_m7531 ();
extern "C" void Hashtable_ToPrime_m7532 ();
extern "C" void Enumerator__ctor_m7533 ();
extern "C" void Enumerator__cctor_m7534 ();
extern "C" void Enumerator_Reset_m7535 ();
extern "C" void Enumerator_MoveNext_m7536 ();
extern "C" void Enumerator_get_Entry_m7537 ();
extern "C" void Enumerator_get_Key_m7538 ();
extern "C" void Enumerator_get_Value_m7539 ();
extern "C" void Enumerator_get_Current_m7540 ();
extern "C" void Enumerator_Clone_m7541 ();
extern "C" void SortedList__ctor_m7542 ();
extern "C" void SortedList__ctor_m4853 ();
extern "C" void SortedList__ctor_m7543 ();
extern "C" void SortedList__ctor_m7544 ();
extern "C" void SortedList__cctor_m7545 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m7546 ();
extern "C" void SortedList_get_Count_m7547 ();
extern "C" void SortedList_get_IsSynchronized_m7548 ();
extern "C" void SortedList_get_SyncRoot_m7549 ();
extern "C" void SortedList_get_IsFixedSize_m7550 ();
extern "C" void SortedList_get_IsReadOnly_m7551 ();
extern "C" void SortedList_get_Item_m7552 ();
extern "C" void SortedList_set_Item_m7553 ();
extern "C" void SortedList_get_Capacity_m7554 ();
extern "C" void SortedList_set_Capacity_m7555 ();
extern "C" void SortedList_Add_m7556 ();
extern "C" void SortedList_Contains_m7557 ();
extern "C" void SortedList_GetEnumerator_m7558 ();
extern "C" void SortedList_Remove_m7559 ();
extern "C" void SortedList_CopyTo_m7560 ();
extern "C" void SortedList_Clone_m7561 ();
extern "C" void SortedList_RemoveAt_m7562 ();
extern "C" void SortedList_IndexOfKey_m7563 ();
extern "C" void SortedList_ContainsKey_m7564 ();
extern "C" void SortedList_GetByIndex_m7565 ();
extern "C" void SortedList_EnsureCapacity_m7566 ();
extern "C" void SortedList_PutImpl_m7567 ();
extern "C" void SortedList_GetImpl_m7568 ();
extern "C" void SortedList_InitTable_m7569 ();
extern "C" void SortedList_Find_m7570 ();
extern "C" void Enumerator__ctor_m7571 ();
extern "C" void Enumerator_Clone_m7572 ();
extern "C" void Enumerator_get_Current_m7573 ();
extern "C" void Enumerator_MoveNext_m7574 ();
extern "C" void Enumerator_Reset_m7575 ();
extern "C" void Stack__ctor_m2244 ();
extern "C" void Stack__ctor_m7576 ();
extern "C" void Stack__ctor_m7577 ();
extern "C" void Stack_Resize_m7578 ();
extern "C" void Stack_get_Count_m7579 ();
extern "C" void Stack_get_IsSynchronized_m7580 ();
extern "C" void Stack_get_SyncRoot_m7581 ();
extern "C" void Stack_Clear_m7582 ();
extern "C" void Stack_Clone_m7583 ();
extern "C" void Stack_CopyTo_m7584 ();
extern "C" void Stack_GetEnumerator_m7585 ();
extern "C" void Stack_Peek_m7586 ();
extern "C" void Stack_Pop_m7587 ();
extern "C" void Stack_Push_m7588 ();
extern "C" void DebuggableAttribute__ctor_m7589 ();
extern "C" void DebuggerDisplayAttribute__ctor_m7590 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m7591 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m7592 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m7593 ();
extern "C" void StackFrame__ctor_m7594 ();
extern "C" void StackFrame__ctor_m7595 ();
extern "C" void StackFrame_get_frame_info_m7596 ();
extern "C" void StackFrame_GetFileLineNumber_m7597 ();
extern "C" void StackFrame_GetFileName_m7598 ();
extern "C" void StackFrame_GetSecureFileName_m7599 ();
extern "C" void StackFrame_GetILOffset_m7600 ();
extern "C" void StackFrame_GetMethod_m7601 ();
extern "C" void StackFrame_GetNativeOffset_m7602 ();
extern "C" void StackFrame_GetInternalMethodName_m7603 ();
extern "C" void StackFrame_ToString_m7604 ();
extern "C" void StackTrace__ctor_m7605 ();
extern "C" void StackTrace__ctor_m2202 ();
extern "C" void StackTrace__ctor_m7606 ();
extern "C" void StackTrace__ctor_m7607 ();
extern "C" void StackTrace__ctor_m7608 ();
extern "C" void StackTrace_init_frames_m7609 ();
extern "C" void StackTrace_get_trace_m7610 ();
extern "C" void StackTrace_get_FrameCount_m7611 ();
extern "C" void StackTrace_GetFrame_m7612 ();
extern "C" void StackTrace_ToString_m7613 ();
extern "C" void Calendar__ctor_m7614 ();
extern "C" void Calendar_Clone_m7615 ();
extern "C" void Calendar_CheckReadOnly_m7616 ();
extern "C" void Calendar_get_EraNames_m7617 ();
extern "C" void CCMath_div_m7618 ();
extern "C" void CCMath_mod_m7619 ();
extern "C" void CCMath_div_mod_m7620 ();
extern "C" void CCFixed_FromDateTime_m7621 ();
extern "C" void CCFixed_day_of_week_m7622 ();
extern "C" void CCGregorianCalendar_is_leap_year_m7623 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m7624 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m7625 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m7626 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m7627 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m7628 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m7629 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m7630 ();
extern "C" void CCGregorianCalendar_GetMonth_m7631 ();
extern "C" void CCGregorianCalendar_GetYear_m7632 ();
extern "C" void CompareInfo__ctor_m7633 ();
extern "C" void CompareInfo__ctor_m7634 ();
extern "C" void CompareInfo__cctor_m7635 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7636 ();
extern "C" void CompareInfo_get_UseManagedCollation_m7637 ();
extern "C" void CompareInfo_construct_compareinfo_m7638 ();
extern "C" void CompareInfo_free_internal_collator_m7639 ();
extern "C" void CompareInfo_internal_compare_m7640 ();
extern "C" void CompareInfo_assign_sortkey_m7641 ();
extern "C" void CompareInfo_internal_index_m7642 ();
extern "C" void CompareInfo_Finalize_m7643 ();
extern "C" void CompareInfo_internal_compare_managed_m7644 ();
extern "C" void CompareInfo_internal_compare_switch_m7645 ();
extern "C" void CompareInfo_Compare_m7646 ();
extern "C" void CompareInfo_Compare_m7647 ();
extern "C" void CompareInfo_Compare_m7648 ();
extern "C" void CompareInfo_Equals_m7649 ();
extern "C" void CompareInfo_GetHashCode_m7650 ();
extern "C" void CompareInfo_GetSortKey_m7651 ();
extern "C" void CompareInfo_IndexOf_m7652 ();
extern "C" void CompareInfo_internal_index_managed_m7653 ();
extern "C" void CompareInfo_internal_index_switch_m7654 ();
extern "C" void CompareInfo_IndexOf_m7655 ();
extern "C" void CompareInfo_IsPrefix_m7656 ();
extern "C" void CompareInfo_IsSuffix_m7657 ();
extern "C" void CompareInfo_LastIndexOf_m7658 ();
extern "C" void CompareInfo_LastIndexOf_m7659 ();
extern "C" void CompareInfo_ToString_m7660 ();
extern "C" void CompareInfo_get_LCID_m7661 ();
extern "C" void CultureInfo__ctor_m7662 ();
extern "C" void CultureInfo__ctor_m7663 ();
extern "C" void CultureInfo__ctor_m7664 ();
extern "C" void CultureInfo__ctor_m7665 ();
extern "C" void CultureInfo__ctor_m7666 ();
extern "C" void CultureInfo__cctor_m7667 ();
extern "C" void CultureInfo_get_InvariantCulture_m4846 ();
extern "C" void CultureInfo_get_CurrentCulture_m5869 ();
extern "C" void CultureInfo_get_CurrentUICulture_m5870 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m7668 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m7669 ();
extern "C" void CultureInfo_get_LCID_m7670 ();
extern "C" void CultureInfo_get_Name_m7671 ();
extern "C" void CultureInfo_get_Parent_m7672 ();
extern "C" void CultureInfo_get_TextInfo_m7673 ();
extern "C" void CultureInfo_get_IcuName_m7674 ();
extern "C" void CultureInfo_Clone_m7675 ();
extern "C" void CultureInfo_Equals_m7676 ();
extern "C" void CultureInfo_GetHashCode_m7677 ();
extern "C" void CultureInfo_ToString_m7678 ();
extern "C" void CultureInfo_get_CompareInfo_m7679 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m7680 ();
extern "C" void CultureInfo_CheckNeutral_m7681 ();
extern "C" void CultureInfo_get_NumberFormat_m7682 ();
extern "C" void CultureInfo_set_NumberFormat_m7683 ();
extern "C" void CultureInfo_get_DateTimeFormat_m7684 ();
extern "C" void CultureInfo_set_DateTimeFormat_m7685 ();
extern "C" void CultureInfo_get_IsReadOnly_m7686 ();
extern "C" void CultureInfo_GetFormat_m7687 ();
extern "C" void CultureInfo_Construct_m7688 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m7689 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m7690 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7691 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m7692 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m7693 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m7694 ();
extern "C" void CultureInfo_construct_datetime_format_m7695 ();
extern "C" void CultureInfo_construct_number_format_m7696 ();
extern "C" void CultureInfo_ConstructInvariant_m7697 ();
extern "C" void CultureInfo_CreateTextInfo_m7698 ();
extern "C" void CultureInfo_CreateCulture_m7699 ();
extern "C" void DateTimeFormatInfo__ctor_m7700 ();
extern "C" void DateTimeFormatInfo__ctor_m7701 ();
extern "C" void DateTimeFormatInfo__cctor_m7702 ();
extern "C" void DateTimeFormatInfo_GetInstance_m7703 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m7704 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m7705 ();
extern "C" void DateTimeFormatInfo_Clone_m7706 ();
extern "C" void DateTimeFormatInfo_GetFormat_m7707 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m7708 ();
extern "C" void DateTimeFormatInfo_GetEraName_m7709 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m7710 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7711 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7712 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m7713 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m7714 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m7715 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m7716 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m7717 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m7718 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m7719 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m7720 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m7721 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m7722 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m7723 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m7724 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m7725 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m7726 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m7727 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m7728 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m7729 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m7730 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m7731 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m7732 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7733 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7734 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m7735 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m7736 ();
extern "C" void DateTimeFormatInfo_GetDayName_m7737 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m7738 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m7739 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m7740 ();
extern "C" void DaylightTime__ctor_m7741 ();
extern "C" void DaylightTime_get_Start_m7742 ();
extern "C" void DaylightTime_get_End_m7743 ();
extern "C" void DaylightTime_get_Delta_m7744 ();
extern "C" void GregorianCalendar__ctor_m7745 ();
extern "C" void GregorianCalendar__ctor_m7746 ();
extern "C" void GregorianCalendar_get_Eras_m7747 ();
extern "C" void GregorianCalendar_set_CalendarType_m7748 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m7749 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m7750 ();
extern "C" void GregorianCalendar_GetEra_m7751 ();
extern "C" void GregorianCalendar_GetMonth_m7752 ();
extern "C" void GregorianCalendar_GetYear_m7753 ();
extern "C" void NumberFormatInfo__ctor_m7754 ();
extern "C" void NumberFormatInfo__ctor_m7755 ();
extern "C" void NumberFormatInfo__ctor_m7756 ();
extern "C" void NumberFormatInfo__cctor_m7757 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m7758 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m7759 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m7760 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m7761 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m7762 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m7763 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m7764 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m7765 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m7766 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m7767 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m7768 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m7769 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m7770 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m7771 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m7772 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m7773 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m7774 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m7775 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m7776 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m7777 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m7778 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m7779 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m7780 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m7781 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m7782 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m7783 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m7784 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m7785 ();
extern "C" void NumberFormatInfo_GetFormat_m7786 ();
extern "C" void NumberFormatInfo_Clone_m7787 ();
extern "C" void NumberFormatInfo_GetInstance_m7788 ();
extern "C" void TextInfo__ctor_m7789 ();
extern "C" void TextInfo__ctor_m7790 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7791 ();
extern "C" void TextInfo_get_ListSeparator_m7792 ();
extern "C" void TextInfo_get_CultureName_m7793 ();
extern "C" void TextInfo_Equals_m7794 ();
extern "C" void TextInfo_GetHashCode_m7795 ();
extern "C" void TextInfo_ToString_m7796 ();
extern "C" void TextInfo_ToLower_m7797 ();
extern "C" void TextInfo_ToUpper_m7798 ();
extern "C" void TextInfo_ToLower_m7799 ();
extern "C" void TextInfo_Clone_m7800 ();
extern "C" void IsolatedStorageException__ctor_m7801 ();
extern "C" void IsolatedStorageException__ctor_m7802 ();
extern "C" void IsolatedStorageException__ctor_m7803 ();
extern "C" void BinaryReader__ctor_m7804 ();
extern "C" void BinaryReader__ctor_m7805 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m7806 ();
extern "C" void BinaryReader_get_BaseStream_m7807 ();
extern "C" void BinaryReader_Close_m7808 ();
extern "C" void BinaryReader_Dispose_m7809 ();
extern "C" void BinaryReader_FillBuffer_m7810 ();
extern "C" void BinaryReader_Read_m7811 ();
extern "C" void BinaryReader_Read_m7812 ();
extern "C" void BinaryReader_Read_m7813 ();
extern "C" void BinaryReader_ReadCharBytes_m7814 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m7815 ();
extern "C" void BinaryReader_ReadBoolean_m7816 ();
extern "C" void BinaryReader_ReadByte_m7817 ();
extern "C" void BinaryReader_ReadBytes_m7818 ();
extern "C" void BinaryReader_ReadChar_m7819 ();
extern "C" void BinaryReader_ReadDecimal_m7820 ();
extern "C" void BinaryReader_ReadDouble_m7821 ();
extern "C" void BinaryReader_ReadInt16_m7822 ();
extern "C" void BinaryReader_ReadInt32_m7823 ();
extern "C" void BinaryReader_ReadInt64_m7824 ();
extern "C" void BinaryReader_ReadSByte_m7825 ();
extern "C" void BinaryReader_ReadString_m7826 ();
extern "C" void BinaryReader_ReadSingle_m7827 ();
extern "C" void BinaryReader_ReadUInt16_m7828 ();
extern "C" void BinaryReader_ReadUInt32_m7829 ();
extern "C" void BinaryReader_ReadUInt64_m7830 ();
extern "C" void BinaryReader_CheckBuffer_m7831 ();
extern "C" void Directory_CreateDirectory_m5855 ();
extern "C" void Directory_CreateDirectoriesInternal_m7832 ();
extern "C" void Directory_Exists_m5854 ();
extern "C" void Directory_GetCurrentDirectory_m7833 ();
extern "C" void Directory_GetFiles_m5857 ();
extern "C" void Directory_GetFileSystemEntries_m7834 ();
extern "C" void DirectoryInfo__ctor_m7835 ();
extern "C" void DirectoryInfo__ctor_m7836 ();
extern "C" void DirectoryInfo__ctor_m7837 ();
extern "C" void DirectoryInfo_Initialize_m7838 ();
extern "C" void DirectoryInfo_get_Exists_m7839 ();
extern "C" void DirectoryInfo_get_Parent_m7840 ();
extern "C" void DirectoryInfo_Create_m7841 ();
extern "C" void DirectoryInfo_ToString_m7842 ();
extern "C" void DirectoryNotFoundException__ctor_m7843 ();
extern "C" void DirectoryNotFoundException__ctor_m7844 ();
extern "C" void DirectoryNotFoundException__ctor_m7845 ();
extern "C" void EndOfStreamException__ctor_m7846 ();
extern "C" void EndOfStreamException__ctor_m7847 ();
extern "C" void File_Delete_m7848 ();
extern "C" void File_Exists_m7849 ();
extern "C" void File_Open_m7850 ();
extern "C" void File_OpenRead_m5853 ();
extern "C" void File_OpenText_m7851 ();
extern "C" void FileNotFoundException__ctor_m7852 ();
extern "C" void FileNotFoundException__ctor_m7853 ();
extern "C" void FileNotFoundException__ctor_m7854 ();
extern "C" void FileNotFoundException_get_Message_m7855 ();
extern "C" void FileNotFoundException_GetObjectData_m7856 ();
extern "C" void FileNotFoundException_ToString_m7857 ();
extern "C" void ReadDelegate__ctor_m7858 ();
extern "C" void ReadDelegate_Invoke_m7859 ();
extern "C" void ReadDelegate_BeginInvoke_m7860 ();
extern "C" void ReadDelegate_EndInvoke_m7861 ();
extern "C" void WriteDelegate__ctor_m7862 ();
extern "C" void WriteDelegate_Invoke_m7863 ();
extern "C" void WriteDelegate_BeginInvoke_m7864 ();
extern "C" void WriteDelegate_EndInvoke_m7865 ();
extern "C" void FileStream__ctor_m7866 ();
extern "C" void FileStream__ctor_m7867 ();
extern "C" void FileStream__ctor_m7868 ();
extern "C" void FileStream__ctor_m7869 ();
extern "C" void FileStream__ctor_m7870 ();
extern "C" void FileStream_get_CanRead_m7871 ();
extern "C" void FileStream_get_CanWrite_m7872 ();
extern "C" void FileStream_get_CanSeek_m7873 ();
extern "C" void FileStream_get_Length_m7874 ();
extern "C" void FileStream_get_Position_m7875 ();
extern "C" void FileStream_set_Position_m7876 ();
extern "C" void FileStream_ReadByte_m7877 ();
extern "C" void FileStream_WriteByte_m7878 ();
extern "C" void FileStream_Read_m7879 ();
extern "C" void FileStream_ReadInternal_m7880 ();
extern "C" void FileStream_BeginRead_m7881 ();
extern "C" void FileStream_EndRead_m7882 ();
extern "C" void FileStream_Write_m7883 ();
extern "C" void FileStream_WriteInternal_m7884 ();
extern "C" void FileStream_BeginWrite_m7885 ();
extern "C" void FileStream_EndWrite_m7886 ();
extern "C" void FileStream_Seek_m7887 ();
extern "C" void FileStream_SetLength_m7888 ();
extern "C" void FileStream_Flush_m7889 ();
extern "C" void FileStream_Finalize_m7890 ();
extern "C" void FileStream_Dispose_m7891 ();
extern "C" void FileStream_ReadSegment_m7892 ();
extern "C" void FileStream_WriteSegment_m7893 ();
extern "C" void FileStream_FlushBuffer_m7894 ();
extern "C" void FileStream_FlushBuffer_m7895 ();
extern "C" void FileStream_FlushBufferIfDirty_m7896 ();
extern "C" void FileStream_RefillBuffer_m7897 ();
extern "C" void FileStream_ReadData_m7898 ();
extern "C" void FileStream_InitBuffer_m7899 ();
extern "C" void FileStream_GetSecureFileName_m7900 ();
extern "C" void FileStream_GetSecureFileName_m7901 ();
extern "C" void FileStreamAsyncResult__ctor_m7902 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m7903 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m7904 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m7905 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m7906 ();
extern "C" void FileSystemInfo__ctor_m7907 ();
extern "C" void FileSystemInfo__ctor_m7908 ();
extern "C" void FileSystemInfo_GetObjectData_m7909 ();
extern "C" void FileSystemInfo_get_FullName_m7910 ();
extern "C" void FileSystemInfo_Refresh_m7911 ();
extern "C" void FileSystemInfo_InternalRefresh_m7912 ();
extern "C" void FileSystemInfo_CheckPath_m7913 ();
extern "C" void IOException__ctor_m7914 ();
extern "C" void IOException__ctor_m7915 ();
extern "C" void IOException__ctor_m5883 ();
extern "C" void IOException__ctor_m7916 ();
extern "C" void IOException__ctor_m7917 ();
extern "C" void MemoryStream__ctor_m5884 ();
extern "C" void MemoryStream__ctor_m5889 ();
extern "C" void MemoryStream__ctor_m5890 ();
extern "C" void MemoryStream_InternalConstructor_m7918 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m7919 ();
extern "C" void MemoryStream_get_CanRead_m7920 ();
extern "C" void MemoryStream_get_CanSeek_m7921 ();
extern "C" void MemoryStream_get_CanWrite_m7922 ();
extern "C" void MemoryStream_set_Capacity_m7923 ();
extern "C" void MemoryStream_get_Length_m7924 ();
extern "C" void MemoryStream_get_Position_m7925 ();
extern "C" void MemoryStream_set_Position_m7926 ();
extern "C" void MemoryStream_Dispose_m7927 ();
extern "C" void MemoryStream_Flush_m7928 ();
extern "C" void MemoryStream_Read_m7929 ();
extern "C" void MemoryStream_ReadByte_m7930 ();
extern "C" void MemoryStream_Seek_m7931 ();
extern "C" void MemoryStream_CalculateNewCapacity_m7932 ();
extern "C" void MemoryStream_Expand_m7933 ();
extern "C" void MemoryStream_SetLength_m7934 ();
extern "C" void MemoryStream_ToArray_m7935 ();
extern "C" void MemoryStream_Write_m7936 ();
extern "C" void MemoryStream_WriteByte_m7937 ();
extern "C" void MonoIO__cctor_m7938 ();
extern "C" void MonoIO_GetException_m7939 ();
extern "C" void MonoIO_GetException_m7940 ();
extern "C" void MonoIO_CreateDirectory_m7941 ();
extern "C" void MonoIO_GetFileSystemEntries_m7942 ();
extern "C" void MonoIO_GetCurrentDirectory_m7943 ();
extern "C" void MonoIO_DeleteFile_m7944 ();
extern "C" void MonoIO_GetFileAttributes_m7945 ();
extern "C" void MonoIO_GetFileType_m7946 ();
extern "C" void MonoIO_ExistsFile_m7947 ();
extern "C" void MonoIO_ExistsDirectory_m7948 ();
extern "C" void MonoIO_GetFileStat_m7949 ();
extern "C" void MonoIO_Open_m7950 ();
extern "C" void MonoIO_Close_m7951 ();
extern "C" void MonoIO_Read_m7952 ();
extern "C" void MonoIO_Write_m7953 ();
extern "C" void MonoIO_Seek_m7954 ();
extern "C" void MonoIO_GetLength_m7955 ();
extern "C" void MonoIO_SetLength_m7956 ();
extern "C" void MonoIO_get_ConsoleOutput_m7957 ();
extern "C" void MonoIO_get_ConsoleInput_m7958 ();
extern "C" void MonoIO_get_ConsoleError_m7959 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m7960 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m7961 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m7962 ();
extern "C" void MonoIO_get_PathSeparator_m7963 ();
extern "C" void Path__cctor_m7964 ();
extern "C" void Path_Combine_m5856 ();
extern "C" void Path_CleanPath_m7965 ();
extern "C" void Path_GetDirectoryName_m7966 ();
extern "C" void Path_GetFileName_m7967 ();
extern "C" void Path_GetFullPath_m7968 ();
extern "C" void Path_WindowsDriveAdjustment_m7969 ();
extern "C" void Path_InsecureGetFullPath_m7970 ();
extern "C" void Path_IsDsc_m7971 ();
extern "C" void Path_GetPathRoot_m7972 ();
extern "C" void Path_IsPathRooted_m7973 ();
extern "C" void Path_GetInvalidPathChars_m7974 ();
extern "C" void Path_GetServerAndShare_m7975 ();
extern "C" void Path_SameRoot_m7976 ();
extern "C" void Path_CanonicalizePath_m7977 ();
extern "C" void PathTooLongException__ctor_m7978 ();
extern "C" void PathTooLongException__ctor_m7979 ();
extern "C" void PathTooLongException__ctor_m7980 ();
extern "C" void SearchPattern__cctor_m7981 ();
extern "C" void Stream__ctor_m5885 ();
extern "C" void Stream__cctor_m7982 ();
extern "C" void Stream_Dispose_m7983 ();
extern "C" void Stream_Dispose_m5888 ();
extern "C" void Stream_Close_m5887 ();
extern "C" void Stream_ReadByte_m7984 ();
extern "C" void Stream_WriteByte_m7985 ();
extern "C" void Stream_BeginRead_m7986 ();
extern "C" void Stream_BeginWrite_m7987 ();
extern "C" void Stream_EndRead_m7988 ();
extern "C" void Stream_EndWrite_m7989 ();
extern "C" void NullStream__ctor_m7990 ();
extern "C" void NullStream_get_CanRead_m7991 ();
extern "C" void NullStream_get_CanSeek_m7992 ();
extern "C" void NullStream_get_CanWrite_m7993 ();
extern "C" void NullStream_get_Length_m7994 ();
extern "C" void NullStream_get_Position_m7995 ();
extern "C" void NullStream_set_Position_m7996 ();
extern "C" void NullStream_Flush_m7997 ();
extern "C" void NullStream_Read_m7998 ();
extern "C" void NullStream_ReadByte_m7999 ();
extern "C" void NullStream_Seek_m8000 ();
extern "C" void NullStream_SetLength_m8001 ();
extern "C" void NullStream_Write_m8002 ();
extern "C" void NullStream_WriteByte_m8003 ();
extern "C" void StreamAsyncResult__ctor_m8004 ();
extern "C" void StreamAsyncResult_SetComplete_m8005 ();
extern "C" void StreamAsyncResult_SetComplete_m8006 ();
extern "C" void StreamAsyncResult_get_AsyncState_m8007 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m8008 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m8009 ();
extern "C" void StreamAsyncResult_get_Exception_m8010 ();
extern "C" void StreamAsyncResult_get_NBytes_m8011 ();
extern "C" void StreamAsyncResult_get_Done_m8012 ();
extern "C" void StreamAsyncResult_set_Done_m8013 ();
extern "C" void NullStreamReader__ctor_m8014 ();
extern "C" void NullStreamReader_Peek_m8015 ();
extern "C" void NullStreamReader_Read_m8016 ();
extern "C" void NullStreamReader_Read_m8017 ();
extern "C" void NullStreamReader_ReadLine_m8018 ();
extern "C" void NullStreamReader_ReadToEnd_m8019 ();
extern "C" void StreamReader__ctor_m8020 ();
extern "C" void StreamReader__ctor_m8021 ();
extern "C" void StreamReader__ctor_m8022 ();
extern "C" void StreamReader__ctor_m8023 ();
extern "C" void StreamReader__ctor_m8024 ();
extern "C" void StreamReader__cctor_m8025 ();
extern "C" void StreamReader_Initialize_m8026 ();
extern "C" void StreamReader_Dispose_m8027 ();
extern "C" void StreamReader_DoChecks_m8028 ();
extern "C" void StreamReader_ReadBuffer_m8029 ();
extern "C" void StreamReader_Peek_m8030 ();
extern "C" void StreamReader_Read_m8031 ();
extern "C" void StreamReader_Read_m8032 ();
extern "C" void StreamReader_FindNextEOL_m8033 ();
extern "C" void StreamReader_ReadLine_m8034 ();
extern "C" void StreamReader_ReadToEnd_m8035 ();
extern "C" void StreamWriter__ctor_m8036 ();
extern "C" void StreamWriter__ctor_m8037 ();
extern "C" void StreamWriter__cctor_m8038 ();
extern "C" void StreamWriter_Initialize_m8039 ();
extern "C" void StreamWriter_set_AutoFlush_m8040 ();
extern "C" void StreamWriter_Dispose_m8041 ();
extern "C" void StreamWriter_Flush_m8042 ();
extern "C" void StreamWriter_FlushBytes_m8043 ();
extern "C" void StreamWriter_Decode_m8044 ();
extern "C" void StreamWriter_Write_m8045 ();
extern "C" void StreamWriter_LowLevelWrite_m8046 ();
extern "C" void StreamWriter_LowLevelWrite_m8047 ();
extern "C" void StreamWriter_Write_m8048 ();
extern "C" void StreamWriter_Write_m8049 ();
extern "C" void StreamWriter_Write_m8050 ();
extern "C" void StreamWriter_Close_m8051 ();
extern "C" void StreamWriter_Finalize_m8052 ();
extern "C" void StringReader__ctor_m8053 ();
extern "C" void StringReader_Dispose_m8054 ();
extern "C" void StringReader_Peek_m8055 ();
extern "C" void StringReader_Read_m8056 ();
extern "C" void StringReader_Read_m8057 ();
extern "C" void StringReader_ReadLine_m8058 ();
extern "C" void StringReader_ReadToEnd_m8059 ();
extern "C" void StringReader_CheckObjectDisposedException_m8060 ();
extern "C" void NullTextReader__ctor_m8061 ();
extern "C" void NullTextReader_ReadLine_m8062 ();
extern "C" void TextReader__ctor_m8063 ();
extern "C" void TextReader__cctor_m8064 ();
extern "C" void TextReader_Dispose_m8065 ();
extern "C" void TextReader_Dispose_m8066 ();
extern "C" void TextReader_Peek_m8067 ();
extern "C" void TextReader_Read_m8068 ();
extern "C" void TextReader_Read_m8069 ();
extern "C" void TextReader_ReadLine_m8070 ();
extern "C" void TextReader_ReadToEnd_m8071 ();
extern "C" void TextReader_Synchronized_m8072 ();
extern "C" void SynchronizedReader__ctor_m8073 ();
extern "C" void SynchronizedReader_Peek_m8074 ();
extern "C" void SynchronizedReader_ReadLine_m8075 ();
extern "C" void SynchronizedReader_ReadToEnd_m8076 ();
extern "C" void SynchronizedReader_Read_m8077 ();
extern "C" void SynchronizedReader_Read_m8078 ();
extern "C" void NullTextWriter__ctor_m8079 ();
extern "C" void NullTextWriter_Write_m8080 ();
extern "C" void NullTextWriter_Write_m8081 ();
extern "C" void NullTextWriter_Write_m8082 ();
extern "C" void TextWriter__ctor_m8083 ();
extern "C" void TextWriter__cctor_m8084 ();
extern "C" void TextWriter_Close_m8085 ();
extern "C" void TextWriter_Dispose_m8086 ();
extern "C" void TextWriter_Dispose_m8087 ();
extern "C" void TextWriter_Flush_m8088 ();
extern "C" void TextWriter_Synchronized_m8089 ();
extern "C" void TextWriter_Write_m8090 ();
extern "C" void TextWriter_Write_m8091 ();
extern "C" void TextWriter_Write_m8092 ();
extern "C" void TextWriter_Write_m8093 ();
extern "C" void TextWriter_WriteLine_m8094 ();
extern "C" void TextWriter_WriteLine_m8095 ();
extern "C" void SynchronizedWriter__ctor_m8096 ();
extern "C" void SynchronizedWriter_Close_m8097 ();
extern "C" void SynchronizedWriter_Flush_m8098 ();
extern "C" void SynchronizedWriter_Write_m8099 ();
extern "C" void SynchronizedWriter_Write_m8100 ();
extern "C" void SynchronizedWriter_Write_m8101 ();
extern "C" void SynchronizedWriter_Write_m8102 ();
extern "C" void SynchronizedWriter_WriteLine_m8103 ();
extern "C" void SynchronizedWriter_WriteLine_m8104 ();
extern "C" void UnexceptionalStreamReader__ctor_m8105 ();
extern "C" void UnexceptionalStreamReader__cctor_m8106 ();
extern "C" void UnexceptionalStreamReader_Peek_m8107 ();
extern "C" void UnexceptionalStreamReader_Read_m8108 ();
extern "C" void UnexceptionalStreamReader_Read_m8109 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m8110 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m8111 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m8112 ();
extern "C" void UnexceptionalStreamWriter__ctor_m8113 ();
extern "C" void UnexceptionalStreamWriter_Flush_m8114 ();
extern "C" void UnexceptionalStreamWriter_Write_m8115 ();
extern "C" void UnexceptionalStreamWriter_Write_m8116 ();
extern "C" void UnexceptionalStreamWriter_Write_m8117 ();
extern "C" void UnexceptionalStreamWriter_Write_m8118 ();
extern "C" void UnmanagedMemoryStream_get_CanRead_m8119 ();
extern "C" void UnmanagedMemoryStream_get_CanSeek_m8120 ();
extern "C" void UnmanagedMemoryStream_get_CanWrite_m8121 ();
extern "C" void UnmanagedMemoryStream_get_Length_m8122 ();
extern "C" void UnmanagedMemoryStream_get_Position_m8123 ();
extern "C" void UnmanagedMemoryStream_set_Position_m8124 ();
extern "C" void UnmanagedMemoryStream_Read_m8125 ();
extern "C" void UnmanagedMemoryStream_ReadByte_m8126 ();
extern "C" void UnmanagedMemoryStream_Seek_m8127 ();
extern "C" void UnmanagedMemoryStream_SetLength_m8128 ();
extern "C" void UnmanagedMemoryStream_Flush_m8129 ();
extern "C" void UnmanagedMemoryStream_Dispose_m8130 ();
extern "C" void UnmanagedMemoryStream_Write_m8131 ();
extern "C" void UnmanagedMemoryStream_WriteByte_m8132 ();
extern "C" void AssemblyBuilder_get_Location_m8133 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m8134 ();
extern "C" void AssemblyBuilder_GetTypes_m8135 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m8136 ();
extern "C" void AssemblyBuilder_not_supported_m8137 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m8138 ();
extern "C" void ConstructorBuilder__ctor_m8139 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m8140 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m8141 ();
extern "C" void ConstructorBuilder_GetParameters_m8142 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m8143 ();
extern "C" void ConstructorBuilder_GetParameterCount_m8144 ();
extern "C" void ConstructorBuilder_Invoke_m8145 ();
extern "C" void ConstructorBuilder_Invoke_m8146 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m8147 ();
extern "C" void ConstructorBuilder_get_Attributes_m8148 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m8149 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m8150 ();
extern "C" void ConstructorBuilder_get_Name_m8151 ();
extern "C" void ConstructorBuilder_IsDefined_m8152 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m8153 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m8154 ();
extern "C" void ConstructorBuilder_GetILGenerator_m8155 ();
extern "C" void ConstructorBuilder_GetILGenerator_m8156 ();
extern "C" void ConstructorBuilder_GetToken_m8157 ();
extern "C" void ConstructorBuilder_get_Module_m8158 ();
extern "C" void ConstructorBuilder_ToString_m8159 ();
extern "C" void ConstructorBuilder_fixup_m8160 ();
extern "C" void ConstructorBuilder_get_next_table_index_m8161 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m8162 ();
extern "C" void ConstructorBuilder_not_supported_m8163 ();
extern "C" void ConstructorBuilder_not_created_m8164 ();
extern "C" void EnumBuilder_get_Assembly_m8165 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m8166 ();
extern "C" void EnumBuilder_get_BaseType_m8167 ();
extern "C" void EnumBuilder_get_DeclaringType_m8168 ();
extern "C" void EnumBuilder_get_FullName_m8169 ();
extern "C" void EnumBuilder_get_Module_m8170 ();
extern "C" void EnumBuilder_get_Name_m8171 ();
extern "C" void EnumBuilder_get_Namespace_m8172 ();
extern "C" void EnumBuilder_get_ReflectedType_m8173 ();
extern "C" void EnumBuilder_get_TypeHandle_m8174 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m8175 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m8176 ();
extern "C" void EnumBuilder_GetConstructorImpl_m8177 ();
extern "C" void EnumBuilder_GetConstructors_m8178 ();
extern "C" void EnumBuilder_GetCustomAttributes_m8179 ();
extern "C" void EnumBuilder_GetCustomAttributes_m8180 ();
extern "C" void EnumBuilder_GetElementType_m8181 ();
extern "C" void EnumBuilder_GetEvent_m8182 ();
extern "C" void EnumBuilder_GetField_m8183 ();
extern "C" void EnumBuilder_GetFields_m8184 ();
extern "C" void EnumBuilder_GetInterfaces_m8185 ();
extern "C" void EnumBuilder_GetMethodImpl_m8186 ();
extern "C" void EnumBuilder_GetMethods_m8187 ();
extern "C" void EnumBuilder_GetPropertyImpl_m8188 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m8189 ();
extern "C" void EnumBuilder_InvokeMember_m8190 ();
extern "C" void EnumBuilder_IsArrayImpl_m8191 ();
extern "C" void EnumBuilder_IsByRefImpl_m8192 ();
extern "C" void EnumBuilder_IsPointerImpl_m8193 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m8194 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m8195 ();
extern "C" void EnumBuilder_IsDefined_m8196 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m8197 ();
extern "C" void FieldBuilder_get_Attributes_m8198 ();
extern "C" void FieldBuilder_get_DeclaringType_m8199 ();
extern "C" void FieldBuilder_get_FieldHandle_m8200 ();
extern "C" void FieldBuilder_get_FieldType_m8201 ();
extern "C" void FieldBuilder_get_Name_m8202 ();
extern "C" void FieldBuilder_get_ReflectedType_m8203 ();
extern "C" void FieldBuilder_GetCustomAttributes_m8204 ();
extern "C" void FieldBuilder_GetCustomAttributes_m8205 ();
extern "C" void FieldBuilder_GetValue_m8206 ();
extern "C" void FieldBuilder_IsDefined_m8207 ();
extern "C" void FieldBuilder_GetFieldOffset_m8208 ();
extern "C" void FieldBuilder_SetValue_m8209 ();
extern "C" void FieldBuilder_get_UMarshal_m8210 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m8211 ();
extern "C" void FieldBuilder_get_Module_m8212 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m8213 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m8214 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m8215 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m8216 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m8217 ();
extern "C" void GenericTypeParameterBuilder_GetField_m8218 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m8219 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m8220 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m8221 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m8222 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m8223 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m8224 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m8225 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m8226 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m8227 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m8228 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m8229 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m8230 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m8231 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m8232 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m8233 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m8234 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m8235 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m8236 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m8237 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m8238 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m8239 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m8240 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m8241 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m8242 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m8243 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m8244 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m8245 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m8246 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m8247 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m8248 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m8249 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m8250 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m8251 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m8252 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m8253 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m8254 ();
extern "C" void GenericTypeParameterBuilder_ToString_m8255 ();
extern "C" void GenericTypeParameterBuilder_Equals_m8256 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m8257 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m8258 ();
extern "C" void ILGenerator__ctor_m8259 ();
extern "C" void ILGenerator__cctor_m8260 ();
extern "C" void ILGenerator_add_token_fixup_m8261 ();
extern "C" void ILGenerator_make_room_m8262 ();
extern "C" void ILGenerator_emit_int_m8263 ();
extern "C" void ILGenerator_ll_emit_m8264 ();
extern "C" void ILGenerator_Emit_m8265 ();
extern "C" void ILGenerator_Emit_m8266 ();
extern "C" void ILGenerator_label_fixup_m8267 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m8268 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m8269 ();
extern "C" void MethodBuilder_get_MethodHandle_m8270 ();
extern "C" void MethodBuilder_get_ReturnType_m8271 ();
extern "C" void MethodBuilder_get_ReflectedType_m8272 ();
extern "C" void MethodBuilder_get_DeclaringType_m8273 ();
extern "C" void MethodBuilder_get_Name_m8274 ();
extern "C" void MethodBuilder_get_Attributes_m8275 ();
extern "C" void MethodBuilder_get_CallingConvention_m8276 ();
extern "C" void MethodBuilder_GetBaseDefinition_m8277 ();
extern "C" void MethodBuilder_GetParameters_m8278 ();
extern "C" void MethodBuilder_GetParameterCount_m8279 ();
extern "C" void MethodBuilder_Invoke_m8280 ();
extern "C" void MethodBuilder_IsDefined_m8281 ();
extern "C" void MethodBuilder_GetCustomAttributes_m8282 ();
extern "C" void MethodBuilder_GetCustomAttributes_m8283 ();
extern "C" void MethodBuilder_check_override_m8284 ();
extern "C" void MethodBuilder_fixup_m8285 ();
extern "C" void MethodBuilder_ToString_m8286 ();
extern "C" void MethodBuilder_Equals_m8287 ();
extern "C" void MethodBuilder_GetHashCode_m8288 ();
extern "C" void MethodBuilder_get_next_table_index_m8289 ();
extern "C" void MethodBuilder_NotSupported_m8290 ();
extern "C" void MethodBuilder_MakeGenericMethod_m8291 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m8292 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m8293 ();
extern "C" void MethodBuilder_GetGenericArguments_m8294 ();
extern "C" void MethodBuilder_get_Module_m8295 ();
extern "C" void MethodToken__ctor_m8296 ();
extern "C" void MethodToken__cctor_m8297 ();
extern "C" void MethodToken_Equals_m8298 ();
extern "C" void MethodToken_GetHashCode_m8299 ();
extern "C" void MethodToken_get_Token_m8300 ();
extern "C" void ModuleBuilder__cctor_m8301 ();
extern "C" void ModuleBuilder_get_next_table_index_m8302 ();
extern "C" void ModuleBuilder_GetTypes_m8303 ();
extern "C" void ModuleBuilder_getToken_m8304 ();
extern "C" void ModuleBuilder_GetToken_m8305 ();
extern "C" void ModuleBuilder_RegisterToken_m8306 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m8307 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m8308 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m8309 ();
extern "C" void OpCode__ctor_m8310 ();
extern "C" void OpCode_GetHashCode_m8311 ();
extern "C" void OpCode_Equals_m8312 ();
extern "C" void OpCode_ToString_m8313 ();
extern "C" void OpCode_get_Name_m8314 ();
extern "C" void OpCode_get_Size_m8315 ();
extern "C" void OpCode_get_StackBehaviourPop_m8316 ();
extern "C" void OpCode_get_StackBehaviourPush_m8317 ();
extern "C" void OpCodeNames__cctor_m8318 ();
extern "C" void OpCodes__cctor_m8319 ();
extern "C" void ParameterBuilder_get_Attributes_m8320 ();
extern "C" void ParameterBuilder_get_Name_m8321 ();
extern "C" void ParameterBuilder_get_Position_m8322 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m8323 ();
extern "C" void TypeBuilder_setup_internal_class_m8324 ();
extern "C" void TypeBuilder_create_generic_class_m8325 ();
extern "C" void TypeBuilder_get_Assembly_m8326 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m8327 ();
extern "C" void TypeBuilder_get_BaseType_m8328 ();
extern "C" void TypeBuilder_get_DeclaringType_m8329 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m8330 ();
extern "C" void TypeBuilder_get_FullName_m8331 ();
extern "C" void TypeBuilder_get_Module_m8332 ();
extern "C" void TypeBuilder_get_Name_m8333 ();
extern "C" void TypeBuilder_get_Namespace_m8334 ();
extern "C" void TypeBuilder_get_ReflectedType_m8335 ();
extern "C" void TypeBuilder_GetConstructorImpl_m8336 ();
extern "C" void TypeBuilder_IsDefined_m8337 ();
extern "C" void TypeBuilder_GetCustomAttributes_m8338 ();
extern "C" void TypeBuilder_GetCustomAttributes_m8339 ();
extern "C" void TypeBuilder_DefineConstructor_m8340 ();
extern "C" void TypeBuilder_DefineConstructor_m8341 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m8342 ();
extern "C" void TypeBuilder_create_runtime_class_m8343 ();
extern "C" void TypeBuilder_is_nested_in_m8344 ();
extern "C" void TypeBuilder_has_ctor_method_m8345 ();
extern "C" void TypeBuilder_CreateType_m8346 ();
extern "C" void TypeBuilder_GetConstructors_m8347 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m8348 ();
extern "C" void TypeBuilder_GetElementType_m8349 ();
extern "C" void TypeBuilder_GetEvent_m8350 ();
extern "C" void TypeBuilder_GetField_m8351 ();
extern "C" void TypeBuilder_GetFields_m8352 ();
extern "C" void TypeBuilder_GetInterfaces_m8353 ();
extern "C" void TypeBuilder_GetMethodsByName_m8354 ();
extern "C" void TypeBuilder_GetMethods_m8355 ();
extern "C" void TypeBuilder_GetMethodImpl_m8356 ();
extern "C" void TypeBuilder_GetPropertyImpl_m8357 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m8358 ();
extern "C" void TypeBuilder_InvokeMember_m8359 ();
extern "C" void TypeBuilder_IsArrayImpl_m8360 ();
extern "C" void TypeBuilder_IsByRefImpl_m8361 ();
extern "C" void TypeBuilder_IsPointerImpl_m8362 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m8363 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m8364 ();
extern "C" void TypeBuilder_MakeGenericType_m8365 ();
extern "C" void TypeBuilder_get_TypeHandle_m8366 ();
extern "C" void TypeBuilder_SetParent_m8367 ();
extern "C" void TypeBuilder_get_next_table_index_m8368 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m8369 ();
extern "C" void TypeBuilder_get_is_created_m8370 ();
extern "C" void TypeBuilder_not_supported_m8371 ();
extern "C" void TypeBuilder_check_not_created_m8372 ();
extern "C" void TypeBuilder_check_created_m8373 ();
extern "C" void TypeBuilder_ToString_m8374 ();
extern "C" void TypeBuilder_IsAssignableFrom_m8375 ();
extern "C" void TypeBuilder_IsSubclassOf_m8376 ();
extern "C" void TypeBuilder_IsAssignableTo_m8377 ();
extern "C" void TypeBuilder_GetGenericArguments_m8378 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m8379 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m8380 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m8381 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m8382 ();
extern "C" void TypeBuilder_get_IsGenericType_m8383 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m8384 ();
extern "C" void AmbiguousMatchException__ctor_m8385 ();
extern "C" void AmbiguousMatchException__ctor_m8386 ();
extern "C" void AmbiguousMatchException__ctor_m8387 ();
extern "C" void ResolveEventHolder__ctor_m8388 ();
extern "C" void Assembly__ctor_m8389 ();
extern "C" void Assembly_get_code_base_m8390 ();
extern "C" void Assembly_get_fullname_m8391 ();
extern "C" void Assembly_get_location_m8392 ();
extern "C" void Assembly_GetCodeBase_m8393 ();
extern "C" void Assembly_get_FullName_m8394 ();
extern "C" void Assembly_get_Location_m8395 ();
extern "C" void Assembly_IsDefined_m8396 ();
extern "C" void Assembly_GetCustomAttributes_m8397 ();
extern "C" void Assembly_GetManifestResourceInternal_m8398 ();
extern "C" void Assembly_GetTypes_m8399 ();
extern "C" void Assembly_GetTypes_m8400 ();
extern "C" void Assembly_GetType_m8401 ();
extern "C" void Assembly_GetType_m8402 ();
extern "C" void Assembly_InternalGetType_m8403 ();
extern "C" void Assembly_GetType_m8404 ();
extern "C" void Assembly_FillName_m8405 ();
extern "C" void Assembly_GetName_m8406 ();
extern "C" void Assembly_GetName_m8407 ();
extern "C" void Assembly_UnprotectedGetName_m8408 ();
extern "C" void Assembly_ToString_m8409 ();
extern "C" void Assembly_Load_m8410 ();
extern "C" void Assembly_GetModule_m8411 ();
extern "C" void Assembly_GetModulesInternal_m8412 ();
extern "C" void Assembly_GetModules_m8413 ();
extern "C" void Assembly_GetExecutingAssembly_m8414 ();
extern "C" void AssemblyCompanyAttribute__ctor_m8415 ();
extern "C" void AssemblyConfigurationAttribute__ctor_m8416 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m8417 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m8418 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m8419 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m8420 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m8421 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m8422 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m8423 ();
extern "C" void AssemblyKeyNameAttribute__ctor_m8424 ();
extern "C" void AssemblyName__ctor_m8425 ();
extern "C" void AssemblyName__ctor_m8426 ();
extern "C" void AssemblyName_get_Name_m8427 ();
extern "C" void AssemblyName_get_Flags_m8428 ();
extern "C" void AssemblyName_get_FullName_m8429 ();
extern "C" void AssemblyName_get_Version_m8430 ();
extern "C" void AssemblyName_set_Version_m8431 ();
extern "C" void AssemblyName_ToString_m8432 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m8433 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m8434 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m8435 ();
extern "C" void AssemblyName_SetPublicKey_m8436 ();
extern "C" void AssemblyName_SetPublicKeyToken_m8437 ();
extern "C" void AssemblyName_GetObjectData_m8438 ();
extern "C" void AssemblyName_Clone_m8439 ();
extern "C" void AssemblyName_OnDeserialization_m8440 ();
extern "C" void AssemblyProductAttribute__ctor_m8441 ();
extern "C" void AssemblyTitleAttribute__ctor_m8442 ();
extern "C" void AssemblyTrademarkAttribute__ctor_m8443 ();
extern "C" void Default__ctor_m8444 ();
extern "C" void Default_BindToMethod_m8445 ();
extern "C" void Default_ReorderParameters_m8446 ();
extern "C" void Default_IsArrayAssignable_m8447 ();
extern "C" void Default_ChangeType_m8448 ();
extern "C" void Default_ReorderArgumentArray_m8449 ();
extern "C" void Default_check_type_m8450 ();
extern "C" void Default_check_arguments_m8451 ();
extern "C" void Default_SelectMethod_m8452 ();
extern "C" void Default_SelectMethod_m8453 ();
extern "C" void Default_GetBetterMethod_m8454 ();
extern "C" void Default_CompareCloserType_m8455 ();
extern "C" void Default_SelectProperty_m8456 ();
extern "C" void Default_check_arguments_with_score_m8457 ();
extern "C" void Default_check_type_with_score_m8458 ();
extern "C" void Binder__ctor_m8459 ();
extern "C" void Binder__cctor_m8460 ();
extern "C" void Binder_get_DefaultBinder_m8461 ();
extern "C" void Binder_ConvertArgs_m8462 ();
extern "C" void Binder_GetDerivedLevel_m8463 ();
extern "C" void Binder_FindMostDerivedMatch_m8464 ();
extern "C" void ConstructorInfo__ctor_m8465 ();
extern "C" void ConstructorInfo__cctor_m8466 ();
extern "C" void ConstructorInfo_get_MemberType_m8467 ();
extern "C" void ConstructorInfo_Invoke_m2233 ();
extern "C" void CustomAttributeData__ctor_m8468 ();
extern "C" void CustomAttributeData_get_Constructor_m8469 ();
extern "C" void CustomAttributeData_get_ConstructorArguments_m8470 ();
extern "C" void CustomAttributeData_get_NamedArguments_m8471 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8472 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8473 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8474 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8475 ();
extern "C" void CustomAttributeData_ToString_m8476 ();
extern "C" void CustomAttributeData_Equals_m8477 ();
extern "C" void CustomAttributeData_GetHashCode_m8478 ();
extern "C" void CustomAttributeNamedArgument_ToString_m8479 ();
extern "C" void CustomAttributeNamedArgument_Equals_m8480 ();
extern "C" void CustomAttributeNamedArgument_GetHashCode_m8481 ();
extern "C" void CustomAttributeTypedArgument_ToString_m8482 ();
extern "C" void CustomAttributeTypedArgument_Equals_m8483 ();
extern "C" void CustomAttributeTypedArgument_GetHashCode_m8484 ();
extern "C" void AddEventAdapter__ctor_m8485 ();
extern "C" void AddEventAdapter_Invoke_m8486 ();
extern "C" void AddEventAdapter_BeginInvoke_m8487 ();
extern "C" void AddEventAdapter_EndInvoke_m8488 ();
extern "C" void EventInfo__ctor_m8489 ();
extern "C" void EventInfo_get_EventHandlerType_m8490 ();
extern "C" void EventInfo_get_MemberType_m8491 ();
extern "C" void FieldInfo__ctor_m8492 ();
extern "C" void FieldInfo_get_MemberType_m8493 ();
extern "C" void FieldInfo_get_IsLiteral_m8494 ();
extern "C" void FieldInfo_get_IsStatic_m8495 ();
extern "C" void FieldInfo_get_IsNotSerialized_m8496 ();
extern "C" void FieldInfo_SetValue_m8497 ();
extern "C" void FieldInfo_internal_from_handle_type_m8498 ();
extern "C" void FieldInfo_GetFieldFromHandle_m8499 ();
extern "C" void FieldInfo_GetFieldOffset_m8500 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m8501 ();
extern "C" void FieldInfo_get_UMarshal_m8502 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m8503 ();
extern "C" void MemberInfoSerializationHolder__ctor_m8504 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m8505 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m8506 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m8507 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m8508 ();
extern "C" void MethodBase__ctor_m8509 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m8510 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m8511 ();
extern "C" void MethodBase_GetMethodFromHandle_m8512 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m8513 ();
extern "C" void MethodBase_GetParameterCount_m8514 ();
extern "C" void MethodBase_Invoke_m8515 ();
extern "C" void MethodBase_get_CallingConvention_m8516 ();
extern "C" void MethodBase_get_IsPublic_m8517 ();
extern "C" void MethodBase_get_IsStatic_m8518 ();
extern "C" void MethodBase_get_IsVirtual_m8519 ();
extern "C" void MethodBase_get_IsAbstract_m8520 ();
extern "C" void MethodBase_get_next_table_index_m8521 ();
extern "C" void MethodBase_GetGenericArguments_m8522 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m8523 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m8524 ();
extern "C" void MethodBase_get_IsGenericMethod_m8525 ();
extern "C" void MethodInfo__ctor_m8526 ();
extern "C" void MethodInfo_get_MemberType_m8527 ();
extern "C" void MethodInfo_get_ReturnType_m8528 ();
extern "C" void MethodInfo_MakeGenericMethod_m8529 ();
extern "C" void MethodInfo_GetGenericArguments_m8530 ();
extern "C" void MethodInfo_get_IsGenericMethod_m8531 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m8532 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m8533 ();
extern "C" void Missing__ctor_m8534 ();
extern "C" void Missing__cctor_m8535 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8536 ();
extern "C" void Module__ctor_m8537 ();
extern "C" void Module__cctor_m8538 ();
extern "C" void Module_get_Assembly_m8539 ();
extern "C" void Module_get_ScopeName_m8540 ();
extern "C" void Module_GetCustomAttributes_m8541 ();
extern "C" void Module_GetObjectData_m8542 ();
extern "C" void Module_InternalGetTypes_m8543 ();
extern "C" void Module_GetTypes_m8544 ();
extern "C" void Module_IsDefined_m8545 ();
extern "C" void Module_IsResource_m8546 ();
extern "C" void Module_ToString_m8547 ();
extern "C" void Module_filter_by_type_name_m8548 ();
extern "C" void Module_filter_by_type_name_ignore_case_m8549 ();
extern "C" void MonoEventInfo_get_event_info_m8550 ();
extern "C" void MonoEventInfo_GetEventInfo_m8551 ();
extern "C" void MonoEvent__ctor_m8552 ();
extern "C" void MonoEvent_get_Attributes_m8553 ();
extern "C" void MonoEvent_GetAddMethod_m8554 ();
extern "C" void MonoEvent_get_DeclaringType_m8555 ();
extern "C" void MonoEvent_get_ReflectedType_m8556 ();
extern "C" void MonoEvent_get_Name_m8557 ();
extern "C" void MonoEvent_ToString_m8558 ();
extern "C" void MonoEvent_IsDefined_m8559 ();
extern "C" void MonoEvent_GetCustomAttributes_m8560 ();
extern "C" void MonoEvent_GetCustomAttributes_m8561 ();
extern "C" void MonoEvent_GetObjectData_m8562 ();
extern "C" void MonoField__ctor_m8563 ();
extern "C" void MonoField_get_Attributes_m8564 ();
extern "C" void MonoField_get_FieldHandle_m8565 ();
extern "C" void MonoField_get_FieldType_m8566 ();
extern "C" void MonoField_GetParentType_m8567 ();
extern "C" void MonoField_get_ReflectedType_m8568 ();
extern "C" void MonoField_get_DeclaringType_m8569 ();
extern "C" void MonoField_get_Name_m8570 ();
extern "C" void MonoField_IsDefined_m8571 ();
extern "C" void MonoField_GetCustomAttributes_m8572 ();
extern "C" void MonoField_GetCustomAttributes_m8573 ();
extern "C" void MonoField_GetFieldOffset_m8574 ();
extern "C" void MonoField_GetValueInternal_m8575 ();
extern "C" void MonoField_GetValue_m8576 ();
extern "C" void MonoField_ToString_m8577 ();
extern "C" void MonoField_SetValueInternal_m8578 ();
extern "C" void MonoField_SetValue_m8579 ();
extern "C" void MonoField_GetObjectData_m8580 ();
extern "C" void MonoField_CheckGeneric_m8581 ();
extern "C" void MonoGenericMethod__ctor_m8582 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m8583 ();
extern "C" void MonoGenericCMethod__ctor_m8584 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m8585 ();
extern "C" void MonoMethodInfo_get_method_info_m8586 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m8587 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m8588 ();
extern "C" void MonoMethodInfo_GetReturnType_m8589 ();
extern "C" void MonoMethodInfo_GetAttributes_m8590 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m8591 ();
extern "C" void MonoMethodInfo_get_parameter_info_m8592 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m8593 ();
extern "C" void MonoMethod__ctor_m8594 ();
extern "C" void MonoMethod_get_name_m8595 ();
extern "C" void MonoMethod_get_base_definition_m8596 ();
extern "C" void MonoMethod_GetBaseDefinition_m8597 ();
extern "C" void MonoMethod_get_ReturnType_m8598 ();
extern "C" void MonoMethod_GetParameters_m8599 ();
extern "C" void MonoMethod_InternalInvoke_m8600 ();
extern "C" void MonoMethod_Invoke_m8601 ();
extern "C" void MonoMethod_get_MethodHandle_m8602 ();
extern "C" void MonoMethod_get_Attributes_m8603 ();
extern "C" void MonoMethod_get_CallingConvention_m8604 ();
extern "C" void MonoMethod_get_ReflectedType_m8605 ();
extern "C" void MonoMethod_get_DeclaringType_m8606 ();
extern "C" void MonoMethod_get_Name_m8607 ();
extern "C" void MonoMethod_IsDefined_m8608 ();
extern "C" void MonoMethod_GetCustomAttributes_m8609 ();
extern "C" void MonoMethod_GetCustomAttributes_m8610 ();
extern "C" void MonoMethod_GetDllImportAttribute_m8611 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m8612 ();
extern "C" void MonoMethod_ShouldPrintFullName_m8613 ();
extern "C" void MonoMethod_ToString_m8614 ();
extern "C" void MonoMethod_GetObjectData_m8615 ();
extern "C" void MonoMethod_MakeGenericMethod_m8616 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m8617 ();
extern "C" void MonoMethod_GetGenericArguments_m8618 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m8619 ();
extern "C" void MonoMethod_get_IsGenericMethod_m8620 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m8621 ();
extern "C" void MonoCMethod__ctor_m8622 ();
extern "C" void MonoCMethod_GetParameters_m8623 ();
extern "C" void MonoCMethod_InternalInvoke_m8624 ();
extern "C" void MonoCMethod_Invoke_m8625 ();
extern "C" void MonoCMethod_Invoke_m8626 ();
extern "C" void MonoCMethod_get_MethodHandle_m8627 ();
extern "C" void MonoCMethod_get_Attributes_m8628 ();
extern "C" void MonoCMethod_get_CallingConvention_m8629 ();
extern "C" void MonoCMethod_get_ReflectedType_m8630 ();
extern "C" void MonoCMethod_get_DeclaringType_m8631 ();
extern "C" void MonoCMethod_get_Name_m8632 ();
extern "C" void MonoCMethod_IsDefined_m8633 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8634 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8635 ();
extern "C" void MonoCMethod_ToString_m8636 ();
extern "C" void MonoCMethod_GetObjectData_m8637 ();
extern "C" void MonoPropertyInfo_get_property_info_m8638 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m8639 ();
extern "C" void GetterAdapter__ctor_m8640 ();
extern "C" void GetterAdapter_Invoke_m8641 ();
extern "C" void GetterAdapter_BeginInvoke_m8642 ();
extern "C" void GetterAdapter_EndInvoke_m8643 ();
extern "C" void MonoProperty__ctor_m8644 ();
extern "C" void MonoProperty_CachePropertyInfo_m8645 ();
extern "C" void MonoProperty_get_Attributes_m8646 ();
extern "C" void MonoProperty_get_CanRead_m8647 ();
extern "C" void MonoProperty_get_CanWrite_m8648 ();
extern "C" void MonoProperty_get_PropertyType_m8649 ();
extern "C" void MonoProperty_get_ReflectedType_m8650 ();
extern "C" void MonoProperty_get_DeclaringType_m8651 ();
extern "C" void MonoProperty_get_Name_m8652 ();
extern "C" void MonoProperty_GetAccessors_m8653 ();
extern "C" void MonoProperty_GetGetMethod_m8654 ();
extern "C" void MonoProperty_GetIndexParameters_m8655 ();
extern "C" void MonoProperty_GetSetMethod_m8656 ();
extern "C" void MonoProperty_IsDefined_m8657 ();
extern "C" void MonoProperty_GetCustomAttributes_m8658 ();
extern "C" void MonoProperty_GetCustomAttributes_m8659 ();
extern "C" void MonoProperty_CreateGetterDelegate_m8660 ();
extern "C" void MonoProperty_GetValue_m8661 ();
extern "C" void MonoProperty_GetValue_m8662 ();
extern "C" void MonoProperty_SetValue_m8663 ();
extern "C" void MonoProperty_ToString_m8664 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m8665 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m8666 ();
extern "C" void MonoProperty_GetObjectData_m8667 ();
extern "C" void ParameterInfo__ctor_m8668 ();
extern "C" void ParameterInfo__ctor_m8669 ();
extern "C" void ParameterInfo__ctor_m8670 ();
extern "C" void ParameterInfo_ToString_m8671 ();
extern "C" void ParameterInfo_get_ParameterType_m8672 ();
extern "C" void ParameterInfo_get_Attributes_m8673 ();
extern "C" void ParameterInfo_get_IsIn_m8674 ();
extern "C" void ParameterInfo_get_IsOptional_m8675 ();
extern "C" void ParameterInfo_get_IsOut_m8676 ();
extern "C" void ParameterInfo_get_IsRetval_m8677 ();
extern "C" void ParameterInfo_get_Member_m8678 ();
extern "C" void ParameterInfo_get_Name_m8679 ();
extern "C" void ParameterInfo_get_Position_m8680 ();
extern "C" void ParameterInfo_GetCustomAttributes_m8681 ();
extern "C" void ParameterInfo_IsDefined_m8682 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m8683 ();
extern "C" void Pointer__ctor_m8684 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8685 ();
extern "C" void PropertyInfo__ctor_m8686 ();
extern "C" void PropertyInfo_get_MemberType_m8687 ();
extern "C" void PropertyInfo_GetValue_m8688 ();
extern "C" void PropertyInfo_SetValue_m8689 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m8690 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m8691 ();
extern "C" void StrongNameKeyPair__ctor_m8692 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8693 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8694 ();
extern "C" void TargetException__ctor_m8695 ();
extern "C" void TargetException__ctor_m8696 ();
extern "C" void TargetException__ctor_m8697 ();
extern "C" void TargetInvocationException__ctor_m8698 ();
extern "C" void TargetInvocationException__ctor_m8699 ();
extern "C" void TargetParameterCountException__ctor_m8700 ();
extern "C" void TargetParameterCountException__ctor_m8701 ();
extern "C" void TargetParameterCountException__ctor_m8702 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m8703 ();
extern "C" void ResourceManager__ctor_m8704 ();
extern "C" void ResourceManager__cctor_m8705 ();
extern "C" void ResourceInfo__ctor_m8706 ();
extern "C" void ResourceCacheItem__ctor_m8707 ();
extern "C" void ResourceEnumerator__ctor_m8708 ();
extern "C" void ResourceEnumerator_get_Entry_m8709 ();
extern "C" void ResourceEnumerator_get_Key_m8710 ();
extern "C" void ResourceEnumerator_get_Value_m8711 ();
extern "C" void ResourceEnumerator_get_Current_m8712 ();
extern "C" void ResourceEnumerator_MoveNext_m8713 ();
extern "C" void ResourceEnumerator_Reset_m8714 ();
extern "C" void ResourceEnumerator_FillCache_m8715 ();
extern "C" void ResourceReader__ctor_m8716 ();
extern "C" void ResourceReader__ctor_m8717 ();
extern "C" void ResourceReader_System_Collections_IEnumerable_GetEnumerator_m8718 ();
extern "C" void ResourceReader_System_IDisposable_Dispose_m8719 ();
extern "C" void ResourceReader_ReadHeaders_m8720 ();
extern "C" void ResourceReader_CreateResourceInfo_m8721 ();
extern "C" void ResourceReader_Read7BitEncodedInt_m8722 ();
extern "C" void ResourceReader_ReadValueVer2_m8723 ();
extern "C" void ResourceReader_ReadValueVer1_m8724 ();
extern "C" void ResourceReader_ReadNonPredefinedValue_m8725 ();
extern "C" void ResourceReader_LoadResourceValues_m8726 ();
extern "C" void ResourceReader_Close_m8727 ();
extern "C" void ResourceReader_GetEnumerator_m8728 ();
extern "C" void ResourceReader_Dispose_m8729 ();
extern "C" void ResourceSet__ctor_m8730 ();
extern "C" void ResourceSet__ctor_m8731 ();
extern "C" void ResourceSet__ctor_m8732 ();
extern "C" void ResourceSet__ctor_m8733 ();
extern "C" void ResourceSet_System_Collections_IEnumerable_GetEnumerator_m8734 ();
extern "C" void ResourceSet_Dispose_m8735 ();
extern "C" void ResourceSet_Dispose_m8736 ();
extern "C" void ResourceSet_GetEnumerator_m8737 ();
extern "C" void ResourceSet_GetObjectInternal_m8738 ();
extern "C" void ResourceSet_GetObject_m8739 ();
extern "C" void ResourceSet_GetObject_m8740 ();
extern "C" void ResourceSet_ReadResources_m8741 ();
extern "C" void RuntimeResourceSet__ctor_m8742 ();
extern "C" void RuntimeResourceSet__ctor_m8743 ();
extern "C" void RuntimeResourceSet__ctor_m8744 ();
extern "C" void RuntimeResourceSet_GetObject_m8745 ();
extern "C" void RuntimeResourceSet_GetObject_m8746 ();
extern "C" void RuntimeResourceSet_CloneDisposableObjectIfPossible_m8747 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m8748 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m8749 ();
extern "C" void DefaultDependencyAttribute__ctor_m8750 ();
extern "C" void StringFreezingAttribute__ctor_m8751 ();
extern "C" void CriticalFinalizerObject__ctor_m8752 ();
extern "C" void CriticalFinalizerObject_Finalize_m8753 ();
extern "C" void ReliabilityContractAttribute__ctor_m8754 ();
extern "C" void ClassInterfaceAttribute__ctor_m8755 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m8756 ();
extern "C" void DispIdAttribute__ctor_m8757 ();
extern "C" void GCHandle__ctor_m8758 ();
extern "C" void GCHandle_get_IsAllocated_m8759 ();
extern "C" void GCHandle_get_Target_m8760 ();
extern "C" void GCHandle_Alloc_m8761 ();
extern "C" void GCHandle_Free_m8762 ();
extern "C" void GCHandle_GetTarget_m8763 ();
extern "C" void GCHandle_GetTargetHandle_m8764 ();
extern "C" void GCHandle_FreeHandle_m8765 ();
extern "C" void GCHandle_Equals_m8766 ();
extern "C" void GCHandle_GetHashCode_m8767 ();
extern "C" void InterfaceTypeAttribute__ctor_m8768 ();
extern "C" void Marshal__cctor_m8769 ();
extern "C" void Marshal_copy_from_unmanaged_m8770 ();
extern "C" void Marshal_Copy_m8771 ();
extern "C" void Marshal_Copy_m8772 ();
extern "C" void Marshal_ReadByte_m8773 ();
extern "C" void Marshal_WriteByte_m8774 ();
extern "C" void MarshalDirectiveException__ctor_m8775 ();
extern "C" void MarshalDirectiveException__ctor_m8776 ();
extern "C" void PreserveSigAttribute__ctor_m8777 ();
extern "C" void SafeHandle__ctor_m8778 ();
extern "C" void SafeHandle_Close_m8779 ();
extern "C" void SafeHandle_DangerousAddRef_m8780 ();
extern "C" void SafeHandle_DangerousGetHandle_m8781 ();
extern "C" void SafeHandle_DangerousRelease_m8782 ();
extern "C" void SafeHandle_Dispose_m8783 ();
extern "C" void SafeHandle_Dispose_m8784 ();
extern "C" void SafeHandle_SetHandle_m8785 ();
extern "C" void SafeHandle_Finalize_m8786 ();
extern "C" void TypeLibImportClassAttribute__ctor_m8787 ();
extern "C" void TypeLibVersionAttribute__ctor_m8788 ();
extern "C" void ActivationServices_get_ConstructionActivator_m8789 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m8790 ();
extern "C" void ActivationServices_CreateConstructionCall_m8791 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m8792 ();
extern "C" void ActivationServices_EnableProxyActivation_m8793 ();
extern "C" void AppDomainLevelActivator__ctor_m8794 ();
extern "C" void ConstructionLevelActivator__ctor_m8795 ();
extern "C" void ContextLevelActivator__ctor_m8796 ();
extern "C" void UrlAttribute_get_UrlValue_m8797 ();
extern "C" void UrlAttribute_Equals_m8798 ();
extern "C" void UrlAttribute_GetHashCode_m8799 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m8800 ();
extern "C" void UrlAttribute_IsContextOK_m8801 ();
extern "C" void ChannelInfo__ctor_m8802 ();
extern "C" void ChannelInfo_get_ChannelData_m8803 ();
extern "C" void ChannelServices__cctor_m8804 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8805 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8806 ();
extern "C" void ChannelServices_RegisterChannel_m8807 ();
extern "C" void ChannelServices_RegisterChannel_m8808 ();
extern "C" void ChannelServices_RegisterChannelConfig_m8809 ();
extern "C" void ChannelServices_CreateProvider_m8810 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m8811 ();
extern "C" void CrossAppDomainData__ctor_m8812 ();
extern "C" void CrossAppDomainData_get_DomainID_m8813 ();
extern "C" void CrossAppDomainData_get_ProcessID_m8814 ();
extern "C" void CrossAppDomainChannel__ctor_m8815 ();
extern "C" void CrossAppDomainChannel__cctor_m8816 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8817 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m8818 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m8819 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m8820 ();
extern "C" void CrossAppDomainChannel_StartListening_m8821 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m8822 ();
extern "C" void CrossAppDomainSink__ctor_m8823 ();
extern "C" void CrossAppDomainSink__cctor_m8824 ();
extern "C" void CrossAppDomainSink_GetSink_m8825 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m8826 ();
extern "C" void SinkProviderData__ctor_m8827 ();
extern "C" void SinkProviderData_get_Children_m8828 ();
extern "C" void SinkProviderData_get_Properties_m8829 ();
extern "C" void Context__ctor_m8830 ();
extern "C" void Context__cctor_m8831 ();
extern "C" void Context_Finalize_m8832 ();
extern "C" void Context_get_DefaultContext_m8833 ();
extern "C" void Context_get_ContextID_m8834 ();
extern "C" void Context_get_ContextProperties_m8835 ();
extern "C" void Context_get_IsDefaultContext_m8836 ();
extern "C" void Context_get_NeedsContextSink_m8837 ();
extern "C" void Context_RegisterDynamicProperty_m8838 ();
extern "C" void Context_UnregisterDynamicProperty_m8839 ();
extern "C" void Context_GetDynamicPropertyCollection_m8840 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m8841 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m8842 ();
extern "C" void Context_NotifyDynamicSinks_m8843 ();
extern "C" void Context_get_HasDynamicSinks_m8844 ();
extern "C" void Context_get_HasExitSinks_m8845 ();
extern "C" void Context_GetProperty_m8846 ();
extern "C" void Context_SetProperty_m8847 ();
extern "C" void Context_Freeze_m8848 ();
extern "C" void Context_ToString_m8849 ();
extern "C" void Context_GetServerContextSinkChain_m8850 ();
extern "C" void Context_GetClientContextSinkChain_m8851 ();
extern "C" void Context_CreateServerObjectSinkChain_m8852 ();
extern "C" void Context_CreateEnvoySink_m8853 ();
extern "C" void Context_SwitchToContext_m8854 ();
extern "C" void Context_CreateNewContext_m8855 ();
extern "C" void Context_DoCallBack_m8856 ();
extern "C" void Context_AllocateDataSlot_m8857 ();
extern "C" void Context_AllocateNamedDataSlot_m8858 ();
extern "C" void Context_FreeNamedDataSlot_m8859 ();
extern "C" void Context_GetData_m8860 ();
extern "C" void Context_GetNamedDataSlot_m8861 ();
extern "C" void Context_SetData_m8862 ();
extern "C" void DynamicPropertyReg__ctor_m8863 ();
extern "C" void DynamicPropertyCollection__ctor_m8864 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m8865 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m8866 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m8867 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m8868 ();
extern "C" void DynamicPropertyCollection_FindProperty_m8869 ();
extern "C" void ContextCallbackObject__ctor_m8870 ();
extern "C" void ContextCallbackObject_DoCallBack_m8871 ();
extern "C" void ContextAttribute__ctor_m8872 ();
extern "C" void ContextAttribute_get_Name_m8873 ();
extern "C" void ContextAttribute_Equals_m8874 ();
extern "C" void ContextAttribute_Freeze_m8875 ();
extern "C" void ContextAttribute_GetHashCode_m8876 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m8877 ();
extern "C" void ContextAttribute_IsContextOK_m8878 ();
extern "C" void ContextAttribute_IsNewContextOK_m8879 ();
extern "C" void CrossContextChannel__ctor_m8880 ();
extern "C" void SynchronizationAttribute__ctor_m8881 ();
extern "C" void SynchronizationAttribute__ctor_m8882 ();
extern "C" void SynchronizationAttribute_set_Locked_m8883 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m8884 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m8885 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m8886 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m8887 ();
extern "C" void SynchronizationAttribute_IsContextOK_m8888 ();
extern "C" void SynchronizationAttribute_ExitContext_m8889 ();
extern "C" void SynchronizationAttribute_EnterContext_m8890 ();
extern "C" void SynchronizedClientContextSink__ctor_m8891 ();
extern "C" void SynchronizedServerContextSink__ctor_m8892 ();
extern "C" void LeaseManager__ctor_m8893 ();
extern "C" void LeaseManager_SetPollTime_m8894 ();
extern "C" void LeaseSink__ctor_m8895 ();
extern "C" void LifetimeServices__cctor_m8896 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m8897 ();
extern "C" void LifetimeServices_set_LeaseTime_m8898 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m8899 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m8900 ();
extern "C" void ArgInfo__ctor_m8901 ();
extern "C" void ArgInfo_GetInOutArgs_m8902 ();
extern "C" void AsyncResult__ctor_m8903 ();
extern "C" void AsyncResult_get_AsyncState_m8904 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m8905 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m8906 ();
extern "C" void AsyncResult_get_IsCompleted_m8907 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m8908 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m8909 ();
extern "C" void AsyncResult_get_AsyncDelegate_m8910 ();
extern "C" void AsyncResult_get_NextSink_m8911 ();
extern "C" void AsyncResult_AsyncProcessMessage_m8912 ();
extern "C" void AsyncResult_GetReplyMessage_m8913 ();
extern "C" void AsyncResult_SetMessageCtrl_m8914 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m8915 ();
extern "C" void AsyncResult_EndInvoke_m8916 ();
extern "C" void AsyncResult_SyncProcessMessage_m8917 ();
extern "C" void AsyncResult_get_CallMessage_m8918 ();
extern "C" void AsyncResult_set_CallMessage_m8919 ();
extern "C" void ClientContextTerminatorSink__ctor_m8920 ();
extern "C" void ConstructionCall__ctor_m8921 ();
extern "C" void ConstructionCall__ctor_m8922 ();
extern "C" void ConstructionCall_InitDictionary_m8923 ();
extern "C" void ConstructionCall_set_IsContextOk_m8924 ();
extern "C" void ConstructionCall_get_ActivationType_m8925 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m8926 ();
extern "C" void ConstructionCall_get_Activator_m8927 ();
extern "C" void ConstructionCall_set_Activator_m8928 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m8929 ();
extern "C" void ConstructionCall_SetActivationAttributes_m8930 ();
extern "C" void ConstructionCall_get_ContextProperties_m8931 ();
extern "C" void ConstructionCall_InitMethodProperty_m8932 ();
extern "C" void ConstructionCall_GetObjectData_m8933 ();
extern "C" void ConstructionCall_get_Properties_m8934 ();
extern "C" void ConstructionCallDictionary__ctor_m8935 ();
extern "C" void ConstructionCallDictionary__cctor_m8936 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m8937 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m8938 ();
extern "C" void EnvoyTerminatorSink__ctor_m8939 ();
extern "C" void EnvoyTerminatorSink__cctor_m8940 ();
extern "C" void Header__ctor_m8941 ();
extern "C" void Header__ctor_m8942 ();
extern "C" void Header__ctor_m8943 ();
extern "C" void LogicalCallContext__ctor_m8944 ();
extern "C" void LogicalCallContext__ctor_m8945 ();
extern "C" void LogicalCallContext_GetObjectData_m8946 ();
extern "C" void LogicalCallContext_SetData_m8947 ();
extern "C" void LogicalCallContext_Clone_m8948 ();
extern "C" void CallContextRemotingData__ctor_m8949 ();
extern "C" void CallContextRemotingData_Clone_m8950 ();
extern "C" void MethodCall__ctor_m8951 ();
extern "C" void MethodCall__ctor_m8952 ();
extern "C" void MethodCall__ctor_m8953 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8954 ();
extern "C" void MethodCall_InitMethodProperty_m8955 ();
extern "C" void MethodCall_GetObjectData_m8956 ();
extern "C" void MethodCall_get_Args_m8957 ();
extern "C" void MethodCall_get_LogicalCallContext_m8958 ();
extern "C" void MethodCall_get_MethodBase_m8959 ();
extern "C" void MethodCall_get_MethodName_m8960 ();
extern "C" void MethodCall_get_MethodSignature_m8961 ();
extern "C" void MethodCall_get_Properties_m8962 ();
extern "C" void MethodCall_InitDictionary_m8963 ();
extern "C" void MethodCall_get_TypeName_m8964 ();
extern "C" void MethodCall_get_Uri_m8965 ();
extern "C" void MethodCall_set_Uri_m8966 ();
extern "C" void MethodCall_Init_m8967 ();
extern "C" void MethodCall_ResolveMethod_m8968 ();
extern "C" void MethodCall_CastTo_m8969 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m8970 ();
extern "C" void MethodCall_get_GenericArguments_m8971 ();
extern "C" void MethodCallDictionary__ctor_m8972 ();
extern "C" void MethodCallDictionary__cctor_m8973 ();
extern "C" void DictionaryEnumerator__ctor_m8974 ();
extern "C" void DictionaryEnumerator_get_Current_m8975 ();
extern "C" void DictionaryEnumerator_MoveNext_m8976 ();
extern "C" void DictionaryEnumerator_Reset_m8977 ();
extern "C" void DictionaryEnumerator_get_Entry_m8978 ();
extern "C" void DictionaryEnumerator_get_Key_m8979 ();
extern "C" void DictionaryEnumerator_get_Value_m8980 ();
extern "C" void MethodDictionary__ctor_m8981 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8982 ();
extern "C" void MethodDictionary_set_MethodKeys_m8983 ();
extern "C" void MethodDictionary_AllocInternalProperties_m8984 ();
extern "C" void MethodDictionary_GetInternalProperties_m8985 ();
extern "C" void MethodDictionary_IsOverridenKey_m8986 ();
extern "C" void MethodDictionary_get_Item_m8987 ();
extern "C" void MethodDictionary_set_Item_m8988 ();
extern "C" void MethodDictionary_GetMethodProperty_m8989 ();
extern "C" void MethodDictionary_SetMethodProperty_m8990 ();
extern "C" void MethodDictionary_get_Values_m8991 ();
extern "C" void MethodDictionary_Add_m8992 ();
extern "C" void MethodDictionary_Contains_m8993 ();
extern "C" void MethodDictionary_Remove_m8994 ();
extern "C" void MethodDictionary_get_Count_m8995 ();
extern "C" void MethodDictionary_get_IsSynchronized_m8996 ();
extern "C" void MethodDictionary_get_SyncRoot_m8997 ();
extern "C" void MethodDictionary_CopyTo_m8998 ();
extern "C" void MethodDictionary_GetEnumerator_m8999 ();
extern "C" void MethodReturnDictionary__ctor_m9000 ();
extern "C" void MethodReturnDictionary__cctor_m9001 ();
extern "C" void MonoMethodMessage_get_Args_m9002 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m9003 ();
extern "C" void MonoMethodMessage_get_MethodBase_m9004 ();
extern "C" void MonoMethodMessage_get_MethodName_m9005 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m9006 ();
extern "C" void MonoMethodMessage_get_TypeName_m9007 ();
extern "C" void MonoMethodMessage_get_Uri_m9008 ();
extern "C" void MonoMethodMessage_set_Uri_m9009 ();
extern "C" void MonoMethodMessage_get_Exception_m9010 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m9011 ();
extern "C" void MonoMethodMessage_get_OutArgs_m9012 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m9013 ();
extern "C" void RemotingSurrogate__ctor_m9014 ();
extern "C" void RemotingSurrogate_SetObjectData_m9015 ();
extern "C" void ObjRefSurrogate__ctor_m9016 ();
extern "C" void ObjRefSurrogate_SetObjectData_m9017 ();
extern "C" void RemotingSurrogateSelector__ctor_m9018 ();
extern "C" void RemotingSurrogateSelector__cctor_m9019 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m9020 ();
extern "C" void ReturnMessage__ctor_m9021 ();
extern "C" void ReturnMessage__ctor_m9022 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m9023 ();
extern "C" void ReturnMessage_get_Args_m9024 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m9025 ();
extern "C" void ReturnMessage_get_MethodBase_m9026 ();
extern "C" void ReturnMessage_get_MethodName_m9027 ();
extern "C" void ReturnMessage_get_MethodSignature_m9028 ();
extern "C" void ReturnMessage_get_Properties_m9029 ();
extern "C" void ReturnMessage_get_TypeName_m9030 ();
extern "C" void ReturnMessage_get_Uri_m9031 ();
extern "C" void ReturnMessage_set_Uri_m9032 ();
extern "C" void ReturnMessage_get_Exception_m9033 ();
extern "C" void ReturnMessage_get_OutArgs_m9034 ();
extern "C" void ReturnMessage_get_ReturnValue_m9035 ();
extern "C" void ServerContextTerminatorSink__ctor_m9036 ();
extern "C" void ServerObjectTerminatorSink__ctor_m9037 ();
extern "C" void StackBuilderSink__ctor_m9038 ();
extern "C" void SoapAttribute__ctor_m9039 ();
extern "C" void SoapAttribute_get_UseAttribute_m9040 ();
extern "C" void SoapAttribute_get_XmlNamespace_m9041 ();
extern "C" void SoapAttribute_SetReflectionObject_m9042 ();
extern "C" void SoapFieldAttribute__ctor_m9043 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m9044 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m9045 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m9046 ();
extern "C" void SoapMethodAttribute__ctor_m9047 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m9048 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m9049 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m9050 ();
extern "C" void SoapParameterAttribute__ctor_m9051 ();
extern "C" void SoapTypeAttribute__ctor_m9052 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m9053 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m9054 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m9055 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m9056 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m9057 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m9058 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m9059 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m9060 ();
extern "C" void ProxyAttribute_CreateInstance_m9061 ();
extern "C" void ProxyAttribute_CreateProxy_m9062 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m9063 ();
extern "C" void ProxyAttribute_IsContextOK_m9064 ();
extern "C" void RealProxy__ctor_m9065 ();
extern "C" void RealProxy__ctor_m9066 ();
extern "C" void RealProxy__ctor_m9067 ();
extern "C" void RealProxy_InternalGetProxyType_m9068 ();
extern "C" void RealProxy_GetProxiedType_m9069 ();
extern "C" void RealProxy_get_ObjectIdentity_m9070 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m9071 ();
extern "C" void RealProxy_GetTransparentProxy_m9072 ();
extern "C" void RealProxy_SetTargetDomain_m9073 ();
extern "C" void RemotingProxy__ctor_m9074 ();
extern "C" void RemotingProxy__ctor_m9075 ();
extern "C" void RemotingProxy__cctor_m9076 ();
extern "C" void RemotingProxy_get_TypeName_m9077 ();
extern "C" void RemotingProxy_Finalize_m9078 ();
extern "C" void TrackingServices__cctor_m9079 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m9080 ();
extern "C" void ActivatedClientTypeEntry__ctor_m9081 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m9082 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m9083 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m9084 ();
extern "C" void ActivatedClientTypeEntry_ToString_m9085 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m9086 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m9087 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m9088 ();
extern "C" void EnvoyInfo__ctor_m9089 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m9090 ();
extern "C" void Identity__ctor_m9091 ();
extern "C" void Identity_get_ChannelSink_m9092 ();
extern "C" void Identity_set_ChannelSink_m9093 ();
extern "C" void Identity_get_ObjectUri_m9094 ();
extern "C" void Identity_get_Disposed_m9095 ();
extern "C" void Identity_set_Disposed_m9096 ();
extern "C" void Identity_get_ClientDynamicProperties_m9097 ();
extern "C" void Identity_get_ServerDynamicProperties_m9098 ();
extern "C" void ClientIdentity__ctor_m9099 ();
extern "C" void ClientIdentity_get_ClientProxy_m9100 ();
extern "C" void ClientIdentity_set_ClientProxy_m9101 ();
extern "C" void ClientIdentity_CreateObjRef_m9102 ();
extern "C" void ClientIdentity_get_TargetUri_m9103 ();
extern "C" void InternalRemotingServices__cctor_m9104 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m9105 ();
extern "C" void ObjRef__ctor_m9106 ();
extern "C" void ObjRef__ctor_m9107 ();
extern "C" void ObjRef__cctor_m9108 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m9109 ();
extern "C" void ObjRef_get_ChannelInfo_m9110 ();
extern "C" void ObjRef_get_EnvoyInfo_m9111 ();
extern "C" void ObjRef_set_EnvoyInfo_m9112 ();
extern "C" void ObjRef_get_TypeInfo_m9113 ();
extern "C" void ObjRef_set_TypeInfo_m9114 ();
extern "C" void ObjRef_get_URI_m9115 ();
extern "C" void ObjRef_set_URI_m9116 ();
extern "C" void ObjRef_GetObjectData_m9117 ();
extern "C" void ObjRef_GetRealObject_m9118 ();
extern "C" void ObjRef_UpdateChannelInfo_m9119 ();
extern "C" void ObjRef_get_ServerType_m9120 ();
extern "C" void RemotingConfiguration__cctor_m9121 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m9122 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m9123 ();
extern "C" void RemotingConfiguration_get_ProcessId_m9124 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m9125 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m9126 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m9127 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m9128 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m9129 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m9130 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m9131 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m9132 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m9133 ();
extern "C" void RemotingConfiguration_RegisterChannels_m9134 ();
extern "C" void RemotingConfiguration_RegisterTypes_m9135 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m9136 ();
extern "C" void ConfigHandler__ctor_m9137 ();
extern "C" void ConfigHandler_ValidatePath_m9138 ();
extern "C" void ConfigHandler_CheckPath_m9139 ();
extern "C" void ConfigHandler_OnStartParsing_m9140 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m9141 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m9142 ();
extern "C" void ConfigHandler_OnStartElement_m9143 ();
extern "C" void ConfigHandler_ParseElement_m9144 ();
extern "C" void ConfigHandler_OnEndElement_m9145 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m9146 ();
extern "C" void ConfigHandler_ReadLifetine_m9147 ();
extern "C" void ConfigHandler_ParseTime_m9148 ();
extern "C" void ConfigHandler_ReadChannel_m9149 ();
extern "C" void ConfigHandler_ReadProvider_m9150 ();
extern "C" void ConfigHandler_ReadClientActivated_m9151 ();
extern "C" void ConfigHandler_ReadServiceActivated_m9152 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m9153 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m9154 ();
extern "C" void ConfigHandler_ReadInteropXml_m9155 ();
extern "C" void ConfigHandler_ReadPreload_m9156 ();
extern "C" void ConfigHandler_GetNotNull_m9157 ();
extern "C" void ConfigHandler_ExtractAssembly_m9158 ();
extern "C" void ConfigHandler_OnChars_m9159 ();
extern "C" void ConfigHandler_OnEndParsing_m9160 ();
extern "C" void ChannelData__ctor_m9161 ();
extern "C" void ChannelData_get_ServerProviders_m9162 ();
extern "C" void ChannelData_get_ClientProviders_m9163 ();
extern "C" void ChannelData_get_CustomProperties_m9164 ();
extern "C" void ChannelData_CopyFrom_m9165 ();
extern "C" void ProviderData__ctor_m9166 ();
extern "C" void ProviderData_CopyFrom_m9167 ();
extern "C" void FormatterData__ctor_m9168 ();
extern "C" void RemotingException__ctor_m9169 ();
extern "C" void RemotingException__ctor_m9170 ();
extern "C" void RemotingException__ctor_m9171 ();
extern "C" void RemotingException__ctor_m9172 ();
extern "C" void RemotingServices__cctor_m9173 ();
extern "C" void RemotingServices_GetVirtualMethod_m9174 ();
extern "C" void RemotingServices_IsTransparentProxy_m9175 ();
extern "C" void RemotingServices_GetServerTypeForUri_m9176 ();
extern "C" void RemotingServices_Unmarshal_m9177 ();
extern "C" void RemotingServices_Unmarshal_m9178 ();
extern "C" void RemotingServices_GetRealProxy_m9179 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m9180 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m9181 ();
extern "C" void RemotingServices_FindInterfaceMethod_m9182 ();
extern "C" void RemotingServices_CreateClientProxy_m9183 ();
extern "C" void RemotingServices_CreateClientProxy_m9184 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m9185 ();
extern "C" void RemotingServices_GetIdentityForUri_m9186 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m9187 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m9188 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m9189 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m9190 ();
extern "C" void RemotingServices_RegisterServerIdentity_m9191 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m9192 ();
extern "C" void RemotingServices_GetRemoteObject_m9193 ();
extern "C" void RemotingServices_RegisterInternalChannels_m9194 ();
extern "C" void RemotingServices_DisposeIdentity_m9195 ();
extern "C" void RemotingServices_GetNormalizedUri_m9196 ();
extern "C" void ServerIdentity__ctor_m9197 ();
extern "C" void ServerIdentity_get_ObjectType_m9198 ();
extern "C" void ServerIdentity_CreateObjRef_m9199 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m9200 ();
extern "C" void SingletonIdentity__ctor_m9201 ();
extern "C" void SingleCallIdentity__ctor_m9202 ();
extern "C" void TypeInfo__ctor_m9203 ();
extern "C" void SoapServices__cctor_m9204 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m9205 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m9206 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m9207 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m9208 ();
extern "C" void SoapServices_GetNameKey_m9209 ();
extern "C" void SoapServices_GetAssemblyName_m9210 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m9211 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m9212 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m9213 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m9214 ();
extern "C" void SoapServices_PreLoad_m9215 ();
extern "C" void SoapServices_PreLoad_m9216 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m9217 ();
extern "C" void SoapServices_RegisterInteropXmlType_m9218 ();
extern "C" void SoapServices_EncodeNs_m9219 ();
extern "C" void TypeEntry__ctor_m9220 ();
extern "C" void TypeEntry_get_AssemblyName_m9221 ();
extern "C" void TypeEntry_set_AssemblyName_m9222 ();
extern "C" void TypeEntry_get_TypeName_m9223 ();
extern "C" void TypeEntry_set_TypeName_m9224 ();
extern "C" void TypeInfo__ctor_m9225 ();
extern "C" void TypeInfo_get_TypeName_m9226 ();
extern "C" void WellKnownClientTypeEntry__ctor_m9227 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m9228 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m9229 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m9230 ();
extern "C" void WellKnownClientTypeEntry_ToString_m9231 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m9232 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m9233 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m9234 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m9235 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m9236 ();
extern "C" void BinaryCommon__cctor_m9237 ();
extern "C" void BinaryCommon_IsPrimitive_m9238 ();
extern "C" void BinaryCommon_GetTypeFromCode_m9239 ();
extern "C" void BinaryCommon_SwapBytes_m9240 ();
extern "C" void BinaryFormatter__ctor_m9241 ();
extern "C" void BinaryFormatter__ctor_m9242 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m9243 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m9244 ();
extern "C" void BinaryFormatter_get_Binder_m9245 ();
extern "C" void BinaryFormatter_get_Context_m9246 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m9247 ();
extern "C" void BinaryFormatter_get_FilterLevel_m9248 ();
extern "C" void BinaryFormatter_Deserialize_m9249 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m9250 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m9251 ();
extern "C" void MessageFormatter_ReadMethodCall_m9252 ();
extern "C" void MessageFormatter_ReadMethodResponse_m9253 ();
extern "C" void TypeMetadata__ctor_m9254 ();
extern "C" void ArrayNullFiller__ctor_m9255 ();
extern "C" void ObjectReader__ctor_m9256 ();
extern "C" void ObjectReader_ReadObjectGraph_m9257 ();
extern "C" void ObjectReader_ReadObjectGraph_m9258 ();
extern "C" void ObjectReader_ReadNextObject_m9259 ();
extern "C" void ObjectReader_ReadNextObject_m9260 ();
extern "C" void ObjectReader_get_CurrentObject_m9261 ();
extern "C" void ObjectReader_ReadObject_m9262 ();
extern "C" void ObjectReader_ReadAssembly_m9263 ();
extern "C" void ObjectReader_ReadObjectInstance_m9264 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m9265 ();
extern "C" void ObjectReader_ReadObjectContent_m9266 ();
extern "C" void ObjectReader_RegisterObject_m9267 ();
extern "C" void ObjectReader_ReadStringIntance_m9268 ();
extern "C" void ObjectReader_ReadGenericArray_m9269 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m9270 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m9271 ();
extern "C" void ObjectReader_BlockRead_m9272 ();
extern "C" void ObjectReader_ReadArrayOfObject_m9273 ();
extern "C" void ObjectReader_ReadArrayOfString_m9274 ();
extern "C" void ObjectReader_ReadSimpleArray_m9275 ();
extern "C" void ObjectReader_ReadTypeMetadata_m9276 ();
extern "C" void ObjectReader_ReadValue_m9277 ();
extern "C" void ObjectReader_SetObjectValue_m9278 ();
extern "C" void ObjectReader_RecordFixup_m9279 ();
extern "C" void ObjectReader_GetDeserializationType_m9280 ();
extern "C" void ObjectReader_ReadType_m9281 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m9282 ();
extern "C" void FormatterConverter__ctor_m9283 ();
extern "C" void FormatterConverter_Convert_m9284 ();
extern "C" void FormatterConverter_ToBoolean_m9285 ();
extern "C" void FormatterConverter_ToInt16_m9286 ();
extern "C" void FormatterConverter_ToInt32_m9287 ();
extern "C" void FormatterConverter_ToInt64_m9288 ();
extern "C" void FormatterConverter_ToString_m9289 ();
extern "C" void FormatterServices_GetUninitializedObject_m9290 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m9291 ();
extern "C" void ObjectManager__ctor_m9292 ();
extern "C" void ObjectManager_DoFixups_m9293 ();
extern "C" void ObjectManager_GetObjectRecord_m9294 ();
extern "C" void ObjectManager_GetObject_m9295 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m9296 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m9297 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m9298 ();
extern "C" void ObjectManager_AddFixup_m9299 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m9300 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m9301 ();
extern "C" void ObjectManager_RecordDelayedFixup_m9302 ();
extern "C" void ObjectManager_RecordFixup_m9303 ();
extern "C" void ObjectManager_RegisterObjectInternal_m9304 ();
extern "C" void ObjectManager_RegisterObject_m9305 ();
extern "C" void BaseFixupRecord__ctor_m9306 ();
extern "C" void BaseFixupRecord_DoFixup_m9307 ();
extern "C" void ArrayFixupRecord__ctor_m9308 ();
extern "C" void ArrayFixupRecord_FixupImpl_m9309 ();
extern "C" void MultiArrayFixupRecord__ctor_m9310 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m9311 ();
extern "C" void FixupRecord__ctor_m9312 ();
extern "C" void FixupRecord_FixupImpl_m9313 ();
extern "C" void DelayedFixupRecord__ctor_m9314 ();
extern "C" void DelayedFixupRecord_FixupImpl_m9315 ();
extern "C" void ObjectRecord__ctor_m9316 ();
extern "C" void ObjectRecord_SetMemberValue_m9317 ();
extern "C" void ObjectRecord_SetArrayValue_m9318 ();
extern "C" void ObjectRecord_SetMemberValue_m9319 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m9320 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m9321 ();
extern "C" void ObjectRecord_get_IsRegistered_m9322 ();
extern "C" void ObjectRecord_DoFixups_m9323 ();
extern "C" void ObjectRecord_RemoveFixup_m9324 ();
extern "C" void ObjectRecord_UnchainFixup_m9325 ();
extern "C" void ObjectRecord_ChainFixup_m9326 ();
extern "C" void ObjectRecord_LoadData_m9327 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m9328 ();
extern "C" void SerializationBinder__ctor_m9329 ();
extern "C" void CallbackHandler__ctor_m9330 ();
extern "C" void CallbackHandler_Invoke_m9331 ();
extern "C" void CallbackHandler_BeginInvoke_m9332 ();
extern "C" void CallbackHandler_EndInvoke_m9333 ();
extern "C" void SerializationCallbacks__ctor_m9334 ();
extern "C" void SerializationCallbacks__cctor_m9335 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m9336 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m9337 ();
extern "C" void SerializationCallbacks_Invoke_m9338 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m9339 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m9340 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m9341 ();
extern "C" void SerializationEntry__ctor_m9342 ();
extern "C" void SerializationEntry_get_Name_m9343 ();
extern "C" void SerializationEntry_get_Value_m9344 ();
extern "C" void SerializationException__ctor_m9345 ();
extern "C" void SerializationException__ctor_m4830 ();
extern "C" void SerializationException__ctor_m9346 ();
extern "C" void SerializationInfo__ctor_m9347 ();
extern "C" void SerializationInfo_AddValue_m4826 ();
extern "C" void SerializationInfo_GetValue_m4829 ();
extern "C" void SerializationInfo_SetType_m9348 ();
extern "C" void SerializationInfo_GetEnumerator_m9349 ();
extern "C" void SerializationInfo_AddValue_m9350 ();
extern "C" void SerializationInfo_AddValue_m4828 ();
extern "C" void SerializationInfo_AddValue_m4827 ();
extern "C" void SerializationInfo_AddValue_m9351 ();
extern "C" void SerializationInfo_AddValue_m9352 ();
extern "C" void SerializationInfo_AddValue_m4839 ();
extern "C" void SerializationInfo_AddValue_m9353 ();
extern "C" void SerializationInfo_AddValue_m4838 ();
extern "C" void SerializationInfo_GetBoolean_m4831 ();
extern "C" void SerializationInfo_GetInt16_m9354 ();
extern "C" void SerializationInfo_GetInt32_m4837 ();
extern "C" void SerializationInfo_GetInt64_m4836 ();
extern "C" void SerializationInfo_GetString_m4835 ();
extern "C" void SerializationInfoEnumerator__ctor_m9355 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m9356 ();
extern "C" void SerializationInfoEnumerator_get_Current_m9357 ();
extern "C" void SerializationInfoEnumerator_get_Name_m9358 ();
extern "C" void SerializationInfoEnumerator_get_Value_m9359 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m9360 ();
extern "C" void SerializationInfoEnumerator_Reset_m9361 ();
extern "C" void StreamingContext__ctor_m9362 ();
extern "C" void StreamingContext__ctor_m9363 ();
extern "C" void StreamingContext_get_State_m9364 ();
extern "C" void StreamingContext_Equals_m9365 ();
extern "C" void StreamingContext_GetHashCode_m9366 ();
extern "C" void X509Certificate__ctor_m9367 ();
extern "C" void X509Certificate__ctor_m5886 ();
extern "C" void X509Certificate__ctor_m4887 ();
extern "C" void X509Certificate__ctor_m9368 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9369 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m9370 ();
extern "C" void X509Certificate_tostr_m9371 ();
extern "C" void X509Certificate_Equals_m9372 ();
extern "C" void X509Certificate_GetCertHash_m9373 ();
extern "C" void X509Certificate_GetCertHashString_m4892 ();
extern "C" void X509Certificate_GetEffectiveDateString_m9374 ();
extern "C" void X509Certificate_GetExpirationDateString_m9375 ();
extern "C" void X509Certificate_GetHashCode_m9376 ();
extern "C" void X509Certificate_GetIssuerName_m9377 ();
extern "C" void X509Certificate_GetName_m9378 ();
extern "C" void X509Certificate_GetPublicKey_m9379 ();
extern "C" void X509Certificate_GetRawCertData_m9380 ();
extern "C" void X509Certificate_ToString_m9381 ();
extern "C" void X509Certificate_ToString_m4905 ();
extern "C" void X509Certificate_get_Issuer_m4908 ();
extern "C" void X509Certificate_get_Subject_m4907 ();
extern "C" void X509Certificate_Equals_m9382 ();
extern "C" void X509Certificate_Import_m4902 ();
extern "C" void X509Certificate_Reset_m4904 ();
extern "C" void AsymmetricAlgorithm__ctor_m9383 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m9384 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m5834 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m5833 ();
extern "C" void AsymmetricAlgorithm_Clear_m5892 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m9385 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m9386 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m5881 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m5882 ();
extern "C" void Base64Constants__cctor_m9387 ();
extern "C" void CryptoConfig__cctor_m9388 ();
extern "C" void CryptoConfig_Initialize_m9389 ();
extern "C" void CryptoConfig_CreateFromName_m4910 ();
extern "C" void CryptoConfig_CreateFromName_m4937 ();
extern "C" void CryptoConfig_MapNameToOID_m5828 ();
extern "C" void CryptoConfig_EncodeOID_m4912 ();
extern "C" void CryptoConfig_EncodeLongNumber_m9390 ();
extern "C" void CryptographicException__ctor_m9391 ();
extern "C" void CryptographicException__ctor_m3892 ();
extern "C" void CryptographicException__ctor_m4872 ();
extern "C" void CryptographicException__ctor_m3901 ();
extern "C" void CryptographicException__ctor_m9392 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m9393 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m5864 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m9394 ();
extern "C" void CspParameters__ctor_m5829 ();
extern "C" void CspParameters__ctor_m9395 ();
extern "C" void CspParameters__ctor_m9396 ();
extern "C" void CspParameters__ctor_m9397 ();
extern "C" void CspParameters_get_Flags_m9398 ();
extern "C" void CspParameters_set_Flags_m5830 ();
extern "C" void DES__ctor_m9399 ();
extern "C" void DES__cctor_m9400 ();
extern "C" void DES_Create_m5865 ();
extern "C" void DES_Create_m9401 ();
extern "C" void DES_IsWeakKey_m9402 ();
extern "C" void DES_IsSemiWeakKey_m9403 ();
extern "C" void DES_get_Key_m9404 ();
extern "C" void DES_set_Key_m9405 ();
extern "C" void DESTransform__ctor_m9406 ();
extern "C" void DESTransform__cctor_m9407 ();
extern "C" void DESTransform_CipherFunct_m9408 ();
extern "C" void DESTransform_Permutation_m9409 ();
extern "C" void DESTransform_BSwap_m9410 ();
extern "C" void DESTransform_SetKey_m9411 ();
extern "C" void DESTransform_ProcessBlock_m9412 ();
extern "C" void DESTransform_ECB_m9413 ();
extern "C" void DESTransform_GetStrongKey_m9414 ();
extern "C" void DESCryptoServiceProvider__ctor_m9415 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m9416 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m9417 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m9418 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m9419 ();
extern "C" void DSA__ctor_m9420 ();
extern "C" void DSA_Create_m4866 ();
extern "C" void DSA_Create_m9421 ();
extern "C" void DSA_ZeroizePrivateKey_m9422 ();
extern "C" void DSA_FromXmlString_m9423 ();
extern "C" void DSA_ToXmlString_m9424 ();
extern "C" void DSACryptoServiceProvider__ctor_m9425 ();
extern "C" void DSACryptoServiceProvider__ctor_m4873 ();
extern "C" void DSACryptoServiceProvider__ctor_m9426 ();
extern "C" void DSACryptoServiceProvider__cctor_m9427 ();
extern "C" void DSACryptoServiceProvider_Finalize_m9428 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m9429 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m4865 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m9430 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m9431 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m9432 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m9433 ();
extern "C" void DSACryptoServiceProvider_Dispose_m9434 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m9435 ();
extern "C" void DSASignatureDeformatter__ctor_m9436 ();
extern "C" void DSASignatureDeformatter__ctor_m5849 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m9437 ();
extern "C" void DSASignatureDeformatter_SetKey_m9438 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m9439 ();
extern "C" void DSASignatureFormatter__ctor_m9440 ();
extern "C" void DSASignatureFormatter_CreateSignature_m9441 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m9442 ();
extern "C" void DSASignatureFormatter_SetKey_m9443 ();
extern "C" void HMAC__ctor_m9444 ();
extern "C" void HMAC_get_BlockSizeValue_m9445 ();
extern "C" void HMAC_set_BlockSizeValue_m9446 ();
extern "C" void HMAC_set_HashName_m9447 ();
extern "C" void HMAC_get_Key_m9448 ();
extern "C" void HMAC_set_Key_m9449 ();
extern "C" void HMAC_get_Block_m9450 ();
extern "C" void HMAC_KeySetup_m9451 ();
extern "C" void HMAC_Dispose_m9452 ();
extern "C" void HMAC_HashCore_m9453 ();
extern "C" void HMAC_HashFinal_m9454 ();
extern "C" void HMAC_Initialize_m9455 ();
extern "C" void HMAC_Create_m5842 ();
extern "C" void HMAC_Create_m9456 ();
extern "C" void HMACMD5__ctor_m9457 ();
extern "C" void HMACMD5__ctor_m9458 ();
extern "C" void HMACRIPEMD160__ctor_m9459 ();
extern "C" void HMACRIPEMD160__ctor_m9460 ();
extern "C" void HMACSHA1__ctor_m9461 ();
extern "C" void HMACSHA1__ctor_m9462 ();
extern "C" void HMACSHA256__ctor_m9463 ();
extern "C" void HMACSHA256__ctor_m9464 ();
extern "C" void HMACSHA384__ctor_m9465 ();
extern "C" void HMACSHA384__ctor_m9466 ();
extern "C" void HMACSHA384__cctor_m9467 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m9468 ();
extern "C" void HMACSHA512__ctor_m9469 ();
extern "C" void HMACSHA512__ctor_m9470 ();
extern "C" void HMACSHA512__cctor_m9471 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m9472 ();
extern "C" void HashAlgorithm__ctor_m5827 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m9473 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m9474 ();
extern "C" void HashAlgorithm_ComputeHash_m4947 ();
extern "C" void HashAlgorithm_ComputeHash_m5837 ();
extern "C" void HashAlgorithm_Create_m5836 ();
extern "C" void HashAlgorithm_get_Hash_m9475 ();
extern "C" void HashAlgorithm_get_HashSize_m9476 ();
extern "C" void HashAlgorithm_Dispose_m9477 ();
extern "C" void HashAlgorithm_TransformBlock_m9478 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m9479 ();
extern "C" void KeySizes__ctor_m3903 ();
extern "C" void KeySizes_get_MaxSize_m9480 ();
extern "C" void KeySizes_get_MinSize_m9481 ();
extern "C" void KeySizes_get_SkipSize_m9482 ();
extern "C" void KeySizes_IsLegal_m9483 ();
extern "C" void KeySizes_IsLegalKeySize_m9484 ();
extern "C" void KeyedHashAlgorithm__ctor_m5863 ();
extern "C" void KeyedHashAlgorithm_Finalize_m9485 ();
extern "C" void KeyedHashAlgorithm_get_Key_m9486 ();
extern "C" void KeyedHashAlgorithm_set_Key_m9487 ();
extern "C" void KeyedHashAlgorithm_Dispose_m9488 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m9489 ();
extern "C" void MACTripleDES__ctor_m9490 ();
extern "C" void MACTripleDES_Setup_m9491 ();
extern "C" void MACTripleDES_Finalize_m9492 ();
extern "C" void MACTripleDES_Dispose_m9493 ();
extern "C" void MACTripleDES_Initialize_m9494 ();
extern "C" void MACTripleDES_HashCore_m9495 ();
extern "C" void MACTripleDES_HashFinal_m9496 ();
extern "C" void MD5__ctor_m9497 ();
extern "C" void MD5_Create_m5846 ();
extern "C" void MD5_Create_m9498 ();
extern "C" void MD5CryptoServiceProvider__ctor_m9499 ();
extern "C" void MD5CryptoServiceProvider__cctor_m9500 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m9501 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m9502 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m9503 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m9504 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m9505 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m9506 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m9507 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m9508 ();
extern "C" void RC2__ctor_m9509 ();
extern "C" void RC2_Create_m5866 ();
extern "C" void RC2_Create_m9510 ();
extern "C" void RC2_get_EffectiveKeySize_m9511 ();
extern "C" void RC2_get_KeySize_m9512 ();
extern "C" void RC2_set_KeySize_m9513 ();
extern "C" void RC2CryptoServiceProvider__ctor_m9514 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m9515 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m9516 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m9517 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m9518 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m9519 ();
extern "C" void RC2Transform__ctor_m9520 ();
extern "C" void RC2Transform__cctor_m9521 ();
extern "C" void RC2Transform_ECB_m9522 ();
extern "C" void RIPEMD160__ctor_m9523 ();
extern "C" void RIPEMD160Managed__ctor_m9524 ();
extern "C" void RIPEMD160Managed_Initialize_m9525 ();
extern "C" void RIPEMD160Managed_HashCore_m9526 ();
extern "C" void RIPEMD160Managed_HashFinal_m9527 ();
extern "C" void RIPEMD160Managed_Finalize_m9528 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m9529 ();
extern "C" void RIPEMD160Managed_Compress_m9530 ();
extern "C" void RIPEMD160Managed_CompressFinal_m9531 ();
extern "C" void RIPEMD160Managed_ROL_m9532 ();
extern "C" void RIPEMD160Managed_F_m9533 ();
extern "C" void RIPEMD160Managed_G_m9534 ();
extern "C" void RIPEMD160Managed_H_m9535 ();
extern "C" void RIPEMD160Managed_I_m9536 ();
extern "C" void RIPEMD160Managed_J_m9537 ();
extern "C" void RIPEMD160Managed_FF_m9538 ();
extern "C" void RIPEMD160Managed_GG_m9539 ();
extern "C" void RIPEMD160Managed_HH_m9540 ();
extern "C" void RIPEMD160Managed_II_m9541 ();
extern "C" void RIPEMD160Managed_JJ_m9542 ();
extern "C" void RIPEMD160Managed_FFF_m9543 ();
extern "C" void RIPEMD160Managed_GGG_m9544 ();
extern "C" void RIPEMD160Managed_HHH_m9545 ();
extern "C" void RIPEMD160Managed_III_m9546 ();
extern "C" void RIPEMD160Managed_JJJ_m9547 ();
extern "C" void RNGCryptoServiceProvider__ctor_m9548 ();
extern "C" void RNGCryptoServiceProvider__cctor_m9549 ();
extern "C" void RNGCryptoServiceProvider_Check_m9550 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m9551 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m9552 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m9553 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m9554 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m9555 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m9556 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m9557 ();
extern "C" void RSA__ctor_m5832 ();
extern "C" void RSA_Create_m4863 ();
extern "C" void RSA_Create_m9558 ();
extern "C" void RSA_ZeroizePrivateKey_m9559 ();
extern "C" void RSA_FromXmlString_m9560 ();
extern "C" void RSA_ToXmlString_m9561 ();
extern "C" void RSACryptoServiceProvider__ctor_m9562 ();
extern "C" void RSACryptoServiceProvider__ctor_m5831 ();
extern "C" void RSACryptoServiceProvider__ctor_m4874 ();
extern "C" void RSACryptoServiceProvider__cctor_m9563 ();
extern "C" void RSACryptoServiceProvider_Common_m9564 ();
extern "C" void RSACryptoServiceProvider_Finalize_m9565 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m9566 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m4861 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m9567 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m9568 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m9569 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m9570 ();
extern "C" void RSACryptoServiceProvider_Dispose_m9571 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m9572 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m5891 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m9573 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m9574 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m9575 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m5850 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m9576 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m9577 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m9578 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m9579 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m9580 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m9581 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m9582 ();
extern "C" void RandomNumberGenerator__ctor_m9583 ();
extern "C" void RandomNumberGenerator_Create_m3891 ();
extern "C" void RandomNumberGenerator_Create_m9584 ();
extern "C" void Rijndael__ctor_m9585 ();
extern "C" void Rijndael_Create_m5868 ();
extern "C" void Rijndael_Create_m9586 ();
extern "C" void RijndaelManaged__ctor_m9587 ();
extern "C" void RijndaelManaged_GenerateIV_m9588 ();
extern "C" void RijndaelManaged_GenerateKey_m9589 ();
extern "C" void RijndaelManaged_CreateDecryptor_m9590 ();
extern "C" void RijndaelManaged_CreateEncryptor_m9591 ();
extern "C" void RijndaelTransform__ctor_m9592 ();
extern "C" void RijndaelTransform__cctor_m9593 ();
extern "C" void RijndaelTransform_Clear_m9594 ();
extern "C" void RijndaelTransform_ECB_m9595 ();
extern "C" void RijndaelTransform_SubByte_m9596 ();
extern "C" void RijndaelTransform_Encrypt128_m9597 ();
extern "C" void RijndaelTransform_Encrypt192_m9598 ();
extern "C" void RijndaelTransform_Encrypt256_m9599 ();
extern "C" void RijndaelTransform_Decrypt128_m9600 ();
extern "C" void RijndaelTransform_Decrypt192_m9601 ();
extern "C" void RijndaelTransform_Decrypt256_m9602 ();
extern "C" void RijndaelManagedTransform__ctor_m9603 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m9604 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m9605 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m9606 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m9607 ();
extern "C" void SHA1__ctor_m9608 ();
extern "C" void SHA1_Create_m4946 ();
extern "C" void SHA1_Create_m9609 ();
extern "C" void SHA1Internal__ctor_m9610 ();
extern "C" void SHA1Internal_HashCore_m9611 ();
extern "C" void SHA1Internal_HashFinal_m9612 ();
extern "C" void SHA1Internal_Initialize_m9613 ();
extern "C" void SHA1Internal_ProcessBlock_m9614 ();
extern "C" void SHA1Internal_InitialiseBuff_m9615 ();
extern "C" void SHA1Internal_FillBuff_m9616 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m9617 ();
extern "C" void SHA1Internal_AddLength_m9618 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m9619 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m9620 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m9621 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m9622 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m9623 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m9624 ();
extern "C" void SHA1Managed__ctor_m9625 ();
extern "C" void SHA1Managed_HashCore_m9626 ();
extern "C" void SHA1Managed_HashFinal_m9627 ();
extern "C" void SHA1Managed_Initialize_m9628 ();
extern "C" void SHA256__ctor_m9629 ();
extern "C" void SHA256_Create_m5847 ();
extern "C" void SHA256_Create_m9630 ();
extern "C" void SHA256Managed__ctor_m9631 ();
extern "C" void SHA256Managed_HashCore_m9632 ();
extern "C" void SHA256Managed_HashFinal_m9633 ();
extern "C" void SHA256Managed_Initialize_m9634 ();
extern "C" void SHA256Managed_ProcessBlock_m9635 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m9636 ();
extern "C" void SHA256Managed_AddLength_m9637 ();
extern "C" void SHA384__ctor_m9638 ();
extern "C" void SHA384Managed__ctor_m9639 ();
extern "C" void SHA384Managed_Initialize_m9640 ();
extern "C" void SHA384Managed_Initialize_m9641 ();
extern "C" void SHA384Managed_HashCore_m9642 ();
extern "C" void SHA384Managed_HashFinal_m9643 ();
extern "C" void SHA384Managed_update_m9644 ();
extern "C" void SHA384Managed_processWord_m9645 ();
extern "C" void SHA384Managed_unpackWord_m9646 ();
extern "C" void SHA384Managed_adjustByteCounts_m9647 ();
extern "C" void SHA384Managed_processLength_m9648 ();
extern "C" void SHA384Managed_processBlock_m9649 ();
extern "C" void SHA512__ctor_m9650 ();
extern "C" void SHA512Managed__ctor_m9651 ();
extern "C" void SHA512Managed_Initialize_m9652 ();
extern "C" void SHA512Managed_Initialize_m9653 ();
extern "C" void SHA512Managed_HashCore_m9654 ();
extern "C" void SHA512Managed_HashFinal_m9655 ();
extern "C" void SHA512Managed_update_m9656 ();
extern "C" void SHA512Managed_processWord_m9657 ();
extern "C" void SHA512Managed_unpackWord_m9658 ();
extern "C" void SHA512Managed_adjustByteCounts_m9659 ();
extern "C" void SHA512Managed_processLength_m9660 ();
extern "C" void SHA512Managed_processBlock_m9661 ();
extern "C" void SHA512Managed_rotateRight_m9662 ();
extern "C" void SHA512Managed_Ch_m9663 ();
extern "C" void SHA512Managed_Maj_m9664 ();
extern "C" void SHA512Managed_Sum0_m9665 ();
extern "C" void SHA512Managed_Sum1_m9666 ();
extern "C" void SHA512Managed_Sigma0_m9667 ();
extern "C" void SHA512Managed_Sigma1_m9668 ();
extern "C" void SHAConstants__cctor_m9669 ();
extern "C" void SignatureDescription__ctor_m9670 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m9671 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m9672 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m9673 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m9674 ();
extern "C" void DSASignatureDescription__ctor_m9675 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m9676 ();
extern "C" void SymmetricAlgorithm__ctor_m3902 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m9677 ();
extern "C" void SymmetricAlgorithm_Finalize_m5825 ();
extern "C" void SymmetricAlgorithm_Clear_m5841 ();
extern "C" void SymmetricAlgorithm_Dispose_m3910 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m9678 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m9679 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m9680 ();
extern "C" void SymmetricAlgorithm_get_IV_m3904 ();
extern "C" void SymmetricAlgorithm_set_IV_m3905 ();
extern "C" void SymmetricAlgorithm_get_Key_m3906 ();
extern "C" void SymmetricAlgorithm_set_Key_m3907 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m3908 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m3909 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m9681 ();
extern "C" void SymmetricAlgorithm_get_Mode_m9682 ();
extern "C" void SymmetricAlgorithm_set_Mode_m9683 ();
extern "C" void SymmetricAlgorithm_get_Padding_m9684 ();
extern "C" void SymmetricAlgorithm_set_Padding_m9685 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m9686 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m9687 ();
extern "C" void SymmetricAlgorithm_Create_m5840 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m9688 ();
extern "C" void ToBase64Transform_Finalize_m9689 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m9690 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m9691 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m9692 ();
extern "C" void ToBase64Transform_Dispose_m9693 ();
extern "C" void ToBase64Transform_TransformBlock_m9694 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m9695 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m9696 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m9697 ();
extern "C" void TripleDES__ctor_m9698 ();
extern "C" void TripleDES_get_Key_m9699 ();
extern "C" void TripleDES_set_Key_m9700 ();
extern "C" void TripleDES_IsWeakKey_m9701 ();
extern "C" void TripleDES_Create_m5867 ();
extern "C" void TripleDES_Create_m9702 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m9703 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m9704 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m9705 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m9706 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m9707 ();
extern "C" void TripleDESTransform__ctor_m9708 ();
extern "C" void TripleDESTransform_ECB_m9709 ();
extern "C" void TripleDESTransform_GetStrongKey_m9710 ();
extern "C" void SecurityPermission__ctor_m9711 ();
extern "C" void SecurityPermission_set_Flags_m9712 ();
extern "C" void SecurityPermission_IsUnrestricted_m9713 ();
extern "C" void SecurityPermission_IsSubsetOf_m9714 ();
extern "C" void SecurityPermission_ToXml_m9715 ();
extern "C" void SecurityPermission_IsEmpty_m9716 ();
extern "C" void SecurityPermission_Cast_m9717 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m9718 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m9719 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m9720 ();
extern "C" void ApplicationTrust__ctor_m9721 ();
extern "C" void EvidenceEnumerator__ctor_m9722 ();
extern "C" void EvidenceEnumerator_MoveNext_m9723 ();
extern "C" void EvidenceEnumerator_Reset_m9724 ();
extern "C" void EvidenceEnumerator_get_Current_m9725 ();
extern "C" void Evidence__ctor_m9726 ();
extern "C" void Evidence_get_Count_m9727 ();
extern "C" void Evidence_get_IsSynchronized_m9728 ();
extern "C" void Evidence_get_SyncRoot_m9729 ();
extern "C" void Evidence_get_HostEvidenceList_m9730 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m9731 ();
extern "C" void Evidence_CopyTo_m9732 ();
extern "C" void Evidence_Equals_m9733 ();
extern "C" void Evidence_GetEnumerator_m9734 ();
extern "C" void Evidence_GetHashCode_m9735 ();
extern "C" void Hash__ctor_m9736 ();
extern "C" void Hash__ctor_m9737 ();
extern "C" void Hash_GetObjectData_m9738 ();
extern "C" void Hash_ToString_m9739 ();
extern "C" void Hash_GetData_m9740 ();
extern "C" void StrongName_get_Name_m9741 ();
extern "C" void StrongName_get_PublicKey_m9742 ();
extern "C" void StrongName_get_Version_m9743 ();
extern "C" void StrongName_Equals_m9744 ();
extern "C" void StrongName_GetHashCode_m9745 ();
extern "C" void StrongName_ToString_m9746 ();
extern "C" void WindowsIdentity__ctor_m9747 ();
extern "C" void WindowsIdentity__cctor_m9748 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9749 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9750 ();
extern "C" void WindowsIdentity_Dispose_m9751 ();
extern "C" void WindowsIdentity_GetCurrentToken_m9752 ();
extern "C" void WindowsIdentity_GetTokenName_m9753 ();
extern "C" void CodeAccessPermission__ctor_m9754 ();
extern "C" void CodeAccessPermission_Equals_m9755 ();
extern "C" void CodeAccessPermission_GetHashCode_m9756 ();
extern "C" void CodeAccessPermission_ToString_m9757 ();
extern "C" void CodeAccessPermission_Element_m9758 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m9759 ();
extern "C" void PermissionSet__ctor_m9760 ();
extern "C" void PermissionSet__ctor_m9761 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m9762 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m9763 ();
extern "C" void SecurityContext__ctor_m9764 ();
extern "C" void SecurityContext__ctor_m9765 ();
extern "C" void SecurityContext_Capture_m9766 ();
extern "C" void SecurityContext_get_FlowSuppressed_m9767 ();
extern "C" void SecurityContext_get_CompressedStack_m9768 ();
extern "C" void SecurityAttribute__ctor_m9769 ();
extern "C" void SecurityAttribute_get_Name_m9770 ();
extern "C" void SecurityAttribute_get_Value_m9771 ();
extern "C" void SecurityElement__ctor_m9772 ();
extern "C" void SecurityElement__ctor_m9773 ();
extern "C" void SecurityElement__cctor_m9774 ();
extern "C" void SecurityElement_get_Children_m9775 ();
extern "C" void SecurityElement_get_Tag_m9776 ();
extern "C" void SecurityElement_set_Text_m9777 ();
extern "C" void SecurityElement_AddAttribute_m9778 ();
extern "C" void SecurityElement_AddChild_m9779 ();
extern "C" void SecurityElement_Escape_m9780 ();
extern "C" void SecurityElement_Unescape_m9781 ();
extern "C" void SecurityElement_IsValidAttributeName_m9782 ();
extern "C" void SecurityElement_IsValidAttributeValue_m9783 ();
extern "C" void SecurityElement_IsValidTag_m9784 ();
extern "C" void SecurityElement_IsValidText_m9785 ();
extern "C" void SecurityElement_SearchForChildByTag_m9786 ();
extern "C" void SecurityElement_ToString_m9787 ();
extern "C" void SecurityElement_ToXml_m9788 ();
extern "C" void SecurityElement_GetAttribute_m9789 ();
extern "C" void SecurityException__ctor_m9790 ();
extern "C" void SecurityException__ctor_m9791 ();
extern "C" void SecurityException__ctor_m9792 ();
extern "C" void SecurityException_get_Demanded_m9793 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m9794 ();
extern "C" void SecurityException_get_PermissionState_m9795 ();
extern "C" void SecurityException_get_PermissionType_m9796 ();
extern "C" void SecurityException_get_GrantedSet_m9797 ();
extern "C" void SecurityException_get_RefusedSet_m9798 ();
extern "C" void SecurityException_GetObjectData_m9799 ();
extern "C" void SecurityException_ToString_m9800 ();
extern "C" void SecurityFrame__ctor_m9801 ();
extern "C" void SecurityFrame__GetSecurityStack_m9802 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m9803 ();
extern "C" void SecurityFrame_get_Assembly_m9804 ();
extern "C" void SecurityFrame_get_Domain_m9805 ();
extern "C" void SecurityFrame_ToString_m9806 ();
extern "C" void SecurityFrame_GetStack_m9807 ();
extern "C" void SecurityManager__cctor_m9808 ();
extern "C" void SecurityManager_get_SecurityEnabled_m9809 ();
extern "C" void SecurityManager_Decode_m9810 ();
extern "C" void SecurityManager_Decode_m9811 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m9812 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m9813 ();
extern "C" void UnverifiableCodeAttribute__ctor_m9814 ();
extern "C" void ASCIIEncoding__ctor_m9815 ();
extern "C" void ASCIIEncoding_GetByteCount_m9816 ();
extern "C" void ASCIIEncoding_GetByteCount_m9817 ();
extern "C" void ASCIIEncoding_GetBytes_m9818 ();
extern "C" void ASCIIEncoding_GetBytes_m9819 ();
extern "C" void ASCIIEncoding_GetBytes_m9820 ();
extern "C" void ASCIIEncoding_GetBytes_m9821 ();
extern "C" void ASCIIEncoding_GetCharCount_m9822 ();
extern "C" void ASCIIEncoding_GetChars_m9823 ();
extern "C" void ASCIIEncoding_GetChars_m9824 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m9825 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m9826 ();
extern "C" void ASCIIEncoding_GetString_m9827 ();
extern "C" void ASCIIEncoding_GetBytes_m9828 ();
extern "C" void ASCIIEncoding_GetByteCount_m9829 ();
extern "C" void ASCIIEncoding_GetDecoder_m9830 ();
extern "C" void Decoder__ctor_m9831 ();
extern "C" void Decoder_set_Fallback_m9832 ();
extern "C" void Decoder_get_FallbackBuffer_m9833 ();
extern "C" void DecoderExceptionFallback__ctor_m9834 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m9835 ();
extern "C" void DecoderExceptionFallback_Equals_m9836 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m9837 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m9838 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m9839 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m9840 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m9841 ();
extern "C" void DecoderFallback__ctor_m9842 ();
extern "C" void DecoderFallback__cctor_m9843 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m9844 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m9845 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m9846 ();
extern "C" void DecoderFallbackBuffer__ctor_m9847 ();
extern "C" void DecoderFallbackBuffer_Reset_m9848 ();
extern "C" void DecoderFallbackException__ctor_m9849 ();
extern "C" void DecoderFallbackException__ctor_m9850 ();
extern "C" void DecoderFallbackException__ctor_m9851 ();
extern "C" void DecoderReplacementFallback__ctor_m9852 ();
extern "C" void DecoderReplacementFallback__ctor_m9853 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m9854 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m9855 ();
extern "C" void DecoderReplacementFallback_Equals_m9856 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m9857 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m9858 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m9859 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m9860 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m9861 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m9862 ();
extern "C" void EncoderExceptionFallback__ctor_m9863 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m9864 ();
extern "C" void EncoderExceptionFallback_Equals_m9865 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m9866 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m9867 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m9868 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9869 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9870 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m9871 ();
extern "C" void EncoderFallback__ctor_m9872 ();
extern "C" void EncoderFallback__cctor_m9873 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m9874 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m9875 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m9876 ();
extern "C" void EncoderFallbackBuffer__ctor_m9877 ();
extern "C" void EncoderFallbackException__ctor_m9878 ();
extern "C" void EncoderFallbackException__ctor_m9879 ();
extern "C" void EncoderFallbackException__ctor_m9880 ();
extern "C" void EncoderFallbackException__ctor_m9881 ();
extern "C" void EncoderReplacementFallback__ctor_m9882 ();
extern "C" void EncoderReplacementFallback__ctor_m9883 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m9884 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m9885 ();
extern "C" void EncoderReplacementFallback_Equals_m9886 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m9887 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m9888 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m9889 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9890 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9891 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9892 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m9893 ();
extern "C" void ForwardingDecoder__ctor_m9894 ();
extern "C" void ForwardingDecoder_GetChars_m9895 ();
extern "C" void Encoding__ctor_m9896 ();
extern "C" void Encoding__ctor_m9897 ();
extern "C" void Encoding__cctor_m9898 ();
extern "C" void Encoding___m9899 ();
extern "C" void Encoding_get_IsReadOnly_m9900 ();
extern "C" void Encoding_get_DecoderFallback_m9901 ();
extern "C" void Encoding_set_DecoderFallback_m9902 ();
extern "C" void Encoding_get_EncoderFallback_m9903 ();
extern "C" void Encoding_SetFallbackInternal_m9904 ();
extern "C" void Encoding_Equals_m9905 ();
extern "C" void Encoding_GetByteCount_m9906 ();
extern "C" void Encoding_GetByteCount_m9907 ();
extern "C" void Encoding_GetBytes_m9908 ();
extern "C" void Encoding_GetBytes_m9909 ();
extern "C" void Encoding_GetBytes_m9910 ();
extern "C" void Encoding_GetBytes_m9911 ();
extern "C" void Encoding_GetChars_m9912 ();
extern "C" void Encoding_GetDecoder_m9913 ();
extern "C" void Encoding_InvokeI18N_m9914 ();
extern "C" void Encoding_GetEncoding_m9915 ();
extern "C" void Encoding_Clone_m9916 ();
extern "C" void Encoding_GetEncoding_m9917 ();
extern "C" void Encoding_GetHashCode_m9918 ();
extern "C" void Encoding_GetPreamble_m9919 ();
extern "C" void Encoding_GetString_m9920 ();
extern "C" void Encoding_GetString_m9921 ();
extern "C" void Encoding_get_ASCII_m4949 ();
extern "C" void Encoding_get_BigEndianUnicode_m5838 ();
extern "C" void Encoding_InternalCodePage_m9922 ();
extern "C" void Encoding_get_Default_m9923 ();
extern "C" void Encoding_get_ISOLatin1_m9924 ();
extern "C" void Encoding_get_UTF7_m5843 ();
extern "C" void Encoding_get_UTF8_m585 ();
extern "C" void Encoding_get_UTF8Unmarked_m9925 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m9926 ();
extern "C" void Encoding_get_Unicode_m9927 ();
extern "C" void Encoding_get_UTF32_m9928 ();
extern "C" void Encoding_get_BigEndianUTF32_m9929 ();
extern "C" void Encoding_GetByteCount_m9930 ();
extern "C" void Encoding_GetBytes_m9931 ();
extern "C" void Latin1Encoding__ctor_m9932 ();
extern "C" void Latin1Encoding_GetByteCount_m9933 ();
extern "C" void Latin1Encoding_GetByteCount_m9934 ();
extern "C" void Latin1Encoding_GetBytes_m9935 ();
extern "C" void Latin1Encoding_GetBytes_m9936 ();
extern "C" void Latin1Encoding_GetBytes_m9937 ();
extern "C" void Latin1Encoding_GetBytes_m9938 ();
extern "C" void Latin1Encoding_GetCharCount_m9939 ();
extern "C" void Latin1Encoding_GetChars_m9940 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m9941 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m9942 ();
extern "C" void Latin1Encoding_GetString_m9943 ();
extern "C" void Latin1Encoding_GetString_m9944 ();
extern "C" void StringBuilder__ctor_m9945 ();
extern "C" void StringBuilder__ctor_m9946 ();
extern "C" void StringBuilder__ctor_m3546 ();
extern "C" void StringBuilder__ctor_m2205 ();
extern "C" void StringBuilder__ctor_m3588 ();
extern "C" void StringBuilder__ctor_m4833 ();
extern "C" void StringBuilder__ctor_m9947 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9948 ();
extern "C" void StringBuilder_get_Capacity_m9949 ();
extern "C" void StringBuilder_set_Capacity_m9950 ();
extern "C" void StringBuilder_get_Length_m4939 ();
extern "C" void StringBuilder_set_Length_m4976 ();
extern "C" void StringBuilder_get_Chars_m9951 ();
extern "C" void StringBuilder_set_Chars_m9952 ();
extern "C" void StringBuilder_ToString_m2209 ();
extern "C" void StringBuilder_ToString_m9953 ();
extern "C" void StringBuilder_Remove_m9954 ();
extern "C" void StringBuilder_Replace_m9955 ();
extern "C" void StringBuilder_Replace_m9956 ();
extern "C" void StringBuilder_Append_m2208 ();
extern "C" void StringBuilder_Append_m4886 ();
extern "C" void StringBuilder_Append_m4850 ();
extern "C" void StringBuilder_Append_m3829 ();
extern "C" void StringBuilder_Append_m4834 ();
extern "C" void StringBuilder_Append_m9957 ();
extern "C" void StringBuilder_Append_m9958 ();
extern "C" void StringBuilder_Append_m4952 ();
extern "C" void StringBuilder_AppendLine_m3548 ();
extern "C" void StringBuilder_AppendLine_m3547 ();
extern "C" void StringBuilder_AppendFormat_m5819 ();
extern "C" void StringBuilder_AppendFormat_m9959 ();
extern "C" void StringBuilder_AppendFormat_m4849 ();
extern "C" void StringBuilder_AppendFormat_m4906 ();
extern "C" void StringBuilder_AppendFormat_m4909 ();
extern "C" void StringBuilder_Insert_m9960 ();
extern "C" void StringBuilder_Insert_m9961 ();
extern "C" void StringBuilder_Insert_m9962 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m9963 ();
extern "C" void UTF32Decoder__ctor_m9964 ();
extern "C" void UTF32Decoder_GetChars_m9965 ();
extern "C" void UTF32Encoding__ctor_m9966 ();
extern "C" void UTF32Encoding__ctor_m9967 ();
extern "C" void UTF32Encoding__ctor_m9968 ();
extern "C" void UTF32Encoding_GetByteCount_m9969 ();
extern "C" void UTF32Encoding_GetBytes_m9970 ();
extern "C" void UTF32Encoding_GetCharCount_m9971 ();
extern "C" void UTF32Encoding_GetChars_m9972 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m9973 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m9974 ();
extern "C" void UTF32Encoding_GetDecoder_m9975 ();
extern "C" void UTF32Encoding_GetPreamble_m9976 ();
extern "C" void UTF32Encoding_Equals_m9977 ();
extern "C" void UTF32Encoding_GetHashCode_m9978 ();
extern "C" void UTF32Encoding_GetByteCount_m9979 ();
extern "C" void UTF32Encoding_GetByteCount_m9980 ();
extern "C" void UTF32Encoding_GetBytes_m9981 ();
extern "C" void UTF32Encoding_GetBytes_m9982 ();
extern "C" void UTF32Encoding_GetString_m9983 ();
extern "C" void UTF7Decoder__ctor_m9984 ();
extern "C" void UTF7Decoder_GetChars_m9985 ();
extern "C" void UTF7Encoding__ctor_m9986 ();
extern "C" void UTF7Encoding__ctor_m9987 ();
extern "C" void UTF7Encoding__cctor_m9988 ();
extern "C" void UTF7Encoding_GetHashCode_m9989 ();
extern "C" void UTF7Encoding_Equals_m9990 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m9991 ();
extern "C" void UTF7Encoding_GetByteCount_m9992 ();
extern "C" void UTF7Encoding_InternalGetBytes_m9993 ();
extern "C" void UTF7Encoding_GetBytes_m9994 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m9995 ();
extern "C" void UTF7Encoding_GetCharCount_m9996 ();
extern "C" void UTF7Encoding_InternalGetChars_m9997 ();
extern "C" void UTF7Encoding_GetChars_m9998 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m9999 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m10000 ();
extern "C" void UTF7Encoding_GetDecoder_m10001 ();
extern "C" void UTF7Encoding_GetByteCount_m10002 ();
extern "C" void UTF7Encoding_GetByteCount_m10003 ();
extern "C" void UTF7Encoding_GetBytes_m10004 ();
extern "C" void UTF7Encoding_GetBytes_m10005 ();
extern "C" void UTF7Encoding_GetString_m10006 ();
extern "C" void UTF8Decoder__ctor_m10007 ();
extern "C" void UTF8Decoder_GetChars_m10008 ();
extern "C" void UTF8Encoding__ctor_m10009 ();
extern "C" void UTF8Encoding__ctor_m10010 ();
extern "C" void UTF8Encoding__ctor_m10011 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m10012 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m10013 ();
extern "C" void UTF8Encoding_GetByteCount_m10014 ();
extern "C" void UTF8Encoding_GetByteCount_m10015 ();
extern "C" void UTF8Encoding_InternalGetBytes_m10016 ();
extern "C" void UTF8Encoding_InternalGetBytes_m10017 ();
extern "C" void UTF8Encoding_GetBytes_m10018 ();
extern "C" void UTF8Encoding_GetBytes_m10019 ();
extern "C" void UTF8Encoding_GetBytes_m10020 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m10021 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m10022 ();
extern "C" void UTF8Encoding_Fallback_m10023 ();
extern "C" void UTF8Encoding_Fallback_m10024 ();
extern "C" void UTF8Encoding_GetCharCount_m10025 ();
extern "C" void UTF8Encoding_InternalGetChars_m10026 ();
extern "C" void UTF8Encoding_InternalGetChars_m10027 ();
extern "C" void UTF8Encoding_GetChars_m10028 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m10029 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m10030 ();
extern "C" void UTF8Encoding_GetDecoder_m10031 ();
extern "C" void UTF8Encoding_GetPreamble_m10032 ();
extern "C" void UTF8Encoding_Equals_m10033 ();
extern "C" void UTF8Encoding_GetHashCode_m10034 ();
extern "C" void UTF8Encoding_GetByteCount_m10035 ();
extern "C" void UTF8Encoding_GetString_m10036 ();
extern "C" void UnicodeDecoder__ctor_m10037 ();
extern "C" void UnicodeDecoder_GetChars_m10038 ();
extern "C" void UnicodeEncoding__ctor_m10039 ();
extern "C" void UnicodeEncoding__ctor_m10040 ();
extern "C" void UnicodeEncoding__ctor_m10041 ();
extern "C" void UnicodeEncoding_GetByteCount_m10042 ();
extern "C" void UnicodeEncoding_GetByteCount_m10043 ();
extern "C" void UnicodeEncoding_GetByteCount_m10044 ();
extern "C" void UnicodeEncoding_GetBytes_m10045 ();
extern "C" void UnicodeEncoding_GetBytes_m10046 ();
extern "C" void UnicodeEncoding_GetBytes_m10047 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m10048 ();
extern "C" void UnicodeEncoding_GetCharCount_m10049 ();
extern "C" void UnicodeEncoding_GetChars_m10050 ();
extern "C" void UnicodeEncoding_GetString_m10051 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m10052 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m10053 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m10054 ();
extern "C" void UnicodeEncoding_GetDecoder_m10055 ();
extern "C" void UnicodeEncoding_GetPreamble_m10056 ();
extern "C" void UnicodeEncoding_Equals_m10057 ();
extern "C" void UnicodeEncoding_GetHashCode_m10058 ();
extern "C" void UnicodeEncoding_CopyChars_m10059 ();
extern "C" void CompressedStack__ctor_m10060 ();
extern "C" void CompressedStack__ctor_m10061 ();
extern "C" void CompressedStack_CreateCopy_m10062 ();
extern "C" void CompressedStack_Capture_m10063 ();
extern "C" void CompressedStack_GetObjectData_m10064 ();
extern "C" void CompressedStack_IsEmpty_m10065 ();
extern "C" void EventWaitHandle__ctor_m10066 ();
extern "C" void EventWaitHandle_IsManualReset_m10067 ();
extern "C" void EventWaitHandle_Reset_m5878 ();
extern "C" void EventWaitHandle_Set_m5876 ();
extern "C" void ExecutionContext__ctor_m10068 ();
extern "C" void ExecutionContext__ctor_m10069 ();
extern "C" void ExecutionContext__ctor_m10070 ();
extern "C" void ExecutionContext_Capture_m10071 ();
extern "C" void ExecutionContext_GetObjectData_m10072 ();
extern "C" void ExecutionContext_get_SecurityContext_m10073 ();
extern "C" void ExecutionContext_set_SecurityContext_m10074 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m10075 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m10076 ();
extern "C" void Interlocked_CompareExchange_m3832 ();
extern "C" void ManualResetEvent__ctor_m5875 ();
extern "C" void Monitor_Enter_m572 ();
extern "C" void Monitor_Exit_m575 ();
extern "C" void Monitor_Monitor_pulse_m10077 ();
extern "C" void Monitor_Monitor_test_synchronised_m10078 ();
extern "C" void Monitor_Pulse_m10079 ();
extern "C" void Monitor_Monitor_wait_m10080 ();
extern "C" void Monitor_Wait_m10081 ();
extern "C" void Mutex__ctor_m10082 ();
extern "C" void Mutex_CreateMutex_internal_m10083 ();
extern "C" void Mutex_ReleaseMutex_internal_m10084 ();
extern "C" void Mutex_ReleaseMutex_m10085 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m10086 ();
extern "C" void NativeEventCalls_SetEvent_internal_m10087 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m10088 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m10089 ();
extern "C" void SynchronizationLockException__ctor_m10090 ();
extern "C" void SynchronizationLockException__ctor_m10091 ();
extern "C" void SynchronizationLockException__ctor_m10092 ();
extern "C" void Thread__ctor_m10093 ();
extern "C" void Thread__cctor_m10094 ();
extern "C" void Thread_get_CurrentContext_m10095 ();
extern "C" void Thread_CurrentThread_internal_m10096 ();
extern "C" void Thread_get_CurrentThread_m10097 ();
extern "C" void Thread_FreeLocalSlotValues_m10098 ();
extern "C" void Thread_GetDomainID_m10099 ();
extern "C" void Thread_Thread_internal_m10100 ();
extern "C" void Thread_Thread_init_m10101 ();
extern "C" void Thread_GetCachedCurrentCulture_m10102 ();
extern "C" void Thread_GetSerializedCurrentCulture_m10103 ();
extern "C" void Thread_SetCachedCurrentCulture_m10104 ();
extern "C" void Thread_GetCachedCurrentUICulture_m10105 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m10106 ();
extern "C" void Thread_SetCachedCurrentUICulture_m10107 ();
extern "C" void Thread_get_CurrentCulture_m10108 ();
extern "C" void Thread_get_CurrentUICulture_m10109 ();
extern "C" void Thread_set_IsBackground_m10110 ();
extern "C" void Thread_SetName_internal_m10111 ();
extern "C" void Thread_set_Name_m10112 ();
extern "C" void Thread_Start_m10113 ();
extern "C" void Thread_Thread_free_internal_m10114 ();
extern "C" void Thread_Finalize_m10115 ();
extern "C" void Thread_SetState_m10116 ();
extern "C" void Thread_ClrState_m10117 ();
extern "C" void Thread_GetNewManagedId_m10118 ();
extern "C" void Thread_GetNewManagedId_internal_m10119 ();
extern "C" void Thread_get_ExecutionContext_m10120 ();
extern "C" void Thread_get_ManagedThreadId_m10121 ();
extern "C" void Thread_GetHashCode_m10122 ();
extern "C" void Thread_GetCompressedStack_m10123 ();
extern "C" void ThreadAbortException__ctor_m10124 ();
extern "C" void ThreadAbortException__ctor_m10125 ();
extern "C" void ThreadInterruptedException__ctor_m10126 ();
extern "C" void ThreadInterruptedException__ctor_m10127 ();
extern "C" void ThreadPool_QueueUserWorkItem_m10128 ();
extern "C" void ThreadStateException__ctor_m10129 ();
extern "C" void ThreadStateException__ctor_m10130 ();
extern "C" void TimerComparer__ctor_m10131 ();
extern "C" void TimerComparer_Compare_m10132 ();
extern "C" void Scheduler__ctor_m10133 ();
extern "C" void Scheduler__cctor_m10134 ();
extern "C" void Scheduler_get_Instance_m10135 ();
extern "C" void Scheduler_Remove_m10136 ();
extern "C" void Scheduler_Change_m10137 ();
extern "C" void Scheduler_Add_m10138 ();
extern "C" void Scheduler_InternalRemove_m10139 ();
extern "C" void Scheduler_SchedulerThread_m10140 ();
extern "C" void Scheduler_ShrinkIfNeeded_m10141 ();
extern "C" void Timer__cctor_m10142 ();
extern "C" void Timer_Change_m10143 ();
extern "C" void Timer_Dispose_m10144 ();
extern "C" void Timer_Change_m10145 ();
extern "C" void WaitHandle__ctor_m10146 ();
extern "C" void WaitHandle__cctor_m10147 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m10148 ();
extern "C" void WaitHandle_get_Handle_m10149 ();
extern "C" void WaitHandle_set_Handle_m10150 ();
extern "C" void WaitHandle_WaitOne_internal_m10151 ();
extern "C" void WaitHandle_Dispose_m10152 ();
extern "C" void WaitHandle_WaitOne_m10153 ();
extern "C" void WaitHandle_WaitOne_m10154 ();
extern "C" void WaitHandle_CheckDisposed_m10155 ();
extern "C" void WaitHandle_Finalize_m10156 ();
extern "C" void AccessViolationException__ctor_m10157 ();
extern "C" void AccessViolationException__ctor_m10158 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m10159 ();
extern "C" void ActivationContext_Finalize_m10160 ();
extern "C" void ActivationContext_Dispose_m10161 ();
extern "C" void ActivationContext_Dispose_m10162 ();
extern "C" void Activator_CreateInstance_m10163 ();
extern "C" void Activator_CreateInstance_m10164 ();
extern "C" void Activator_CreateInstance_m10165 ();
extern "C" void Activator_CreateInstance_m10166 ();
extern "C" void Activator_CreateInstance_m4860 ();
extern "C" void Activator_CheckType_m10167 ();
extern "C" void Activator_CheckAbstractType_m10168 ();
extern "C" void Activator_CreateInstanceInternal_m10169 ();
extern "C" void AppDomain_add_UnhandledException_m2141 ();
extern "C" void AppDomain_remove_UnhandledException_m10170 ();
extern "C" void AppDomain_getFriendlyName_m10171 ();
extern "C" void AppDomain_getCurDomain_m10172 ();
extern "C" void AppDomain_get_CurrentDomain_m2139 ();
extern "C" void AppDomain_LoadAssembly_m10173 ();
extern "C" void AppDomain_Load_m10174 ();
extern "C" void AppDomain_Load_m10175 ();
extern "C" void AppDomain_InternalSetContext_m10176 ();
extern "C" void AppDomain_InternalGetContext_m10177 ();
extern "C" void AppDomain_InternalGetDefaultContext_m10178 ();
extern "C" void AppDomain_InternalGetProcessGuid_m10179 ();
extern "C" void AppDomain_GetProcessGuid_m10180 ();
extern "C" void AppDomain_ToString_m10181 ();
extern "C" void AppDomain_DoTypeResolve_m10182 ();
extern "C" void AppDomainSetup__ctor_m10183 ();
extern "C" void ApplicationException__ctor_m10184 ();
extern "C" void ApplicationException__ctor_m10185 ();
extern "C" void ApplicationException__ctor_m10186 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m10187 ();
extern "C" void ApplicationIdentity_ToString_m10188 ();
extern "C" void ArgumentException__ctor_m10189 ();
extern "C" void ArgumentException__ctor_m2137 ();
extern "C" void ArgumentException__ctor_m4918 ();
extern "C" void ArgumentException__ctor_m3899 ();
extern "C" void ArgumentException__ctor_m10190 ();
extern "C" void ArgumentException__ctor_m10191 ();
extern "C" void ArgumentException_get_ParamName_m10192 ();
extern "C" void ArgumentException_get_Message_m10193 ();
extern "C" void ArgumentException_GetObjectData_m10194 ();
extern "C" void ArgumentNullException__ctor_m10195 ();
extern "C" void ArgumentNullException__ctor_m2226 ();
extern "C" void ArgumentNullException__ctor_m4820 ();
extern "C" void ArgumentNullException__ctor_m10196 ();
extern "C" void ArgumentOutOfRangeException__ctor_m4951 ();
extern "C" void ArgumentOutOfRangeException__ctor_m4823 ();
extern "C" void ArgumentOutOfRangeException__ctor_m3898 ();
extern "C" void ArgumentOutOfRangeException__ctor_m10197 ();
extern "C" void ArgumentOutOfRangeException__ctor_m10198 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m10199 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m10200 ();
extern "C" void ArithmeticException__ctor_m10201 ();
extern "C" void ArithmeticException__ctor_m5818 ();
extern "C" void ArithmeticException__ctor_m10202 ();
extern "C" void ArrayTypeMismatchException__ctor_m10203 ();
extern "C" void ArrayTypeMismatchException__ctor_m10204 ();
extern "C" void ArrayTypeMismatchException__ctor_m10205 ();
extern "C" void BitConverter__cctor_m10206 ();
extern "C" void BitConverter_AmILittleEndian_m10207 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m10208 ();
extern "C" void BitConverter_DoubleToInt64Bits_m10209 ();
extern "C" void BitConverter_GetBytes_m10210 ();
extern "C" void BitConverter_GetBytes_m10211 ();
extern "C" void BitConverter_PutBytes_m10212 ();
extern "C" void BitConverter_ToInt64_m10213 ();
extern "C" void BitConverter_ToString_m5873 ();
extern "C" void BitConverter_ToString_m10214 ();
extern "C" void Buffer_ByteLength_m10215 ();
extern "C" void Buffer_BlockCopy_m3894 ();
extern "C" void Buffer_ByteLengthInternal_m10216 ();
extern "C" void Buffer_BlockCopyInternal_m10217 ();
extern "C" void CharEnumerator__ctor_m10218 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m10219 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m10220 ();
extern "C" void CharEnumerator_get_Current_m10221 ();
extern "C" void CharEnumerator_Clone_m10222 ();
extern "C" void CharEnumerator_MoveNext_m10223 ();
extern "C" void CharEnumerator_Reset_m10224 ();
extern "C" void Console__cctor_m10225 ();
extern "C" void Console_SetEncodings_m10226 ();
extern "C" void Console_get_Error_m4967 ();
extern "C" void Console_Open_m10227 ();
extern "C" void Console_OpenStandardError_m10228 ();
extern "C" void Console_OpenStandardInput_m10229 ();
extern "C" void Console_OpenStandardOutput_m10230 ();
extern "C" void ContextBoundObject__ctor_m10231 ();
extern "C" void Convert__cctor_m10232 ();
extern "C" void Convert_InternalFromBase64String_m10233 ();
extern "C" void Convert_FromBase64String_m5851 ();
extern "C" void Convert_ToBase64String_m5835 ();
extern "C" void Convert_ToBase64String_m10234 ();
extern "C" void Convert_ToBoolean_m10235 ();
extern "C" void Convert_ToBoolean_m10236 ();
extern "C" void Convert_ToBoolean_m10237 ();
extern "C" void Convert_ToBoolean_m10238 ();
extern "C" void Convert_ToBoolean_m10239 ();
extern "C" void Convert_ToBoolean_m10240 ();
extern "C" void Convert_ToBoolean_m10241 ();
extern "C" void Convert_ToBoolean_m10242 ();
extern "C" void Convert_ToBoolean_m10243 ();
extern "C" void Convert_ToBoolean_m10244 ();
extern "C" void Convert_ToBoolean_m10245 ();
extern "C" void Convert_ToBoolean_m10246 ();
extern "C" void Convert_ToBoolean_m10247 ();
extern "C" void Convert_ToBoolean_m10248 ();
extern "C" void Convert_ToByte_m10249 ();
extern "C" void Convert_ToByte_m10250 ();
extern "C" void Convert_ToByte_m10251 ();
extern "C" void Convert_ToByte_m10252 ();
extern "C" void Convert_ToByte_m10253 ();
extern "C" void Convert_ToByte_m10254 ();
extern "C" void Convert_ToByte_m10255 ();
extern "C" void Convert_ToByte_m10256 ();
extern "C" void Convert_ToByte_m10257 ();
extern "C" void Convert_ToByte_m10258 ();
extern "C" void Convert_ToByte_m10259 ();
extern "C" void Convert_ToByte_m10260 ();
extern "C" void Convert_ToByte_m10261 ();
extern "C" void Convert_ToByte_m10262 ();
extern "C" void Convert_ToByte_m10263 ();
extern "C" void Convert_ToChar_m5852 ();
extern "C" void Convert_ToChar_m10264 ();
extern "C" void Convert_ToChar_m10265 ();
extern "C" void Convert_ToChar_m10266 ();
extern "C" void Convert_ToChar_m10267 ();
extern "C" void Convert_ToChar_m10268 ();
extern "C" void Convert_ToChar_m10269 ();
extern "C" void Convert_ToChar_m10270 ();
extern "C" void Convert_ToChar_m10271 ();
extern "C" void Convert_ToChar_m10272 ();
extern "C" void Convert_ToChar_m10273 ();
extern "C" void Convert_ToDateTime_m10274 ();
extern "C" void Convert_ToDateTime_m10275 ();
extern "C" void Convert_ToDateTime_m10276 ();
extern "C" void Convert_ToDateTime_m10277 ();
extern "C" void Convert_ToDateTime_m10278 ();
extern "C" void Convert_ToDateTime_m10279 ();
extern "C" void Convert_ToDateTime_m10280 ();
extern "C" void Convert_ToDateTime_m10281 ();
extern "C" void Convert_ToDateTime_m10282 ();
extern "C" void Convert_ToDateTime_m10283 ();
extern "C" void Convert_ToDecimal_m10284 ();
extern "C" void Convert_ToDecimal_m10285 ();
extern "C" void Convert_ToDecimal_m10286 ();
extern "C" void Convert_ToDecimal_m10287 ();
extern "C" void Convert_ToDecimal_m10288 ();
extern "C" void Convert_ToDecimal_m10289 ();
extern "C" void Convert_ToDecimal_m10290 ();
extern "C" void Convert_ToDecimal_m10291 ();
extern "C" void Convert_ToDecimal_m10292 ();
extern "C" void Convert_ToDecimal_m10293 ();
extern "C" void Convert_ToDecimal_m10294 ();
extern "C" void Convert_ToDecimal_m10295 ();
extern "C" void Convert_ToDecimal_m10296 ();
extern "C" void Convert_ToDouble_m10297 ();
extern "C" void Convert_ToDouble_m10298 ();
extern "C" void Convert_ToDouble_m10299 ();
extern "C" void Convert_ToDouble_m10300 ();
extern "C" void Convert_ToDouble_m10301 ();
extern "C" void Convert_ToDouble_m10302 ();
extern "C" void Convert_ToDouble_m10303 ();
extern "C" void Convert_ToDouble_m10304 ();
extern "C" void Convert_ToDouble_m10305 ();
extern "C" void Convert_ToDouble_m10306 ();
extern "C" void Convert_ToDouble_m10307 ();
extern "C" void Convert_ToDouble_m10308 ();
extern "C" void Convert_ToDouble_m10309 ();
extern "C" void Convert_ToDouble_m10310 ();
extern "C" void Convert_ToInt16_m10311 ();
extern "C" void Convert_ToInt16_m10312 ();
extern "C" void Convert_ToInt16_m10313 ();
extern "C" void Convert_ToInt16_m10314 ();
extern "C" void Convert_ToInt16_m10315 ();
extern "C" void Convert_ToInt16_m10316 ();
extern "C" void Convert_ToInt16_m10317 ();
extern "C" void Convert_ToInt16_m10318 ();
extern "C" void Convert_ToInt16_m10319 ();
extern "C" void Convert_ToInt16_m10320 ();
extern "C" void Convert_ToInt16_m5823 ();
extern "C" void Convert_ToInt16_m10321 ();
extern "C" void Convert_ToInt16_m10322 ();
extern "C" void Convert_ToInt16_m10323 ();
extern "C" void Convert_ToInt16_m10324 ();
extern "C" void Convert_ToInt16_m10325 ();
extern "C" void Convert_ToInt32_m10326 ();
extern "C" void Convert_ToInt32_m10327 ();
extern "C" void Convert_ToInt32_m10328 ();
extern "C" void Convert_ToInt32_m10329 ();
extern "C" void Convert_ToInt32_m10330 ();
extern "C" void Convert_ToInt32_m10331 ();
extern "C" void Convert_ToInt32_m10332 ();
extern "C" void Convert_ToInt32_m10333 ();
extern "C" void Convert_ToInt32_m10334 ();
extern "C" void Convert_ToInt32_m10335 ();
extern "C" void Convert_ToInt32_m10336 ();
extern "C" void Convert_ToInt32_m10337 ();
extern "C" void Convert_ToInt32_m10338 ();
extern "C" void Convert_ToInt32_m10339 ();
extern "C" void Convert_ToInt32_m5861 ();
extern "C" void Convert_ToInt64_m10340 ();
extern "C" void Convert_ToInt64_m10341 ();
extern "C" void Convert_ToInt64_m10342 ();
extern "C" void Convert_ToInt64_m10343 ();
extern "C" void Convert_ToInt64_m10344 ();
extern "C" void Convert_ToInt64_m10345 ();
extern "C" void Convert_ToInt64_m10346 ();
extern "C" void Convert_ToInt64_m10347 ();
extern "C" void Convert_ToInt64_m10348 ();
extern "C" void Convert_ToInt64_m10349 ();
extern "C" void Convert_ToInt64_m10350 ();
extern "C" void Convert_ToInt64_m10351 ();
extern "C" void Convert_ToInt64_m10352 ();
extern "C" void Convert_ToInt64_m10353 ();
extern "C" void Convert_ToInt64_m10354 ();
extern "C" void Convert_ToInt64_m10355 ();
extern "C" void Convert_ToInt64_m10356 ();
extern "C" void Convert_ToSByte_m10357 ();
extern "C" void Convert_ToSByte_m10358 ();
extern "C" void Convert_ToSByte_m10359 ();
extern "C" void Convert_ToSByte_m10360 ();
extern "C" void Convert_ToSByte_m10361 ();
extern "C" void Convert_ToSByte_m10362 ();
extern "C" void Convert_ToSByte_m10363 ();
extern "C" void Convert_ToSByte_m10364 ();
extern "C" void Convert_ToSByte_m10365 ();
extern "C" void Convert_ToSByte_m10366 ();
extern "C" void Convert_ToSByte_m10367 ();
extern "C" void Convert_ToSByte_m10368 ();
extern "C" void Convert_ToSByte_m10369 ();
extern "C" void Convert_ToSByte_m10370 ();
extern "C" void Convert_ToSingle_m10371 ();
extern "C" void Convert_ToSingle_m10372 ();
extern "C" void Convert_ToSingle_m10373 ();
extern "C" void Convert_ToSingle_m10374 ();
extern "C" void Convert_ToSingle_m10375 ();
extern "C" void Convert_ToSingle_m10376 ();
extern "C" void Convert_ToSingle_m10377 ();
extern "C" void Convert_ToSingle_m10378 ();
extern "C" void Convert_ToSingle_m10379 ();
extern "C" void Convert_ToSingle_m10380 ();
extern "C" void Convert_ToSingle_m10381 ();
extern "C" void Convert_ToSingle_m10382 ();
extern "C" void Convert_ToSingle_m10383 ();
extern "C" void Convert_ToSingle_m10384 ();
extern "C" void Convert_ToString_m10385 ();
extern "C" void Convert_ToString_m10386 ();
extern "C" void Convert_ToUInt16_m10387 ();
extern "C" void Convert_ToUInt16_m10388 ();
extern "C" void Convert_ToUInt16_m10389 ();
extern "C" void Convert_ToUInt16_m10390 ();
extern "C" void Convert_ToUInt16_m10391 ();
extern "C" void Convert_ToUInt16_m10392 ();
extern "C" void Convert_ToUInt16_m10393 ();
extern "C" void Convert_ToUInt16_m10394 ();
extern "C" void Convert_ToUInt16_m10395 ();
extern "C" void Convert_ToUInt16_m10396 ();
extern "C" void Convert_ToUInt16_m10397 ();
extern "C" void Convert_ToUInt16_m10398 ();
extern "C" void Convert_ToUInt16_m10399 ();
extern "C" void Convert_ToUInt16_m10400 ();
extern "C" void Convert_ToUInt32_m2159 ();
extern "C" void Convert_ToUInt32_m10401 ();
extern "C" void Convert_ToUInt32_m10402 ();
extern "C" void Convert_ToUInt32_m10403 ();
extern "C" void Convert_ToUInt32_m10404 ();
extern "C" void Convert_ToUInt32_m10405 ();
extern "C" void Convert_ToUInt32_m10406 ();
extern "C" void Convert_ToUInt32_m10407 ();
extern "C" void Convert_ToUInt32_m10408 ();
extern "C" void Convert_ToUInt32_m10409 ();
extern "C" void Convert_ToUInt32_m10410 ();
extern "C" void Convert_ToUInt32_m10411 ();
extern "C" void Convert_ToUInt32_m10412 ();
extern "C" void Convert_ToUInt32_m2158 ();
extern "C" void Convert_ToUInt32_m10413 ();
extern "C" void Convert_ToUInt64_m10414 ();
extern "C" void Convert_ToUInt64_m10415 ();
extern "C" void Convert_ToUInt64_m10416 ();
extern "C" void Convert_ToUInt64_m10417 ();
extern "C" void Convert_ToUInt64_m10418 ();
extern "C" void Convert_ToUInt64_m10419 ();
extern "C" void Convert_ToUInt64_m10420 ();
extern "C" void Convert_ToUInt64_m10421 ();
extern "C" void Convert_ToUInt64_m10422 ();
extern "C" void Convert_ToUInt64_m10423 ();
extern "C" void Convert_ToUInt64_m10424 ();
extern "C" void Convert_ToUInt64_m10425 ();
extern "C" void Convert_ToUInt64_m10426 ();
extern "C" void Convert_ToUInt64_m10427 ();
extern "C" void Convert_ToUInt64_m10428 ();
extern "C" void Convert_ChangeType_m10429 ();
extern "C" void Convert_ToType_m10430 ();
extern "C" void DBNull__ctor_m10431 ();
extern "C" void DBNull__ctor_m10432 ();
extern "C" void DBNull__cctor_m10433 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m10434 ();
extern "C" void DBNull_System_IConvertible_ToByte_m10435 ();
extern "C" void DBNull_System_IConvertible_ToChar_m10436 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m10437 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m10438 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m10439 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m10440 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m10441 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m10442 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m10443 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m10444 ();
extern "C" void DBNull_System_IConvertible_ToType_m10445 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m10446 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m10447 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m10448 ();
extern "C" void DBNull_GetObjectData_m10449 ();
extern "C" void DBNull_ToString_m10450 ();
extern "C" void DBNull_ToString_m10451 ();
extern "C" void DateTime__ctor_m10452 ();
extern "C" void DateTime__ctor_m10453 ();
extern "C" void DateTime__ctor_m2199 ();
extern "C" void DateTime__ctor_m10454 ();
extern "C" void DateTime__ctor_m10455 ();
extern "C" void DateTime__cctor_m10456 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m10457 ();
extern "C" void DateTime_System_IConvertible_ToByte_m10458 ();
extern "C" void DateTime_System_IConvertible_ToChar_m10459 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m10460 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m10461 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m10462 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m10463 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m10464 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m10465 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m10466 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m10467 ();
extern "C" void DateTime_System_IConvertible_ToType_m10468 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m10469 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m10470 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m10471 ();
extern "C" void DateTime_AbsoluteDays_m10472 ();
extern "C" void DateTime_FromTicks_m10473 ();
extern "C" void DateTime_get_Month_m10474 ();
extern "C" void DateTime_get_Day_m10475 ();
extern "C" void DateTime_get_DayOfWeek_m10476 ();
extern "C" void DateTime_get_Hour_m10477 ();
extern "C" void DateTime_get_Minute_m10478 ();
extern "C" void DateTime_get_Second_m10479 ();
extern "C" void DateTime_GetTimeMonotonic_m10480 ();
extern "C" void DateTime_GetNow_m10481 ();
extern "C" void DateTime_get_Now_m2184 ();
extern "C" void DateTime_get_Ticks_m5874 ();
extern "C" void DateTime_get_Today_m10482 ();
extern "C" void DateTime_get_UtcNow_m5848 ();
extern "C" void DateTime_get_Year_m10483 ();
extern "C" void DateTime_get_Kind_m10484 ();
extern "C" void DateTime_Add_m10485 ();
extern "C" void DateTime_AddTicks_m10486 ();
extern "C" void DateTime_AddMilliseconds_m4851 ();
extern "C" void DateTime_AddSeconds_m2200 ();
extern "C" void DateTime_Compare_m10487 ();
extern "C" void DateTime_CompareTo_m10488 ();
extern "C" void DateTime_CompareTo_m10489 ();
extern "C" void DateTime_Equals_m10490 ();
extern "C" void DateTime_FromBinary_m10491 ();
extern "C" void DateTime_SpecifyKind_m10492 ();
extern "C" void DateTime_DaysInMonth_m10493 ();
extern "C" void DateTime_Equals_m10494 ();
extern "C" void DateTime_CheckDateTimeKind_m10495 ();
extern "C" void DateTime_GetHashCode_m10496 ();
extern "C" void DateTime_IsLeapYear_m10497 ();
extern "C" void DateTime_Parse_m10498 ();
extern "C" void DateTime_Parse_m10499 ();
extern "C" void DateTime_CoreParse_m10500 ();
extern "C" void DateTime_YearMonthDayFormats_m10501 ();
extern "C" void DateTime__ParseNumber_m10502 ();
extern "C" void DateTime__ParseEnum_m10503 ();
extern "C" void DateTime__ParseString_m10504 ();
extern "C" void DateTime__ParseAmPm_m10505 ();
extern "C" void DateTime__ParseTimeSeparator_m10506 ();
extern "C" void DateTime__ParseDateSeparator_m10507 ();
extern "C" void DateTime_IsLetter_m10508 ();
extern "C" void DateTime__DoParse_m10509 ();
extern "C" void DateTime_ParseExact_m5824 ();
extern "C" void DateTime_ParseExact_m10510 ();
extern "C" void DateTime_CheckStyle_m10511 ();
extern "C" void DateTime_ParseExact_m10512 ();
extern "C" void DateTime_Subtract_m10513 ();
extern "C" void DateTime_ToString_m10514 ();
extern "C" void DateTime_ToString_m10515 ();
extern "C" void DateTime_ToString_m10516 ();
extern "C" void DateTime_ToLocalTime_m4889 ();
extern "C" void DateTime_ToUniversalTime_m10517 ();
extern "C" void DateTime_op_Addition_m10518 ();
extern "C" void DateTime_op_Equality_m10519 ();
extern "C" void DateTime_op_GreaterThan_m4916 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m4852 ();
extern "C" void DateTime_op_Inequality_m10520 ();
extern "C" void DateTime_op_LessThan_m4915 ();
extern "C" void DateTime_op_LessThanOrEqual_m4914 ();
extern "C" void DateTime_op_Subtraction_m10521 ();
extern "C" void DateTimeOffset__ctor_m10522 ();
extern "C" void DateTimeOffset__ctor_m10523 ();
extern "C" void DateTimeOffset__ctor_m10524 ();
extern "C" void DateTimeOffset__ctor_m10525 ();
extern "C" void DateTimeOffset__cctor_m10526 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m10527 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m10528 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10529 ();
extern "C" void DateTimeOffset_CompareTo_m10530 ();
extern "C" void DateTimeOffset_Equals_m10531 ();
extern "C" void DateTimeOffset_Equals_m10532 ();
extern "C" void DateTimeOffset_GetHashCode_m10533 ();
extern "C" void DateTimeOffset_ToString_m10534 ();
extern "C" void DateTimeOffset_ToString_m10535 ();
extern "C" void DateTimeOffset_get_DateTime_m10536 ();
extern "C" void DateTimeOffset_get_Offset_m10537 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m10538 ();
extern "C" void DateTimeUtils_CountRepeat_m10539 ();
extern "C" void DateTimeUtils_ZeroPad_m10540 ();
extern "C" void DateTimeUtils_ParseQuotedString_m10541 ();
extern "C" void DateTimeUtils_GetStandardPattern_m10542 ();
extern "C" void DateTimeUtils_GetStandardPattern_m10543 ();
extern "C" void DateTimeUtils_ToString_m10544 ();
extern "C" void DateTimeUtils_ToString_m10545 ();
extern "C" void DelegateEntry__ctor_m10546 ();
extern "C" void DelegateEntry_DeserializeDelegate_m10547 ();
extern "C" void DelegateSerializationHolder__ctor_m10548 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m10549 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m10550 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m10551 ();
extern "C" void DivideByZeroException__ctor_m10552 ();
extern "C" void DivideByZeroException__ctor_m10553 ();
extern "C" void DllNotFoundException__ctor_m10554 ();
extern "C" void DllNotFoundException__ctor_m10555 ();
extern "C" void EntryPointNotFoundException__ctor_m10556 ();
extern "C" void EntryPointNotFoundException__ctor_m10557 ();
extern "C" void SByteComparer__ctor_m10558 ();
extern "C" void SByteComparer_Compare_m10559 ();
extern "C" void SByteComparer_Compare_m10560 ();
extern "C" void ShortComparer__ctor_m10561 ();
extern "C" void ShortComparer_Compare_m10562 ();
extern "C" void ShortComparer_Compare_m10563 ();
extern "C" void IntComparer__ctor_m10564 ();
extern "C" void IntComparer_Compare_m10565 ();
extern "C" void IntComparer_Compare_m10566 ();
extern "C" void LongComparer__ctor_m10567 ();
extern "C" void LongComparer_Compare_m10568 ();
extern "C" void LongComparer_Compare_m10569 ();
extern "C" void MonoEnumInfo__ctor_m10570 ();
extern "C" void MonoEnumInfo__cctor_m10571 ();
extern "C" void MonoEnumInfo_get_enum_info_m10572 ();
extern "C" void MonoEnumInfo_get_Cache_m10573 ();
extern "C" void MonoEnumInfo_GetInfo_m10574 ();
extern "C" void Environment_get_SocketSecurityEnabled_m10575 ();
extern "C" void Environment_get_NewLine_m4876 ();
extern "C" void Environment_get_Platform_m10576 ();
extern "C" void Environment_GetOSVersionString_m10577 ();
extern "C" void Environment_get_OSVersion_m10578 ();
extern "C" void Environment_internalGetEnvironmentVariable_m10579 ();
extern "C" void Environment_GetEnvironmentVariable_m5872 ();
extern "C" void Environment_GetWindowsFolderPath_m10580 ();
extern "C" void Environment_GetFolderPath_m5858 ();
extern "C" void Environment_ReadXdgUserDir_m10581 ();
extern "C" void Environment_InternalGetFolderPath_m10582 ();
extern "C" void Environment_get_IsRunningOnWindows_m10583 ();
extern "C" void Environment_GetMachineConfigPath_m10584 ();
extern "C" void Environment_internalGetHome_m10585 ();
extern "C" void EventArgs__ctor_m10586 ();
extern "C" void EventArgs__cctor_m10587 ();
extern "C" void ExecutionEngineException__ctor_m10588 ();
extern "C" void ExecutionEngineException__ctor_m10589 ();
extern "C" void FieldAccessException__ctor_m10590 ();
extern "C" void FieldAccessException__ctor_m10591 ();
extern "C" void FieldAccessException__ctor_m10592 ();
extern "C" void FlagsAttribute__ctor_m10593 ();
extern "C" void FormatException__ctor_m10594 ();
extern "C" void FormatException__ctor_m4841 ();
extern "C" void FormatException__ctor_m4978 ();
extern "C" void GC_SuppressFinalize_m3895 ();
extern "C" void Guid__ctor_m10595 ();
extern "C" void Guid__ctor_m10596 ();
extern "C" void Guid__cctor_m10597 ();
extern "C" void Guid_CheckNull_m10598 ();
extern "C" void Guid_CheckLength_m10599 ();
extern "C" void Guid_CheckArray_m10600 ();
extern "C" void Guid_Compare_m10601 ();
extern "C" void Guid_CompareTo_m10602 ();
extern "C" void Guid_Equals_m10603 ();
extern "C" void Guid_CompareTo_m10604 ();
extern "C" void Guid_Equals_m10605 ();
extern "C" void Guid_GetHashCode_m10606 ();
extern "C" void Guid_ToHex_m10607 ();
extern "C" void Guid_NewGuid_m10608 ();
extern "C" void Guid_AppendInt_m10609 ();
extern "C" void Guid_AppendShort_m10610 ();
extern "C" void Guid_AppendByte_m10611 ();
extern "C" void Guid_BaseToString_m10612 ();
extern "C" void Guid_ToString_m10613 ();
extern "C" void Guid_ToString_m10614 ();
extern "C" void Guid_ToString_m10615 ();
extern "C" void IndexOutOfRangeException__ctor_m10616 ();
extern "C" void IndexOutOfRangeException__ctor_m2160 ();
extern "C" void IndexOutOfRangeException__ctor_m10617 ();
extern "C" void InvalidCastException__ctor_m10618 ();
extern "C" void InvalidCastException__ctor_m10619 ();
extern "C" void InvalidCastException__ctor_m10620 ();
extern "C" void InvalidOperationException__ctor_m4822 ();
extern "C" void InvalidOperationException__ctor_m4818 ();
extern "C" void InvalidOperationException__ctor_m10621 ();
extern "C" void InvalidOperationException__ctor_m10622 ();
extern "C" void LocalDataStoreSlot__ctor_m10623 ();
extern "C" void LocalDataStoreSlot__cctor_m10624 ();
extern "C" void LocalDataStoreSlot_Finalize_m10625 ();
extern "C" void Math_Abs_m10626 ();
extern "C" void Math_Abs_m10627 ();
extern "C" void Math_Abs_m10628 ();
extern "C" void Math_Ceiling_m10629 ();
extern "C" void Math_Floor_m10630 ();
extern "C" void Math_Log_m2162 ();
extern "C" void Math_Max_m489 ();
extern "C" void Math_Max_m2174 ();
extern "C" void Math_Min_m490 ();
extern "C" void Math_Min_m3893 ();
extern "C" void Math_Round_m10631 ();
extern "C" void Math_Round_m10632 ();
extern "C" void Math_Sin_m10633 ();
extern "C" void Math_Cos_m10634 ();
extern "C" void Math_Tan_m10635 ();
extern "C" void Math_Atan_m10636 ();
extern "C" void Math_Log_m10637 ();
extern "C" void Math_Pow_m10638 ();
extern "C" void Math_Sqrt_m10639 ();
extern "C" void MemberAccessException__ctor_m10640 ();
extern "C" void MemberAccessException__ctor_m10641 ();
extern "C" void MemberAccessException__ctor_m10642 ();
extern "C" void MethodAccessException__ctor_m10643 ();
extern "C" void MethodAccessException__ctor_m10644 ();
extern "C" void MissingFieldException__ctor_m10645 ();
extern "C" void MissingFieldException__ctor_m10646 ();
extern "C" void MissingFieldException__ctor_m10647 ();
extern "C" void MissingFieldException_get_Message_m10648 ();
extern "C" void MissingMemberException__ctor_m10649 ();
extern "C" void MissingMemberException__ctor_m10650 ();
extern "C" void MissingMemberException__ctor_m10651 ();
extern "C" void MissingMemberException__ctor_m10652 ();
extern "C" void MissingMemberException_GetObjectData_m10653 ();
extern "C" void MissingMemberException_get_Message_m10654 ();
extern "C" void MissingMethodException__ctor_m10655 ();
extern "C" void MissingMethodException__ctor_m10656 ();
extern "C" void MissingMethodException__ctor_m10657 ();
extern "C" void MissingMethodException__ctor_m10658 ();
extern "C" void MissingMethodException_get_Message_m10659 ();
extern "C" void MonoAsyncCall__ctor_m10660 ();
extern "C" void AttributeInfo__ctor_m10661 ();
extern "C" void AttributeInfo_get_Usage_m10662 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m10663 ();
extern "C" void MonoCustomAttrs__cctor_m10664 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m10665 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m10666 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m10667 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m10668 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m10669 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10670 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10671 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesDataInternal_m10672 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesData_m10673 ();
extern "C" void MonoCustomAttrs_IsDefined_m10674 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m10675 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m10676 ();
extern "C" void MonoCustomAttrs_GetBase_m10677 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m10678 ();
extern "C" void MonoTouchAOTHelper__cctor_m10679 ();
extern "C" void MonoTypeInfo__ctor_m10680 ();
extern "C" void MonoType_get_attributes_m10681 ();
extern "C" void MonoType_GetDefaultConstructor_m10682 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m10683 ();
extern "C" void MonoType_GetConstructorImpl_m10684 ();
extern "C" void MonoType_GetConstructors_internal_m10685 ();
extern "C" void MonoType_GetConstructors_m10686 ();
extern "C" void MonoType_InternalGetEvent_m10687 ();
extern "C" void MonoType_GetEvent_m10688 ();
extern "C" void MonoType_GetField_m10689 ();
extern "C" void MonoType_GetFields_internal_m10690 ();
extern "C" void MonoType_GetFields_m10691 ();
extern "C" void MonoType_GetInterfaces_m10692 ();
extern "C" void MonoType_GetMethodsByName_m10693 ();
extern "C" void MonoType_GetMethods_m10694 ();
extern "C" void MonoType_GetMethodImpl_m10695 ();
extern "C" void MonoType_GetPropertiesByName_m10696 ();
extern "C" void MonoType_GetPropertyImpl_m10697 ();
extern "C" void MonoType_HasElementTypeImpl_m10698 ();
extern "C" void MonoType_IsArrayImpl_m10699 ();
extern "C" void MonoType_IsByRefImpl_m10700 ();
extern "C" void MonoType_IsPointerImpl_m10701 ();
extern "C" void MonoType_IsPrimitiveImpl_m10702 ();
extern "C" void MonoType_IsSubclassOf_m10703 ();
extern "C" void MonoType_InvokeMember_m10704 ();
extern "C" void MonoType_GetElementType_m10705 ();
extern "C" void MonoType_get_UnderlyingSystemType_m10706 ();
extern "C" void MonoType_get_Assembly_m10707 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m10708 ();
extern "C" void MonoType_getFullName_m10709 ();
extern "C" void MonoType_get_BaseType_m10710 ();
extern "C" void MonoType_get_FullName_m10711 ();
extern "C" void MonoType_IsDefined_m10712 ();
extern "C" void MonoType_GetCustomAttributes_m10713 ();
extern "C" void MonoType_GetCustomAttributes_m10714 ();
extern "C" void MonoType_get_MemberType_m10715 ();
extern "C" void MonoType_get_Name_m10716 ();
extern "C" void MonoType_get_Namespace_m10717 ();
extern "C" void MonoType_get_Module_m10718 ();
extern "C" void MonoType_get_DeclaringType_m10719 ();
extern "C" void MonoType_get_ReflectedType_m10720 ();
extern "C" void MonoType_get_TypeHandle_m10721 ();
extern "C" void MonoType_GetObjectData_m10722 ();
extern "C" void MonoType_ToString_m10723 ();
extern "C" void MonoType_GetGenericArguments_m10724 ();
extern "C" void MonoType_get_ContainsGenericParameters_m10725 ();
extern "C" void MonoType_get_IsGenericParameter_m10726 ();
extern "C" void MonoType_GetGenericTypeDefinition_m10727 ();
extern "C" void MonoType_CheckMethodSecurity_m10728 ();
extern "C" void MonoType_ReorderParamArrayArguments_m10729 ();
extern "C" void MulticastNotSupportedException__ctor_m10730 ();
extern "C" void MulticastNotSupportedException__ctor_m10731 ();
extern "C" void MulticastNotSupportedException__ctor_m10732 ();
extern "C" void NonSerializedAttribute__ctor_m10733 ();
extern "C" void NotImplementedException__ctor_m10734 ();
extern "C" void NotImplementedException__ctor_m3897 ();
extern "C" void NotImplementedException__ctor_m10735 ();
extern "C" void NotSupportedException__ctor_m392 ();
extern "C" void NotSupportedException__ctor_m4832 ();
extern "C" void NotSupportedException__ctor_m10736 ();
extern "C" void NullReferenceException__ctor_m10737 ();
extern "C" void NullReferenceException__ctor_m2135 ();
extern "C" void NullReferenceException__ctor_m10738 ();
extern "C" void CustomInfo__ctor_m10739 ();
extern "C" void CustomInfo_GetActiveSection_m10740 ();
extern "C" void CustomInfo_Parse_m10741 ();
extern "C" void CustomInfo_Format_m10742 ();
extern "C" void NumberFormatter__ctor_m10743 ();
extern "C" void NumberFormatter__cctor_m10744 ();
extern "C" void NumberFormatter_GetFormatterTables_m10745 ();
extern "C" void NumberFormatter_GetTenPowerOf_m10746 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10747 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10748 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10749 ();
extern "C" void NumberFormatter_FastToDecHex_m10750 ();
extern "C" void NumberFormatter_ToDecHex_m10751 ();
extern "C" void NumberFormatter_FastDecHexLen_m10752 ();
extern "C" void NumberFormatter_DecHexLen_m10753 ();
extern "C" void NumberFormatter_DecHexLen_m10754 ();
extern "C" void NumberFormatter_ScaleOrder_m10755 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m10756 ();
extern "C" void NumberFormatter_ParsePrecision_m10757 ();
extern "C" void NumberFormatter_Init_m10758 ();
extern "C" void NumberFormatter_InitHex_m10759 ();
extern "C" void NumberFormatter_Init_m10760 ();
extern "C" void NumberFormatter_Init_m10761 ();
extern "C" void NumberFormatter_Init_m10762 ();
extern "C" void NumberFormatter_Init_m10763 ();
extern "C" void NumberFormatter_Init_m10764 ();
extern "C" void NumberFormatter_Init_m10765 ();
extern "C" void NumberFormatter_ResetCharBuf_m10766 ();
extern "C" void NumberFormatter_Resize_m10767 ();
extern "C" void NumberFormatter_Append_m10768 ();
extern "C" void NumberFormatter_Append_m10769 ();
extern "C" void NumberFormatter_Append_m10770 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m10771 ();
extern "C" void NumberFormatter_set_CurrentCulture_m10772 ();
extern "C" void NumberFormatter_get_IntegerDigits_m10773 ();
extern "C" void NumberFormatter_get_DecimalDigits_m10774 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m10775 ();
extern "C" void NumberFormatter_get_IsZero_m10776 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m10777 ();
extern "C" void NumberFormatter_RoundPos_m10778 ();
extern "C" void NumberFormatter_RoundDecimal_m10779 ();
extern "C" void NumberFormatter_RoundBits_m10780 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m10781 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10782 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10783 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10784 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10785 ();
extern "C" void NumberFormatter_GetInstance_m10786 ();
extern "C" void NumberFormatter_Release_m10787 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m10788 ();
extern "C" void NumberFormatter_NumberToString_m10789 ();
extern "C" void NumberFormatter_NumberToString_m10790 ();
extern "C" void NumberFormatter_NumberToString_m10791 ();
extern "C" void NumberFormatter_NumberToString_m10792 ();
extern "C" void NumberFormatter_NumberToString_m10793 ();
extern "C" void NumberFormatter_NumberToString_m10794 ();
extern "C" void NumberFormatter_NumberToString_m10795 ();
extern "C" void NumberFormatter_NumberToString_m10796 ();
extern "C" void NumberFormatter_NumberToString_m10797 ();
extern "C" void NumberFormatter_NumberToString_m10798 ();
extern "C" void NumberFormatter_NumberToString_m10799 ();
extern "C" void NumberFormatter_NumberToString_m10800 ();
extern "C" void NumberFormatter_NumberToString_m10801 ();
extern "C" void NumberFormatter_NumberToString_m10802 ();
extern "C" void NumberFormatter_NumberToString_m10803 ();
extern "C" void NumberFormatter_NumberToString_m10804 ();
extern "C" void NumberFormatter_NumberToString_m10805 ();
extern "C" void NumberFormatter_FastIntegerToString_m10806 ();
extern "C" void NumberFormatter_IntegerToString_m10807 ();
extern "C" void NumberFormatter_NumberToString_m10808 ();
extern "C" void NumberFormatter_FormatCurrency_m10809 ();
extern "C" void NumberFormatter_FormatDecimal_m10810 ();
extern "C" void NumberFormatter_FormatHexadecimal_m10811 ();
extern "C" void NumberFormatter_FormatFixedPoint_m10812 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10813 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10814 ();
extern "C" void NumberFormatter_FormatGeneral_m10815 ();
extern "C" void NumberFormatter_FormatNumber_m10816 ();
extern "C" void NumberFormatter_FormatPercent_m10817 ();
extern "C" void NumberFormatter_FormatExponential_m10818 ();
extern "C" void NumberFormatter_FormatExponential_m10819 ();
extern "C" void NumberFormatter_FormatCustom_m10820 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m10821 ();
extern "C" void NumberFormatter_IsZeroOnly_m10822 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m10823 ();
extern "C" void NumberFormatter_AppendIntegerString_m10824 ();
extern "C" void NumberFormatter_AppendIntegerString_m10825 ();
extern "C" void NumberFormatter_AppendDecimalString_m10826 ();
extern "C" void NumberFormatter_AppendDecimalString_m10827 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m10828 ();
extern "C" void NumberFormatter_AppendExponent_m10829 ();
extern "C" void NumberFormatter_AppendOneDigit_m10830 ();
extern "C" void NumberFormatter_FastAppendDigits_m10831 ();
extern "C" void NumberFormatter_AppendDigits_m10832 ();
extern "C" void NumberFormatter_AppendDigits_m10833 ();
extern "C" void NumberFormatter_Multiply10_m10834 ();
extern "C" void NumberFormatter_Divide10_m10835 ();
extern "C" void NumberFormatter_GetClone_m10836 ();
extern "C" void ObjectDisposedException__ctor_m3900 ();
extern "C" void ObjectDisposedException__ctor_m10837 ();
extern "C" void ObjectDisposedException__ctor_m10838 ();
extern "C" void ObjectDisposedException_get_Message_m10839 ();
extern "C" void ObjectDisposedException_GetObjectData_m10840 ();
extern "C" void OperatingSystem__ctor_m10841 ();
extern "C" void OperatingSystem_get_Platform_m10842 ();
extern "C" void OperatingSystem_Clone_m10843 ();
extern "C" void OperatingSystem_GetObjectData_m10844 ();
extern "C" void OperatingSystem_ToString_m10845 ();
extern "C" void OutOfMemoryException__ctor_m10846 ();
extern "C" void OutOfMemoryException__ctor_m10847 ();
extern "C" void OverflowException__ctor_m10848 ();
extern "C" void OverflowException__ctor_m10849 ();
extern "C" void OverflowException__ctor_m10850 ();
extern "C" void RankException__ctor_m10851 ();
extern "C" void RankException__ctor_m10852 ();
extern "C" void RankException__ctor_m10853 ();
extern "C" void ResolveEventArgs__ctor_m10854 ();
extern "C" void RuntimeMethodHandle__ctor_m10855 ();
extern "C" void RuntimeMethodHandle__ctor_m10856 ();
extern "C" void RuntimeMethodHandle_get_Value_m10857 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m10858 ();
extern "C" void RuntimeMethodHandle_Equals_m10859 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m10860 ();
extern "C" void StringComparer__ctor_m10861 ();
extern "C" void StringComparer__cctor_m10862 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m4855 ();
extern "C" void StringComparer_get_OrdinalIgnoreCase_m2190 ();
extern "C" void StringComparer_Compare_m10863 ();
extern "C" void StringComparer_Equals_m10864 ();
extern "C" void StringComparer_GetHashCode_m10865 ();
extern "C" void CultureAwareComparer__ctor_m10866 ();
extern "C" void CultureAwareComparer_Compare_m10867 ();
extern "C" void CultureAwareComparer_Equals_m10868 ();
extern "C" void CultureAwareComparer_GetHashCode_m10869 ();
extern "C" void OrdinalComparer__ctor_m10870 ();
extern "C" void OrdinalComparer_Compare_m10871 ();
extern "C" void OrdinalComparer_Equals_m10872 ();
extern "C" void OrdinalComparer_GetHashCode_m10873 ();
extern "C" void SystemException__ctor_m10874 ();
extern "C" void SystemException__ctor_m4953 ();
extern "C" void SystemException__ctor_m10875 ();
extern "C" void SystemException__ctor_m10876 ();
extern "C" void ThreadStaticAttribute__ctor_m10877 ();
extern "C" void TimeSpan__ctor_m10878 ();
extern "C" void TimeSpan__ctor_m10879 ();
extern "C" void TimeSpan__ctor_m10880 ();
extern "C" void TimeSpan__cctor_m10881 ();
extern "C" void TimeSpan_CalculateTicks_m10882 ();
extern "C" void TimeSpan_get_Days_m10883 ();
extern "C" void TimeSpan_get_Hours_m10884 ();
extern "C" void TimeSpan_get_Milliseconds_m10885 ();
extern "C" void TimeSpan_get_Minutes_m10886 ();
extern "C" void TimeSpan_get_Seconds_m10887 ();
extern "C" void TimeSpan_get_Ticks_m10888 ();
extern "C" void TimeSpan_get_TotalDays_m10889 ();
extern "C" void TimeSpan_get_TotalHours_m10890 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m10891 ();
extern "C" void TimeSpan_get_TotalMinutes_m10892 ();
extern "C" void TimeSpan_get_TotalSeconds_m10893 ();
extern "C" void TimeSpan_Add_m10894 ();
extern "C" void TimeSpan_Compare_m10895 ();
extern "C" void TimeSpan_CompareTo_m10896 ();
extern "C" void TimeSpan_CompareTo_m10897 ();
extern "C" void TimeSpan_Equals_m10898 ();
extern "C" void TimeSpan_Duration_m10899 ();
extern "C" void TimeSpan_Equals_m10900 ();
extern "C" void TimeSpan_FromDays_m10901 ();
extern "C" void TimeSpan_FromHours_m10902 ();
extern "C" void TimeSpan_FromMinutes_m10903 ();
extern "C" void TimeSpan_FromSeconds_m10904 ();
extern "C" void TimeSpan_FromMilliseconds_m10905 ();
extern "C" void TimeSpan_From_m10906 ();
extern "C" void TimeSpan_GetHashCode_m10907 ();
extern "C" void TimeSpan_Negate_m10908 ();
extern "C" void TimeSpan_Subtract_m10909 ();
extern "C" void TimeSpan_ToString_m10910 ();
extern "C" void TimeSpan_op_Addition_m10911 ();
extern "C" void TimeSpan_op_Equality_m10912 ();
extern "C" void TimeSpan_op_GreaterThan_m10913 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m10914 ();
extern "C" void TimeSpan_op_Inequality_m10915 ();
extern "C" void TimeSpan_op_LessThan_m10916 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m10917 ();
extern "C" void TimeSpan_op_Subtraction_m10918 ();
extern "C" void TimeZone__ctor_m10919 ();
extern "C" void TimeZone__cctor_m10920 ();
extern "C" void TimeZone_get_CurrentTimeZone_m10921 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10922 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10923 ();
extern "C" void TimeZone_ToLocalTime_m10924 ();
extern "C" void TimeZone_ToUniversalTime_m10925 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10926 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10927 ();
extern "C" void CurrentSystemTimeZone__ctor_m10928 ();
extern "C" void CurrentSystemTimeZone__ctor_m10929 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10930 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m10931 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m10932 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m10933 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m10934 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m10935 ();
extern "C" void TypeInitializationException__ctor_m10936 ();
extern "C" void TypeInitializationException_GetObjectData_m10937 ();
extern "C" void TypeLoadException__ctor_m10938 ();
extern "C" void TypeLoadException__ctor_m10939 ();
extern "C" void TypeLoadException__ctor_m10940 ();
extern "C" void TypeLoadException_get_Message_m10941 ();
extern "C" void TypeLoadException_GetObjectData_m10942 ();
extern "C" void UnauthorizedAccessException__ctor_m10943 ();
extern "C" void UnauthorizedAccessException__ctor_m10944 ();
extern "C" void UnauthorizedAccessException__ctor_m10945 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m10946 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m2142 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m10947 ();
extern "C" void UnitySerializationHolder__ctor_m10948 ();
extern "C" void UnitySerializationHolder_GetTypeData_m10949 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m10950 ();
extern "C" void UnitySerializationHolder_GetModuleData_m10951 ();
extern "C" void UnitySerializationHolder_GetObjectData_m10952 ();
extern "C" void UnitySerializationHolder_GetRealObject_m10953 ();
extern "C" void Version__ctor_m10954 ();
extern "C" void Version__ctor_m581 ();
extern "C" void Version__ctor_m4840 ();
extern "C" void Version__ctor_m10955 ();
extern "C" void Version__ctor_m10956 ();
extern "C" void Version_CheckedSet_m10957 ();
extern "C" void Version_get_Build_m10958 ();
extern "C" void Version_get_Major_m10959 ();
extern "C" void Version_get_Minor_m10960 ();
extern "C" void Version_get_Revision_m10961 ();
extern "C" void Version_Clone_m10962 ();
extern "C" void Version_CompareTo_m10963 ();
extern "C" void Version_Equals_m10964 ();
extern "C" void Version_CompareTo_m10965 ();
extern "C" void Version_Equals_m10966 ();
extern "C" void Version_GetHashCode_m10967 ();
extern "C" void Version_ToString_m10968 ();
extern "C" void Version_CreateFromString_m10969 ();
extern "C" void Version_op_Equality_m10970 ();
extern "C" void Version_op_Inequality_m10971 ();
extern "C" void Version_op_LessThan_m582 ();
extern "C" void WeakReference__ctor_m10972 ();
extern "C" void WeakReference__ctor_m10973 ();
extern "C" void WeakReference__ctor_m10974 ();
extern "C" void WeakReference__ctor_m10975 ();
extern "C" void WeakReference_AllocateHandle_m10976 ();
extern "C" void WeakReference_get_Target_m10977 ();
extern "C" void WeakReference_get_TrackResurrection_m10978 ();
extern "C" void WeakReference_Finalize_m10979 ();
extern "C" void WeakReference_GetObjectData_m10980 ();
extern "C" void PrimalityTest__ctor_m10981 ();
extern "C" void PrimalityTest_Invoke_m10982 ();
extern "C" void PrimalityTest_BeginInvoke_m10983 ();
extern "C" void PrimalityTest_EndInvoke_m10984 ();
extern "C" void MemberFilter__ctor_m10985 ();
extern "C" void MemberFilter_Invoke_m10986 ();
extern "C" void MemberFilter_BeginInvoke_m10987 ();
extern "C" void MemberFilter_EndInvoke_m10988 ();
extern "C" void TypeFilter__ctor_m10989 ();
extern "C" void TypeFilter_Invoke_m10990 ();
extern "C" void TypeFilter_BeginInvoke_m10991 ();
extern "C" void TypeFilter_EndInvoke_m10992 ();
extern "C" void CrossContextDelegate__ctor_m10993 ();
extern "C" void CrossContextDelegate_Invoke_m10994 ();
extern "C" void CrossContextDelegate_BeginInvoke_m10995 ();
extern "C" void CrossContextDelegate_EndInvoke_m10996 ();
extern "C" void HeaderHandler__ctor_m10997 ();
extern "C" void HeaderHandler_Invoke_m10998 ();
extern "C" void HeaderHandler_BeginInvoke_m10999 ();
extern "C" void HeaderHandler_EndInvoke_m11000 ();
extern "C" void ThreadStart__ctor_m11001 ();
extern "C" void ThreadStart_Invoke_m11002 ();
extern "C" void ThreadStart_BeginInvoke_m11003 ();
extern "C" void ThreadStart_EndInvoke_m11004 ();
extern "C" void TimerCallback__ctor_m11005 ();
extern "C" void TimerCallback_Invoke_m11006 ();
extern "C" void TimerCallback_BeginInvoke_m11007 ();
extern "C" void TimerCallback_EndInvoke_m11008 ();
extern "C" void WaitCallback__ctor_m11009 ();
extern "C" void WaitCallback_Invoke_m11010 ();
extern "C" void WaitCallback_BeginInvoke_m11011 ();
extern "C" void WaitCallback_EndInvoke_m11012 ();
extern "C" void AppDomainInitializer__ctor_m11013 ();
extern "C" void AppDomainInitializer_Invoke_m11014 ();
extern "C" void AppDomainInitializer_BeginInvoke_m11015 ();
extern "C" void AppDomainInitializer_EndInvoke_m11016 ();
extern "C" void AssemblyLoadEventHandler__ctor_m11017 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m11018 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m11019 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m11020 ();
extern "C" void EventHandler__ctor_m11021 ();
extern "C" void EventHandler_Invoke_m594 ();
extern "C" void EventHandler_BeginInvoke_m11022 ();
extern "C" void EventHandler_EndInvoke_m11023 ();
extern "C" void ResolveEventHandler__ctor_m11024 ();
extern "C" void ResolveEventHandler_Invoke_m11025 ();
extern "C" void ResolveEventHandler_BeginInvoke_m11026 ();
extern "C" void ResolveEventHandler_EndInvoke_m11027 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m2140 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m11028 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m11029 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m11030 ();
extern const methodPointerType g_MethodPointers[10698] = 
{
	Teleport__ctor_m0,
	Teleport_Start_m1,
	Teleport_SetGazedAt_m2,
	Teleport_Reset_m3,
	Teleport_ToggleVRMode_m4,
	Teleport_TeleportRandomly_m5,
	TeleportLegacyUI__ctor_m6,
	TeleportLegacyUI_Awake_m7,
	TeleportLegacyUI_Update_m8,
	TeleportLegacyUI_OnGUI_m9,
	TeleportLegacyUI_OnDestroy_m10,
	OnGUICallback__ctor_m11,
	OnGUICallback_Invoke_m12,
	OnGUICallback_BeginInvoke_m13,
	OnGUICallback_EndInvoke_m14,
	CardboardOnGUI__ctor_m15,
	CardboardOnGUI_add_onGUICallback_m16,
	CardboardOnGUI_remove_onGUICallback_m17,
	CardboardOnGUI_OKToDraw_m18,
	CardboardOnGUI_get_IsGUIVisible_m19,
	CardboardOnGUI_set_IsGUIVisible_m20,
	CardboardOnGUI_get_Triggered_m21,
	CardboardOnGUI_Awake_m22,
	CardboardOnGUI_Start_m23,
	CardboardOnGUI_Create_m24,
	CardboardOnGUI_LateUpdate_m25,
	CardboardOnGUI_OnGUI_m26,
	CardboardOnGUIMouse__ctor_m27,
	CardboardOnGUIMouse_LateUpdate_m28,
	CardboardOnGUIMouse_DrawPointerImage_m29,
	CardboardOnGUIWindow__ctor_m30,
	CardboardOnGUIWindow_Reset_m31,
	CardboardOnGUIWindow_Awake_m32,
	CardboardOnGUIWindow_Create_m33,
	CardboardOnGUIWindow_OnDisable_m34,
	CardboardOnGUIWindow_LateUpdate_m35,
	SkyboxMesh__ctor_m36,
	SkyboxMesh_Awake_m37,
	StereoLensFlare__ctor_m38,
	StereoLensFlare_Awake_m39,
	U3CEndOfFrameU3Ec__Iterator0__ctor_m40,
	U3CEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41,
	U3CEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m42,
	U3CEndOfFrameU3Ec__Iterator0_MoveNext_m43,
	U3CEndOfFrameU3Ec__Iterator0_Dispose_m44,
	U3CEndOfFrameU3Ec__Iterator0_Reset_m45,
	Cardboard__ctor_m46,
	Cardboard__cctor_m47,
	Cardboard_add_OnTrigger_m48,
	Cardboard_remove_OnTrigger_m49,
	Cardboard_add_OnTilt_m50,
	Cardboard_remove_OnTilt_m51,
	Cardboard_get_SDK_m52,
	Cardboard_get_DistortionCorrection_m53,
	Cardboard_set_DistortionCorrection_m54,
	Cardboard_get_VRModeEnabled_m55,
	Cardboard_set_VRModeEnabled_m56,
	Cardboard_get_EnableAlignmentMarker_m57,
	Cardboard_set_EnableAlignmentMarker_m58,
	Cardboard_get_EnableSettingsButton_m59,
	Cardboard_set_EnableSettingsButton_m60,
	Cardboard_get_TapIsTrigger_m61,
	Cardboard_set_TapIsTrigger_m62,
	Cardboard_get_NeckModelScale_m63,
	Cardboard_set_NeckModelScale_m64,
	Cardboard_get_AutoDriftCorrection_m65,
	Cardboard_set_AutoDriftCorrection_m66,
	Cardboard_get_SyncWithCardboardApp_m67,
	Cardboard_set_SyncWithCardboardApp_m68,
	Cardboard_get_NativeDistortionCorrectionSupported_m69,
	Cardboard_set_NativeDistortionCorrectionSupported_m70,
	Cardboard_get_NativeUILayerSupported_m71,
	Cardboard_set_NativeUILayerSupported_m72,
	Cardboard_get_StereoScreen_m73,
	Cardboard_set_StereoScreen_m74,
	Cardboard_get_UseDistortionEffect_m75,
	Cardboard_get_Profile_m76,
	Cardboard_get_HeadPose_m77,
	Cardboard_EyePose_m78,
	Cardboard_Projection_m79,
	Cardboard_Viewport_m80,
	Cardboard_get_ComfortableViewingRange_m81,
	Cardboard_InitDevice_m82,
	Cardboard_Awake_m83,
	Cardboard_get_Triggered_m84,
	Cardboard_set_Triggered_m85,
	Cardboard_get_Tilted_m86,
	Cardboard_set_Tilted_m87,
	Cardboard_UpdateState_m88,
	Cardboard_DispatchEvents_m89,
	Cardboard_AddDummyCamera_m90,
	Cardboard_EndOfFrame_m91,
	Cardboard_CreateStereoScreen_m92,
	Cardboard_Recenter_m93,
	Cardboard_SetTouchCoordinates_m94,
	Cardboard_ShowSettingsDialog_m95,
	Cardboard_OnEnable_m96,
	Cardboard_OnDisable_m97,
	Cardboard_OnApplicationPause_m98,
	Cardboard_OnApplicationFocus_m99,
	Cardboard_OnLevelWasLoaded_m100,
	Cardboard_OnDestroy_m101,
	Cardboard_OnApplicationQuit_m102,
	Cardboard_get_nativeDistortionCorrection_m103,
	Cardboard_set_nativeDistortionCorrection_m104,
	Cardboard_get_InCardboard_m105,
	Cardboard_get_CardboardTriggered_m106,
	Cardboard_get_HeadView_m107,
	Cardboard_get_HeadRotation_m108,
	Cardboard_get_HeadPosition_m109,
	Cardboard_EyeView_m110,
	Cardboard_EyeOffset_m111,
	Cardboard_UndistortedProjection_m112,
	Cardboard_EyeRect_m113,
	Cardboard_get_MinimumComfortDistance_m114,
	Cardboard_get_MaximumComfortDistance_m115,
	CardboardEye__ctor_m116,
	CardboardEye_get_Controller_m117,
	CardboardEye_get_Head_m118,
	CardboardEye_Awake_m119,
	CardboardEye_Start_m120,
	CardboardEye_FixProjection_m121,
	CardboardEye_Setup_m122,
	CardboardEye_Render_m123,
	CardboardEye_OnPreCull_m124,
	CardboardEye_CopyCameraAndMakeSideBySide_m125,
	CardboardHead__ctor_m126,
	CardboardHead_get_Gaze_m127,
	CardboardHead_Update_m128,
	CardboardHead_LateUpdate_m129,
	CardboardHead_UpdateHead_m130,
	Distortion_distort_m131,
	CardboardProfile__ctor_m132,
	CardboardProfile__cctor_m133,
	CardboardProfile_Clone_m134,
	CardboardProfile_get_VerticalLensOffset_m135,
	CardboardProfile_GetKnownProfile_m136,
	CardboardProfile_GetLeftEyeVisibleTanAngles_m137,
	CardboardProfile_GetLeftEyeNoLensTanAngles_m138,
	CardboardProfile_GetLeftEyeVisibleScreenRect_m139,
	CardboardProfile_solveLeastSquares_m140,
	CardboardProfile_ApproximateInverse_m141,
	CardboardProfile_ApproximateInverse_m142,
	CardboardUILayer__ctor_m143,
	CardboardUILayer__cctor_m144,
	CardboardUILayer_ComputeMatrix_m145,
	CardboardUILayer_Draw_m146,
	GazeInputModule__ctor_m147,
	GazeInputModule_ShouldActivateModule_m148,
	GazeInputModule_DeactivateModule_m149,
	GazeInputModule_IsPointerOverGameObject_m150,
	GazeInputModule_Process_m151,
	GazeInputModule_CastRayFromGaze_m152,
	GazeInputModule_UpdateCurrentObject_m153,
	GazeInputModule_PlaceCursor_m154,
	GazeInputModule_HandlePendingClick_m155,
	GazeInputModule_HandleTrigger_m156,
	Pose3D__ctor_m157,
	Pose3D__ctor_m158,
	Pose3D__ctor_m159,
	Pose3D__cctor_m160,
	Pose3D_get_Position_m161,
	Pose3D_set_Position_m162,
	Pose3D_get_Orientation_m163,
	Pose3D_set_Orientation_m164,
	Pose3D_get_Matrix_m165,
	Pose3D_set_Matrix_m166,
	Pose3D_get_RightHandedMatrix_m167,
	Pose3D_Set_m168,
	Pose3D_Set_m169,
	MutablePose3D__ctor_m170,
	MutablePose3D_Set_m171,
	MutablePose3D_Set_m172,
	MutablePose3D_SetRightHanded_m173,
	RadialUndistortionEffect__ctor_m174,
	RadialUndistortionEffect_Awake_m175,
	RadialUndistortionEffect_OnRenderImage_m176,
	U3CEndOfFrameU3Ec__Iterator1__ctor_m177,
	U3CEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m178,
	U3CEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m179,
	U3CEndOfFrameU3Ec__Iterator1_MoveNext_m180,
	U3CEndOfFrameU3Ec__Iterator1_Dispose_m181,
	U3CEndOfFrameU3Ec__Iterator1_Reset_m182,
	StereoController__ctor_m183,
	StereoController_get_Eyes_m184,
	StereoController_InvalidateEyes_m185,
	StereoController_get_Head_m186,
	StereoController_get_StereoScreen_m187,
	StereoController_get_ScreenHeight_m188,
	StereoController_Awake_m189,
	StereoController_AddStereoRig_m190,
	StereoController_CreateEye_m191,
	StereoController_ComputeStereoAdjustment_m192,
	StereoController_OnEnable_m193,
	StereoController_OnDisable_m194,
	StereoController_OnPreCull_m195,
	StereoController_EndOfFrame_m196,
	StereoController_U3Cget_EyesU3Em__0_m197,
	StereoController_U3Cget_HeadU3Em__1_m198,
	VREventCallback__ctor_m199,
	VREventCallback_Invoke_m200,
	VREventCallback_BeginInvoke_m201,
	VREventCallback_EndInvoke_m202,
	BaseCardboardDevice__ctor_m203,
	BaseCardboardDevice_SupportsNativeDistortionCorrection_m204,
	BaseCardboardDevice_SupportsNativeUILayer_m205,
	BaseCardboardDevice_SetDistortionCorrectionEnabled_m206,
	BaseCardboardDevice_SetAlignmentMarkerEnabled_m207,
	BaseCardboardDevice_SetNeckModelScale_m208,
	BaseCardboardDevice_SetAutoDriftCorrectionEnabled_m209,
	BaseCardboardDevice_SetElectronicDisplayStabilizationEnabled_m210,
	BaseCardboardDevice_Init_m211,
	BaseCardboardDevice_SetStereoScreen_m212,
	BaseCardboardDevice_UpdateState_m213,
	BaseCardboardDevice_UpdateScreenData_m214,
	BaseCardboardDevice_Recenter_m215,
	BaseCardboardDevice_PostRender_m216,
	BaseCardboardDevice_OnApplicationQuit_m217,
	BaseCardboardDevice_UpdateView_m218,
	BaseCardboardDevice_UpdateProfile_m219,
	BaseCardboardDevice_ExtractMatrix_m220,
	BaseCardboardDevice_ProcessEvents_m221,
	BaseCardboardDevice_OnVREvent_m222,
	BaseCardboardDevice_Start_m223,
	BaseCardboardDevice_SetEventCallback_m224,
	BaseCardboardDevice_SetTextureId_m225,
	BaseCardboardDevice_EnableDistortionCorrection_m226,
	BaseCardboardDevice_EnableAlignmentMarker_m227,
	BaseCardboardDevice_EnableAutoDriftCorrection_m228,
	BaseCardboardDevice_EnableElectronicDisplayStabilization_m229,
	BaseCardboardDevice_SetNeckModelFactor_m230,
	BaseCardboardDevice_ResetHeadTracker_m231,
	BaseCardboardDevice_GetProfile_m232,
	BaseCardboardDevice_GetHeadPose_m233,
	BaseCardboardDevice_GetViewParameters_m234,
	BaseCardboardDevice_Stop_m235,
	BaseVRDevice__ctor_m236,
	BaseVRDevice__cctor_m237,
	BaseVRDevice_get_Profile_m238,
	BaseVRDevice_set_Profile_m239,
	BaseVRDevice_SupportsNativeDistortionCorrection_m240,
	BaseVRDevice_SupportsNativeUILayer_m241,
	BaseVRDevice_SupportsUnityRenderEvent_m242,
	BaseVRDevice_CreateStereoScreen_m243,
	BaseVRDevice_SetDefaultDeviceProfile_m244,
	BaseVRDevice_ShowSettingsDialog_m245,
	BaseVRDevice_GetHeadPose_m246,
	BaseVRDevice_GetEyePose_m247,
	BaseVRDevice_GetProjection_m248,
	BaseVRDevice_GetViewport_m249,
	BaseVRDevice_SetTouchCoordinates_m250,
	BaseVRDevice_OnPause_m251,
	BaseVRDevice_OnFocus_m252,
	BaseVRDevice_Reset_m253,
	BaseVRDevice_OnApplicationQuit_m254,
	BaseVRDevice_Destroy_m255,
	BaseVRDevice_ComputeEyesFromProfile_m256,
	BaseVRDevice_MakeProjection_m257,
	BaseVRDevice_GetDevice_m258,
	CardboardiOSDevice__ctor_m259,
	CardboardiOSDevice_SupportsNativeDistortionCorrection_m260,
	CardboardiOSDevice_SupportsNativeUILayer_m261,
	CardboardiOSDevice_SetVRModeEnabled_m262,
	CardboardiOSDevice_SetSettingsButtonEnabled_m263,
	CardboardiOSDevice_SetAutoDriftCorrectionEnabled_m264,
	CardboardiOSDevice_SetTapIsTrigger_m265,
	CardboardiOSDevice_SetDefaultDeviceProfile_m266,
	CardboardiOSDevice_Init_m267,
	CardboardiOSDevice_PostRender_m268,
	CardboardiOSDevice_OnFocus_m269,
	CardboardiOSDevice_OnPause_m270,
	CardboardiOSDevice_ShowSettingsDialog_m271,
	CardboardiOSDevice_isOpenGLAPI_m272,
	CardboardiOSDevice_setVRModeEnabled_m273,
	CardboardiOSDevice_setSettingsButtonEnabled_m274,
	CardboardiOSDevice_setSyncWithCardboardEnabled_m275,
	CardboardiOSDevice_readProfile_m276,
	CardboardiOSDevice_setDefaultDeviceProfile_m277,
	CardboardiOSDevice_isOnboardingDone_m278,
	CardboardiOSDevice_launchSettingsDialog_m279,
	CardboardiOSDevice_launchOnboardingDialog_m280,
	DiveJava__ctor_m281,
	DiveJava__cctor_m282,
	DiveJava_Start_m283,
	DiveJava_Update_m284,
	DiveJava_setFullscreen_m285,
	DiveJava_init_m286,
	DiveMouseLook__ctor_m287,
	DiveMouseLook_Update_m288,
	DiveMouseLook_Start_m289,
	NaturalOrientation__ctor_m290,
	NaturalOrientation__cctor_m291,
	OffsetCenter__ctor_m292,
	OffsetCenter_Start_m293,
	OffsetCenter_Update_m294,
	OffsetCenter_SetVanishingPoint_m295,
	OffsetCenter_PerspectiveOffCenter_m296,
	OffsetCenter_setCorrectionFactor_m297,
	OpenDiveSensor__ctor_m298,
	OpenDiveSensor_add_MagnetTriggered_m299,
	OpenDiveSensor_remove_MagnetTriggered_m300,
	OpenDiveSensor_initialize_sensors_m301,
	OpenDiveSensor_stop_sensors_m302,
	OpenDiveSensor_get_q0_m303,
	OpenDiveSensor_get_q1_m304,
	OpenDiveSensor_get_q2_m305,
	OpenDiveSensor_get_q3_m306,
	OpenDiveSensor_DiveUpdateGyroData_m307,
	OpenDiveSensor_get_q_m308,
	OpenDiveSensor_get_magnet_m309,
	OpenDiveSensor_get_m_m310,
	OpenDiveSensor_Start_m311,
	OpenDiveSensor_OnMagnetTriggered_m312,
	OpenDiveSensor_Update_m313,
	OpenDiveSensor_OnGUI_m314,
	OpenDiveSensor_OnApplicationQuit_m315,
	DInput__cctor_m316,
	DInput_save_m317,
	DInput_load_m318,
	U3CPrivateImplementationDetailsU3E__ctor_m319,
	U24__ctor_m616,
	U24_MoveNext_m617,
	U24DieU248__ctor_m618,
	U24DieU248_GetEnumerator_m619,
	DiveFPSController__ctor_m620,
	DiveFPSController_Awake_m621,
	DiveFPSController_toggle_autowalk_m622,
	DiveFPSController_JumpUp_m623,
	DiveFPSController_Start_m624,
	DiveFPSController_OnControllerColliderHit_m625,
	DiveFPSController_Die_m626,
	DiveFPSController_Update_m627,
	DiveFPSController_LateUpdate_m628,
	DiveFPSController_OnGUI_m629,
	DiveFPSController_OnTriggerEnter_m630,
	DiveFPSController_OnTriggerExit_m631,
	DiveFPSController_Main_m632,
	fps__ctor_m633,
	fps_Start_m634,
	fps_Update_m635,
	fps_Main_m636,
	U24__ctor_m637,
	U24_MoveNext_m638,
	U24StartU2417__ctor_m639,
	U24StartU2417_GetEnumerator_m640,
	U24__ctor_m641,
	U24_MoveNext_m642,
	U24FadeGUITextureU2422__ctor_m643,
	U24FadeGUITextureU2422_GetEnumerator_m644,
	splashscreen__ctor_m645,
	splashscreen_Start_m646,
	splashscreen_FadeGUITexture_m647,
	splashscreen_Main_m648,
	AssetBundleCreateRequest__ctor_m702,
	AssetBundleCreateRequest_get_assetBundle_m703,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m704,
	AssetBundleRequest__ctor_m705,
	AssetBundleRequest_get_asset_m706,
	AssetBundleRequest_get_allAssets_m707,
	AssetBundle_LoadAsset_m708,
	AssetBundle_LoadAsset_Internal_m709,
	AssetBundle_LoadAssetWithSubAssets_Internal_m710,
	SystemInfo_get_supportsRenderTextures_m346,
	WaitForSeconds__ctor_m694,
	WaitForFixedUpdate__ctor_m711,
	WaitForEndOfFrame__ctor_m391,
	Coroutine__ctor_m712,
	Coroutine_ReleaseCoroutine_m713,
	Coroutine_Finalize_m714,
	ScriptableObject__ctor_m715,
	ScriptableObject_Internal_CreateScriptableObject_m716,
	ScriptableObject_CreateInstance_m717,
	ScriptableObject_CreateInstance_m718,
	ScriptableObject_CreateInstanceFromType_m719,
	UnhandledExceptionHandler__ctor_m720,
	UnhandledExceptionHandler_RegisterUECatcher_m721,
	UnhandledExceptionHandler_HandleUnhandledException_m722,
	UnhandledExceptionHandler_PrintException_m723,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m724,
	GameCenterPlatform__ctor_m725,
	GameCenterPlatform__cctor_m726,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m727,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m728,
	GameCenterPlatform_Internal_Authenticate_m729,
	GameCenterPlatform_Internal_Authenticated_m730,
	GameCenterPlatform_Internal_UserName_m731,
	GameCenterPlatform_Internal_UserID_m732,
	GameCenterPlatform_Internal_Underage_m733,
	GameCenterPlatform_Internal_UserImage_m734,
	GameCenterPlatform_Internal_LoadFriends_m735,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m736,
	GameCenterPlatform_Internal_LoadAchievements_m737,
	GameCenterPlatform_Internal_ReportProgress_m738,
	GameCenterPlatform_Internal_ReportScore_m739,
	GameCenterPlatform_Internal_LoadScores_m740,
	GameCenterPlatform_Internal_ShowAchievementsUI_m741,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m742,
	GameCenterPlatform_Internal_LoadUsers_m743,
	GameCenterPlatform_Internal_ResetAllAchievements_m744,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m745,
	GameCenterPlatform_ResetAllAchievements_m746,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m747,
	GameCenterPlatform_ShowLeaderboardUI_m748,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m749,
	GameCenterPlatform_ClearAchievementDescriptions_m750,
	GameCenterPlatform_SetAchievementDescription_m751,
	GameCenterPlatform_SetAchievementDescriptionImage_m752,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m753,
	GameCenterPlatform_AuthenticateCallbackWrapper_m754,
	GameCenterPlatform_ClearFriends_m755,
	GameCenterPlatform_SetFriends_m756,
	GameCenterPlatform_SetFriendImage_m757,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m758,
	GameCenterPlatform_AchievementCallbackWrapper_m759,
	GameCenterPlatform_ProgressCallbackWrapper_m760,
	GameCenterPlatform_ScoreCallbackWrapper_m761,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m762,
	GameCenterPlatform_get_localUser_m763,
	GameCenterPlatform_PopulateLocalUser_m764,
	GameCenterPlatform_LoadAchievementDescriptions_m765,
	GameCenterPlatform_ReportProgress_m766,
	GameCenterPlatform_LoadAchievements_m767,
	GameCenterPlatform_ReportScore_m768,
	GameCenterPlatform_LoadScores_m769,
	GameCenterPlatform_LoadScores_m770,
	GameCenterPlatform_LeaderboardCallbackWrapper_m771,
	GameCenterPlatform_GetLoading_m772,
	GameCenterPlatform_VerifyAuthentication_m773,
	GameCenterPlatform_ShowAchievementsUI_m774,
	GameCenterPlatform_ShowLeaderboardUI_m775,
	GameCenterPlatform_ClearUsers_m776,
	GameCenterPlatform_SetUser_m777,
	GameCenterPlatform_SetUserImage_m778,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m779,
	GameCenterPlatform_LoadUsers_m780,
	GameCenterPlatform_SafeSetUserImage_m781,
	GameCenterPlatform_SafeClearArray_m782,
	GameCenterPlatform_CreateLeaderboard_m783,
	GameCenterPlatform_CreateAchievement_m784,
	GameCenterPlatform_TriggerResetAchievementCallback_m785,
	GcLeaderboard__ctor_m786,
	GcLeaderboard_Finalize_m787,
	GcLeaderboard_Contains_m788,
	GcLeaderboard_SetScores_m789,
	GcLeaderboard_SetLocalScore_m790,
	GcLeaderboard_SetMaxRange_m791,
	GcLeaderboard_SetTitle_m792,
	GcLeaderboard_Internal_LoadScores_m793,
	GcLeaderboard_Internal_LoadScoresWithUsers_m794,
	GcLeaderboard_Loading_m795,
	GcLeaderboard_Dispose_m796,
	QualitySettings_get_activeColorSpace_m797,
	Mesh__ctor_m798,
	Mesh_Internal_Create_m799,
	Mesh_Clear_m800,
	Mesh_Clear_m801,
	Mesh_get_vertices_m802,
	Mesh_SetVertices_m803,
	Mesh_SetVerticesInternal_m804,
	Mesh_get_normals_m805,
	Mesh_SetNormals_m806,
	Mesh_SetNormalsInternal_m807,
	Mesh_get_tangents_m808,
	Mesh_SetTangents_m809,
	Mesh_SetTangentsInternal_m810,
	Mesh_get_uv_m811,
	Mesh_get_uv2_m812,
	Mesh_SetUVs_m813,
	Mesh_SetUVInternal_m814,
	Mesh_get_colors32_m815,
	Mesh_SetColors_m816,
	Mesh_SetColors32Internal_m817,
	Mesh_RecalculateBounds_m818,
	Mesh_SetTriangles_m819,
	Mesh_SetTrianglesInternal_m820,
	Mesh_GetIndices_m821,
	BoneWeight_get_weight0_m822,
	BoneWeight_set_weight0_m823,
	BoneWeight_get_weight1_m824,
	BoneWeight_set_weight1_m825,
	BoneWeight_get_weight2_m826,
	BoneWeight_set_weight2_m827,
	BoneWeight_get_weight3_m828,
	BoneWeight_set_weight3_m829,
	BoneWeight_get_boneIndex0_m830,
	BoneWeight_set_boneIndex0_m831,
	BoneWeight_get_boneIndex1_m832,
	BoneWeight_set_boneIndex1_m833,
	BoneWeight_get_boneIndex2_m834,
	BoneWeight_set_boneIndex2_m835,
	BoneWeight_get_boneIndex3_m836,
	BoneWeight_set_boneIndex3_m837,
	BoneWeight_GetHashCode_m838,
	BoneWeight_Equals_m839,
	BoneWeight_op_Equality_m840,
	BoneWeight_op_Inequality_m841,
	Renderer_set_enabled_m385,
	Renderer_get_material_m324,
	Renderer_set_material_m384,
	Renderer_get_sortingLayerID_m842,
	Renderer_get_sortingOrder_m843,
	Graphics_DrawTexture_m468,
	Graphics_DrawTexture_m844,
	Graphics_DrawTexture_m845,
	Graphics_DrawTexture_m846,
	Graphics_DrawTexture_m847,
	Graphics_Blit_m548,
	Graphics_Blit_m549,
	Graphics_Blit_m848,
	Graphics_Internal_BlitMaterial_m849,
	Screen_get_width_m353,
	Screen_get_height_m355,
	Screen_get_dpi_m494,
	Screen_set_sleepTimeout_m408,
	GL_Vertex3_m499,
	GL_Begin_m498,
	GL_End_m500,
	GL_LoadPixelMatrix_m497,
	GL_LoadPixelMatrixArgs_m850,
	GL_LoadPixelMatrix_m466,
	GL_PushMatrix_m465,
	GL_PopMatrix_m469,
	GL_Clear_m363,
	GL_Clear_m851,
	GL_Internal_Clear_m852,
	GL_INTERNAL_CALL_Internal_Clear_m853,
	GL_InvalidateState_m570,
	GL_IssuePluginEvent_m569,
	GUITexture_get_color_m659,
	GUITexture_set_color_m660,
	GUITexture_INTERNAL_get_color_m854,
	GUITexture_INTERNAL_set_color_m855,
	GUITexture_set_texture_m658,
	GUITexture_set_pixelInset_m654,
	GUITexture_INTERNAL_set_pixelInset_m856,
	GUILayer_HitTest_m857,
	GUILayer_INTERNAL_CALL_HitTest_m858,
	Texture__ctor_m859,
	Texture_Internal_GetWidth_m860,
	Texture_Internal_GetHeight_m861,
	Texture_get_width_m862,
	Texture_set_width_m863,
	Texture_get_height_m864,
	Texture_set_height_m865,
	Texture_GetNativeTextureID_m566,
	Texture2D__ctor_m655,
	Texture2D_Internal_Create_m866,
	Texture2D_get_whiteTexture_m867,
	Texture2D_SetPixel_m656,
	Texture2D_INTERNAL_CALL_SetPixel_m868,
	Texture2D_GetPixelBilinear_m869,
	Texture2D_Apply_m870,
	Texture2D_Apply_m657,
	RenderTexture__ctor_m356,
	RenderTexture_Internal_CreateRenderTexture_m871,
	RenderTexture_GetTemporary_m872,
	RenderTexture_GetTemporary_m464,
	RenderTexture_ReleaseTemporary_m470,
	RenderTexture_Internal_GetWidth_m873,
	RenderTexture_Internal_SetWidth_m874,
	RenderTexture_Internal_GetHeight_m875,
	RenderTexture_Internal_SetHeight_m876,
	RenderTexture_Internal_SetSRGBReadWrite_m877,
	RenderTexture_get_width_m352,
	RenderTexture_set_width_m878,
	RenderTexture_get_height_m354,
	RenderTexture_set_height_m879,
	RenderTexture_get_depth_m462,
	RenderTexture_set_depth_m880,
	RenderTexture_get_format_m463,
	RenderTexture_set_format_m881,
	RenderTexture_Create_m357,
	RenderTexture_INTERNAL_CALL_Create_m882,
	RenderTexture_Release_m350,
	RenderTexture_INTERNAL_CALL_Release_m883,
	RenderTexture_IsCreated_m401,
	RenderTexture_INTERNAL_CALL_IsCreated_m884,
	RenderTexture_get_active_m361,
	RenderTexture_set_active_m362,
	StateChanged__ctor_m885,
	StateChanged_Invoke_m886,
	StateChanged_BeginInvoke_m887,
	StateChanged_EndInvoke_m888,
	CullingGroup_Finalize_m889,
	CullingGroup_Dispose_m890,
	CullingGroup_SendEvents_m891,
	CullingGroup_FinalizerFailure_m892,
	GradientColorKey__ctor_m893,
	GradientAlphaKey__ctor_m894,
	Gradient__ctor_m895,
	Gradient_Init_m896,
	Gradient_Cleanup_m897,
	Gradient_Finalize_m898,
	TouchScreenKeyboard__ctor_m899,
	TouchScreenKeyboard_Destroy_m900,
	TouchScreenKeyboard_Finalize_m901,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m902,
	TouchScreenKeyboard_get_isSupported_m903,
	TouchScreenKeyboard_Open_m904,
	TouchScreenKeyboard_Open_m905,
	TouchScreenKeyboard_Open_m906,
	TouchScreenKeyboard_get_text_m907,
	TouchScreenKeyboard_set_text_m908,
	TouchScreenKeyboard_set_hideInput_m909,
	TouchScreenKeyboard_get_active_m910,
	TouchScreenKeyboard_set_active_m911,
	TouchScreenKeyboard_get_done_m912,
	TouchScreenKeyboard_get_wasCanceled_m913,
	LayerMask_get_value_m473,
	LayerMask_set_value_m914,
	LayerMask_LayerToName_m915,
	LayerMask_NameToLayer_m916,
	LayerMask_GetMask_m917,
	LayerMask_op_Implicit_m918,
	LayerMask_op_Implicit_m423,
	Vector2__ctor_m373,
	Vector2_get_Item_m919,
	Vector2_set_Item_m920,
	Vector2_Scale_m921,
	Vector2_Normalize_m922,
	Vector2_get_normalized_m436,
	Vector2_ToString_m923,
	Vector2_GetHashCode_m924,
	Vector2_Equals_m925,
	Vector2_Dot_m926,
	Vector2_get_magnitude_m927,
	Vector2_get_sqrMagnitude_m374,
	Vector2_SqrMagnitude_m928,
	Vector2_get_zero_m365,
	Vector2_get_one_m929,
	Vector2_get_up_m930,
	Vector2_op_Addition_m931,
	Vector2_op_Subtraction_m932,
	Vector2_op_Multiply_m437,
	Vector2_op_Division_m933,
	Vector2_op_Equality_m934,
	Vector2_op_Inequality_m935,
	Vector2_op_Implicit_m435,
	Vector2_op_Implicit_m936,
	Vector3__ctor_m386,
	Vector3__ctor_m937,
	Vector3_Lerp_m679,
	Vector3_get_Item_m938,
	Vector3_set_Item_m939,
	Vector3_GetHashCode_m940,
	Vector3_Equals_m941,
	Vector3_Normalize_m942,
	Vector3_get_normalized_m943,
	Vector3_ToString_m944,
	Vector3_ToString_m945,
	Vector3_Dot_m946,
	Vector3_Distance_m947,
	Vector3_Magnitude_m948,
	Vector3_get_magnitude_m562,
	Vector3_SqrMagnitude_m949,
	Vector3_get_sqrMagnitude_m950,
	Vector3_Min_m951,
	Vector3_Max_m952,
	Vector3_get_zero_m397,
	Vector3_get_one_m477,
	Vector3_get_forward_m442,
	Vector3_get_back_m953,
	Vector3_get_up_m954,
	Vector3_get_down_m955,
	Vector3_get_left_m956,
	Vector3_get_right_m474,
	Vector3_op_Addition_m443,
	Vector3_op_Subtraction_m561,
	Vector3_op_Multiply_m332,
	Vector3_op_Multiply_m441,
	Vector3_op_Division_m675,
	Vector3_op_Equality_m957,
	Vector3_op_Inequality_m674,
	Color__ctor_m958,
	Color__ctor_m492,
	Color_ToString_m959,
	Color_GetHashCode_m960,
	Color_Equals_m961,
	Color_Lerp_m962,
	Color_get_red_m326,
	Color_get_green_m325,
	Color_get_white_m963,
	Color_get_black_m416,
	Color_get_clear_m339,
	Color_op_Multiply_m964,
	Color_op_Implicit_m965,
	Color_op_Implicit_m966,
	Color32__ctor_m967,
	Color32_ToString_m968,
	Color32_op_Implicit_m969,
	Color32_op_Implicit_m970,
	Quaternion__ctor_m971,
	Quaternion_get_identity_m475,
	Quaternion_Dot_m972,
	Quaternion_AngleAxis_m596,
	Quaternion_INTERNAL_CALL_AngleAxis_m973,
	Quaternion_LookRotation_m547,
	Quaternion_INTERNAL_CALL_LookRotation_m974,
	Quaternion_Inverse_m975,
	Quaternion_INTERNAL_CALL_Inverse_m976,
	Quaternion_ToString_m977,
	Quaternion_get_eulerAngles_m680,
	Quaternion_Euler_m681,
	Quaternion_Internal_ToEulerRad_m978,
	Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m979,
	Quaternion_Internal_FromEulerRad_m980,
	Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m981,
	Quaternion_GetHashCode_m982,
	Quaternion_Equals_m983,
	Quaternion_op_Multiply_m485,
	Quaternion_op_Multiply_m487,
	Quaternion_op_Inequality_m984,
	Rect__ctor_m337,
	Rect_Set_m571,
	Rect_get_x_m452,
	Rect_set_x_m453,
	Rect_get_y_m456,
	Rect_set_y_m457,
	Rect_get_position_m380,
	Rect_get_center_m478,
	Rect_set_center_m479,
	Rect_get_min_m985,
	Rect_get_max_m986,
	Rect_get_width_m370,
	Rect_set_width_m454,
	Rect_get_height_m372,
	Rect_set_height_m455,
	Rect_get_size_m382,
	Rect_get_xMin_m369,
	Rect_get_yMin_m371,
	Rect_get_xMax_m583,
	Rect_get_yMax_m987,
	Rect_ToString_m988,
	Rect_Contains_m989,
	Rect_Overlaps_m990,
	Rect_GetHashCode_m991,
	Rect_Equals_m992,
	Rect_op_Inequality_m993,
	Rect_op_Equality_m994,
	Matrix4x4_get_Item_m433,
	Matrix4x4_set_Item_m434,
	Matrix4x4_get_Item_m995,
	Matrix4x4_set_Item_m996,
	Matrix4x4_GetHashCode_m997,
	Matrix4x4_Equals_m998,
	Matrix4x4_Inverse_m999,
	Matrix4x4_INTERNAL_CALL_Inverse_m1000,
	Matrix4x4_Transpose_m1001,
	Matrix4x4_INTERNAL_CALL_Transpose_m1002,
	Matrix4x4_Invert_m1003,
	Matrix4x4_INTERNAL_CALL_Invert_m1004,
	Matrix4x4_get_inverse_m568,
	Matrix4x4_get_transpose_m1005,
	Matrix4x4_get_isIdentity_m1006,
	Matrix4x4_GetColumn_m545,
	Matrix4x4_GetRow_m1007,
	Matrix4x4_SetColumn_m1008,
	Matrix4x4_SetRow_m1009,
	Matrix4x4_MultiplyPoint_m1010,
	Matrix4x4_MultiplyPoint3x4_m1011,
	Matrix4x4_MultiplyVector_m1012,
	Matrix4x4_Scale_m543,
	Matrix4x4_get_zero_m584,
	Matrix4x4_get_identity_m542,
	Matrix4x4_SetTRS_m1013,
	Matrix4x4_TRS_m495,
	Matrix4x4_INTERNAL_CALL_TRS_m1014,
	Matrix4x4_ToString_m1015,
	Matrix4x4_ToString_m1016,
	Matrix4x4_Ortho_m1017,
	Matrix4x4_Perspective_m1018,
	Matrix4x4_op_Multiply_m544,
	Matrix4x4_op_Multiply_m1019,
	Matrix4x4_op_Equality_m1020,
	Matrix4x4_op_Inequality_m1021,
	Bounds__ctor_m1022,
	Bounds_GetHashCode_m1023,
	Bounds_Equals_m1024,
	Bounds_get_center_m1025,
	Bounds_set_center_m1026,
	Bounds_get_size_m1027,
	Bounds_set_size_m1028,
	Bounds_get_extents_m1029,
	Bounds_set_extents_m1030,
	Bounds_get_min_m1031,
	Bounds_set_min_m1032,
	Bounds_get_max_m1033,
	Bounds_set_max_m1034,
	Bounds_SetMinMax_m1035,
	Bounds_Encapsulate_m1036,
	Bounds_Encapsulate_m1037,
	Bounds_Expand_m1038,
	Bounds_Expand_m1039,
	Bounds_Intersects_m1040,
	Bounds_Internal_Contains_m1041,
	Bounds_INTERNAL_CALL_Internal_Contains_m1042,
	Bounds_Contains_m1043,
	Bounds_Internal_SqrDistance_m1044,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m1045,
	Bounds_SqrDistance_m1046,
	Bounds_Internal_IntersectRay_m1047,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m1048,
	Bounds_IntersectRay_m1049,
	Bounds_IntersectRay_m1050,
	Bounds_Internal_GetClosestPoint_m1051,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m1052,
	Bounds_ClosestPoint_m1053,
	Bounds_ToString_m1054,
	Bounds_ToString_m1055,
	Bounds_op_Equality_m1056,
	Bounds_op_Inequality_m1057,
	Vector4__ctor_m448,
	Vector4__ctor_m451,
	Vector4_get_Item_m1058,
	Vector4_set_Item_m1059,
	Vector4_GetHashCode_m1060,
	Vector4_Equals_m1061,
	Vector4_ToString_m1062,
	Vector4_Dot_m1063,
	Vector4_SqrMagnitude_m1064,
	Vector4_get_sqrMagnitude_m1065,
	Vector4_get_zero_m1066,
	Vector4_op_Subtraction_m1067,
	Vector4_op_Division_m449,
	Vector4_op_Equality_m1068,
	Vector4_op_Implicit_m546,
	Ray__ctor_m483,
	Ray_get_origin_m1069,
	Ray_get_direction_m1070,
	Ray_GetPoint_m1071,
	Ray_ToString_m1072,
	Plane__ctor_m1073,
	Plane_get_normal_m1074,
	Plane_get_distance_m1075,
	Plane_Raycast_m1076,
	MathfInternal__cctor_m1077,
	Mathf__cctor_m1078,
	Mathf_Sin_m1079,
	Mathf_Cos_m1080,
	Mathf_Atan_m1081,
	Mathf_Sqrt_m1082,
	Mathf_Abs_m1083,
	Mathf_Min_m676,
	Mathf_Min_m565,
	Mathf_Max_m1084,
	Mathf_Max_m564,
	Mathf_Pow_m1085,
	Mathf_Log_m1086,
	Mathf_Floor_m1087,
	Mathf_Round_m1088,
	Mathf_CeilToInt_m1089,
	Mathf_FloorToInt_m1090,
	Mathf_RoundToInt_m1091,
	Mathf_Sign_m1092,
	Mathf_Clamp_m330,
	Mathf_Clamp_m1093,
	Mathf_Clamp01_m398,
	Mathf_Lerp_m439,
	Mathf_SmoothStep_m480,
	Mathf_Approximately_m399,
	Mathf_SmoothDamp_m1094,
	Mathf_Repeat_m1095,
	Mathf_InverseLerp_m1096,
	DrivenRectTransformTracker_Add_m1097,
	DrivenRectTransformTracker_Clear_m1098,
	ReapplyDrivenProperties__ctor_m1099,
	ReapplyDrivenProperties_Invoke_m1100,
	ReapplyDrivenProperties_BeginInvoke_m1101,
	ReapplyDrivenProperties_EndInvoke_m1102,
	RectTransform_add_reapplyDrivenProperties_m1103,
	RectTransform_remove_reapplyDrivenProperties_m1104,
	RectTransform_get_rect_m1105,
	RectTransform_INTERNAL_get_rect_m1106,
	RectTransform_get_anchorMin_m1107,
	RectTransform_set_anchorMin_m1108,
	RectTransform_INTERNAL_get_anchorMin_m1109,
	RectTransform_INTERNAL_set_anchorMin_m1110,
	RectTransform_get_anchorMax_m1111,
	RectTransform_set_anchorMax_m1112,
	RectTransform_INTERNAL_get_anchorMax_m1113,
	RectTransform_INTERNAL_set_anchorMax_m1114,
	RectTransform_get_anchoredPosition_m1115,
	RectTransform_set_anchoredPosition_m1116,
	RectTransform_INTERNAL_get_anchoredPosition_m1117,
	RectTransform_INTERNAL_set_anchoredPosition_m1118,
	RectTransform_get_sizeDelta_m1119,
	RectTransform_set_sizeDelta_m1120,
	RectTransform_INTERNAL_get_sizeDelta_m1121,
	RectTransform_INTERNAL_set_sizeDelta_m1122,
	RectTransform_get_pivot_m1123,
	RectTransform_set_pivot_m1124,
	RectTransform_INTERNAL_get_pivot_m1125,
	RectTransform_INTERNAL_set_pivot_m1126,
	RectTransform_SendReapplyDrivenProperties_m1127,
	RectTransform_GetLocalCorners_m1128,
	RectTransform_GetWorldCorners_m1129,
	RectTransform_SetInsetAndSizeFromParentEdge_m1130,
	RectTransform_SetSizeWithCurrentAnchors_m1131,
	RectTransform_GetParentSize_m1132,
	ResourceRequest__ctor_m1133,
	ResourceRequest_get_asset_m1134,
	Resources_Load_m1135,
	SerializePrivateVariables__ctor_m1136,
	SerializeField__ctor_m1137,
	Shader_Find_m377,
	Shader_SetGlobalColor_m1138,
	Shader_SetGlobalColor_m1139,
	Shader_INTERNAL_CALL_SetGlobalColor_m1140,
	Shader_SetGlobalVector_m450,
	Shader_PropertyToID_m1141,
	Material__ctor_m378,
	Material__ctor_m1142,
	Material_set_color_m327,
	Material_get_mainTexture_m1143,
	Material_set_mainTexture_m379,
	Material_set_mainTextureOffset_m381,
	Material_set_mainTextureScale_m383,
	Material_SetColor_m1144,
	Material_SetColor_m1145,
	Material_INTERNAL_CALL_SetColor_m1146,
	Material_SetTexture_m1147,
	Material_SetTexture_m1148,
	Material_GetTexture_m1149,
	Material_GetTexture_m1150,
	Material_SetTextureOffset_m1151,
	Material_INTERNAL_CALL_SetTextureOffset_m1152,
	Material_SetTextureScale_m1153,
	Material_INTERNAL_CALL_SetTextureScale_m1154,
	Material_SetFloat_m1155,
	Material_SetFloat_m1156,
	Material_SetInt_m1157,
	Material_HasProperty_m1158,
	Material_HasProperty_m1159,
	Material_SetPass_m496,
	Material_Internal_CreateWithShader_m1160,
	Material_Internal_CreateWithMaterial_m1161,
	SortingLayer_GetLayerValueFromID_m1162,
	SphericalHarmonicsL2_Clear_m1163,
	SphericalHarmonicsL2_ClearInternal_m1164,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m1165,
	SphericalHarmonicsL2_AddAmbientLight_m1166,
	SphericalHarmonicsL2_AddAmbientLightInternal_m1167,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m1168,
	SphericalHarmonicsL2_AddDirectionalLight_m1169,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m1170,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m1171,
	SphericalHarmonicsL2_get_Item_m1172,
	SphericalHarmonicsL2_set_Item_m1173,
	SphericalHarmonicsL2_GetHashCode_m1174,
	SphericalHarmonicsL2_Equals_m1175,
	SphericalHarmonicsL2_op_Multiply_m1176,
	SphericalHarmonicsL2_op_Multiply_m1177,
	SphericalHarmonicsL2_op_Addition_m1178,
	SphericalHarmonicsL2_op_Equality_m1179,
	SphericalHarmonicsL2_op_Inequality_m1180,
	Sprite_get_rect_m1181,
	Sprite_INTERNAL_get_rect_m1182,
	Sprite_get_pixelsPerUnit_m1183,
	Sprite_get_texture_m1184,
	Sprite_get_textureRect_m1185,
	Sprite_INTERNAL_get_textureRect_m1186,
	Sprite_get_border_m1187,
	Sprite_INTERNAL_get_border_m1188,
	DataUtility_GetInnerUV_m1189,
	DataUtility_GetOuterUV_m1190,
	DataUtility_GetPadding_m1191,
	DataUtility_GetMinSize_m1192,
	DataUtility_Internal_GetMinSize_m1193,
	UnityString_Format_m1194,
	AsyncOperation__ctor_m1195,
	AsyncOperation_InternalDestroy_m1196,
	AsyncOperation_Finalize_m1197,
	LogCallback__ctor_m1198,
	LogCallback_Invoke_m1199,
	LogCallback_BeginInvoke_m1200,
	LogCallback_EndInvoke_m1201,
	Application_get_loadedLevel_m662,
	Application_LoadLevel_m696,
	Application_LoadLevelAsync_m663,
	Application_LoadLevelAsync_m1202,
	Application_get_isPlaying_m426,
	Application_get_isEditor_m425,
	Application_get_platform_m591,
	Application_get_isMobilePlatform_m577,
	Application_get_unityVersion_m579,
	Application_set_targetFrameRate_m407,
	Application_CallLogCallback_m1203,
	Behaviour__ctor_m1204,
	Behaviour_get_enabled_m343,
	Behaviour_set_enabled_m348,
	Behaviour_get_isActiveAndEnabled_m1205,
	CameraCallback__ctor_m1206,
	CameraCallback_Invoke_m1207,
	CameraCallback_BeginInvoke_m1208,
	CameraCallback_EndInvoke_m1209,
	Camera_set_fieldOfView_m447,
	Camera_get_nearClipPlane_m444,
	Camera_get_farClipPlane_m445,
	Camera_get_depth_m1210,
	Camera_set_depth_m420,
	Camera_get_cullingMask_m472,
	Camera_set_cullingMask_m418,
	Camera_get_eventMask_m1211,
	Camera_set_backgroundColor_m417,
	Camera_INTERNAL_set_backgroundColor_m1212,
	Camera_get_rect_m432,
	Camera_set_rect_m458,
	Camera_INTERNAL_get_rect_m1213,
	Camera_INTERNAL_set_rect_m1214,
	Camera_get_pixelRect_m461,
	Camera_INTERNAL_get_pixelRect_m1215,
	Camera_get_targetTexture_m467,
	Camera_set_targetTexture_m459,
	Camera_get_projectionMatrix_m438,
	Camera_set_projectionMatrix_m446,
	Camera_INTERNAL_get_projectionMatrix_m1216,
	Camera_INTERNAL_set_projectionMatrix_m1217,
	Camera_get_clearFlags_m1218,
	Camera_set_clearFlags_m415,
	Camera_ScreenToViewportPoint_m1219,
	Camera_INTERNAL_CALL_ScreenToViewportPoint_m1220,
	Camera_ScreenPointToRay_m1221,
	Camera_INTERNAL_CALL_ScreenPointToRay_m1222,
	Camera_get_main_m333,
	Camera_get_allCamerasCount_m1223,
	Camera_GetAllCameras_m1224,
	Camera_FireOnPreCull_m1225,
	Camera_FireOnPreRender_m1226,
	Camera_FireOnPostRender_m1227,
	Camera_Render_m460,
	Camera_set_useOcclusionCulling_m419,
	Camera_CopyFrom_m471,
	Camera_RaycastTry_m1228,
	Camera_INTERNAL_CALL_RaycastTry_m1229,
	Camera_RaycastTry2D_m1230,
	Camera_INTERNAL_CALL_RaycastTry2D_m1231,
	Debug_DrawLine_m673,
	Debug_INTERNAL_CALL_DrawLine_m1232,
	Debug_Internal_Log_m1233,
	Debug_Internal_LogException_m1234,
	Debug_Log_m388,
	Debug_LogError_m400,
	Debug_LogError_m1235,
	Debug_LogException_m1236,
	Debug_LogException_m1237,
	Debug_LogWarning_m347,
	Debug_LogWarning_m1238,
	DisplaysUpdatedDelegate__ctor_m1239,
	DisplaysUpdatedDelegate_Invoke_m1240,
	DisplaysUpdatedDelegate_BeginInvoke_m1241,
	DisplaysUpdatedDelegate_EndInvoke_m1242,
	Display__ctor_m1243,
	Display__ctor_m1244,
	Display__cctor_m1245,
	Display_add_onDisplaysUpdated_m1246,
	Display_remove_onDisplaysUpdated_m1247,
	Display_get_renderingWidth_m1248,
	Display_get_renderingHeight_m1249,
	Display_get_systemWidth_m1250,
	Display_get_systemHeight_m1251,
	Display_get_colorBuffer_m1252,
	Display_get_depthBuffer_m1253,
	Display_Activate_m1254,
	Display_Activate_m1255,
	Display_SetParams_m1256,
	Display_SetRenderingResolution_m1257,
	Display_MultiDisplayLicense_m1258,
	Display_RelativeMouseAt_m1259,
	Display_get_main_m1260,
	Display_RecreateDisplayList_m1261,
	Display_FireDisplaysUpdated_m1262,
	Display_GetSystemExtImpl_m1263,
	Display_GetRenderingExtImpl_m1264,
	Display_GetRenderingBuffersImpl_m1265,
	Display_SetRenderingResolutionImpl_m1266,
	Display_ActivateDisplayImpl_m1267,
	Display_SetParamsImpl_m1268,
	Display_MultiDisplayLicenseImpl_m1269,
	Display_RelativeMouseAtImpl_m1270,
	MonoBehaviour__ctor_m320,
	MonoBehaviour_StartCoroutine_m1271,
	MonoBehaviour_StartCoroutine_Auto_m672,
	MonoBehaviour_StartCoroutine_m1272,
	MonoBehaviour_StartCoroutine_m421,
	MonoBehaviour_StopCoroutine_m422,
	MonoBehaviour_StopCoroutine_m1273,
	MonoBehaviour_StopCoroutine_m1274,
	MonoBehaviour_StopCoroutineViaEnumerator_Auto_m1275,
	MonoBehaviour_StopCoroutine_Auto_m1276,
	MonoBehaviour_print_m683,
	Touch_get_fingerId_m1277,
	Touch_get_position_m1278,
	Touch_get_phase_m1279,
	Input__cctor_m1280,
	Input_GetKeyDownInt_m1281,
	Input_GetAxis_m588,
	Input_GetAxisRaw_m1282,
	Input_GetButton_m677,
	Input_GetButtonDown_m1283,
	Input_GetKeyDown_m410,
	Input_GetMouseButton_m525,
	Input_GetMouseButtonDown_m409,
	Input_GetMouseButtonUp_m1284,
	Input_get_mousePosition_m1285,
	Input_INTERNAL_get_mousePosition_m1286,
	Input_get_mouseScrollDelta_m1287,
	Input_INTERNAL_get_mouseScrollDelta_m1288,
	Input_get_mousePresent_m1289,
	Input_GetTouch_m1290,
	Input_get_touchCount_m1291,
	Input_get_touchSupported_m1292,
	Input_set_imeCompositionMode_m1293,
	Input_get_compositionString_m1294,
	Input_set_compositionCursorPos_m1295,
	Input_INTERNAL_set_compositionCursorPos_m1296,
	Object__ctor_m1297,
	Object_Internal_CloneSingle_m1298,
	Object_Destroy_m1299,
	Object_Destroy_m389,
	Object_DestroyImmediate_m1300,
	Object_DestroyImmediate_m1301,
	Object_FindObjectsOfType_m1302,
	Object_get_name_m558,
	Object_set_name_m1303,
	Object_set_hideFlags_m1304,
	Object_ToString_m1305,
	Object_Equals_m1306,
	Object_GetHashCode_m1307,
	Object_CompareBaseObjects_m1308,
	Object_IsNativeObjectAlive_m1309,
	Object_GetInstanceID_m1310,
	Object_GetCachedPtr_m1311,
	Object_CheckNullArgument_m1312,
	Object_FindObjectOfType_m1313,
	Object_op_Implicit_m351,
	Object_op_Equality_m342,
	Object_op_Inequality_m349,
	Component__ctor_m1314,
	Component_get_transform_m321,
	Component_get_gameObject_m344,
	Component_GetComponent_m666,
	Component_GetComponentFastPath_m1315,
	Component_GetComponentInChildren_m1316,
	Component_GetComponentInParent_m1317,
	Component_GetComponentsForListInternal_m1318,
	Component_GetComponents_m1319,
	GameObject__ctor_m394,
	GameObject__ctor_m650,
	GameObject_GetComponent_m1320,
	GameObject_GetComponentFastPath_m1321,
	GameObject_GetComponentInChildren_m1322,
	GameObject_GetComponentInParent_m1323,
	GameObject_GetComponentsInternal_m1324,
	GameObject_get_transform_m396,
	GameObject_get_layer_m1325,
	GameObject_set_layer_m1326,
	GameObject_SetActive_m507,
	GameObject_get_activeSelf_m1327,
	GameObject_get_activeInHierarchy_m345,
	GameObject_SendMessage_m1328,
	GameObject_Internal_AddComponentWithType_m1329,
	GameObject_AddComponent_m652,
	GameObject_Internal_CreateGameObject_m1330,
	Enumerator__ctor_m1331,
	Enumerator_get_Current_m1332,
	Enumerator_MoveNext_m1333,
	Enumerator_Reset_m1334,
	Transform_get_position_m481,
	Transform_set_position_m488,
	Transform_INTERNAL_get_position_m1335,
	Transform_INTERNAL_set_position_m1336,
	Transform_get_localPosition_m322,
	Transform_set_localPosition_m328,
	Transform_INTERNAL_get_localPosition_m1337,
	Transform_INTERNAL_set_localPosition_m1338,
	Transform_get_localEulerAngles_m587,
	Transform_set_localEulerAngles_m589,
	Transform_INTERNAL_get_localEulerAngles_m1339,
	Transform_INTERNAL_set_localEulerAngles_m1340,
	Transform_get_forward_m482,
	Transform_get_rotation_m484,
	Transform_set_rotation_m486,
	Transform_INTERNAL_get_rotation_m1341,
	Transform_INTERNAL_set_rotation_m1342,
	Transform_get_localRotation_m1343,
	Transform_set_localRotation_m476,
	Transform_INTERNAL_get_localRotation_m1344,
	Transform_INTERNAL_set_localRotation_m1345,
	Transform_get_localScale_m1346,
	Transform_set_localScale_m387,
	Transform_INTERNAL_get_localScale_m1347,
	Transform_INTERNAL_set_localScale_m1348,
	Transform_get_parent_m424,
	Transform_set_parent_m413,
	Transform_get_parentInternal_m1349,
	Transform_set_parentInternal_m1350,
	Transform_SetParent_m1351,
	Transform_SetParent_m1352,
	Transform_get_worldToLocalMatrix_m1353,
	Transform_INTERNAL_get_worldToLocalMatrix_m1354,
	Transform_Rotate_m1355,
	Transform_Rotate_m590,
	Transform_Rotate_m1356,
	Transform_TransformPoint_m1357,
	Transform_INTERNAL_CALL_TransformPoint_m1358,
	Transform_InverseTransformPoint_m1359,
	Transform_INTERNAL_CALL_InverseTransformPoint_m1360,
	Transform_get_childCount_m1361,
	Transform_SetAsFirstSibling_m1362,
	Transform_get_lossyScale_m440,
	Transform_INTERNAL_get_lossyScale_m1363,
	Transform_IsChildOf_m1364,
	Transform_GetEnumerator_m1365,
	Transform_GetChild_m1366,
	Time_get_timeSinceLevelLoad_m595,
	Time_get_deltaTime_m586,
	Time_get_unscaledTime_m523,
	Time_get_unscaledDeltaTime_m1367,
	Time_get_smoothDeltaTime_m567,
	Time_get_timeScale_m689,
	Random_get_value_m331,
	Random_get_onUnitSphere_m329,
	Random_INTERNAL_get_onUnitSphere_m1368,
	YieldInstruction__ctor_m1369,
	PlayerPrefsException__ctor_m1370,
	PlayerPrefs_TrySetInt_m1371,
	PlayerPrefs_TrySetFloat_m1372,
	PlayerPrefs_SetInt_m598,
	PlayerPrefs_GetInt_m1373,
	PlayerPrefs_GetInt_m600,
	PlayerPrefs_SetFloat_m599,
	PlayerPrefs_GetFloat_m1374,
	PlayerPrefs_GetFloat_m601,
	UnityAdsInternal__ctor_m1375,
	UnityAdsInternal_add_onCampaignsAvailable_m1376,
	UnityAdsInternal_remove_onCampaignsAvailable_m1377,
	UnityAdsInternal_add_onCampaignsFetchFailed_m1378,
	UnityAdsInternal_remove_onCampaignsFetchFailed_m1379,
	UnityAdsInternal_add_onShow_m1380,
	UnityAdsInternal_remove_onShow_m1381,
	UnityAdsInternal_add_onHide_m1382,
	UnityAdsInternal_remove_onHide_m1383,
	UnityAdsInternal_add_onVideoCompleted_m1384,
	UnityAdsInternal_remove_onVideoCompleted_m1385,
	UnityAdsInternal_add_onVideoStarted_m1386,
	UnityAdsInternal_remove_onVideoStarted_m1387,
	UnityAdsInternal_RegisterNative_m1388,
	UnityAdsInternal_Init_m1389,
	UnityAdsInternal_Show_m1390,
	UnityAdsInternal_CanShowAds_m1391,
	UnityAdsInternal_SetLogLevel_m1392,
	UnityAdsInternal_SetCampaignDataURL_m1393,
	UnityAdsInternal_RemoveAllEventHandlers_m1394,
	UnityAdsInternal_CallUnityAdsCampaignsAvailable_m1395,
	UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m1396,
	UnityAdsInternal_CallUnityAdsShow_m1397,
	UnityAdsInternal_CallUnityAdsHide_m1398,
	UnityAdsInternal_CallUnityAdsVideoCompleted_m1399,
	UnityAdsInternal_CallUnityAdsVideoStarted_m1400,
	Particle_get_position_m1401,
	Particle_set_position_m1402,
	Particle_get_velocity_m1403,
	Particle_set_velocity_m1404,
	Particle_get_energy_m1405,
	Particle_set_energy_m1406,
	Particle_get_startEnergy_m1407,
	Particle_set_startEnergy_m1408,
	Particle_get_size_m1409,
	Particle_set_size_m1410,
	Particle_get_rotation_m1411,
	Particle_set_rotation_m1412,
	Particle_get_angularVelocity_m1413,
	Particle_set_angularVelocity_m1414,
	Particle_get_color_m1415,
	Particle_set_color_m1416,
	ControllerColliderHit_get_gameObject_m671,
	ControllerColliderHit_get_normal_m669,
	ControllerColliderHit_get_moveDirection_m670,
	Physics_Raycast_m1417,
	Physics_Raycast_m1418,
	Physics_Raycast_m1419,
	Physics_RaycastAll_m1420,
	Physics_RaycastAll_m1421,
	Physics_RaycastAll_m1422,
	Physics_INTERNAL_CALL_RaycastAll_m1423,
	Physics_Internal_Raycast_m1424,
	Physics_INTERNAL_CALL_Internal_Raycast_m1425,
	Rigidbody_set_freezeRotation_m593,
	Collider_Internal_Raycast_m1426,
	Collider_INTERNAL_CALL_Internal_Raycast_m1427,
	Collider_Raycast_m336,
	RaycastHit_CalculateRaycastTexCoord_m1428,
	RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1429,
	RaycastHit_get_point_m1430,
	RaycastHit_get_normal_m1431,
	RaycastHit_get_distance_m367,
	RaycastHit_get_textureCoord_m368,
	RaycastHit_get_collider_m1432,
	CharacterController_Move_m682,
	CharacterController_INTERNAL_CALL_Move_m1433,
	CharacterController_get_slopeLimit_m667,
	Physics2D__cctor_m1434,
	Physics2D_Internal_Raycast_m1435,
	Physics2D_INTERNAL_CALL_Internal_Raycast_m1436,
	Physics2D_Raycast_m1437,
	Physics2D_Raycast_m1438,
	Physics2D_RaycastAll_m1439,
	Physics2D_INTERNAL_CALL_RaycastAll_m1440,
	RaycastHit2D_get_point_m1441,
	RaycastHit2D_get_normal_m1442,
	RaycastHit2D_get_fraction_m1443,
	RaycastHit2D_get_collider_m1444,
	RaycastHit2D_get_rigidbody_m1445,
	RaycastHit2D_get_transform_m1446,
	Collider2D_get_attachedRigidbody_m1447,
	AudioConfigurationChangeHandler__ctor_m1448,
	AudioConfigurationChangeHandler_Invoke_m1449,
	AudioConfigurationChangeHandler_BeginInvoke_m1450,
	AudioConfigurationChangeHandler_EndInvoke_m1451,
	AudioSettings_InvokeOnAudioConfigurationChanged_m1452,
	PCMReaderCallback__ctor_m1453,
	PCMReaderCallback_Invoke_m1454,
	PCMReaderCallback_BeginInvoke_m1455,
	PCMReaderCallback_EndInvoke_m1456,
	PCMSetPositionCallback__ctor_m1457,
	PCMSetPositionCallback_Invoke_m1458,
	PCMSetPositionCallback_BeginInvoke_m1459,
	PCMSetPositionCallback_EndInvoke_m1460,
	AudioClip_InvokePCMReaderCallback_Internal_m1461,
	AudioClip_InvokePCMSetPositionCallback_Internal_m1462,
	WebCamDevice_get_name_m1463,
	WebCamDevice_get_isFrontFacing_m1464,
	AnimationEvent__ctor_m1465,
	AnimationEvent_get_data_m1466,
	AnimationEvent_set_data_m1467,
	AnimationEvent_get_stringParameter_m1468,
	AnimationEvent_set_stringParameter_m1469,
	AnimationEvent_get_floatParameter_m1470,
	AnimationEvent_set_floatParameter_m1471,
	AnimationEvent_get_intParameter_m1472,
	AnimationEvent_set_intParameter_m1473,
	AnimationEvent_get_objectReferenceParameter_m1474,
	AnimationEvent_set_objectReferenceParameter_m1475,
	AnimationEvent_get_functionName_m1476,
	AnimationEvent_set_functionName_m1477,
	AnimationEvent_get_time_m1478,
	AnimationEvent_set_time_m1479,
	AnimationEvent_get_messageOptions_m1480,
	AnimationEvent_set_messageOptions_m1481,
	AnimationEvent_get_isFiredByLegacy_m1482,
	AnimationEvent_get_isFiredByAnimator_m1483,
	AnimationEvent_get_animationState_m1484,
	AnimationEvent_get_animatorStateInfo_m1485,
	AnimationEvent_get_animatorClipInfo_m1486,
	AnimationEvent_GetHash_m1487,
	AnimationCurve__ctor_m1488,
	AnimationCurve__ctor_m1489,
	AnimationCurve_Cleanup_m1490,
	AnimationCurve_Finalize_m1491,
	AnimationCurve_Init_m1492,
	AnimatorStateInfo_IsName_m1493,
	AnimatorStateInfo_get_fullPathHash_m1494,
	AnimatorStateInfo_get_nameHash_m1495,
	AnimatorStateInfo_get_shortNameHash_m1496,
	AnimatorStateInfo_get_normalizedTime_m1497,
	AnimatorStateInfo_get_length_m1498,
	AnimatorStateInfo_get_speed_m1499,
	AnimatorStateInfo_get_speedMultiplier_m1500,
	AnimatorStateInfo_get_tagHash_m1501,
	AnimatorStateInfo_IsTag_m1502,
	AnimatorStateInfo_get_loop_m1503,
	AnimatorTransitionInfo_IsName_m1504,
	AnimatorTransitionInfo_IsUserName_m1505,
	AnimatorTransitionInfo_get_fullPathHash_m1506,
	AnimatorTransitionInfo_get_nameHash_m1507,
	AnimatorTransitionInfo_get_userNameHash_m1508,
	AnimatorTransitionInfo_get_normalizedTime_m1509,
	AnimatorTransitionInfo_get_anyState_m1510,
	AnimatorTransitionInfo_get_entry_m1511,
	AnimatorTransitionInfo_get_exit_m1512,
	Animator_SetTrigger_m1513,
	Animator_ResetTrigger_m1514,
	Animator_get_runtimeAnimatorController_m1515,
	Animator_StringToHash_m1516,
	Animator_SetTriggerString_m1517,
	Animator_ResetTriggerString_m1518,
	HumanBone_get_boneName_m1519,
	HumanBone_set_boneName_m1520,
	HumanBone_get_humanName_m1521,
	HumanBone_set_humanName_m1522,
	GUIText_set_text_m692,
	CharacterInfo_get_advance_m1523,
	CharacterInfo_set_advance_m1524,
	CharacterInfo_get_glyphWidth_m1525,
	CharacterInfo_set_glyphWidth_m1526,
	CharacterInfo_get_glyphHeight_m1527,
	CharacterInfo_set_glyphHeight_m1528,
	CharacterInfo_get_bearing_m1529,
	CharacterInfo_set_bearing_m1530,
	CharacterInfo_get_minY_m1531,
	CharacterInfo_set_minY_m1532,
	CharacterInfo_get_maxY_m1533,
	CharacterInfo_set_maxY_m1534,
	CharacterInfo_get_minX_m1535,
	CharacterInfo_set_minX_m1536,
	CharacterInfo_get_maxX_m1537,
	CharacterInfo_set_maxX_m1538,
	CharacterInfo_get_uvBottomLeftUnFlipped_m1539,
	CharacterInfo_set_uvBottomLeftUnFlipped_m1540,
	CharacterInfo_get_uvBottomRightUnFlipped_m1541,
	CharacterInfo_set_uvBottomRightUnFlipped_m1542,
	CharacterInfo_get_uvTopRightUnFlipped_m1543,
	CharacterInfo_set_uvTopRightUnFlipped_m1544,
	CharacterInfo_get_uvTopLeftUnFlipped_m1545,
	CharacterInfo_set_uvTopLeftUnFlipped_m1546,
	CharacterInfo_get_uvBottomLeft_m1547,
	CharacterInfo_set_uvBottomLeft_m1548,
	CharacterInfo_get_uvBottomRight_m1549,
	CharacterInfo_set_uvBottomRight_m1550,
	CharacterInfo_get_uvTopRight_m1551,
	CharacterInfo_set_uvTopRight_m1552,
	CharacterInfo_get_uvTopLeft_m1553,
	CharacterInfo_set_uvTopLeft_m1554,
	FontTextureRebuildCallback__ctor_m1555,
	FontTextureRebuildCallback_Invoke_m1556,
	FontTextureRebuildCallback_BeginInvoke_m1557,
	FontTextureRebuildCallback_EndInvoke_m1558,
	Font__ctor_m1559,
	Font__ctor_m1560,
	Font__ctor_m1561,
	Font_add_textureRebuilt_m1562,
	Font_remove_textureRebuilt_m1563,
	Font_add_m_FontTextureRebuildCallback_m1564,
	Font_remove_m_FontTextureRebuildCallback_m1565,
	Font_GetOSInstalledFontNames_m1566,
	Font_Internal_CreateFont_m1567,
	Font_Internal_CreateDynamicFont_m1568,
	Font_CreateDynamicFontFromOSFont_m1569,
	Font_CreateDynamicFontFromOSFont_m1570,
	Font_get_material_m1571,
	Font_set_material_m1572,
	Font_HasCharacter_m1573,
	Font_get_fontNames_m1574,
	Font_set_fontNames_m1575,
	Font_get_characterInfo_m1576,
	Font_set_characterInfo_m1577,
	Font_RequestCharactersInTexture_m1578,
	Font_RequestCharactersInTexture_m1579,
	Font_RequestCharactersInTexture_m1580,
	Font_InvokeTextureRebuilt_Internal_m1581,
	Font_get_textureRebuildCallback_m1582,
	Font_set_textureRebuildCallback_m1583,
	Font_GetMaxVertsForString_m1584,
	Font_GetCharacterInfo_m1585,
	Font_GetCharacterInfo_m1586,
	Font_GetCharacterInfo_m1587,
	Font_get_dynamic_m1588,
	Font_get_ascent_m1589,
	Font_get_lineHeight_m1590,
	Font_get_fontSize_m1591,
	TextGenerator__ctor_m1592,
	TextGenerator__ctor_m1593,
	TextGenerator_System_IDisposable_Dispose_m1594,
	TextGenerator_Init_m1595,
	TextGenerator_Dispose_cpp_m1596,
	TextGenerator_Populate_Internal_m1597,
	TextGenerator_Populate_Internal_cpp_m1598,
	TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1599,
	TextGenerator_get_rectExtents_m1600,
	TextGenerator_INTERNAL_get_rectExtents_m1601,
	TextGenerator_get_vertexCount_m1602,
	TextGenerator_GetVerticesInternal_m1603,
	TextGenerator_GetVerticesArray_m1604,
	TextGenerator_get_characterCount_m1605,
	TextGenerator_get_characterCountVisible_m1606,
	TextGenerator_GetCharactersInternal_m1607,
	TextGenerator_GetCharactersArray_m1608,
	TextGenerator_get_lineCount_m1609,
	TextGenerator_GetLinesInternal_m1610,
	TextGenerator_GetLinesArray_m1611,
	TextGenerator_get_fontSizeUsedForBestFit_m1612,
	TextGenerator_Finalize_m1613,
	TextGenerator_ValidatedSettings_m1614,
	TextGenerator_Invalidate_m1615,
	TextGenerator_GetCharacters_m1616,
	TextGenerator_GetLines_m1617,
	TextGenerator_GetVertices_m1618,
	TextGenerator_GetPreferredWidth_m1619,
	TextGenerator_GetPreferredHeight_m1620,
	TextGenerator_Populate_m1621,
	TextGenerator_PopulateAlways_m1622,
	TextGenerator_get_verts_m1623,
	TextGenerator_get_characters_m1624,
	TextGenerator_get_lines_m1625,
	WillRenderCanvases__ctor_m1626,
	WillRenderCanvases_Invoke_m1627,
	WillRenderCanvases_BeginInvoke_m1628,
	WillRenderCanvases_EndInvoke_m1629,
	Canvas_add_willRenderCanvases_m1630,
	Canvas_remove_willRenderCanvases_m1631,
	Canvas_get_renderMode_m1632,
	Canvas_get_isRootCanvas_m1633,
	Canvas_get_worldCamera_m1634,
	Canvas_get_scaleFactor_m1635,
	Canvas_set_scaleFactor_m1636,
	Canvas_get_referencePixelsPerUnit_m1637,
	Canvas_set_referencePixelsPerUnit_m1638,
	Canvas_get_pixelPerfect_m1639,
	Canvas_get_renderOrder_m1640,
	Canvas_get_overrideSorting_m1641,
	Canvas_set_overrideSorting_m1642,
	Canvas_get_sortingOrder_m1643,
	Canvas_set_sortingOrder_m1644,
	Canvas_get_sortingLayerID_m1645,
	Canvas_set_sortingLayerID_m1646,
	Canvas_get_cachedSortingLayerValue_m1647,
	Canvas_GetDefaultCanvasMaterial_m1648,
	Canvas_SendWillRenderCanvases_m1649,
	Canvas_ForceUpdateCanvases_m1650,
	CanvasGroup_get_alpha_m1651,
	CanvasGroup_set_alpha_m1652,
	CanvasGroup_get_interactable_m1653,
	CanvasGroup_get_blocksRaycasts_m1654,
	CanvasGroup_get_ignoreParentGroups_m1655,
	CanvasGroup_IsRaycastLocationValid_m1656,
	UIVertex__cctor_m1657,
	CanvasRenderer_SetColor_m1658,
	CanvasRenderer_INTERNAL_CALL_SetColor_m1659,
	CanvasRenderer_GetColor_m1660,
	CanvasRenderer_EnableRectClipping_m1661,
	CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1662,
	CanvasRenderer_DisableRectClipping_m1663,
	CanvasRenderer_set_hasPopInstruction_m1664,
	CanvasRenderer_get_materialCount_m1665,
	CanvasRenderer_set_materialCount_m1666,
	CanvasRenderer_SetMaterial_m1667,
	CanvasRenderer_SetMaterial_m1668,
	CanvasRenderer_set_popMaterialCount_m1669,
	CanvasRenderer_SetPopMaterial_m1670,
	CanvasRenderer_SetTexture_m1671,
	CanvasRenderer_SetMesh_m1672,
	CanvasRenderer_Clear_m1673,
	CanvasRenderer_get_cull_m1674,
	CanvasRenderer_set_cull_m1675,
	CanvasRenderer_get_absoluteDepth_m1676,
	CanvasRenderer_get_hasMoved_m1677,
	RectTransformUtility__cctor_m1678,
	RectTransformUtility_RectangleContainsScreenPoint_m1679,
	RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1680,
	RectTransformUtility_PixelAdjustPoint_m1681,
	RectTransformUtility_PixelAdjustPoint_m1682,
	RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1683,
	RectTransformUtility_PixelAdjustRect_m1684,
	RectTransformUtility_ScreenPointToWorldPointInRectangle_m1685,
	RectTransformUtility_ScreenPointToLocalPointInRectangle_m1686,
	RectTransformUtility_ScreenPointToRay_m1687,
	RectTransformUtility_FlipLayoutOnAxis_m1688,
	RectTransformUtility_FlipLayoutAxes_m1689,
	RectTransformUtility_GetTransposed_m1690,
	Event__ctor_m1691,
	Event__ctor_m1692,
	Event__ctor_m1693,
	Event_Finalize_m1694,
	Event_get_mousePosition_m1695,
	Event_set_mousePosition_m1696,
	Event_get_delta_m1697,
	Event_set_delta_m1698,
	Event_get_mouseRay_m1699,
	Event_set_mouseRay_m1700,
	Event_get_shift_m1701,
	Event_set_shift_m1702,
	Event_get_control_m1703,
	Event_set_control_m1704,
	Event_get_alt_m1705,
	Event_set_alt_m1706,
	Event_get_command_m1707,
	Event_set_command_m1708,
	Event_get_capsLock_m1709,
	Event_set_capsLock_m1710,
	Event_get_numeric_m1711,
	Event_set_numeric_m1712,
	Event_get_functionKey_m1713,
	Event_get_current_m359,
	Event_set_current_m1714,
	Event_Internal_MakeMasterEventCurrent_m1715,
	Event_get_isKey_m684,
	Event_get_isMouse_m1716,
	Event_KeyboardEvent_m1717,
	Event_GetHashCode_m1718,
	Event_Equals_m1719,
	Event_ToString_m1720,
	Event_Init_m1721,
	Event_Cleanup_m1722,
	Event_InitCopy_m1723,
	Event_InitPtr_m1724,
	Event_get_rawType_m1725,
	Event_get_type_m360,
	Event_set_type_m1726,
	Event_GetTypeForControl_m1727,
	Event_Internal_SetMousePosition_m1728,
	Event_INTERNAL_CALL_Internal_SetMousePosition_m1729,
	Event_Internal_GetMousePosition_m1730,
	Event_Internal_SetMouseDelta_m1731,
	Event_INTERNAL_CALL_Internal_SetMouseDelta_m1732,
	Event_Internal_GetMouseDelta_m1733,
	Event_get_button_m1734,
	Event_set_button_m1735,
	Event_get_modifiers_m1736,
	Event_set_modifiers_m1737,
	Event_get_pressure_m1738,
	Event_set_pressure_m1739,
	Event_get_clickCount_m1740,
	Event_set_clickCount_m1741,
	Event_get_character_m1742,
	Event_set_character_m1743,
	Event_get_commandName_m1744,
	Event_set_commandName_m1745,
	Event_get_keyCode_m685,
	Event_set_keyCode_m1746,
	Event_Internal_SetNativeEvent_m1747,
	Event_Use_m1748,
	Event_PopEvent_m1749,
	Event_GetEventCount_m1750,
	ScrollViewState__ctor_m1751,
	WindowFunction__ctor_m1752,
	WindowFunction_Invoke_m1753,
	WindowFunction_BeginInvoke_m1754,
	WindowFunction_EndInvoke_m1755,
	GUI__cctor_m1756,
	GUI_set_nextScrollStepTime_m1757,
	GUI_set_skin_m1758,
	GUI_get_skin_m1759,
	GUI_DoSetSkin_m1760,
	GUI_Label_m686,
	GUI_Label_m1761,
	GUI_DrawTexture_m375,
	GUI_DrawTexture_m1762,
	GUI_DrawTexture_m597,
	GUI_Button_m338,
	GUI_CallWindowDelegate_m1763,
	GUI_get_color_m1764,
	GUI_INTERNAL_get_color_m1765,
	GUI_set_changed_m1766,
	GUI_DoLabel_m1767,
	GUI_INTERNAL_CALL_DoLabel_m1768,
	GUI_get_blendMaterial_m1769,
	GUI_get_blitMaterial_m1770,
	GUI_DoButton_m1771,
	GUI_INTERNAL_CALL_DoButton_m1772,
	GUIContent__ctor_m1773,
	GUIContent__ctor_m1774,
	GUIContent__cctor_m1775,
	GUIContent_Temp_m1776,
	GUIContent_ClearStaticCache_m1777,
	GUILayout_Width_m1778,
	GUILayout_Height_m1779,
	LayoutCache__ctor_m1780,
	GUILayoutUtility__cctor_m1781,
	GUILayoutUtility_SelectIDList_m1782,
	GUILayoutUtility_Begin_m1783,
	GUILayoutUtility_BeginWindow_m1784,
	GUILayoutUtility_Layout_m1785,
	GUILayoutUtility_LayoutFromEditorWindow_m1786,
	GUILayoutUtility_LayoutFreeGroup_m1787,
	GUILayoutUtility_LayoutSingleGroup_m1788,
	GUILayoutUtility_get_spaceStyle_m1789,
	GUILayoutUtility_Internal_GetWindowRect_m1790,
	GUILayoutUtility_Internal_MoveWindow_m1791,
	GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m1792,
	GUILayoutEntry__ctor_m1793,
	GUILayoutEntry__cctor_m1794,
	GUILayoutEntry_get_style_m1795,
	GUILayoutEntry_set_style_m1796,
	GUILayoutEntry_get_margin_m1797,
	GUILayoutEntry_CalcWidth_m1798,
	GUILayoutEntry_CalcHeight_m1799,
	GUILayoutEntry_SetHorizontal_m1800,
	GUILayoutEntry_SetVertical_m1801,
	GUILayoutEntry_ApplyStyleSettings_m1802,
	GUILayoutEntry_ApplyOptions_m1803,
	GUILayoutEntry_ToString_m1804,
	GUILayoutGroup__ctor_m1805,
	GUILayoutGroup_get_margin_m1806,
	GUILayoutGroup_ApplyOptions_m1807,
	GUILayoutGroup_ApplyStyleSettings_m1808,
	GUILayoutGroup_ResetCursor_m1809,
	GUILayoutGroup_CalcWidth_m1810,
	GUILayoutGroup_SetHorizontal_m1811,
	GUILayoutGroup_CalcHeight_m1812,
	GUILayoutGroup_SetVertical_m1813,
	GUILayoutGroup_ToString_m1814,
	GUIScrollGroup__ctor_m1815,
	GUIScrollGroup_CalcWidth_m1816,
	GUIScrollGroup_SetHorizontal_m1817,
	GUIScrollGroup_CalcHeight_m1818,
	GUIScrollGroup_SetVertical_m1819,
	GUILayoutOption__ctor_m1820,
	GUISettings__ctor_m1821,
	SkinChangedDelegate__ctor_m1822,
	SkinChangedDelegate_Invoke_m1823,
	SkinChangedDelegate_BeginInvoke_m1824,
	SkinChangedDelegate_EndInvoke_m1825,
	GUISkin__ctor_m1826,
	GUISkin_OnEnable_m1827,
	GUISkin_get_font_m1828,
	GUISkin_set_font_m1829,
	GUISkin_get_box_m1830,
	GUISkin_set_box_m1831,
	GUISkin_get_label_m1832,
	GUISkin_set_label_m1833,
	GUISkin_get_textField_m1834,
	GUISkin_set_textField_m1835,
	GUISkin_get_textArea_m1836,
	GUISkin_set_textArea_m1837,
	GUISkin_get_button_m1838,
	GUISkin_set_button_m1839,
	GUISkin_get_toggle_m1840,
	GUISkin_set_toggle_m1841,
	GUISkin_get_window_m1842,
	GUISkin_set_window_m1843,
	GUISkin_get_horizontalSlider_m1844,
	GUISkin_set_horizontalSlider_m1845,
	GUISkin_get_horizontalSliderThumb_m1846,
	GUISkin_set_horizontalSliderThumb_m1847,
	GUISkin_get_verticalSlider_m1848,
	GUISkin_set_verticalSlider_m1849,
	GUISkin_get_verticalSliderThumb_m1850,
	GUISkin_set_verticalSliderThumb_m1851,
	GUISkin_get_horizontalScrollbar_m1852,
	GUISkin_set_horizontalScrollbar_m1853,
	GUISkin_get_horizontalScrollbarThumb_m1854,
	GUISkin_set_horizontalScrollbarThumb_m1855,
	GUISkin_get_horizontalScrollbarLeftButton_m1856,
	GUISkin_set_horizontalScrollbarLeftButton_m1857,
	GUISkin_get_horizontalScrollbarRightButton_m1858,
	GUISkin_set_horizontalScrollbarRightButton_m1859,
	GUISkin_get_verticalScrollbar_m1860,
	GUISkin_set_verticalScrollbar_m1861,
	GUISkin_get_verticalScrollbarThumb_m1862,
	GUISkin_set_verticalScrollbarThumb_m1863,
	GUISkin_get_verticalScrollbarUpButton_m1864,
	GUISkin_set_verticalScrollbarUpButton_m1865,
	GUISkin_get_verticalScrollbarDownButton_m1866,
	GUISkin_set_verticalScrollbarDownButton_m1867,
	GUISkin_get_scrollView_m1868,
	GUISkin_set_scrollView_m1869,
	GUISkin_get_customStyles_m1870,
	GUISkin_set_customStyles_m1871,
	GUISkin_get_settings_m1872,
	GUISkin_get_error_m1873,
	GUISkin_Apply_m1874,
	GUISkin_BuildStyleCache_m1875,
	GUISkin_GetStyle_m1876,
	GUISkin_FindStyle_m1877,
	GUISkin_MakeCurrent_m1878,
	GUISkin_GetEnumerator_m1879,
	GUIStyleState__ctor_m1880,
	GUIStyleState__ctor_m1881,
	GUIStyleState_Finalize_m1882,
	GUIStyleState_Init_m1883,
	GUIStyleState_Cleanup_m1884,
	GUIStyleState_GetBackgroundInternal_m1885,
	GUIStyleState_set_textColor_m1886,
	GUIStyleState_INTERNAL_set_textColor_m1887,
	RectOffset__ctor_m1888,
	RectOffset__ctor_m1889,
	RectOffset_Finalize_m1890,
	RectOffset_ToString_m1891,
	RectOffset_Init_m1892,
	RectOffset_Cleanup_m1893,
	RectOffset_get_left_m1894,
	RectOffset_set_left_m1895,
	RectOffset_get_right_m1896,
	RectOffset_set_right_m1897,
	RectOffset_get_top_m1898,
	RectOffset_set_top_m1899,
	RectOffset_get_bottom_m1900,
	RectOffset_set_bottom_m1901,
	RectOffset_get_horizontal_m1902,
	RectOffset_get_vertical_m1903,
	GUIStyle__ctor_m1904,
	GUIStyle__cctor_m1905,
	GUIStyle_Finalize_m1906,
	GUIStyle_get_normal_m1907,
	GUIStyle_get_margin_m1908,
	GUIStyle_get_padding_m1909,
	GUIStyle_get_none_m1910,
	GUIStyle_ToString_m1911,
	GUIStyle_Init_m1912,
	GUIStyle_Cleanup_m1913,
	GUIStyle_get_name_m1914,
	GUIStyle_set_name_m1915,
	GUIStyle_GetStyleStatePtr_m1916,
	GUIStyle_GetRectOffsetPtr_m1917,
	GUIStyle_get_fixedWidth_m1918,
	GUIStyle_get_fixedHeight_m1919,
	GUIStyle_get_stretchWidth_m1920,
	GUIStyle_set_stretchWidth_m1921,
	GUIStyle_get_stretchHeight_m1922,
	GUIStyle_set_stretchHeight_m1923,
	GUIStyle_SetDefaultFont_m1924,
	GUIUtility__cctor_m1925,
	GUIUtility_get_pixelsPerPoint_m1926,
	GUIUtility_GetDefaultSkin_m1927,
	GUIUtility_BeginGUI_m1928,
	GUIUtility_EndGUI_m1929,
	GUIUtility_EndGUIFromException_m1930,
	GUIUtility_CheckOnGUI_m1931,
	GUIUtility_Internal_GetPixelsPerPoint_m1932,
	GUIUtility_get_systemCopyBuffer_m1933,
	GUIUtility_set_systemCopyBuffer_m1934,
	GUIUtility_Internal_GetDefaultSkin_m1935,
	GUIUtility_Internal_ExitGUI_m1936,
	GUIUtility_Internal_GetGUIDepth_m1937,
	MonoPInvokeCallbackAttribute__ctor_m1938,
	WrapperlessIcall__ctor_m1939,
	IL2CPPStructAlignmentAttribute__ctor_m1940,
	AttributeHelperEngine__cctor_m1941,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m1942,
	AttributeHelperEngine_GetRequiredComponents_m1943,
	AttributeHelperEngine_CheckIsEditorScript_m1944,
	DisallowMultipleComponent__ctor_m1945,
	RequireComponent__ctor_m1946,
	AddComponentMenu__ctor_m1947,
	AddComponentMenu__ctor_m1948,
	ExecuteInEditMode__ctor_m1949,
	HideInInspector__ctor_m1950,
	SetupCoroutine__ctor_m1951,
	SetupCoroutine_InvokeMember_m1952,
	SetupCoroutine_InvokeStatic_m1953,
	WritableAttribute__ctor_m1954,
	AssemblyIsEditorAssembly__ctor_m1955,
	GcUserProfileData_ToUserProfile_m1956,
	GcUserProfileData_AddToArray_m1957,
	GcAchievementDescriptionData_ToAchievementDescription_m1958,
	GcAchievementData_ToAchievement_m1959,
	GcScoreData_ToScore_m1960,
	Resolution_get_width_m1961,
	Resolution_set_width_m1962,
	Resolution_get_height_m1963,
	Resolution_set_height_m1964,
	Resolution_get_refreshRate_m1965,
	Resolution_set_refreshRate_m1966,
	Resolution_ToString_m1967,
	LocalUser__ctor_m1968,
	LocalUser_SetFriends_m1969,
	LocalUser_SetAuthenticated_m1970,
	LocalUser_SetUnderage_m1971,
	LocalUser_get_authenticated_m1972,
	UserProfile__ctor_m1973,
	UserProfile__ctor_m1974,
	UserProfile_ToString_m1975,
	UserProfile_SetUserName_m1976,
	UserProfile_SetUserID_m1977,
	UserProfile_SetImage_m1978,
	UserProfile_get_userName_m1979,
	UserProfile_get_id_m1980,
	UserProfile_get_isFriend_m1981,
	UserProfile_get_state_m1982,
	Achievement__ctor_m1983,
	Achievement__ctor_m1984,
	Achievement__ctor_m1985,
	Achievement_ToString_m1986,
	Achievement_get_id_m1987,
	Achievement_set_id_m1988,
	Achievement_get_percentCompleted_m1989,
	Achievement_set_percentCompleted_m1990,
	Achievement_get_completed_m1991,
	Achievement_get_hidden_m1992,
	Achievement_get_lastReportedDate_m1993,
	AchievementDescription__ctor_m1994,
	AchievementDescription_ToString_m1995,
	AchievementDescription_SetImage_m1996,
	AchievementDescription_get_id_m1997,
	AchievementDescription_set_id_m1998,
	AchievementDescription_get_title_m1999,
	AchievementDescription_get_achievedDescription_m2000,
	AchievementDescription_get_unachievedDescription_m2001,
	AchievementDescription_get_hidden_m2002,
	AchievementDescription_get_points_m2003,
	Score__ctor_m2004,
	Score__ctor_m2005,
	Score_ToString_m2006,
	Score_get_leaderboardID_m2007,
	Score_set_leaderboardID_m2008,
	Score_get_value_m2009,
	Score_set_value_m2010,
	Leaderboard__ctor_m2011,
	Leaderboard_ToString_m2012,
	Leaderboard_SetLocalUserScore_m2013,
	Leaderboard_SetMaxRange_m2014,
	Leaderboard_SetScores_m2015,
	Leaderboard_SetTitle_m2016,
	Leaderboard_GetUserFilter_m2017,
	Leaderboard_get_id_m2018,
	Leaderboard_set_id_m2019,
	Leaderboard_get_userScope_m2020,
	Leaderboard_set_userScope_m2021,
	Leaderboard_get_range_m2022,
	Leaderboard_set_range_m2023,
	Leaderboard_get_timeScope_m2024,
	Leaderboard_set_timeScope_m2025,
	HitInfo_SendMessage_m2026,
	HitInfo_Compare_m2027,
	HitInfo_op_Implicit_m2028,
	SendMouseEvents__cctor_m2029,
	SendMouseEvents_SetMouseMoved_m2030,
	SendMouseEvents_DoSendMouseEvents_m2031,
	SendMouseEvents_SendEvents_m2032,
	Range__ctor_m2033,
	PropertyAttribute__ctor_m2034,
	TooltipAttribute__ctor_m2035,
	SpaceAttribute__ctor_m2036,
	SpaceAttribute__ctor_m2037,
	RangeAttribute__ctor_m2038,
	TextAreaAttribute__ctor_m2039,
	SelectionBaseAttribute__ctor_m2040,
	SliderState__ctor_m2041,
	StackTraceUtility__ctor_m2042,
	StackTraceUtility__cctor_m2043,
	StackTraceUtility_SetProjectFolder_m2044,
	StackTraceUtility_ExtractStackTrace_m2045,
	StackTraceUtility_IsSystemStacktraceType_m2046,
	StackTraceUtility_ExtractStringFromException_m2047,
	StackTraceUtility_ExtractStringFromExceptionInternal_m2048,
	StackTraceUtility_PostprocessStacktrace_m2049,
	StackTraceUtility_ExtractFormattedStackTrace_m2050,
	UnityException__ctor_m2051,
	UnityException__ctor_m2052,
	UnityException__ctor_m2053,
	UnityException__ctor_m2054,
	SharedBetweenAnimatorsAttribute__ctor_m2055,
	StateMachineBehaviour__ctor_m2056,
	TextEditor__ctor_m2057,
	TextGenerationSettings_CompareColors_m2058,
	TextGenerationSettings_CompareVector2_m2059,
	TextGenerationSettings_Equals_m2060,
	TrackedReference_Equals_m2061,
	TrackedReference_GetHashCode_m2062,
	TrackedReference_op_Equality_m2063,
	ArgumentCache__ctor_m2064,
	ArgumentCache_get_unityObjectArgument_m2065,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2066,
	ArgumentCache_get_intArgument_m2067,
	ArgumentCache_get_floatArgument_m2068,
	ArgumentCache_get_stringArgument_m2069,
	ArgumentCache_get_boolArgument_m2070,
	ArgumentCache_TidyAssemblyTypeName_m2071,
	ArgumentCache_OnBeforeSerialize_m2072,
	ArgumentCache_OnAfterDeserialize_m2073,
	BaseInvokableCall__ctor_m2074,
	BaseInvokableCall__ctor_m2075,
	BaseInvokableCall_AllowInvoke_m2076,
	InvokableCall__ctor_m2077,
	InvokableCall__ctor_m2078,
	InvokableCall_Invoke_m2079,
	InvokableCall_Find_m2080,
	PersistentCall__ctor_m2081,
	PersistentCall_get_target_m2082,
	PersistentCall_get_methodName_m2083,
	PersistentCall_get_mode_m2084,
	PersistentCall_get_arguments_m2085,
	PersistentCall_IsValid_m2086,
	PersistentCall_GetRuntimeCall_m2087,
	PersistentCall_GetObjectCall_m2088,
	PersistentCallGroup__ctor_m2089,
	PersistentCallGroup_Initialize_m2090,
	InvokableCallList__ctor_m2091,
	InvokableCallList_AddPersistentInvokableCall_m2092,
	InvokableCallList_AddListener_m2093,
	InvokableCallList_RemoveListener_m2094,
	InvokableCallList_ClearPersistent_m2095,
	InvokableCallList_Invoke_m2096,
	UnityEventBase__ctor_m2097,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2098,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2099,
	UnityEventBase_FindMethod_m2100,
	UnityEventBase_FindMethod_m2101,
	UnityEventBase_DirtyPersistentCalls_m2102,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m2103,
	UnityEventBase_AddCall_m2104,
	UnityEventBase_RemoveListener_m2105,
	UnityEventBase_Invoke_m2106,
	UnityEventBase_ToString_m2107,
	UnityEventBase_GetValidMethodInfo_m2108,
	UnityEvent__ctor_m2109,
	UnityEvent_AddListener_m2110,
	UnityEvent_FindMethod_Impl_m2111,
	UnityEvent_GetDelegate_m2112,
	UnityEvent_GetDelegate_m2113,
	UnityEvent_Invoke_m2114,
	DefaultValueAttribute__ctor_m2115,
	DefaultValueAttribute_get_Value_m2116,
	DefaultValueAttribute_Equals_m2117,
	DefaultValueAttribute_GetHashCode_m2118,
	ExcludeFromDocsAttribute__ctor_m2119,
	FormerlySerializedAsAttribute__ctor_m2120,
	TypeInferenceRuleAttribute__ctor_m2121,
	TypeInferenceRuleAttribute__ctor_m2122,
	TypeInferenceRuleAttribute_ToString_m2123,
	GenericStack__ctor_m2124,
	NetFxCoreExtensions_CreateDelegate_m2125,
	NetFxCoreExtensions_GetMethodInfo_m2126,
	UnityAdsDelegate__ctor_m2127,
	UnityAdsDelegate_Invoke_m2128,
	UnityAdsDelegate_BeginInvoke_m2129,
	UnityAdsDelegate_EndInvoke_m2130,
	UnityAction__ctor_m2131,
	UnityAction_Invoke_m2132,
	UnityAction_BeginInvoke_m2133,
	UnityAction_EndInvoke_m2134,
	EventSystem__ctor_m2247,
	EventSystem__cctor_m2248,
	EventSystem_get_current_m2249,
	EventSystem_set_current_m2250,
	EventSystem_get_sendNavigationEvents_m2251,
	EventSystem_set_sendNavigationEvents_m2252,
	EventSystem_get_pixelDragThreshold_m2253,
	EventSystem_set_pixelDragThreshold_m2254,
	EventSystem_get_currentInputModule_m2255,
	EventSystem_get_firstSelectedGameObject_m2256,
	EventSystem_set_firstSelectedGameObject_m2257,
	EventSystem_get_currentSelectedGameObject_m518,
	EventSystem_get_lastSelectedGameObject_m2258,
	EventSystem_UpdateModules_m2259,
	EventSystem_get_alreadySelecting_m2260,
	EventSystem_SetSelectedGameObject_m506,
	EventSystem_get_baseEventDataCache_m2261,
	EventSystem_SetSelectedGameObject_m2262,
	EventSystem_RaycastComparer_m2263,
	EventSystem_RaycastAll_m512,
	EventSystem_IsPointerOverGameObject_m2264,
	EventSystem_IsPointerOverGameObject_m2265,
	EventSystem_OnEnable_m2266,
	EventSystem_OnDisable_m2267,
	EventSystem_TickModules_m2268,
	EventSystem_Update_m2269,
	EventSystem_ChangeEventModule_m2270,
	EventSystem_ToString_m2271,
	TriggerEvent__ctor_m2272,
	Entry__ctor_m2273,
	EventTrigger__ctor_m2274,
	EventTrigger_get_triggers_m2275,
	EventTrigger_set_triggers_m2276,
	EventTrigger_Execute_m2277,
	EventTrigger_OnPointerEnter_m2278,
	EventTrigger_OnPointerExit_m2279,
	EventTrigger_OnDrag_m2280,
	EventTrigger_OnDrop_m2281,
	EventTrigger_OnPointerDown_m2282,
	EventTrigger_OnPointerUp_m2283,
	EventTrigger_OnPointerClick_m2284,
	EventTrigger_OnSelect_m2285,
	EventTrigger_OnDeselect_m2286,
	EventTrigger_OnScroll_m2287,
	EventTrigger_OnMove_m2288,
	EventTrigger_OnUpdateSelected_m2289,
	EventTrigger_OnInitializePotentialDrag_m2290,
	EventTrigger_OnBeginDrag_m2291,
	EventTrigger_OnEndDrag_m2292,
	EventTrigger_OnSubmit_m2293,
	EventTrigger_OnCancel_m2294,
	ExecuteEvents__cctor_m2295,
	ExecuteEvents_Execute_m2296,
	ExecuteEvents_Execute_m2297,
	ExecuteEvents_Execute_m2298,
	ExecuteEvents_Execute_m2299,
	ExecuteEvents_Execute_m2300,
	ExecuteEvents_Execute_m2301,
	ExecuteEvents_Execute_m2302,
	ExecuteEvents_Execute_m2303,
	ExecuteEvents_Execute_m2304,
	ExecuteEvents_Execute_m2305,
	ExecuteEvents_Execute_m2306,
	ExecuteEvents_Execute_m2307,
	ExecuteEvents_Execute_m2308,
	ExecuteEvents_Execute_m2309,
	ExecuteEvents_Execute_m2310,
	ExecuteEvents_Execute_m2311,
	ExecuteEvents_Execute_m2312,
	ExecuteEvents_get_pointerEnterHandler_m2313,
	ExecuteEvents_get_pointerExitHandler_m2314,
	ExecuteEvents_get_pointerDownHandler_m538,
	ExecuteEvents_get_pointerUpHandler_m527,
	ExecuteEvents_get_pointerClickHandler_m529,
	ExecuteEvents_get_initializePotentialDrag_m2315,
	ExecuteEvents_get_beginDragHandler_m2316,
	ExecuteEvents_get_dragHandler_m2317,
	ExecuteEvents_get_endDragHandler_m2318,
	ExecuteEvents_get_dropHandler_m2319,
	ExecuteEvents_get_scrollHandler_m2320,
	ExecuteEvents_get_updateSelectedHandler_m519,
	ExecuteEvents_get_selectHandler_m2321,
	ExecuteEvents_get_deselectHandler_m2322,
	ExecuteEvents_get_moveHandler_m2323,
	ExecuteEvents_get_submitHandler_m2324,
	ExecuteEvents_get_cancelHandler_m2325,
	ExecuteEvents_GetEventChain_m2326,
	ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m2327,
	RaycasterManager__cctor_m2328,
	RaycasterManager_AddRaycaster_m2329,
	RaycasterManager_GetRaycasters_m2330,
	RaycasterManager_RemoveRaycasters_m2331,
	RaycastResult_get_gameObject_m516,
	RaycastResult_set_gameObject_m2332,
	RaycastResult_get_isValid_m2333,
	RaycastResult_ToString_m2334,
	UIBehaviour__ctor_m2335,
	UIBehaviour_Awake_m2336,
	UIBehaviour_OnEnable_m2337,
	UIBehaviour_Start_m2338,
	UIBehaviour_OnDisable_m2339,
	UIBehaviour_OnDestroy_m2340,
	UIBehaviour_IsActive_m2341,
	UIBehaviour_OnRectTransformDimensionsChange_m2342,
	UIBehaviour_OnBeforeTransformParentChanged_m2343,
	UIBehaviour_OnTransformParentChanged_m2344,
	UIBehaviour_OnDidApplyAnimationProperties_m2345,
	UIBehaviour_OnCanvasGroupChanged_m2346,
	UIBehaviour_OnCanvasHierarchyChanged_m2347,
	UIBehaviour_IsDestroyed_m2348,
	AxisEventData__ctor_m2349,
	AxisEventData_set_moveVector_m2350,
	AxisEventData_get_moveDir_m2351,
	AxisEventData_set_moveDir_m2352,
	BaseEventData__ctor_m2353,
	BaseEventData_Reset_m510,
	BaseEventData_Use_m2354,
	BaseEventData_get_used_m2355,
	BaseEventData_set_selectedObject_m2356,
	PointerEventData__ctor_m509,
	PointerEventData_get_pointerEnter_m508,
	PointerEventData_set_pointerEnter_m2357,
	PointerEventData_get_lastPress_m2358,
	PointerEventData_set_lastPress_m2359,
	PointerEventData_set_rawPointerPress_m532,
	PointerEventData_get_pointerDrag_m2360,
	PointerEventData_set_pointerDrag_m2361,
	PointerEventData_get_pointerCurrentRaycast_m515,
	PointerEventData_set_pointerCurrentRaycast_m514,
	PointerEventData_get_pointerPressRaycast_m2362,
	PointerEventData_set_pointerPressRaycast_m537,
	PointerEventData_get_eligibleForClick_m522,
	PointerEventData_set_eligibleForClick_m533,
	PointerEventData_get_pointerId_m2363,
	PointerEventData_set_pointerId_m2364,
	PointerEventData_get_position_m535,
	PointerEventData_set_position_m511,
	PointerEventData_get_delta_m2365,
	PointerEventData_set_delta_m2366,
	PointerEventData_get_pressPosition_m2367,
	PointerEventData_set_pressPosition_m536,
	PointerEventData_get_clickTime_m524,
	PointerEventData_set_clickTime_m541,
	PointerEventData_get_clickCount_m2368,
	PointerEventData_set_clickCount_m534,
	PointerEventData_get_scrollDelta_m2369,
	PointerEventData_set_scrollDelta_m2370,
	PointerEventData_get_useDragThreshold_m2371,
	PointerEventData_set_useDragThreshold_m2372,
	PointerEventData_get_dragging_m2373,
	PointerEventData_set_dragging_m2374,
	PointerEventData_get_button_m2375,
	PointerEventData_set_button_m2376,
	PointerEventData_IsPointerMoving_m2377,
	PointerEventData_get_enterEventCamera_m521,
	PointerEventData_get_pressEventCamera_m2378,
	PointerEventData_get_pointerPress_m526,
	PointerEventData_set_pointerPress_m531,
	PointerEventData_ToString_m2379,
	BaseInputModule__ctor_m501,
	BaseInputModule_get_eventSystem_m505,
	BaseInputModule_OnEnable_m2380,
	BaseInputModule_OnDisable_m2381,
	BaseInputModule_FindFirstRaycast_m513,
	BaseInputModule_DetermineMoveDirection_m2382,
	BaseInputModule_DetermineMoveDirection_m2383,
	BaseInputModule_FindCommonRoot_m2384,
	BaseInputModule_HandlePointerExitAndEnter_m504,
	BaseInputModule_GetAxisEventData_m2385,
	BaseInputModule_GetBaseEventData_m2386,
	BaseInputModule_IsPointerOverGameObject_m2387,
	BaseInputModule_ShouldActivateModule_m502,
	BaseInputModule_DeactivateModule_m503,
	BaseInputModule_ActivateModule_m2388,
	BaseInputModule_UpdateModule_m2389,
	BaseInputModule_IsModuleSupported_m2390,
	ButtonState__ctor_m2391,
	ButtonState_get_eventData_m2392,
	ButtonState_set_eventData_m2393,
	ButtonState_get_button_m2394,
	ButtonState_set_button_m2395,
	MouseState__ctor_m2396,
	MouseState_GetButtonState_m2397,
	MouseState_SetButtonState_m2398,
	MouseButtonEventData__ctor_m2399,
	MouseButtonEventData_PressedThisFrame_m2400,
	MouseButtonEventData_ReleasedThisFrame_m2401,
	PointerInputModule__ctor_m2402,
	PointerInputModule_GetPointerData_m2403,
	PointerInputModule_RemovePointerData_m2404,
	PointerInputModule_GetTouchPointerEventData_m2405,
	PointerInputModule_CopyFromTo_m2406,
	PointerInputModule_StateForMouseButton_m2407,
	PointerInputModule_GetMousePointerEventData_m2408,
	PointerInputModule_GetMousePointerEventData_m2409,
	PointerInputModule_GetLastPointerEventData_m2410,
	PointerInputModule_ShouldStartDrag_m2411,
	PointerInputModule_ProcessMove_m2412,
	PointerInputModule_ProcessDrag_m2413,
	PointerInputModule_IsPointerOverGameObject_m2414,
	PointerInputModule_ClearSelection_m2415,
	PointerInputModule_ToString_m2416,
	PointerInputModule_DeselectIfSelectionChanged_m2417,
	StandaloneInputModule__ctor_m2418,
	StandaloneInputModule_get_inputMode_m2419,
	StandaloneInputModule_get_allowActivationOnMobileDevice_m2420,
	StandaloneInputModule_set_allowActivationOnMobileDevice_m2421,
	StandaloneInputModule_get_forceModuleActive_m2422,
	StandaloneInputModule_set_forceModuleActive_m2423,
	StandaloneInputModule_get_inputActionsPerSecond_m2424,
	StandaloneInputModule_set_inputActionsPerSecond_m2425,
	StandaloneInputModule_get_repeatDelay_m2426,
	StandaloneInputModule_set_repeatDelay_m2427,
	StandaloneInputModule_get_horizontalAxis_m2428,
	StandaloneInputModule_set_horizontalAxis_m2429,
	StandaloneInputModule_get_verticalAxis_m2430,
	StandaloneInputModule_set_verticalAxis_m2431,
	StandaloneInputModule_get_submitButton_m2432,
	StandaloneInputModule_set_submitButton_m2433,
	StandaloneInputModule_get_cancelButton_m2434,
	StandaloneInputModule_set_cancelButton_m2435,
	StandaloneInputModule_UpdateModule_m2436,
	StandaloneInputModule_IsModuleSupported_m2437,
	StandaloneInputModule_ShouldActivateModule_m2438,
	StandaloneInputModule_ActivateModule_m2439,
	StandaloneInputModule_DeactivateModule_m2440,
	StandaloneInputModule_Process_m2441,
	StandaloneInputModule_SendSubmitEventToSelectedObject_m2442,
	StandaloneInputModule_GetRawMoveVector_m2443,
	StandaloneInputModule_SendMoveEventToSelectedObject_m2444,
	StandaloneInputModule_ProcessMouseEvent_m2445,
	StandaloneInputModule_ProcessMouseEvent_m2446,
	StandaloneInputModule_SendUpdateEventToSelectedObject_m2447,
	StandaloneInputModule_ProcessMousePress_m2448,
	TouchInputModule__ctor_m2449,
	TouchInputModule_get_allowActivationOnStandalone_m2450,
	TouchInputModule_set_allowActivationOnStandalone_m2451,
	TouchInputModule_get_forceModuleActive_m2452,
	TouchInputModule_set_forceModuleActive_m2453,
	TouchInputModule_UpdateModule_m2454,
	TouchInputModule_IsModuleSupported_m2455,
	TouchInputModule_ShouldActivateModule_m2456,
	TouchInputModule_UseFakeInput_m2457,
	TouchInputModule_Process_m2458,
	TouchInputModule_FakeTouches_m2459,
	TouchInputModule_ProcessTouchEvents_m2460,
	TouchInputModule_ProcessTouchPress_m2461,
	TouchInputModule_DeactivateModule_m2462,
	TouchInputModule_ToString_m2463,
	BaseRaycaster__ctor_m2464,
	BaseRaycaster_get_priority_m2465,
	BaseRaycaster_get_sortOrderPriority_m2466,
	BaseRaycaster_get_renderOrderPriority_m2467,
	BaseRaycaster_ToString_m2468,
	BaseRaycaster_OnEnable_m2469,
	BaseRaycaster_OnDisable_m2470,
	Physics2DRaycaster__ctor_m2471,
	Physics2DRaycaster_Raycast_m2472,
	PhysicsRaycaster__ctor_m2473,
	PhysicsRaycaster_get_eventCamera_m2474,
	PhysicsRaycaster_get_depth_m2475,
	PhysicsRaycaster_get_finalEventMask_m2476,
	PhysicsRaycaster_get_eventMask_m2477,
	PhysicsRaycaster_set_eventMask_m2478,
	PhysicsRaycaster_Raycast_m2479,
	PhysicsRaycaster_U3CRaycastU3Em__1_m2480,
	ColorTweenCallback__ctor_m2481,
	ColorTween_get_startColor_m2482,
	ColorTween_set_startColor_m2483,
	ColorTween_get_targetColor_m2484,
	ColorTween_set_targetColor_m2485,
	ColorTween_get_tweenMode_m2486,
	ColorTween_set_tweenMode_m2487,
	ColorTween_get_duration_m2488,
	ColorTween_set_duration_m2489,
	ColorTween_get_ignoreTimeScale_m2490,
	ColorTween_set_ignoreTimeScale_m2491,
	ColorTween_TweenValue_m2492,
	ColorTween_AddOnChangedCallback_m2493,
	ColorTween_GetIgnoreTimescale_m2494,
	ColorTween_GetDuration_m2495,
	ColorTween_ValidTarget_m2496,
	FloatTweenCallback__ctor_m2497,
	FloatTween_get_startValue_m2498,
	FloatTween_set_startValue_m2499,
	FloatTween_get_targetValue_m2500,
	FloatTween_set_targetValue_m2501,
	FloatTween_get_duration_m2502,
	FloatTween_set_duration_m2503,
	FloatTween_get_ignoreTimeScale_m2504,
	FloatTween_set_ignoreTimeScale_m2505,
	FloatTween_TweenValue_m2506,
	FloatTween_AddOnChangedCallback_m2507,
	FloatTween_GetIgnoreTimescale_m2508,
	FloatTween_GetDuration_m2509,
	FloatTween_ValidTarget_m2510,
	AnimationTriggers__ctor_m2511,
	AnimationTriggers_get_normalTrigger_m2512,
	AnimationTriggers_get_highlightedTrigger_m2513,
	AnimationTriggers_get_pressedTrigger_m2514,
	AnimationTriggers_get_disabledTrigger_m2515,
	ButtonClickedEvent__ctor_m2516,
	U3COnFinishSubmitU3Ec__Iterator1__ctor_m2517,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2518,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2519,
	U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m2520,
	U3COnFinishSubmitU3Ec__Iterator1_Dispose_m2521,
	U3COnFinishSubmitU3Ec__Iterator1_Reset_m2522,
	Button__ctor_m2523,
	Button_get_onClick_m2524,
	Button_set_onClick_m2525,
	Button_Press_m2526,
	Button_OnPointerClick_m2527,
	Button_OnSubmit_m2528,
	Button_OnFinishSubmit_m2529,
	CanvasUpdateRegistry__ctor_m2530,
	CanvasUpdateRegistry__cctor_m2531,
	CanvasUpdateRegistry_get_instance_m2532,
	CanvasUpdateRegistry_ObjectValidForUpdate_m2533,
	CanvasUpdateRegistry_PerformUpdate_m2534,
	CanvasUpdateRegistry_ParentCount_m2535,
	CanvasUpdateRegistry_SortLayoutList_m2536,
	CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m2537,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m2538,
	CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m2539,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m2540,
	CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m2541,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m2542,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m2543,
	CanvasUpdateRegistry_IsRebuildingLayout_m2544,
	CanvasUpdateRegistry_IsRebuildingGraphics_m2545,
	CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m2546,
	CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m2547,
	ColorBlock_get_normalColor_m2548,
	ColorBlock_get_highlightedColor_m2549,
	ColorBlock_get_pressedColor_m2550,
	ColorBlock_get_disabledColor_m2551,
	ColorBlock_get_colorMultiplier_m2552,
	ColorBlock_set_colorMultiplier_m2553,
	ColorBlock_get_fadeDuration_m2554,
	ColorBlock_set_fadeDuration_m2555,
	ColorBlock_get_defaultColorBlock_m2556,
	DropdownItem_get_text_m2557,
	DropdownItem_set_text_m2558,
	DropdownItem_get_image_m2559,
	DropdownItem_set_image_m2560,
	DropdownItem_get_rectTransform_m2561,
	DropdownItem_set_rectTransform_m2562,
	DropdownItem_get_toggle_m2563,
	DropdownItem_set_toggle_m2564,
	DropdownItem_OnPointerEnter_m2565,
	DropdownItem_OnCancel_m2566,
	OptionData__ctor_m2567,
	OptionData_get_text_m2568,
	OptionData_get_image_m2569,
	OptionDataList__ctor_m2570,
	OptionDataList_get_options_m2571,
	OptionDataList_set_options_m2572,
	DropdownEvent__ctor_m2573,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2__ctor_m2574,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2575,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2576,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_MoveNext_m2577,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_Dispose_m2578,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_Reset_m2579,
	U3CShowU3Ec__AnonStorey6__ctor_m2580,
	U3CShowU3Ec__AnonStorey6_U3CU3Em__4_m2581,
	Dropdown__ctor_m2582,
	Dropdown_get_template_m2583,
	Dropdown_set_template_m2584,
	Dropdown_get_captionText_m2585,
	Dropdown_set_captionText_m2586,
	Dropdown_get_captionImage_m2587,
	Dropdown_set_captionImage_m2588,
	Dropdown_get_itemText_m2589,
	Dropdown_set_itemText_m2590,
	Dropdown_get_itemImage_m2591,
	Dropdown_set_itemImage_m2592,
	Dropdown_get_options_m2593,
	Dropdown_set_options_m2594,
	Dropdown_get_onValueChanged_m2595,
	Dropdown_set_onValueChanged_m2596,
	Dropdown_get_value_m2597,
	Dropdown_set_value_m2598,
	Dropdown_Awake_m2599,
	Dropdown_Refresh_m2600,
	Dropdown_SetupTemplate_m2601,
	Dropdown_OnPointerClick_m2602,
	Dropdown_OnSubmit_m2603,
	Dropdown_OnCancel_m2604,
	Dropdown_Show_m2605,
	Dropdown_CreateBlocker_m2606,
	Dropdown_DestroyBlocker_m2607,
	Dropdown_CreateDropdownList_m2608,
	Dropdown_DestroyDropdownList_m2609,
	Dropdown_CreateItem_m2610,
	Dropdown_DestroyItem_m2611,
	Dropdown_AddItem_m2612,
	Dropdown_AlphaFadeList_m2613,
	Dropdown_AlphaFadeList_m2614,
	Dropdown_SetAlpha_m2615,
	Dropdown_Hide_m2616,
	Dropdown_DelayedDestroyDropdownList_m2617,
	Dropdown_OnSelectItem_m2618,
	FontData__ctor_m2619,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2620,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2621,
	FontData_get_defaultFontData_m2622,
	FontData_get_font_m2623,
	FontData_set_font_m2624,
	FontData_get_fontSize_m2625,
	FontData_set_fontSize_m2626,
	FontData_get_fontStyle_m2627,
	FontData_set_fontStyle_m2628,
	FontData_get_bestFit_m2629,
	FontData_set_bestFit_m2630,
	FontData_get_minSize_m2631,
	FontData_set_minSize_m2632,
	FontData_get_maxSize_m2633,
	FontData_set_maxSize_m2634,
	FontData_get_alignment_m2635,
	FontData_set_alignment_m2636,
	FontData_get_richText_m2637,
	FontData_set_richText_m2638,
	FontData_get_horizontalOverflow_m2639,
	FontData_set_horizontalOverflow_m2640,
	FontData_get_verticalOverflow_m2641,
	FontData_set_verticalOverflow_m2642,
	FontData_get_lineSpacing_m2643,
	FontData_set_lineSpacing_m2644,
	FontUpdateTracker__cctor_m2645,
	FontUpdateTracker_TrackText_m2646,
	FontUpdateTracker_RebuildForFont_m2647,
	FontUpdateTracker_UntrackText_m2648,
	Graphic__ctor_m2649,
	Graphic__cctor_m2650,
	Graphic_get_defaultGraphicMaterial_m2651,
	Graphic_get_color_m2652,
	Graphic_set_color_m2653,
	Graphic_get_raycastTarget_m2654,
	Graphic_set_raycastTarget_m2655,
	Graphic_SetAllDirty_m2656,
	Graphic_SetLayoutDirty_m2657,
	Graphic_SetVerticesDirty_m2658,
	Graphic_SetMaterialDirty_m2659,
	Graphic_OnRectTransformDimensionsChange_m2660,
	Graphic_OnBeforeTransformParentChanged_m2661,
	Graphic_OnTransformParentChanged_m2662,
	Graphic_get_depth_m2663,
	Graphic_get_rectTransform_m2664,
	Graphic_get_canvas_m2665,
	Graphic_CacheCanvas_m2666,
	Graphic_get_canvasRenderer_m2667,
	Graphic_get_defaultMaterial_m2668,
	Graphic_get_material_m2669,
	Graphic_set_material_m2670,
	Graphic_get_materialForRendering_m2671,
	Graphic_get_mainTexture_m2672,
	Graphic_OnEnable_m2673,
	Graphic_OnDisable_m2674,
	Graphic_OnCanvasHierarchyChanged_m2675,
	Graphic_Rebuild_m2676,
	Graphic_UpdateMaterial_m2677,
	Graphic_UpdateGeometry_m2678,
	Graphic_get_workerMesh_m2679,
	Graphic_OnFillVBO_m2680,
	Graphic_OnPopulateMesh_m2681,
	Graphic_OnDidApplyAnimationProperties_m2682,
	Graphic_SetNativeSize_m2683,
	Graphic_Raycast_m2684,
	Graphic_PixelAdjustPoint_m2685,
	Graphic_GetPixelAdjustedRect_m2686,
	Graphic_CrossFadeColor_m2687,
	Graphic_CrossFadeColor_m2688,
	Graphic_CreateColorFromAlpha_m2689,
	Graphic_CrossFadeAlpha_m2690,
	Graphic_RegisterDirtyLayoutCallback_m2691,
	Graphic_UnregisterDirtyLayoutCallback_m2692,
	Graphic_RegisterDirtyVerticesCallback_m2693,
	Graphic_UnregisterDirtyVerticesCallback_m2694,
	Graphic_RegisterDirtyMaterialCallback_m2695,
	Graphic_UnregisterDirtyMaterialCallback_m2696,
	Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m2697,
	Graphic_UnityEngine_UI_ICanvasElement_get_transform_m2698,
	GraphicRaycaster__ctor_m2699,
	GraphicRaycaster__cctor_m2700,
	GraphicRaycaster_get_sortOrderPriority_m2701,
	GraphicRaycaster_get_renderOrderPriority_m2702,
	GraphicRaycaster_get_ignoreReversedGraphics_m2703,
	GraphicRaycaster_set_ignoreReversedGraphics_m2704,
	GraphicRaycaster_get_blockingObjects_m2705,
	GraphicRaycaster_set_blockingObjects_m2706,
	GraphicRaycaster_get_canvas_m2707,
	GraphicRaycaster_Raycast_m2708,
	GraphicRaycaster_get_eventCamera_m2709,
	GraphicRaycaster_Raycast_m2710,
	GraphicRaycaster_U3CRaycastU3Em__5_m2711,
	GraphicRegistry__ctor_m2712,
	GraphicRegistry__cctor_m2713,
	GraphicRegistry_get_instance_m2714,
	GraphicRegistry_RegisterGraphicForCanvas_m2715,
	GraphicRegistry_UnregisterGraphicForCanvas_m2716,
	GraphicRegistry_GetGraphicsForCanvas_m2717,
	Image__ctor_m2718,
	Image__cctor_m2719,
	Image_get_sprite_m2720,
	Image_set_sprite_m2721,
	Image_get_overrideSprite_m2722,
	Image_set_overrideSprite_m2723,
	Image_get_type_m2724,
	Image_set_type_m2725,
	Image_get_preserveAspect_m2726,
	Image_set_preserveAspect_m2727,
	Image_get_fillCenter_m2728,
	Image_set_fillCenter_m2729,
	Image_get_fillMethod_m2730,
	Image_set_fillMethod_m2731,
	Image_get_fillAmount_m2732,
	Image_set_fillAmount_m2733,
	Image_get_fillClockwise_m2734,
	Image_set_fillClockwise_m2735,
	Image_get_fillOrigin_m2736,
	Image_set_fillOrigin_m2737,
	Image_get_eventAlphaThreshold_m2738,
	Image_set_eventAlphaThreshold_m2739,
	Image_get_mainTexture_m2740,
	Image_get_hasBorder_m2741,
	Image_get_pixelsPerUnit_m2742,
	Image_OnBeforeSerialize_m2743,
	Image_OnAfterDeserialize_m2744,
	Image_GetDrawingDimensions_m2745,
	Image_SetNativeSize_m2746,
	Image_OnPopulateMesh_m2747,
	Image_GenerateSimpleSprite_m2748,
	Image_GenerateSlicedSprite_m2749,
	Image_GenerateTiledSprite_m2750,
	Image_AddQuad_m2751,
	Image_AddQuad_m2752,
	Image_GetAdjustedBorders_m2753,
	Image_GenerateFilledSprite_m2754,
	Image_RadialCut_m2755,
	Image_RadialCut_m2756,
	Image_CalculateLayoutInputHorizontal_m2757,
	Image_CalculateLayoutInputVertical_m2758,
	Image_get_minWidth_m2759,
	Image_get_preferredWidth_m2760,
	Image_get_flexibleWidth_m2761,
	Image_get_minHeight_m2762,
	Image_get_preferredHeight_m2763,
	Image_get_flexibleHeight_m2764,
	Image_get_layoutPriority_m2765,
	Image_IsRaycastLocationValid_m2766,
	Image_MapCoordinate_m2767,
	SubmitEvent__ctor_m2768,
	OnChangeEvent__ctor_m2769,
	OnValidateInput__ctor_m2770,
	OnValidateInput_Invoke_m2771,
	OnValidateInput_BeginInvoke_m2772,
	OnValidateInput_EndInvoke_m2773,
	U3CCaretBlinkU3Ec__Iterator3__ctor_m2774,
	U3CCaretBlinkU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2775,
	U3CCaretBlinkU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2776,
	U3CCaretBlinkU3Ec__Iterator3_MoveNext_m2777,
	U3CCaretBlinkU3Ec__Iterator3_Dispose_m2778,
	U3CCaretBlinkU3Ec__Iterator3_Reset_m2779,
	U3CMouseDragOutsideRectU3Ec__Iterator4__ctor_m2780,
	U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2781,
	U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2782,
	U3CMouseDragOutsideRectU3Ec__Iterator4_MoveNext_m2783,
	U3CMouseDragOutsideRectU3Ec__Iterator4_Dispose_m2784,
	U3CMouseDragOutsideRectU3Ec__Iterator4_Reset_m2785,
	InputField__ctor_m2786,
	InputField__cctor_m2787,
	InputField_get_mesh_m2788,
	InputField_get_cachedInputTextGenerator_m2789,
	InputField_set_shouldHideMobileInput_m2790,
	InputField_get_shouldHideMobileInput_m2791,
	InputField_get_text_m2792,
	InputField_set_text_m2793,
	InputField_get_isFocused_m2794,
	InputField_get_caretBlinkRate_m2795,
	InputField_set_caretBlinkRate_m2796,
	InputField_get_textComponent_m2797,
	InputField_set_textComponent_m2798,
	InputField_get_placeholder_m2799,
	InputField_set_placeholder_m2800,
	InputField_get_selectionColor_m2801,
	InputField_set_selectionColor_m2802,
	InputField_get_onEndEdit_m2803,
	InputField_set_onEndEdit_m2804,
	InputField_get_onValueChange_m2805,
	InputField_set_onValueChange_m2806,
	InputField_get_onValidateInput_m2807,
	InputField_set_onValidateInput_m2808,
	InputField_get_characterLimit_m2809,
	InputField_set_characterLimit_m2810,
	InputField_get_contentType_m2811,
	InputField_set_contentType_m2812,
	InputField_get_lineType_m2813,
	InputField_set_lineType_m2814,
	InputField_get_inputType_m2815,
	InputField_set_inputType_m2816,
	InputField_get_keyboardType_m2817,
	InputField_set_keyboardType_m2818,
	InputField_get_characterValidation_m2819,
	InputField_set_characterValidation_m2820,
	InputField_get_multiLine_m2821,
	InputField_get_asteriskChar_m2822,
	InputField_set_asteriskChar_m2823,
	InputField_get_wasCanceled_m2824,
	InputField_ClampPos_m2825,
	InputField_get_caretPositionInternal_m2826,
	InputField_set_caretPositionInternal_m2827,
	InputField_get_caretSelectPositionInternal_m2828,
	InputField_set_caretSelectPositionInternal_m2829,
	InputField_get_hasSelection_m2830,
	InputField_get_caretPosition_m2831,
	InputField_set_caretPosition_m2832,
	InputField_get_selectionAnchorPosition_m2833,
	InputField_set_selectionAnchorPosition_m2834,
	InputField_get_selectionFocusPosition_m2835,
	InputField_set_selectionFocusPosition_m2836,
	InputField_OnEnable_m2837,
	InputField_OnDisable_m2838,
	InputField_CaretBlink_m2839,
	InputField_SetCaretVisible_m2840,
	InputField_SetCaretActive_m2841,
	InputField_OnFocus_m2842,
	InputField_SelectAll_m2843,
	InputField_MoveTextEnd_m2844,
	InputField_MoveTextStart_m2845,
	InputField_get_clipboard_m2846,
	InputField_set_clipboard_m2847,
	InputField_InPlaceEditing_m2848,
	InputField_LateUpdate_m2849,
	InputField_ScreenToLocal_m2850,
	InputField_GetUnclampedCharacterLineFromPosition_m2851,
	InputField_GetCharacterIndexFromPosition_m2852,
	InputField_MayDrag_m2853,
	InputField_OnBeginDrag_m2854,
	InputField_OnDrag_m2855,
	InputField_MouseDragOutsideRect_m2856,
	InputField_OnEndDrag_m2857,
	InputField_OnPointerDown_m2858,
	InputField_KeyPressed_m2859,
	InputField_IsValidChar_m2860,
	InputField_ProcessEvent_m2861,
	InputField_OnUpdateSelected_m2862,
	InputField_GetSelectedString_m2863,
	InputField_FindtNextWordBegin_m2864,
	InputField_MoveRight_m2865,
	InputField_FindtPrevWordBegin_m2866,
	InputField_MoveLeft_m2867,
	InputField_DetermineCharacterLine_m2868,
	InputField_LineUpCharacterPosition_m2869,
	InputField_LineDownCharacterPosition_m2870,
	InputField_MoveDown_m2871,
	InputField_MoveDown_m2872,
	InputField_MoveUp_m2873,
	InputField_MoveUp_m2874,
	InputField_Delete_m2875,
	InputField_ForwardSpace_m2876,
	InputField_Backspace_m2877,
	InputField_Insert_m2878,
	InputField_SendOnValueChangedAndUpdateLabel_m2879,
	InputField_SendOnValueChanged_m2880,
	InputField_SendOnSubmit_m2881,
	InputField_Append_m2882,
	InputField_Append_m2883,
	InputField_UpdateLabel_m2884,
	InputField_IsSelectionVisible_m2885,
	InputField_GetLineStartPosition_m2886,
	InputField_GetLineEndPosition_m2887,
	InputField_SetDrawRangeToContainCaretPosition_m2888,
	InputField_MarkGeometryAsDirty_m2889,
	InputField_Rebuild_m2890,
	InputField_UpdateGeometry_m2891,
	InputField_AssignPositioningIfNeeded_m2892,
	InputField_OnFillVBO_m2893,
	InputField_GenerateCursor_m2894,
	InputField_CreateCursorVerts_m2895,
	InputField_SumLineHeights_m2896,
	InputField_GenerateHightlight_m2897,
	InputField_Validate_m2898,
	InputField_ActivateInputField_m2899,
	InputField_ActivateInputFieldInternal_m2900,
	InputField_OnSelect_m2901,
	InputField_OnPointerClick_m2902,
	InputField_DeactivateInputField_m2903,
	InputField_OnDeselect_m2904,
	InputField_OnSubmit_m2905,
	InputField_EnforceContentType_m2906,
	InputField_SetToCustomIfContentTypeIsNot_m2907,
	InputField_SetToCustom_m2908,
	InputField_DoStateTransition_m2909,
	InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m2910,
	InputField_UnityEngine_UI_ICanvasElement_get_transform_m2911,
	Mask__ctor_m2912,
	Mask_get_rectTransform_m2913,
	Mask_get_showMaskGraphic_m2914,
	Mask_set_showMaskGraphic_m2915,
	Mask_get_graphic_m2916,
	Mask_MaskEnabled_m2917,
	Mask_OnSiblingGraphicEnabledDisabled_m2918,
	Mask_OnEnable_m2919,
	Mask_OnDisable_m2920,
	Mask_IsRaycastLocationValid_m2921,
	Mask_GetModifiedMaterial_m2922,
	CullStateChangedEvent__ctor_m2923,
	MaskableGraphic__ctor_m2924,
	MaskableGraphic_get_onCullStateChanged_m2925,
	MaskableGraphic_set_onCullStateChanged_m2926,
	MaskableGraphic_get_maskable_m2927,
	MaskableGraphic_set_maskable_m2928,
	MaskableGraphic_GetModifiedMaterial_m2929,
	MaskableGraphic_Cull_m2930,
	MaskableGraphic_SetClipRect_m2931,
	MaskableGraphic_OnEnable_m2932,
	MaskableGraphic_OnDisable_m2933,
	MaskableGraphic_OnTransformParentChanged_m2934,
	MaskableGraphic_ParentMaskStateChanged_m2935,
	MaskableGraphic_OnCanvasHierarchyChanged_m2936,
	MaskableGraphic_get_canvasRect_m2937,
	MaskableGraphic_UpdateClipParent_m2938,
	MaskableGraphic_RecalculateClipping_m2939,
	MaskableGraphic_RecalculateMasking_m2940,
	MaskableGraphic_UnityEngine_UI_IClippable_get_rectTransform_m2941,
	MaskUtilities_Notify2DMaskStateChanged_m2942,
	MaskUtilities_NotifyStencilStateChanged_m2943,
	MaskUtilities_FindRootSortOverrideCanvas_m2944,
	MaskUtilities_GetStencilDepth_m2945,
	MaskUtilities_GetRectMaskForClippable_m2946,
	MaskUtilities_GetRectMasksForClip_m2947,
	Misc_DestroyImmediate_m2948,
	Navigation_get_mode_m2949,
	Navigation_set_mode_m2950,
	Navigation_get_selectOnUp_m2951,
	Navigation_set_selectOnUp_m2952,
	Navigation_get_selectOnDown_m2953,
	Navigation_set_selectOnDown_m2954,
	Navigation_get_selectOnLeft_m2955,
	Navigation_set_selectOnLeft_m2956,
	Navigation_get_selectOnRight_m2957,
	Navigation_set_selectOnRight_m2958,
	Navigation_get_defaultNavigation_m2959,
	RawImage__ctor_m2960,
	RawImage_get_mainTexture_m2961,
	RawImage_get_texture_m2962,
	RawImage_set_texture_m2963,
	RawImage_get_uvRect_m2964,
	RawImage_set_uvRect_m2965,
	RawImage_SetNativeSize_m2966,
	RawImage_OnPopulateMesh_m2967,
	RectMask2D__ctor_m2968,
	RectMask2D_get_canvasRect_m2969,
	RectMask2D_get_rectTransform_m2970,
	RectMask2D_OnEnable_m2971,
	RectMask2D_OnDisable_m2972,
	RectMask2D_IsRaycastLocationValid_m2973,
	RectMask2D_PerformClipping_m2974,
	RectMask2D_AddClippable_m2975,
	RectMask2D_RemoveClippable_m2976,
	RectMask2D_OnTransformParentChanged_m2977,
	RectMask2D_OnCanvasHierarchyChanged_m2978,
	ScrollEvent__ctor_m2979,
	U3CClickRepeatU3Ec__Iterator5__ctor_m2980,
	U3CClickRepeatU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2981,
	U3CClickRepeatU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2982,
	U3CClickRepeatU3Ec__Iterator5_MoveNext_m2983,
	U3CClickRepeatU3Ec__Iterator5_Dispose_m2984,
	U3CClickRepeatU3Ec__Iterator5_Reset_m2985,
	Scrollbar__ctor_m2986,
	Scrollbar_get_handleRect_m2987,
	Scrollbar_set_handleRect_m2988,
	Scrollbar_get_direction_m2989,
	Scrollbar_set_direction_m2990,
	Scrollbar_get_value_m2991,
	Scrollbar_set_value_m2992,
	Scrollbar_get_size_m2993,
	Scrollbar_set_size_m2994,
	Scrollbar_get_numberOfSteps_m2995,
	Scrollbar_set_numberOfSteps_m2996,
	Scrollbar_get_onValueChanged_m2997,
	Scrollbar_set_onValueChanged_m2998,
	Scrollbar_get_stepSize_m2999,
	Scrollbar_Rebuild_m3000,
	Scrollbar_OnEnable_m3001,
	Scrollbar_OnDisable_m3002,
	Scrollbar_UpdateCachedReferences_m3003,
	Scrollbar_Set_m3004,
	Scrollbar_Set_m3005,
	Scrollbar_OnRectTransformDimensionsChange_m3006,
	Scrollbar_get_axis_m3007,
	Scrollbar_get_reverseValue_m3008,
	Scrollbar_UpdateVisuals_m3009,
	Scrollbar_UpdateDrag_m3010,
	Scrollbar_MayDrag_m3011,
	Scrollbar_OnBeginDrag_m3012,
	Scrollbar_OnDrag_m3013,
	Scrollbar_OnPointerDown_m3014,
	Scrollbar_ClickRepeat_m3015,
	Scrollbar_OnPointerUp_m3016,
	Scrollbar_OnMove_m3017,
	Scrollbar_FindSelectableOnLeft_m3018,
	Scrollbar_FindSelectableOnRight_m3019,
	Scrollbar_FindSelectableOnUp_m3020,
	Scrollbar_FindSelectableOnDown_m3021,
	Scrollbar_OnInitializePotentialDrag_m3022,
	Scrollbar_SetDirection_m3023,
	Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m3024,
	Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m3025,
	ScrollRectEvent__ctor_m3026,
	ScrollRect__ctor_m3027,
	ScrollRect_get_content_m3028,
	ScrollRect_set_content_m3029,
	ScrollRect_get_horizontal_m3030,
	ScrollRect_set_horizontal_m3031,
	ScrollRect_get_vertical_m3032,
	ScrollRect_set_vertical_m3033,
	ScrollRect_get_movementType_m3034,
	ScrollRect_set_movementType_m3035,
	ScrollRect_get_elasticity_m3036,
	ScrollRect_set_elasticity_m3037,
	ScrollRect_get_inertia_m3038,
	ScrollRect_set_inertia_m3039,
	ScrollRect_get_decelerationRate_m3040,
	ScrollRect_set_decelerationRate_m3041,
	ScrollRect_get_scrollSensitivity_m3042,
	ScrollRect_set_scrollSensitivity_m3043,
	ScrollRect_get_viewport_m3044,
	ScrollRect_set_viewport_m3045,
	ScrollRect_get_horizontalScrollbar_m3046,
	ScrollRect_set_horizontalScrollbar_m3047,
	ScrollRect_get_verticalScrollbar_m3048,
	ScrollRect_set_verticalScrollbar_m3049,
	ScrollRect_get_horizontalScrollbarVisibility_m3050,
	ScrollRect_set_horizontalScrollbarVisibility_m3051,
	ScrollRect_get_verticalScrollbarVisibility_m3052,
	ScrollRect_set_verticalScrollbarVisibility_m3053,
	ScrollRect_get_horizontalScrollbarSpacing_m3054,
	ScrollRect_set_horizontalScrollbarSpacing_m3055,
	ScrollRect_get_verticalScrollbarSpacing_m3056,
	ScrollRect_set_verticalScrollbarSpacing_m3057,
	ScrollRect_get_onValueChanged_m3058,
	ScrollRect_set_onValueChanged_m3059,
	ScrollRect_get_viewRect_m3060,
	ScrollRect_get_velocity_m3061,
	ScrollRect_set_velocity_m3062,
	ScrollRect_get_rectTransform_m3063,
	ScrollRect_Rebuild_m3064,
	ScrollRect_UpdateCachedData_m3065,
	ScrollRect_OnEnable_m3066,
	ScrollRect_OnDisable_m3067,
	ScrollRect_IsActive_m3068,
	ScrollRect_EnsureLayoutHasRebuilt_m3069,
	ScrollRect_StopMovement_m3070,
	ScrollRect_OnScroll_m3071,
	ScrollRect_OnInitializePotentialDrag_m3072,
	ScrollRect_OnBeginDrag_m3073,
	ScrollRect_OnEndDrag_m3074,
	ScrollRect_OnDrag_m3075,
	ScrollRect_SetContentAnchoredPosition_m3076,
	ScrollRect_LateUpdate_m3077,
	ScrollRect_UpdatePrevData_m3078,
	ScrollRect_UpdateScrollbars_m3079,
	ScrollRect_get_normalizedPosition_m3080,
	ScrollRect_set_normalizedPosition_m3081,
	ScrollRect_get_horizontalNormalizedPosition_m3082,
	ScrollRect_set_horizontalNormalizedPosition_m3083,
	ScrollRect_get_verticalNormalizedPosition_m3084,
	ScrollRect_set_verticalNormalizedPosition_m3085,
	ScrollRect_SetHorizontalNormalizedPosition_m3086,
	ScrollRect_SetVerticalNormalizedPosition_m3087,
	ScrollRect_SetNormalizedPosition_m3088,
	ScrollRect_RubberDelta_m3089,
	ScrollRect_OnRectTransformDimensionsChange_m3090,
	ScrollRect_get_hScrollingNeeded_m3091,
	ScrollRect_get_vScrollingNeeded_m3092,
	ScrollRect_CalculateLayoutInputHorizontal_m3093,
	ScrollRect_CalculateLayoutInputVertical_m3094,
	ScrollRect_get_minWidth_m3095,
	ScrollRect_get_preferredWidth_m3096,
	ScrollRect_get_flexibleWidth_m3097,
	ScrollRect_get_minHeight_m3098,
	ScrollRect_get_preferredHeight_m3099,
	ScrollRect_get_flexibleHeight_m3100,
	ScrollRect_get_layoutPriority_m3101,
	ScrollRect_SetLayoutHorizontal_m3102,
	ScrollRect_SetLayoutVertical_m3103,
	ScrollRect_UpdateScrollbarVisibility_m3104,
	ScrollRect_UpdateScrollbarLayout_m3105,
	ScrollRect_UpdateBounds_m3106,
	ScrollRect_GetBounds_m3107,
	ScrollRect_CalculateOffset_m3108,
	ScrollRect_SetDirty_m3109,
	ScrollRect_SetDirtyCaching_m3110,
	ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m3111,
	ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m3112,
	Selectable__ctor_m3113,
	Selectable__cctor_m3114,
	Selectable_get_allSelectables_m3115,
	Selectable_get_navigation_m3116,
	Selectable_set_navigation_m3117,
	Selectable_get_transition_m3118,
	Selectable_set_transition_m3119,
	Selectable_get_colors_m3120,
	Selectable_set_colors_m3121,
	Selectable_get_spriteState_m3122,
	Selectable_set_spriteState_m3123,
	Selectable_get_animationTriggers_m3124,
	Selectable_set_animationTriggers_m3125,
	Selectable_get_targetGraphic_m3126,
	Selectable_set_targetGraphic_m3127,
	Selectable_get_interactable_m3128,
	Selectable_set_interactable_m3129,
	Selectable_get_isPointerInside_m3130,
	Selectable_set_isPointerInside_m3131,
	Selectable_get_isPointerDown_m3132,
	Selectable_set_isPointerDown_m3133,
	Selectable_get_hasSelection_m3134,
	Selectable_set_hasSelection_m3135,
	Selectable_get_image_m3136,
	Selectable_set_image_m3137,
	Selectable_get_animator_m3138,
	Selectable_Awake_m3139,
	Selectable_OnCanvasGroupChanged_m3140,
	Selectable_IsInteractable_m3141,
	Selectable_OnDidApplyAnimationProperties_m3142,
	Selectable_OnEnable_m3143,
	Selectable_OnSetProperty_m3144,
	Selectable_OnDisable_m3145,
	Selectable_get_currentSelectionState_m3146,
	Selectable_InstantClearState_m3147,
	Selectable_DoStateTransition_m3148,
	Selectable_FindSelectable_m3149,
	Selectable_GetPointOnRectEdge_m3150,
	Selectable_Navigate_m3151,
	Selectable_FindSelectableOnLeft_m3152,
	Selectable_FindSelectableOnRight_m3153,
	Selectable_FindSelectableOnUp_m3154,
	Selectable_FindSelectableOnDown_m3155,
	Selectable_OnMove_m3156,
	Selectable_StartColorTween_m3157,
	Selectable_DoSpriteSwap_m3158,
	Selectable_TriggerAnimation_m3159,
	Selectable_IsHighlighted_m3160,
	Selectable_IsPressed_m3161,
	Selectable_IsPressed_m3162,
	Selectable_UpdateSelectionState_m3163,
	Selectable_EvaluateAndTransitionToSelectionState_m3164,
	Selectable_InternalEvaluateAndTransitionToSelectionState_m3165,
	Selectable_OnPointerDown_m3166,
	Selectable_OnPointerUp_m3167,
	Selectable_OnPointerEnter_m3168,
	Selectable_OnPointerExit_m3169,
	Selectable_OnSelect_m3170,
	Selectable_OnDeselect_m3171,
	Selectable_Select_m3172,
	SetPropertyUtility_SetColor_m3173,
	SliderEvent__ctor_m3174,
	Slider__ctor_m3175,
	Slider_get_fillRect_m3176,
	Slider_set_fillRect_m3177,
	Slider_get_handleRect_m3178,
	Slider_set_handleRect_m3179,
	Slider_get_direction_m3180,
	Slider_set_direction_m3181,
	Slider_get_minValue_m3182,
	Slider_set_minValue_m3183,
	Slider_get_maxValue_m3184,
	Slider_set_maxValue_m3185,
	Slider_get_wholeNumbers_m3186,
	Slider_set_wholeNumbers_m3187,
	Slider_get_value_m3188,
	Slider_set_value_m3189,
	Slider_get_normalizedValue_m3190,
	Slider_set_normalizedValue_m3191,
	Slider_get_onValueChanged_m3192,
	Slider_set_onValueChanged_m3193,
	Slider_get_stepSize_m3194,
	Slider_Rebuild_m3195,
	Slider_OnEnable_m3196,
	Slider_OnDisable_m3197,
	Slider_OnDidApplyAnimationProperties_m3198,
	Slider_UpdateCachedReferences_m3199,
	Slider_ClampValue_m3200,
	Slider_Set_m3201,
	Slider_Set_m3202,
	Slider_OnRectTransformDimensionsChange_m3203,
	Slider_get_axis_m3204,
	Slider_get_reverseValue_m3205,
	Slider_UpdateVisuals_m3206,
	Slider_UpdateDrag_m3207,
	Slider_MayDrag_m3208,
	Slider_OnPointerDown_m3209,
	Slider_OnDrag_m3210,
	Slider_OnMove_m3211,
	Slider_FindSelectableOnLeft_m3212,
	Slider_FindSelectableOnRight_m3213,
	Slider_FindSelectableOnUp_m3214,
	Slider_FindSelectableOnDown_m3215,
	Slider_OnInitializePotentialDrag_m3216,
	Slider_SetDirection_m3217,
	Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m3218,
	Slider_UnityEngine_UI_ICanvasElement_get_transform_m3219,
	SpriteState_get_highlightedSprite_m3220,
	SpriteState_get_pressedSprite_m3221,
	SpriteState_get_disabledSprite_m3222,
	MatEntry__ctor_m3223,
	StencilMaterial__cctor_m3224,
	StencilMaterial_Add_m3225,
	StencilMaterial_Add_m3226,
	StencilMaterial_Remove_m3227,
	Text__ctor_m3228,
	Text__cctor_m3229,
	Text_get_cachedTextGenerator_m3230,
	Text_get_cachedTextGeneratorForLayout_m3231,
	Text_get_mainTexture_m3232,
	Text_FontTextureChanged_m3233,
	Text_get_font_m3234,
	Text_set_font_m3235,
	Text_get_text_m3236,
	Text_set_text_m3237,
	Text_get_supportRichText_m3238,
	Text_set_supportRichText_m3239,
	Text_get_resizeTextForBestFit_m3240,
	Text_set_resizeTextForBestFit_m3241,
	Text_get_resizeTextMinSize_m3242,
	Text_set_resizeTextMinSize_m3243,
	Text_get_resizeTextMaxSize_m3244,
	Text_set_resizeTextMaxSize_m3245,
	Text_get_alignment_m3246,
	Text_set_alignment_m3247,
	Text_get_fontSize_m3248,
	Text_set_fontSize_m3249,
	Text_get_horizontalOverflow_m3250,
	Text_set_horizontalOverflow_m3251,
	Text_get_verticalOverflow_m3252,
	Text_set_verticalOverflow_m3253,
	Text_get_lineSpacing_m3254,
	Text_set_lineSpacing_m3255,
	Text_get_fontStyle_m3256,
	Text_set_fontStyle_m3257,
	Text_get_pixelsPerUnit_m3258,
	Text_OnEnable_m3259,
	Text_OnDisable_m3260,
	Text_UpdateGeometry_m3261,
	Text_GetGenerationSettings_m3262,
	Text_GetTextAnchorPivot_m3263,
	Text_OnPopulateMesh_m3264,
	Text_CalculateLayoutInputHorizontal_m3265,
	Text_CalculateLayoutInputVertical_m3266,
	Text_get_minWidth_m3267,
	Text_get_preferredWidth_m3268,
	Text_get_flexibleWidth_m3269,
	Text_get_minHeight_m3270,
	Text_get_preferredHeight_m3271,
	Text_get_flexibleHeight_m3272,
	Text_get_layoutPriority_m3273,
	ToggleEvent__ctor_m3274,
	Toggle__ctor_m3275,
	Toggle_get_group_m3276,
	Toggle_set_group_m3277,
	Toggle_Rebuild_m3278,
	Toggle_OnEnable_m3279,
	Toggle_OnDisable_m3280,
	Toggle_OnDidApplyAnimationProperties_m3281,
	Toggle_SetToggleGroup_m3282,
	Toggle_get_isOn_m3283,
	Toggle_set_isOn_m3284,
	Toggle_Set_m3285,
	Toggle_Set_m3286,
	Toggle_PlayEffect_m3287,
	Toggle_Start_m3288,
	Toggle_InternalToggle_m3289,
	Toggle_OnPointerClick_m3290,
	Toggle_OnSubmit_m3291,
	Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m3292,
	Toggle_UnityEngine_UI_ICanvasElement_get_transform_m3293,
	ToggleGroup__ctor_m3294,
	ToggleGroup_get_allowSwitchOff_m3295,
	ToggleGroup_set_allowSwitchOff_m3296,
	ToggleGroup_ValidateToggleIsInGroup_m3297,
	ToggleGroup_NotifyToggleOn_m3298,
	ToggleGroup_UnregisterToggle_m3299,
	ToggleGroup_RegisterToggle_m3300,
	ToggleGroup_AnyTogglesOn_m3301,
	ToggleGroup_ActiveToggles_m3302,
	ToggleGroup_SetAllTogglesOff_m3303,
	ToggleGroup_U3CAnyTogglesOnU3Em__6_m3304,
	ToggleGroup_U3CActiveTogglesU3Em__7_m3305,
	ClipperRegistry__ctor_m3306,
	ClipperRegistry_get_instance_m3307,
	ClipperRegistry_Cull_m3308,
	ClipperRegistry_Register_m3309,
	ClipperRegistry_Unregister_m3310,
	Clipping_FindCullAndClipWorldRect_m3311,
	Clipping_RectIntersect_m3312,
	RectangularVertexClipper__ctor_m3313,
	RectangularVertexClipper_GetCanvasRect_m3314,
	AspectRatioFitter__ctor_m3315,
	AspectRatioFitter_get_aspectMode_m3316,
	AspectRatioFitter_set_aspectMode_m3317,
	AspectRatioFitter_get_aspectRatio_m3318,
	AspectRatioFitter_set_aspectRatio_m3319,
	AspectRatioFitter_get_rectTransform_m3320,
	AspectRatioFitter_OnEnable_m3321,
	AspectRatioFitter_OnDisable_m3322,
	AspectRatioFitter_OnRectTransformDimensionsChange_m3323,
	AspectRatioFitter_UpdateRect_m3324,
	AspectRatioFitter_GetSizeDeltaToProduceSize_m3325,
	AspectRatioFitter_GetParentSize_m3326,
	AspectRatioFitter_SetLayoutHorizontal_m3327,
	AspectRatioFitter_SetLayoutVertical_m3328,
	AspectRatioFitter_SetDirty_m3329,
	CanvasScaler__ctor_m3330,
	CanvasScaler_get_uiScaleMode_m3331,
	CanvasScaler_set_uiScaleMode_m3332,
	CanvasScaler_get_referencePixelsPerUnit_m3333,
	CanvasScaler_set_referencePixelsPerUnit_m3334,
	CanvasScaler_get_scaleFactor_m3335,
	CanvasScaler_set_scaleFactor_m3336,
	CanvasScaler_get_referenceResolution_m3337,
	CanvasScaler_set_referenceResolution_m3338,
	CanvasScaler_get_screenMatchMode_m3339,
	CanvasScaler_set_screenMatchMode_m3340,
	CanvasScaler_get_matchWidthOrHeight_m3341,
	CanvasScaler_set_matchWidthOrHeight_m3342,
	CanvasScaler_get_physicalUnit_m3343,
	CanvasScaler_set_physicalUnit_m3344,
	CanvasScaler_get_fallbackScreenDPI_m3345,
	CanvasScaler_set_fallbackScreenDPI_m3346,
	CanvasScaler_get_defaultSpriteDPI_m3347,
	CanvasScaler_set_defaultSpriteDPI_m3348,
	CanvasScaler_get_dynamicPixelsPerUnit_m3349,
	CanvasScaler_set_dynamicPixelsPerUnit_m3350,
	CanvasScaler_OnEnable_m3351,
	CanvasScaler_OnDisable_m3352,
	CanvasScaler_Update_m3353,
	CanvasScaler_Handle_m3354,
	CanvasScaler_HandleWorldCanvas_m3355,
	CanvasScaler_HandleConstantPixelSize_m3356,
	CanvasScaler_HandleScaleWithScreenSize_m3357,
	CanvasScaler_HandleConstantPhysicalSize_m3358,
	CanvasScaler_SetScaleFactor_m3359,
	CanvasScaler_SetReferencePixelsPerUnit_m3360,
	ContentSizeFitter__ctor_m3361,
	ContentSizeFitter_get_horizontalFit_m3362,
	ContentSizeFitter_set_horizontalFit_m3363,
	ContentSizeFitter_get_verticalFit_m3364,
	ContentSizeFitter_set_verticalFit_m3365,
	ContentSizeFitter_get_rectTransform_m3366,
	ContentSizeFitter_OnEnable_m3367,
	ContentSizeFitter_OnDisable_m3368,
	ContentSizeFitter_OnRectTransformDimensionsChange_m3369,
	ContentSizeFitter_HandleSelfFittingAlongAxis_m3370,
	ContentSizeFitter_SetLayoutHorizontal_m3371,
	ContentSizeFitter_SetLayoutVertical_m3372,
	ContentSizeFitter_SetDirty_m3373,
	GridLayoutGroup__ctor_m3374,
	GridLayoutGroup_get_startCorner_m3375,
	GridLayoutGroup_set_startCorner_m3376,
	GridLayoutGroup_get_startAxis_m3377,
	GridLayoutGroup_set_startAxis_m3378,
	GridLayoutGroup_get_cellSize_m3379,
	GridLayoutGroup_set_cellSize_m3380,
	GridLayoutGroup_get_spacing_m3381,
	GridLayoutGroup_set_spacing_m3382,
	GridLayoutGroup_get_constraint_m3383,
	GridLayoutGroup_set_constraint_m3384,
	GridLayoutGroup_get_constraintCount_m3385,
	GridLayoutGroup_set_constraintCount_m3386,
	GridLayoutGroup_CalculateLayoutInputHorizontal_m3387,
	GridLayoutGroup_CalculateLayoutInputVertical_m3388,
	GridLayoutGroup_SetLayoutHorizontal_m3389,
	GridLayoutGroup_SetLayoutVertical_m3390,
	GridLayoutGroup_SetCellsAlongAxis_m3391,
	HorizontalLayoutGroup__ctor_m3392,
	HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m3393,
	HorizontalLayoutGroup_CalculateLayoutInputVertical_m3394,
	HorizontalLayoutGroup_SetLayoutHorizontal_m3395,
	HorizontalLayoutGroup_SetLayoutVertical_m3396,
	HorizontalOrVerticalLayoutGroup__ctor_m3397,
	HorizontalOrVerticalLayoutGroup_get_spacing_m3398,
	HorizontalOrVerticalLayoutGroup_set_spacing_m3399,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3400,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m3401,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3402,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m3403,
	HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3404,
	HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3405,
	LayoutElement__ctor_m3406,
	LayoutElement_get_ignoreLayout_m3407,
	LayoutElement_set_ignoreLayout_m3408,
	LayoutElement_CalculateLayoutInputHorizontal_m3409,
	LayoutElement_CalculateLayoutInputVertical_m3410,
	LayoutElement_get_minWidth_m3411,
	LayoutElement_set_minWidth_m3412,
	LayoutElement_get_minHeight_m3413,
	LayoutElement_set_minHeight_m3414,
	LayoutElement_get_preferredWidth_m3415,
	LayoutElement_set_preferredWidth_m3416,
	LayoutElement_get_preferredHeight_m3417,
	LayoutElement_set_preferredHeight_m3418,
	LayoutElement_get_flexibleWidth_m3419,
	LayoutElement_set_flexibleWidth_m3420,
	LayoutElement_get_flexibleHeight_m3421,
	LayoutElement_set_flexibleHeight_m3422,
	LayoutElement_get_layoutPriority_m3423,
	LayoutElement_OnEnable_m3424,
	LayoutElement_OnTransformParentChanged_m3425,
	LayoutElement_OnDisable_m3426,
	LayoutElement_OnDidApplyAnimationProperties_m3427,
	LayoutElement_OnBeforeTransformParentChanged_m3428,
	LayoutElement_SetDirty_m3429,
	LayoutGroup__ctor_m3430,
	LayoutGroup_get_padding_m3431,
	LayoutGroup_set_padding_m3432,
	LayoutGroup_get_childAlignment_m3433,
	LayoutGroup_set_childAlignment_m3434,
	LayoutGroup_get_rectTransform_m3435,
	LayoutGroup_get_rectChildren_m3436,
	LayoutGroup_CalculateLayoutInputHorizontal_m3437,
	LayoutGroup_get_minWidth_m3438,
	LayoutGroup_get_preferredWidth_m3439,
	LayoutGroup_get_flexibleWidth_m3440,
	LayoutGroup_get_minHeight_m3441,
	LayoutGroup_get_preferredHeight_m3442,
	LayoutGroup_get_flexibleHeight_m3443,
	LayoutGroup_get_layoutPriority_m3444,
	LayoutGroup_OnEnable_m3445,
	LayoutGroup_OnDisable_m3446,
	LayoutGroup_OnDidApplyAnimationProperties_m3447,
	LayoutGroup_GetTotalMinSize_m3448,
	LayoutGroup_GetTotalPreferredSize_m3449,
	LayoutGroup_GetTotalFlexibleSize_m3450,
	LayoutGroup_GetStartOffset_m3451,
	LayoutGroup_SetLayoutInputForAxis_m3452,
	LayoutGroup_SetChildAlongAxis_m3453,
	LayoutGroup_get_isRootLayoutGroup_m3454,
	LayoutGroup_OnRectTransformDimensionsChange_m3455,
	LayoutGroup_OnTransformChildrenChanged_m3456,
	LayoutGroup_SetDirty_m3457,
	LayoutRebuilder__ctor_m3458,
	LayoutRebuilder__cctor_m3459,
	LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m3460,
	LayoutRebuilder_ReapplyDrivenProperties_m3461,
	LayoutRebuilder_get_transform_m3462,
	LayoutRebuilder_IsDestroyed_m3463,
	LayoutRebuilder_StripDisabledBehavioursFromList_m3464,
	LayoutRebuilder_ForceRebuildLayoutImmediate_m3465,
	LayoutRebuilder_PerformLayoutControl_m3466,
	LayoutRebuilder_PerformLayoutCalculation_m3467,
	LayoutRebuilder_MarkLayoutForRebuild_m3468,
	LayoutRebuilder_ValidLayoutGroup_m3469,
	LayoutRebuilder_ValidController_m3470,
	LayoutRebuilder_MarkLayoutRootForRebuild_m3471,
	LayoutRebuilder_Equals_m3472,
	LayoutRebuilder_GetHashCode_m3473,
	LayoutRebuilder_ToString_m3474,
	LayoutRebuilder_U3CRebuildU3Em__8_m3475,
	LayoutRebuilder_U3CRebuildU3Em__9_m3476,
	LayoutRebuilder_U3CRebuildU3Em__A_m3477,
	LayoutRebuilder_U3CRebuildU3Em__B_m3478,
	LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3479,
	LayoutUtility_GetMinSize_m3480,
	LayoutUtility_GetPreferredSize_m3481,
	LayoutUtility_GetFlexibleSize_m3482,
	LayoutUtility_GetMinWidth_m3483,
	LayoutUtility_GetPreferredWidth_m3484,
	LayoutUtility_GetFlexibleWidth_m3485,
	LayoutUtility_GetMinHeight_m3486,
	LayoutUtility_GetPreferredHeight_m3487,
	LayoutUtility_GetFlexibleHeight_m3488,
	LayoutUtility_GetLayoutProperty_m3489,
	LayoutUtility_GetLayoutProperty_m3490,
	LayoutUtility_U3CGetMinWidthU3Em__D_m3491,
	LayoutUtility_U3CGetPreferredWidthU3Em__E_m3492,
	LayoutUtility_U3CGetPreferredWidthU3Em__F_m3493,
	LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3494,
	LayoutUtility_U3CGetMinHeightU3Em__11_m3495,
	LayoutUtility_U3CGetPreferredHeightU3Em__12_m3496,
	LayoutUtility_U3CGetPreferredHeightU3Em__13_m3497,
	LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3498,
	VerticalLayoutGroup__ctor_m3499,
	VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3500,
	VerticalLayoutGroup_CalculateLayoutInputVertical_m3501,
	VerticalLayoutGroup_SetLayoutHorizontal_m3502,
	VerticalLayoutGroup_SetLayoutVertical_m3503,
	VertexHelper__ctor_m3504,
	VertexHelper__ctor_m3505,
	VertexHelper__cctor_m3506,
	VertexHelper_get_currentVertCount_m3507,
	VertexHelper_PopulateUIVertex_m3508,
	VertexHelper_FillMesh_m3509,
	VertexHelper_Dispose_m3510,
	VertexHelper_AddVert_m3511,
	VertexHelper_AddVert_m3512,
	VertexHelper_AddVert_m3513,
	VertexHelper_AddTriangle_m3514,
	VertexHelper_AddUIVertexQuad_m3515,
	VertexHelper_AddUIVertexTriangleStream_m3516,
	VertexHelper_GetUIVertexStream_m3517,
	BaseMeshEffect__ctor_m3518,
	BaseMeshEffect_get_graphic_m3519,
	BaseMeshEffect_OnEnable_m3520,
	BaseMeshEffect_OnDisable_m3521,
	BaseMeshEffect_OnDidApplyAnimationProperties_m3522,
	Outline__ctor_m3523,
	Outline_ModifyMesh_m3524,
	PositionAsUV1__ctor_m3525,
	PositionAsUV1_ModifyMesh_m3526,
	Shadow__ctor_m3527,
	Shadow_get_effectColor_m3528,
	Shadow_set_effectColor_m3529,
	Shadow_get_effectDistance_m3530,
	Shadow_set_effectDistance_m3531,
	Shadow_get_useGraphicAlpha_m3532,
	Shadow_set_useGraphicAlpha_m3533,
	Shadow_ApplyShadowZeroAlloc_m3534,
	Shadow_ApplyShadow_m3535,
	Shadow_ModifyMesh_m3536,
	Builtins_join_m3780,
	DispatcherFactory__ctor_m3781,
	DispatcherFactory_Invoke_m3782,
	DispatcherFactory_BeginInvoke_m3783,
	DispatcherFactory_EndInvoke_m3784,
	DispatcherCache__ctor_m3785,
	DispatcherCache__cctor_m3786,
	DispatcherCache_Get_m3787,
	_EqualityComparer__ctor_m3788,
	_EqualityComparer_GetHashCode_m3789,
	_EqualityComparer_Equals_m3790,
	DispatcherKey__ctor_m3791,
	DispatcherKey__cctor_m3792,
	ExtensionRegistry__ctor_m3793,
	ExtensionRegistry_get_Extensions_m3794,
	U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m3795,
	U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m3796,
	U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3797,
	U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerable_GetEnumerator_m3798,
	U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m3799,
	U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m3800,
	U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m3801,
	U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m3802,
	U3CCoerceU3Ec__AnonStorey1D__ctor_m3803,
	U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m3804,
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m3805,
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m3806,
	RuntimeServices__cctor_m3807,
	RuntimeServices_GetDispatcher_m3808,
	RuntimeServices_Coerce_m3809,
	RuntimeServices_CreateCoerceDispatcher_m3810,
	RuntimeServices_EmitPromotionDispatcher_m3811,
	RuntimeServices_IsPromotableNumeric_m3812,
	RuntimeServices_EmitImplicitConversionDispatcher_m3813,
	RuntimeServices_CoercibleDispatcher_m3814,
	RuntimeServices_IdentityDispatcher_m3815,
	RuntimeServices_IsNumeric_m3816,
	RuntimeServices_op_Addition_m691,
	RuntimeServices_op_Addition_m668,
	RuntimeServices_EqualityOperator_m3817,
	RuntimeServices_ArrayEqualityImpl_m3818,
	RuntimeServices_GetConvertTypeCode_m3819,
	RuntimeServices_EqualityOperator_m3820,
	RuntimeServices_IsPromotableNumeric_m3821,
	RuntimeServices_FindImplicitConversionOperator_m3822,
	RuntimeServices_GetExtensionMethods_m3823,
	RuntimeServices_FindImplicitConversionMethod_m3824,
	Dispatcher__ctor_m3825,
	Dispatcher_Invoke_m3826,
	Dispatcher_BeginInvoke_m3827,
	Dispatcher_EndInvoke_m3828,
	ExtensionAttribute__ctor_m3839,
	Locale_GetText_m3840,
	Locale_GetText_m3841,
	KeyBuilder_get_Rng_m3842,
	KeyBuilder_Key_m3843,
	KeyBuilder_IV_m3844,
	SymmetricTransform__ctor_m3845,
	SymmetricTransform_System_IDisposable_Dispose_m3846,
	SymmetricTransform_Finalize_m3847,
	SymmetricTransform_Dispose_m3848,
	SymmetricTransform_get_CanReuseTransform_m3849,
	SymmetricTransform_Transform_m3850,
	SymmetricTransform_CBC_m3851,
	SymmetricTransform_CFB_m3852,
	SymmetricTransform_OFB_m3853,
	SymmetricTransform_CTS_m3854,
	SymmetricTransform_CheckInput_m3855,
	SymmetricTransform_TransformBlock_m3856,
	SymmetricTransform_get_KeepLastBlock_m3857,
	SymmetricTransform_InternalTransformBlock_m3858,
	SymmetricTransform_Random_m3859,
	SymmetricTransform_ThrowBadPaddingException_m3860,
	SymmetricTransform_FinalEncrypt_m3861,
	SymmetricTransform_FinalDecrypt_m3862,
	SymmetricTransform_TransformFinalBlock_m3863,
	Check_Source_m3864,
	Check_SourceAndSelector_m3865,
	Check_SourceAndPredicate_m3866,
	Aes__ctor_m3867,
	AesManaged__ctor_m3868,
	AesManaged_GenerateIV_m3869,
	AesManaged_GenerateKey_m3870,
	AesManaged_CreateDecryptor_m3871,
	AesManaged_CreateEncryptor_m3872,
	AesManaged_get_IV_m3873,
	AesManaged_set_IV_m3874,
	AesManaged_get_Key_m3875,
	AesManaged_set_Key_m3876,
	AesManaged_get_KeySize_m3877,
	AesManaged_set_KeySize_m3878,
	AesManaged_CreateDecryptor_m3879,
	AesManaged_CreateEncryptor_m3880,
	AesManaged_Dispose_m3881,
	AesTransform__ctor_m3882,
	AesTransform__cctor_m3883,
	AesTransform_ECB_m3884,
	AesTransform_SubByte_m3885,
	AesTransform_Encrypt128_m3886,
	AesTransform_Decrypt128_m3887,
	Action__ctor_m3888,
	Action_Invoke_m411,
	Action_BeginInvoke_m3889,
	Action_EndInvoke_m3890,
	Locale_GetText_m3911,
	Locale_GetText_m3912,
	MonoTODOAttribute__ctor_m3913,
	MonoTODOAttribute__ctor_m3914,
	HybridDictionary__ctor_m3915,
	HybridDictionary__ctor_m3916,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m3917,
	HybridDictionary_get_inner_m3918,
	HybridDictionary_get_Count_m3919,
	HybridDictionary_get_IsSynchronized_m3920,
	HybridDictionary_get_Item_m3921,
	HybridDictionary_set_Item_m3922,
	HybridDictionary_get_SyncRoot_m3923,
	HybridDictionary_Add_m3924,
	HybridDictionary_Contains_m3925,
	HybridDictionary_CopyTo_m3926,
	HybridDictionary_GetEnumerator_m3927,
	HybridDictionary_Remove_m3928,
	HybridDictionary_Switch_m3929,
	DictionaryNode__ctor_m3930,
	DictionaryNodeEnumerator__ctor_m3931,
	DictionaryNodeEnumerator_FailFast_m3932,
	DictionaryNodeEnumerator_MoveNext_m3933,
	DictionaryNodeEnumerator_Reset_m3934,
	DictionaryNodeEnumerator_get_Current_m3935,
	DictionaryNodeEnumerator_get_DictionaryNode_m3936,
	DictionaryNodeEnumerator_get_Entry_m3937,
	DictionaryNodeEnumerator_get_Key_m3938,
	DictionaryNodeEnumerator_get_Value_m3939,
	ListDictionary__ctor_m3940,
	ListDictionary__ctor_m3941,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m3942,
	ListDictionary_FindEntry_m3943,
	ListDictionary_FindEntry_m3944,
	ListDictionary_AddImpl_m3945,
	ListDictionary_get_Count_m3946,
	ListDictionary_get_IsSynchronized_m3947,
	ListDictionary_get_SyncRoot_m3948,
	ListDictionary_CopyTo_m3949,
	ListDictionary_get_Item_m3950,
	ListDictionary_set_Item_m3951,
	ListDictionary_Add_m3952,
	ListDictionary_Clear_m3953,
	ListDictionary_Contains_m3954,
	ListDictionary_GetEnumerator_m3955,
	ListDictionary_Remove_m3956,
	_Item__ctor_m3957,
	_KeysEnumerator__ctor_m3958,
	_KeysEnumerator_get_Current_m3959,
	_KeysEnumerator_MoveNext_m3960,
	_KeysEnumerator_Reset_m3961,
	KeysCollection__ctor_m3962,
	KeysCollection_System_Collections_ICollection_CopyTo_m3963,
	KeysCollection_System_Collections_ICollection_get_IsSynchronized_m3964,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m3965,
	KeysCollection_get_Count_m3966,
	KeysCollection_GetEnumerator_m3967,
	NameObjectCollectionBase__ctor_m3968,
	NameObjectCollectionBase__ctor_m3969,
	NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m3970,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m3971,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3972,
	NameObjectCollectionBase_Init_m3973,
	NameObjectCollectionBase_get_Keys_m3974,
	NameObjectCollectionBase_GetEnumerator_m3975,
	NameObjectCollectionBase_GetObjectData_m3976,
	NameObjectCollectionBase_get_Count_m3977,
	NameObjectCollectionBase_OnDeserialization_m3978,
	NameObjectCollectionBase_get_IsReadOnly_m3979,
	NameObjectCollectionBase_BaseAdd_m3980,
	NameObjectCollectionBase_BaseGet_m3981,
	NameObjectCollectionBase_BaseGet_m3982,
	NameObjectCollectionBase_BaseGetKey_m3983,
	NameObjectCollectionBase_FindFirstMatchedItem_m3984,
	NameValueCollection__ctor_m3985,
	NameValueCollection__ctor_m3986,
	NameValueCollection_Add_m3987,
	NameValueCollection_Get_m3988,
	NameValueCollection_AsSingleString_m3989,
	NameValueCollection_GetKey_m3990,
	NameValueCollection_InvalidateCachedArrays_m3991,
	EditorBrowsableAttribute__ctor_m3992,
	EditorBrowsableAttribute_get_State_m3993,
	EditorBrowsableAttribute_Equals_m3994,
	EditorBrowsableAttribute_GetHashCode_m3995,
	TypeConverterAttribute__ctor_m3996,
	TypeConverterAttribute__ctor_m3997,
	TypeConverterAttribute__cctor_m3998,
	TypeConverterAttribute_Equals_m3999,
	TypeConverterAttribute_GetHashCode_m4000,
	TypeConverterAttribute_get_ConverterTypeName_m4001,
	DefaultCertificatePolicy__ctor_m4002,
	DefaultCertificatePolicy_CheckValidationResult_m4003,
	FileWebRequest__ctor_m4004,
	FileWebRequest__ctor_m4005,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4006,
	FileWebRequest_GetObjectData_m4007,
	FileWebRequestCreator__ctor_m4008,
	FileWebRequestCreator_Create_m4009,
	FtpRequestCreator__ctor_m4010,
	FtpRequestCreator_Create_m4011,
	FtpWebRequest__ctor_m4012,
	FtpWebRequest__cctor_m4013,
	FtpWebRequest_U3CcallbackU3Em__B_m4014,
	GlobalProxySelection_get_Select_m4015,
	HttpRequestCreator__ctor_m4016,
	HttpRequestCreator_Create_m4017,
	HttpVersion__cctor_m4018,
	HttpWebRequest__ctor_m4019,
	HttpWebRequest__ctor_m4020,
	HttpWebRequest__cctor_m4021,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4022,
	HttpWebRequest_get_Address_m4023,
	HttpWebRequest_get_ServicePoint_m4024,
	HttpWebRequest_GetServicePoint_m4025,
	HttpWebRequest_GetObjectData_m4026,
	IPAddress__ctor_m4027,
	IPAddress__ctor_m4028,
	IPAddress__cctor_m4029,
	IPAddress_SwapShort_m4030,
	IPAddress_HostToNetworkOrder_m4031,
	IPAddress_NetworkToHostOrder_m4032,
	IPAddress_Parse_m4033,
	IPAddress_TryParse_m4034,
	IPAddress_ParseIPV4_m4035,
	IPAddress_ParseIPV6_m4036,
	IPAddress_get_InternalIPv4Address_m4037,
	IPAddress_get_ScopeId_m4038,
	IPAddress_get_AddressFamily_m4039,
	IPAddress_IsLoopback_m4040,
	IPAddress_ToString_m4041,
	IPAddress_ToString_m4042,
	IPAddress_Equals_m4043,
	IPAddress_GetHashCode_m4044,
	IPAddress_Hash_m4045,
	IPv6Address__ctor_m4046,
	IPv6Address__ctor_m4047,
	IPv6Address__ctor_m4048,
	IPv6Address__cctor_m4049,
	IPv6Address_Parse_m4050,
	IPv6Address_Fill_m4051,
	IPv6Address_TryParse_m4052,
	IPv6Address_TryParse_m4053,
	IPv6Address_get_Address_m4054,
	IPv6Address_get_ScopeId_m4055,
	IPv6Address_set_ScopeId_m4056,
	IPv6Address_IsLoopback_m4057,
	IPv6Address_SwapUShort_m4058,
	IPv6Address_AsIPv4Int_m4059,
	IPv6Address_IsIPv4Compatible_m4060,
	IPv6Address_IsIPv4Mapped_m4061,
	IPv6Address_ToString_m4062,
	IPv6Address_ToString_m4063,
	IPv6Address_Equals_m4064,
	IPv6Address_GetHashCode_m4065,
	IPv6Address_Hash_m4066,
	ServicePoint__ctor_m4067,
	ServicePoint_get_Address_m4068,
	ServicePoint_get_CurrentConnections_m4069,
	ServicePoint_get_IdleSince_m4070,
	ServicePoint_set_IdleSince_m4071,
	ServicePoint_set_Expect100Continue_m4072,
	ServicePoint_set_UseNagleAlgorithm_m4073,
	ServicePoint_set_SendContinue_m4074,
	ServicePoint_set_UsesProxy_m4075,
	ServicePoint_set_UseConnect_m4076,
	ServicePoint_get_AvailableForRecycling_m4077,
	SPKey__ctor_m4078,
	SPKey_GetHashCode_m4079,
	SPKey_Equals_m4080,
	ServicePointManager__cctor_m4081,
	ServicePointManager_get_CertificatePolicy_m4082,
	ServicePointManager_get_CheckCertificateRevocationList_m4083,
	ServicePointManager_get_SecurityProtocol_m4084,
	ServicePointManager_get_ServerCertificateValidationCallback_m4085,
	ServicePointManager_FindServicePoint_m4086,
	ServicePointManager_RecycleServicePoints_m4087,
	WebHeaderCollection__ctor_m4088,
	WebHeaderCollection__ctor_m4089,
	WebHeaderCollection__ctor_m4090,
	WebHeaderCollection__cctor_m4091,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4092,
	WebHeaderCollection_Add_m4093,
	WebHeaderCollection_AddWithoutValidate_m4094,
	WebHeaderCollection_IsRestricted_m4095,
	WebHeaderCollection_OnDeserialization_m4096,
	WebHeaderCollection_ToString_m4097,
	WebHeaderCollection_GetObjectData_m4098,
	WebHeaderCollection_get_Count_m4099,
	WebHeaderCollection_get_Keys_m4100,
	WebHeaderCollection_Get_m4101,
	WebHeaderCollection_GetKey_m4102,
	WebHeaderCollection_GetEnumerator_m4103,
	WebHeaderCollection_IsHeaderValue_m4104,
	WebHeaderCollection_IsHeaderName_m4105,
	WebProxy__ctor_m4106,
	WebProxy__ctor_m4107,
	WebProxy__ctor_m4108,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4109,
	WebProxy_get_UseDefaultCredentials_m4110,
	WebProxy_GetProxy_m4111,
	WebProxy_IsBypassed_m4112,
	WebProxy_GetObjectData_m4113,
	WebProxy_CheckBypassList_m4114,
	WebRequest__ctor_m4115,
	WebRequest__ctor_m4116,
	WebRequest__cctor_m4117,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4118,
	WebRequest_AddDynamicPrefix_m4119,
	WebRequest_GetMustImplement_m4120,
	WebRequest_get_DefaultWebProxy_m4121,
	WebRequest_GetDefaultWebProxy_m4122,
	WebRequest_GetObjectData_m4123,
	WebRequest_AddPrefix_m4124,
	PublicKey__ctor_m4125,
	PublicKey_get_EncodedKeyValue_m4126,
	PublicKey_get_EncodedParameters_m4127,
	PublicKey_get_Key_m4128,
	PublicKey_get_Oid_m4129,
	PublicKey_GetUnsignedBigInteger_m4130,
	PublicKey_DecodeDSA_m4131,
	PublicKey_DecodeRSA_m4132,
	X500DistinguishedName__ctor_m4133,
	X500DistinguishedName_Decode_m4134,
	X500DistinguishedName_GetSeparator_m4135,
	X500DistinguishedName_DecodeRawData_m4136,
	X500DistinguishedName_Canonize_m4137,
	X500DistinguishedName_AreEqual_m4138,
	X509BasicConstraintsExtension__ctor_m4139,
	X509BasicConstraintsExtension__ctor_m4140,
	X509BasicConstraintsExtension__ctor_m4141,
	X509BasicConstraintsExtension_get_CertificateAuthority_m4142,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4143,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m4144,
	X509BasicConstraintsExtension_CopyFrom_m4145,
	X509BasicConstraintsExtension_Decode_m4146,
	X509BasicConstraintsExtension_Encode_m4147,
	X509BasicConstraintsExtension_ToString_m4148,
	X509Certificate2__ctor_m4149,
	X509Certificate2__cctor_m4150,
	X509Certificate2_get_Extensions_m4151,
	X509Certificate2_get_IssuerName_m4152,
	X509Certificate2_get_NotAfter_m4153,
	X509Certificate2_get_NotBefore_m4154,
	X509Certificate2_get_PrivateKey_m4155,
	X509Certificate2_get_PublicKey_m4156,
	X509Certificate2_get_SerialNumber_m4157,
	X509Certificate2_get_SignatureAlgorithm_m4158,
	X509Certificate2_get_SubjectName_m4159,
	X509Certificate2_get_Thumbprint_m4160,
	X509Certificate2_get_Version_m4161,
	X509Certificate2_GetNameInfo_m4162,
	X509Certificate2_Find_m4163,
	X509Certificate2_GetValueAsString_m4164,
	X509Certificate2_ImportPkcs12_m4165,
	X509Certificate2_Import_m4166,
	X509Certificate2_Reset_m4167,
	X509Certificate2_ToString_m4168,
	X509Certificate2_ToString_m4169,
	X509Certificate2_AppendBuffer_m4170,
	X509Certificate2_Verify_m4171,
	X509Certificate2_get_MonoCertificate_m4172,
	X509Certificate2Collection__ctor_m4173,
	X509Certificate2Collection__ctor_m4174,
	X509Certificate2Collection_get_Item_m4175,
	X509Certificate2Collection_Add_m4176,
	X509Certificate2Collection_AddRange_m4177,
	X509Certificate2Collection_Contains_m4178,
	X509Certificate2Collection_Find_m4179,
	X509Certificate2Collection_GetEnumerator_m4180,
	X509Certificate2Enumerator__ctor_m4181,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m4182,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m4183,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m4184,
	X509Certificate2Enumerator_get_Current_m4185,
	X509Certificate2Enumerator_MoveNext_m4186,
	X509Certificate2Enumerator_Reset_m4187,
	X509CertificateEnumerator__ctor_m4188,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4189,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4190,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4191,
	X509CertificateEnumerator_get_Current_m4192,
	X509CertificateEnumerator_MoveNext_m4193,
	X509CertificateEnumerator_Reset_m4194,
	X509CertificateCollection__ctor_m4195,
	X509CertificateCollection__ctor_m4196,
	X509CertificateCollection_get_Item_m4197,
	X509CertificateCollection_AddRange_m4198,
	X509CertificateCollection_GetEnumerator_m4199,
	X509CertificateCollection_GetHashCode_m4200,
	X509Chain__ctor_m4201,
	X509Chain__ctor_m4202,
	X509Chain__cctor_m4203,
	X509Chain_get_ChainPolicy_m4204,
	X509Chain_Build_m4205,
	X509Chain_Reset_m4206,
	X509Chain_get_Roots_m4207,
	X509Chain_get_CertificateAuthorities_m4208,
	X509Chain_get_CertificateCollection_m4209,
	X509Chain_BuildChainFrom_m4210,
	X509Chain_SelectBestFromCollection_m4211,
	X509Chain_FindParent_m4212,
	X509Chain_IsChainComplete_m4213,
	X509Chain_IsSelfIssued_m4214,
	X509Chain_ValidateChain_m4215,
	X509Chain_Process_m4216,
	X509Chain_PrepareForNextCertificate_m4217,
	X509Chain_WrapUp_m4218,
	X509Chain_ProcessCertificateExtensions_m4219,
	X509Chain_IsSignedWith_m4220,
	X509Chain_GetSubjectKeyIdentifier_m4221,
	X509Chain_GetAuthorityKeyIdentifier_m4222,
	X509Chain_GetAuthorityKeyIdentifier_m4223,
	X509Chain_GetAuthorityKeyIdentifier_m4224,
	X509Chain_CheckRevocationOnChain_m4225,
	X509Chain_CheckRevocation_m4226,
	X509Chain_CheckRevocation_m4227,
	X509Chain_FindCrl_m4228,
	X509Chain_ProcessCrlExtensions_m4229,
	X509Chain_ProcessCrlEntryExtensions_m4230,
	X509ChainElement__ctor_m4231,
	X509ChainElement_get_Certificate_m4232,
	X509ChainElement_get_ChainElementStatus_m4233,
	X509ChainElement_get_StatusFlags_m4234,
	X509ChainElement_set_StatusFlags_m4235,
	X509ChainElement_Count_m4236,
	X509ChainElement_Set_m4237,
	X509ChainElement_UncompressFlags_m4238,
	X509ChainElementCollection__ctor_m4239,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4240,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m4241,
	X509ChainElementCollection_get_Count_m4242,
	X509ChainElementCollection_get_IsSynchronized_m4243,
	X509ChainElementCollection_get_Item_m4244,
	X509ChainElementCollection_get_SyncRoot_m4245,
	X509ChainElementCollection_GetEnumerator_m4246,
	X509ChainElementCollection_Add_m4247,
	X509ChainElementCollection_Clear_m4248,
	X509ChainElementCollection_Contains_m4249,
	X509ChainElementEnumerator__ctor_m4250,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m4251,
	X509ChainElementEnumerator_get_Current_m4252,
	X509ChainElementEnumerator_MoveNext_m4253,
	X509ChainElementEnumerator_Reset_m4254,
	X509ChainPolicy__ctor_m4255,
	X509ChainPolicy_get_ExtraStore_m4256,
	X509ChainPolicy_get_RevocationFlag_m4257,
	X509ChainPolicy_get_RevocationMode_m4258,
	X509ChainPolicy_get_VerificationFlags_m4259,
	X509ChainPolicy_get_VerificationTime_m4260,
	X509ChainPolicy_Reset_m4261,
	X509ChainStatus__ctor_m4262,
	X509ChainStatus_get_Status_m4263,
	X509ChainStatus_set_Status_m4264,
	X509ChainStatus_set_StatusInformation_m4265,
	X509ChainStatus_GetInformation_m4266,
	X509EnhancedKeyUsageExtension__ctor_m4267,
	X509EnhancedKeyUsageExtension_CopyFrom_m4268,
	X509EnhancedKeyUsageExtension_Decode_m4269,
	X509EnhancedKeyUsageExtension_ToString_m4270,
	X509Extension__ctor_m4271,
	X509Extension__ctor_m4272,
	X509Extension_get_Critical_m4273,
	X509Extension_set_Critical_m4274,
	X509Extension_CopyFrom_m4275,
	X509Extension_FormatUnkownData_m4276,
	X509ExtensionCollection__ctor_m4277,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m4278,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4279,
	X509ExtensionCollection_get_Count_m4280,
	X509ExtensionCollection_get_IsSynchronized_m4281,
	X509ExtensionCollection_get_SyncRoot_m4282,
	X509ExtensionCollection_get_Item_m4283,
	X509ExtensionCollection_GetEnumerator_m4284,
	X509ExtensionEnumerator__ctor_m4285,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4286,
	X509ExtensionEnumerator_get_Current_m4287,
	X509ExtensionEnumerator_MoveNext_m4288,
	X509ExtensionEnumerator_Reset_m4289,
	X509KeyUsageExtension__ctor_m4290,
	X509KeyUsageExtension__ctor_m4291,
	X509KeyUsageExtension__ctor_m4292,
	X509KeyUsageExtension_get_KeyUsages_m4293,
	X509KeyUsageExtension_CopyFrom_m4294,
	X509KeyUsageExtension_GetValidFlags_m4295,
	X509KeyUsageExtension_Decode_m4296,
	X509KeyUsageExtension_Encode_m4297,
	X509KeyUsageExtension_ToString_m4298,
	X509Store__ctor_m4299,
	X509Store_get_Certificates_m4300,
	X509Store_get_Factory_m4301,
	X509Store_get_Store_m4302,
	X509Store_Close_m4303,
	X509Store_Open_m4304,
	X509SubjectKeyIdentifierExtension__ctor_m4305,
	X509SubjectKeyIdentifierExtension__ctor_m4306,
	X509SubjectKeyIdentifierExtension__ctor_m4307,
	X509SubjectKeyIdentifierExtension__ctor_m4308,
	X509SubjectKeyIdentifierExtension__ctor_m4309,
	X509SubjectKeyIdentifierExtension__ctor_m4310,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4311,
	X509SubjectKeyIdentifierExtension_CopyFrom_m4312,
	X509SubjectKeyIdentifierExtension_FromHexChar_m4313,
	X509SubjectKeyIdentifierExtension_FromHexChars_m4314,
	X509SubjectKeyIdentifierExtension_FromHex_m4315,
	X509SubjectKeyIdentifierExtension_Decode_m4316,
	X509SubjectKeyIdentifierExtension_Encode_m4317,
	X509SubjectKeyIdentifierExtension_ToString_m4318,
	AsnEncodedData__ctor_m4319,
	AsnEncodedData__ctor_m4320,
	AsnEncodedData__ctor_m4321,
	AsnEncodedData_get_Oid_m4322,
	AsnEncodedData_set_Oid_m4323,
	AsnEncodedData_get_RawData_m4324,
	AsnEncodedData_set_RawData_m4325,
	AsnEncodedData_CopyFrom_m4326,
	AsnEncodedData_ToString_m4327,
	AsnEncodedData_Default_m4328,
	AsnEncodedData_BasicConstraintsExtension_m4329,
	AsnEncodedData_EnhancedKeyUsageExtension_m4330,
	AsnEncodedData_KeyUsageExtension_m4331,
	AsnEncodedData_SubjectKeyIdentifierExtension_m4332,
	AsnEncodedData_SubjectAltName_m4333,
	AsnEncodedData_NetscapeCertType_m4334,
	Oid__ctor_m4335,
	Oid__ctor_m4336,
	Oid__ctor_m4337,
	Oid__ctor_m4338,
	Oid_get_FriendlyName_m4339,
	Oid_get_Value_m4340,
	Oid_GetName_m4341,
	OidCollection__ctor_m4342,
	OidCollection_System_Collections_ICollection_CopyTo_m4343,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m4344,
	OidCollection_get_Count_m4345,
	OidCollection_get_IsSynchronized_m4346,
	OidCollection_get_Item_m4347,
	OidCollection_get_SyncRoot_m4348,
	OidCollection_Add_m4349,
	OidEnumerator__ctor_m4350,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m4351,
	OidEnumerator_MoveNext_m4352,
	OidEnumerator_Reset_m4353,
	MatchAppendEvaluator__ctor_m4354,
	MatchAppendEvaluator_Invoke_m4355,
	MatchAppendEvaluator_BeginInvoke_m4356,
	MatchAppendEvaluator_EndInvoke_m4357,
	BaseMachine__ctor_m4358,
	BaseMachine_Replace_m4359,
	BaseMachine_Scan_m4360,
	BaseMachine_LTRReplace_m4361,
	BaseMachine_RTLReplace_m4362,
	Capture__ctor_m4363,
	Capture__ctor_m4364,
	Capture_get_Index_m4365,
	Capture_get_Length_m4366,
	Capture_get_Value_m4367,
	Capture_ToString_m4368,
	Capture_get_Text_m4369,
	CaptureCollection__ctor_m4370,
	CaptureCollection_get_Count_m4371,
	CaptureCollection_get_IsSynchronized_m4372,
	CaptureCollection_SetValue_m4373,
	CaptureCollection_get_SyncRoot_m4374,
	CaptureCollection_CopyTo_m4375,
	CaptureCollection_GetEnumerator_m4376,
	Group__ctor_m4377,
	Group__ctor_m4378,
	Group__ctor_m4379,
	Group__cctor_m4380,
	Group_get_Captures_m4381,
	Group_get_Success_m4382,
	GroupCollection__ctor_m4383,
	GroupCollection_get_Count_m4384,
	GroupCollection_get_IsSynchronized_m4385,
	GroupCollection_get_Item_m4386,
	GroupCollection_SetValue_m4387,
	GroupCollection_get_SyncRoot_m4388,
	GroupCollection_CopyTo_m4389,
	GroupCollection_GetEnumerator_m4390,
	Match__ctor_m4391,
	Match__ctor_m4392,
	Match__ctor_m4393,
	Match__cctor_m4394,
	Match_get_Empty_m4395,
	Match_get_Groups_m4396,
	Match_NextMatch_m4397,
	Match_get_Regex_m4398,
	Enumerator__ctor_m4399,
	Enumerator_System_Collections_IEnumerator_Reset_m4400,
	Enumerator_System_Collections_IEnumerator_get_Current_m4401,
	Enumerator_System_Collections_IEnumerator_MoveNext_m4402,
	MatchCollection__ctor_m4403,
	MatchCollection_get_Count_m4404,
	MatchCollection_get_IsSynchronized_m4405,
	MatchCollection_get_Item_m4406,
	MatchCollection_get_SyncRoot_m4407,
	MatchCollection_CopyTo_m4408,
	MatchCollection_GetEnumerator_m4409,
	MatchCollection_TryToGet_m4410,
	MatchCollection_get_FullList_m4411,
	Regex__ctor_m4412,
	Regex__ctor_m578,
	Regex__ctor_m4413,
	Regex__ctor_m4414,
	Regex__cctor_m4415,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m4416,
	Regex_Replace_m2225,
	Regex_Replace_m4417,
	Regex_validate_options_m4418,
	Regex_Init_m4419,
	Regex_InitNewRegex_m4420,
	Regex_CreateMachineFactory_m4421,
	Regex_get_Options_m4422,
	Regex_get_RightToLeft_m4423,
	Regex_GroupNumberFromName_m4424,
	Regex_GetGroupIndex_m4425,
	Regex_default_startat_m4426,
	Regex_IsMatch_m4427,
	Regex_IsMatch_m4428,
	Regex_Match_m4429,
	Regex_Matches_m4430,
	Regex_Matches_m4431,
	Regex_Replace_m580,
	Regex_Replace_m4432,
	Regex_ToString_m4433,
	Regex_get_GroupCount_m4434,
	Regex_get_Gap_m4435,
	Regex_CreateMachine_m4436,
	Regex_GetGroupNamesArray_m4437,
	Regex_get_GroupNumbers_m4438,
	Key__ctor_m4439,
	Key_GetHashCode_m4440,
	Key_Equals_m4441,
	Key_ToString_m4442,
	FactoryCache__ctor_m4443,
	FactoryCache_Add_m4444,
	FactoryCache_Cleanup_m4445,
	FactoryCache_Lookup_m4446,
	Node__ctor_m4447,
	MRUList__ctor_m4448,
	MRUList_Use_m4449,
	MRUList_Evict_m4450,
	CategoryUtils_CategoryFromName_m4451,
	CategoryUtils_IsCategory_m4452,
	CategoryUtils_IsCategory_m4453,
	LinkRef__ctor_m4454,
	InterpreterFactory__ctor_m4455,
	InterpreterFactory_NewInstance_m4456,
	InterpreterFactory_get_GroupCount_m4457,
	InterpreterFactory_get_Gap_m4458,
	InterpreterFactory_set_Gap_m4459,
	InterpreterFactory_get_Mapping_m4460,
	InterpreterFactory_set_Mapping_m4461,
	InterpreterFactory_get_NamesMapping_m4462,
	InterpreterFactory_set_NamesMapping_m4463,
	PatternLinkStack__ctor_m4464,
	PatternLinkStack_set_BaseAddress_m4465,
	PatternLinkStack_get_OffsetAddress_m4466,
	PatternLinkStack_set_OffsetAddress_m4467,
	PatternLinkStack_GetOffset_m4468,
	PatternLinkStack_GetCurrent_m4469,
	PatternLinkStack_SetCurrent_m4470,
	PatternCompiler__ctor_m4471,
	PatternCompiler_EncodeOp_m4472,
	PatternCompiler_GetMachineFactory_m4473,
	PatternCompiler_EmitFalse_m4474,
	PatternCompiler_EmitTrue_m4475,
	PatternCompiler_EmitCount_m4476,
	PatternCompiler_EmitCharacter_m4477,
	PatternCompiler_EmitCategory_m4478,
	PatternCompiler_EmitNotCategory_m4479,
	PatternCompiler_EmitRange_m4480,
	PatternCompiler_EmitSet_m4481,
	PatternCompiler_EmitString_m4482,
	PatternCompiler_EmitPosition_m4483,
	PatternCompiler_EmitOpen_m4484,
	PatternCompiler_EmitClose_m4485,
	PatternCompiler_EmitBalanceStart_m4486,
	PatternCompiler_EmitBalance_m4487,
	PatternCompiler_EmitReference_m4488,
	PatternCompiler_EmitIfDefined_m4489,
	PatternCompiler_EmitSub_m4490,
	PatternCompiler_EmitTest_m4491,
	PatternCompiler_EmitBranch_m4492,
	PatternCompiler_EmitJump_m4493,
	PatternCompiler_EmitRepeat_m4494,
	PatternCompiler_EmitUntil_m4495,
	PatternCompiler_EmitFastRepeat_m4496,
	PatternCompiler_EmitIn_m4497,
	PatternCompiler_EmitAnchor_m4498,
	PatternCompiler_EmitInfo_m4499,
	PatternCompiler_NewLink_m4500,
	PatternCompiler_ResolveLink_m4501,
	PatternCompiler_EmitBranchEnd_m4502,
	PatternCompiler_EmitAlternationEnd_m4503,
	PatternCompiler_MakeFlags_m4504,
	PatternCompiler_Emit_m4505,
	PatternCompiler_Emit_m4506,
	PatternCompiler_Emit_m4507,
	PatternCompiler_get_CurrentAddress_m4508,
	PatternCompiler_BeginLink_m4509,
	PatternCompiler_EmitLink_m4510,
	LinkStack__ctor_m4511,
	LinkStack_Push_m4512,
	LinkStack_Pop_m4513,
	Mark_get_IsDefined_m4514,
	Mark_get_Index_m4515,
	Mark_get_Length_m4516,
	IntStack_Pop_m4517,
	IntStack_Push_m4518,
	IntStack_get_Count_m4519,
	IntStack_set_Count_m4520,
	RepeatContext__ctor_m4521,
	RepeatContext_get_Count_m4522,
	RepeatContext_set_Count_m4523,
	RepeatContext_get_Start_m4524,
	RepeatContext_set_Start_m4525,
	RepeatContext_get_IsMinimum_m4526,
	RepeatContext_get_IsMaximum_m4527,
	RepeatContext_get_IsLazy_m4528,
	RepeatContext_get_Expression_m4529,
	RepeatContext_get_Previous_m4530,
	Interpreter__ctor_m4531,
	Interpreter_ReadProgramCount_m4532,
	Interpreter_Scan_m4533,
	Interpreter_Reset_m4534,
	Interpreter_Eval_m4535,
	Interpreter_EvalChar_m4536,
	Interpreter_TryMatch_m4537,
	Interpreter_IsPosition_m4538,
	Interpreter_IsWordChar_m4539,
	Interpreter_GetString_m4540,
	Interpreter_Open_m4541,
	Interpreter_Close_m4542,
	Interpreter_Balance_m4543,
	Interpreter_Checkpoint_m4544,
	Interpreter_Backtrack_m4545,
	Interpreter_ResetGroups_m4546,
	Interpreter_GetLastDefined_m4547,
	Interpreter_CreateMark_m4548,
	Interpreter_GetGroupInfo_m4549,
	Interpreter_PopulateGroup_m4550,
	Interpreter_GenerateMatch_m4551,
	Interval__ctor_m4552,
	Interval_get_Empty_m4553,
	Interval_get_IsDiscontiguous_m4554,
	Interval_get_IsSingleton_m4555,
	Interval_get_IsEmpty_m4556,
	Interval_get_Size_m4557,
	Interval_IsDisjoint_m4558,
	Interval_IsAdjacent_m4559,
	Interval_Contains_m4560,
	Interval_Contains_m4561,
	Interval_Intersects_m4562,
	Interval_Merge_m4563,
	Interval_CompareTo_m4564,
	Enumerator__ctor_m4565,
	Enumerator_get_Current_m4566,
	Enumerator_MoveNext_m4567,
	Enumerator_Reset_m4568,
	CostDelegate__ctor_m4569,
	CostDelegate_Invoke_m4570,
	CostDelegate_BeginInvoke_m4571,
	CostDelegate_EndInvoke_m4572,
	IntervalCollection__ctor_m4573,
	IntervalCollection_get_Item_m4574,
	IntervalCollection_Add_m4575,
	IntervalCollection_Normalize_m4576,
	IntervalCollection_GetMetaCollection_m4577,
	IntervalCollection_Optimize_m4578,
	IntervalCollection_get_Count_m4579,
	IntervalCollection_get_IsSynchronized_m4580,
	IntervalCollection_get_SyncRoot_m4581,
	IntervalCollection_CopyTo_m4582,
	IntervalCollection_GetEnumerator_m4583,
	Parser__ctor_m4584,
	Parser_ParseDecimal_m4585,
	Parser_ParseOctal_m4586,
	Parser_ParseHex_m4587,
	Parser_ParseNumber_m4588,
	Parser_ParseName_m4589,
	Parser_ParseRegularExpression_m4590,
	Parser_GetMapping_m4591,
	Parser_ParseGroup_m4592,
	Parser_ParseGroupingConstruct_m4593,
	Parser_ParseAssertionType_m4594,
	Parser_ParseOptions_m4595,
	Parser_ParseCharacterClass_m4596,
	Parser_ParseRepetitionBounds_m4597,
	Parser_ParseUnicodeCategory_m4598,
	Parser_ParseSpecial_m4599,
	Parser_ParseEscape_m4600,
	Parser_ParseName_m4601,
	Parser_IsNameChar_m4602,
	Parser_ParseNumber_m4603,
	Parser_ParseDigit_m4604,
	Parser_ConsumeWhitespace_m4605,
	Parser_ResolveReferences_m4606,
	Parser_HandleExplicitNumericGroups_m4607,
	Parser_IsIgnoreCase_m4608,
	Parser_IsMultiline_m4609,
	Parser_IsExplicitCapture_m4610,
	Parser_IsSingleline_m4611,
	Parser_IsIgnorePatternWhitespace_m4612,
	Parser_IsECMAScript_m4613,
	Parser_NewParseException_m4614,
	QuickSearch__ctor_m4615,
	QuickSearch__cctor_m4616,
	QuickSearch_get_Length_m4617,
	QuickSearch_Search_m4618,
	QuickSearch_SetupShiftTable_m4619,
	QuickSearch_GetShiftDistance_m4620,
	QuickSearch_GetChar_m4621,
	ReplacementEvaluator__ctor_m4622,
	ReplacementEvaluator_Evaluate_m4623,
	ReplacementEvaluator_EvaluateAppend_m4624,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4625,
	ReplacementEvaluator_Ensure_m4626,
	ReplacementEvaluator_AddFromReplacement_m4627,
	ReplacementEvaluator_AddInt_m4628,
	ReplacementEvaluator_Compile_m4629,
	ReplacementEvaluator_CompileTerm_m4630,
	ExpressionCollection__ctor_m4631,
	ExpressionCollection_Add_m4632,
	ExpressionCollection_get_Item_m4633,
	ExpressionCollection_set_Item_m4634,
	ExpressionCollection_OnValidate_m4635,
	Expression__ctor_m4636,
	Expression_GetFixedWidth_m4637,
	Expression_GetAnchorInfo_m4638,
	CompositeExpression__ctor_m4639,
	CompositeExpression_get_Expressions_m4640,
	CompositeExpression_GetWidth_m4641,
	CompositeExpression_IsComplex_m4642,
	Group__ctor_m4643,
	Group_AppendExpression_m4644,
	Group_Compile_m4645,
	Group_GetWidth_m4646,
	Group_GetAnchorInfo_m4647,
	RegularExpression__ctor_m4648,
	RegularExpression_set_GroupCount_m4649,
	RegularExpression_Compile_m4650,
	CapturingGroup__ctor_m4651,
	CapturingGroup_get_Index_m4652,
	CapturingGroup_set_Index_m4653,
	CapturingGroup_get_Name_m4654,
	CapturingGroup_set_Name_m4655,
	CapturingGroup_get_IsNamed_m4656,
	CapturingGroup_Compile_m4657,
	CapturingGroup_IsComplex_m4658,
	CapturingGroup_CompareTo_m4659,
	BalancingGroup__ctor_m4660,
	BalancingGroup_set_Balance_m4661,
	BalancingGroup_Compile_m4662,
	NonBacktrackingGroup__ctor_m4663,
	NonBacktrackingGroup_Compile_m4664,
	NonBacktrackingGroup_IsComplex_m4665,
	Repetition__ctor_m4666,
	Repetition_get_Expression_m4667,
	Repetition_set_Expression_m4668,
	Repetition_get_Minimum_m4669,
	Repetition_Compile_m4670,
	Repetition_GetWidth_m4671,
	Repetition_GetAnchorInfo_m4672,
	Assertion__ctor_m4673,
	Assertion_get_TrueExpression_m4674,
	Assertion_set_TrueExpression_m4675,
	Assertion_get_FalseExpression_m4676,
	Assertion_set_FalseExpression_m4677,
	Assertion_GetWidth_m4678,
	CaptureAssertion__ctor_m4679,
	CaptureAssertion_set_CapturingGroup_m4680,
	CaptureAssertion_Compile_m4681,
	CaptureAssertion_IsComplex_m4682,
	CaptureAssertion_get_Alternate_m4683,
	ExpressionAssertion__ctor_m4684,
	ExpressionAssertion_set_Reverse_m4685,
	ExpressionAssertion_set_Negate_m4686,
	ExpressionAssertion_get_TestExpression_m4687,
	ExpressionAssertion_set_TestExpression_m4688,
	ExpressionAssertion_Compile_m4689,
	ExpressionAssertion_IsComplex_m4690,
	Alternation__ctor_m4691,
	Alternation_get_Alternatives_m4692,
	Alternation_AddAlternative_m4693,
	Alternation_Compile_m4694,
	Alternation_GetWidth_m4695,
	Literal__ctor_m4696,
	Literal_CompileLiteral_m4697,
	Literal_Compile_m4698,
	Literal_GetWidth_m4699,
	Literal_GetAnchorInfo_m4700,
	Literal_IsComplex_m4701,
	PositionAssertion__ctor_m4702,
	PositionAssertion_Compile_m4703,
	PositionAssertion_GetWidth_m4704,
	PositionAssertion_IsComplex_m4705,
	PositionAssertion_GetAnchorInfo_m4706,
	Reference__ctor_m4707,
	Reference_get_CapturingGroup_m4708,
	Reference_set_CapturingGroup_m4709,
	Reference_get_IgnoreCase_m4710,
	Reference_Compile_m4711,
	Reference_GetWidth_m4712,
	Reference_IsComplex_m4713,
	BackslashNumber__ctor_m4714,
	BackslashNumber_ResolveReference_m4715,
	BackslashNumber_Compile_m4716,
	CharacterClass__ctor_m4717,
	CharacterClass__ctor_m4718,
	CharacterClass__cctor_m4719,
	CharacterClass_AddCategory_m4720,
	CharacterClass_AddCharacter_m4721,
	CharacterClass_AddRange_m4722,
	CharacterClass_Compile_m4723,
	CharacterClass_GetWidth_m4724,
	CharacterClass_IsComplex_m4725,
	CharacterClass_GetIntervalCost_m4726,
	AnchorInfo__ctor_m4727,
	AnchorInfo__ctor_m4728,
	AnchorInfo__ctor_m4729,
	AnchorInfo_get_Offset_m4730,
	AnchorInfo_get_Width_m4731,
	AnchorInfo_get_Length_m4732,
	AnchorInfo_get_IsUnknownWidth_m4733,
	AnchorInfo_get_IsComplete_m4734,
	AnchorInfo_get_Substring_m4735,
	AnchorInfo_get_IgnoreCase_m4736,
	AnchorInfo_get_Position_m4737,
	AnchorInfo_get_IsSubstring_m4738,
	AnchorInfo_get_IsPosition_m4739,
	AnchorInfo_GetInterval_m4740,
	DefaultUriParser__ctor_m4741,
	DefaultUriParser__ctor_m4742,
	UriScheme__ctor_m4743,
	Uri__ctor_m4744,
	Uri__ctor_m4745,
	Uri__ctor_m4746,
	Uri__cctor_m4747,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4748,
	Uri_get_AbsoluteUri_m4749,
	Uri_get_Authority_m4750,
	Uri_get_Host_m4751,
	Uri_get_IsFile_m4752,
	Uri_get_IsLoopback_m4753,
	Uri_get_IsUnc_m4754,
	Uri_get_Scheme_m4755,
	Uri_get_IsAbsoluteUri_m4756,
	Uri_CheckHostName_m4757,
	Uri_IsIPv4Address_m4758,
	Uri_IsDomainAddress_m4759,
	Uri_CheckSchemeName_m4760,
	Uri_IsAlpha_m4761,
	Uri_Equals_m4762,
	Uri_InternalEquals_m4763,
	Uri_GetHashCode_m4764,
	Uri_GetLeftPart_m4765,
	Uri_FromHex_m4766,
	Uri_HexEscape_m4767,
	Uri_IsHexDigit_m4768,
	Uri_IsHexEncoding_m4769,
	Uri_AppendQueryAndFragment_m4770,
	Uri_ToString_m4771,
	Uri_EscapeString_m4772,
	Uri_EscapeString_m4773,
	Uri_ParseUri_m4774,
	Uri_Unescape_m4775,
	Uri_Unescape_m4776,
	Uri_ParseAsWindowsUNC_m4777,
	Uri_ParseAsWindowsAbsoluteFilePath_m4778,
	Uri_ParseAsUnixAbsoluteFilePath_m4779,
	Uri_Parse_m4780,
	Uri_ParseNoExceptions_m4781,
	Uri_CompactEscaped_m4782,
	Uri_Reduce_m4783,
	Uri_HexUnescapeMultiByte_m4784,
	Uri_GetSchemeDelimiter_m4785,
	Uri_GetDefaultPort_m4786,
	Uri_GetOpaqueWiseSchemeDelimiter_m4787,
	Uri_IsPredefinedScheme_m4788,
	Uri_get_Parser_m4789,
	Uri_EnsureAbsoluteUri_m4790,
	Uri_op_Equality_m4791,
	Uri_op_Inequality_m406,
	UriFormatException__ctor_m4792,
	UriFormatException__ctor_m4793,
	UriFormatException__ctor_m4794,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4795,
	UriParser__ctor_m4796,
	UriParser__cctor_m4797,
	UriParser_InitializeAndValidate_m4798,
	UriParser_OnRegister_m4799,
	UriParser_set_SchemeName_m4800,
	UriParser_get_DefaultPort_m4801,
	UriParser_set_DefaultPort_m4802,
	UriParser_CreateDefaults_m4803,
	UriParser_InternalRegister_m4804,
	UriParser_GetParser_m4805,
	RemoteCertificateValidationCallback__ctor_m4806,
	RemoteCertificateValidationCallback_Invoke_m4807,
	RemoteCertificateValidationCallback_BeginInvoke_m4808,
	RemoteCertificateValidationCallback_EndInvoke_m4809,
	MatchEvaluator__ctor_m4810,
	MatchEvaluator_Invoke_m4811,
	MatchEvaluator_BeginInvoke_m4812,
	MatchEvaluator_EndInvoke_m4813,
	Extensions_op_Implicit_m678,
	Locale_GetText_m4980,
	ModulusRing__ctor_m4981,
	ModulusRing_BarrettReduction_m4982,
	ModulusRing_Multiply_m4983,
	ModulusRing_Difference_m4984,
	ModulusRing_Pow_m4985,
	ModulusRing_Pow_m4986,
	Kernel_AddSameSign_m4987,
	Kernel_Subtract_m4988,
	Kernel_MinusEq_m4989,
	Kernel_PlusEq_m4990,
	Kernel_Compare_m4991,
	Kernel_SingleByteDivideInPlace_m4992,
	Kernel_DwordMod_m4993,
	Kernel_DwordDivMod_m4994,
	Kernel_multiByteDivide_m4995,
	Kernel_LeftShift_m4996,
	Kernel_RightShift_m4997,
	Kernel_Multiply_m4998,
	Kernel_MultiplyMod2p32pmod_m4999,
	Kernel_modInverse_m5000,
	Kernel_modInverse_m5001,
	BigInteger__ctor_m5002,
	BigInteger__ctor_m5003,
	BigInteger__ctor_m5004,
	BigInteger__ctor_m5005,
	BigInteger__ctor_m5006,
	BigInteger__cctor_m5007,
	BigInteger_get_Rng_m5008,
	BigInteger_GenerateRandom_m5009,
	BigInteger_GenerateRandom_m5010,
	BigInteger_BitCount_m5011,
	BigInteger_TestBit_m5012,
	BigInteger_SetBit_m5013,
	BigInteger_SetBit_m5014,
	BigInteger_LowestSetBit_m5015,
	BigInteger_GetBytes_m5016,
	BigInteger_ToString_m5017,
	BigInteger_ToString_m5018,
	BigInteger_Normalize_m5019,
	BigInteger_Clear_m5020,
	BigInteger_GetHashCode_m5021,
	BigInteger_ToString_m5022,
	BigInteger_Equals_m5023,
	BigInteger_ModInverse_m5024,
	BigInteger_ModPow_m5025,
	BigInteger_GeneratePseudoPrime_m5026,
	BigInteger_Incr2_m5027,
	BigInteger_op_Implicit_m5028,
	BigInteger_op_Implicit_m5029,
	BigInteger_op_Addition_m5030,
	BigInteger_op_Subtraction_m5031,
	BigInteger_op_Modulus_m5032,
	BigInteger_op_Modulus_m5033,
	BigInteger_op_Division_m5034,
	BigInteger_op_Multiply_m5035,
	BigInteger_op_LeftShift_m5036,
	BigInteger_op_RightShift_m5037,
	BigInteger_op_Equality_m5038,
	BigInteger_op_Inequality_m5039,
	BigInteger_op_Equality_m5040,
	BigInteger_op_Inequality_m5041,
	BigInteger_op_GreaterThan_m5042,
	BigInteger_op_LessThan_m5043,
	BigInteger_op_GreaterThanOrEqual_m5044,
	BigInteger_op_LessThanOrEqual_m5045,
	PrimalityTests_GetSPPRounds_m5046,
	PrimalityTests_RabinMillerTest_m5047,
	PrimeGeneratorBase__ctor_m5048,
	PrimeGeneratorBase_get_Confidence_m5049,
	PrimeGeneratorBase_get_PrimalityTest_m5050,
	PrimeGeneratorBase_get_TrialDivisionBounds_m5051,
	SequentialSearchPrimeGeneratorBase__ctor_m5052,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m5053,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m5054,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m5055,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m5056,
	ASN1__ctor_m4882,
	ASN1__ctor_m4883,
	ASN1__ctor_m4867,
	ASN1_get_Count_m4870,
	ASN1_get_Tag_m4868,
	ASN1_get_Length_m4895,
	ASN1_get_Value_m4869,
	ASN1_set_Value_m5057,
	ASN1_CompareArray_m5058,
	ASN1_CompareValue_m4894,
	ASN1_Add_m4884,
	ASN1_GetBytes_m5059,
	ASN1_Decode_m5060,
	ASN1_DecodeTLV_m5061,
	ASN1_get_Item_m4871,
	ASN1_Element_m5062,
	ASN1_ToString_m5063,
	ASN1Convert_FromInt32_m4885,
	ASN1Convert_FromOid_m5064,
	ASN1Convert_ToInt32_m4881,
	ASN1Convert_ToOid_m4934,
	ASN1Convert_ToDateTime_m5065,
	BitConverterLE_GetUIntBytes_m5066,
	BitConverterLE_GetBytes_m5067,
	ContentInfo__ctor_m5068,
	ContentInfo__ctor_m5069,
	ContentInfo__ctor_m5070,
	ContentInfo__ctor_m5071,
	ContentInfo_get_ASN1_m5072,
	ContentInfo_get_Content_m5073,
	ContentInfo_set_Content_m5074,
	ContentInfo_get_ContentType_m5075,
	ContentInfo_set_ContentType_m5076,
	ContentInfo_GetASN1_m5077,
	EncryptedData__ctor_m5078,
	EncryptedData__ctor_m5079,
	EncryptedData_get_EncryptionAlgorithm_m5080,
	EncryptedData_get_EncryptedContent_m5081,
	ARC4Managed__ctor_m5082,
	ARC4Managed_Finalize_m5083,
	ARC4Managed_Dispose_m5084,
	ARC4Managed_get_Key_m5085,
	ARC4Managed_set_Key_m5086,
	ARC4Managed_get_CanReuseTransform_m5087,
	ARC4Managed_CreateEncryptor_m5088,
	ARC4Managed_CreateDecryptor_m5089,
	ARC4Managed_GenerateIV_m5090,
	ARC4Managed_GenerateKey_m5091,
	ARC4Managed_KeySetup_m5092,
	ARC4Managed_CheckInput_m5093,
	ARC4Managed_TransformBlock_m5094,
	ARC4Managed_InternalTransformBlock_m5095,
	ARC4Managed_TransformFinalBlock_m5096,
	CryptoConvert_ToHex_m4948,
	KeyBuilder_get_Rng_m5097,
	KeyBuilder_Key_m5098,
	MD2__ctor_m5099,
	MD2_Create_m5100,
	MD2_Create_m5101,
	MD2Managed__ctor_m5102,
	MD2Managed__cctor_m5103,
	MD2Managed_Padding_m5104,
	MD2Managed_Initialize_m5105,
	MD2Managed_HashCore_m5106,
	MD2Managed_HashFinal_m5107,
	MD2Managed_MD2Transform_m5108,
	PKCS1__cctor_m5109,
	PKCS1_Compare_m5110,
	PKCS1_I2OSP_m5111,
	PKCS1_OS2IP_m5112,
	PKCS1_RSASP1_m5113,
	PKCS1_RSAVP1_m5114,
	PKCS1_Sign_v15_m5115,
	PKCS1_Verify_v15_m5116,
	PKCS1_Verify_v15_m5117,
	PKCS1_Encode_v15_m5118,
	PrivateKeyInfo__ctor_m5119,
	PrivateKeyInfo__ctor_m5120,
	PrivateKeyInfo_get_PrivateKey_m5121,
	PrivateKeyInfo_Decode_m5122,
	PrivateKeyInfo_RemoveLeadingZero_m5123,
	PrivateKeyInfo_Normalize_m5124,
	PrivateKeyInfo_DecodeRSA_m5125,
	PrivateKeyInfo_DecodeDSA_m5126,
	EncryptedPrivateKeyInfo__ctor_m5127,
	EncryptedPrivateKeyInfo__ctor_m5128,
	EncryptedPrivateKeyInfo_get_Algorithm_m5129,
	EncryptedPrivateKeyInfo_get_EncryptedData_m5130,
	EncryptedPrivateKeyInfo_get_Salt_m5131,
	EncryptedPrivateKeyInfo_get_IterationCount_m5132,
	EncryptedPrivateKeyInfo_Decode_m5133,
	RC4__ctor_m5134,
	RC4__cctor_m5135,
	RC4_get_IV_m5136,
	RC4_set_IV_m5137,
	KeyGeneratedEventHandler__ctor_m5138,
	KeyGeneratedEventHandler_Invoke_m5139,
	KeyGeneratedEventHandler_BeginInvoke_m5140,
	KeyGeneratedEventHandler_EndInvoke_m5141,
	RSAManaged__ctor_m5142,
	RSAManaged__ctor_m5143,
	RSAManaged_Finalize_m5144,
	RSAManaged_GenerateKeyPair_m5145,
	RSAManaged_get_KeySize_m5146,
	RSAManaged_get_PublicOnly_m4862,
	RSAManaged_DecryptValue_m5147,
	RSAManaged_EncryptValue_m5148,
	RSAManaged_ExportParameters_m5149,
	RSAManaged_ImportParameters_m5150,
	RSAManaged_Dispose_m5151,
	RSAManaged_ToXmlString_m5152,
	RSAManaged_GetPaddedValue_m5153,
	SafeBag__ctor_m5154,
	SafeBag_get_BagOID_m5155,
	SafeBag_get_ASN1_m5156,
	DeriveBytes__ctor_m5157,
	DeriveBytes__cctor_m5158,
	DeriveBytes_set_HashName_m5159,
	DeriveBytes_set_IterationCount_m5160,
	DeriveBytes_set_Password_m5161,
	DeriveBytes_set_Salt_m5162,
	DeriveBytes_Adjust_m5163,
	DeriveBytes_Derive_m5164,
	DeriveBytes_DeriveKey_m5165,
	DeriveBytes_DeriveIV_m5166,
	DeriveBytes_DeriveMAC_m5167,
	PKCS12__ctor_m5168,
	PKCS12__ctor_m4896,
	PKCS12__ctor_m4897,
	PKCS12__cctor_m5169,
	PKCS12_Decode_m5170,
	PKCS12_Finalize_m5171,
	PKCS12_set_Password_m5172,
	PKCS12_get_IterationCount_m5173,
	PKCS12_set_IterationCount_m5174,
	PKCS12_get_Keys_m4900,
	PKCS12_get_Certificates_m4898,
	PKCS12_get_RNG_m5175,
	PKCS12_Compare_m5176,
	PKCS12_GetSymmetricAlgorithm_m5177,
	PKCS12_Decrypt_m5178,
	PKCS12_Decrypt_m5179,
	PKCS12_Encrypt_m5180,
	PKCS12_GetExistingParameters_m5181,
	PKCS12_AddPrivateKey_m5182,
	PKCS12_ReadSafeBag_m5183,
	PKCS12_CertificateSafeBag_m5184,
	PKCS12_MAC_m5185,
	PKCS12_GetBytes_m5186,
	PKCS12_EncryptedContentInfo_m5187,
	PKCS12_AddCertificate_m5188,
	PKCS12_AddCertificate_m5189,
	PKCS12_RemoveCertificate_m5190,
	PKCS12_RemoveCertificate_m5191,
	PKCS12_Clone_m5192,
	PKCS12_get_MaximumPasswordLength_m5193,
	X501__cctor_m5194,
	X501_ToString_m5195,
	X501_ToString_m4875,
	X501_AppendEntry_m5196,
	X509Certificate__ctor_m4903,
	X509Certificate__cctor_m5197,
	X509Certificate_Parse_m5198,
	X509Certificate_GetUnsignedBigInteger_m5199,
	X509Certificate_get_DSA_m4864,
	X509Certificate_set_DSA_m4901,
	X509Certificate_get_Extensions_m4920,
	X509Certificate_get_Hash_m5200,
	X509Certificate_get_IssuerName_m5201,
	X509Certificate_get_KeyAlgorithm_m5202,
	X509Certificate_get_KeyAlgorithmParameters_m5203,
	X509Certificate_set_KeyAlgorithmParameters_m5204,
	X509Certificate_get_PublicKey_m5205,
	X509Certificate_get_RSA_m5206,
	X509Certificate_set_RSA_m5207,
	X509Certificate_get_RawData_m5208,
	X509Certificate_get_SerialNumber_m5209,
	X509Certificate_get_Signature_m5210,
	X509Certificate_get_SignatureAlgorithm_m5211,
	X509Certificate_get_SubjectName_m5212,
	X509Certificate_get_ValidFrom_m5213,
	X509Certificate_get_ValidUntil_m5214,
	X509Certificate_get_Version_m4893,
	X509Certificate_get_IsCurrent_m5215,
	X509Certificate_WasCurrent_m5216,
	X509Certificate_VerifySignature_m5217,
	X509Certificate_VerifySignature_m5218,
	X509Certificate_VerifySignature_m4919,
	X509Certificate_get_IsSelfSigned_m5219,
	X509Certificate_GetIssuerName_m4888,
	X509Certificate_GetSubjectName_m4891,
	X509Certificate_GetObjectData_m5220,
	X509Certificate_PEM_m5221,
	X509CertificateEnumerator__ctor_m5222,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m5223,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m5224,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m5225,
	X509CertificateEnumerator_get_Current_m4945,
	X509CertificateEnumerator_MoveNext_m5226,
	X509CertificateEnumerator_Reset_m5227,
	X509CertificateCollection__ctor_m5228,
	X509CertificateCollection__ctor_m5229,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m5230,
	X509CertificateCollection_get_Item_m4899,
	X509CertificateCollection_Add_m5231,
	X509CertificateCollection_AddRange_m5232,
	X509CertificateCollection_Contains_m5233,
	X509CertificateCollection_GetEnumerator_m4944,
	X509CertificateCollection_GetHashCode_m5234,
	X509CertificateCollection_IndexOf_m5235,
	X509CertificateCollection_Remove_m5236,
	X509CertificateCollection_Compare_m5237,
	X509Chain__ctor_m5238,
	X509Chain__ctor_m5239,
	X509Chain_get_Status_m5240,
	X509Chain_get_TrustAnchors_m5241,
	X509Chain_Build_m5242,
	X509Chain_IsValid_m5243,
	X509Chain_FindCertificateParent_m5244,
	X509Chain_FindCertificateRoot_m5245,
	X509Chain_IsTrusted_m5246,
	X509Chain_IsParent_m5247,
	X509CrlEntry__ctor_m5248,
	X509CrlEntry_get_SerialNumber_m5249,
	X509CrlEntry_get_RevocationDate_m4927,
	X509CrlEntry_get_Extensions_m4933,
	X509Crl__ctor_m5250,
	X509Crl_Parse_m5251,
	X509Crl_get_Extensions_m4922,
	X509Crl_get_Hash_m5252,
	X509Crl_get_IssuerName_m4930,
	X509Crl_get_NextUpdate_m4928,
	X509Crl_Compare_m5253,
	X509Crl_GetCrlEntry_m4926,
	X509Crl_GetCrlEntry_m5254,
	X509Crl_GetHashName_m5255,
	X509Crl_VerifySignature_m5256,
	X509Crl_VerifySignature_m5257,
	X509Crl_VerifySignature_m4925,
	X509Extension__ctor_m5258,
	X509Extension__ctor_m5259,
	X509Extension_Decode_m5260,
	X509Extension_Encode_m5261,
	X509Extension_get_Oid_m4932,
	X509Extension_get_Critical_m4931,
	X509Extension_get_Value_m4936,
	X509Extension_Equals_m5262,
	X509Extension_GetHashCode_m5263,
	X509Extension_WriteLine_m5264,
	X509Extension_ToString_m5265,
	X509ExtensionCollection__ctor_m5266,
	X509ExtensionCollection__ctor_m5267,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m5268,
	X509ExtensionCollection_IndexOf_m5269,
	X509ExtensionCollection_get_Item_m4921,
	X509Store__ctor_m5270,
	X509Store_get_Certificates_m4943,
	X509Store_get_Crls_m4929,
	X509Store_Load_m5271,
	X509Store_LoadCertificate_m5272,
	X509Store_LoadCrl_m5273,
	X509Store_CheckStore_m5274,
	X509Store_BuildCertificatesCollection_m5275,
	X509Store_BuildCrlsCollection_m5276,
	X509StoreManager_get_CurrentUser_m4940,
	X509StoreManager_get_LocalMachine_m4941,
	X509StoreManager_get_TrustedRootCertificates_m5277,
	X509Stores__ctor_m5278,
	X509Stores_get_TrustedRoot_m5279,
	X509Stores_Open_m4942,
	AuthorityKeyIdentifierExtension__ctor_m4923,
	AuthorityKeyIdentifierExtension_Decode_m5280,
	AuthorityKeyIdentifierExtension_get_Identifier_m4924,
	AuthorityKeyIdentifierExtension_ToString_m5281,
	BasicConstraintsExtension__ctor_m5282,
	BasicConstraintsExtension_Decode_m5283,
	BasicConstraintsExtension_Encode_m5284,
	BasicConstraintsExtension_get_CertificateAuthority_m5285,
	BasicConstraintsExtension_ToString_m5286,
	ExtendedKeyUsageExtension__ctor_m5287,
	ExtendedKeyUsageExtension_Decode_m5288,
	ExtendedKeyUsageExtension_Encode_m5289,
	ExtendedKeyUsageExtension_get_KeyPurpose_m5290,
	ExtendedKeyUsageExtension_ToString_m5291,
	GeneralNames__ctor_m5292,
	GeneralNames_get_DNSNames_m5293,
	GeneralNames_get_IPAddresses_m5294,
	GeneralNames_ToString_m5295,
	KeyUsageExtension__ctor_m5296,
	KeyUsageExtension_Decode_m5297,
	KeyUsageExtension_Encode_m5298,
	KeyUsageExtension_Support_m5299,
	KeyUsageExtension_ToString_m5300,
	NetscapeCertTypeExtension__ctor_m5301,
	NetscapeCertTypeExtension_Decode_m5302,
	NetscapeCertTypeExtension_Support_m5303,
	NetscapeCertTypeExtension_ToString_m5304,
	SubjectAltNameExtension__ctor_m5305,
	SubjectAltNameExtension_Decode_m5306,
	SubjectAltNameExtension_get_DNSNames_m5307,
	SubjectAltNameExtension_get_IPAddresses_m5308,
	SubjectAltNameExtension_ToString_m5309,
	HMAC__ctor_m5310,
	HMAC_get_Key_m5311,
	HMAC_set_Key_m5312,
	HMAC_Initialize_m5313,
	HMAC_HashFinal_m5314,
	HMAC_HashCore_m5315,
	HMAC_initializePad_m5316,
	MD5SHA1__ctor_m5317,
	MD5SHA1_Initialize_m5318,
	MD5SHA1_HashFinal_m5319,
	MD5SHA1_HashCore_m5320,
	MD5SHA1_CreateSignature_m5321,
	MD5SHA1_VerifySignature_m5322,
	Alert__ctor_m5323,
	Alert__ctor_m5324,
	Alert_get_Level_m5325,
	Alert_get_Description_m5326,
	Alert_get_IsWarning_m5327,
	Alert_get_IsCloseNotify_m5328,
	Alert_inferAlertLevel_m5329,
	Alert_GetAlertMessage_m5330,
	CipherSuite__ctor_m5331,
	CipherSuite__cctor_m5332,
	CipherSuite_get_EncryptionCipher_m5333,
	CipherSuite_get_DecryptionCipher_m5334,
	CipherSuite_get_ClientHMAC_m5335,
	CipherSuite_get_ServerHMAC_m5336,
	CipherSuite_get_CipherAlgorithmType_m5337,
	CipherSuite_get_HashAlgorithmName_m5338,
	CipherSuite_get_HashAlgorithmType_m5339,
	CipherSuite_get_HashSize_m5340,
	CipherSuite_get_ExchangeAlgorithmType_m5341,
	CipherSuite_get_CipherMode_m5342,
	CipherSuite_get_Code_m5343,
	CipherSuite_get_Name_m5344,
	CipherSuite_get_IsExportable_m5345,
	CipherSuite_get_KeyMaterialSize_m5346,
	CipherSuite_get_KeyBlockSize_m5347,
	CipherSuite_get_ExpandedKeyMaterialSize_m5348,
	CipherSuite_get_EffectiveKeyBits_m5349,
	CipherSuite_get_IvSize_m5350,
	CipherSuite_get_Context_m5351,
	CipherSuite_set_Context_m5352,
	CipherSuite_Write_m5353,
	CipherSuite_Write_m5354,
	CipherSuite_InitializeCipher_m5355,
	CipherSuite_EncryptRecord_m5356,
	CipherSuite_DecryptRecord_m5357,
	CipherSuite_CreatePremasterSecret_m5358,
	CipherSuite_PRF_m5359,
	CipherSuite_Expand_m5360,
	CipherSuite_createEncryptionCipher_m5361,
	CipherSuite_createDecryptionCipher_m5362,
	CipherSuiteCollection__ctor_m5363,
	CipherSuiteCollection_System_Collections_IList_get_Item_m5364,
	CipherSuiteCollection_System_Collections_IList_set_Item_m5365,
	CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m5366,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m5367,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m5368,
	CipherSuiteCollection_System_Collections_IList_Contains_m5369,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m5370,
	CipherSuiteCollection_System_Collections_IList_Insert_m5371,
	CipherSuiteCollection_System_Collections_IList_Remove_m5372,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m5373,
	CipherSuiteCollection_System_Collections_IList_Add_m5374,
	CipherSuiteCollection_get_Item_m5375,
	CipherSuiteCollection_get_Item_m5376,
	CipherSuiteCollection_set_Item_m5377,
	CipherSuiteCollection_get_Item_m5378,
	CipherSuiteCollection_get_Count_m5379,
	CipherSuiteCollection_get_IsFixedSize_m5380,
	CipherSuiteCollection_get_IsReadOnly_m5381,
	CipherSuiteCollection_CopyTo_m5382,
	CipherSuiteCollection_Clear_m5383,
	CipherSuiteCollection_IndexOf_m5384,
	CipherSuiteCollection_IndexOf_m5385,
	CipherSuiteCollection_Add_m5386,
	CipherSuiteCollection_add_m5387,
	CipherSuiteCollection_add_m5388,
	CipherSuiteCollection_cultureAwareCompare_m5389,
	CipherSuiteFactory_GetSupportedCiphers_m5390,
	CipherSuiteFactory_GetTls1SupportedCiphers_m5391,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m5392,
	ClientContext__ctor_m5393,
	ClientContext_get_SslStream_m5394,
	ClientContext_get_ClientHelloProtocol_m5395,
	ClientContext_set_ClientHelloProtocol_m5396,
	ClientContext_Clear_m5397,
	ClientRecordProtocol__ctor_m5398,
	ClientRecordProtocol_GetMessage_m5399,
	ClientRecordProtocol_ProcessHandshakeMessage_m5400,
	ClientRecordProtocol_createClientHandshakeMessage_m5401,
	ClientRecordProtocol_createServerHandshakeMessage_m5402,
	ClientSessionInfo__ctor_m5403,
	ClientSessionInfo__cctor_m5404,
	ClientSessionInfo_Finalize_m5405,
	ClientSessionInfo_get_HostName_m5406,
	ClientSessionInfo_get_Id_m5407,
	ClientSessionInfo_get_Valid_m5408,
	ClientSessionInfo_GetContext_m5409,
	ClientSessionInfo_SetContext_m5410,
	ClientSessionInfo_KeepAlive_m5411,
	ClientSessionInfo_Dispose_m5412,
	ClientSessionInfo_Dispose_m5413,
	ClientSessionInfo_CheckDisposed_m5414,
	ClientSessionCache__cctor_m5415,
	ClientSessionCache_Add_m5416,
	ClientSessionCache_FromHost_m5417,
	ClientSessionCache_FromContext_m5418,
	ClientSessionCache_SetContextInCache_m5419,
	ClientSessionCache_SetContextFromCache_m5420,
	Context__ctor_m5421,
	Context_get_AbbreviatedHandshake_m5422,
	Context_set_AbbreviatedHandshake_m5423,
	Context_get_ProtocolNegotiated_m5424,
	Context_set_ProtocolNegotiated_m5425,
	Context_get_SecurityProtocol_m5426,
	Context_set_SecurityProtocol_m5427,
	Context_get_SecurityProtocolFlags_m5428,
	Context_get_Protocol_m5429,
	Context_get_SessionId_m5430,
	Context_set_SessionId_m5431,
	Context_get_CompressionMethod_m5432,
	Context_set_CompressionMethod_m5433,
	Context_get_ServerSettings_m5434,
	Context_get_ClientSettings_m5435,
	Context_get_LastHandshakeMsg_m5436,
	Context_set_LastHandshakeMsg_m5437,
	Context_get_HandshakeState_m5438,
	Context_set_HandshakeState_m5439,
	Context_get_ReceivedConnectionEnd_m5440,
	Context_set_ReceivedConnectionEnd_m5441,
	Context_get_SentConnectionEnd_m5442,
	Context_set_SentConnectionEnd_m5443,
	Context_get_SupportedCiphers_m5444,
	Context_set_SupportedCiphers_m5445,
	Context_get_HandshakeMessages_m5446,
	Context_get_WriteSequenceNumber_m5447,
	Context_set_WriteSequenceNumber_m5448,
	Context_get_ReadSequenceNumber_m5449,
	Context_set_ReadSequenceNumber_m5450,
	Context_get_ClientRandom_m5451,
	Context_set_ClientRandom_m5452,
	Context_get_ServerRandom_m5453,
	Context_set_ServerRandom_m5454,
	Context_get_RandomCS_m5455,
	Context_set_RandomCS_m5456,
	Context_get_RandomSC_m5457,
	Context_set_RandomSC_m5458,
	Context_get_MasterSecret_m5459,
	Context_set_MasterSecret_m5460,
	Context_get_ClientWriteKey_m5461,
	Context_set_ClientWriteKey_m5462,
	Context_get_ServerWriteKey_m5463,
	Context_set_ServerWriteKey_m5464,
	Context_get_ClientWriteIV_m5465,
	Context_set_ClientWriteIV_m5466,
	Context_get_ServerWriteIV_m5467,
	Context_set_ServerWriteIV_m5468,
	Context_get_RecordProtocol_m5469,
	Context_set_RecordProtocol_m5470,
	Context_GetUnixTime_m5471,
	Context_GetSecureRandomBytes_m5472,
	Context_Clear_m5473,
	Context_ClearKeyInfo_m5474,
	Context_DecodeProtocolCode_m5475,
	Context_ChangeProtocol_m5476,
	Context_get_Current_m5477,
	Context_get_Negotiating_m5478,
	Context_get_Read_m5479,
	Context_get_Write_m5480,
	Context_StartSwitchingSecurityParameters_m5481,
	Context_EndSwitchingSecurityParameters_m5482,
	HttpsClientStream__ctor_m5483,
	HttpsClientStream_get_TrustFailure_m5484,
	HttpsClientStream_RaiseServerCertificateValidation_m5485,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m5486,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m5487,
	ReceiveRecordAsyncResult__ctor_m5488,
	ReceiveRecordAsyncResult_get_Record_m5489,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m5490,
	ReceiveRecordAsyncResult_get_InitialBuffer_m5491,
	ReceiveRecordAsyncResult_get_AsyncState_m5492,
	ReceiveRecordAsyncResult_get_AsyncException_m5493,
	ReceiveRecordAsyncResult_get_CompletedWithError_m5494,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m5495,
	ReceiveRecordAsyncResult_get_IsCompleted_m5496,
	ReceiveRecordAsyncResult_SetComplete_m5497,
	ReceiveRecordAsyncResult_SetComplete_m5498,
	ReceiveRecordAsyncResult_SetComplete_m5499,
	SendRecordAsyncResult__ctor_m5500,
	SendRecordAsyncResult_get_Message_m5501,
	SendRecordAsyncResult_get_AsyncState_m5502,
	SendRecordAsyncResult_get_AsyncException_m5503,
	SendRecordAsyncResult_get_CompletedWithError_m5504,
	SendRecordAsyncResult_get_AsyncWaitHandle_m5505,
	SendRecordAsyncResult_get_IsCompleted_m5506,
	SendRecordAsyncResult_SetComplete_m5507,
	SendRecordAsyncResult_SetComplete_m5508,
	RecordProtocol__ctor_m5509,
	RecordProtocol__cctor_m5510,
	RecordProtocol_get_Context_m5511,
	RecordProtocol_SendRecord_m5512,
	RecordProtocol_ProcessChangeCipherSpec_m5513,
	RecordProtocol_GetMessage_m5514,
	RecordProtocol_BeginReceiveRecord_m5515,
	RecordProtocol_InternalReceiveRecordCallback_m5516,
	RecordProtocol_EndReceiveRecord_m5517,
	RecordProtocol_ReceiveRecord_m5518,
	RecordProtocol_ReadRecordBuffer_m5519,
	RecordProtocol_ReadClientHelloV2_m5520,
	RecordProtocol_ReadStandardRecordBuffer_m5521,
	RecordProtocol_ProcessAlert_m5522,
	RecordProtocol_SendAlert_m5523,
	RecordProtocol_SendAlert_m5524,
	RecordProtocol_SendAlert_m5525,
	RecordProtocol_SendChangeCipherSpec_m5526,
	RecordProtocol_BeginSendRecord_m5527,
	RecordProtocol_InternalSendRecordCallback_m5528,
	RecordProtocol_BeginSendRecord_m5529,
	RecordProtocol_EndSendRecord_m5530,
	RecordProtocol_SendRecord_m5531,
	RecordProtocol_EncodeRecord_m5532,
	RecordProtocol_EncodeRecord_m5533,
	RecordProtocol_encryptRecordFragment_m5534,
	RecordProtocol_decryptRecordFragment_m5535,
	RecordProtocol_Compare_m5536,
	RecordProtocol_ProcessCipherSpecV2Buffer_m5537,
	RecordProtocol_MapV2CipherCode_m5538,
	RSASslSignatureDeformatter__ctor_m5539,
	RSASslSignatureDeformatter_VerifySignature_m5540,
	RSASslSignatureDeformatter_SetHashAlgorithm_m5541,
	RSASslSignatureDeformatter_SetKey_m5542,
	RSASslSignatureFormatter__ctor_m5543,
	RSASslSignatureFormatter_CreateSignature_m5544,
	RSASslSignatureFormatter_SetHashAlgorithm_m5545,
	RSASslSignatureFormatter_SetKey_m5546,
	SecurityParameters__ctor_m5547,
	SecurityParameters_get_Cipher_m5548,
	SecurityParameters_set_Cipher_m5549,
	SecurityParameters_get_ClientWriteMAC_m5550,
	SecurityParameters_set_ClientWriteMAC_m5551,
	SecurityParameters_get_ServerWriteMAC_m5552,
	SecurityParameters_set_ServerWriteMAC_m5553,
	SecurityParameters_Clear_m5554,
	ValidationResult_get_Trusted_m5555,
	ValidationResult_get_ErrorCode_m5556,
	SslClientStream__ctor_m5557,
	SslClientStream__ctor_m5558,
	SslClientStream__ctor_m5559,
	SslClientStream__ctor_m5560,
	SslClientStream__ctor_m5561,
	SslClientStream_add_ServerCertValidation_m5562,
	SslClientStream_remove_ServerCertValidation_m5563,
	SslClientStream_add_ClientCertSelection_m5564,
	SslClientStream_remove_ClientCertSelection_m5565,
	SslClientStream_add_PrivateKeySelection_m5566,
	SslClientStream_remove_PrivateKeySelection_m5567,
	SslClientStream_add_ServerCertValidation2_m5568,
	SslClientStream_remove_ServerCertValidation2_m5569,
	SslClientStream_get_InputBuffer_m5570,
	SslClientStream_get_ClientCertificates_m5571,
	SslClientStream_get_SelectedClientCertificate_m5572,
	SslClientStream_get_ServerCertValidationDelegate_m5573,
	SslClientStream_set_ServerCertValidationDelegate_m5574,
	SslClientStream_get_ClientCertSelectionDelegate_m5575,
	SslClientStream_set_ClientCertSelectionDelegate_m5576,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m5577,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m5578,
	SslClientStream_Finalize_m5579,
	SslClientStream_Dispose_m5580,
	SslClientStream_OnBeginNegotiateHandshake_m5581,
	SslClientStream_SafeReceiveRecord_m5582,
	SslClientStream_OnNegotiateHandshakeCallback_m5583,
	SslClientStream_OnLocalCertificateSelection_m5584,
	SslClientStream_get_HaveRemoteValidation2Callback_m5585,
	SslClientStream_OnRemoteCertificateValidation2_m5586,
	SslClientStream_OnRemoteCertificateValidation_m5587,
	SslClientStream_RaiseServerCertificateValidation_m5588,
	SslClientStream_RaiseServerCertificateValidation2_m5589,
	SslClientStream_RaiseClientCertificateSelection_m5590,
	SslClientStream_OnLocalPrivateKeySelection_m5591,
	SslClientStream_RaisePrivateKeySelection_m5592,
	SslCipherSuite__ctor_m5593,
	SslCipherSuite_ComputeServerRecordMAC_m5594,
	SslCipherSuite_ComputeClientRecordMAC_m5595,
	SslCipherSuite_ComputeMasterSecret_m5596,
	SslCipherSuite_ComputeKeys_m5597,
	SslCipherSuite_prf_m5598,
	SslHandshakeHash__ctor_m5599,
	SslHandshakeHash_Initialize_m5600,
	SslHandshakeHash_HashFinal_m5601,
	SslHandshakeHash_HashCore_m5602,
	SslHandshakeHash_CreateSignature_m5603,
	SslHandshakeHash_initializePad_m5604,
	InternalAsyncResult__ctor_m5605,
	InternalAsyncResult_get_ProceedAfterHandshake_m5606,
	InternalAsyncResult_get_FromWrite_m5607,
	InternalAsyncResult_get_Buffer_m5608,
	InternalAsyncResult_get_Offset_m5609,
	InternalAsyncResult_get_Count_m5610,
	InternalAsyncResult_get_BytesRead_m5611,
	InternalAsyncResult_get_AsyncState_m5612,
	InternalAsyncResult_get_AsyncException_m5613,
	InternalAsyncResult_get_CompletedWithError_m5614,
	InternalAsyncResult_get_AsyncWaitHandle_m5615,
	InternalAsyncResult_get_IsCompleted_m5616,
	InternalAsyncResult_SetComplete_m5617,
	InternalAsyncResult_SetComplete_m5618,
	InternalAsyncResult_SetComplete_m5619,
	InternalAsyncResult_SetComplete_m5620,
	SslStreamBase__ctor_m5621,
	SslStreamBase__cctor_m5622,
	SslStreamBase_AsyncHandshakeCallback_m5623,
	SslStreamBase_get_MightNeedHandshake_m5624,
	SslStreamBase_NegotiateHandshake_m5625,
	SslStreamBase_RaiseLocalCertificateSelection_m5626,
	SslStreamBase_RaiseRemoteCertificateValidation_m5627,
	SslStreamBase_RaiseRemoteCertificateValidation2_m5628,
	SslStreamBase_RaiseLocalPrivateKeySelection_m5629,
	SslStreamBase_get_CheckCertRevocationStatus_m5630,
	SslStreamBase_set_CheckCertRevocationStatus_m5631,
	SslStreamBase_get_CipherAlgorithm_m5632,
	SslStreamBase_get_CipherStrength_m5633,
	SslStreamBase_get_HashAlgorithm_m5634,
	SslStreamBase_get_HashStrength_m5635,
	SslStreamBase_get_KeyExchangeStrength_m5636,
	SslStreamBase_get_KeyExchangeAlgorithm_m5637,
	SslStreamBase_get_SecurityProtocol_m5638,
	SslStreamBase_get_ServerCertificate_m5639,
	SslStreamBase_get_ServerCertificates_m5640,
	SslStreamBase_BeginNegotiateHandshake_m5641,
	SslStreamBase_EndNegotiateHandshake_m5642,
	SslStreamBase_BeginRead_m5643,
	SslStreamBase_InternalBeginRead_m5644,
	SslStreamBase_InternalReadCallback_m5645,
	SslStreamBase_InternalBeginWrite_m5646,
	SslStreamBase_InternalWriteCallback_m5647,
	SslStreamBase_BeginWrite_m5648,
	SslStreamBase_EndRead_m5649,
	SslStreamBase_EndWrite_m5650,
	SslStreamBase_Close_m5651,
	SslStreamBase_Flush_m5652,
	SslStreamBase_Read_m5653,
	SslStreamBase_Read_m5654,
	SslStreamBase_Seek_m5655,
	SslStreamBase_SetLength_m5656,
	SslStreamBase_Write_m5657,
	SslStreamBase_Write_m5658,
	SslStreamBase_get_CanRead_m5659,
	SslStreamBase_get_CanSeek_m5660,
	SslStreamBase_get_CanWrite_m5661,
	SslStreamBase_get_Length_m5662,
	SslStreamBase_get_Position_m5663,
	SslStreamBase_set_Position_m5664,
	SslStreamBase_Finalize_m5665,
	SslStreamBase_Dispose_m5666,
	SslStreamBase_resetBuffer_m5667,
	SslStreamBase_checkDisposed_m5668,
	TlsCipherSuite__ctor_m5669,
	TlsCipherSuite_ComputeServerRecordMAC_m5670,
	TlsCipherSuite_ComputeClientRecordMAC_m5671,
	TlsCipherSuite_ComputeMasterSecret_m5672,
	TlsCipherSuite_ComputeKeys_m5673,
	TlsClientSettings__ctor_m5674,
	TlsClientSettings_get_TargetHost_m5675,
	TlsClientSettings_set_TargetHost_m5676,
	TlsClientSettings_get_Certificates_m5677,
	TlsClientSettings_set_Certificates_m5678,
	TlsClientSettings_get_ClientCertificate_m5679,
	TlsClientSettings_set_ClientCertificate_m5680,
	TlsClientSettings_UpdateCertificateRSA_m5681,
	TlsException__ctor_m5682,
	TlsException__ctor_m5683,
	TlsException__ctor_m5684,
	TlsException__ctor_m5685,
	TlsException__ctor_m5686,
	TlsException__ctor_m5687,
	TlsException_get_Alert_m5688,
	TlsServerSettings__ctor_m5689,
	TlsServerSettings_get_ServerKeyExchange_m5690,
	TlsServerSettings_set_ServerKeyExchange_m5691,
	TlsServerSettings_get_Certificates_m5692,
	TlsServerSettings_set_Certificates_m5693,
	TlsServerSettings_get_CertificateRSA_m5694,
	TlsServerSettings_get_RsaParameters_m5695,
	TlsServerSettings_set_RsaParameters_m5696,
	TlsServerSettings_set_SignedParams_m5697,
	TlsServerSettings_get_CertificateRequest_m5698,
	TlsServerSettings_set_CertificateRequest_m5699,
	TlsServerSettings_set_CertificateTypes_m5700,
	TlsServerSettings_set_DistinguisedNames_m5701,
	TlsServerSettings_UpdateCertificateRSA_m5702,
	TlsStream__ctor_m5703,
	TlsStream__ctor_m5704,
	TlsStream_get_EOF_m5705,
	TlsStream_get_CanWrite_m5706,
	TlsStream_get_CanRead_m5707,
	TlsStream_get_CanSeek_m5708,
	TlsStream_get_Position_m5709,
	TlsStream_set_Position_m5710,
	TlsStream_get_Length_m5711,
	TlsStream_ReadSmallValue_m5712,
	TlsStream_ReadByte_m5713,
	TlsStream_ReadInt16_m5714,
	TlsStream_ReadInt24_m5715,
	TlsStream_ReadBytes_m5716,
	TlsStream_Write_m5717,
	TlsStream_Write_m5718,
	TlsStream_WriteInt24_m5719,
	TlsStream_Write_m5720,
	TlsStream_Write_m5721,
	TlsStream_Reset_m5722,
	TlsStream_ToArray_m5723,
	TlsStream_Flush_m5724,
	TlsStream_SetLength_m5725,
	TlsStream_Seek_m5726,
	TlsStream_Read_m5727,
	TlsStream_Write_m5728,
	HandshakeMessage__ctor_m5729,
	HandshakeMessage__ctor_m5730,
	HandshakeMessage__ctor_m5731,
	HandshakeMessage_get_Context_m5732,
	HandshakeMessage_get_HandshakeType_m5733,
	HandshakeMessage_get_ContentType_m5734,
	HandshakeMessage_Process_m5735,
	HandshakeMessage_Update_m5736,
	HandshakeMessage_EncodeMessage_m5737,
	HandshakeMessage_Compare_m5738,
	TlsClientCertificate__ctor_m5739,
	TlsClientCertificate_get_ClientCertificate_m5740,
	TlsClientCertificate_Update_m5741,
	TlsClientCertificate_GetClientCertificate_m5742,
	TlsClientCertificate_SendCertificates_m5743,
	TlsClientCertificate_ProcessAsSsl3_m5744,
	TlsClientCertificate_ProcessAsTls1_m5745,
	TlsClientCertificate_FindParentCertificate_m5746,
	TlsClientCertificateVerify__ctor_m5747,
	TlsClientCertificateVerify_Update_m5748,
	TlsClientCertificateVerify_ProcessAsSsl3_m5749,
	TlsClientCertificateVerify_ProcessAsTls1_m5750,
	TlsClientCertificateVerify_getClientCertRSA_m5751,
	TlsClientCertificateVerify_getUnsignedBigInteger_m5752,
	TlsClientFinished__ctor_m5753,
	TlsClientFinished__cctor_m5754,
	TlsClientFinished_Update_m5755,
	TlsClientFinished_ProcessAsSsl3_m5756,
	TlsClientFinished_ProcessAsTls1_m5757,
	TlsClientHello__ctor_m5758,
	TlsClientHello_Update_m5759,
	TlsClientHello_ProcessAsSsl3_m5760,
	TlsClientHello_ProcessAsTls1_m5761,
	TlsClientKeyExchange__ctor_m5762,
	TlsClientKeyExchange_ProcessAsSsl3_m5763,
	TlsClientKeyExchange_ProcessAsTls1_m5764,
	TlsClientKeyExchange_ProcessCommon_m5765,
	TlsServerCertificate__ctor_m5766,
	TlsServerCertificate_Update_m5767,
	TlsServerCertificate_ProcessAsSsl3_m5768,
	TlsServerCertificate_ProcessAsTls1_m5769,
	TlsServerCertificate_checkCertificateUsage_m5770,
	TlsServerCertificate_validateCertificates_m5771,
	TlsServerCertificate_checkServerIdentity_m5772,
	TlsServerCertificate_checkDomainName_m5773,
	TlsServerCertificate_Match_m5774,
	TlsServerCertificateRequest__ctor_m5775,
	TlsServerCertificateRequest_Update_m5776,
	TlsServerCertificateRequest_ProcessAsSsl3_m5777,
	TlsServerCertificateRequest_ProcessAsTls1_m5778,
	TlsServerFinished__ctor_m5779,
	TlsServerFinished__cctor_m5780,
	TlsServerFinished_Update_m5781,
	TlsServerFinished_ProcessAsSsl3_m5782,
	TlsServerFinished_ProcessAsTls1_m5783,
	TlsServerHello__ctor_m5784,
	TlsServerHello_Update_m5785,
	TlsServerHello_ProcessAsSsl3_m5786,
	TlsServerHello_ProcessAsTls1_m5787,
	TlsServerHello_processProtocol_m5788,
	TlsServerHelloDone__ctor_m5789,
	TlsServerHelloDone_ProcessAsSsl3_m5790,
	TlsServerHelloDone_ProcessAsTls1_m5791,
	TlsServerKeyExchange__ctor_m5792,
	TlsServerKeyExchange_Update_m5793,
	TlsServerKeyExchange_ProcessAsSsl3_m5794,
	TlsServerKeyExchange_ProcessAsTls1_m5795,
	TlsServerKeyExchange_verifySignature_m5796,
	PrimalityTest__ctor_m5797,
	PrimalityTest_Invoke_m5798,
	PrimalityTest_BeginInvoke_m5799,
	PrimalityTest_EndInvoke_m5800,
	CertificateValidationCallback__ctor_m5801,
	CertificateValidationCallback_Invoke_m5802,
	CertificateValidationCallback_BeginInvoke_m5803,
	CertificateValidationCallback_EndInvoke_m5804,
	CertificateValidationCallback2__ctor_m5805,
	CertificateValidationCallback2_Invoke_m5806,
	CertificateValidationCallback2_BeginInvoke_m5807,
	CertificateValidationCallback2_EndInvoke_m5808,
	CertificateSelectionCallback__ctor_m5809,
	CertificateSelectionCallback_Invoke_m5810,
	CertificateSelectionCallback_BeginInvoke_m5811,
	CertificateSelectionCallback_EndInvoke_m5812,
	PrivateKeySelectionCallback__ctor_m5813,
	PrivateKeySelectionCallback_Invoke_m5814,
	PrivateKeySelectionCallback_BeginInvoke_m5815,
	PrivateKeySelectionCallback_EndInvoke_m5816,
	Object__ctor_m390,
	Object_Equals_m5894,
	Object_Equals_m4977,
	Object_Finalize_m2138,
	Object_GetHashCode_m5895,
	Object_GetType_m2181,
	Object_MemberwiseClone_m5896,
	Object_ToString_m2242,
	Object_ReferenceEquals_m2180,
	Object_InternalGetHashCode_m5897,
	ValueType__ctor_m5898,
	ValueType_InternalEquals_m5899,
	ValueType_DefaultEquals_m5900,
	ValueType_Equals_m5901,
	ValueType_InternalGetHashCode_m5902,
	ValueType_GetHashCode_m5903,
	ValueType_ToString_m5904,
	Attribute__ctor_m2163,
	Attribute_CheckParameters_m5905,
	Attribute_GetCustomAttribute_m5906,
	Attribute_GetCustomAttribute_m5907,
	Attribute_GetHashCode_m2243,
	Attribute_IsDefined_m5908,
	Attribute_IsDefined_m5909,
	Attribute_IsDefined_m5910,
	Attribute_IsDefined_m5911,
	Attribute_Equals_m5912,
	Int32_System_IConvertible_ToBoolean_m5913,
	Int32_System_IConvertible_ToByte_m5914,
	Int32_System_IConvertible_ToChar_m5915,
	Int32_System_IConvertible_ToDateTime_m5916,
	Int32_System_IConvertible_ToDecimal_m5917,
	Int32_System_IConvertible_ToDouble_m5918,
	Int32_System_IConvertible_ToInt16_m5919,
	Int32_System_IConvertible_ToInt32_m5920,
	Int32_System_IConvertible_ToInt64_m5921,
	Int32_System_IConvertible_ToSByte_m5922,
	Int32_System_IConvertible_ToSingle_m5923,
	Int32_System_IConvertible_ToType_m5924,
	Int32_System_IConvertible_ToUInt16_m5925,
	Int32_System_IConvertible_ToUInt32_m5926,
	Int32_System_IConvertible_ToUInt64_m5927,
	Int32_CompareTo_m5928,
	Int32_Equals_m5929,
	Int32_GetHashCode_m2152,
	Int32_CompareTo_m3543,
	Int32_Equals_m2154,
	Int32_ProcessTrailingWhitespace_m5930,
	Int32_Parse_m5931,
	Int32_Parse_m5932,
	Int32_CheckStyle_m5933,
	Int32_JumpOverWhite_m5934,
	Int32_FindSign_m5935,
	Int32_FindCurrency_m5936,
	Int32_FindExponent_m5937,
	Int32_FindOther_m5938,
	Int32_ValidDigit_m5939,
	Int32_GetFormatException_m5940,
	Int32_Parse_m5941,
	Int32_Parse_m4955,
	Int32_Parse_m5942,
	Int32_TryParse_m5943,
	Int32_TryParse_m4847,
	Int32_ToString_m2219,
	Int32_ToString_m5859,
	Int32_ToString_m4950,
	Int32_ToString_m5862,
	Int32_GetTypeCode_m5944,
	SerializableAttribute__ctor_m5945,
	AttributeUsageAttribute__ctor_m5946,
	AttributeUsageAttribute_get_AllowMultiple_m5947,
	AttributeUsageAttribute_set_AllowMultiple_m5948,
	AttributeUsageAttribute_get_Inherited_m5949,
	AttributeUsageAttribute_set_Inherited_m5950,
	ComVisibleAttribute__ctor_m5951,
	Int64_System_IConvertible_ToBoolean_m5952,
	Int64_System_IConvertible_ToByte_m5953,
	Int64_System_IConvertible_ToChar_m5954,
	Int64_System_IConvertible_ToDateTime_m5955,
	Int64_System_IConvertible_ToDecimal_m5956,
	Int64_System_IConvertible_ToDouble_m5957,
	Int64_System_IConvertible_ToInt16_m5958,
	Int64_System_IConvertible_ToInt32_m5959,
	Int64_System_IConvertible_ToInt64_m5960,
	Int64_System_IConvertible_ToSByte_m5961,
	Int64_System_IConvertible_ToSingle_m5962,
	Int64_System_IConvertible_ToType_m5963,
	Int64_System_IConvertible_ToUInt16_m5964,
	Int64_System_IConvertible_ToUInt32_m5965,
	Int64_System_IConvertible_ToUInt64_m5966,
	Int64_CompareTo_m5967,
	Int64_Equals_m5968,
	Int64_GetHashCode_m5969,
	Int64_CompareTo_m5970,
	Int64_Equals_m5971,
	Int64_Parse_m5972,
	Int64_Parse_m5973,
	Int64_Parse_m5974,
	Int64_Parse_m5975,
	Int64_Parse_m5976,
	Int64_TryParse_m5977,
	Int64_TryParse_m4843,
	Int64_ToString_m4844,
	Int64_ToString_m5978,
	Int64_ToString_m5979,
	Int64_ToString_m5980,
	UInt32_System_IConvertible_ToBoolean_m5981,
	UInt32_System_IConvertible_ToByte_m5982,
	UInt32_System_IConvertible_ToChar_m5983,
	UInt32_System_IConvertible_ToDateTime_m5984,
	UInt32_System_IConvertible_ToDecimal_m5985,
	UInt32_System_IConvertible_ToDouble_m5986,
	UInt32_System_IConvertible_ToInt16_m5987,
	UInt32_System_IConvertible_ToInt32_m5988,
	UInt32_System_IConvertible_ToInt64_m5989,
	UInt32_System_IConvertible_ToSByte_m5990,
	UInt32_System_IConvertible_ToSingle_m5991,
	UInt32_System_IConvertible_ToType_m5992,
	UInt32_System_IConvertible_ToUInt16_m5993,
	UInt32_System_IConvertible_ToUInt32_m5994,
	UInt32_System_IConvertible_ToUInt64_m5995,
	UInt32_CompareTo_m5996,
	UInt32_Equals_m5997,
	UInt32_GetHashCode_m5998,
	UInt32_CompareTo_m5999,
	UInt32_Equals_m6000,
	UInt32_Parse_m6001,
	UInt32_Parse_m6002,
	UInt32_Parse_m6003,
	UInt32_Parse_m6004,
	UInt32_TryParse_m4970,
	UInt32_TryParse_m6005,
	UInt32_ToString_m6006,
	UInt32_ToString_m6007,
	UInt32_ToString_m6008,
	UInt32_ToString_m6009,
	CLSCompliantAttribute__ctor_m6010,
	UInt64_System_IConvertible_ToBoolean_m6011,
	UInt64_System_IConvertible_ToByte_m6012,
	UInt64_System_IConvertible_ToChar_m6013,
	UInt64_System_IConvertible_ToDateTime_m6014,
	UInt64_System_IConvertible_ToDecimal_m6015,
	UInt64_System_IConvertible_ToDouble_m6016,
	UInt64_System_IConvertible_ToInt16_m6017,
	UInt64_System_IConvertible_ToInt32_m6018,
	UInt64_System_IConvertible_ToInt64_m6019,
	UInt64_System_IConvertible_ToSByte_m6020,
	UInt64_System_IConvertible_ToSingle_m6021,
	UInt64_System_IConvertible_ToType_m6022,
	UInt64_System_IConvertible_ToUInt16_m6023,
	UInt64_System_IConvertible_ToUInt32_m6024,
	UInt64_System_IConvertible_ToUInt64_m6025,
	UInt64_CompareTo_m6026,
	UInt64_Equals_m6027,
	UInt64_GetHashCode_m6028,
	UInt64_CompareTo_m6029,
	UInt64_Equals_m6030,
	UInt64_Parse_m6031,
	UInt64_Parse_m6032,
	UInt64_Parse_m6033,
	UInt64_TryParse_m6034,
	UInt64_ToString_m6035,
	UInt64_ToString_m5822,
	UInt64_ToString_m6036,
	UInt64_ToString_m6037,
	Byte_System_IConvertible_ToType_m6038,
	Byte_System_IConvertible_ToBoolean_m6039,
	Byte_System_IConvertible_ToByte_m6040,
	Byte_System_IConvertible_ToChar_m6041,
	Byte_System_IConvertible_ToDateTime_m6042,
	Byte_System_IConvertible_ToDecimal_m6043,
	Byte_System_IConvertible_ToDouble_m6044,
	Byte_System_IConvertible_ToInt16_m6045,
	Byte_System_IConvertible_ToInt32_m6046,
	Byte_System_IConvertible_ToInt64_m6047,
	Byte_System_IConvertible_ToSByte_m6048,
	Byte_System_IConvertible_ToSingle_m6049,
	Byte_System_IConvertible_ToUInt16_m6050,
	Byte_System_IConvertible_ToUInt32_m6051,
	Byte_System_IConvertible_ToUInt64_m6052,
	Byte_CompareTo_m6053,
	Byte_Equals_m6054,
	Byte_GetHashCode_m6055,
	Byte_CompareTo_m6056,
	Byte_Equals_m6057,
	Byte_Parse_m6058,
	Byte_Parse_m6059,
	Byte_Parse_m6060,
	Byte_TryParse_m6061,
	Byte_TryParse_m6062,
	Byte_ToString_m5860,
	Byte_ToString_m4890,
	Byte_ToString_m5821,
	Byte_ToString_m5826,
	SByte_System_IConvertible_ToBoolean_m6063,
	SByte_System_IConvertible_ToByte_m6064,
	SByte_System_IConvertible_ToChar_m6065,
	SByte_System_IConvertible_ToDateTime_m6066,
	SByte_System_IConvertible_ToDecimal_m6067,
	SByte_System_IConvertible_ToDouble_m6068,
	SByte_System_IConvertible_ToInt16_m6069,
	SByte_System_IConvertible_ToInt32_m6070,
	SByte_System_IConvertible_ToInt64_m6071,
	SByte_System_IConvertible_ToSByte_m6072,
	SByte_System_IConvertible_ToSingle_m6073,
	SByte_System_IConvertible_ToType_m6074,
	SByte_System_IConvertible_ToUInt16_m6075,
	SByte_System_IConvertible_ToUInt32_m6076,
	SByte_System_IConvertible_ToUInt64_m6077,
	SByte_CompareTo_m6078,
	SByte_Equals_m6079,
	SByte_GetHashCode_m6080,
	SByte_CompareTo_m6081,
	SByte_Equals_m6082,
	SByte_Parse_m6083,
	SByte_Parse_m6084,
	SByte_Parse_m6085,
	SByte_TryParse_m6086,
	SByte_ToString_m6087,
	SByte_ToString_m6088,
	SByte_ToString_m6089,
	SByte_ToString_m6090,
	Int16_System_IConvertible_ToBoolean_m6091,
	Int16_System_IConvertible_ToByte_m6092,
	Int16_System_IConvertible_ToChar_m6093,
	Int16_System_IConvertible_ToDateTime_m6094,
	Int16_System_IConvertible_ToDecimal_m6095,
	Int16_System_IConvertible_ToDouble_m6096,
	Int16_System_IConvertible_ToInt16_m6097,
	Int16_System_IConvertible_ToInt32_m6098,
	Int16_System_IConvertible_ToInt64_m6099,
	Int16_System_IConvertible_ToSByte_m6100,
	Int16_System_IConvertible_ToSingle_m6101,
	Int16_System_IConvertible_ToType_m6102,
	Int16_System_IConvertible_ToUInt16_m6103,
	Int16_System_IConvertible_ToUInt32_m6104,
	Int16_System_IConvertible_ToUInt64_m6105,
	Int16_CompareTo_m6106,
	Int16_Equals_m6107,
	Int16_GetHashCode_m6108,
	Int16_CompareTo_m6109,
	Int16_Equals_m6110,
	Int16_Parse_m6111,
	Int16_Parse_m6112,
	Int16_Parse_m6113,
	Int16_TryParse_m6114,
	Int16_ToString_m6115,
	Int16_ToString_m6116,
	Int16_ToString_m6117,
	Int16_ToString_m6118,
	UInt16_System_IConvertible_ToBoolean_m6119,
	UInt16_System_IConvertible_ToByte_m6120,
	UInt16_System_IConvertible_ToChar_m6121,
	UInt16_System_IConvertible_ToDateTime_m6122,
	UInt16_System_IConvertible_ToDecimal_m6123,
	UInt16_System_IConvertible_ToDouble_m6124,
	UInt16_System_IConvertible_ToInt16_m6125,
	UInt16_System_IConvertible_ToInt32_m6126,
	UInt16_System_IConvertible_ToInt64_m6127,
	UInt16_System_IConvertible_ToSByte_m6128,
	UInt16_System_IConvertible_ToSingle_m6129,
	UInt16_System_IConvertible_ToType_m6130,
	UInt16_System_IConvertible_ToUInt16_m6131,
	UInt16_System_IConvertible_ToUInt32_m6132,
	UInt16_System_IConvertible_ToUInt64_m6133,
	UInt16_CompareTo_m6134,
	UInt16_Equals_m6135,
	UInt16_GetHashCode_m6136,
	UInt16_CompareTo_m6137,
	UInt16_Equals_m6138,
	UInt16_Parse_m6139,
	UInt16_Parse_m6140,
	UInt16_TryParse_m6141,
	UInt16_TryParse_m6142,
	UInt16_ToString_m6143,
	UInt16_ToString_m6144,
	UInt16_ToString_m6145,
	UInt16_ToString_m6146,
	Char__cctor_m6147,
	Char_System_IConvertible_ToType_m6148,
	Char_System_IConvertible_ToBoolean_m6149,
	Char_System_IConvertible_ToByte_m6150,
	Char_System_IConvertible_ToChar_m6151,
	Char_System_IConvertible_ToDateTime_m6152,
	Char_System_IConvertible_ToDecimal_m6153,
	Char_System_IConvertible_ToDouble_m6154,
	Char_System_IConvertible_ToInt16_m6155,
	Char_System_IConvertible_ToInt32_m6156,
	Char_System_IConvertible_ToInt64_m6157,
	Char_System_IConvertible_ToSByte_m6158,
	Char_System_IConvertible_ToSingle_m6159,
	Char_System_IConvertible_ToUInt16_m6160,
	Char_System_IConvertible_ToUInt32_m6161,
	Char_System_IConvertible_ToUInt64_m6162,
	Char_GetDataTablePointers_m6163,
	Char_CompareTo_m6164,
	Char_Equals_m6165,
	Char_CompareTo_m6166,
	Char_Equals_m6167,
	Char_GetHashCode_m6168,
	Char_GetUnicodeCategory_m4961,
	Char_IsDigit_m4959,
	Char_IsLetter_m3695,
	Char_IsLetterOrDigit_m4958,
	Char_IsLower_m3696,
	Char_IsSurrogate_m6169,
	Char_IsUpper_m3698,
	Char_IsWhiteSpace_m4960,
	Char_IsWhiteSpace_m4877,
	Char_CheckParameter_m6170,
	Char_Parse_m6171,
	Char_ToLower_m3699,
	Char_ToLowerInvariant_m6172,
	Char_ToLower_m6173,
	Char_ToUpper_m3697,
	Char_ToUpperInvariant_m4879,
	Char_ToString_m3688,
	Char_ToString_m6174,
	Char_GetTypeCode_m6175,
	String__ctor_m6176,
	String__ctor_m6177,
	String__ctor_m6178,
	String__ctor_m6179,
	String__cctor_m6180,
	String_System_IConvertible_ToBoolean_m6181,
	String_System_IConvertible_ToByte_m6182,
	String_System_IConvertible_ToChar_m6183,
	String_System_IConvertible_ToDateTime_m6184,
	String_System_IConvertible_ToDecimal_m6185,
	String_System_IConvertible_ToDouble_m6186,
	String_System_IConvertible_ToInt16_m6187,
	String_System_IConvertible_ToInt32_m6188,
	String_System_IConvertible_ToInt64_m6189,
	String_System_IConvertible_ToSByte_m6190,
	String_System_IConvertible_ToSingle_m6191,
	String_System_IConvertible_ToType_m6192,
	String_System_IConvertible_ToUInt16_m6193,
	String_System_IConvertible_ToUInt32_m6194,
	String_System_IConvertible_ToUInt64_m6195,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m6196,
	String_System_Collections_IEnumerable_GetEnumerator_m6197,
	String_Equals_m6198,
	String_Equals_m6199,
	String_Equals_m4938,
	String_get_Chars_m2175,
	String_Clone_m6200,
	String_CopyTo_m6201,
	String_ToCharArray_m4842,
	String_ToCharArray_m6202,
	String_Split_m2210,
	String_Split_m6203,
	String_Split_m6204,
	String_Split_m6205,
	String_Split_m4880,
	String_Substring_m3690,
	String_Substring_m2176,
	String_SubstringUnchecked_m6206,
	String_Trim_m2206,
	String_Trim_m6207,
	String_TrimStart_m4972,
	String_TrimEnd_m4878,
	String_FindNotWhiteSpace_m6208,
	String_FindNotInTable_m6209,
	String_Compare_m6210,
	String_Compare_m6211,
	String_Compare_m4858,
	String_Compare_m5893,
	String_CompareTo_m6212,
	String_CompareTo_m6213,
	String_CompareOrdinal_m6214,
	String_CompareOrdinalUnchecked_m6215,
	String_CompareOrdinalCaseInsensitiveUnchecked_m6216,
	String_EndsWith_m2212,
	String_IndexOfAny_m6217,
	String_IndexOfAny_m3686,
	String_IndexOfAny_m5844,
	String_IndexOfAnyUnchecked_m6218,
	String_IndexOf_m4913,
	String_IndexOf_m6219,
	String_IndexOfOrdinal_m6220,
	String_IndexOfOrdinalUnchecked_m6221,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m6222,
	String_IndexOf_m3700,
	String_IndexOf_m4973,
	String_IndexOf_m4974,
	String_IndexOfUnchecked_m6223,
	String_IndexOf_m2211,
	String_IndexOf_m2214,
	String_IndexOf_m6224,
	String_LastIndexOfAny_m6225,
	String_LastIndexOfAny_m3687,
	String_LastIndexOfAnyUnchecked_m6226,
	String_LastIndexOf_m4848,
	String_LastIndexOf_m6227,
	String_LastIndexOf_m4975,
	String_LastIndexOfUnchecked_m6228,
	String_LastIndexOf_m2217,
	String_LastIndexOf_m6229,
	String_Contains_m3694,
	String_IsNullOrEmpty_m2173,
	String_PadRight_m6230,
	String_StartsWith_m2204,
	String_Replace_m2216,
	String_Replace_m2215,
	String_ReplaceUnchecked_m6231,
	String_ReplaceFallback_m6232,
	String_Remove_m2213,
	String_ToLower_m2177,
	String_ToLower_m4971,
	String_ToLowerInvariant_m6233,
	String_ToString_m2203,
	String_ToString_m6234,
	String_Format_m3619,
	String_Format_m6235,
	String_Format_m6236,
	String_Format_m2164,
	String_Format_m5871,
	String_FormatHelper_m6237,
	String_Concat_m2183,
	String_Concat_m3540,
	String_Concat_m559,
	String_Concat_m405,
	String_Concat_m2207,
	String_Concat_m2182,
	String_Concat_m4845,
	String_ConcatInternal_m6238,
	String_Insert_m2218,
	String_Join_m404,
	String_Join_m6239,
	String_JoinUnchecked_m6240,
	String_get_Length_m2136,
	String_ParseFormatSpecifier_m6241,
	String_ParseDecimal_m6242,
	String_InternalSetChar_m6243,
	String_InternalSetLength_m6244,
	String_GetHashCode_m2168,
	String_GetCaseInsensitiveHashCode_m6245,
	String_CreateString_m6246,
	String_CreateString_m6247,
	String_CreateString_m6248,
	String_CreateString_m6249,
	String_CreateString_m6250,
	String_CreateString_m6251,
	String_CreateString_m4964,
	String_CreateString_m3691,
	String_memcpy4_m6252,
	String_memcpy2_m6253,
	String_memcpy1_m6254,
	String_memcpy_m6255,
	String_CharCopy_m6256,
	String_CharCopyReverse_m6257,
	String_CharCopy_m6258,
	String_CharCopy_m6259,
	String_CharCopyReverse_m6260,
	String_InternalSplit_m6261,
	String_InternalAllocateStr_m6262,
	String_op_Equality_m687,
	String_op_Inequality_m3685,
	Single_System_IConvertible_ToBoolean_m6263,
	Single_System_IConvertible_ToByte_m6264,
	Single_System_IConvertible_ToChar_m6265,
	Single_System_IConvertible_ToDateTime_m6266,
	Single_System_IConvertible_ToDecimal_m6267,
	Single_System_IConvertible_ToDouble_m6268,
	Single_System_IConvertible_ToInt16_m6269,
	Single_System_IConvertible_ToInt32_m6270,
	Single_System_IConvertible_ToInt64_m6271,
	Single_System_IConvertible_ToSByte_m6272,
	Single_System_IConvertible_ToSingle_m6273,
	Single_System_IConvertible_ToType_m6274,
	Single_System_IConvertible_ToUInt16_m6275,
	Single_System_IConvertible_ToUInt32_m6276,
	Single_System_IConvertible_ToUInt64_m6277,
	Single_CompareTo_m6278,
	Single_Equals_m6279,
	Single_CompareTo_m3544,
	Single_Equals_m2161,
	Single_GetHashCode_m2153,
	Single_IsInfinity_m6280,
	Single_IsNaN_m6281,
	Single_IsNegativeInfinity_m6282,
	Single_IsPositiveInfinity_m6283,
	Single_Parse_m6284,
	Single_ToString_m6285,
	Single_ToString_m6286,
	Single_ToString_m690,
	Single_ToString_m6287,
	Single_GetTypeCode_m6288,
	Double_System_IConvertible_ToType_m6289,
	Double_System_IConvertible_ToBoolean_m6290,
	Double_System_IConvertible_ToByte_m6291,
	Double_System_IConvertible_ToChar_m6292,
	Double_System_IConvertible_ToDateTime_m6293,
	Double_System_IConvertible_ToDecimal_m6294,
	Double_System_IConvertible_ToDouble_m6295,
	Double_System_IConvertible_ToInt16_m6296,
	Double_System_IConvertible_ToInt32_m6297,
	Double_System_IConvertible_ToInt64_m6298,
	Double_System_IConvertible_ToSByte_m6299,
	Double_System_IConvertible_ToSingle_m6300,
	Double_System_IConvertible_ToUInt16_m6301,
	Double_System_IConvertible_ToUInt32_m6302,
	Double_System_IConvertible_ToUInt64_m6303,
	Double_CompareTo_m6304,
	Double_Equals_m6305,
	Double_CompareTo_m6306,
	Double_Equals_m6307,
	Double_GetHashCode_m6308,
	Double_IsInfinity_m6309,
	Double_IsNaN_m6310,
	Double_IsNegativeInfinity_m6311,
	Double_IsPositiveInfinity_m6312,
	Double_Parse_m6313,
	Double_Parse_m6314,
	Double_Parse_m6315,
	Double_Parse_m6316,
	Double_TryParseStringConstant_m6317,
	Double_ParseImpl_m6318,
	Double_ToString_m6319,
	Double_ToString_m6320,
	Double_ToString_m6321,
	Decimal__ctor_m6322,
	Decimal__ctor_m6323,
	Decimal__ctor_m6324,
	Decimal__ctor_m6325,
	Decimal__ctor_m6326,
	Decimal__ctor_m6327,
	Decimal__ctor_m6328,
	Decimal__cctor_m6329,
	Decimal_System_IConvertible_ToType_m6330,
	Decimal_System_IConvertible_ToBoolean_m6331,
	Decimal_System_IConvertible_ToByte_m6332,
	Decimal_System_IConvertible_ToChar_m6333,
	Decimal_System_IConvertible_ToDateTime_m6334,
	Decimal_System_IConvertible_ToDecimal_m6335,
	Decimal_System_IConvertible_ToDouble_m6336,
	Decimal_System_IConvertible_ToInt16_m6337,
	Decimal_System_IConvertible_ToInt32_m6338,
	Decimal_System_IConvertible_ToInt64_m6339,
	Decimal_System_IConvertible_ToSByte_m6340,
	Decimal_System_IConvertible_ToSingle_m6341,
	Decimal_System_IConvertible_ToUInt16_m6342,
	Decimal_System_IConvertible_ToUInt32_m6343,
	Decimal_System_IConvertible_ToUInt64_m6344,
	Decimal_GetBits_m6345,
	Decimal_Add_m6346,
	Decimal_Subtract_m6347,
	Decimal_GetHashCode_m6348,
	Decimal_u64_m6349,
	Decimal_s64_m6350,
	Decimal_Equals_m6351,
	Decimal_Equals_m6352,
	Decimal_IsZero_m6353,
	Decimal_Floor_m6354,
	Decimal_Multiply_m6355,
	Decimal_Divide_m6356,
	Decimal_Compare_m6357,
	Decimal_CompareTo_m6358,
	Decimal_CompareTo_m6359,
	Decimal_Equals_m6360,
	Decimal_Parse_m6361,
	Decimal_ThrowAtPos_m6362,
	Decimal_ThrowInvalidExp_m6363,
	Decimal_stripStyles_m6364,
	Decimal_Parse_m6365,
	Decimal_PerformParse_m6366,
	Decimal_ToString_m6367,
	Decimal_ToString_m6368,
	Decimal_ToString_m6369,
	Decimal_decimal2UInt64_m6370,
	Decimal_decimal2Int64_m6371,
	Decimal_decimalIncr_m6372,
	Decimal_string2decimal_m6373,
	Decimal_decimalSetExponent_m6374,
	Decimal_decimal2double_m6375,
	Decimal_decimalFloorAndTrunc_m6376,
	Decimal_decimalMult_m6377,
	Decimal_decimalDiv_m6378,
	Decimal_decimalCompare_m6379,
	Decimal_op_Increment_m6380,
	Decimal_op_Subtraction_m6381,
	Decimal_op_Multiply_m6382,
	Decimal_op_Division_m6383,
	Decimal_op_Explicit_m6384,
	Decimal_op_Explicit_m6385,
	Decimal_op_Explicit_m6386,
	Decimal_op_Explicit_m6387,
	Decimal_op_Explicit_m6388,
	Decimal_op_Explicit_m6389,
	Decimal_op_Explicit_m6390,
	Decimal_op_Explicit_m6391,
	Decimal_op_Implicit_m6392,
	Decimal_op_Implicit_m6393,
	Decimal_op_Implicit_m6394,
	Decimal_op_Implicit_m6395,
	Decimal_op_Implicit_m6396,
	Decimal_op_Implicit_m6397,
	Decimal_op_Implicit_m6398,
	Decimal_op_Implicit_m6399,
	Decimal_op_Explicit_m6400,
	Decimal_op_Explicit_m6401,
	Decimal_op_Explicit_m6402,
	Decimal_op_Explicit_m6403,
	Decimal_op_Inequality_m6404,
	Decimal_op_Equality_m3838,
	Decimal_op_GreaterThan_m6405,
	Decimal_op_LessThan_m6406,
	Boolean__cctor_m6407,
	Boolean_System_IConvertible_ToType_m6408,
	Boolean_System_IConvertible_ToBoolean_m6409,
	Boolean_System_IConvertible_ToByte_m6410,
	Boolean_System_IConvertible_ToChar_m6411,
	Boolean_System_IConvertible_ToDateTime_m6412,
	Boolean_System_IConvertible_ToDecimal_m6413,
	Boolean_System_IConvertible_ToDouble_m6414,
	Boolean_System_IConvertible_ToInt16_m6415,
	Boolean_System_IConvertible_ToInt32_m6416,
	Boolean_System_IConvertible_ToInt64_m6417,
	Boolean_System_IConvertible_ToSByte_m6418,
	Boolean_System_IConvertible_ToSingle_m6419,
	Boolean_System_IConvertible_ToUInt16_m6420,
	Boolean_System_IConvertible_ToUInt32_m6421,
	Boolean_System_IConvertible_ToUInt64_m6422,
	Boolean_CompareTo_m6423,
	Boolean_Equals_m6424,
	Boolean_CompareTo_m6425,
	Boolean_Equals_m6426,
	Boolean_GetHashCode_m6427,
	Boolean_Parse_m6428,
	Boolean_ToString_m6429,
	Boolean_GetTypeCode_m6430,
	Boolean_ToString_m6431,
	IntPtr__ctor_m2165,
	IntPtr__ctor_m6432,
	IntPtr__ctor_m6433,
	IntPtr__ctor_m6434,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6435,
	IntPtr_get_Size_m6436,
	IntPtr_Equals_m6437,
	IntPtr_GetHashCode_m6438,
	IntPtr_ToInt64_m6439,
	IntPtr_ToPointer_m2157,
	IntPtr_ToString_m6440,
	IntPtr_ToString_m6441,
	IntPtr_op_Equality_m2224,
	IntPtr_op_Inequality_m2156,
	IntPtr_op_Explicit_m6442,
	IntPtr_op_Explicit_m6443,
	IntPtr_op_Explicit_m6444,
	IntPtr_op_Explicit_m2223,
	IntPtr_op_Explicit_m6445,
	UIntPtr__ctor_m6446,
	UIntPtr__ctor_m6447,
	UIntPtr__ctor_m6448,
	UIntPtr__cctor_m6449,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6450,
	UIntPtr_Equals_m6451,
	UIntPtr_GetHashCode_m6452,
	UIntPtr_ToUInt32_m6453,
	UIntPtr_ToUInt64_m6454,
	UIntPtr_ToPointer_m6455,
	UIntPtr_ToString_m6456,
	UIntPtr_get_Size_m6457,
	UIntPtr_op_Equality_m6458,
	UIntPtr_op_Inequality_m6459,
	UIntPtr_op_Explicit_m6460,
	UIntPtr_op_Explicit_m6461,
	UIntPtr_op_Explicit_m6462,
	UIntPtr_op_Explicit_m6463,
	UIntPtr_op_Explicit_m6464,
	UIntPtr_op_Explicit_m6465,
	MulticastDelegate_GetObjectData_m6466,
	MulticastDelegate_Equals_m6467,
	MulticastDelegate_GetHashCode_m6468,
	MulticastDelegate_GetInvocationList_m6469,
	MulticastDelegate_CombineImpl_m6470,
	MulticastDelegate_BaseEquals_m6471,
	MulticastDelegate_KPM_m6472,
	MulticastDelegate_RemoveImpl_m6473,
	Delegate_get_Method_m2246,
	Delegate_get_Target_m2227,
	Delegate_CreateDelegate_internal_m6474,
	Delegate_SetMulticastInvoke_m6475,
	Delegate_arg_type_match_m6476,
	Delegate_return_type_match_m6477,
	Delegate_CreateDelegate_m6478,
	Delegate_CreateDelegate_m2245,
	Delegate_CreateDelegate_m6479,
	Delegate_CreateDelegate_m3834,
	Delegate_CreateDelegate_m6480,
	Delegate_GetCandidateMethod_m6481,
	Delegate_CreateDelegate_m6482,
	Delegate_CreateDelegate_m6483,
	Delegate_CreateDelegate_m6484,
	Delegate_CreateDelegate_m6485,
	Delegate_Clone_m6486,
	Delegate_Equals_m6487,
	Delegate_GetHashCode_m6488,
	Delegate_GetObjectData_m6489,
	Delegate_GetInvocationList_m6490,
	Delegate_Combine_m340,
	Delegate_Combine_m6491,
	Delegate_CombineImpl_m6492,
	Delegate_Remove_m341,
	Delegate_RemoveImpl_m6493,
	Enum__ctor_m6494,
	Enum__cctor_m6495,
	Enum_System_IConvertible_ToBoolean_m6496,
	Enum_System_IConvertible_ToByte_m6497,
	Enum_System_IConvertible_ToChar_m6498,
	Enum_System_IConvertible_ToDateTime_m6499,
	Enum_System_IConvertible_ToDecimal_m6500,
	Enum_System_IConvertible_ToDouble_m6501,
	Enum_System_IConvertible_ToInt16_m6502,
	Enum_System_IConvertible_ToInt32_m6503,
	Enum_System_IConvertible_ToInt64_m6504,
	Enum_System_IConvertible_ToSByte_m6505,
	Enum_System_IConvertible_ToSingle_m6506,
	Enum_System_IConvertible_ToType_m6507,
	Enum_System_IConvertible_ToUInt16_m6508,
	Enum_System_IConvertible_ToUInt32_m6509,
	Enum_System_IConvertible_ToUInt64_m6510,
	Enum_GetTypeCode_m6511,
	Enum_get_value_m6512,
	Enum_get_Value_m6513,
	Enum_FindPosition_m6514,
	Enum_GetName_m6515,
	Enum_IsDefined_m5880,
	Enum_get_underlying_type_m6516,
	Enum_GetUnderlyingType_m6517,
	Enum_FindName_m6518,
	Enum_GetValue_m6519,
	Enum_Parse_m2179,
	Enum_compare_value_to_m6520,
	Enum_CompareTo_m6521,
	Enum_ToString_m6522,
	Enum_ToString_m6523,
	Enum_ToString_m6524,
	Enum_ToString_m6525,
	Enum_ToObject_m6526,
	Enum_ToObject_m6527,
	Enum_ToObject_m6528,
	Enum_ToObject_m6529,
	Enum_ToObject_m6530,
	Enum_ToObject_m6531,
	Enum_ToObject_m6532,
	Enum_ToObject_m6533,
	Enum_ToObject_m6534,
	Enum_Equals_m6535,
	Enum_get_hashcode_m6536,
	Enum_GetHashCode_m6537,
	Enum_FormatSpecifier_X_m6538,
	Enum_FormatFlags_m6539,
	Enum_Format_m6540,
	SimpleEnumerator__ctor_m6541,
	SimpleEnumerator_get_Current_m6542,
	SimpleEnumerator_MoveNext_m6543,
	SimpleEnumerator_Reset_m6544,
	SimpleEnumerator_Clone_m6545,
	Swapper__ctor_m6546,
	Swapper_Invoke_m6547,
	Swapper_BeginInvoke_m6548,
	Swapper_EndInvoke_m6549,
	Array__ctor_m6550,
	Array_System_Collections_IList_get_Item_m6551,
	Array_System_Collections_IList_set_Item_m6552,
	Array_System_Collections_IList_Add_m6553,
	Array_System_Collections_IList_Clear_m6554,
	Array_System_Collections_IList_Contains_m6555,
	Array_System_Collections_IList_IndexOf_m6556,
	Array_System_Collections_IList_Insert_m6557,
	Array_System_Collections_IList_Remove_m6558,
	Array_System_Collections_IList_RemoveAt_m6559,
	Array_System_Collections_ICollection_get_Count_m6560,
	Array_InternalArray__ICollection_get_Count_m6561,
	Array_InternalArray__ICollection_get_IsReadOnly_m6562,
	Array_InternalArray__ICollection_Clear_m6563,
	Array_InternalArray__RemoveAt_m6564,
	Array_get_Length_m3836,
	Array_get_LongLength_m6565,
	Array_get_Rank_m3835,
	Array_GetRank_m6566,
	Array_GetLength_m491,
	Array_GetLongLength_m6567,
	Array_GetLowerBound_m6568,
	Array_GetValue_m6569,
	Array_SetValue_m6570,
	Array_GetValueImpl_m6571,
	Array_SetValueImpl_m6572,
	Array_FastCopy_m6573,
	Array_CreateInstanceImpl_m6574,
	Array_get_IsSynchronized_m6575,
	Array_get_SyncRoot_m6576,
	Array_get_IsFixedSize_m6577,
	Array_get_IsReadOnly_m6578,
	Array_GetEnumerator_m6579,
	Array_GetUpperBound_m6580,
	Array_GetValue_m3837,
	Array_GetValue_m6581,
	Array_GetValue_m6582,
	Array_GetValue_m6583,
	Array_GetValue_m6584,
	Array_GetValue_m6585,
	Array_SetValue_m6586,
	Array_SetValue_m6587,
	Array_SetValue_m6588,
	Array_SetValue_m4821,
	Array_SetValue_m6589,
	Array_SetValue_m6590,
	Array_CreateInstance_m6591,
	Array_CreateInstance_m6592,
	Array_CreateInstance_m6593,
	Array_CreateInstance_m6594,
	Array_CreateInstance_m6595,
	Array_GetIntArray_m6596,
	Array_CreateInstance_m6597,
	Array_GetValue_m6598,
	Array_SetValue_m6599,
	Array_BinarySearch_m6600,
	Array_BinarySearch_m6601,
	Array_BinarySearch_m6602,
	Array_BinarySearch_m6603,
	Array_DoBinarySearch_m6604,
	Array_Clear_m3896,
	Array_ClearInternal_m6605,
	Array_Clone_m6606,
	Array_Copy_m4965,
	Array_Copy_m6607,
	Array_Copy_m6608,
	Array_Copy_m6609,
	Array_IndexOf_m6610,
	Array_IndexOf_m6611,
	Array_IndexOf_m6612,
	Array_Initialize_m6613,
	Array_LastIndexOf_m6614,
	Array_LastIndexOf_m6615,
	Array_LastIndexOf_m6616,
	Array_get_swapper_m6617,
	Array_Reverse_m5820,
	Array_Reverse_m5845,
	Array_Sort_m6618,
	Array_Sort_m6619,
	Array_Sort_m6620,
	Array_Sort_m6621,
	Array_Sort_m6622,
	Array_Sort_m6623,
	Array_Sort_m6624,
	Array_Sort_m6625,
	Array_int_swapper_m6626,
	Array_obj_swapper_m6627,
	Array_slow_swapper_m6628,
	Array_double_swapper_m6629,
	Array_new_gap_m6630,
	Array_combsort_m6631,
	Array_combsort_m6632,
	Array_combsort_m6633,
	Array_qsort_m6634,
	Array_swap_m6635,
	Array_compare_m6636,
	Array_CopyTo_m6637,
	Array_CopyTo_m6638,
	Array_ConstrainedCopy_m6639,
	Type__ctor_m6640,
	Type__cctor_m6641,
	Type_FilterName_impl_m6642,
	Type_FilterNameIgnoreCase_impl_m6643,
	Type_FilterAttribute_impl_m6644,
	Type_get_Attributes_m6645,
	Type_get_DeclaringType_m6646,
	Type_get_HasElementType_m6647,
	Type_get_IsAbstract_m6648,
	Type_get_IsArray_m6649,
	Type_get_IsByRef_m6650,
	Type_get_IsClass_m6651,
	Type_get_IsContextful_m6652,
	Type_get_IsEnum_m6653,
	Type_get_IsExplicitLayout_m6654,
	Type_get_IsInterface_m6655,
	Type_get_IsMarshalByRef_m6656,
	Type_get_IsPointer_m6657,
	Type_get_IsPrimitive_m6658,
	Type_get_IsSealed_m6659,
	Type_get_IsSerializable_m6660,
	Type_get_IsValueType_m6661,
	Type_get_MemberType_m6662,
	Type_get_ReflectedType_m6663,
	Type_get_TypeHandle_m6664,
	Type_Equals_m6665,
	Type_Equals_m6666,
	Type_EqualsInternal_m6667,
	Type_internal_from_handle_m6668,
	Type_internal_from_name_m6669,
	Type_GetType_m6670,
	Type_GetType_m2232,
	Type_GetTypeCodeInternal_m6671,
	Type_GetTypeCode_m3833,
	Type_GetTypeFromHandle_m651,
	Type_GetTypeHandle_m6672,
	Type_type_is_subtype_of_m6673,
	Type_type_is_assignable_from_m6674,
	Type_IsSubclassOf_m6675,
	Type_IsAssignableFrom_m6676,
	Type_IsInstanceOfType_m6677,
	Type_GetHashCode_m6678,
	Type_GetMethod_m6679,
	Type_GetMethod_m6680,
	Type_GetMethod_m6681,
	Type_GetMethod_m6682,
	Type_GetProperty_m6683,
	Type_GetProperty_m6684,
	Type_GetProperty_m6685,
	Type_GetProperty_m6686,
	Type_IsArrayImpl_m6687,
	Type_IsValueTypeImpl_m6688,
	Type_IsContextfulImpl_m6689,
	Type_IsMarshalByRefImpl_m6690,
	Type_GetConstructor_m6691,
	Type_GetConstructor_m6692,
	Type_GetConstructor_m6693,
	Type_ToString_m6694,
	Type_get_IsSystemType_m6695,
	Type_GetGenericArguments_m6696,
	Type_get_ContainsGenericParameters_m6697,
	Type_get_IsGenericTypeDefinition_m6698,
	Type_GetGenericTypeDefinition_impl_m6699,
	Type_GetGenericTypeDefinition_m6700,
	Type_get_IsGenericType_m6701,
	Type_MakeGenericType_m6702,
	Type_MakeGenericType_m6703,
	Type_get_IsGenericParameter_m6704,
	Type_get_IsNested_m6705,
	Type_GetPseudoCustomAttributes_m6706,
	MemberInfo__ctor_m6707,
	MemberInfo_get_Module_m6708,
	Exception__ctor_m5817,
	Exception__ctor_m2155,
	Exception__ctor_m2222,
	Exception__ctor_m2221,
	Exception_get_InnerException_m6709,
	Exception_set_HResult_m2220,
	Exception_get_ClassName_m6710,
	Exception_get_Message_m6711,
	Exception_get_Source_m6712,
	Exception_get_StackTrace_m6713,
	Exception_GetObjectData_m4979,
	Exception_ToString_m6714,
	Exception_GetFullNameForStackTrace_m6715,
	Exception_GetType_m6716,
	RuntimeFieldHandle__ctor_m6717,
	RuntimeFieldHandle_get_Value_m6718,
	RuntimeFieldHandle_GetObjectData_m6719,
	RuntimeFieldHandle_Equals_m6720,
	RuntimeFieldHandle_GetHashCode_m6721,
	RuntimeTypeHandle__ctor_m6722,
	RuntimeTypeHandle_get_Value_m6723,
	RuntimeTypeHandle_GetObjectData_m6724,
	RuntimeTypeHandle_Equals_m6725,
	RuntimeTypeHandle_GetHashCode_m6726,
	ParamArrayAttribute__ctor_m6727,
	OutAttribute__ctor_m6728,
	ObsoleteAttribute__ctor_m6729,
	ObsoleteAttribute__ctor_m6730,
	ObsoleteAttribute__ctor_m6731,
	DllImportAttribute__ctor_m6732,
	DllImportAttribute_get_Value_m6733,
	MarshalAsAttribute__ctor_m6734,
	InAttribute__ctor_m6735,
	GuidAttribute__ctor_m6736,
	AssemblyCultureAttribute__ctor_m6737,
	AssemblyVersionAttribute__ctor_m6738,
	ComImportAttribute__ctor_m6739,
	OptionalAttribute__ctor_m6740,
	CompilerGeneratedAttribute__ctor_m6741,
	InternalsVisibleToAttribute__ctor_m6742,
	RuntimeCompatibilityAttribute__ctor_m6743,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6744,
	DebuggerHiddenAttribute__ctor_m6745,
	DefaultMemberAttribute__ctor_m6746,
	DefaultMemberAttribute_get_MemberName_m6747,
	DecimalConstantAttribute__ctor_m6748,
	FieldOffsetAttribute__ctor_m6749,
	AsyncCallback__ctor_m5879,
	AsyncCallback_Invoke_m6750,
	AsyncCallback_BeginInvoke_m5877,
	AsyncCallback_EndInvoke_m6751,
	TypedReference_Equals_m6752,
	TypedReference_GetHashCode_m6753,
	ArgIterator_Equals_m6754,
	ArgIterator_GetHashCode_m6755,
	MarshalByRefObject__ctor_m4859,
	MarshalByRefObject_get_ObjectIdentity_m6756,
	RuntimeHelpers_InitializeArray_m6757,
	RuntimeHelpers_InitializeArray_m493,
	RuntimeHelpers_get_OffsetToStringData_m6758,
	Locale_GetText_m6759,
	Locale_GetText_m6760,
	MonoTODOAttribute__ctor_m6761,
	MonoTODOAttribute__ctor_m6762,
	MonoDocumentationNoteAttribute__ctor_m6763,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m6764,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6765,
	SafeWaitHandle__ctor_m6766,
	SafeWaitHandle_ReleaseHandle_m6767,
	TableRange__ctor_m6768,
	CodePointIndexer__ctor_m6769,
	CodePointIndexer_ToIndex_m6770,
	TailoringInfo__ctor_m6771,
	Contraction__ctor_m6772,
	ContractionComparer__ctor_m6773,
	ContractionComparer__cctor_m6774,
	ContractionComparer_Compare_m6775,
	Level2Map__ctor_m6776,
	Level2MapComparer__ctor_m6777,
	Level2MapComparer__cctor_m6778,
	Level2MapComparer_Compare_m6779,
	MSCompatUnicodeTable__cctor_m6780,
	MSCompatUnicodeTable_GetTailoringInfo_m6781,
	MSCompatUnicodeTable_BuildTailoringTables_m6782,
	MSCompatUnicodeTable_SetCJKReferences_m6783,
	MSCompatUnicodeTable_Category_m6784,
	MSCompatUnicodeTable_Level1_m6785,
	MSCompatUnicodeTable_Level2_m6786,
	MSCompatUnicodeTable_Level3_m6787,
	MSCompatUnicodeTable_IsIgnorable_m6788,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m6789,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m6790,
	MSCompatUnicodeTable_ToWidthCompat_m6791,
	MSCompatUnicodeTable_HasSpecialWeight_m6792,
	MSCompatUnicodeTable_IsHalfWidthKana_m6793,
	MSCompatUnicodeTable_IsHiragana_m6794,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m6795,
	MSCompatUnicodeTable_get_IsReady_m6796,
	MSCompatUnicodeTable_GetResource_m6797,
	MSCompatUnicodeTable_UInt32FromBytePtr_m6798,
	MSCompatUnicodeTable_FillCJK_m6799,
	MSCompatUnicodeTable_FillCJKCore_m6800,
	MSCompatUnicodeTableUtil__cctor_m6801,
	Context__ctor_m6802,
	PreviousInfo__ctor_m6803,
	SimpleCollator__ctor_m6804,
	SimpleCollator__cctor_m6805,
	SimpleCollator_SetCJKTable_m6806,
	SimpleCollator_GetNeutralCulture_m6807,
	SimpleCollator_Category_m6808,
	SimpleCollator_Level1_m6809,
	SimpleCollator_Level2_m6810,
	SimpleCollator_IsHalfKana_m6811,
	SimpleCollator_GetContraction_m6812,
	SimpleCollator_GetContraction_m6813,
	SimpleCollator_GetTailContraction_m6814,
	SimpleCollator_GetTailContraction_m6815,
	SimpleCollator_FilterOptions_m6816,
	SimpleCollator_GetExtenderType_m6817,
	SimpleCollator_ToDashTypeValue_m6818,
	SimpleCollator_FilterExtender_m6819,
	SimpleCollator_IsIgnorable_m6820,
	SimpleCollator_IsSafe_m6821,
	SimpleCollator_GetSortKey_m6822,
	SimpleCollator_GetSortKey_m6823,
	SimpleCollator_GetSortKey_m6824,
	SimpleCollator_FillSortKeyRaw_m6825,
	SimpleCollator_FillSurrogateSortKeyRaw_m6826,
	SimpleCollator_CompareOrdinal_m6827,
	SimpleCollator_CompareQuick_m6828,
	SimpleCollator_CompareOrdinalIgnoreCase_m6829,
	SimpleCollator_Compare_m6830,
	SimpleCollator_ClearBuffer_m6831,
	SimpleCollator_QuickCheckPossible_m6832,
	SimpleCollator_CompareInternal_m6833,
	SimpleCollator_CompareFlagPair_m6834,
	SimpleCollator_IsPrefix_m6835,
	SimpleCollator_IsPrefix_m6836,
	SimpleCollator_IsPrefix_m6837,
	SimpleCollator_IsSuffix_m6838,
	SimpleCollator_IsSuffix_m6839,
	SimpleCollator_QuickIndexOf_m6840,
	SimpleCollator_IndexOf_m6841,
	SimpleCollator_IndexOfOrdinal_m6842,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m6843,
	SimpleCollator_IndexOfSortKey_m6844,
	SimpleCollator_IndexOf_m6845,
	SimpleCollator_LastIndexOf_m6846,
	SimpleCollator_LastIndexOfOrdinal_m6847,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6848,
	SimpleCollator_LastIndexOfSortKey_m6849,
	SimpleCollator_LastIndexOf_m6850,
	SimpleCollator_MatchesForward_m6851,
	SimpleCollator_MatchesForwardCore_m6852,
	SimpleCollator_MatchesPrimitive_m6853,
	SimpleCollator_MatchesBackward_m6854,
	SimpleCollator_MatchesBackwardCore_m6855,
	SortKey__ctor_m6856,
	SortKey__ctor_m6857,
	SortKey_Compare_m6858,
	SortKey_get_OriginalString_m6859,
	SortKey_get_KeyData_m6860,
	SortKey_Equals_m6861,
	SortKey_GetHashCode_m6862,
	SortKey_ToString_m6863,
	SortKeyBuffer__ctor_m6864,
	SortKeyBuffer_Reset_m6865,
	SortKeyBuffer_Initialize_m6866,
	SortKeyBuffer_AppendCJKExtension_m6867,
	SortKeyBuffer_AppendKana_m6868,
	SortKeyBuffer_AppendNormal_m6869,
	SortKeyBuffer_AppendLevel5_m6870,
	SortKeyBuffer_AppendBufferPrimitive_m6871,
	SortKeyBuffer_GetResultAndReset_m6872,
	SortKeyBuffer_GetOptimizedLength_m6873,
	SortKeyBuffer_GetResult_m6874,
	PrimeGeneratorBase__ctor_m6875,
	PrimeGeneratorBase_get_Confidence_m6876,
	PrimeGeneratorBase_get_PrimalityTest_m6877,
	PrimeGeneratorBase_get_TrialDivisionBounds_m6878,
	SequentialSearchPrimeGeneratorBase__ctor_m6879,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6880,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6881,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6882,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6883,
	PrimalityTests_GetSPPRounds_m6884,
	PrimalityTests_Test_m6885,
	PrimalityTests_RabinMillerTest_m6886,
	PrimalityTests_SmallPrimeSppTest_m6887,
	ModulusRing__ctor_m6888,
	ModulusRing_BarrettReduction_m6889,
	ModulusRing_Multiply_m6890,
	ModulusRing_Difference_m6891,
	ModulusRing_Pow_m6892,
	ModulusRing_Pow_m6893,
	Kernel_AddSameSign_m6894,
	Kernel_Subtract_m6895,
	Kernel_MinusEq_m6896,
	Kernel_PlusEq_m6897,
	Kernel_Compare_m6898,
	Kernel_SingleByteDivideInPlace_m6899,
	Kernel_DwordMod_m6900,
	Kernel_DwordDivMod_m6901,
	Kernel_multiByteDivide_m6902,
	Kernel_LeftShift_m6903,
	Kernel_RightShift_m6904,
	Kernel_MultiplyByDword_m6905,
	Kernel_Multiply_m6906,
	Kernel_MultiplyMod2p32pmod_m6907,
	Kernel_modInverse_m6908,
	Kernel_modInverse_m6909,
	BigInteger__ctor_m6910,
	BigInteger__ctor_m6911,
	BigInteger__ctor_m6912,
	BigInteger__ctor_m6913,
	BigInteger__ctor_m6914,
	BigInteger__cctor_m6915,
	BigInteger_get_Rng_m6916,
	BigInteger_GenerateRandom_m6917,
	BigInteger_GenerateRandom_m6918,
	BigInteger_Randomize_m6919,
	BigInteger_Randomize_m6920,
	BigInteger_BitCount_m6921,
	BigInteger_TestBit_m6922,
	BigInteger_TestBit_m6923,
	BigInteger_SetBit_m6924,
	BigInteger_SetBit_m6925,
	BigInteger_LowestSetBit_m6926,
	BigInteger_GetBytes_m6927,
	BigInteger_ToString_m6928,
	BigInteger_ToString_m6929,
	BigInteger_Normalize_m6930,
	BigInteger_Clear_m6931,
	BigInteger_GetHashCode_m6932,
	BigInteger_ToString_m6933,
	BigInteger_Equals_m6934,
	BigInteger_ModInverse_m6935,
	BigInteger_ModPow_m6936,
	BigInteger_IsProbablePrime_m6937,
	BigInteger_GeneratePseudoPrime_m6938,
	BigInteger_Incr2_m6939,
	BigInteger_op_Implicit_m6940,
	BigInteger_op_Implicit_m6941,
	BigInteger_op_Addition_m6942,
	BigInteger_op_Subtraction_m6943,
	BigInteger_op_Modulus_m6944,
	BigInteger_op_Modulus_m6945,
	BigInteger_op_Division_m6946,
	BigInteger_op_Multiply_m6947,
	BigInteger_op_Multiply_m6948,
	BigInteger_op_LeftShift_m6949,
	BigInteger_op_RightShift_m6950,
	BigInteger_op_Equality_m6951,
	BigInteger_op_Inequality_m6952,
	BigInteger_op_Equality_m6953,
	BigInteger_op_Inequality_m6954,
	BigInteger_op_GreaterThan_m6955,
	BigInteger_op_LessThan_m6956,
	BigInteger_op_GreaterThanOrEqual_m6957,
	BigInteger_op_LessThanOrEqual_m6958,
	CryptoConvert_ToInt32LE_m6959,
	CryptoConvert_ToUInt32LE_m6960,
	CryptoConvert_GetBytesLE_m6961,
	CryptoConvert_ToCapiPrivateKeyBlob_m6962,
	CryptoConvert_FromCapiPublicKeyBlob_m6963,
	CryptoConvert_FromCapiPublicKeyBlob_m6964,
	CryptoConvert_ToCapiPublicKeyBlob_m6965,
	CryptoConvert_ToCapiKeyBlob_m6966,
	KeyBuilder_get_Rng_m6967,
	KeyBuilder_Key_m6968,
	KeyBuilder_IV_m6969,
	BlockProcessor__ctor_m6970,
	BlockProcessor_Finalize_m6971,
	BlockProcessor_Initialize_m6972,
	BlockProcessor_Core_m6973,
	BlockProcessor_Core_m6974,
	BlockProcessor_Final_m6975,
	KeyGeneratedEventHandler__ctor_m6976,
	KeyGeneratedEventHandler_Invoke_m6977,
	KeyGeneratedEventHandler_BeginInvoke_m6978,
	KeyGeneratedEventHandler_EndInvoke_m6979,
	DSAManaged__ctor_m6980,
	DSAManaged_add_KeyGenerated_m6981,
	DSAManaged_remove_KeyGenerated_m6982,
	DSAManaged_Finalize_m6983,
	DSAManaged_Generate_m6984,
	DSAManaged_GenerateKeyPair_m6985,
	DSAManaged_add_m6986,
	DSAManaged_GenerateParams_m6987,
	DSAManaged_get_Random_m6988,
	DSAManaged_get_KeySize_m6989,
	DSAManaged_get_PublicOnly_m6990,
	DSAManaged_NormalizeArray_m6991,
	DSAManaged_ExportParameters_m6992,
	DSAManaged_ImportParameters_m6993,
	DSAManaged_CreateSignature_m6994,
	DSAManaged_VerifySignature_m6995,
	DSAManaged_Dispose_m6996,
	KeyPairPersistence__ctor_m6997,
	KeyPairPersistence__ctor_m6998,
	KeyPairPersistence__cctor_m6999,
	KeyPairPersistence_get_Filename_m7000,
	KeyPairPersistence_get_KeyValue_m7001,
	KeyPairPersistence_set_KeyValue_m7002,
	KeyPairPersistence_Load_m7003,
	KeyPairPersistence_Save_m7004,
	KeyPairPersistence_Remove_m7005,
	KeyPairPersistence_get_UserPath_m7006,
	KeyPairPersistence_get_MachinePath_m7007,
	KeyPairPersistence__CanSecure_m7008,
	KeyPairPersistence__ProtectUser_m7009,
	KeyPairPersistence__ProtectMachine_m7010,
	KeyPairPersistence__IsUserProtected_m7011,
	KeyPairPersistence__IsMachineProtected_m7012,
	KeyPairPersistence_CanSecure_m7013,
	KeyPairPersistence_ProtectUser_m7014,
	KeyPairPersistence_ProtectMachine_m7015,
	KeyPairPersistence_IsUserProtected_m7016,
	KeyPairPersistence_IsMachineProtected_m7017,
	KeyPairPersistence_get_CanChange_m7018,
	KeyPairPersistence_get_UseDefaultKeyContainer_m7019,
	KeyPairPersistence_get_UseMachineKeyStore_m7020,
	KeyPairPersistence_get_ContainerName_m7021,
	KeyPairPersistence_Copy_m7022,
	KeyPairPersistence_FromXml_m7023,
	KeyPairPersistence_ToXml_m7024,
	MACAlgorithm__ctor_m7025,
	MACAlgorithm_Initialize_m7026,
	MACAlgorithm_Core_m7027,
	MACAlgorithm_Final_m7028,
	PKCS1__cctor_m7029,
	PKCS1_Compare_m7030,
	PKCS1_I2OSP_m7031,
	PKCS1_OS2IP_m7032,
	PKCS1_RSAEP_m7033,
	PKCS1_RSASP1_m7034,
	PKCS1_RSAVP1_m7035,
	PKCS1_Encrypt_v15_m7036,
	PKCS1_Sign_v15_m7037,
	PKCS1_Verify_v15_m7038,
	PKCS1_Verify_v15_m7039,
	PKCS1_Encode_v15_m7040,
	PrivateKeyInfo__ctor_m7041,
	PrivateKeyInfo__ctor_m7042,
	PrivateKeyInfo_get_PrivateKey_m7043,
	PrivateKeyInfo_Decode_m7044,
	PrivateKeyInfo_RemoveLeadingZero_m7045,
	PrivateKeyInfo_Normalize_m7046,
	PrivateKeyInfo_DecodeRSA_m7047,
	PrivateKeyInfo_DecodeDSA_m7048,
	EncryptedPrivateKeyInfo__ctor_m7049,
	EncryptedPrivateKeyInfo__ctor_m7050,
	EncryptedPrivateKeyInfo_get_Algorithm_m7051,
	EncryptedPrivateKeyInfo_get_EncryptedData_m7052,
	EncryptedPrivateKeyInfo_get_Salt_m7053,
	EncryptedPrivateKeyInfo_get_IterationCount_m7054,
	EncryptedPrivateKeyInfo_Decode_m7055,
	KeyGeneratedEventHandler__ctor_m7056,
	KeyGeneratedEventHandler_Invoke_m7057,
	KeyGeneratedEventHandler_BeginInvoke_m7058,
	KeyGeneratedEventHandler_EndInvoke_m7059,
	RSAManaged__ctor_m7060,
	RSAManaged_add_KeyGenerated_m7061,
	RSAManaged_remove_KeyGenerated_m7062,
	RSAManaged_Finalize_m7063,
	RSAManaged_GenerateKeyPair_m7064,
	RSAManaged_get_KeySize_m7065,
	RSAManaged_get_PublicOnly_m7066,
	RSAManaged_DecryptValue_m7067,
	RSAManaged_EncryptValue_m7068,
	RSAManaged_ExportParameters_m7069,
	RSAManaged_ImportParameters_m7070,
	RSAManaged_Dispose_m7071,
	RSAManaged_ToXmlString_m7072,
	RSAManaged_get_IsCrtPossible_m7073,
	RSAManaged_GetPaddedValue_m7074,
	SymmetricTransform__ctor_m7075,
	SymmetricTransform_System_IDisposable_Dispose_m7076,
	SymmetricTransform_Finalize_m7077,
	SymmetricTransform_Dispose_m7078,
	SymmetricTransform_get_CanReuseTransform_m7079,
	SymmetricTransform_Transform_m7080,
	SymmetricTransform_CBC_m7081,
	SymmetricTransform_CFB_m7082,
	SymmetricTransform_OFB_m7083,
	SymmetricTransform_CTS_m7084,
	SymmetricTransform_CheckInput_m7085,
	SymmetricTransform_TransformBlock_m7086,
	SymmetricTransform_get_KeepLastBlock_m7087,
	SymmetricTransform_InternalTransformBlock_m7088,
	SymmetricTransform_Random_m7089,
	SymmetricTransform_ThrowBadPaddingException_m7090,
	SymmetricTransform_FinalEncrypt_m7091,
	SymmetricTransform_FinalDecrypt_m7092,
	SymmetricTransform_TransformFinalBlock_m7093,
	SafeBag__ctor_m7094,
	SafeBag_get_BagOID_m7095,
	SafeBag_get_ASN1_m7096,
	DeriveBytes__ctor_m7097,
	DeriveBytes__cctor_m7098,
	DeriveBytes_set_HashName_m7099,
	DeriveBytes_set_IterationCount_m7100,
	DeriveBytes_set_Password_m7101,
	DeriveBytes_set_Salt_m7102,
	DeriveBytes_Adjust_m7103,
	DeriveBytes_Derive_m7104,
	DeriveBytes_DeriveKey_m7105,
	DeriveBytes_DeriveIV_m7106,
	DeriveBytes_DeriveMAC_m7107,
	PKCS12__ctor_m7108,
	PKCS12__ctor_m7109,
	PKCS12__ctor_m7110,
	PKCS12__cctor_m7111,
	PKCS12_Decode_m7112,
	PKCS12_Finalize_m7113,
	PKCS12_set_Password_m7114,
	PKCS12_get_IterationCount_m7115,
	PKCS12_set_IterationCount_m7116,
	PKCS12_get_Certificates_m7117,
	PKCS12_get_RNG_m7118,
	PKCS12_Compare_m7119,
	PKCS12_GetSymmetricAlgorithm_m7120,
	PKCS12_Decrypt_m7121,
	PKCS12_Decrypt_m7122,
	PKCS12_Encrypt_m7123,
	PKCS12_GetExistingParameters_m7124,
	PKCS12_AddPrivateKey_m7125,
	PKCS12_ReadSafeBag_m7126,
	PKCS12_CertificateSafeBag_m7127,
	PKCS12_MAC_m7128,
	PKCS12_GetBytes_m7129,
	PKCS12_EncryptedContentInfo_m7130,
	PKCS12_AddCertificate_m7131,
	PKCS12_AddCertificate_m7132,
	PKCS12_RemoveCertificate_m7133,
	PKCS12_RemoveCertificate_m7134,
	PKCS12_Clone_m7135,
	PKCS12_get_MaximumPasswordLength_m7136,
	X501__cctor_m7137,
	X501_ToString_m7138,
	X501_ToString_m7139,
	X501_AppendEntry_m7140,
	X509Certificate__ctor_m7141,
	X509Certificate__cctor_m7142,
	X509Certificate_Parse_m7143,
	X509Certificate_GetUnsignedBigInteger_m7144,
	X509Certificate_get_DSA_m7145,
	X509Certificate_get_IssuerName_m7146,
	X509Certificate_get_KeyAlgorithmParameters_m7147,
	X509Certificate_get_PublicKey_m7148,
	X509Certificate_get_RawData_m7149,
	X509Certificate_get_SubjectName_m7150,
	X509Certificate_get_ValidFrom_m7151,
	X509Certificate_get_ValidUntil_m7152,
	X509Certificate_GetIssuerName_m7153,
	X509Certificate_GetSubjectName_m7154,
	X509Certificate_GetObjectData_m7155,
	X509Certificate_PEM_m7156,
	X509CertificateEnumerator__ctor_m7157,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m7158,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m7159,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m7160,
	X509CertificateEnumerator_get_Current_m7161,
	X509CertificateEnumerator_MoveNext_m7162,
	X509CertificateEnumerator_Reset_m7163,
	X509CertificateCollection__ctor_m7164,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m7165,
	X509CertificateCollection_get_Item_m7166,
	X509CertificateCollection_Add_m7167,
	X509CertificateCollection_GetEnumerator_m7168,
	X509CertificateCollection_GetHashCode_m7169,
	X509Extension__ctor_m7170,
	X509Extension_Decode_m7171,
	X509Extension_Equals_m7172,
	X509Extension_GetHashCode_m7173,
	X509Extension_WriteLine_m7174,
	X509Extension_ToString_m7175,
	X509ExtensionCollection__ctor_m7176,
	X509ExtensionCollection__ctor_m7177,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m7178,
	ASN1__ctor_m7179,
	ASN1__ctor_m7180,
	ASN1__ctor_m7181,
	ASN1_get_Count_m7182,
	ASN1_get_Tag_m7183,
	ASN1_get_Length_m7184,
	ASN1_get_Value_m7185,
	ASN1_set_Value_m7186,
	ASN1_CompareArray_m7187,
	ASN1_CompareValue_m7188,
	ASN1_Add_m7189,
	ASN1_GetBytes_m7190,
	ASN1_Decode_m7191,
	ASN1_DecodeTLV_m7192,
	ASN1_get_Item_m7193,
	ASN1_Element_m7194,
	ASN1_ToString_m7195,
	ASN1Convert_FromInt32_m7196,
	ASN1Convert_FromOid_m7197,
	ASN1Convert_ToInt32_m7198,
	ASN1Convert_ToOid_m7199,
	ASN1Convert_ToDateTime_m7200,
	BitConverterLE_GetUIntBytes_m7201,
	BitConverterLE_GetBytes_m7202,
	BitConverterLE_UShortFromBytes_m7203,
	BitConverterLE_UIntFromBytes_m7204,
	BitConverterLE_ULongFromBytes_m7205,
	BitConverterLE_ToInt16_m7206,
	BitConverterLE_ToInt32_m7207,
	BitConverterLE_ToSingle_m7208,
	BitConverterLE_ToDouble_m7209,
	ContentInfo__ctor_m7210,
	ContentInfo__ctor_m7211,
	ContentInfo__ctor_m7212,
	ContentInfo__ctor_m7213,
	ContentInfo_get_ASN1_m7214,
	ContentInfo_get_Content_m7215,
	ContentInfo_set_Content_m7216,
	ContentInfo_get_ContentType_m7217,
	ContentInfo_set_ContentType_m7218,
	ContentInfo_GetASN1_m7219,
	EncryptedData__ctor_m7220,
	EncryptedData__ctor_m7221,
	EncryptedData_get_EncryptionAlgorithm_m7222,
	EncryptedData_get_EncryptedContent_m7223,
	StrongName__cctor_m7224,
	StrongName_get_PublicKey_m7225,
	StrongName_get_PublicKeyToken_m7226,
	StrongName_get_TokenAlgorithm_m7227,
	SecurityParser__ctor_m7228,
	SecurityParser_LoadXml_m7229,
	SecurityParser_ToXml_m7230,
	SecurityParser_OnStartParsing_m7231,
	SecurityParser_OnProcessingInstruction_m7232,
	SecurityParser_OnIgnorableWhitespace_m7233,
	SecurityParser_OnStartElement_m7234,
	SecurityParser_OnEndElement_m7235,
	SecurityParser_OnChars_m7236,
	SecurityParser_OnEndParsing_m7237,
	AttrListImpl__ctor_m7238,
	AttrListImpl_get_Length_m7239,
	AttrListImpl_GetName_m7240,
	AttrListImpl_GetValue_m7241,
	AttrListImpl_GetValue_m7242,
	AttrListImpl_get_Names_m7243,
	AttrListImpl_get_Values_m7244,
	AttrListImpl_Clear_m7245,
	AttrListImpl_Add_m7246,
	SmallXmlParser__ctor_m7247,
	SmallXmlParser_Error_m7248,
	SmallXmlParser_UnexpectedEndError_m7249,
	SmallXmlParser_IsNameChar_m7250,
	SmallXmlParser_IsWhitespace_m7251,
	SmallXmlParser_SkipWhitespaces_m7252,
	SmallXmlParser_HandleWhitespaces_m7253,
	SmallXmlParser_SkipWhitespaces_m7254,
	SmallXmlParser_Peek_m7255,
	SmallXmlParser_Read_m7256,
	SmallXmlParser_Expect_m7257,
	SmallXmlParser_ReadUntil_m7258,
	SmallXmlParser_ReadName_m7259,
	SmallXmlParser_Parse_m7260,
	SmallXmlParser_Cleanup_m7261,
	SmallXmlParser_ReadContent_m7262,
	SmallXmlParser_HandleBufferedContent_m7263,
	SmallXmlParser_ReadCharacters_m7264,
	SmallXmlParser_ReadReference_m7265,
	SmallXmlParser_ReadCharacterReference_m7266,
	SmallXmlParser_ReadAttribute_m7267,
	SmallXmlParser_ReadCDATASection_m7268,
	SmallXmlParser_ReadComment_m7269,
	SmallXmlParserException__ctor_m7270,
	Runtime_GetDisplayName_m7271,
	KeyNotFoundException__ctor_m7272,
	KeyNotFoundException__ctor_m7273,
	SimpleEnumerator__ctor_m7274,
	SimpleEnumerator__cctor_m7275,
	SimpleEnumerator_Clone_m7276,
	SimpleEnumerator_MoveNext_m7277,
	SimpleEnumerator_get_Current_m7278,
	SimpleEnumerator_Reset_m7279,
	ArrayListWrapper__ctor_m7280,
	ArrayListWrapper_get_Item_m7281,
	ArrayListWrapper_set_Item_m7282,
	ArrayListWrapper_get_Count_m7283,
	ArrayListWrapper_get_Capacity_m7284,
	ArrayListWrapper_set_Capacity_m7285,
	ArrayListWrapper_get_IsFixedSize_m7286,
	ArrayListWrapper_get_IsReadOnly_m7287,
	ArrayListWrapper_get_IsSynchronized_m7288,
	ArrayListWrapper_get_SyncRoot_m7289,
	ArrayListWrapper_Add_m7290,
	ArrayListWrapper_Clear_m7291,
	ArrayListWrapper_Contains_m7292,
	ArrayListWrapper_IndexOf_m7293,
	ArrayListWrapper_IndexOf_m7294,
	ArrayListWrapper_IndexOf_m7295,
	ArrayListWrapper_Insert_m7296,
	ArrayListWrapper_InsertRange_m7297,
	ArrayListWrapper_Remove_m7298,
	ArrayListWrapper_RemoveAt_m7299,
	ArrayListWrapper_CopyTo_m7300,
	ArrayListWrapper_CopyTo_m7301,
	ArrayListWrapper_CopyTo_m7302,
	ArrayListWrapper_GetEnumerator_m7303,
	ArrayListWrapper_AddRange_m7304,
	ArrayListWrapper_Clone_m7305,
	ArrayListWrapper_Sort_m7306,
	ArrayListWrapper_Sort_m7307,
	ArrayListWrapper_ToArray_m7308,
	ArrayListWrapper_ToArray_m7309,
	SynchronizedArrayListWrapper__ctor_m7310,
	SynchronizedArrayListWrapper_get_Item_m7311,
	SynchronizedArrayListWrapper_set_Item_m7312,
	SynchronizedArrayListWrapper_get_Count_m7313,
	SynchronizedArrayListWrapper_get_Capacity_m7314,
	SynchronizedArrayListWrapper_set_Capacity_m7315,
	SynchronizedArrayListWrapper_get_IsFixedSize_m7316,
	SynchronizedArrayListWrapper_get_IsReadOnly_m7317,
	SynchronizedArrayListWrapper_get_IsSynchronized_m7318,
	SynchronizedArrayListWrapper_get_SyncRoot_m7319,
	SynchronizedArrayListWrapper_Add_m7320,
	SynchronizedArrayListWrapper_Clear_m7321,
	SynchronizedArrayListWrapper_Contains_m7322,
	SynchronizedArrayListWrapper_IndexOf_m7323,
	SynchronizedArrayListWrapper_IndexOf_m7324,
	SynchronizedArrayListWrapper_IndexOf_m7325,
	SynchronizedArrayListWrapper_Insert_m7326,
	SynchronizedArrayListWrapper_InsertRange_m7327,
	SynchronizedArrayListWrapper_Remove_m7328,
	SynchronizedArrayListWrapper_RemoveAt_m7329,
	SynchronizedArrayListWrapper_CopyTo_m7330,
	SynchronizedArrayListWrapper_CopyTo_m7331,
	SynchronizedArrayListWrapper_CopyTo_m7332,
	SynchronizedArrayListWrapper_GetEnumerator_m7333,
	SynchronizedArrayListWrapper_AddRange_m7334,
	SynchronizedArrayListWrapper_Clone_m7335,
	SynchronizedArrayListWrapper_Sort_m7336,
	SynchronizedArrayListWrapper_Sort_m7337,
	SynchronizedArrayListWrapper_ToArray_m7338,
	SynchronizedArrayListWrapper_ToArray_m7339,
	FixedSizeArrayListWrapper__ctor_m7340,
	FixedSizeArrayListWrapper_get_ErrorMessage_m7341,
	FixedSizeArrayListWrapper_get_Capacity_m7342,
	FixedSizeArrayListWrapper_set_Capacity_m7343,
	FixedSizeArrayListWrapper_get_IsFixedSize_m7344,
	FixedSizeArrayListWrapper_Add_m7345,
	FixedSizeArrayListWrapper_AddRange_m7346,
	FixedSizeArrayListWrapper_Clear_m7347,
	FixedSizeArrayListWrapper_Insert_m7348,
	FixedSizeArrayListWrapper_InsertRange_m7349,
	FixedSizeArrayListWrapper_Remove_m7350,
	FixedSizeArrayListWrapper_RemoveAt_m7351,
	ReadOnlyArrayListWrapper__ctor_m7352,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m7353,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m7354,
	ReadOnlyArrayListWrapper_get_Item_m7355,
	ReadOnlyArrayListWrapper_set_Item_m7356,
	ReadOnlyArrayListWrapper_Sort_m7357,
	ReadOnlyArrayListWrapper_Sort_m7358,
	ArrayList__ctor_m4825,
	ArrayList__ctor_m4857,
	ArrayList__ctor_m4935,
	ArrayList__ctor_m7359,
	ArrayList__cctor_m7360,
	ArrayList_get_Item_m7361,
	ArrayList_set_Item_m7362,
	ArrayList_get_Count_m7363,
	ArrayList_get_Capacity_m7364,
	ArrayList_set_Capacity_m7365,
	ArrayList_get_IsFixedSize_m7366,
	ArrayList_get_IsReadOnly_m7367,
	ArrayList_get_IsSynchronized_m7368,
	ArrayList_get_SyncRoot_m7369,
	ArrayList_EnsureCapacity_m7370,
	ArrayList_Shift_m7371,
	ArrayList_Add_m7372,
	ArrayList_Clear_m7373,
	ArrayList_Contains_m7374,
	ArrayList_IndexOf_m7375,
	ArrayList_IndexOf_m7376,
	ArrayList_IndexOf_m7377,
	ArrayList_Insert_m7378,
	ArrayList_InsertRange_m7379,
	ArrayList_Remove_m7380,
	ArrayList_RemoveAt_m7381,
	ArrayList_CopyTo_m7382,
	ArrayList_CopyTo_m7383,
	ArrayList_CopyTo_m7384,
	ArrayList_GetEnumerator_m7385,
	ArrayList_AddRange_m7386,
	ArrayList_Sort_m7387,
	ArrayList_Sort_m7388,
	ArrayList_ToArray_m7389,
	ArrayList_ToArray_m7390,
	ArrayList_Clone_m7391,
	ArrayList_ThrowNewArgumentOutOfRangeException_m7392,
	ArrayList_Synchronized_m7393,
	ArrayList_ReadOnly_m5839,
	BitArrayEnumerator__ctor_m7394,
	BitArrayEnumerator_Clone_m7395,
	BitArrayEnumerator_get_Current_m7396,
	BitArrayEnumerator_MoveNext_m7397,
	BitArrayEnumerator_Reset_m7398,
	BitArrayEnumerator_checkVersion_m7399,
	BitArray__ctor_m7400,
	BitArray__ctor_m4968,
	BitArray_getByte_m7401,
	BitArray_get_Count_m7402,
	BitArray_get_IsSynchronized_m7403,
	BitArray_get_Item_m4963,
	BitArray_set_Item_m4969,
	BitArray_get_Length_m4962,
	BitArray_get_SyncRoot_m7404,
	BitArray_Clone_m7405,
	BitArray_CopyTo_m7406,
	BitArray_Get_m7407,
	BitArray_Set_m7408,
	BitArray_GetEnumerator_m7409,
	CaseInsensitiveComparer__ctor_m7410,
	CaseInsensitiveComparer__ctor_m7411,
	CaseInsensitiveComparer__cctor_m7412,
	CaseInsensitiveComparer_get_DefaultInvariant_m4814,
	CaseInsensitiveComparer_Compare_m7413,
	CaseInsensitiveHashCodeProvider__ctor_m7414,
	CaseInsensitiveHashCodeProvider__ctor_m7415,
	CaseInsensitiveHashCodeProvider__cctor_m7416,
	CaseInsensitiveHashCodeProvider_AreEqual_m7417,
	CaseInsensitiveHashCodeProvider_AreEqual_m7418,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m4815,
	CaseInsensitiveHashCodeProvider_GetHashCode_m7419,
	CollectionBase__ctor_m4917,
	CollectionBase_System_Collections_ICollection_CopyTo_m7420,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m7421,
	CollectionBase_System_Collections_ICollection_get_IsSynchronized_m7422,
	CollectionBase_System_Collections_IList_Add_m7423,
	CollectionBase_System_Collections_IList_Contains_m7424,
	CollectionBase_System_Collections_IList_IndexOf_m7425,
	CollectionBase_System_Collections_IList_Insert_m7426,
	CollectionBase_System_Collections_IList_Remove_m7427,
	CollectionBase_System_Collections_IList_get_IsFixedSize_m7428,
	CollectionBase_System_Collections_IList_get_IsReadOnly_m7429,
	CollectionBase_System_Collections_IList_get_Item_m7430,
	CollectionBase_System_Collections_IList_set_Item_m7431,
	CollectionBase_get_Count_m7432,
	CollectionBase_GetEnumerator_m7433,
	CollectionBase_Clear_m7434,
	CollectionBase_RemoveAt_m7435,
	CollectionBase_get_InnerList_m4911,
	CollectionBase_get_List_m4966,
	CollectionBase_OnClear_m7436,
	CollectionBase_OnClearComplete_m7437,
	CollectionBase_OnInsert_m7438,
	CollectionBase_OnInsertComplete_m7439,
	CollectionBase_OnRemove_m7440,
	CollectionBase_OnRemoveComplete_m7441,
	CollectionBase_OnSet_m7442,
	CollectionBase_OnSetComplete_m7443,
	CollectionBase_OnValidate_m7444,
	Comparer__ctor_m7445,
	Comparer__ctor_m7446,
	Comparer__cctor_m7447,
	Comparer_Compare_m7448,
	Comparer_GetObjectData_m7449,
	DictionaryEntry__ctor_m4819,
	DictionaryEntry_get_Key_m7450,
	DictionaryEntry_get_Value_m7451,
	KeyMarker__ctor_m7452,
	KeyMarker__cctor_m7453,
	Enumerator__ctor_m7454,
	Enumerator__cctor_m7455,
	Enumerator_FailFast_m7456,
	Enumerator_Reset_m7457,
	Enumerator_MoveNext_m7458,
	Enumerator_get_Entry_m7459,
	Enumerator_get_Key_m7460,
	Enumerator_get_Value_m7461,
	Enumerator_get_Current_m7462,
	HashKeys__ctor_m7463,
	HashKeys_get_Count_m7464,
	HashKeys_get_IsSynchronized_m7465,
	HashKeys_get_SyncRoot_m7466,
	HashKeys_CopyTo_m7467,
	HashKeys_GetEnumerator_m7468,
	HashValues__ctor_m7469,
	HashValues_get_Count_m7470,
	HashValues_get_IsSynchronized_m7471,
	HashValues_get_SyncRoot_m7472,
	HashValues_CopyTo_m7473,
	HashValues_GetEnumerator_m7474,
	SyncHashtable__ctor_m7475,
	SyncHashtable__ctor_m7476,
	SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m7477,
	SyncHashtable_GetObjectData_m7478,
	SyncHashtable_get_Count_m7479,
	SyncHashtable_get_IsSynchronized_m7480,
	SyncHashtable_get_SyncRoot_m7481,
	SyncHashtable_get_Keys_m7482,
	SyncHashtable_get_Values_m7483,
	SyncHashtable_get_Item_m7484,
	SyncHashtable_set_Item_m7485,
	SyncHashtable_CopyTo_m7486,
	SyncHashtable_Add_m7487,
	SyncHashtable_Clear_m7488,
	SyncHashtable_Contains_m7489,
	SyncHashtable_GetEnumerator_m7490,
	SyncHashtable_Remove_m7491,
	SyncHashtable_ContainsKey_m7492,
	SyncHashtable_Clone_m7493,
	Hashtable__ctor_m4954,
	Hashtable__ctor_m7494,
	Hashtable__ctor_m7495,
	Hashtable__ctor_m4957,
	Hashtable__ctor_m7496,
	Hashtable__ctor_m4816,
	Hashtable__ctor_m7497,
	Hashtable__ctor_m4817,
	Hashtable__ctor_m4854,
	Hashtable__ctor_m7498,
	Hashtable__ctor_m4824,
	Hashtable__ctor_m7499,
	Hashtable__cctor_m7500,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m7501,
	Hashtable_set_comparer_m7502,
	Hashtable_set_hcp_m7503,
	Hashtable_get_Count_m7504,
	Hashtable_get_IsSynchronized_m7505,
	Hashtable_get_SyncRoot_m7506,
	Hashtable_get_Keys_m7507,
	Hashtable_get_Values_m7508,
	Hashtable_get_Item_m7509,
	Hashtable_set_Item_m7510,
	Hashtable_CopyTo_m7511,
	Hashtable_Add_m7512,
	Hashtable_Clear_m7513,
	Hashtable_Contains_m7514,
	Hashtable_GetEnumerator_m7515,
	Hashtable_Remove_m7516,
	Hashtable_ContainsKey_m7517,
	Hashtable_Clone_m7518,
	Hashtable_GetObjectData_m7519,
	Hashtable_OnDeserialization_m7520,
	Hashtable_Synchronized_m7521,
	Hashtable_GetHash_m7522,
	Hashtable_KeyEquals_m7523,
	Hashtable_AdjustThreshold_m7524,
	Hashtable_SetTable_m7525,
	Hashtable_Find_m7526,
	Hashtable_Rehash_m7527,
	Hashtable_PutImpl_m7528,
	Hashtable_CopyToArray_m7529,
	Hashtable_TestPrime_m7530,
	Hashtable_CalcPrime_m7531,
	Hashtable_ToPrime_m7532,
	Enumerator__ctor_m7533,
	Enumerator__cctor_m7534,
	Enumerator_Reset_m7535,
	Enumerator_MoveNext_m7536,
	Enumerator_get_Entry_m7537,
	Enumerator_get_Key_m7538,
	Enumerator_get_Value_m7539,
	Enumerator_get_Current_m7540,
	Enumerator_Clone_m7541,
	SortedList__ctor_m7542,
	SortedList__ctor_m4853,
	SortedList__ctor_m7543,
	SortedList__ctor_m7544,
	SortedList__cctor_m7545,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m7546,
	SortedList_get_Count_m7547,
	SortedList_get_IsSynchronized_m7548,
	SortedList_get_SyncRoot_m7549,
	SortedList_get_IsFixedSize_m7550,
	SortedList_get_IsReadOnly_m7551,
	SortedList_get_Item_m7552,
	SortedList_set_Item_m7553,
	SortedList_get_Capacity_m7554,
	SortedList_set_Capacity_m7555,
	SortedList_Add_m7556,
	SortedList_Contains_m7557,
	SortedList_GetEnumerator_m7558,
	SortedList_Remove_m7559,
	SortedList_CopyTo_m7560,
	SortedList_Clone_m7561,
	SortedList_RemoveAt_m7562,
	SortedList_IndexOfKey_m7563,
	SortedList_ContainsKey_m7564,
	SortedList_GetByIndex_m7565,
	SortedList_EnsureCapacity_m7566,
	SortedList_PutImpl_m7567,
	SortedList_GetImpl_m7568,
	SortedList_InitTable_m7569,
	SortedList_Find_m7570,
	Enumerator__ctor_m7571,
	Enumerator_Clone_m7572,
	Enumerator_get_Current_m7573,
	Enumerator_MoveNext_m7574,
	Enumerator_Reset_m7575,
	Stack__ctor_m2244,
	Stack__ctor_m7576,
	Stack__ctor_m7577,
	Stack_Resize_m7578,
	Stack_get_Count_m7579,
	Stack_get_IsSynchronized_m7580,
	Stack_get_SyncRoot_m7581,
	Stack_Clear_m7582,
	Stack_Clone_m7583,
	Stack_CopyTo_m7584,
	Stack_GetEnumerator_m7585,
	Stack_Peek_m7586,
	Stack_Pop_m7587,
	Stack_Push_m7588,
	DebuggableAttribute__ctor_m7589,
	DebuggerDisplayAttribute__ctor_m7590,
	DebuggerDisplayAttribute_set_Name_m7591,
	DebuggerStepThroughAttribute__ctor_m7592,
	DebuggerTypeProxyAttribute__ctor_m7593,
	StackFrame__ctor_m7594,
	StackFrame__ctor_m7595,
	StackFrame_get_frame_info_m7596,
	StackFrame_GetFileLineNumber_m7597,
	StackFrame_GetFileName_m7598,
	StackFrame_GetSecureFileName_m7599,
	StackFrame_GetILOffset_m7600,
	StackFrame_GetMethod_m7601,
	StackFrame_GetNativeOffset_m7602,
	StackFrame_GetInternalMethodName_m7603,
	StackFrame_ToString_m7604,
	StackTrace__ctor_m7605,
	StackTrace__ctor_m2202,
	StackTrace__ctor_m7606,
	StackTrace__ctor_m7607,
	StackTrace__ctor_m7608,
	StackTrace_init_frames_m7609,
	StackTrace_get_trace_m7610,
	StackTrace_get_FrameCount_m7611,
	StackTrace_GetFrame_m7612,
	StackTrace_ToString_m7613,
	Calendar__ctor_m7614,
	Calendar_Clone_m7615,
	Calendar_CheckReadOnly_m7616,
	Calendar_get_EraNames_m7617,
	CCMath_div_m7618,
	CCMath_mod_m7619,
	CCMath_div_mod_m7620,
	CCFixed_FromDateTime_m7621,
	CCFixed_day_of_week_m7622,
	CCGregorianCalendar_is_leap_year_m7623,
	CCGregorianCalendar_fixed_from_dmy_m7624,
	CCGregorianCalendar_year_from_fixed_m7625,
	CCGregorianCalendar_my_from_fixed_m7626,
	CCGregorianCalendar_dmy_from_fixed_m7627,
	CCGregorianCalendar_month_from_fixed_m7628,
	CCGregorianCalendar_day_from_fixed_m7629,
	CCGregorianCalendar_GetDayOfMonth_m7630,
	CCGregorianCalendar_GetMonth_m7631,
	CCGregorianCalendar_GetYear_m7632,
	CompareInfo__ctor_m7633,
	CompareInfo__ctor_m7634,
	CompareInfo__cctor_m7635,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7636,
	CompareInfo_get_UseManagedCollation_m7637,
	CompareInfo_construct_compareinfo_m7638,
	CompareInfo_free_internal_collator_m7639,
	CompareInfo_internal_compare_m7640,
	CompareInfo_assign_sortkey_m7641,
	CompareInfo_internal_index_m7642,
	CompareInfo_Finalize_m7643,
	CompareInfo_internal_compare_managed_m7644,
	CompareInfo_internal_compare_switch_m7645,
	CompareInfo_Compare_m7646,
	CompareInfo_Compare_m7647,
	CompareInfo_Compare_m7648,
	CompareInfo_Equals_m7649,
	CompareInfo_GetHashCode_m7650,
	CompareInfo_GetSortKey_m7651,
	CompareInfo_IndexOf_m7652,
	CompareInfo_internal_index_managed_m7653,
	CompareInfo_internal_index_switch_m7654,
	CompareInfo_IndexOf_m7655,
	CompareInfo_IsPrefix_m7656,
	CompareInfo_IsSuffix_m7657,
	CompareInfo_LastIndexOf_m7658,
	CompareInfo_LastIndexOf_m7659,
	CompareInfo_ToString_m7660,
	CompareInfo_get_LCID_m7661,
	CultureInfo__ctor_m7662,
	CultureInfo__ctor_m7663,
	CultureInfo__ctor_m7664,
	CultureInfo__ctor_m7665,
	CultureInfo__ctor_m7666,
	CultureInfo__cctor_m7667,
	CultureInfo_get_InvariantCulture_m4846,
	CultureInfo_get_CurrentCulture_m5869,
	CultureInfo_get_CurrentUICulture_m5870,
	CultureInfo_ConstructCurrentCulture_m7668,
	CultureInfo_ConstructCurrentUICulture_m7669,
	CultureInfo_get_LCID_m7670,
	CultureInfo_get_Name_m7671,
	CultureInfo_get_Parent_m7672,
	CultureInfo_get_TextInfo_m7673,
	CultureInfo_get_IcuName_m7674,
	CultureInfo_Clone_m7675,
	CultureInfo_Equals_m7676,
	CultureInfo_GetHashCode_m7677,
	CultureInfo_ToString_m7678,
	CultureInfo_get_CompareInfo_m7679,
	CultureInfo_get_IsNeutralCulture_m7680,
	CultureInfo_CheckNeutral_m7681,
	CultureInfo_get_NumberFormat_m7682,
	CultureInfo_set_NumberFormat_m7683,
	CultureInfo_get_DateTimeFormat_m7684,
	CultureInfo_set_DateTimeFormat_m7685,
	CultureInfo_get_IsReadOnly_m7686,
	CultureInfo_GetFormat_m7687,
	CultureInfo_Construct_m7688,
	CultureInfo_ConstructInternalLocaleFromName_m7689,
	CultureInfo_ConstructInternalLocaleFromLcid_m7690,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7691,
	CultureInfo_construct_internal_locale_from_lcid_m7692,
	CultureInfo_construct_internal_locale_from_name_m7693,
	CultureInfo_construct_internal_locale_from_current_locale_m7694,
	CultureInfo_construct_datetime_format_m7695,
	CultureInfo_construct_number_format_m7696,
	CultureInfo_ConstructInvariant_m7697,
	CultureInfo_CreateTextInfo_m7698,
	CultureInfo_CreateCulture_m7699,
	DateTimeFormatInfo__ctor_m7700,
	DateTimeFormatInfo__ctor_m7701,
	DateTimeFormatInfo__cctor_m7702,
	DateTimeFormatInfo_GetInstance_m7703,
	DateTimeFormatInfo_get_IsReadOnly_m7704,
	DateTimeFormatInfo_ReadOnly_m7705,
	DateTimeFormatInfo_Clone_m7706,
	DateTimeFormatInfo_GetFormat_m7707,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m7708,
	DateTimeFormatInfo_GetEraName_m7709,
	DateTimeFormatInfo_GetMonthName_m7710,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7711,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7712,
	DateTimeFormatInfo_get_RawDayNames_m7713,
	DateTimeFormatInfo_get_RawMonthNames_m7714,
	DateTimeFormatInfo_get_AMDesignator_m7715,
	DateTimeFormatInfo_get_PMDesignator_m7716,
	DateTimeFormatInfo_get_DateSeparator_m7717,
	DateTimeFormatInfo_get_TimeSeparator_m7718,
	DateTimeFormatInfo_get_LongDatePattern_m7719,
	DateTimeFormatInfo_get_ShortDatePattern_m7720,
	DateTimeFormatInfo_get_ShortTimePattern_m7721,
	DateTimeFormatInfo_get_LongTimePattern_m7722,
	DateTimeFormatInfo_get_MonthDayPattern_m7723,
	DateTimeFormatInfo_get_YearMonthPattern_m7724,
	DateTimeFormatInfo_get_FullDateTimePattern_m7725,
	DateTimeFormatInfo_get_CurrentInfo_m7726,
	DateTimeFormatInfo_get_InvariantInfo_m7727,
	DateTimeFormatInfo_get_Calendar_m7728,
	DateTimeFormatInfo_set_Calendar_m7729,
	DateTimeFormatInfo_get_RFC1123Pattern_m7730,
	DateTimeFormatInfo_get_RoundtripPattern_m7731,
	DateTimeFormatInfo_get_SortableDateTimePattern_m7732,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7733,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7734,
	DateTimeFormatInfo_FillAllDateTimePatterns_m7735,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m7736,
	DateTimeFormatInfo_GetDayName_m7737,
	DateTimeFormatInfo_GetAbbreviatedDayName_m7738,
	DateTimeFormatInfo_FillInvariantPatterns_m7739,
	DateTimeFormatInfo_PopulateCombinedList_m7740,
	DaylightTime__ctor_m7741,
	DaylightTime_get_Start_m7742,
	DaylightTime_get_End_m7743,
	DaylightTime_get_Delta_m7744,
	GregorianCalendar__ctor_m7745,
	GregorianCalendar__ctor_m7746,
	GregorianCalendar_get_Eras_m7747,
	GregorianCalendar_set_CalendarType_m7748,
	GregorianCalendar_GetDayOfMonth_m7749,
	GregorianCalendar_GetDayOfWeek_m7750,
	GregorianCalendar_GetEra_m7751,
	GregorianCalendar_GetMonth_m7752,
	GregorianCalendar_GetYear_m7753,
	NumberFormatInfo__ctor_m7754,
	NumberFormatInfo__ctor_m7755,
	NumberFormatInfo__ctor_m7756,
	NumberFormatInfo__cctor_m7757,
	NumberFormatInfo_get_CurrencyDecimalDigits_m7758,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m7759,
	NumberFormatInfo_get_CurrencyGroupSeparator_m7760,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m7761,
	NumberFormatInfo_get_CurrencyNegativePattern_m7762,
	NumberFormatInfo_get_CurrencyPositivePattern_m7763,
	NumberFormatInfo_get_CurrencySymbol_m7764,
	NumberFormatInfo_get_CurrentInfo_m7765,
	NumberFormatInfo_get_InvariantInfo_m7766,
	NumberFormatInfo_get_NaNSymbol_m7767,
	NumberFormatInfo_get_NegativeInfinitySymbol_m7768,
	NumberFormatInfo_get_NegativeSign_m7769,
	NumberFormatInfo_get_NumberDecimalDigits_m7770,
	NumberFormatInfo_get_NumberDecimalSeparator_m7771,
	NumberFormatInfo_get_NumberGroupSeparator_m7772,
	NumberFormatInfo_get_RawNumberGroupSizes_m7773,
	NumberFormatInfo_get_NumberNegativePattern_m7774,
	NumberFormatInfo_set_NumberNegativePattern_m7775,
	NumberFormatInfo_get_PercentDecimalDigits_m7776,
	NumberFormatInfo_get_PercentDecimalSeparator_m7777,
	NumberFormatInfo_get_PercentGroupSeparator_m7778,
	NumberFormatInfo_get_RawPercentGroupSizes_m7779,
	NumberFormatInfo_get_PercentNegativePattern_m7780,
	NumberFormatInfo_get_PercentPositivePattern_m7781,
	NumberFormatInfo_get_PercentSymbol_m7782,
	NumberFormatInfo_get_PerMilleSymbol_m7783,
	NumberFormatInfo_get_PositiveInfinitySymbol_m7784,
	NumberFormatInfo_get_PositiveSign_m7785,
	NumberFormatInfo_GetFormat_m7786,
	NumberFormatInfo_Clone_m7787,
	NumberFormatInfo_GetInstance_m7788,
	TextInfo__ctor_m7789,
	TextInfo__ctor_m7790,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7791,
	TextInfo_get_ListSeparator_m7792,
	TextInfo_get_CultureName_m7793,
	TextInfo_Equals_m7794,
	TextInfo_GetHashCode_m7795,
	TextInfo_ToString_m7796,
	TextInfo_ToLower_m7797,
	TextInfo_ToUpper_m7798,
	TextInfo_ToLower_m7799,
	TextInfo_Clone_m7800,
	IsolatedStorageException__ctor_m7801,
	IsolatedStorageException__ctor_m7802,
	IsolatedStorageException__ctor_m7803,
	BinaryReader__ctor_m7804,
	BinaryReader__ctor_m7805,
	BinaryReader_System_IDisposable_Dispose_m7806,
	BinaryReader_get_BaseStream_m7807,
	BinaryReader_Close_m7808,
	BinaryReader_Dispose_m7809,
	BinaryReader_FillBuffer_m7810,
	BinaryReader_Read_m7811,
	BinaryReader_Read_m7812,
	BinaryReader_Read_m7813,
	BinaryReader_ReadCharBytes_m7814,
	BinaryReader_Read7BitEncodedInt_m7815,
	BinaryReader_ReadBoolean_m7816,
	BinaryReader_ReadByte_m7817,
	BinaryReader_ReadBytes_m7818,
	BinaryReader_ReadChar_m7819,
	BinaryReader_ReadDecimal_m7820,
	BinaryReader_ReadDouble_m7821,
	BinaryReader_ReadInt16_m7822,
	BinaryReader_ReadInt32_m7823,
	BinaryReader_ReadInt64_m7824,
	BinaryReader_ReadSByte_m7825,
	BinaryReader_ReadString_m7826,
	BinaryReader_ReadSingle_m7827,
	BinaryReader_ReadUInt16_m7828,
	BinaryReader_ReadUInt32_m7829,
	BinaryReader_ReadUInt64_m7830,
	BinaryReader_CheckBuffer_m7831,
	Directory_CreateDirectory_m5855,
	Directory_CreateDirectoriesInternal_m7832,
	Directory_Exists_m5854,
	Directory_GetCurrentDirectory_m7833,
	Directory_GetFiles_m5857,
	Directory_GetFileSystemEntries_m7834,
	DirectoryInfo__ctor_m7835,
	DirectoryInfo__ctor_m7836,
	DirectoryInfo__ctor_m7837,
	DirectoryInfo_Initialize_m7838,
	DirectoryInfo_get_Exists_m7839,
	DirectoryInfo_get_Parent_m7840,
	DirectoryInfo_Create_m7841,
	DirectoryInfo_ToString_m7842,
	DirectoryNotFoundException__ctor_m7843,
	DirectoryNotFoundException__ctor_m7844,
	DirectoryNotFoundException__ctor_m7845,
	EndOfStreamException__ctor_m7846,
	EndOfStreamException__ctor_m7847,
	File_Delete_m7848,
	File_Exists_m7849,
	File_Open_m7850,
	File_OpenRead_m5853,
	File_OpenText_m7851,
	FileNotFoundException__ctor_m7852,
	FileNotFoundException__ctor_m7853,
	FileNotFoundException__ctor_m7854,
	FileNotFoundException_get_Message_m7855,
	FileNotFoundException_GetObjectData_m7856,
	FileNotFoundException_ToString_m7857,
	ReadDelegate__ctor_m7858,
	ReadDelegate_Invoke_m7859,
	ReadDelegate_BeginInvoke_m7860,
	ReadDelegate_EndInvoke_m7861,
	WriteDelegate__ctor_m7862,
	WriteDelegate_Invoke_m7863,
	WriteDelegate_BeginInvoke_m7864,
	WriteDelegate_EndInvoke_m7865,
	FileStream__ctor_m7866,
	FileStream__ctor_m7867,
	FileStream__ctor_m7868,
	FileStream__ctor_m7869,
	FileStream__ctor_m7870,
	FileStream_get_CanRead_m7871,
	FileStream_get_CanWrite_m7872,
	FileStream_get_CanSeek_m7873,
	FileStream_get_Length_m7874,
	FileStream_get_Position_m7875,
	FileStream_set_Position_m7876,
	FileStream_ReadByte_m7877,
	FileStream_WriteByte_m7878,
	FileStream_Read_m7879,
	FileStream_ReadInternal_m7880,
	FileStream_BeginRead_m7881,
	FileStream_EndRead_m7882,
	FileStream_Write_m7883,
	FileStream_WriteInternal_m7884,
	FileStream_BeginWrite_m7885,
	FileStream_EndWrite_m7886,
	FileStream_Seek_m7887,
	FileStream_SetLength_m7888,
	FileStream_Flush_m7889,
	FileStream_Finalize_m7890,
	FileStream_Dispose_m7891,
	FileStream_ReadSegment_m7892,
	FileStream_WriteSegment_m7893,
	FileStream_FlushBuffer_m7894,
	FileStream_FlushBuffer_m7895,
	FileStream_FlushBufferIfDirty_m7896,
	FileStream_RefillBuffer_m7897,
	FileStream_ReadData_m7898,
	FileStream_InitBuffer_m7899,
	FileStream_GetSecureFileName_m7900,
	FileStream_GetSecureFileName_m7901,
	FileStreamAsyncResult__ctor_m7902,
	FileStreamAsyncResult_CBWrapper_m7903,
	FileStreamAsyncResult_get_AsyncState_m7904,
	FileStreamAsyncResult_get_AsyncWaitHandle_m7905,
	FileStreamAsyncResult_get_IsCompleted_m7906,
	FileSystemInfo__ctor_m7907,
	FileSystemInfo__ctor_m7908,
	FileSystemInfo_GetObjectData_m7909,
	FileSystemInfo_get_FullName_m7910,
	FileSystemInfo_Refresh_m7911,
	FileSystemInfo_InternalRefresh_m7912,
	FileSystemInfo_CheckPath_m7913,
	IOException__ctor_m7914,
	IOException__ctor_m7915,
	IOException__ctor_m5883,
	IOException__ctor_m7916,
	IOException__ctor_m7917,
	MemoryStream__ctor_m5884,
	MemoryStream__ctor_m5889,
	MemoryStream__ctor_m5890,
	MemoryStream_InternalConstructor_m7918,
	MemoryStream_CheckIfClosedThrowDisposed_m7919,
	MemoryStream_get_CanRead_m7920,
	MemoryStream_get_CanSeek_m7921,
	MemoryStream_get_CanWrite_m7922,
	MemoryStream_set_Capacity_m7923,
	MemoryStream_get_Length_m7924,
	MemoryStream_get_Position_m7925,
	MemoryStream_set_Position_m7926,
	MemoryStream_Dispose_m7927,
	MemoryStream_Flush_m7928,
	MemoryStream_Read_m7929,
	MemoryStream_ReadByte_m7930,
	MemoryStream_Seek_m7931,
	MemoryStream_CalculateNewCapacity_m7932,
	MemoryStream_Expand_m7933,
	MemoryStream_SetLength_m7934,
	MemoryStream_ToArray_m7935,
	MemoryStream_Write_m7936,
	MemoryStream_WriteByte_m7937,
	MonoIO__cctor_m7938,
	MonoIO_GetException_m7939,
	MonoIO_GetException_m7940,
	MonoIO_CreateDirectory_m7941,
	MonoIO_GetFileSystemEntries_m7942,
	MonoIO_GetCurrentDirectory_m7943,
	MonoIO_DeleteFile_m7944,
	MonoIO_GetFileAttributes_m7945,
	MonoIO_GetFileType_m7946,
	MonoIO_ExistsFile_m7947,
	MonoIO_ExistsDirectory_m7948,
	MonoIO_GetFileStat_m7949,
	MonoIO_Open_m7950,
	MonoIO_Close_m7951,
	MonoIO_Read_m7952,
	MonoIO_Write_m7953,
	MonoIO_Seek_m7954,
	MonoIO_GetLength_m7955,
	MonoIO_SetLength_m7956,
	MonoIO_get_ConsoleOutput_m7957,
	MonoIO_get_ConsoleInput_m7958,
	MonoIO_get_ConsoleError_m7959,
	MonoIO_get_VolumeSeparatorChar_m7960,
	MonoIO_get_DirectorySeparatorChar_m7961,
	MonoIO_get_AltDirectorySeparatorChar_m7962,
	MonoIO_get_PathSeparator_m7963,
	Path__cctor_m7964,
	Path_Combine_m5856,
	Path_CleanPath_m7965,
	Path_GetDirectoryName_m7966,
	Path_GetFileName_m7967,
	Path_GetFullPath_m7968,
	Path_WindowsDriveAdjustment_m7969,
	Path_InsecureGetFullPath_m7970,
	Path_IsDsc_m7971,
	Path_GetPathRoot_m7972,
	Path_IsPathRooted_m7973,
	Path_GetInvalidPathChars_m7974,
	Path_GetServerAndShare_m7975,
	Path_SameRoot_m7976,
	Path_CanonicalizePath_m7977,
	PathTooLongException__ctor_m7978,
	PathTooLongException__ctor_m7979,
	PathTooLongException__ctor_m7980,
	SearchPattern__cctor_m7981,
	Stream__ctor_m5885,
	Stream__cctor_m7982,
	Stream_Dispose_m7983,
	Stream_Dispose_m5888,
	Stream_Close_m5887,
	Stream_ReadByte_m7984,
	Stream_WriteByte_m7985,
	Stream_BeginRead_m7986,
	Stream_BeginWrite_m7987,
	Stream_EndRead_m7988,
	Stream_EndWrite_m7989,
	NullStream__ctor_m7990,
	NullStream_get_CanRead_m7991,
	NullStream_get_CanSeek_m7992,
	NullStream_get_CanWrite_m7993,
	NullStream_get_Length_m7994,
	NullStream_get_Position_m7995,
	NullStream_set_Position_m7996,
	NullStream_Flush_m7997,
	NullStream_Read_m7998,
	NullStream_ReadByte_m7999,
	NullStream_Seek_m8000,
	NullStream_SetLength_m8001,
	NullStream_Write_m8002,
	NullStream_WriteByte_m8003,
	StreamAsyncResult__ctor_m8004,
	StreamAsyncResult_SetComplete_m8005,
	StreamAsyncResult_SetComplete_m8006,
	StreamAsyncResult_get_AsyncState_m8007,
	StreamAsyncResult_get_AsyncWaitHandle_m8008,
	StreamAsyncResult_get_IsCompleted_m8009,
	StreamAsyncResult_get_Exception_m8010,
	StreamAsyncResult_get_NBytes_m8011,
	StreamAsyncResult_get_Done_m8012,
	StreamAsyncResult_set_Done_m8013,
	NullStreamReader__ctor_m8014,
	NullStreamReader_Peek_m8015,
	NullStreamReader_Read_m8016,
	NullStreamReader_Read_m8017,
	NullStreamReader_ReadLine_m8018,
	NullStreamReader_ReadToEnd_m8019,
	StreamReader__ctor_m8020,
	StreamReader__ctor_m8021,
	StreamReader__ctor_m8022,
	StreamReader__ctor_m8023,
	StreamReader__ctor_m8024,
	StreamReader__cctor_m8025,
	StreamReader_Initialize_m8026,
	StreamReader_Dispose_m8027,
	StreamReader_DoChecks_m8028,
	StreamReader_ReadBuffer_m8029,
	StreamReader_Peek_m8030,
	StreamReader_Read_m8031,
	StreamReader_Read_m8032,
	StreamReader_FindNextEOL_m8033,
	StreamReader_ReadLine_m8034,
	StreamReader_ReadToEnd_m8035,
	StreamWriter__ctor_m8036,
	StreamWriter__ctor_m8037,
	StreamWriter__cctor_m8038,
	StreamWriter_Initialize_m8039,
	StreamWriter_set_AutoFlush_m8040,
	StreamWriter_Dispose_m8041,
	StreamWriter_Flush_m8042,
	StreamWriter_FlushBytes_m8043,
	StreamWriter_Decode_m8044,
	StreamWriter_Write_m8045,
	StreamWriter_LowLevelWrite_m8046,
	StreamWriter_LowLevelWrite_m8047,
	StreamWriter_Write_m8048,
	StreamWriter_Write_m8049,
	StreamWriter_Write_m8050,
	StreamWriter_Close_m8051,
	StreamWriter_Finalize_m8052,
	StringReader__ctor_m8053,
	StringReader_Dispose_m8054,
	StringReader_Peek_m8055,
	StringReader_Read_m8056,
	StringReader_Read_m8057,
	StringReader_ReadLine_m8058,
	StringReader_ReadToEnd_m8059,
	StringReader_CheckObjectDisposedException_m8060,
	NullTextReader__ctor_m8061,
	NullTextReader_ReadLine_m8062,
	TextReader__ctor_m8063,
	TextReader__cctor_m8064,
	TextReader_Dispose_m8065,
	TextReader_Dispose_m8066,
	TextReader_Peek_m8067,
	TextReader_Read_m8068,
	TextReader_Read_m8069,
	TextReader_ReadLine_m8070,
	TextReader_ReadToEnd_m8071,
	TextReader_Synchronized_m8072,
	SynchronizedReader__ctor_m8073,
	SynchronizedReader_Peek_m8074,
	SynchronizedReader_ReadLine_m8075,
	SynchronizedReader_ReadToEnd_m8076,
	SynchronizedReader_Read_m8077,
	SynchronizedReader_Read_m8078,
	NullTextWriter__ctor_m8079,
	NullTextWriter_Write_m8080,
	NullTextWriter_Write_m8081,
	NullTextWriter_Write_m8082,
	TextWriter__ctor_m8083,
	TextWriter__cctor_m8084,
	TextWriter_Close_m8085,
	TextWriter_Dispose_m8086,
	TextWriter_Dispose_m8087,
	TextWriter_Flush_m8088,
	TextWriter_Synchronized_m8089,
	TextWriter_Write_m8090,
	TextWriter_Write_m8091,
	TextWriter_Write_m8092,
	TextWriter_Write_m8093,
	TextWriter_WriteLine_m8094,
	TextWriter_WriteLine_m8095,
	SynchronizedWriter__ctor_m8096,
	SynchronizedWriter_Close_m8097,
	SynchronizedWriter_Flush_m8098,
	SynchronizedWriter_Write_m8099,
	SynchronizedWriter_Write_m8100,
	SynchronizedWriter_Write_m8101,
	SynchronizedWriter_Write_m8102,
	SynchronizedWriter_WriteLine_m8103,
	SynchronizedWriter_WriteLine_m8104,
	UnexceptionalStreamReader__ctor_m8105,
	UnexceptionalStreamReader__cctor_m8106,
	UnexceptionalStreamReader_Peek_m8107,
	UnexceptionalStreamReader_Read_m8108,
	UnexceptionalStreamReader_Read_m8109,
	UnexceptionalStreamReader_CheckEOL_m8110,
	UnexceptionalStreamReader_ReadLine_m8111,
	UnexceptionalStreamReader_ReadToEnd_m8112,
	UnexceptionalStreamWriter__ctor_m8113,
	UnexceptionalStreamWriter_Flush_m8114,
	UnexceptionalStreamWriter_Write_m8115,
	UnexceptionalStreamWriter_Write_m8116,
	UnexceptionalStreamWriter_Write_m8117,
	UnexceptionalStreamWriter_Write_m8118,
	UnmanagedMemoryStream_get_CanRead_m8119,
	UnmanagedMemoryStream_get_CanSeek_m8120,
	UnmanagedMemoryStream_get_CanWrite_m8121,
	UnmanagedMemoryStream_get_Length_m8122,
	UnmanagedMemoryStream_get_Position_m8123,
	UnmanagedMemoryStream_set_Position_m8124,
	UnmanagedMemoryStream_Read_m8125,
	UnmanagedMemoryStream_ReadByte_m8126,
	UnmanagedMemoryStream_Seek_m8127,
	UnmanagedMemoryStream_SetLength_m8128,
	UnmanagedMemoryStream_Flush_m8129,
	UnmanagedMemoryStream_Dispose_m8130,
	UnmanagedMemoryStream_Write_m8131,
	UnmanagedMemoryStream_WriteByte_m8132,
	AssemblyBuilder_get_Location_m8133,
	AssemblyBuilder_GetModulesInternal_m8134,
	AssemblyBuilder_GetTypes_m8135,
	AssemblyBuilder_get_IsCompilerContext_m8136,
	AssemblyBuilder_not_supported_m8137,
	AssemblyBuilder_UnprotectedGetName_m8138,
	ConstructorBuilder__ctor_m8139,
	ConstructorBuilder_get_CallingConvention_m8140,
	ConstructorBuilder_get_TypeBuilder_m8141,
	ConstructorBuilder_GetParameters_m8142,
	ConstructorBuilder_GetParametersInternal_m8143,
	ConstructorBuilder_GetParameterCount_m8144,
	ConstructorBuilder_Invoke_m8145,
	ConstructorBuilder_Invoke_m8146,
	ConstructorBuilder_get_MethodHandle_m8147,
	ConstructorBuilder_get_Attributes_m8148,
	ConstructorBuilder_get_ReflectedType_m8149,
	ConstructorBuilder_get_DeclaringType_m8150,
	ConstructorBuilder_get_Name_m8151,
	ConstructorBuilder_IsDefined_m8152,
	ConstructorBuilder_GetCustomAttributes_m8153,
	ConstructorBuilder_GetCustomAttributes_m8154,
	ConstructorBuilder_GetILGenerator_m8155,
	ConstructorBuilder_GetILGenerator_m8156,
	ConstructorBuilder_GetToken_m8157,
	ConstructorBuilder_get_Module_m8158,
	ConstructorBuilder_ToString_m8159,
	ConstructorBuilder_fixup_m8160,
	ConstructorBuilder_get_next_table_index_m8161,
	ConstructorBuilder_get_IsCompilerContext_m8162,
	ConstructorBuilder_not_supported_m8163,
	ConstructorBuilder_not_created_m8164,
	EnumBuilder_get_Assembly_m8165,
	EnumBuilder_get_AssemblyQualifiedName_m8166,
	EnumBuilder_get_BaseType_m8167,
	EnumBuilder_get_DeclaringType_m8168,
	EnumBuilder_get_FullName_m8169,
	EnumBuilder_get_Module_m8170,
	EnumBuilder_get_Name_m8171,
	EnumBuilder_get_Namespace_m8172,
	EnumBuilder_get_ReflectedType_m8173,
	EnumBuilder_get_TypeHandle_m8174,
	EnumBuilder_get_UnderlyingSystemType_m8175,
	EnumBuilder_GetAttributeFlagsImpl_m8176,
	EnumBuilder_GetConstructorImpl_m8177,
	EnumBuilder_GetConstructors_m8178,
	EnumBuilder_GetCustomAttributes_m8179,
	EnumBuilder_GetCustomAttributes_m8180,
	EnumBuilder_GetElementType_m8181,
	EnumBuilder_GetEvent_m8182,
	EnumBuilder_GetField_m8183,
	EnumBuilder_GetFields_m8184,
	EnumBuilder_GetInterfaces_m8185,
	EnumBuilder_GetMethodImpl_m8186,
	EnumBuilder_GetMethods_m8187,
	EnumBuilder_GetPropertyImpl_m8188,
	EnumBuilder_HasElementTypeImpl_m8189,
	EnumBuilder_InvokeMember_m8190,
	EnumBuilder_IsArrayImpl_m8191,
	EnumBuilder_IsByRefImpl_m8192,
	EnumBuilder_IsPointerImpl_m8193,
	EnumBuilder_IsPrimitiveImpl_m8194,
	EnumBuilder_IsValueTypeImpl_m8195,
	EnumBuilder_IsDefined_m8196,
	EnumBuilder_CreateNotSupportedException_m8197,
	FieldBuilder_get_Attributes_m8198,
	FieldBuilder_get_DeclaringType_m8199,
	FieldBuilder_get_FieldHandle_m8200,
	FieldBuilder_get_FieldType_m8201,
	FieldBuilder_get_Name_m8202,
	FieldBuilder_get_ReflectedType_m8203,
	FieldBuilder_GetCustomAttributes_m8204,
	FieldBuilder_GetCustomAttributes_m8205,
	FieldBuilder_GetValue_m8206,
	FieldBuilder_IsDefined_m8207,
	FieldBuilder_GetFieldOffset_m8208,
	FieldBuilder_SetValue_m8209,
	FieldBuilder_get_UMarshal_m8210,
	FieldBuilder_CreateNotSupportedException_m8211,
	FieldBuilder_get_Module_m8212,
	GenericTypeParameterBuilder_IsSubclassOf_m8213,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m8214,
	GenericTypeParameterBuilder_GetConstructorImpl_m8215,
	GenericTypeParameterBuilder_GetConstructors_m8216,
	GenericTypeParameterBuilder_GetEvent_m8217,
	GenericTypeParameterBuilder_GetField_m8218,
	GenericTypeParameterBuilder_GetFields_m8219,
	GenericTypeParameterBuilder_GetInterfaces_m8220,
	GenericTypeParameterBuilder_GetMethods_m8221,
	GenericTypeParameterBuilder_GetMethodImpl_m8222,
	GenericTypeParameterBuilder_GetPropertyImpl_m8223,
	GenericTypeParameterBuilder_HasElementTypeImpl_m8224,
	GenericTypeParameterBuilder_IsAssignableFrom_m8225,
	GenericTypeParameterBuilder_IsInstanceOfType_m8226,
	GenericTypeParameterBuilder_IsArrayImpl_m8227,
	GenericTypeParameterBuilder_IsByRefImpl_m8228,
	GenericTypeParameterBuilder_IsPointerImpl_m8229,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m8230,
	GenericTypeParameterBuilder_IsValueTypeImpl_m8231,
	GenericTypeParameterBuilder_InvokeMember_m8232,
	GenericTypeParameterBuilder_GetElementType_m8233,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m8234,
	GenericTypeParameterBuilder_get_Assembly_m8235,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m8236,
	GenericTypeParameterBuilder_get_BaseType_m8237,
	GenericTypeParameterBuilder_get_FullName_m8238,
	GenericTypeParameterBuilder_IsDefined_m8239,
	GenericTypeParameterBuilder_GetCustomAttributes_m8240,
	GenericTypeParameterBuilder_GetCustomAttributes_m8241,
	GenericTypeParameterBuilder_get_Name_m8242,
	GenericTypeParameterBuilder_get_Namespace_m8243,
	GenericTypeParameterBuilder_get_Module_m8244,
	GenericTypeParameterBuilder_get_DeclaringType_m8245,
	GenericTypeParameterBuilder_get_ReflectedType_m8246,
	GenericTypeParameterBuilder_get_TypeHandle_m8247,
	GenericTypeParameterBuilder_GetGenericArguments_m8248,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m8249,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m8250,
	GenericTypeParameterBuilder_get_IsGenericParameter_m8251,
	GenericTypeParameterBuilder_get_IsGenericType_m8252,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m8253,
	GenericTypeParameterBuilder_not_supported_m8254,
	GenericTypeParameterBuilder_ToString_m8255,
	GenericTypeParameterBuilder_Equals_m8256,
	GenericTypeParameterBuilder_GetHashCode_m8257,
	GenericTypeParameterBuilder_MakeGenericType_m8258,
	ILGenerator__ctor_m8259,
	ILGenerator__cctor_m8260,
	ILGenerator_add_token_fixup_m8261,
	ILGenerator_make_room_m8262,
	ILGenerator_emit_int_m8263,
	ILGenerator_ll_emit_m8264,
	ILGenerator_Emit_m8265,
	ILGenerator_Emit_m8266,
	ILGenerator_label_fixup_m8267,
	ILGenerator_Mono_GetCurrentOffset_m8268,
	MethodBuilder_get_ContainsGenericParameters_m8269,
	MethodBuilder_get_MethodHandle_m8270,
	MethodBuilder_get_ReturnType_m8271,
	MethodBuilder_get_ReflectedType_m8272,
	MethodBuilder_get_DeclaringType_m8273,
	MethodBuilder_get_Name_m8274,
	MethodBuilder_get_Attributes_m8275,
	MethodBuilder_get_CallingConvention_m8276,
	MethodBuilder_GetBaseDefinition_m8277,
	MethodBuilder_GetParameters_m8278,
	MethodBuilder_GetParameterCount_m8279,
	MethodBuilder_Invoke_m8280,
	MethodBuilder_IsDefined_m8281,
	MethodBuilder_GetCustomAttributes_m8282,
	MethodBuilder_GetCustomAttributes_m8283,
	MethodBuilder_check_override_m8284,
	MethodBuilder_fixup_m8285,
	MethodBuilder_ToString_m8286,
	MethodBuilder_Equals_m8287,
	MethodBuilder_GetHashCode_m8288,
	MethodBuilder_get_next_table_index_m8289,
	MethodBuilder_NotSupported_m8290,
	MethodBuilder_MakeGenericMethod_m8291,
	MethodBuilder_get_IsGenericMethodDefinition_m8292,
	MethodBuilder_get_IsGenericMethod_m8293,
	MethodBuilder_GetGenericArguments_m8294,
	MethodBuilder_get_Module_m8295,
	MethodToken__ctor_m8296,
	MethodToken__cctor_m8297,
	MethodToken_Equals_m8298,
	MethodToken_GetHashCode_m8299,
	MethodToken_get_Token_m8300,
	ModuleBuilder__cctor_m8301,
	ModuleBuilder_get_next_table_index_m8302,
	ModuleBuilder_GetTypes_m8303,
	ModuleBuilder_getToken_m8304,
	ModuleBuilder_GetToken_m8305,
	ModuleBuilder_RegisterToken_m8306,
	ModuleBuilder_GetTokenGenerator_m8307,
	ModuleBuilderTokenGenerator__ctor_m8308,
	ModuleBuilderTokenGenerator_GetToken_m8309,
	OpCode__ctor_m8310,
	OpCode_GetHashCode_m8311,
	OpCode_Equals_m8312,
	OpCode_ToString_m8313,
	OpCode_get_Name_m8314,
	OpCode_get_Size_m8315,
	OpCode_get_StackBehaviourPop_m8316,
	OpCode_get_StackBehaviourPush_m8317,
	OpCodeNames__cctor_m8318,
	OpCodes__cctor_m8319,
	ParameterBuilder_get_Attributes_m8320,
	ParameterBuilder_get_Name_m8321,
	ParameterBuilder_get_Position_m8322,
	TypeBuilder_GetAttributeFlagsImpl_m8323,
	TypeBuilder_setup_internal_class_m8324,
	TypeBuilder_create_generic_class_m8325,
	TypeBuilder_get_Assembly_m8326,
	TypeBuilder_get_AssemblyQualifiedName_m8327,
	TypeBuilder_get_BaseType_m8328,
	TypeBuilder_get_DeclaringType_m8329,
	TypeBuilder_get_UnderlyingSystemType_m8330,
	TypeBuilder_get_FullName_m8331,
	TypeBuilder_get_Module_m8332,
	TypeBuilder_get_Name_m8333,
	TypeBuilder_get_Namespace_m8334,
	TypeBuilder_get_ReflectedType_m8335,
	TypeBuilder_GetConstructorImpl_m8336,
	TypeBuilder_IsDefined_m8337,
	TypeBuilder_GetCustomAttributes_m8338,
	TypeBuilder_GetCustomAttributes_m8339,
	TypeBuilder_DefineConstructor_m8340,
	TypeBuilder_DefineConstructor_m8341,
	TypeBuilder_DefineDefaultConstructor_m8342,
	TypeBuilder_create_runtime_class_m8343,
	TypeBuilder_is_nested_in_m8344,
	TypeBuilder_has_ctor_method_m8345,
	TypeBuilder_CreateType_m8346,
	TypeBuilder_GetConstructors_m8347,
	TypeBuilder_GetConstructorsInternal_m8348,
	TypeBuilder_GetElementType_m8349,
	TypeBuilder_GetEvent_m8350,
	TypeBuilder_GetField_m8351,
	TypeBuilder_GetFields_m8352,
	TypeBuilder_GetInterfaces_m8353,
	TypeBuilder_GetMethodsByName_m8354,
	TypeBuilder_GetMethods_m8355,
	TypeBuilder_GetMethodImpl_m8356,
	TypeBuilder_GetPropertyImpl_m8357,
	TypeBuilder_HasElementTypeImpl_m8358,
	TypeBuilder_InvokeMember_m8359,
	TypeBuilder_IsArrayImpl_m8360,
	TypeBuilder_IsByRefImpl_m8361,
	TypeBuilder_IsPointerImpl_m8362,
	TypeBuilder_IsPrimitiveImpl_m8363,
	TypeBuilder_IsValueTypeImpl_m8364,
	TypeBuilder_MakeGenericType_m8365,
	TypeBuilder_get_TypeHandle_m8366,
	TypeBuilder_SetParent_m8367,
	TypeBuilder_get_next_table_index_m8368,
	TypeBuilder_get_IsCompilerContext_m8369,
	TypeBuilder_get_is_created_m8370,
	TypeBuilder_not_supported_m8371,
	TypeBuilder_check_not_created_m8372,
	TypeBuilder_check_created_m8373,
	TypeBuilder_ToString_m8374,
	TypeBuilder_IsAssignableFrom_m8375,
	TypeBuilder_IsSubclassOf_m8376,
	TypeBuilder_IsAssignableTo_m8377,
	TypeBuilder_GetGenericArguments_m8378,
	TypeBuilder_GetGenericTypeDefinition_m8379,
	TypeBuilder_get_ContainsGenericParameters_m8380,
	TypeBuilder_get_IsGenericParameter_m8381,
	TypeBuilder_get_IsGenericTypeDefinition_m8382,
	TypeBuilder_get_IsGenericType_m8383,
	UnmanagedMarshal_ToMarshalAsAttribute_m8384,
	AmbiguousMatchException__ctor_m8385,
	AmbiguousMatchException__ctor_m8386,
	AmbiguousMatchException__ctor_m8387,
	ResolveEventHolder__ctor_m8388,
	Assembly__ctor_m8389,
	Assembly_get_code_base_m8390,
	Assembly_get_fullname_m8391,
	Assembly_get_location_m8392,
	Assembly_GetCodeBase_m8393,
	Assembly_get_FullName_m8394,
	Assembly_get_Location_m8395,
	Assembly_IsDefined_m8396,
	Assembly_GetCustomAttributes_m8397,
	Assembly_GetManifestResourceInternal_m8398,
	Assembly_GetTypes_m8399,
	Assembly_GetTypes_m8400,
	Assembly_GetType_m8401,
	Assembly_GetType_m8402,
	Assembly_InternalGetType_m8403,
	Assembly_GetType_m8404,
	Assembly_FillName_m8405,
	Assembly_GetName_m8406,
	Assembly_GetName_m8407,
	Assembly_UnprotectedGetName_m8408,
	Assembly_ToString_m8409,
	Assembly_Load_m8410,
	Assembly_GetModule_m8411,
	Assembly_GetModulesInternal_m8412,
	Assembly_GetModules_m8413,
	Assembly_GetExecutingAssembly_m8414,
	AssemblyCompanyAttribute__ctor_m8415,
	AssemblyConfigurationAttribute__ctor_m8416,
	AssemblyCopyrightAttribute__ctor_m8417,
	AssemblyDefaultAliasAttribute__ctor_m8418,
	AssemblyDelaySignAttribute__ctor_m8419,
	AssemblyDescriptionAttribute__ctor_m8420,
	AssemblyFileVersionAttribute__ctor_m8421,
	AssemblyInformationalVersionAttribute__ctor_m8422,
	AssemblyKeyFileAttribute__ctor_m8423,
	AssemblyKeyNameAttribute__ctor_m8424,
	AssemblyName__ctor_m8425,
	AssemblyName__ctor_m8426,
	AssemblyName_get_Name_m8427,
	AssemblyName_get_Flags_m8428,
	AssemblyName_get_FullName_m8429,
	AssemblyName_get_Version_m8430,
	AssemblyName_set_Version_m8431,
	AssemblyName_ToString_m8432,
	AssemblyName_get_IsPublicKeyValid_m8433,
	AssemblyName_InternalGetPublicKeyToken_m8434,
	AssemblyName_ComputePublicKeyToken_m8435,
	AssemblyName_SetPublicKey_m8436,
	AssemblyName_SetPublicKeyToken_m8437,
	AssemblyName_GetObjectData_m8438,
	AssemblyName_Clone_m8439,
	AssemblyName_OnDeserialization_m8440,
	AssemblyProductAttribute__ctor_m8441,
	AssemblyTitleAttribute__ctor_m8442,
	AssemblyTrademarkAttribute__ctor_m8443,
	Default__ctor_m8444,
	Default_BindToMethod_m8445,
	Default_ReorderParameters_m8446,
	Default_IsArrayAssignable_m8447,
	Default_ChangeType_m8448,
	Default_ReorderArgumentArray_m8449,
	Default_check_type_m8450,
	Default_check_arguments_m8451,
	Default_SelectMethod_m8452,
	Default_SelectMethod_m8453,
	Default_GetBetterMethod_m8454,
	Default_CompareCloserType_m8455,
	Default_SelectProperty_m8456,
	Default_check_arguments_with_score_m8457,
	Default_check_type_with_score_m8458,
	Binder__ctor_m8459,
	Binder__cctor_m8460,
	Binder_get_DefaultBinder_m8461,
	Binder_ConvertArgs_m8462,
	Binder_GetDerivedLevel_m8463,
	Binder_FindMostDerivedMatch_m8464,
	ConstructorInfo__ctor_m8465,
	ConstructorInfo__cctor_m8466,
	ConstructorInfo_get_MemberType_m8467,
	ConstructorInfo_Invoke_m2233,
	CustomAttributeData__ctor_m8468,
	CustomAttributeData_get_Constructor_m8469,
	CustomAttributeData_get_ConstructorArguments_m8470,
	CustomAttributeData_get_NamedArguments_m8471,
	CustomAttributeData_GetCustomAttributes_m8472,
	CustomAttributeData_GetCustomAttributes_m8473,
	CustomAttributeData_GetCustomAttributes_m8474,
	CustomAttributeData_GetCustomAttributes_m8475,
	CustomAttributeData_ToString_m8476,
	CustomAttributeData_Equals_m8477,
	CustomAttributeData_GetHashCode_m8478,
	CustomAttributeNamedArgument_ToString_m8479,
	CustomAttributeNamedArgument_Equals_m8480,
	CustomAttributeNamedArgument_GetHashCode_m8481,
	CustomAttributeTypedArgument_ToString_m8482,
	CustomAttributeTypedArgument_Equals_m8483,
	CustomAttributeTypedArgument_GetHashCode_m8484,
	AddEventAdapter__ctor_m8485,
	AddEventAdapter_Invoke_m8486,
	AddEventAdapter_BeginInvoke_m8487,
	AddEventAdapter_EndInvoke_m8488,
	EventInfo__ctor_m8489,
	EventInfo_get_EventHandlerType_m8490,
	EventInfo_get_MemberType_m8491,
	FieldInfo__ctor_m8492,
	FieldInfo_get_MemberType_m8493,
	FieldInfo_get_IsLiteral_m8494,
	FieldInfo_get_IsStatic_m8495,
	FieldInfo_get_IsNotSerialized_m8496,
	FieldInfo_SetValue_m8497,
	FieldInfo_internal_from_handle_type_m8498,
	FieldInfo_GetFieldFromHandle_m8499,
	FieldInfo_GetFieldOffset_m8500,
	FieldInfo_GetUnmanagedMarshal_m8501,
	FieldInfo_get_UMarshal_m8502,
	FieldInfo_GetPseudoCustomAttributes_m8503,
	MemberInfoSerializationHolder__ctor_m8504,
	MemberInfoSerializationHolder_Serialize_m8505,
	MemberInfoSerializationHolder_Serialize_m8506,
	MemberInfoSerializationHolder_GetObjectData_m8507,
	MemberInfoSerializationHolder_GetRealObject_m8508,
	MethodBase__ctor_m8509,
	MethodBase_GetMethodFromHandleNoGenericCheck_m8510,
	MethodBase_GetMethodFromIntPtr_m8511,
	MethodBase_GetMethodFromHandle_m8512,
	MethodBase_GetMethodFromHandleInternalType_m8513,
	MethodBase_GetParameterCount_m8514,
	MethodBase_Invoke_m8515,
	MethodBase_get_CallingConvention_m8516,
	MethodBase_get_IsPublic_m8517,
	MethodBase_get_IsStatic_m8518,
	MethodBase_get_IsVirtual_m8519,
	MethodBase_get_IsAbstract_m8520,
	MethodBase_get_next_table_index_m8521,
	MethodBase_GetGenericArguments_m8522,
	MethodBase_get_ContainsGenericParameters_m8523,
	MethodBase_get_IsGenericMethodDefinition_m8524,
	MethodBase_get_IsGenericMethod_m8525,
	MethodInfo__ctor_m8526,
	MethodInfo_get_MemberType_m8527,
	MethodInfo_get_ReturnType_m8528,
	MethodInfo_MakeGenericMethod_m8529,
	MethodInfo_GetGenericArguments_m8530,
	MethodInfo_get_IsGenericMethod_m8531,
	MethodInfo_get_IsGenericMethodDefinition_m8532,
	MethodInfo_get_ContainsGenericParameters_m8533,
	Missing__ctor_m8534,
	Missing__cctor_m8535,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8536,
	Module__ctor_m8537,
	Module__cctor_m8538,
	Module_get_Assembly_m8539,
	Module_get_ScopeName_m8540,
	Module_GetCustomAttributes_m8541,
	Module_GetObjectData_m8542,
	Module_InternalGetTypes_m8543,
	Module_GetTypes_m8544,
	Module_IsDefined_m8545,
	Module_IsResource_m8546,
	Module_ToString_m8547,
	Module_filter_by_type_name_m8548,
	Module_filter_by_type_name_ignore_case_m8549,
	MonoEventInfo_get_event_info_m8550,
	MonoEventInfo_GetEventInfo_m8551,
	MonoEvent__ctor_m8552,
	MonoEvent_get_Attributes_m8553,
	MonoEvent_GetAddMethod_m8554,
	MonoEvent_get_DeclaringType_m8555,
	MonoEvent_get_ReflectedType_m8556,
	MonoEvent_get_Name_m8557,
	MonoEvent_ToString_m8558,
	MonoEvent_IsDefined_m8559,
	MonoEvent_GetCustomAttributes_m8560,
	MonoEvent_GetCustomAttributes_m8561,
	MonoEvent_GetObjectData_m8562,
	MonoField__ctor_m8563,
	MonoField_get_Attributes_m8564,
	MonoField_get_FieldHandle_m8565,
	MonoField_get_FieldType_m8566,
	MonoField_GetParentType_m8567,
	MonoField_get_ReflectedType_m8568,
	MonoField_get_DeclaringType_m8569,
	MonoField_get_Name_m8570,
	MonoField_IsDefined_m8571,
	MonoField_GetCustomAttributes_m8572,
	MonoField_GetCustomAttributes_m8573,
	MonoField_GetFieldOffset_m8574,
	MonoField_GetValueInternal_m8575,
	MonoField_GetValue_m8576,
	MonoField_ToString_m8577,
	MonoField_SetValueInternal_m8578,
	MonoField_SetValue_m8579,
	MonoField_GetObjectData_m8580,
	MonoField_CheckGeneric_m8581,
	MonoGenericMethod__ctor_m8582,
	MonoGenericMethod_get_ReflectedType_m8583,
	MonoGenericCMethod__ctor_m8584,
	MonoGenericCMethod_get_ReflectedType_m8585,
	MonoMethodInfo_get_method_info_m8586,
	MonoMethodInfo_GetMethodInfo_m8587,
	MonoMethodInfo_GetDeclaringType_m8588,
	MonoMethodInfo_GetReturnType_m8589,
	MonoMethodInfo_GetAttributes_m8590,
	MonoMethodInfo_GetCallingConvention_m8591,
	MonoMethodInfo_get_parameter_info_m8592,
	MonoMethodInfo_GetParametersInfo_m8593,
	MonoMethod__ctor_m8594,
	MonoMethod_get_name_m8595,
	MonoMethod_get_base_definition_m8596,
	MonoMethod_GetBaseDefinition_m8597,
	MonoMethod_get_ReturnType_m8598,
	MonoMethod_GetParameters_m8599,
	MonoMethod_InternalInvoke_m8600,
	MonoMethod_Invoke_m8601,
	MonoMethod_get_MethodHandle_m8602,
	MonoMethod_get_Attributes_m8603,
	MonoMethod_get_CallingConvention_m8604,
	MonoMethod_get_ReflectedType_m8605,
	MonoMethod_get_DeclaringType_m8606,
	MonoMethod_get_Name_m8607,
	MonoMethod_IsDefined_m8608,
	MonoMethod_GetCustomAttributes_m8609,
	MonoMethod_GetCustomAttributes_m8610,
	MonoMethod_GetDllImportAttribute_m8611,
	MonoMethod_GetPseudoCustomAttributes_m8612,
	MonoMethod_ShouldPrintFullName_m8613,
	MonoMethod_ToString_m8614,
	MonoMethod_GetObjectData_m8615,
	MonoMethod_MakeGenericMethod_m8616,
	MonoMethod_MakeGenericMethod_impl_m8617,
	MonoMethod_GetGenericArguments_m8618,
	MonoMethod_get_IsGenericMethodDefinition_m8619,
	MonoMethod_get_IsGenericMethod_m8620,
	MonoMethod_get_ContainsGenericParameters_m8621,
	MonoCMethod__ctor_m8622,
	MonoCMethod_GetParameters_m8623,
	MonoCMethod_InternalInvoke_m8624,
	MonoCMethod_Invoke_m8625,
	MonoCMethod_Invoke_m8626,
	MonoCMethod_get_MethodHandle_m8627,
	MonoCMethod_get_Attributes_m8628,
	MonoCMethod_get_CallingConvention_m8629,
	MonoCMethod_get_ReflectedType_m8630,
	MonoCMethod_get_DeclaringType_m8631,
	MonoCMethod_get_Name_m8632,
	MonoCMethod_IsDefined_m8633,
	MonoCMethod_GetCustomAttributes_m8634,
	MonoCMethod_GetCustomAttributes_m8635,
	MonoCMethod_ToString_m8636,
	MonoCMethod_GetObjectData_m8637,
	MonoPropertyInfo_get_property_info_m8638,
	MonoPropertyInfo_GetTypeModifiers_m8639,
	GetterAdapter__ctor_m8640,
	GetterAdapter_Invoke_m8641,
	GetterAdapter_BeginInvoke_m8642,
	GetterAdapter_EndInvoke_m8643,
	MonoProperty__ctor_m8644,
	MonoProperty_CachePropertyInfo_m8645,
	MonoProperty_get_Attributes_m8646,
	MonoProperty_get_CanRead_m8647,
	MonoProperty_get_CanWrite_m8648,
	MonoProperty_get_PropertyType_m8649,
	MonoProperty_get_ReflectedType_m8650,
	MonoProperty_get_DeclaringType_m8651,
	MonoProperty_get_Name_m8652,
	MonoProperty_GetAccessors_m8653,
	MonoProperty_GetGetMethod_m8654,
	MonoProperty_GetIndexParameters_m8655,
	MonoProperty_GetSetMethod_m8656,
	MonoProperty_IsDefined_m8657,
	MonoProperty_GetCustomAttributes_m8658,
	MonoProperty_GetCustomAttributes_m8659,
	MonoProperty_CreateGetterDelegate_m8660,
	MonoProperty_GetValue_m8661,
	MonoProperty_GetValue_m8662,
	MonoProperty_SetValue_m8663,
	MonoProperty_ToString_m8664,
	MonoProperty_GetOptionalCustomModifiers_m8665,
	MonoProperty_GetRequiredCustomModifiers_m8666,
	MonoProperty_GetObjectData_m8667,
	ParameterInfo__ctor_m8668,
	ParameterInfo__ctor_m8669,
	ParameterInfo__ctor_m8670,
	ParameterInfo_ToString_m8671,
	ParameterInfo_get_ParameterType_m8672,
	ParameterInfo_get_Attributes_m8673,
	ParameterInfo_get_IsIn_m8674,
	ParameterInfo_get_IsOptional_m8675,
	ParameterInfo_get_IsOut_m8676,
	ParameterInfo_get_IsRetval_m8677,
	ParameterInfo_get_Member_m8678,
	ParameterInfo_get_Name_m8679,
	ParameterInfo_get_Position_m8680,
	ParameterInfo_GetCustomAttributes_m8681,
	ParameterInfo_IsDefined_m8682,
	ParameterInfo_GetPseudoCustomAttributes_m8683,
	Pointer__ctor_m8684,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8685,
	PropertyInfo__ctor_m8686,
	PropertyInfo_get_MemberType_m8687,
	PropertyInfo_GetValue_m8688,
	PropertyInfo_SetValue_m8689,
	PropertyInfo_GetOptionalCustomModifiers_m8690,
	PropertyInfo_GetRequiredCustomModifiers_m8691,
	StrongNameKeyPair__ctor_m8692,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8693,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8694,
	TargetException__ctor_m8695,
	TargetException__ctor_m8696,
	TargetException__ctor_m8697,
	TargetInvocationException__ctor_m8698,
	TargetInvocationException__ctor_m8699,
	TargetParameterCountException__ctor_m8700,
	TargetParameterCountException__ctor_m8701,
	TargetParameterCountException__ctor_m8702,
	NeutralResourcesLanguageAttribute__ctor_m8703,
	ResourceManager__ctor_m8704,
	ResourceManager__cctor_m8705,
	ResourceInfo__ctor_m8706,
	ResourceCacheItem__ctor_m8707,
	ResourceEnumerator__ctor_m8708,
	ResourceEnumerator_get_Entry_m8709,
	ResourceEnumerator_get_Key_m8710,
	ResourceEnumerator_get_Value_m8711,
	ResourceEnumerator_get_Current_m8712,
	ResourceEnumerator_MoveNext_m8713,
	ResourceEnumerator_Reset_m8714,
	ResourceEnumerator_FillCache_m8715,
	ResourceReader__ctor_m8716,
	ResourceReader__ctor_m8717,
	ResourceReader_System_Collections_IEnumerable_GetEnumerator_m8718,
	ResourceReader_System_IDisposable_Dispose_m8719,
	ResourceReader_ReadHeaders_m8720,
	ResourceReader_CreateResourceInfo_m8721,
	ResourceReader_Read7BitEncodedInt_m8722,
	ResourceReader_ReadValueVer2_m8723,
	ResourceReader_ReadValueVer1_m8724,
	ResourceReader_ReadNonPredefinedValue_m8725,
	ResourceReader_LoadResourceValues_m8726,
	ResourceReader_Close_m8727,
	ResourceReader_GetEnumerator_m8728,
	ResourceReader_Dispose_m8729,
	ResourceSet__ctor_m8730,
	ResourceSet__ctor_m8731,
	ResourceSet__ctor_m8732,
	ResourceSet__ctor_m8733,
	ResourceSet_System_Collections_IEnumerable_GetEnumerator_m8734,
	ResourceSet_Dispose_m8735,
	ResourceSet_Dispose_m8736,
	ResourceSet_GetEnumerator_m8737,
	ResourceSet_GetObjectInternal_m8738,
	ResourceSet_GetObject_m8739,
	ResourceSet_GetObject_m8740,
	ResourceSet_ReadResources_m8741,
	RuntimeResourceSet__ctor_m8742,
	RuntimeResourceSet__ctor_m8743,
	RuntimeResourceSet__ctor_m8744,
	RuntimeResourceSet_GetObject_m8745,
	RuntimeResourceSet_GetObject_m8746,
	RuntimeResourceSet_CloneDisposableObjectIfPossible_m8747,
	SatelliteContractVersionAttribute__ctor_m8748,
	CompilationRelaxationsAttribute__ctor_m8749,
	DefaultDependencyAttribute__ctor_m8750,
	StringFreezingAttribute__ctor_m8751,
	CriticalFinalizerObject__ctor_m8752,
	CriticalFinalizerObject_Finalize_m8753,
	ReliabilityContractAttribute__ctor_m8754,
	ClassInterfaceAttribute__ctor_m8755,
	ComDefaultInterfaceAttribute__ctor_m8756,
	DispIdAttribute__ctor_m8757,
	GCHandle__ctor_m8758,
	GCHandle_get_IsAllocated_m8759,
	GCHandle_get_Target_m8760,
	GCHandle_Alloc_m8761,
	GCHandle_Free_m8762,
	GCHandle_GetTarget_m8763,
	GCHandle_GetTargetHandle_m8764,
	GCHandle_FreeHandle_m8765,
	GCHandle_Equals_m8766,
	GCHandle_GetHashCode_m8767,
	InterfaceTypeAttribute__ctor_m8768,
	Marshal__cctor_m8769,
	Marshal_copy_from_unmanaged_m8770,
	Marshal_Copy_m8771,
	Marshal_Copy_m8772,
	Marshal_ReadByte_m8773,
	Marshal_WriteByte_m8774,
	MarshalDirectiveException__ctor_m8775,
	MarshalDirectiveException__ctor_m8776,
	PreserveSigAttribute__ctor_m8777,
	SafeHandle__ctor_m8778,
	SafeHandle_Close_m8779,
	SafeHandle_DangerousAddRef_m8780,
	SafeHandle_DangerousGetHandle_m8781,
	SafeHandle_DangerousRelease_m8782,
	SafeHandle_Dispose_m8783,
	SafeHandle_Dispose_m8784,
	SafeHandle_SetHandle_m8785,
	SafeHandle_Finalize_m8786,
	TypeLibImportClassAttribute__ctor_m8787,
	TypeLibVersionAttribute__ctor_m8788,
	ActivationServices_get_ConstructionActivator_m8789,
	ActivationServices_CreateProxyFromAttributes_m8790,
	ActivationServices_CreateConstructionCall_m8791,
	ActivationServices_AllocateUninitializedClassInstance_m8792,
	ActivationServices_EnableProxyActivation_m8793,
	AppDomainLevelActivator__ctor_m8794,
	ConstructionLevelActivator__ctor_m8795,
	ContextLevelActivator__ctor_m8796,
	UrlAttribute_get_UrlValue_m8797,
	UrlAttribute_Equals_m8798,
	UrlAttribute_GetHashCode_m8799,
	UrlAttribute_GetPropertiesForNewContext_m8800,
	UrlAttribute_IsContextOK_m8801,
	ChannelInfo__ctor_m8802,
	ChannelInfo_get_ChannelData_m8803,
	ChannelServices__cctor_m8804,
	ChannelServices_CreateClientChannelSinkChain_m8805,
	ChannelServices_CreateClientChannelSinkChain_m8806,
	ChannelServices_RegisterChannel_m8807,
	ChannelServices_RegisterChannel_m8808,
	ChannelServices_RegisterChannelConfig_m8809,
	ChannelServices_CreateProvider_m8810,
	ChannelServices_GetCurrentChannelInfo_m8811,
	CrossAppDomainData__ctor_m8812,
	CrossAppDomainData_get_DomainID_m8813,
	CrossAppDomainData_get_ProcessID_m8814,
	CrossAppDomainChannel__ctor_m8815,
	CrossAppDomainChannel__cctor_m8816,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8817,
	CrossAppDomainChannel_get_ChannelName_m8818,
	CrossAppDomainChannel_get_ChannelPriority_m8819,
	CrossAppDomainChannel_get_ChannelData_m8820,
	CrossAppDomainChannel_StartListening_m8821,
	CrossAppDomainChannel_CreateMessageSink_m8822,
	CrossAppDomainSink__ctor_m8823,
	CrossAppDomainSink__cctor_m8824,
	CrossAppDomainSink_GetSink_m8825,
	CrossAppDomainSink_get_TargetDomainId_m8826,
	SinkProviderData__ctor_m8827,
	SinkProviderData_get_Children_m8828,
	SinkProviderData_get_Properties_m8829,
	Context__ctor_m8830,
	Context__cctor_m8831,
	Context_Finalize_m8832,
	Context_get_DefaultContext_m8833,
	Context_get_ContextID_m8834,
	Context_get_ContextProperties_m8835,
	Context_get_IsDefaultContext_m8836,
	Context_get_NeedsContextSink_m8837,
	Context_RegisterDynamicProperty_m8838,
	Context_UnregisterDynamicProperty_m8839,
	Context_GetDynamicPropertyCollection_m8840,
	Context_NotifyGlobalDynamicSinks_m8841,
	Context_get_HasGlobalDynamicSinks_m8842,
	Context_NotifyDynamicSinks_m8843,
	Context_get_HasDynamicSinks_m8844,
	Context_get_HasExitSinks_m8845,
	Context_GetProperty_m8846,
	Context_SetProperty_m8847,
	Context_Freeze_m8848,
	Context_ToString_m8849,
	Context_GetServerContextSinkChain_m8850,
	Context_GetClientContextSinkChain_m8851,
	Context_CreateServerObjectSinkChain_m8852,
	Context_CreateEnvoySink_m8853,
	Context_SwitchToContext_m8854,
	Context_CreateNewContext_m8855,
	Context_DoCallBack_m8856,
	Context_AllocateDataSlot_m8857,
	Context_AllocateNamedDataSlot_m8858,
	Context_FreeNamedDataSlot_m8859,
	Context_GetData_m8860,
	Context_GetNamedDataSlot_m8861,
	Context_SetData_m8862,
	DynamicPropertyReg__ctor_m8863,
	DynamicPropertyCollection__ctor_m8864,
	DynamicPropertyCollection_get_HasProperties_m8865,
	DynamicPropertyCollection_RegisterDynamicProperty_m8866,
	DynamicPropertyCollection_UnregisterDynamicProperty_m8867,
	DynamicPropertyCollection_NotifyMessage_m8868,
	DynamicPropertyCollection_FindProperty_m8869,
	ContextCallbackObject__ctor_m8870,
	ContextCallbackObject_DoCallBack_m8871,
	ContextAttribute__ctor_m8872,
	ContextAttribute_get_Name_m8873,
	ContextAttribute_Equals_m8874,
	ContextAttribute_Freeze_m8875,
	ContextAttribute_GetHashCode_m8876,
	ContextAttribute_GetPropertiesForNewContext_m8877,
	ContextAttribute_IsContextOK_m8878,
	ContextAttribute_IsNewContextOK_m8879,
	CrossContextChannel__ctor_m8880,
	SynchronizationAttribute__ctor_m8881,
	SynchronizationAttribute__ctor_m8882,
	SynchronizationAttribute_set_Locked_m8883,
	SynchronizationAttribute_ReleaseLock_m8884,
	SynchronizationAttribute_GetPropertiesForNewContext_m8885,
	SynchronizationAttribute_GetClientContextSink_m8886,
	SynchronizationAttribute_GetServerContextSink_m8887,
	SynchronizationAttribute_IsContextOK_m8888,
	SynchronizationAttribute_ExitContext_m8889,
	SynchronizationAttribute_EnterContext_m8890,
	SynchronizedClientContextSink__ctor_m8891,
	SynchronizedServerContextSink__ctor_m8892,
	LeaseManager__ctor_m8893,
	LeaseManager_SetPollTime_m8894,
	LeaseSink__ctor_m8895,
	LifetimeServices__cctor_m8896,
	LifetimeServices_set_LeaseManagerPollTime_m8897,
	LifetimeServices_set_LeaseTime_m8898,
	LifetimeServices_set_RenewOnCallTime_m8899,
	LifetimeServices_set_SponsorshipTimeout_m8900,
	ArgInfo__ctor_m8901,
	ArgInfo_GetInOutArgs_m8902,
	AsyncResult__ctor_m8903,
	AsyncResult_get_AsyncState_m8904,
	AsyncResult_get_AsyncWaitHandle_m8905,
	AsyncResult_get_CompletedSynchronously_m8906,
	AsyncResult_get_IsCompleted_m8907,
	AsyncResult_get_EndInvokeCalled_m8908,
	AsyncResult_set_EndInvokeCalled_m8909,
	AsyncResult_get_AsyncDelegate_m8910,
	AsyncResult_get_NextSink_m8911,
	AsyncResult_AsyncProcessMessage_m8912,
	AsyncResult_GetReplyMessage_m8913,
	AsyncResult_SetMessageCtrl_m8914,
	AsyncResult_SetCompletedSynchronously_m8915,
	AsyncResult_EndInvoke_m8916,
	AsyncResult_SyncProcessMessage_m8917,
	AsyncResult_get_CallMessage_m8918,
	AsyncResult_set_CallMessage_m8919,
	ClientContextTerminatorSink__ctor_m8920,
	ConstructionCall__ctor_m8921,
	ConstructionCall__ctor_m8922,
	ConstructionCall_InitDictionary_m8923,
	ConstructionCall_set_IsContextOk_m8924,
	ConstructionCall_get_ActivationType_m8925,
	ConstructionCall_get_ActivationTypeName_m8926,
	ConstructionCall_get_Activator_m8927,
	ConstructionCall_set_Activator_m8928,
	ConstructionCall_get_CallSiteActivationAttributes_m8929,
	ConstructionCall_SetActivationAttributes_m8930,
	ConstructionCall_get_ContextProperties_m8931,
	ConstructionCall_InitMethodProperty_m8932,
	ConstructionCall_GetObjectData_m8933,
	ConstructionCall_get_Properties_m8934,
	ConstructionCallDictionary__ctor_m8935,
	ConstructionCallDictionary__cctor_m8936,
	ConstructionCallDictionary_GetMethodProperty_m8937,
	ConstructionCallDictionary_SetMethodProperty_m8938,
	EnvoyTerminatorSink__ctor_m8939,
	EnvoyTerminatorSink__cctor_m8940,
	Header__ctor_m8941,
	Header__ctor_m8942,
	Header__ctor_m8943,
	LogicalCallContext__ctor_m8944,
	LogicalCallContext__ctor_m8945,
	LogicalCallContext_GetObjectData_m8946,
	LogicalCallContext_SetData_m8947,
	LogicalCallContext_Clone_m8948,
	CallContextRemotingData__ctor_m8949,
	CallContextRemotingData_Clone_m8950,
	MethodCall__ctor_m8951,
	MethodCall__ctor_m8952,
	MethodCall__ctor_m8953,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8954,
	MethodCall_InitMethodProperty_m8955,
	MethodCall_GetObjectData_m8956,
	MethodCall_get_Args_m8957,
	MethodCall_get_LogicalCallContext_m8958,
	MethodCall_get_MethodBase_m8959,
	MethodCall_get_MethodName_m8960,
	MethodCall_get_MethodSignature_m8961,
	MethodCall_get_Properties_m8962,
	MethodCall_InitDictionary_m8963,
	MethodCall_get_TypeName_m8964,
	MethodCall_get_Uri_m8965,
	MethodCall_set_Uri_m8966,
	MethodCall_Init_m8967,
	MethodCall_ResolveMethod_m8968,
	MethodCall_CastTo_m8969,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m8970,
	MethodCall_get_GenericArguments_m8971,
	MethodCallDictionary__ctor_m8972,
	MethodCallDictionary__cctor_m8973,
	DictionaryEnumerator__ctor_m8974,
	DictionaryEnumerator_get_Current_m8975,
	DictionaryEnumerator_MoveNext_m8976,
	DictionaryEnumerator_Reset_m8977,
	DictionaryEnumerator_get_Entry_m8978,
	DictionaryEnumerator_get_Key_m8979,
	DictionaryEnumerator_get_Value_m8980,
	MethodDictionary__ctor_m8981,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8982,
	MethodDictionary_set_MethodKeys_m8983,
	MethodDictionary_AllocInternalProperties_m8984,
	MethodDictionary_GetInternalProperties_m8985,
	MethodDictionary_IsOverridenKey_m8986,
	MethodDictionary_get_Item_m8987,
	MethodDictionary_set_Item_m8988,
	MethodDictionary_GetMethodProperty_m8989,
	MethodDictionary_SetMethodProperty_m8990,
	MethodDictionary_get_Values_m8991,
	MethodDictionary_Add_m8992,
	MethodDictionary_Contains_m8993,
	MethodDictionary_Remove_m8994,
	MethodDictionary_get_Count_m8995,
	MethodDictionary_get_IsSynchronized_m8996,
	MethodDictionary_get_SyncRoot_m8997,
	MethodDictionary_CopyTo_m8998,
	MethodDictionary_GetEnumerator_m8999,
	MethodReturnDictionary__ctor_m9000,
	MethodReturnDictionary__cctor_m9001,
	MonoMethodMessage_get_Args_m9002,
	MonoMethodMessage_get_LogicalCallContext_m9003,
	MonoMethodMessage_get_MethodBase_m9004,
	MonoMethodMessage_get_MethodName_m9005,
	MonoMethodMessage_get_MethodSignature_m9006,
	MonoMethodMessage_get_TypeName_m9007,
	MonoMethodMessage_get_Uri_m9008,
	MonoMethodMessage_set_Uri_m9009,
	MonoMethodMessage_get_Exception_m9010,
	MonoMethodMessage_get_OutArgCount_m9011,
	MonoMethodMessage_get_OutArgs_m9012,
	MonoMethodMessage_get_ReturnValue_m9013,
	RemotingSurrogate__ctor_m9014,
	RemotingSurrogate_SetObjectData_m9015,
	ObjRefSurrogate__ctor_m9016,
	ObjRefSurrogate_SetObjectData_m9017,
	RemotingSurrogateSelector__ctor_m9018,
	RemotingSurrogateSelector__cctor_m9019,
	RemotingSurrogateSelector_GetSurrogate_m9020,
	ReturnMessage__ctor_m9021,
	ReturnMessage__ctor_m9022,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m9023,
	ReturnMessage_get_Args_m9024,
	ReturnMessage_get_LogicalCallContext_m9025,
	ReturnMessage_get_MethodBase_m9026,
	ReturnMessage_get_MethodName_m9027,
	ReturnMessage_get_MethodSignature_m9028,
	ReturnMessage_get_Properties_m9029,
	ReturnMessage_get_TypeName_m9030,
	ReturnMessage_get_Uri_m9031,
	ReturnMessage_set_Uri_m9032,
	ReturnMessage_get_Exception_m9033,
	ReturnMessage_get_OutArgs_m9034,
	ReturnMessage_get_ReturnValue_m9035,
	ServerContextTerminatorSink__ctor_m9036,
	ServerObjectTerminatorSink__ctor_m9037,
	StackBuilderSink__ctor_m9038,
	SoapAttribute__ctor_m9039,
	SoapAttribute_get_UseAttribute_m9040,
	SoapAttribute_get_XmlNamespace_m9041,
	SoapAttribute_SetReflectionObject_m9042,
	SoapFieldAttribute__ctor_m9043,
	SoapFieldAttribute_get_XmlElementName_m9044,
	SoapFieldAttribute_IsInteropXmlElement_m9045,
	SoapFieldAttribute_SetReflectionObject_m9046,
	SoapMethodAttribute__ctor_m9047,
	SoapMethodAttribute_get_UseAttribute_m9048,
	SoapMethodAttribute_get_XmlNamespace_m9049,
	SoapMethodAttribute_SetReflectionObject_m9050,
	SoapParameterAttribute__ctor_m9051,
	SoapTypeAttribute__ctor_m9052,
	SoapTypeAttribute_get_UseAttribute_m9053,
	SoapTypeAttribute_get_XmlElementName_m9054,
	SoapTypeAttribute_get_XmlNamespace_m9055,
	SoapTypeAttribute_get_XmlTypeName_m9056,
	SoapTypeAttribute_get_XmlTypeNamespace_m9057,
	SoapTypeAttribute_get_IsInteropXmlElement_m9058,
	SoapTypeAttribute_get_IsInteropXmlType_m9059,
	SoapTypeAttribute_SetReflectionObject_m9060,
	ProxyAttribute_CreateInstance_m9061,
	ProxyAttribute_CreateProxy_m9062,
	ProxyAttribute_GetPropertiesForNewContext_m9063,
	ProxyAttribute_IsContextOK_m9064,
	RealProxy__ctor_m9065,
	RealProxy__ctor_m9066,
	RealProxy__ctor_m9067,
	RealProxy_InternalGetProxyType_m9068,
	RealProxy_GetProxiedType_m9069,
	RealProxy_get_ObjectIdentity_m9070,
	RealProxy_InternalGetTransparentProxy_m9071,
	RealProxy_GetTransparentProxy_m9072,
	RealProxy_SetTargetDomain_m9073,
	RemotingProxy__ctor_m9074,
	RemotingProxy__ctor_m9075,
	RemotingProxy__cctor_m9076,
	RemotingProxy_get_TypeName_m9077,
	RemotingProxy_Finalize_m9078,
	TrackingServices__cctor_m9079,
	TrackingServices_NotifyUnmarshaledObject_m9080,
	ActivatedClientTypeEntry__ctor_m9081,
	ActivatedClientTypeEntry_get_ApplicationUrl_m9082,
	ActivatedClientTypeEntry_get_ContextAttributes_m9083,
	ActivatedClientTypeEntry_get_ObjectType_m9084,
	ActivatedClientTypeEntry_ToString_m9085,
	ActivatedServiceTypeEntry__ctor_m9086,
	ActivatedServiceTypeEntry_get_ObjectType_m9087,
	ActivatedServiceTypeEntry_ToString_m9088,
	EnvoyInfo__ctor_m9089,
	EnvoyInfo_get_EnvoySinks_m9090,
	Identity__ctor_m9091,
	Identity_get_ChannelSink_m9092,
	Identity_set_ChannelSink_m9093,
	Identity_get_ObjectUri_m9094,
	Identity_get_Disposed_m9095,
	Identity_set_Disposed_m9096,
	Identity_get_ClientDynamicProperties_m9097,
	Identity_get_ServerDynamicProperties_m9098,
	ClientIdentity__ctor_m9099,
	ClientIdentity_get_ClientProxy_m9100,
	ClientIdentity_set_ClientProxy_m9101,
	ClientIdentity_CreateObjRef_m9102,
	ClientIdentity_get_TargetUri_m9103,
	InternalRemotingServices__cctor_m9104,
	InternalRemotingServices_GetCachedSoapAttribute_m9105,
	ObjRef__ctor_m9106,
	ObjRef__ctor_m9107,
	ObjRef__cctor_m9108,
	ObjRef_get_IsReferenceToWellKnow_m9109,
	ObjRef_get_ChannelInfo_m9110,
	ObjRef_get_EnvoyInfo_m9111,
	ObjRef_set_EnvoyInfo_m9112,
	ObjRef_get_TypeInfo_m9113,
	ObjRef_set_TypeInfo_m9114,
	ObjRef_get_URI_m9115,
	ObjRef_set_URI_m9116,
	ObjRef_GetObjectData_m9117,
	ObjRef_GetRealObject_m9118,
	ObjRef_UpdateChannelInfo_m9119,
	ObjRef_get_ServerType_m9120,
	RemotingConfiguration__cctor_m9121,
	RemotingConfiguration_get_ApplicationName_m9122,
	RemotingConfiguration_set_ApplicationName_m9123,
	RemotingConfiguration_get_ProcessId_m9124,
	RemotingConfiguration_LoadDefaultDelayedChannels_m9125,
	RemotingConfiguration_IsRemotelyActivatedClientType_m9126,
	RemotingConfiguration_RegisterActivatedClientType_m9127,
	RemotingConfiguration_RegisterActivatedServiceType_m9128,
	RemotingConfiguration_RegisterWellKnownClientType_m9129,
	RemotingConfiguration_RegisterWellKnownServiceType_m9130,
	RemotingConfiguration_RegisterChannelTemplate_m9131,
	RemotingConfiguration_RegisterClientProviderTemplate_m9132,
	RemotingConfiguration_RegisterServerProviderTemplate_m9133,
	RemotingConfiguration_RegisterChannels_m9134,
	RemotingConfiguration_RegisterTypes_m9135,
	RemotingConfiguration_SetCustomErrorsMode_m9136,
	ConfigHandler__ctor_m9137,
	ConfigHandler_ValidatePath_m9138,
	ConfigHandler_CheckPath_m9139,
	ConfigHandler_OnStartParsing_m9140,
	ConfigHandler_OnProcessingInstruction_m9141,
	ConfigHandler_OnIgnorableWhitespace_m9142,
	ConfigHandler_OnStartElement_m9143,
	ConfigHandler_ParseElement_m9144,
	ConfigHandler_OnEndElement_m9145,
	ConfigHandler_ReadCustomProviderData_m9146,
	ConfigHandler_ReadLifetine_m9147,
	ConfigHandler_ParseTime_m9148,
	ConfigHandler_ReadChannel_m9149,
	ConfigHandler_ReadProvider_m9150,
	ConfigHandler_ReadClientActivated_m9151,
	ConfigHandler_ReadServiceActivated_m9152,
	ConfigHandler_ReadClientWellKnown_m9153,
	ConfigHandler_ReadServiceWellKnown_m9154,
	ConfigHandler_ReadInteropXml_m9155,
	ConfigHandler_ReadPreload_m9156,
	ConfigHandler_GetNotNull_m9157,
	ConfigHandler_ExtractAssembly_m9158,
	ConfigHandler_OnChars_m9159,
	ConfigHandler_OnEndParsing_m9160,
	ChannelData__ctor_m9161,
	ChannelData_get_ServerProviders_m9162,
	ChannelData_get_ClientProviders_m9163,
	ChannelData_get_CustomProperties_m9164,
	ChannelData_CopyFrom_m9165,
	ProviderData__ctor_m9166,
	ProviderData_CopyFrom_m9167,
	FormatterData__ctor_m9168,
	RemotingException__ctor_m9169,
	RemotingException__ctor_m9170,
	RemotingException__ctor_m9171,
	RemotingException__ctor_m9172,
	RemotingServices__cctor_m9173,
	RemotingServices_GetVirtualMethod_m9174,
	RemotingServices_IsTransparentProxy_m9175,
	RemotingServices_GetServerTypeForUri_m9176,
	RemotingServices_Unmarshal_m9177,
	RemotingServices_Unmarshal_m9178,
	RemotingServices_GetRealProxy_m9179,
	RemotingServices_GetMethodBaseFromMethodMessage_m9180,
	RemotingServices_GetMethodBaseFromName_m9181,
	RemotingServices_FindInterfaceMethod_m9182,
	RemotingServices_CreateClientProxy_m9183,
	RemotingServices_CreateClientProxy_m9184,
	RemotingServices_CreateClientProxyForContextBound_m9185,
	RemotingServices_GetIdentityForUri_m9186,
	RemotingServices_RemoveAppNameFromUri_m9187,
	RemotingServices_GetOrCreateClientIdentity_m9188,
	RemotingServices_GetClientChannelSinkChain_m9189,
	RemotingServices_CreateWellKnownServerIdentity_m9190,
	RemotingServices_RegisterServerIdentity_m9191,
	RemotingServices_GetProxyForRemoteObject_m9192,
	RemotingServices_GetRemoteObject_m9193,
	RemotingServices_RegisterInternalChannels_m9194,
	RemotingServices_DisposeIdentity_m9195,
	RemotingServices_GetNormalizedUri_m9196,
	ServerIdentity__ctor_m9197,
	ServerIdentity_get_ObjectType_m9198,
	ServerIdentity_CreateObjRef_m9199,
	ClientActivatedIdentity_GetServerObject_m9200,
	SingletonIdentity__ctor_m9201,
	SingleCallIdentity__ctor_m9202,
	TypeInfo__ctor_m9203,
	SoapServices__cctor_m9204,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m9205,
	SoapServices_get_XmlNsForClrTypeWithNs_m9206,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m9207,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m9208,
	SoapServices_GetNameKey_m9209,
	SoapServices_GetAssemblyName_m9210,
	SoapServices_GetXmlElementForInteropType_m9211,
	SoapServices_GetXmlNamespaceForMethodCall_m9212,
	SoapServices_GetXmlNamespaceForMethodResponse_m9213,
	SoapServices_GetXmlTypeForInteropType_m9214,
	SoapServices_PreLoad_m9215,
	SoapServices_PreLoad_m9216,
	SoapServices_RegisterInteropXmlElement_m9217,
	SoapServices_RegisterInteropXmlType_m9218,
	SoapServices_EncodeNs_m9219,
	TypeEntry__ctor_m9220,
	TypeEntry_get_AssemblyName_m9221,
	TypeEntry_set_AssemblyName_m9222,
	TypeEntry_get_TypeName_m9223,
	TypeEntry_set_TypeName_m9224,
	TypeInfo__ctor_m9225,
	TypeInfo_get_TypeName_m9226,
	WellKnownClientTypeEntry__ctor_m9227,
	WellKnownClientTypeEntry_get_ApplicationUrl_m9228,
	WellKnownClientTypeEntry_get_ObjectType_m9229,
	WellKnownClientTypeEntry_get_ObjectUrl_m9230,
	WellKnownClientTypeEntry_ToString_m9231,
	WellKnownServiceTypeEntry__ctor_m9232,
	WellKnownServiceTypeEntry_get_Mode_m9233,
	WellKnownServiceTypeEntry_get_ObjectType_m9234,
	WellKnownServiceTypeEntry_get_ObjectUri_m9235,
	WellKnownServiceTypeEntry_ToString_m9236,
	BinaryCommon__cctor_m9237,
	BinaryCommon_IsPrimitive_m9238,
	BinaryCommon_GetTypeFromCode_m9239,
	BinaryCommon_SwapBytes_m9240,
	BinaryFormatter__ctor_m9241,
	BinaryFormatter__ctor_m9242,
	BinaryFormatter_get_DefaultSurrogateSelector_m9243,
	BinaryFormatter_set_AssemblyFormat_m9244,
	BinaryFormatter_get_Binder_m9245,
	BinaryFormatter_get_Context_m9246,
	BinaryFormatter_get_SurrogateSelector_m9247,
	BinaryFormatter_get_FilterLevel_m9248,
	BinaryFormatter_Deserialize_m9249,
	BinaryFormatter_NoCheckDeserialize_m9250,
	BinaryFormatter_ReadBinaryHeader_m9251,
	MessageFormatter_ReadMethodCall_m9252,
	MessageFormatter_ReadMethodResponse_m9253,
	TypeMetadata__ctor_m9254,
	ArrayNullFiller__ctor_m9255,
	ObjectReader__ctor_m9256,
	ObjectReader_ReadObjectGraph_m9257,
	ObjectReader_ReadObjectGraph_m9258,
	ObjectReader_ReadNextObject_m9259,
	ObjectReader_ReadNextObject_m9260,
	ObjectReader_get_CurrentObject_m9261,
	ObjectReader_ReadObject_m9262,
	ObjectReader_ReadAssembly_m9263,
	ObjectReader_ReadObjectInstance_m9264,
	ObjectReader_ReadRefTypeObjectInstance_m9265,
	ObjectReader_ReadObjectContent_m9266,
	ObjectReader_RegisterObject_m9267,
	ObjectReader_ReadStringIntance_m9268,
	ObjectReader_ReadGenericArray_m9269,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m9270,
	ObjectReader_ReadArrayOfPrimitiveType_m9271,
	ObjectReader_BlockRead_m9272,
	ObjectReader_ReadArrayOfObject_m9273,
	ObjectReader_ReadArrayOfString_m9274,
	ObjectReader_ReadSimpleArray_m9275,
	ObjectReader_ReadTypeMetadata_m9276,
	ObjectReader_ReadValue_m9277,
	ObjectReader_SetObjectValue_m9278,
	ObjectReader_RecordFixup_m9279,
	ObjectReader_GetDeserializationType_m9280,
	ObjectReader_ReadType_m9281,
	ObjectReader_ReadPrimitiveTypeValue_m9282,
	FormatterConverter__ctor_m9283,
	FormatterConverter_Convert_m9284,
	FormatterConverter_ToBoolean_m9285,
	FormatterConverter_ToInt16_m9286,
	FormatterConverter_ToInt32_m9287,
	FormatterConverter_ToInt64_m9288,
	FormatterConverter_ToString_m9289,
	FormatterServices_GetUninitializedObject_m9290,
	FormatterServices_GetSafeUninitializedObject_m9291,
	ObjectManager__ctor_m9292,
	ObjectManager_DoFixups_m9293,
	ObjectManager_GetObjectRecord_m9294,
	ObjectManager_GetObject_m9295,
	ObjectManager_RaiseDeserializationEvent_m9296,
	ObjectManager_RaiseOnDeserializingEvent_m9297,
	ObjectManager_RaiseOnDeserializedEvent_m9298,
	ObjectManager_AddFixup_m9299,
	ObjectManager_RecordArrayElementFixup_m9300,
	ObjectManager_RecordArrayElementFixup_m9301,
	ObjectManager_RecordDelayedFixup_m9302,
	ObjectManager_RecordFixup_m9303,
	ObjectManager_RegisterObjectInternal_m9304,
	ObjectManager_RegisterObject_m9305,
	BaseFixupRecord__ctor_m9306,
	BaseFixupRecord_DoFixup_m9307,
	ArrayFixupRecord__ctor_m9308,
	ArrayFixupRecord_FixupImpl_m9309,
	MultiArrayFixupRecord__ctor_m9310,
	MultiArrayFixupRecord_FixupImpl_m9311,
	FixupRecord__ctor_m9312,
	FixupRecord_FixupImpl_m9313,
	DelayedFixupRecord__ctor_m9314,
	DelayedFixupRecord_FixupImpl_m9315,
	ObjectRecord__ctor_m9316,
	ObjectRecord_SetMemberValue_m9317,
	ObjectRecord_SetArrayValue_m9318,
	ObjectRecord_SetMemberValue_m9319,
	ObjectRecord_get_IsInstanceReady_m9320,
	ObjectRecord_get_IsUnsolvedObjectReference_m9321,
	ObjectRecord_get_IsRegistered_m9322,
	ObjectRecord_DoFixups_m9323,
	ObjectRecord_RemoveFixup_m9324,
	ObjectRecord_UnchainFixup_m9325,
	ObjectRecord_ChainFixup_m9326,
	ObjectRecord_LoadData_m9327,
	ObjectRecord_get_HasPendingFixups_m9328,
	SerializationBinder__ctor_m9329,
	CallbackHandler__ctor_m9330,
	CallbackHandler_Invoke_m9331,
	CallbackHandler_BeginInvoke_m9332,
	CallbackHandler_EndInvoke_m9333,
	SerializationCallbacks__ctor_m9334,
	SerializationCallbacks__cctor_m9335,
	SerializationCallbacks_get_HasDeserializedCallbacks_m9336,
	SerializationCallbacks_GetMethodsByAttribute_m9337,
	SerializationCallbacks_Invoke_m9338,
	SerializationCallbacks_RaiseOnDeserializing_m9339,
	SerializationCallbacks_RaiseOnDeserialized_m9340,
	SerializationCallbacks_GetSerializationCallbacks_m9341,
	SerializationEntry__ctor_m9342,
	SerializationEntry_get_Name_m9343,
	SerializationEntry_get_Value_m9344,
	SerializationException__ctor_m9345,
	SerializationException__ctor_m4830,
	SerializationException__ctor_m9346,
	SerializationInfo__ctor_m9347,
	SerializationInfo_AddValue_m4826,
	SerializationInfo_GetValue_m4829,
	SerializationInfo_SetType_m9348,
	SerializationInfo_GetEnumerator_m9349,
	SerializationInfo_AddValue_m9350,
	SerializationInfo_AddValue_m4828,
	SerializationInfo_AddValue_m4827,
	SerializationInfo_AddValue_m9351,
	SerializationInfo_AddValue_m9352,
	SerializationInfo_AddValue_m4839,
	SerializationInfo_AddValue_m9353,
	SerializationInfo_AddValue_m4838,
	SerializationInfo_GetBoolean_m4831,
	SerializationInfo_GetInt16_m9354,
	SerializationInfo_GetInt32_m4837,
	SerializationInfo_GetInt64_m4836,
	SerializationInfo_GetString_m4835,
	SerializationInfoEnumerator__ctor_m9355,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m9356,
	SerializationInfoEnumerator_get_Current_m9357,
	SerializationInfoEnumerator_get_Name_m9358,
	SerializationInfoEnumerator_get_Value_m9359,
	SerializationInfoEnumerator_MoveNext_m9360,
	SerializationInfoEnumerator_Reset_m9361,
	StreamingContext__ctor_m9362,
	StreamingContext__ctor_m9363,
	StreamingContext_get_State_m9364,
	StreamingContext_Equals_m9365,
	StreamingContext_GetHashCode_m9366,
	X509Certificate__ctor_m9367,
	X509Certificate__ctor_m5886,
	X509Certificate__ctor_m4887,
	X509Certificate__ctor_m9368,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9369,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m9370,
	X509Certificate_tostr_m9371,
	X509Certificate_Equals_m9372,
	X509Certificate_GetCertHash_m9373,
	X509Certificate_GetCertHashString_m4892,
	X509Certificate_GetEffectiveDateString_m9374,
	X509Certificate_GetExpirationDateString_m9375,
	X509Certificate_GetHashCode_m9376,
	X509Certificate_GetIssuerName_m9377,
	X509Certificate_GetName_m9378,
	X509Certificate_GetPublicKey_m9379,
	X509Certificate_GetRawCertData_m9380,
	X509Certificate_ToString_m9381,
	X509Certificate_ToString_m4905,
	X509Certificate_get_Issuer_m4908,
	X509Certificate_get_Subject_m4907,
	X509Certificate_Equals_m9382,
	X509Certificate_Import_m4902,
	X509Certificate_Reset_m4904,
	AsymmetricAlgorithm__ctor_m9383,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m9384,
	AsymmetricAlgorithm_get_KeySize_m5834,
	AsymmetricAlgorithm_set_KeySize_m5833,
	AsymmetricAlgorithm_Clear_m5892,
	AsymmetricAlgorithm_GetNamedParam_m9385,
	AsymmetricKeyExchangeFormatter__ctor_m9386,
	AsymmetricSignatureDeformatter__ctor_m5881,
	AsymmetricSignatureFormatter__ctor_m5882,
	Base64Constants__cctor_m9387,
	CryptoConfig__cctor_m9388,
	CryptoConfig_Initialize_m9389,
	CryptoConfig_CreateFromName_m4910,
	CryptoConfig_CreateFromName_m4937,
	CryptoConfig_MapNameToOID_m5828,
	CryptoConfig_EncodeOID_m4912,
	CryptoConfig_EncodeLongNumber_m9390,
	CryptographicException__ctor_m9391,
	CryptographicException__ctor_m3892,
	CryptographicException__ctor_m4872,
	CryptographicException__ctor_m3901,
	CryptographicException__ctor_m9392,
	CryptographicUnexpectedOperationException__ctor_m9393,
	CryptographicUnexpectedOperationException__ctor_m5864,
	CryptographicUnexpectedOperationException__ctor_m9394,
	CspParameters__ctor_m5829,
	CspParameters__ctor_m9395,
	CspParameters__ctor_m9396,
	CspParameters__ctor_m9397,
	CspParameters_get_Flags_m9398,
	CspParameters_set_Flags_m5830,
	DES__ctor_m9399,
	DES__cctor_m9400,
	DES_Create_m5865,
	DES_Create_m9401,
	DES_IsWeakKey_m9402,
	DES_IsSemiWeakKey_m9403,
	DES_get_Key_m9404,
	DES_set_Key_m9405,
	DESTransform__ctor_m9406,
	DESTransform__cctor_m9407,
	DESTransform_CipherFunct_m9408,
	DESTransform_Permutation_m9409,
	DESTransform_BSwap_m9410,
	DESTransform_SetKey_m9411,
	DESTransform_ProcessBlock_m9412,
	DESTransform_ECB_m9413,
	DESTransform_GetStrongKey_m9414,
	DESCryptoServiceProvider__ctor_m9415,
	DESCryptoServiceProvider_CreateDecryptor_m9416,
	DESCryptoServiceProvider_CreateEncryptor_m9417,
	DESCryptoServiceProvider_GenerateIV_m9418,
	DESCryptoServiceProvider_GenerateKey_m9419,
	DSA__ctor_m9420,
	DSA_Create_m4866,
	DSA_Create_m9421,
	DSA_ZeroizePrivateKey_m9422,
	DSA_FromXmlString_m9423,
	DSA_ToXmlString_m9424,
	DSACryptoServiceProvider__ctor_m9425,
	DSACryptoServiceProvider__ctor_m4873,
	DSACryptoServiceProvider__ctor_m9426,
	DSACryptoServiceProvider__cctor_m9427,
	DSACryptoServiceProvider_Finalize_m9428,
	DSACryptoServiceProvider_get_KeySize_m9429,
	DSACryptoServiceProvider_get_PublicOnly_m4865,
	DSACryptoServiceProvider_ExportParameters_m9430,
	DSACryptoServiceProvider_ImportParameters_m9431,
	DSACryptoServiceProvider_CreateSignature_m9432,
	DSACryptoServiceProvider_VerifySignature_m9433,
	DSACryptoServiceProvider_Dispose_m9434,
	DSACryptoServiceProvider_OnKeyGenerated_m9435,
	DSASignatureDeformatter__ctor_m9436,
	DSASignatureDeformatter__ctor_m5849,
	DSASignatureDeformatter_SetHashAlgorithm_m9437,
	DSASignatureDeformatter_SetKey_m9438,
	DSASignatureDeformatter_VerifySignature_m9439,
	DSASignatureFormatter__ctor_m9440,
	DSASignatureFormatter_CreateSignature_m9441,
	DSASignatureFormatter_SetHashAlgorithm_m9442,
	DSASignatureFormatter_SetKey_m9443,
	HMAC__ctor_m9444,
	HMAC_get_BlockSizeValue_m9445,
	HMAC_set_BlockSizeValue_m9446,
	HMAC_set_HashName_m9447,
	HMAC_get_Key_m9448,
	HMAC_set_Key_m9449,
	HMAC_get_Block_m9450,
	HMAC_KeySetup_m9451,
	HMAC_Dispose_m9452,
	HMAC_HashCore_m9453,
	HMAC_HashFinal_m9454,
	HMAC_Initialize_m9455,
	HMAC_Create_m5842,
	HMAC_Create_m9456,
	HMACMD5__ctor_m9457,
	HMACMD5__ctor_m9458,
	HMACRIPEMD160__ctor_m9459,
	HMACRIPEMD160__ctor_m9460,
	HMACSHA1__ctor_m9461,
	HMACSHA1__ctor_m9462,
	HMACSHA256__ctor_m9463,
	HMACSHA256__ctor_m9464,
	HMACSHA384__ctor_m9465,
	HMACSHA384__ctor_m9466,
	HMACSHA384__cctor_m9467,
	HMACSHA384_set_ProduceLegacyHmacValues_m9468,
	HMACSHA512__ctor_m9469,
	HMACSHA512__ctor_m9470,
	HMACSHA512__cctor_m9471,
	HMACSHA512_set_ProduceLegacyHmacValues_m9472,
	HashAlgorithm__ctor_m5827,
	HashAlgorithm_System_IDisposable_Dispose_m9473,
	HashAlgorithm_get_CanReuseTransform_m9474,
	HashAlgorithm_ComputeHash_m4947,
	HashAlgorithm_ComputeHash_m5837,
	HashAlgorithm_Create_m5836,
	HashAlgorithm_get_Hash_m9475,
	HashAlgorithm_get_HashSize_m9476,
	HashAlgorithm_Dispose_m9477,
	HashAlgorithm_TransformBlock_m9478,
	HashAlgorithm_TransformFinalBlock_m9479,
	KeySizes__ctor_m3903,
	KeySizes_get_MaxSize_m9480,
	KeySizes_get_MinSize_m9481,
	KeySizes_get_SkipSize_m9482,
	KeySizes_IsLegal_m9483,
	KeySizes_IsLegalKeySize_m9484,
	KeyedHashAlgorithm__ctor_m5863,
	KeyedHashAlgorithm_Finalize_m9485,
	KeyedHashAlgorithm_get_Key_m9486,
	KeyedHashAlgorithm_set_Key_m9487,
	KeyedHashAlgorithm_Dispose_m9488,
	KeyedHashAlgorithm_ZeroizeKey_m9489,
	MACTripleDES__ctor_m9490,
	MACTripleDES_Setup_m9491,
	MACTripleDES_Finalize_m9492,
	MACTripleDES_Dispose_m9493,
	MACTripleDES_Initialize_m9494,
	MACTripleDES_HashCore_m9495,
	MACTripleDES_HashFinal_m9496,
	MD5__ctor_m9497,
	MD5_Create_m5846,
	MD5_Create_m9498,
	MD5CryptoServiceProvider__ctor_m9499,
	MD5CryptoServiceProvider__cctor_m9500,
	MD5CryptoServiceProvider_Finalize_m9501,
	MD5CryptoServiceProvider_Dispose_m9502,
	MD5CryptoServiceProvider_HashCore_m9503,
	MD5CryptoServiceProvider_HashFinal_m9504,
	MD5CryptoServiceProvider_Initialize_m9505,
	MD5CryptoServiceProvider_ProcessBlock_m9506,
	MD5CryptoServiceProvider_ProcessFinalBlock_m9507,
	MD5CryptoServiceProvider_AddLength_m9508,
	RC2__ctor_m9509,
	RC2_Create_m5866,
	RC2_Create_m9510,
	RC2_get_EffectiveKeySize_m9511,
	RC2_get_KeySize_m9512,
	RC2_set_KeySize_m9513,
	RC2CryptoServiceProvider__ctor_m9514,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m9515,
	RC2CryptoServiceProvider_CreateDecryptor_m9516,
	RC2CryptoServiceProvider_CreateEncryptor_m9517,
	RC2CryptoServiceProvider_GenerateIV_m9518,
	RC2CryptoServiceProvider_GenerateKey_m9519,
	RC2Transform__ctor_m9520,
	RC2Transform__cctor_m9521,
	RC2Transform_ECB_m9522,
	RIPEMD160__ctor_m9523,
	RIPEMD160Managed__ctor_m9524,
	RIPEMD160Managed_Initialize_m9525,
	RIPEMD160Managed_HashCore_m9526,
	RIPEMD160Managed_HashFinal_m9527,
	RIPEMD160Managed_Finalize_m9528,
	RIPEMD160Managed_ProcessBlock_m9529,
	RIPEMD160Managed_Compress_m9530,
	RIPEMD160Managed_CompressFinal_m9531,
	RIPEMD160Managed_ROL_m9532,
	RIPEMD160Managed_F_m9533,
	RIPEMD160Managed_G_m9534,
	RIPEMD160Managed_H_m9535,
	RIPEMD160Managed_I_m9536,
	RIPEMD160Managed_J_m9537,
	RIPEMD160Managed_FF_m9538,
	RIPEMD160Managed_GG_m9539,
	RIPEMD160Managed_HH_m9540,
	RIPEMD160Managed_II_m9541,
	RIPEMD160Managed_JJ_m9542,
	RIPEMD160Managed_FFF_m9543,
	RIPEMD160Managed_GGG_m9544,
	RIPEMD160Managed_HHH_m9545,
	RIPEMD160Managed_III_m9546,
	RIPEMD160Managed_JJJ_m9547,
	RNGCryptoServiceProvider__ctor_m9548,
	RNGCryptoServiceProvider__cctor_m9549,
	RNGCryptoServiceProvider_Check_m9550,
	RNGCryptoServiceProvider_RngOpen_m9551,
	RNGCryptoServiceProvider_RngInitialize_m9552,
	RNGCryptoServiceProvider_RngGetBytes_m9553,
	RNGCryptoServiceProvider_RngClose_m9554,
	RNGCryptoServiceProvider_GetBytes_m9555,
	RNGCryptoServiceProvider_GetNonZeroBytes_m9556,
	RNGCryptoServiceProvider_Finalize_m9557,
	RSA__ctor_m5832,
	RSA_Create_m4863,
	RSA_Create_m9558,
	RSA_ZeroizePrivateKey_m9559,
	RSA_FromXmlString_m9560,
	RSA_ToXmlString_m9561,
	RSACryptoServiceProvider__ctor_m9562,
	RSACryptoServiceProvider__ctor_m5831,
	RSACryptoServiceProvider__ctor_m4874,
	RSACryptoServiceProvider__cctor_m9563,
	RSACryptoServiceProvider_Common_m9564,
	RSACryptoServiceProvider_Finalize_m9565,
	RSACryptoServiceProvider_get_KeySize_m9566,
	RSACryptoServiceProvider_get_PublicOnly_m4861,
	RSACryptoServiceProvider_DecryptValue_m9567,
	RSACryptoServiceProvider_EncryptValue_m9568,
	RSACryptoServiceProvider_ExportParameters_m9569,
	RSACryptoServiceProvider_ImportParameters_m9570,
	RSACryptoServiceProvider_Dispose_m9571,
	RSACryptoServiceProvider_OnKeyGenerated_m9572,
	RSAPKCS1KeyExchangeFormatter__ctor_m5891,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m9573,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m9574,
	RSAPKCS1SignatureDeformatter__ctor_m9575,
	RSAPKCS1SignatureDeformatter__ctor_m5850,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m9576,
	RSAPKCS1SignatureDeformatter_SetKey_m9577,
	RSAPKCS1SignatureDeformatter_VerifySignature_m9578,
	RSAPKCS1SignatureFormatter__ctor_m9579,
	RSAPKCS1SignatureFormatter_CreateSignature_m9580,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m9581,
	RSAPKCS1SignatureFormatter_SetKey_m9582,
	RandomNumberGenerator__ctor_m9583,
	RandomNumberGenerator_Create_m3891,
	RandomNumberGenerator_Create_m9584,
	Rijndael__ctor_m9585,
	Rijndael_Create_m5868,
	Rijndael_Create_m9586,
	RijndaelManaged__ctor_m9587,
	RijndaelManaged_GenerateIV_m9588,
	RijndaelManaged_GenerateKey_m9589,
	RijndaelManaged_CreateDecryptor_m9590,
	RijndaelManaged_CreateEncryptor_m9591,
	RijndaelTransform__ctor_m9592,
	RijndaelTransform__cctor_m9593,
	RijndaelTransform_Clear_m9594,
	RijndaelTransform_ECB_m9595,
	RijndaelTransform_SubByte_m9596,
	RijndaelTransform_Encrypt128_m9597,
	RijndaelTransform_Encrypt192_m9598,
	RijndaelTransform_Encrypt256_m9599,
	RijndaelTransform_Decrypt128_m9600,
	RijndaelTransform_Decrypt192_m9601,
	RijndaelTransform_Decrypt256_m9602,
	RijndaelManagedTransform__ctor_m9603,
	RijndaelManagedTransform_System_IDisposable_Dispose_m9604,
	RijndaelManagedTransform_get_CanReuseTransform_m9605,
	RijndaelManagedTransform_TransformBlock_m9606,
	RijndaelManagedTransform_TransformFinalBlock_m9607,
	SHA1__ctor_m9608,
	SHA1_Create_m4946,
	SHA1_Create_m9609,
	SHA1Internal__ctor_m9610,
	SHA1Internal_HashCore_m9611,
	SHA1Internal_HashFinal_m9612,
	SHA1Internal_Initialize_m9613,
	SHA1Internal_ProcessBlock_m9614,
	SHA1Internal_InitialiseBuff_m9615,
	SHA1Internal_FillBuff_m9616,
	SHA1Internal_ProcessFinalBlock_m9617,
	SHA1Internal_AddLength_m9618,
	SHA1CryptoServiceProvider__ctor_m9619,
	SHA1CryptoServiceProvider_Finalize_m9620,
	SHA1CryptoServiceProvider_Dispose_m9621,
	SHA1CryptoServiceProvider_HashCore_m9622,
	SHA1CryptoServiceProvider_HashFinal_m9623,
	SHA1CryptoServiceProvider_Initialize_m9624,
	SHA1Managed__ctor_m9625,
	SHA1Managed_HashCore_m9626,
	SHA1Managed_HashFinal_m9627,
	SHA1Managed_Initialize_m9628,
	SHA256__ctor_m9629,
	SHA256_Create_m5847,
	SHA256_Create_m9630,
	SHA256Managed__ctor_m9631,
	SHA256Managed_HashCore_m9632,
	SHA256Managed_HashFinal_m9633,
	SHA256Managed_Initialize_m9634,
	SHA256Managed_ProcessBlock_m9635,
	SHA256Managed_ProcessFinalBlock_m9636,
	SHA256Managed_AddLength_m9637,
	SHA384__ctor_m9638,
	SHA384Managed__ctor_m9639,
	SHA384Managed_Initialize_m9640,
	SHA384Managed_Initialize_m9641,
	SHA384Managed_HashCore_m9642,
	SHA384Managed_HashFinal_m9643,
	SHA384Managed_update_m9644,
	SHA384Managed_processWord_m9645,
	SHA384Managed_unpackWord_m9646,
	SHA384Managed_adjustByteCounts_m9647,
	SHA384Managed_processLength_m9648,
	SHA384Managed_processBlock_m9649,
	SHA512__ctor_m9650,
	SHA512Managed__ctor_m9651,
	SHA512Managed_Initialize_m9652,
	SHA512Managed_Initialize_m9653,
	SHA512Managed_HashCore_m9654,
	SHA512Managed_HashFinal_m9655,
	SHA512Managed_update_m9656,
	SHA512Managed_processWord_m9657,
	SHA512Managed_unpackWord_m9658,
	SHA512Managed_adjustByteCounts_m9659,
	SHA512Managed_processLength_m9660,
	SHA512Managed_processBlock_m9661,
	SHA512Managed_rotateRight_m9662,
	SHA512Managed_Ch_m9663,
	SHA512Managed_Maj_m9664,
	SHA512Managed_Sum0_m9665,
	SHA512Managed_Sum1_m9666,
	SHA512Managed_Sigma0_m9667,
	SHA512Managed_Sigma1_m9668,
	SHAConstants__cctor_m9669,
	SignatureDescription__ctor_m9670,
	SignatureDescription_set_DeformatterAlgorithm_m9671,
	SignatureDescription_set_DigestAlgorithm_m9672,
	SignatureDescription_set_FormatterAlgorithm_m9673,
	SignatureDescription_set_KeyAlgorithm_m9674,
	DSASignatureDescription__ctor_m9675,
	RSAPKCS1SHA1SignatureDescription__ctor_m9676,
	SymmetricAlgorithm__ctor_m3902,
	SymmetricAlgorithm_System_IDisposable_Dispose_m9677,
	SymmetricAlgorithm_Finalize_m5825,
	SymmetricAlgorithm_Clear_m5841,
	SymmetricAlgorithm_Dispose_m3910,
	SymmetricAlgorithm_get_BlockSize_m9678,
	SymmetricAlgorithm_set_BlockSize_m9679,
	SymmetricAlgorithm_get_FeedbackSize_m9680,
	SymmetricAlgorithm_get_IV_m3904,
	SymmetricAlgorithm_set_IV_m3905,
	SymmetricAlgorithm_get_Key_m3906,
	SymmetricAlgorithm_set_Key_m3907,
	SymmetricAlgorithm_get_KeySize_m3908,
	SymmetricAlgorithm_set_KeySize_m3909,
	SymmetricAlgorithm_get_LegalKeySizes_m9681,
	SymmetricAlgorithm_get_Mode_m9682,
	SymmetricAlgorithm_set_Mode_m9683,
	SymmetricAlgorithm_get_Padding_m9684,
	SymmetricAlgorithm_set_Padding_m9685,
	SymmetricAlgorithm_CreateDecryptor_m9686,
	SymmetricAlgorithm_CreateEncryptor_m9687,
	SymmetricAlgorithm_Create_m5840,
	ToBase64Transform_System_IDisposable_Dispose_m9688,
	ToBase64Transform_Finalize_m9689,
	ToBase64Transform_get_CanReuseTransform_m9690,
	ToBase64Transform_get_InputBlockSize_m9691,
	ToBase64Transform_get_OutputBlockSize_m9692,
	ToBase64Transform_Dispose_m9693,
	ToBase64Transform_TransformBlock_m9694,
	ToBase64Transform_InternalTransformBlock_m9695,
	ToBase64Transform_TransformFinalBlock_m9696,
	ToBase64Transform_InternalTransformFinalBlock_m9697,
	TripleDES__ctor_m9698,
	TripleDES_get_Key_m9699,
	TripleDES_set_Key_m9700,
	TripleDES_IsWeakKey_m9701,
	TripleDES_Create_m5867,
	TripleDES_Create_m9702,
	TripleDESCryptoServiceProvider__ctor_m9703,
	TripleDESCryptoServiceProvider_GenerateIV_m9704,
	TripleDESCryptoServiceProvider_GenerateKey_m9705,
	TripleDESCryptoServiceProvider_CreateDecryptor_m9706,
	TripleDESCryptoServiceProvider_CreateEncryptor_m9707,
	TripleDESTransform__ctor_m9708,
	TripleDESTransform_ECB_m9709,
	TripleDESTransform_GetStrongKey_m9710,
	SecurityPermission__ctor_m9711,
	SecurityPermission_set_Flags_m9712,
	SecurityPermission_IsUnrestricted_m9713,
	SecurityPermission_IsSubsetOf_m9714,
	SecurityPermission_ToXml_m9715,
	SecurityPermission_IsEmpty_m9716,
	SecurityPermission_Cast_m9717,
	StrongNamePublicKeyBlob_Equals_m9718,
	StrongNamePublicKeyBlob_GetHashCode_m9719,
	StrongNamePublicKeyBlob_ToString_m9720,
	ApplicationTrust__ctor_m9721,
	EvidenceEnumerator__ctor_m9722,
	EvidenceEnumerator_MoveNext_m9723,
	EvidenceEnumerator_Reset_m9724,
	EvidenceEnumerator_get_Current_m9725,
	Evidence__ctor_m9726,
	Evidence_get_Count_m9727,
	Evidence_get_IsSynchronized_m9728,
	Evidence_get_SyncRoot_m9729,
	Evidence_get_HostEvidenceList_m9730,
	Evidence_get_AssemblyEvidenceList_m9731,
	Evidence_CopyTo_m9732,
	Evidence_Equals_m9733,
	Evidence_GetEnumerator_m9734,
	Evidence_GetHashCode_m9735,
	Hash__ctor_m9736,
	Hash__ctor_m9737,
	Hash_GetObjectData_m9738,
	Hash_ToString_m9739,
	Hash_GetData_m9740,
	StrongName_get_Name_m9741,
	StrongName_get_PublicKey_m9742,
	StrongName_get_Version_m9743,
	StrongName_Equals_m9744,
	StrongName_GetHashCode_m9745,
	StrongName_ToString_m9746,
	WindowsIdentity__ctor_m9747,
	WindowsIdentity__cctor_m9748,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9749,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9750,
	WindowsIdentity_Dispose_m9751,
	WindowsIdentity_GetCurrentToken_m9752,
	WindowsIdentity_GetTokenName_m9753,
	CodeAccessPermission__ctor_m9754,
	CodeAccessPermission_Equals_m9755,
	CodeAccessPermission_GetHashCode_m9756,
	CodeAccessPermission_ToString_m9757,
	CodeAccessPermission_Element_m9758,
	CodeAccessPermission_ThrowInvalidPermission_m9759,
	PermissionSet__ctor_m9760,
	PermissionSet__ctor_m9761,
	PermissionSet_set_DeclarativeSecurity_m9762,
	PermissionSet_CreateFromBinaryFormat_m9763,
	SecurityContext__ctor_m9764,
	SecurityContext__ctor_m9765,
	SecurityContext_Capture_m9766,
	SecurityContext_get_FlowSuppressed_m9767,
	SecurityContext_get_CompressedStack_m9768,
	SecurityAttribute__ctor_m9769,
	SecurityAttribute_get_Name_m9770,
	SecurityAttribute_get_Value_m9771,
	SecurityElement__ctor_m9772,
	SecurityElement__ctor_m9773,
	SecurityElement__cctor_m9774,
	SecurityElement_get_Children_m9775,
	SecurityElement_get_Tag_m9776,
	SecurityElement_set_Text_m9777,
	SecurityElement_AddAttribute_m9778,
	SecurityElement_AddChild_m9779,
	SecurityElement_Escape_m9780,
	SecurityElement_Unescape_m9781,
	SecurityElement_IsValidAttributeName_m9782,
	SecurityElement_IsValidAttributeValue_m9783,
	SecurityElement_IsValidTag_m9784,
	SecurityElement_IsValidText_m9785,
	SecurityElement_SearchForChildByTag_m9786,
	SecurityElement_ToString_m9787,
	SecurityElement_ToXml_m9788,
	SecurityElement_GetAttribute_m9789,
	SecurityException__ctor_m9790,
	SecurityException__ctor_m9791,
	SecurityException__ctor_m9792,
	SecurityException_get_Demanded_m9793,
	SecurityException_get_FirstPermissionThatFailed_m9794,
	SecurityException_get_PermissionState_m9795,
	SecurityException_get_PermissionType_m9796,
	SecurityException_get_GrantedSet_m9797,
	SecurityException_get_RefusedSet_m9798,
	SecurityException_GetObjectData_m9799,
	SecurityException_ToString_m9800,
	SecurityFrame__ctor_m9801,
	SecurityFrame__GetSecurityStack_m9802,
	SecurityFrame_InitFromRuntimeFrame_m9803,
	SecurityFrame_get_Assembly_m9804,
	SecurityFrame_get_Domain_m9805,
	SecurityFrame_ToString_m9806,
	SecurityFrame_GetStack_m9807,
	SecurityManager__cctor_m9808,
	SecurityManager_get_SecurityEnabled_m9809,
	SecurityManager_Decode_m9810,
	SecurityManager_Decode_m9811,
	SecuritySafeCriticalAttribute__ctor_m9812,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m9813,
	UnverifiableCodeAttribute__ctor_m9814,
	ASCIIEncoding__ctor_m9815,
	ASCIIEncoding_GetByteCount_m9816,
	ASCIIEncoding_GetByteCount_m9817,
	ASCIIEncoding_GetBytes_m9818,
	ASCIIEncoding_GetBytes_m9819,
	ASCIIEncoding_GetBytes_m9820,
	ASCIIEncoding_GetBytes_m9821,
	ASCIIEncoding_GetCharCount_m9822,
	ASCIIEncoding_GetChars_m9823,
	ASCIIEncoding_GetChars_m9824,
	ASCIIEncoding_GetMaxByteCount_m9825,
	ASCIIEncoding_GetMaxCharCount_m9826,
	ASCIIEncoding_GetString_m9827,
	ASCIIEncoding_GetBytes_m9828,
	ASCIIEncoding_GetByteCount_m9829,
	ASCIIEncoding_GetDecoder_m9830,
	Decoder__ctor_m9831,
	Decoder_set_Fallback_m9832,
	Decoder_get_FallbackBuffer_m9833,
	DecoderExceptionFallback__ctor_m9834,
	DecoderExceptionFallback_CreateFallbackBuffer_m9835,
	DecoderExceptionFallback_Equals_m9836,
	DecoderExceptionFallback_GetHashCode_m9837,
	DecoderExceptionFallbackBuffer__ctor_m9838,
	DecoderExceptionFallbackBuffer_get_Remaining_m9839,
	DecoderExceptionFallbackBuffer_Fallback_m9840,
	DecoderExceptionFallbackBuffer_GetNextChar_m9841,
	DecoderFallback__ctor_m9842,
	DecoderFallback__cctor_m9843,
	DecoderFallback_get_ExceptionFallback_m9844,
	DecoderFallback_get_ReplacementFallback_m9845,
	DecoderFallback_get_StandardSafeFallback_m9846,
	DecoderFallbackBuffer__ctor_m9847,
	DecoderFallbackBuffer_Reset_m9848,
	DecoderFallbackException__ctor_m9849,
	DecoderFallbackException__ctor_m9850,
	DecoderFallbackException__ctor_m9851,
	DecoderReplacementFallback__ctor_m9852,
	DecoderReplacementFallback__ctor_m9853,
	DecoderReplacementFallback_get_DefaultString_m9854,
	DecoderReplacementFallback_CreateFallbackBuffer_m9855,
	DecoderReplacementFallback_Equals_m9856,
	DecoderReplacementFallback_GetHashCode_m9857,
	DecoderReplacementFallbackBuffer__ctor_m9858,
	DecoderReplacementFallbackBuffer_get_Remaining_m9859,
	DecoderReplacementFallbackBuffer_Fallback_m9860,
	DecoderReplacementFallbackBuffer_GetNextChar_m9861,
	DecoderReplacementFallbackBuffer_Reset_m9862,
	EncoderExceptionFallback__ctor_m9863,
	EncoderExceptionFallback_CreateFallbackBuffer_m9864,
	EncoderExceptionFallback_Equals_m9865,
	EncoderExceptionFallback_GetHashCode_m9866,
	EncoderExceptionFallbackBuffer__ctor_m9867,
	EncoderExceptionFallbackBuffer_get_Remaining_m9868,
	EncoderExceptionFallbackBuffer_Fallback_m9869,
	EncoderExceptionFallbackBuffer_Fallback_m9870,
	EncoderExceptionFallbackBuffer_GetNextChar_m9871,
	EncoderFallback__ctor_m9872,
	EncoderFallback__cctor_m9873,
	EncoderFallback_get_ExceptionFallback_m9874,
	EncoderFallback_get_ReplacementFallback_m9875,
	EncoderFallback_get_StandardSafeFallback_m9876,
	EncoderFallbackBuffer__ctor_m9877,
	EncoderFallbackException__ctor_m9878,
	EncoderFallbackException__ctor_m9879,
	EncoderFallbackException__ctor_m9880,
	EncoderFallbackException__ctor_m9881,
	EncoderReplacementFallback__ctor_m9882,
	EncoderReplacementFallback__ctor_m9883,
	EncoderReplacementFallback_get_DefaultString_m9884,
	EncoderReplacementFallback_CreateFallbackBuffer_m9885,
	EncoderReplacementFallback_Equals_m9886,
	EncoderReplacementFallback_GetHashCode_m9887,
	EncoderReplacementFallbackBuffer__ctor_m9888,
	EncoderReplacementFallbackBuffer_get_Remaining_m9889,
	EncoderReplacementFallbackBuffer_Fallback_m9890,
	EncoderReplacementFallbackBuffer_Fallback_m9891,
	EncoderReplacementFallbackBuffer_Fallback_m9892,
	EncoderReplacementFallbackBuffer_GetNextChar_m9893,
	ForwardingDecoder__ctor_m9894,
	ForwardingDecoder_GetChars_m9895,
	Encoding__ctor_m9896,
	Encoding__ctor_m9897,
	Encoding__cctor_m9898,
	Encoding___m9899,
	Encoding_get_IsReadOnly_m9900,
	Encoding_get_DecoderFallback_m9901,
	Encoding_set_DecoderFallback_m9902,
	Encoding_get_EncoderFallback_m9903,
	Encoding_SetFallbackInternal_m9904,
	Encoding_Equals_m9905,
	Encoding_GetByteCount_m9906,
	Encoding_GetByteCount_m9907,
	Encoding_GetBytes_m9908,
	Encoding_GetBytes_m9909,
	Encoding_GetBytes_m9910,
	Encoding_GetBytes_m9911,
	Encoding_GetChars_m9912,
	Encoding_GetDecoder_m9913,
	Encoding_InvokeI18N_m9914,
	Encoding_GetEncoding_m9915,
	Encoding_Clone_m9916,
	Encoding_GetEncoding_m9917,
	Encoding_GetHashCode_m9918,
	Encoding_GetPreamble_m9919,
	Encoding_GetString_m9920,
	Encoding_GetString_m9921,
	Encoding_get_ASCII_m4949,
	Encoding_get_BigEndianUnicode_m5838,
	Encoding_InternalCodePage_m9922,
	Encoding_get_Default_m9923,
	Encoding_get_ISOLatin1_m9924,
	Encoding_get_UTF7_m5843,
	Encoding_get_UTF8_m585,
	Encoding_get_UTF8Unmarked_m9925,
	Encoding_get_UTF8UnmarkedUnsafe_m9926,
	Encoding_get_Unicode_m9927,
	Encoding_get_UTF32_m9928,
	Encoding_get_BigEndianUTF32_m9929,
	Encoding_GetByteCount_m9930,
	Encoding_GetBytes_m9931,
	Latin1Encoding__ctor_m9932,
	Latin1Encoding_GetByteCount_m9933,
	Latin1Encoding_GetByteCount_m9934,
	Latin1Encoding_GetBytes_m9935,
	Latin1Encoding_GetBytes_m9936,
	Latin1Encoding_GetBytes_m9937,
	Latin1Encoding_GetBytes_m9938,
	Latin1Encoding_GetCharCount_m9939,
	Latin1Encoding_GetChars_m9940,
	Latin1Encoding_GetMaxByteCount_m9941,
	Latin1Encoding_GetMaxCharCount_m9942,
	Latin1Encoding_GetString_m9943,
	Latin1Encoding_GetString_m9944,
	StringBuilder__ctor_m9945,
	StringBuilder__ctor_m9946,
	StringBuilder__ctor_m3546,
	StringBuilder__ctor_m2205,
	StringBuilder__ctor_m3588,
	StringBuilder__ctor_m4833,
	StringBuilder__ctor_m9947,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9948,
	StringBuilder_get_Capacity_m9949,
	StringBuilder_set_Capacity_m9950,
	StringBuilder_get_Length_m4939,
	StringBuilder_set_Length_m4976,
	StringBuilder_get_Chars_m9951,
	StringBuilder_set_Chars_m9952,
	StringBuilder_ToString_m2209,
	StringBuilder_ToString_m9953,
	StringBuilder_Remove_m9954,
	StringBuilder_Replace_m9955,
	StringBuilder_Replace_m9956,
	StringBuilder_Append_m2208,
	StringBuilder_Append_m4886,
	StringBuilder_Append_m4850,
	StringBuilder_Append_m3829,
	StringBuilder_Append_m4834,
	StringBuilder_Append_m9957,
	StringBuilder_Append_m9958,
	StringBuilder_Append_m4952,
	StringBuilder_AppendLine_m3548,
	StringBuilder_AppendLine_m3547,
	StringBuilder_AppendFormat_m5819,
	StringBuilder_AppendFormat_m9959,
	StringBuilder_AppendFormat_m4849,
	StringBuilder_AppendFormat_m4906,
	StringBuilder_AppendFormat_m4909,
	StringBuilder_Insert_m9960,
	StringBuilder_Insert_m9961,
	StringBuilder_Insert_m9962,
	StringBuilder_InternalEnsureCapacity_m9963,
	UTF32Decoder__ctor_m9964,
	UTF32Decoder_GetChars_m9965,
	UTF32Encoding__ctor_m9966,
	UTF32Encoding__ctor_m9967,
	UTF32Encoding__ctor_m9968,
	UTF32Encoding_GetByteCount_m9969,
	UTF32Encoding_GetBytes_m9970,
	UTF32Encoding_GetCharCount_m9971,
	UTF32Encoding_GetChars_m9972,
	UTF32Encoding_GetMaxByteCount_m9973,
	UTF32Encoding_GetMaxCharCount_m9974,
	UTF32Encoding_GetDecoder_m9975,
	UTF32Encoding_GetPreamble_m9976,
	UTF32Encoding_Equals_m9977,
	UTF32Encoding_GetHashCode_m9978,
	UTF32Encoding_GetByteCount_m9979,
	UTF32Encoding_GetByteCount_m9980,
	UTF32Encoding_GetBytes_m9981,
	UTF32Encoding_GetBytes_m9982,
	UTF32Encoding_GetString_m9983,
	UTF7Decoder__ctor_m9984,
	UTF7Decoder_GetChars_m9985,
	UTF7Encoding__ctor_m9986,
	UTF7Encoding__ctor_m9987,
	UTF7Encoding__cctor_m9988,
	UTF7Encoding_GetHashCode_m9989,
	UTF7Encoding_Equals_m9990,
	UTF7Encoding_InternalGetByteCount_m9991,
	UTF7Encoding_GetByteCount_m9992,
	UTF7Encoding_InternalGetBytes_m9993,
	UTF7Encoding_GetBytes_m9994,
	UTF7Encoding_InternalGetCharCount_m9995,
	UTF7Encoding_GetCharCount_m9996,
	UTF7Encoding_InternalGetChars_m9997,
	UTF7Encoding_GetChars_m9998,
	UTF7Encoding_GetMaxByteCount_m9999,
	UTF7Encoding_GetMaxCharCount_m10000,
	UTF7Encoding_GetDecoder_m10001,
	UTF7Encoding_GetByteCount_m10002,
	UTF7Encoding_GetByteCount_m10003,
	UTF7Encoding_GetBytes_m10004,
	UTF7Encoding_GetBytes_m10005,
	UTF7Encoding_GetString_m10006,
	UTF8Decoder__ctor_m10007,
	UTF8Decoder_GetChars_m10008,
	UTF8Encoding__ctor_m10009,
	UTF8Encoding__ctor_m10010,
	UTF8Encoding__ctor_m10011,
	UTF8Encoding_InternalGetByteCount_m10012,
	UTF8Encoding_InternalGetByteCount_m10013,
	UTF8Encoding_GetByteCount_m10014,
	UTF8Encoding_GetByteCount_m10015,
	UTF8Encoding_InternalGetBytes_m10016,
	UTF8Encoding_InternalGetBytes_m10017,
	UTF8Encoding_GetBytes_m10018,
	UTF8Encoding_GetBytes_m10019,
	UTF8Encoding_GetBytes_m10020,
	UTF8Encoding_InternalGetCharCount_m10021,
	UTF8Encoding_InternalGetCharCount_m10022,
	UTF8Encoding_Fallback_m10023,
	UTF8Encoding_Fallback_m10024,
	UTF8Encoding_GetCharCount_m10025,
	UTF8Encoding_InternalGetChars_m10026,
	UTF8Encoding_InternalGetChars_m10027,
	UTF8Encoding_GetChars_m10028,
	UTF8Encoding_GetMaxByteCount_m10029,
	UTF8Encoding_GetMaxCharCount_m10030,
	UTF8Encoding_GetDecoder_m10031,
	UTF8Encoding_GetPreamble_m10032,
	UTF8Encoding_Equals_m10033,
	UTF8Encoding_GetHashCode_m10034,
	UTF8Encoding_GetByteCount_m10035,
	UTF8Encoding_GetString_m10036,
	UnicodeDecoder__ctor_m10037,
	UnicodeDecoder_GetChars_m10038,
	UnicodeEncoding__ctor_m10039,
	UnicodeEncoding__ctor_m10040,
	UnicodeEncoding__ctor_m10041,
	UnicodeEncoding_GetByteCount_m10042,
	UnicodeEncoding_GetByteCount_m10043,
	UnicodeEncoding_GetByteCount_m10044,
	UnicodeEncoding_GetBytes_m10045,
	UnicodeEncoding_GetBytes_m10046,
	UnicodeEncoding_GetBytes_m10047,
	UnicodeEncoding_GetBytesInternal_m10048,
	UnicodeEncoding_GetCharCount_m10049,
	UnicodeEncoding_GetChars_m10050,
	UnicodeEncoding_GetString_m10051,
	UnicodeEncoding_GetCharsInternal_m10052,
	UnicodeEncoding_GetMaxByteCount_m10053,
	UnicodeEncoding_GetMaxCharCount_m10054,
	UnicodeEncoding_GetDecoder_m10055,
	UnicodeEncoding_GetPreamble_m10056,
	UnicodeEncoding_Equals_m10057,
	UnicodeEncoding_GetHashCode_m10058,
	UnicodeEncoding_CopyChars_m10059,
	CompressedStack__ctor_m10060,
	CompressedStack__ctor_m10061,
	CompressedStack_CreateCopy_m10062,
	CompressedStack_Capture_m10063,
	CompressedStack_GetObjectData_m10064,
	CompressedStack_IsEmpty_m10065,
	EventWaitHandle__ctor_m10066,
	EventWaitHandle_IsManualReset_m10067,
	EventWaitHandle_Reset_m5878,
	EventWaitHandle_Set_m5876,
	ExecutionContext__ctor_m10068,
	ExecutionContext__ctor_m10069,
	ExecutionContext__ctor_m10070,
	ExecutionContext_Capture_m10071,
	ExecutionContext_GetObjectData_m10072,
	ExecutionContext_get_SecurityContext_m10073,
	ExecutionContext_set_SecurityContext_m10074,
	ExecutionContext_get_FlowSuppressed_m10075,
	ExecutionContext_IsFlowSuppressed_m10076,
	Interlocked_CompareExchange_m3832,
	ManualResetEvent__ctor_m5875,
	Monitor_Enter_m572,
	Monitor_Exit_m575,
	Monitor_Monitor_pulse_m10077,
	Monitor_Monitor_test_synchronised_m10078,
	Monitor_Pulse_m10079,
	Monitor_Monitor_wait_m10080,
	Monitor_Wait_m10081,
	Mutex__ctor_m10082,
	Mutex_CreateMutex_internal_m10083,
	Mutex_ReleaseMutex_internal_m10084,
	Mutex_ReleaseMutex_m10085,
	NativeEventCalls_CreateEvent_internal_m10086,
	NativeEventCalls_SetEvent_internal_m10087,
	NativeEventCalls_ResetEvent_internal_m10088,
	NativeEventCalls_CloseEvent_internal_m10089,
	SynchronizationLockException__ctor_m10090,
	SynchronizationLockException__ctor_m10091,
	SynchronizationLockException__ctor_m10092,
	Thread__ctor_m10093,
	Thread__cctor_m10094,
	Thread_get_CurrentContext_m10095,
	Thread_CurrentThread_internal_m10096,
	Thread_get_CurrentThread_m10097,
	Thread_FreeLocalSlotValues_m10098,
	Thread_GetDomainID_m10099,
	Thread_Thread_internal_m10100,
	Thread_Thread_init_m10101,
	Thread_GetCachedCurrentCulture_m10102,
	Thread_GetSerializedCurrentCulture_m10103,
	Thread_SetCachedCurrentCulture_m10104,
	Thread_GetCachedCurrentUICulture_m10105,
	Thread_GetSerializedCurrentUICulture_m10106,
	Thread_SetCachedCurrentUICulture_m10107,
	Thread_get_CurrentCulture_m10108,
	Thread_get_CurrentUICulture_m10109,
	Thread_set_IsBackground_m10110,
	Thread_SetName_internal_m10111,
	Thread_set_Name_m10112,
	Thread_Start_m10113,
	Thread_Thread_free_internal_m10114,
	Thread_Finalize_m10115,
	Thread_SetState_m10116,
	Thread_ClrState_m10117,
	Thread_GetNewManagedId_m10118,
	Thread_GetNewManagedId_internal_m10119,
	Thread_get_ExecutionContext_m10120,
	Thread_get_ManagedThreadId_m10121,
	Thread_GetHashCode_m10122,
	Thread_GetCompressedStack_m10123,
	ThreadAbortException__ctor_m10124,
	ThreadAbortException__ctor_m10125,
	ThreadInterruptedException__ctor_m10126,
	ThreadInterruptedException__ctor_m10127,
	ThreadPool_QueueUserWorkItem_m10128,
	ThreadStateException__ctor_m10129,
	ThreadStateException__ctor_m10130,
	TimerComparer__ctor_m10131,
	TimerComparer_Compare_m10132,
	Scheduler__ctor_m10133,
	Scheduler__cctor_m10134,
	Scheduler_get_Instance_m10135,
	Scheduler_Remove_m10136,
	Scheduler_Change_m10137,
	Scheduler_Add_m10138,
	Scheduler_InternalRemove_m10139,
	Scheduler_SchedulerThread_m10140,
	Scheduler_ShrinkIfNeeded_m10141,
	Timer__cctor_m10142,
	Timer_Change_m10143,
	Timer_Dispose_m10144,
	Timer_Change_m10145,
	WaitHandle__ctor_m10146,
	WaitHandle__cctor_m10147,
	WaitHandle_System_IDisposable_Dispose_m10148,
	WaitHandle_get_Handle_m10149,
	WaitHandle_set_Handle_m10150,
	WaitHandle_WaitOne_internal_m10151,
	WaitHandle_Dispose_m10152,
	WaitHandle_WaitOne_m10153,
	WaitHandle_WaitOne_m10154,
	WaitHandle_CheckDisposed_m10155,
	WaitHandle_Finalize_m10156,
	AccessViolationException__ctor_m10157,
	AccessViolationException__ctor_m10158,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m10159,
	ActivationContext_Finalize_m10160,
	ActivationContext_Dispose_m10161,
	ActivationContext_Dispose_m10162,
	Activator_CreateInstance_m10163,
	Activator_CreateInstance_m10164,
	Activator_CreateInstance_m10165,
	Activator_CreateInstance_m10166,
	Activator_CreateInstance_m4860,
	Activator_CheckType_m10167,
	Activator_CheckAbstractType_m10168,
	Activator_CreateInstanceInternal_m10169,
	AppDomain_add_UnhandledException_m2141,
	AppDomain_remove_UnhandledException_m10170,
	AppDomain_getFriendlyName_m10171,
	AppDomain_getCurDomain_m10172,
	AppDomain_get_CurrentDomain_m2139,
	AppDomain_LoadAssembly_m10173,
	AppDomain_Load_m10174,
	AppDomain_Load_m10175,
	AppDomain_InternalSetContext_m10176,
	AppDomain_InternalGetContext_m10177,
	AppDomain_InternalGetDefaultContext_m10178,
	AppDomain_InternalGetProcessGuid_m10179,
	AppDomain_GetProcessGuid_m10180,
	AppDomain_ToString_m10181,
	AppDomain_DoTypeResolve_m10182,
	AppDomainSetup__ctor_m10183,
	ApplicationException__ctor_m10184,
	ApplicationException__ctor_m10185,
	ApplicationException__ctor_m10186,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m10187,
	ApplicationIdentity_ToString_m10188,
	ArgumentException__ctor_m10189,
	ArgumentException__ctor_m2137,
	ArgumentException__ctor_m4918,
	ArgumentException__ctor_m3899,
	ArgumentException__ctor_m10190,
	ArgumentException__ctor_m10191,
	ArgumentException_get_ParamName_m10192,
	ArgumentException_get_Message_m10193,
	ArgumentException_GetObjectData_m10194,
	ArgumentNullException__ctor_m10195,
	ArgumentNullException__ctor_m2226,
	ArgumentNullException__ctor_m4820,
	ArgumentNullException__ctor_m10196,
	ArgumentOutOfRangeException__ctor_m4951,
	ArgumentOutOfRangeException__ctor_m4823,
	ArgumentOutOfRangeException__ctor_m3898,
	ArgumentOutOfRangeException__ctor_m10197,
	ArgumentOutOfRangeException__ctor_m10198,
	ArgumentOutOfRangeException_get_Message_m10199,
	ArgumentOutOfRangeException_GetObjectData_m10200,
	ArithmeticException__ctor_m10201,
	ArithmeticException__ctor_m5818,
	ArithmeticException__ctor_m10202,
	ArrayTypeMismatchException__ctor_m10203,
	ArrayTypeMismatchException__ctor_m10204,
	ArrayTypeMismatchException__ctor_m10205,
	BitConverter__cctor_m10206,
	BitConverter_AmILittleEndian_m10207,
	BitConverter_DoubleWordsAreSwapped_m10208,
	BitConverter_DoubleToInt64Bits_m10209,
	BitConverter_GetBytes_m10210,
	BitConverter_GetBytes_m10211,
	BitConverter_PutBytes_m10212,
	BitConverter_ToInt64_m10213,
	BitConverter_ToString_m5873,
	BitConverter_ToString_m10214,
	Buffer_ByteLength_m10215,
	Buffer_BlockCopy_m3894,
	Buffer_ByteLengthInternal_m10216,
	Buffer_BlockCopyInternal_m10217,
	CharEnumerator__ctor_m10218,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m10219,
	CharEnumerator_System_IDisposable_Dispose_m10220,
	CharEnumerator_get_Current_m10221,
	CharEnumerator_Clone_m10222,
	CharEnumerator_MoveNext_m10223,
	CharEnumerator_Reset_m10224,
	Console__cctor_m10225,
	Console_SetEncodings_m10226,
	Console_get_Error_m4967,
	Console_Open_m10227,
	Console_OpenStandardError_m10228,
	Console_OpenStandardInput_m10229,
	Console_OpenStandardOutput_m10230,
	ContextBoundObject__ctor_m10231,
	Convert__cctor_m10232,
	Convert_InternalFromBase64String_m10233,
	Convert_FromBase64String_m5851,
	Convert_ToBase64String_m5835,
	Convert_ToBase64String_m10234,
	Convert_ToBoolean_m10235,
	Convert_ToBoolean_m10236,
	Convert_ToBoolean_m10237,
	Convert_ToBoolean_m10238,
	Convert_ToBoolean_m10239,
	Convert_ToBoolean_m10240,
	Convert_ToBoolean_m10241,
	Convert_ToBoolean_m10242,
	Convert_ToBoolean_m10243,
	Convert_ToBoolean_m10244,
	Convert_ToBoolean_m10245,
	Convert_ToBoolean_m10246,
	Convert_ToBoolean_m10247,
	Convert_ToBoolean_m10248,
	Convert_ToByte_m10249,
	Convert_ToByte_m10250,
	Convert_ToByte_m10251,
	Convert_ToByte_m10252,
	Convert_ToByte_m10253,
	Convert_ToByte_m10254,
	Convert_ToByte_m10255,
	Convert_ToByte_m10256,
	Convert_ToByte_m10257,
	Convert_ToByte_m10258,
	Convert_ToByte_m10259,
	Convert_ToByte_m10260,
	Convert_ToByte_m10261,
	Convert_ToByte_m10262,
	Convert_ToByte_m10263,
	Convert_ToChar_m5852,
	Convert_ToChar_m10264,
	Convert_ToChar_m10265,
	Convert_ToChar_m10266,
	Convert_ToChar_m10267,
	Convert_ToChar_m10268,
	Convert_ToChar_m10269,
	Convert_ToChar_m10270,
	Convert_ToChar_m10271,
	Convert_ToChar_m10272,
	Convert_ToChar_m10273,
	Convert_ToDateTime_m10274,
	Convert_ToDateTime_m10275,
	Convert_ToDateTime_m10276,
	Convert_ToDateTime_m10277,
	Convert_ToDateTime_m10278,
	Convert_ToDateTime_m10279,
	Convert_ToDateTime_m10280,
	Convert_ToDateTime_m10281,
	Convert_ToDateTime_m10282,
	Convert_ToDateTime_m10283,
	Convert_ToDecimal_m10284,
	Convert_ToDecimal_m10285,
	Convert_ToDecimal_m10286,
	Convert_ToDecimal_m10287,
	Convert_ToDecimal_m10288,
	Convert_ToDecimal_m10289,
	Convert_ToDecimal_m10290,
	Convert_ToDecimal_m10291,
	Convert_ToDecimal_m10292,
	Convert_ToDecimal_m10293,
	Convert_ToDecimal_m10294,
	Convert_ToDecimal_m10295,
	Convert_ToDecimal_m10296,
	Convert_ToDouble_m10297,
	Convert_ToDouble_m10298,
	Convert_ToDouble_m10299,
	Convert_ToDouble_m10300,
	Convert_ToDouble_m10301,
	Convert_ToDouble_m10302,
	Convert_ToDouble_m10303,
	Convert_ToDouble_m10304,
	Convert_ToDouble_m10305,
	Convert_ToDouble_m10306,
	Convert_ToDouble_m10307,
	Convert_ToDouble_m10308,
	Convert_ToDouble_m10309,
	Convert_ToDouble_m10310,
	Convert_ToInt16_m10311,
	Convert_ToInt16_m10312,
	Convert_ToInt16_m10313,
	Convert_ToInt16_m10314,
	Convert_ToInt16_m10315,
	Convert_ToInt16_m10316,
	Convert_ToInt16_m10317,
	Convert_ToInt16_m10318,
	Convert_ToInt16_m10319,
	Convert_ToInt16_m10320,
	Convert_ToInt16_m5823,
	Convert_ToInt16_m10321,
	Convert_ToInt16_m10322,
	Convert_ToInt16_m10323,
	Convert_ToInt16_m10324,
	Convert_ToInt16_m10325,
	Convert_ToInt32_m10326,
	Convert_ToInt32_m10327,
	Convert_ToInt32_m10328,
	Convert_ToInt32_m10329,
	Convert_ToInt32_m10330,
	Convert_ToInt32_m10331,
	Convert_ToInt32_m10332,
	Convert_ToInt32_m10333,
	Convert_ToInt32_m10334,
	Convert_ToInt32_m10335,
	Convert_ToInt32_m10336,
	Convert_ToInt32_m10337,
	Convert_ToInt32_m10338,
	Convert_ToInt32_m10339,
	Convert_ToInt32_m5861,
	Convert_ToInt64_m10340,
	Convert_ToInt64_m10341,
	Convert_ToInt64_m10342,
	Convert_ToInt64_m10343,
	Convert_ToInt64_m10344,
	Convert_ToInt64_m10345,
	Convert_ToInt64_m10346,
	Convert_ToInt64_m10347,
	Convert_ToInt64_m10348,
	Convert_ToInt64_m10349,
	Convert_ToInt64_m10350,
	Convert_ToInt64_m10351,
	Convert_ToInt64_m10352,
	Convert_ToInt64_m10353,
	Convert_ToInt64_m10354,
	Convert_ToInt64_m10355,
	Convert_ToInt64_m10356,
	Convert_ToSByte_m10357,
	Convert_ToSByte_m10358,
	Convert_ToSByte_m10359,
	Convert_ToSByte_m10360,
	Convert_ToSByte_m10361,
	Convert_ToSByte_m10362,
	Convert_ToSByte_m10363,
	Convert_ToSByte_m10364,
	Convert_ToSByte_m10365,
	Convert_ToSByte_m10366,
	Convert_ToSByte_m10367,
	Convert_ToSByte_m10368,
	Convert_ToSByte_m10369,
	Convert_ToSByte_m10370,
	Convert_ToSingle_m10371,
	Convert_ToSingle_m10372,
	Convert_ToSingle_m10373,
	Convert_ToSingle_m10374,
	Convert_ToSingle_m10375,
	Convert_ToSingle_m10376,
	Convert_ToSingle_m10377,
	Convert_ToSingle_m10378,
	Convert_ToSingle_m10379,
	Convert_ToSingle_m10380,
	Convert_ToSingle_m10381,
	Convert_ToSingle_m10382,
	Convert_ToSingle_m10383,
	Convert_ToSingle_m10384,
	Convert_ToString_m10385,
	Convert_ToString_m10386,
	Convert_ToUInt16_m10387,
	Convert_ToUInt16_m10388,
	Convert_ToUInt16_m10389,
	Convert_ToUInt16_m10390,
	Convert_ToUInt16_m10391,
	Convert_ToUInt16_m10392,
	Convert_ToUInt16_m10393,
	Convert_ToUInt16_m10394,
	Convert_ToUInt16_m10395,
	Convert_ToUInt16_m10396,
	Convert_ToUInt16_m10397,
	Convert_ToUInt16_m10398,
	Convert_ToUInt16_m10399,
	Convert_ToUInt16_m10400,
	Convert_ToUInt32_m2159,
	Convert_ToUInt32_m10401,
	Convert_ToUInt32_m10402,
	Convert_ToUInt32_m10403,
	Convert_ToUInt32_m10404,
	Convert_ToUInt32_m10405,
	Convert_ToUInt32_m10406,
	Convert_ToUInt32_m10407,
	Convert_ToUInt32_m10408,
	Convert_ToUInt32_m10409,
	Convert_ToUInt32_m10410,
	Convert_ToUInt32_m10411,
	Convert_ToUInt32_m10412,
	Convert_ToUInt32_m2158,
	Convert_ToUInt32_m10413,
	Convert_ToUInt64_m10414,
	Convert_ToUInt64_m10415,
	Convert_ToUInt64_m10416,
	Convert_ToUInt64_m10417,
	Convert_ToUInt64_m10418,
	Convert_ToUInt64_m10419,
	Convert_ToUInt64_m10420,
	Convert_ToUInt64_m10421,
	Convert_ToUInt64_m10422,
	Convert_ToUInt64_m10423,
	Convert_ToUInt64_m10424,
	Convert_ToUInt64_m10425,
	Convert_ToUInt64_m10426,
	Convert_ToUInt64_m10427,
	Convert_ToUInt64_m10428,
	Convert_ChangeType_m10429,
	Convert_ToType_m10430,
	DBNull__ctor_m10431,
	DBNull__ctor_m10432,
	DBNull__cctor_m10433,
	DBNull_System_IConvertible_ToBoolean_m10434,
	DBNull_System_IConvertible_ToByte_m10435,
	DBNull_System_IConvertible_ToChar_m10436,
	DBNull_System_IConvertible_ToDateTime_m10437,
	DBNull_System_IConvertible_ToDecimal_m10438,
	DBNull_System_IConvertible_ToDouble_m10439,
	DBNull_System_IConvertible_ToInt16_m10440,
	DBNull_System_IConvertible_ToInt32_m10441,
	DBNull_System_IConvertible_ToInt64_m10442,
	DBNull_System_IConvertible_ToSByte_m10443,
	DBNull_System_IConvertible_ToSingle_m10444,
	DBNull_System_IConvertible_ToType_m10445,
	DBNull_System_IConvertible_ToUInt16_m10446,
	DBNull_System_IConvertible_ToUInt32_m10447,
	DBNull_System_IConvertible_ToUInt64_m10448,
	DBNull_GetObjectData_m10449,
	DBNull_ToString_m10450,
	DBNull_ToString_m10451,
	DateTime__ctor_m10452,
	DateTime__ctor_m10453,
	DateTime__ctor_m2199,
	DateTime__ctor_m10454,
	DateTime__ctor_m10455,
	DateTime__cctor_m10456,
	DateTime_System_IConvertible_ToBoolean_m10457,
	DateTime_System_IConvertible_ToByte_m10458,
	DateTime_System_IConvertible_ToChar_m10459,
	DateTime_System_IConvertible_ToDateTime_m10460,
	DateTime_System_IConvertible_ToDecimal_m10461,
	DateTime_System_IConvertible_ToDouble_m10462,
	DateTime_System_IConvertible_ToInt16_m10463,
	DateTime_System_IConvertible_ToInt32_m10464,
	DateTime_System_IConvertible_ToInt64_m10465,
	DateTime_System_IConvertible_ToSByte_m10466,
	DateTime_System_IConvertible_ToSingle_m10467,
	DateTime_System_IConvertible_ToType_m10468,
	DateTime_System_IConvertible_ToUInt16_m10469,
	DateTime_System_IConvertible_ToUInt32_m10470,
	DateTime_System_IConvertible_ToUInt64_m10471,
	DateTime_AbsoluteDays_m10472,
	DateTime_FromTicks_m10473,
	DateTime_get_Month_m10474,
	DateTime_get_Day_m10475,
	DateTime_get_DayOfWeek_m10476,
	DateTime_get_Hour_m10477,
	DateTime_get_Minute_m10478,
	DateTime_get_Second_m10479,
	DateTime_GetTimeMonotonic_m10480,
	DateTime_GetNow_m10481,
	DateTime_get_Now_m2184,
	DateTime_get_Ticks_m5874,
	DateTime_get_Today_m10482,
	DateTime_get_UtcNow_m5848,
	DateTime_get_Year_m10483,
	DateTime_get_Kind_m10484,
	DateTime_Add_m10485,
	DateTime_AddTicks_m10486,
	DateTime_AddMilliseconds_m4851,
	DateTime_AddSeconds_m2200,
	DateTime_Compare_m10487,
	DateTime_CompareTo_m10488,
	DateTime_CompareTo_m10489,
	DateTime_Equals_m10490,
	DateTime_FromBinary_m10491,
	DateTime_SpecifyKind_m10492,
	DateTime_DaysInMonth_m10493,
	DateTime_Equals_m10494,
	DateTime_CheckDateTimeKind_m10495,
	DateTime_GetHashCode_m10496,
	DateTime_IsLeapYear_m10497,
	DateTime_Parse_m10498,
	DateTime_Parse_m10499,
	DateTime_CoreParse_m10500,
	DateTime_YearMonthDayFormats_m10501,
	DateTime__ParseNumber_m10502,
	DateTime__ParseEnum_m10503,
	DateTime__ParseString_m10504,
	DateTime__ParseAmPm_m10505,
	DateTime__ParseTimeSeparator_m10506,
	DateTime__ParseDateSeparator_m10507,
	DateTime_IsLetter_m10508,
	DateTime__DoParse_m10509,
	DateTime_ParseExact_m5824,
	DateTime_ParseExact_m10510,
	DateTime_CheckStyle_m10511,
	DateTime_ParseExact_m10512,
	DateTime_Subtract_m10513,
	DateTime_ToString_m10514,
	DateTime_ToString_m10515,
	DateTime_ToString_m10516,
	DateTime_ToLocalTime_m4889,
	DateTime_ToUniversalTime_m10517,
	DateTime_op_Addition_m10518,
	DateTime_op_Equality_m10519,
	DateTime_op_GreaterThan_m4916,
	DateTime_op_GreaterThanOrEqual_m4852,
	DateTime_op_Inequality_m10520,
	DateTime_op_LessThan_m4915,
	DateTime_op_LessThanOrEqual_m4914,
	DateTime_op_Subtraction_m10521,
	DateTimeOffset__ctor_m10522,
	DateTimeOffset__ctor_m10523,
	DateTimeOffset__ctor_m10524,
	DateTimeOffset__ctor_m10525,
	DateTimeOffset__cctor_m10526,
	DateTimeOffset_System_IComparable_CompareTo_m10527,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m10528,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10529,
	DateTimeOffset_CompareTo_m10530,
	DateTimeOffset_Equals_m10531,
	DateTimeOffset_Equals_m10532,
	DateTimeOffset_GetHashCode_m10533,
	DateTimeOffset_ToString_m10534,
	DateTimeOffset_ToString_m10535,
	DateTimeOffset_get_DateTime_m10536,
	DateTimeOffset_get_Offset_m10537,
	DateTimeOffset_get_UtcDateTime_m10538,
	DateTimeUtils_CountRepeat_m10539,
	DateTimeUtils_ZeroPad_m10540,
	DateTimeUtils_ParseQuotedString_m10541,
	DateTimeUtils_GetStandardPattern_m10542,
	DateTimeUtils_GetStandardPattern_m10543,
	DateTimeUtils_ToString_m10544,
	DateTimeUtils_ToString_m10545,
	DelegateEntry__ctor_m10546,
	DelegateEntry_DeserializeDelegate_m10547,
	DelegateSerializationHolder__ctor_m10548,
	DelegateSerializationHolder_GetDelegateData_m10549,
	DelegateSerializationHolder_GetObjectData_m10550,
	DelegateSerializationHolder_GetRealObject_m10551,
	DivideByZeroException__ctor_m10552,
	DivideByZeroException__ctor_m10553,
	DllNotFoundException__ctor_m10554,
	DllNotFoundException__ctor_m10555,
	EntryPointNotFoundException__ctor_m10556,
	EntryPointNotFoundException__ctor_m10557,
	SByteComparer__ctor_m10558,
	SByteComparer_Compare_m10559,
	SByteComparer_Compare_m10560,
	ShortComparer__ctor_m10561,
	ShortComparer_Compare_m10562,
	ShortComparer_Compare_m10563,
	IntComparer__ctor_m10564,
	IntComparer_Compare_m10565,
	IntComparer_Compare_m10566,
	LongComparer__ctor_m10567,
	LongComparer_Compare_m10568,
	LongComparer_Compare_m10569,
	MonoEnumInfo__ctor_m10570,
	MonoEnumInfo__cctor_m10571,
	MonoEnumInfo_get_enum_info_m10572,
	MonoEnumInfo_get_Cache_m10573,
	MonoEnumInfo_GetInfo_m10574,
	Environment_get_SocketSecurityEnabled_m10575,
	Environment_get_NewLine_m4876,
	Environment_get_Platform_m10576,
	Environment_GetOSVersionString_m10577,
	Environment_get_OSVersion_m10578,
	Environment_internalGetEnvironmentVariable_m10579,
	Environment_GetEnvironmentVariable_m5872,
	Environment_GetWindowsFolderPath_m10580,
	Environment_GetFolderPath_m5858,
	Environment_ReadXdgUserDir_m10581,
	Environment_InternalGetFolderPath_m10582,
	Environment_get_IsRunningOnWindows_m10583,
	Environment_GetMachineConfigPath_m10584,
	Environment_internalGetHome_m10585,
	EventArgs__ctor_m10586,
	EventArgs__cctor_m10587,
	ExecutionEngineException__ctor_m10588,
	ExecutionEngineException__ctor_m10589,
	FieldAccessException__ctor_m10590,
	FieldAccessException__ctor_m10591,
	FieldAccessException__ctor_m10592,
	FlagsAttribute__ctor_m10593,
	FormatException__ctor_m10594,
	FormatException__ctor_m4841,
	FormatException__ctor_m4978,
	GC_SuppressFinalize_m3895,
	Guid__ctor_m10595,
	Guid__ctor_m10596,
	Guid__cctor_m10597,
	Guid_CheckNull_m10598,
	Guid_CheckLength_m10599,
	Guid_CheckArray_m10600,
	Guid_Compare_m10601,
	Guid_CompareTo_m10602,
	Guid_Equals_m10603,
	Guid_CompareTo_m10604,
	Guid_Equals_m10605,
	Guid_GetHashCode_m10606,
	Guid_ToHex_m10607,
	Guid_NewGuid_m10608,
	Guid_AppendInt_m10609,
	Guid_AppendShort_m10610,
	Guid_AppendByte_m10611,
	Guid_BaseToString_m10612,
	Guid_ToString_m10613,
	Guid_ToString_m10614,
	Guid_ToString_m10615,
	IndexOutOfRangeException__ctor_m10616,
	IndexOutOfRangeException__ctor_m2160,
	IndexOutOfRangeException__ctor_m10617,
	InvalidCastException__ctor_m10618,
	InvalidCastException__ctor_m10619,
	InvalidCastException__ctor_m10620,
	InvalidOperationException__ctor_m4822,
	InvalidOperationException__ctor_m4818,
	InvalidOperationException__ctor_m10621,
	InvalidOperationException__ctor_m10622,
	LocalDataStoreSlot__ctor_m10623,
	LocalDataStoreSlot__cctor_m10624,
	LocalDataStoreSlot_Finalize_m10625,
	Math_Abs_m10626,
	Math_Abs_m10627,
	Math_Abs_m10628,
	Math_Ceiling_m10629,
	Math_Floor_m10630,
	Math_Log_m2162,
	Math_Max_m489,
	Math_Max_m2174,
	Math_Min_m490,
	Math_Min_m3893,
	Math_Round_m10631,
	Math_Round_m10632,
	Math_Sin_m10633,
	Math_Cos_m10634,
	Math_Tan_m10635,
	Math_Atan_m10636,
	Math_Log_m10637,
	Math_Pow_m10638,
	Math_Sqrt_m10639,
	MemberAccessException__ctor_m10640,
	MemberAccessException__ctor_m10641,
	MemberAccessException__ctor_m10642,
	MethodAccessException__ctor_m10643,
	MethodAccessException__ctor_m10644,
	MissingFieldException__ctor_m10645,
	MissingFieldException__ctor_m10646,
	MissingFieldException__ctor_m10647,
	MissingFieldException_get_Message_m10648,
	MissingMemberException__ctor_m10649,
	MissingMemberException__ctor_m10650,
	MissingMemberException__ctor_m10651,
	MissingMemberException__ctor_m10652,
	MissingMemberException_GetObjectData_m10653,
	MissingMemberException_get_Message_m10654,
	MissingMethodException__ctor_m10655,
	MissingMethodException__ctor_m10656,
	MissingMethodException__ctor_m10657,
	MissingMethodException__ctor_m10658,
	MissingMethodException_get_Message_m10659,
	MonoAsyncCall__ctor_m10660,
	AttributeInfo__ctor_m10661,
	AttributeInfo_get_Usage_m10662,
	AttributeInfo_get_InheritanceLevel_m10663,
	MonoCustomAttrs__cctor_m10664,
	MonoCustomAttrs_IsUserCattrProvider_m10665,
	MonoCustomAttrs_GetCustomAttributesInternal_m10666,
	MonoCustomAttrs_GetPseudoCustomAttributes_m10667,
	MonoCustomAttrs_GetCustomAttributesBase_m10668,
	MonoCustomAttrs_GetCustomAttribute_m10669,
	MonoCustomAttrs_GetCustomAttributes_m10670,
	MonoCustomAttrs_GetCustomAttributes_m10671,
	MonoCustomAttrs_GetCustomAttributesDataInternal_m10672,
	MonoCustomAttrs_GetCustomAttributesData_m10673,
	MonoCustomAttrs_IsDefined_m10674,
	MonoCustomAttrs_IsDefinedInternal_m10675,
	MonoCustomAttrs_GetBasePropertyDefinition_m10676,
	MonoCustomAttrs_GetBase_m10677,
	MonoCustomAttrs_RetrieveAttributeUsage_m10678,
	MonoTouchAOTHelper__cctor_m10679,
	MonoTypeInfo__ctor_m10680,
	MonoType_get_attributes_m10681,
	MonoType_GetDefaultConstructor_m10682,
	MonoType_GetAttributeFlagsImpl_m10683,
	MonoType_GetConstructorImpl_m10684,
	MonoType_GetConstructors_internal_m10685,
	MonoType_GetConstructors_m10686,
	MonoType_InternalGetEvent_m10687,
	MonoType_GetEvent_m10688,
	MonoType_GetField_m10689,
	MonoType_GetFields_internal_m10690,
	MonoType_GetFields_m10691,
	MonoType_GetInterfaces_m10692,
	MonoType_GetMethodsByName_m10693,
	MonoType_GetMethods_m10694,
	MonoType_GetMethodImpl_m10695,
	MonoType_GetPropertiesByName_m10696,
	MonoType_GetPropertyImpl_m10697,
	MonoType_HasElementTypeImpl_m10698,
	MonoType_IsArrayImpl_m10699,
	MonoType_IsByRefImpl_m10700,
	MonoType_IsPointerImpl_m10701,
	MonoType_IsPrimitiveImpl_m10702,
	MonoType_IsSubclassOf_m10703,
	MonoType_InvokeMember_m10704,
	MonoType_GetElementType_m10705,
	MonoType_get_UnderlyingSystemType_m10706,
	MonoType_get_Assembly_m10707,
	MonoType_get_AssemblyQualifiedName_m10708,
	MonoType_getFullName_m10709,
	MonoType_get_BaseType_m10710,
	MonoType_get_FullName_m10711,
	MonoType_IsDefined_m10712,
	MonoType_GetCustomAttributes_m10713,
	MonoType_GetCustomAttributes_m10714,
	MonoType_get_MemberType_m10715,
	MonoType_get_Name_m10716,
	MonoType_get_Namespace_m10717,
	MonoType_get_Module_m10718,
	MonoType_get_DeclaringType_m10719,
	MonoType_get_ReflectedType_m10720,
	MonoType_get_TypeHandle_m10721,
	MonoType_GetObjectData_m10722,
	MonoType_ToString_m10723,
	MonoType_GetGenericArguments_m10724,
	MonoType_get_ContainsGenericParameters_m10725,
	MonoType_get_IsGenericParameter_m10726,
	MonoType_GetGenericTypeDefinition_m10727,
	MonoType_CheckMethodSecurity_m10728,
	MonoType_ReorderParamArrayArguments_m10729,
	MulticastNotSupportedException__ctor_m10730,
	MulticastNotSupportedException__ctor_m10731,
	MulticastNotSupportedException__ctor_m10732,
	NonSerializedAttribute__ctor_m10733,
	NotImplementedException__ctor_m10734,
	NotImplementedException__ctor_m3897,
	NotImplementedException__ctor_m10735,
	NotSupportedException__ctor_m392,
	NotSupportedException__ctor_m4832,
	NotSupportedException__ctor_m10736,
	NullReferenceException__ctor_m10737,
	NullReferenceException__ctor_m2135,
	NullReferenceException__ctor_m10738,
	CustomInfo__ctor_m10739,
	CustomInfo_GetActiveSection_m10740,
	CustomInfo_Parse_m10741,
	CustomInfo_Format_m10742,
	NumberFormatter__ctor_m10743,
	NumberFormatter__cctor_m10744,
	NumberFormatter_GetFormatterTables_m10745,
	NumberFormatter_GetTenPowerOf_m10746,
	NumberFormatter_InitDecHexDigits_m10747,
	NumberFormatter_InitDecHexDigits_m10748,
	NumberFormatter_InitDecHexDigits_m10749,
	NumberFormatter_FastToDecHex_m10750,
	NumberFormatter_ToDecHex_m10751,
	NumberFormatter_FastDecHexLen_m10752,
	NumberFormatter_DecHexLen_m10753,
	NumberFormatter_DecHexLen_m10754,
	NumberFormatter_ScaleOrder_m10755,
	NumberFormatter_InitialFloatingPrecision_m10756,
	NumberFormatter_ParsePrecision_m10757,
	NumberFormatter_Init_m10758,
	NumberFormatter_InitHex_m10759,
	NumberFormatter_Init_m10760,
	NumberFormatter_Init_m10761,
	NumberFormatter_Init_m10762,
	NumberFormatter_Init_m10763,
	NumberFormatter_Init_m10764,
	NumberFormatter_Init_m10765,
	NumberFormatter_ResetCharBuf_m10766,
	NumberFormatter_Resize_m10767,
	NumberFormatter_Append_m10768,
	NumberFormatter_Append_m10769,
	NumberFormatter_Append_m10770,
	NumberFormatter_GetNumberFormatInstance_m10771,
	NumberFormatter_set_CurrentCulture_m10772,
	NumberFormatter_get_IntegerDigits_m10773,
	NumberFormatter_get_DecimalDigits_m10774,
	NumberFormatter_get_IsFloatingSource_m10775,
	NumberFormatter_get_IsZero_m10776,
	NumberFormatter_get_IsZeroInteger_m10777,
	NumberFormatter_RoundPos_m10778,
	NumberFormatter_RoundDecimal_m10779,
	NumberFormatter_RoundBits_m10780,
	NumberFormatter_RemoveTrailingZeros_m10781,
	NumberFormatter_AddOneToDecHex_m10782,
	NumberFormatter_AddOneToDecHex_m10783,
	NumberFormatter_CountTrailingZeros_m10784,
	NumberFormatter_CountTrailingZeros_m10785,
	NumberFormatter_GetInstance_m10786,
	NumberFormatter_Release_m10787,
	NumberFormatter_SetThreadCurrentCulture_m10788,
	NumberFormatter_NumberToString_m10789,
	NumberFormatter_NumberToString_m10790,
	NumberFormatter_NumberToString_m10791,
	NumberFormatter_NumberToString_m10792,
	NumberFormatter_NumberToString_m10793,
	NumberFormatter_NumberToString_m10794,
	NumberFormatter_NumberToString_m10795,
	NumberFormatter_NumberToString_m10796,
	NumberFormatter_NumberToString_m10797,
	NumberFormatter_NumberToString_m10798,
	NumberFormatter_NumberToString_m10799,
	NumberFormatter_NumberToString_m10800,
	NumberFormatter_NumberToString_m10801,
	NumberFormatter_NumberToString_m10802,
	NumberFormatter_NumberToString_m10803,
	NumberFormatter_NumberToString_m10804,
	NumberFormatter_NumberToString_m10805,
	NumberFormatter_FastIntegerToString_m10806,
	NumberFormatter_IntegerToString_m10807,
	NumberFormatter_NumberToString_m10808,
	NumberFormatter_FormatCurrency_m10809,
	NumberFormatter_FormatDecimal_m10810,
	NumberFormatter_FormatHexadecimal_m10811,
	NumberFormatter_FormatFixedPoint_m10812,
	NumberFormatter_FormatRoundtrip_m10813,
	NumberFormatter_FormatRoundtrip_m10814,
	NumberFormatter_FormatGeneral_m10815,
	NumberFormatter_FormatNumber_m10816,
	NumberFormatter_FormatPercent_m10817,
	NumberFormatter_FormatExponential_m10818,
	NumberFormatter_FormatExponential_m10819,
	NumberFormatter_FormatCustom_m10820,
	NumberFormatter_ZeroTrimEnd_m10821,
	NumberFormatter_IsZeroOnly_m10822,
	NumberFormatter_AppendNonNegativeNumber_m10823,
	NumberFormatter_AppendIntegerString_m10824,
	NumberFormatter_AppendIntegerString_m10825,
	NumberFormatter_AppendDecimalString_m10826,
	NumberFormatter_AppendDecimalString_m10827,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m10828,
	NumberFormatter_AppendExponent_m10829,
	NumberFormatter_AppendOneDigit_m10830,
	NumberFormatter_FastAppendDigits_m10831,
	NumberFormatter_AppendDigits_m10832,
	NumberFormatter_AppendDigits_m10833,
	NumberFormatter_Multiply10_m10834,
	NumberFormatter_Divide10_m10835,
	NumberFormatter_GetClone_m10836,
	ObjectDisposedException__ctor_m3900,
	ObjectDisposedException__ctor_m10837,
	ObjectDisposedException__ctor_m10838,
	ObjectDisposedException_get_Message_m10839,
	ObjectDisposedException_GetObjectData_m10840,
	OperatingSystem__ctor_m10841,
	OperatingSystem_get_Platform_m10842,
	OperatingSystem_Clone_m10843,
	OperatingSystem_GetObjectData_m10844,
	OperatingSystem_ToString_m10845,
	OutOfMemoryException__ctor_m10846,
	OutOfMemoryException__ctor_m10847,
	OverflowException__ctor_m10848,
	OverflowException__ctor_m10849,
	OverflowException__ctor_m10850,
	RankException__ctor_m10851,
	RankException__ctor_m10852,
	RankException__ctor_m10853,
	ResolveEventArgs__ctor_m10854,
	RuntimeMethodHandle__ctor_m10855,
	RuntimeMethodHandle__ctor_m10856,
	RuntimeMethodHandle_get_Value_m10857,
	RuntimeMethodHandle_GetObjectData_m10858,
	RuntimeMethodHandle_Equals_m10859,
	RuntimeMethodHandle_GetHashCode_m10860,
	StringComparer__ctor_m10861,
	StringComparer__cctor_m10862,
	StringComparer_get_InvariantCultureIgnoreCase_m4855,
	StringComparer_get_OrdinalIgnoreCase_m2190,
	StringComparer_Compare_m10863,
	StringComparer_Equals_m10864,
	StringComparer_GetHashCode_m10865,
	CultureAwareComparer__ctor_m10866,
	CultureAwareComparer_Compare_m10867,
	CultureAwareComparer_Equals_m10868,
	CultureAwareComparer_GetHashCode_m10869,
	OrdinalComparer__ctor_m10870,
	OrdinalComparer_Compare_m10871,
	OrdinalComparer_Equals_m10872,
	OrdinalComparer_GetHashCode_m10873,
	SystemException__ctor_m10874,
	SystemException__ctor_m4953,
	SystemException__ctor_m10875,
	SystemException__ctor_m10876,
	ThreadStaticAttribute__ctor_m10877,
	TimeSpan__ctor_m10878,
	TimeSpan__ctor_m10879,
	TimeSpan__ctor_m10880,
	TimeSpan__cctor_m10881,
	TimeSpan_CalculateTicks_m10882,
	TimeSpan_get_Days_m10883,
	TimeSpan_get_Hours_m10884,
	TimeSpan_get_Milliseconds_m10885,
	TimeSpan_get_Minutes_m10886,
	TimeSpan_get_Seconds_m10887,
	TimeSpan_get_Ticks_m10888,
	TimeSpan_get_TotalDays_m10889,
	TimeSpan_get_TotalHours_m10890,
	TimeSpan_get_TotalMilliseconds_m10891,
	TimeSpan_get_TotalMinutes_m10892,
	TimeSpan_get_TotalSeconds_m10893,
	TimeSpan_Add_m10894,
	TimeSpan_Compare_m10895,
	TimeSpan_CompareTo_m10896,
	TimeSpan_CompareTo_m10897,
	TimeSpan_Equals_m10898,
	TimeSpan_Duration_m10899,
	TimeSpan_Equals_m10900,
	TimeSpan_FromDays_m10901,
	TimeSpan_FromHours_m10902,
	TimeSpan_FromMinutes_m10903,
	TimeSpan_FromSeconds_m10904,
	TimeSpan_FromMilliseconds_m10905,
	TimeSpan_From_m10906,
	TimeSpan_GetHashCode_m10907,
	TimeSpan_Negate_m10908,
	TimeSpan_Subtract_m10909,
	TimeSpan_ToString_m10910,
	TimeSpan_op_Addition_m10911,
	TimeSpan_op_Equality_m10912,
	TimeSpan_op_GreaterThan_m10913,
	TimeSpan_op_GreaterThanOrEqual_m10914,
	TimeSpan_op_Inequality_m10915,
	TimeSpan_op_LessThan_m10916,
	TimeSpan_op_LessThanOrEqual_m10917,
	TimeSpan_op_Subtraction_m10918,
	TimeZone__ctor_m10919,
	TimeZone__cctor_m10920,
	TimeZone_get_CurrentTimeZone_m10921,
	TimeZone_IsDaylightSavingTime_m10922,
	TimeZone_IsDaylightSavingTime_m10923,
	TimeZone_ToLocalTime_m10924,
	TimeZone_ToUniversalTime_m10925,
	TimeZone_GetLocalTimeDiff_m10926,
	TimeZone_GetLocalTimeDiff_m10927,
	CurrentSystemTimeZone__ctor_m10928,
	CurrentSystemTimeZone__ctor_m10929,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10930,
	CurrentSystemTimeZone_GetTimeZoneData_m10931,
	CurrentSystemTimeZone_GetDaylightChanges_m10932,
	CurrentSystemTimeZone_GetUtcOffset_m10933,
	CurrentSystemTimeZone_OnDeserialization_m10934,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m10935,
	TypeInitializationException__ctor_m10936,
	TypeInitializationException_GetObjectData_m10937,
	TypeLoadException__ctor_m10938,
	TypeLoadException__ctor_m10939,
	TypeLoadException__ctor_m10940,
	TypeLoadException_get_Message_m10941,
	TypeLoadException_GetObjectData_m10942,
	UnauthorizedAccessException__ctor_m10943,
	UnauthorizedAccessException__ctor_m10944,
	UnauthorizedAccessException__ctor_m10945,
	UnhandledExceptionEventArgs__ctor_m10946,
	UnhandledExceptionEventArgs_get_ExceptionObject_m2142,
	UnhandledExceptionEventArgs_get_IsTerminating_m10947,
	UnitySerializationHolder__ctor_m10948,
	UnitySerializationHolder_GetTypeData_m10949,
	UnitySerializationHolder_GetDBNullData_m10950,
	UnitySerializationHolder_GetModuleData_m10951,
	UnitySerializationHolder_GetObjectData_m10952,
	UnitySerializationHolder_GetRealObject_m10953,
	Version__ctor_m10954,
	Version__ctor_m581,
	Version__ctor_m4840,
	Version__ctor_m10955,
	Version__ctor_m10956,
	Version_CheckedSet_m10957,
	Version_get_Build_m10958,
	Version_get_Major_m10959,
	Version_get_Minor_m10960,
	Version_get_Revision_m10961,
	Version_Clone_m10962,
	Version_CompareTo_m10963,
	Version_Equals_m10964,
	Version_CompareTo_m10965,
	Version_Equals_m10966,
	Version_GetHashCode_m10967,
	Version_ToString_m10968,
	Version_CreateFromString_m10969,
	Version_op_Equality_m10970,
	Version_op_Inequality_m10971,
	Version_op_LessThan_m582,
	WeakReference__ctor_m10972,
	WeakReference__ctor_m10973,
	WeakReference__ctor_m10974,
	WeakReference__ctor_m10975,
	WeakReference_AllocateHandle_m10976,
	WeakReference_get_Target_m10977,
	WeakReference_get_TrackResurrection_m10978,
	WeakReference_Finalize_m10979,
	WeakReference_GetObjectData_m10980,
	PrimalityTest__ctor_m10981,
	PrimalityTest_Invoke_m10982,
	PrimalityTest_BeginInvoke_m10983,
	PrimalityTest_EndInvoke_m10984,
	MemberFilter__ctor_m10985,
	MemberFilter_Invoke_m10986,
	MemberFilter_BeginInvoke_m10987,
	MemberFilter_EndInvoke_m10988,
	TypeFilter__ctor_m10989,
	TypeFilter_Invoke_m10990,
	TypeFilter_BeginInvoke_m10991,
	TypeFilter_EndInvoke_m10992,
	CrossContextDelegate__ctor_m10993,
	CrossContextDelegate_Invoke_m10994,
	CrossContextDelegate_BeginInvoke_m10995,
	CrossContextDelegate_EndInvoke_m10996,
	HeaderHandler__ctor_m10997,
	HeaderHandler_Invoke_m10998,
	HeaderHandler_BeginInvoke_m10999,
	HeaderHandler_EndInvoke_m11000,
	ThreadStart__ctor_m11001,
	ThreadStart_Invoke_m11002,
	ThreadStart_BeginInvoke_m11003,
	ThreadStart_EndInvoke_m11004,
	TimerCallback__ctor_m11005,
	TimerCallback_Invoke_m11006,
	TimerCallback_BeginInvoke_m11007,
	TimerCallback_EndInvoke_m11008,
	WaitCallback__ctor_m11009,
	WaitCallback_Invoke_m11010,
	WaitCallback_BeginInvoke_m11011,
	WaitCallback_EndInvoke_m11012,
	AppDomainInitializer__ctor_m11013,
	AppDomainInitializer_Invoke_m11014,
	AppDomainInitializer_BeginInvoke_m11015,
	AppDomainInitializer_EndInvoke_m11016,
	AssemblyLoadEventHandler__ctor_m11017,
	AssemblyLoadEventHandler_Invoke_m11018,
	AssemblyLoadEventHandler_BeginInvoke_m11019,
	AssemblyLoadEventHandler_EndInvoke_m11020,
	EventHandler__ctor_m11021,
	EventHandler_Invoke_m594,
	EventHandler_BeginInvoke_m11022,
	EventHandler_EndInvoke_m11023,
	ResolveEventHandler__ctor_m11024,
	ResolveEventHandler_Invoke_m11025,
	ResolveEventHandler_BeginInvoke_m11026,
	ResolveEventHandler_EndInvoke_m11027,
	UnhandledExceptionEventHandler__ctor_m2140,
	UnhandledExceptionEventHandler_Invoke_m11028,
	UnhandledExceptionEventHandler_BeginInvoke_m11029,
	UnhandledExceptionEventHandler_EndInvoke_m11030,
};
