﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12879_gshared (InternalEnumerator_1_t1999 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12879(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1999 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12879_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12880_gshared (InternalEnumerator_1_t1999 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12880(__this, method) (( void (*) (InternalEnumerator_1_t1999 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12880_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12881_gshared (InternalEnumerator_1_t1999 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12881(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1999 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12881_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12882_gshared (InternalEnumerator_1_t1999 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12882(__this, method) (( void (*) (InternalEnumerator_1_t1999 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12882_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12883_gshared (InternalEnumerator_1_t1999 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12883(__this, method) (( bool (*) (InternalEnumerator_1_t1999 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12883_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern "C" ContactPoint_t247  InternalEnumerator_1_get_Current_m12884_gshared (InternalEnumerator_1_t1999 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12884(__this, method) (( ContactPoint_t247  (*) (InternalEnumerator_1_t1999 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12884_gshared)(__this, method)
